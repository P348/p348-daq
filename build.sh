#!/bin/bash -e

# Figure out optimization/debug flags
if [[ $DEBUG_BUILD = 1 ]] ; then
    export CFLAGS="-g -ggdb -O0"
    export CMAKE_BUILD_TYPE="Debug"
else
    export CFLAGS="-O3 -DNDEBUG=1"
    export CMAKE_BUILD_TYPE="Release"
fi
export CXXFLAGS=$CFLAGS


echo "===> Compiling DATE monitoring library..."
cd date
DATEDIR=$(pwd)
rm -rf monitoring/Linux lib include
./build.sh

echo "===> Compiling eventDumpAllNew..."
cd eventDumpAllNew
make clean
make
cd ../..

echo "===> Compiling libsdc..."
if [[ "${SDC_DIR}" == "" ]] ; then
    rm -rf sdc
    git clone --depth 1 --branch v0.3 https://github.com/CrankOne/sdc.git
    make -C sdc install
fi

echo "===> Configuring Coral..."
cd coral
export ROOTSYS=$(root-config --prefix)
./configure --without-ORACLE --without-MySQL --disable-CERN_LIBRARY --disable-HIGZ --disable-HBOOK --disable-ZEBRA --without-CLHEP --without-RFIO --without-XROOTD --without-EXPAT --without-TGEANT --with-CFLAGS="$CFLAGS"
rm -rf lib/Linux
mkdir -p lib/Linux
echo ""

echo "===> Compiling GEM library..."
cd src/geom/gem
touch dummy0.pcm
make clean
make -j1 LIB_TYPE=static
echo ""

echo "===> Compiling MuMega library..."
cd ../mumega
touch dummy1.pcm
make clean
make -j1 LIB_TYPE=static
echo ""

echo "===> Compiling decoding library..."
cd ../../DaqDataDecoding
./configure --with-DATE_LIB=$DATEDIR
make clean
make -j4
echo ""

echo "===> Compiling na64utils-msadc library..."
cd ../../..
if [[ "${NA64UTIL_DIR}" == "" ]] ; then
    rm -rf na64utils-msadc
    git clone --depth 1 --branch v1.16 https://gitlab.cern.ch/P348/na64utils-msadc.git
    cd na64utils-msadc
    make
else
    cd p348reco
fi

echo "===> Compiling P348 library..."
cd ../p348reco
make clean
make libp348.a
echo ""

echo "===> Compiling MurphyTV..."
cd ../coral/src/DaqDataDecoding/examples/MurphyTV
make clean
make

echo "===> Compiling Coool..."
cd ../../../coool
DIR_P348="`pwd`/../../../p348reco"
export LIBS="$LIBS -lp348 -L$DIR_P348"
export CXXFLAGS="$CXXFLAGS -I$DIR_P348"
export CFLAGS="$CFLAGS -I$DIR_P348"
./configure --with-ROOT=$ROOTSYS --with-CFLAGS="$CXXFLAGS"
make clean
make -j4
