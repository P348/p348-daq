# NA64 Experiment Reconstruction Code

## Introduction

Welcome to the P348 daq software repository. This codebase is dedicated to the 
reconstruction and analysis of experimental data collected from the NA64 experiment conducted at
CERN (European Organization for Nuclear Research). 

## Key Features

- **Data and Monte Carlo reconstruction**: Process both the data recorded by the experiment located in 
  `/eos/experiment/na64/data/cdr` as well as the output files from the 
  [NA64 simulation framework](https://gitlab.cern.ch/P348/na64-simulation). Digitize hits and smear 
  energy depositions in simulated events to obtain a more realistic detector response.
- **Track Reconstruction:** Implement techniques to reconstruct particle tracks from
  detector signals and obtain the particle's momentum.
- **Event Visualization:** Generate visual representations of reconstructed events for
  analysis and validation. This includes displaying the hit positions in the trackers as well as
  the complete waveforms recorded in the calorimeters. Please refer to the programs: `event-display.exe`
  and `print-reco-event.exe`.

## Getting Started

To begin using the NA64 Experiment Reconstruction Code, follow these steps in LXPLUS:

1. Clone this repository to your local directory.
```bash
git clone https://gitlab.cern.ch/P348/p348-daq.git
```
2. Compile the software
```bash
cd p348-daq
./build.sh
```
3. (Optional) Install [GenFit](https://github.com/GenFit/GenFit) and [tracking-tools](https://gitlab.cern.ch/P348/tracking-tools) packages to get the most out of the track reconstruction:
```bash
cd p348reco
make genfit-install
make tracking-tools-install
export GENFIT=1 TRACKINGTOOLS=1
```
4. Compile the reconstruction code:
```bash
cd p348reco
make
```
5. Now you are all set! Start reconstructing events by running the usage example:
```bash
./example.exe /eos/experiment/na64/data/cdr/cdr01001-005133.dat
```

Detailed instructions and usage examples can be found in a dedicated [Twiki Page](https://twiki.cern.ch/twiki/bin/view/P348/DataAnalysis).

## News

Please refer to the [boards page](https://gitlab.cern.ch/P348/p348-daq/-/boards) and [activity page](https://gitlab.cern.ch/P348/p348-daq/activity) 
to stay up-to-date with the newest developments of this repository.

Profit from these latest developments by updating your existing software installation and
recompiling it:
```bash
cd p348-daq
git checkout master
git pull
./build.sh
