/*!
        \file    CsTriggerMatrix.h
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */


#ifndef CsTriggerMatrix_h
#define CsTriggerMatrix_h

#include "CsDigit.h"
#include <array>
#include <list>
#include <bitset>
#include <array>

/*! \class CsTriggerMatrix
    \brief Trigger Logic Class.

    Contains some method to rebuild trigger logic signals
 */


class CsTriggerMatrix {

private:

    std::vector<std::string> _name = {"LAS", "OY", "MY"};
    std::vector<std::string> _path;
    std::vector<std::array<std::array<int,32>,32> > _pattern;

public:

    CsTriggerMatrix();
    ~CsTriggerMatrix();

    void Print() const;
    inline int GetN() const {
        return _name.size();
    }

    std::string GetName(std::string) const;
    std::string GetName(CsDigit*) const;
    int GetIndex(std::string) const;
    int GetIndex(CsDigit*) const;
    int GetOffsetX(std::string) const;
    int GetOffsetX(CsDigit*) const;
    int GetOffsetY(std::string) const;
    int GetOffsetY(CsDigit*) const;

    std::vector<std::array<std::array<int,32>,32> > GetPattern() {
        return _pattern;
    };
    bool CheckCoincidence(CsDigit* digit_x, CsDigit* digit_y) const;
    bool Load(std::string, std::string);
};

#endif // CsTriggerMatrix_h
