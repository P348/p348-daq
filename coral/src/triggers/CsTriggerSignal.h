/*!
        \file    CsTriggerSignal.h
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */


#ifndef CsTriggerSignal_h
#define CsTriggerSignal_h

#include "CsDigit.h"
#include <array>
#include <list>
#include <bitset>
#include "CsTriggerMatrix.h"

/*! \class CsTriggerSignal
    \brief Trigger Logic Class.

    Contains some method to reCompute trigger logic signals
 */

class CsTriggerSignal {

private:

    CsTriggerMatrix *_pTriggerMatrix = NULL;
    const std::vector<std::pair<std::string,std::string> > LAST    = { std::make_pair("HG02Y1","HG01Y1"), std::make_pair("HG02Y2","HG01Y1") };
    const std::vector<std::pair<std::string,std::string> > OT      = { std::make_pair("HO04Y1","HO03Y1"), std::make_pair("HO04Y2","HO03Y1") };
    const std::vector<std::pair<std::string,std::string> > MT      = { std::make_pair("HM04Y1","HM05Y1") };

    std::vector<std::pair<double,int> > _tsignal; // signal[i-th][TIME,MULTIPLICITY]
    std::string _tname;
    int _tbit;
    //double timecut;
    //int majority_veto;

    // Methods related to hodoscope and trigger signal
    std::vector<std::pair<double,int> > GetGandalfMatrix(std::vector<CsDigit*>, std::vector<CsDigit*>);

public:

    CsTriggerSignal(std::string, int, CsTriggerMatrix*);
    ~CsTriggerSignal();

    void Print() const;
    void Reset();

    std::string GetName();
    int GetBit();
    std::vector<std::pair<double,int> > GetSignal();

    bool Compute(std::vector<CsDigit*>);
    bool ComputeLAST2(std::vector<CsDigit*>);

    bool IsBitSet();
};

#endif // CsTriggerSignal_h
