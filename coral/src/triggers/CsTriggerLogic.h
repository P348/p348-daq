/*!
        \file    CsTriggerLogic.h
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */


#ifndef CsTriggerLogic_h
#define CsTriggerLogic_h

#include "CsDigit.h"
#include <array>
#include <list>
#include <bitset>
#include "CsInit.h"
#include "DaqDataDecoding/Trigger.h"

#include "CsTriggerMatrix.h"
#include "CsTriggerSignal.h"
#include "CsVetoSignal.h"

/*! \class CsTriggerLogic
    \brief Trigger Logic Class.

    Contains some method to rebuild trigger logic signals
 */

class CsTriggerLogic {

public:
    static constexpr double tdc_res_bms = 0.06446156;         //BMS
    static constexpr double tdc_res_gandalf = 0.160751028;         //SciFiJ on Gandalf
    static constexpr double tdc_res_hodo = 0.128;          //0.108;

private:

    bool _triggerLogicFlag;

    std::vector<CsTriggerSignal> _vTriggerSignal;
    CsTriggerMatrix _triggerMatrix;
    CsVetoSignal _vetoSignal;

    std::vector<CsDigit*> _vHodoDigits;

public:

    CsTriggerLogic();
    ~CsTriggerLogic();

    void Reset();
    inline bool IsFlagSet() {
        return _triggerLogicFlag;
    }

    static std::vector<CsDigit*> GetDigits(std::string, std::list<CsDigit*>);
    static std::vector<CsDigit*> GetDigits(std::string, std::vector<CsDigit*>);

    bool Compute(std::list<CsDigit*> lDigits, std::vector<unsigned int>);
    void Print();

    int GetTriggerMask();
    int GetTriggerMaskWithoutVetoDelay();

    const std::vector<std::vector<std::pair<double,int> > > GetTriggerSignal();
    const std::vector<std::vector<std::pair<double,int> > > GetTriggerSignalWithoutVetoDelay();

    void AddBit(std::string, int);
    int GetN() const {
        return _vTriggerSignal.size();
    };
};

#endif // CsTriggerLogic_h
