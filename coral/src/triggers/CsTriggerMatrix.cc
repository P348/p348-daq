/*!
        \file    CsTriggerMatrix.cc
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */

#include "CsTriggerMatrix.h"
#include "CsEvent.h"
#include "CsGeom.h"
#include "CsOpt.h"
#include "CsMCDigit.h"
#include "CsMCUtils.h"
#include "CsHistograms.h"

#ifdef COMPASS_USE_OSPACE_STD
#  include <ospace/std/algorithm>
#else
# include <algorithm>
#endif

using namespace std;

CsTriggerMatrix::CsTriggerMatrix(){

    _path.resize(_name.size());
    _pattern.resize(_name.size());
};

CsTriggerMatrix::~CsTriggerMatrix() {
};

int CsTriggerMatrix::GetIndex(string name) const
{
    name = GetName(name); // Transform TBname to trigger matrix name

    for(int i = 0, N = _name.size(); i < N; i++)
        if(_name[i].compare(name) == 0) return i;

    CsErrLog::msg(elFatal,__FILE__,__LINE__, "Unknown name for trigger matrix.. \"%s\"", name.c_str());
    return -1;
}

bool CsTriggerMatrix::Load(std::string name, std::string path)
{
    int i = GetIndex(name);
    if(i < 0) CsErrLog::msg(elFatal,__FILE__,__LINE__, "Unknown name for trigger matrix.. \"%s\"", name.c_str());

    _path[i] = path;
    if(_path[i].empty()) {
        CsErrLog::msg(elWarning,__FILE__,__LINE__, "No path provided for \"%s\"", _name[i].c_str());
        return false;
    }

    ifstream f(_path[i], ios::out);
    if(!f.is_open()) {
        CsErrLog::msg(elWarning,__FILE__,__LINE__, "Cannot load trigger matrix file : \"%s\"", _name[i].c_str());
        return false;
    }

    cout << "Loading.. " << _path[i] << endl;
    for(int y = 0; y < 32; y++) for(int x = 0; x < 32; x++) f >> _pattern[i][x][31-y];

    f.close();
    return true;
};

string CsTriggerMatrix::GetName(CsDigit* digit) const {

    if(digit == NULL || digit->getDet() == NULL)
        CsErrLog::msg(elFatal,__FILE__,__LINE__,"Cannot compute matrix offset wrong digit information..");

    return GetName(digit->getDet()->GetTBName());
}

string CsTriggerMatrix::GetName(string tbname) const {

    if(tbname.rfind("HG") == 0 || tbname.rfind("LAS") == 0) return "LAS";
    if(tbname.rfind("HO") == 0 || tbname.rfind("OY") == 0) return "OY";
    if(tbname.rfind("HM") == 0 || tbname.rfind("MY") == 0) return "MY";

    CsErrLog::msg(elFatal,__FILE__,__LINE__,"Cannot return a trigger matrix name.. unknown string(=%s) value", tbname.c_str());
    return "";
}

int CsTriggerMatrix::GetIndex(CsDigit* digit) const {

    if(digit == NULL || digit->getDet() == NULL)
        CsErrLog::msg(elFatal,__FILE__,__LINE__,"Cannot compute matrix offset wrong digit information..");

    return GetIndex(digit->getDet()->GetTBName());
}

int CsTriggerMatrix::GetOffsetX(CsDigit* digit) const {

    if(digit == NULL || digit->getDet() == NULL)
        CsErrLog::msg(elFatal,__FILE__,__LINE__,"Cannot compute matrix offset wrong digit information..");

    return GetOffsetX(digit->getDet()->GetTBName());
}

int CsTriggerMatrix::GetOffsetX(std::string tbname) const {

    string tmatrix_name = this->GetName(tbname);

    if(tmatrix_name.compare("LAS")) return 0;
    if(tmatrix_name.compare("OY")) return (tbname.rfind("HO04Y2") == 0) ? 16 : 0;
    if(tmatrix_name.compare("MY")) return 0;

    CsErrLog::msg(elFatal,__FILE__,__LINE__, "Unknown trigger id (tbname:%s), cannot compute matrix x-offset", tbname.c_str());
    return 0;
};

int CsTriggerMatrix::GetOffsetY(CsDigit* digit) const {

    if(digit == NULL || digit->getDet() == NULL)
        CsErrLog::msg(elFatal,__FILE__,__LINE__,"Cannot compute matrix offset wrong digit information..");

    return GetOffsetY(digit->getDet()->GetTBName());
}
int CsTriggerMatrix::GetOffsetY(std::string tbname) const {

    string tmatrix_name = this->GetName(tbname);

    if(tmatrix_name.compare("LAS")) return 0;
    if(tmatrix_name.compare("OY")) return 14;
    if(tmatrix_name.compare("MY")) return 0;

    CsErrLog::msg(elFatal,__FILE__,__LINE__, "Unknown trigger id (tbname:%s), cannot compute matrix y-offset", tbname.c_str());
    return 0;
};

bool CsTriggerMatrix::CheckCoincidence(CsDigit* digit_x, CsDigit* digit_y)  const
{
    int id0 = GetIndex(digit_x->getDet()->GetTBName());
    int id1 = GetIndex(digit_y->getDet()->GetTBName());
    if(id0 != id1) return 0;

    int i = digit_x->getAddress(); // TBD
    int i0 = GetOffsetX(digit_x->getDet()->GetTBName());
    if(i == -1) return 0;

    int j = digit_y->getAddress(); // TBD
    int j0 = GetOffsetY(digit_y->getDet()->GetTBName());
    if(j == -1) return 0;

    return (_pattern[id0][i+i0][j+j0] != 0);
};

void CsTriggerMatrix::Print() const {

    for(int i = 0, N = _name.size(); i < N; i++) {
        cout << "Trigger matrix \""<< _name[i] << "\": " << _path[i] << endl;
        for(int y = 0; y < 32; y++) {
            for(int x = 0; x < 32; x++) cout << _pattern[i][x][31-y] << " ";
            cout << endl;
        }
        cout << endl;
    }
}
