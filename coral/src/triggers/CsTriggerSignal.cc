/*!
        \file    CsTriggerSignal.cc
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */

#include "CsTriggerSignal.h"
#include "CsEvent.h"
#include "CsGeom.h"
#include "CsOpt.h"
#include "CsMCDigit.h"
#include "CsMCUtils.h"
#include "CsHistograms.h"

#ifdef COMPASS_USE_OSPACE_STD
#  include <ospace/std/algorithm>
#else
# include <algorithm>
#endif

using namespace std;

template <typename T> std::vector<std::vector<T>> cartesian_product(std::vector<T> a, std::vector<T> b) {

    std::vector<std::vector<T>> c(a.size()*b.size());
    for(int i = 0, I = a.size(); i < I; i++) {
        for(int j = 0, J = b.size(); j < J; j++) {

            c[i*J+j][0] = a[i];
            c[i*J+j][1] = b[j];
        }
    }

    return c;
}

//
// ! CsTriggerSignal
//

CsTriggerSignal::CsTriggerSignal(std::string name, int tbit, CsTriggerMatrix *pTriggerMatrix)
{
    _tname = name;
    _tbit = tbit;

    if(pTriggerMatrix == NULL) CsErrLog::msg(elFatal,__FILE__,__LINE__, "Trigger matrix is a NULL prt..");
    _pTriggerMatrix = pTriggerMatrix;
}

CsTriggerSignal::~CsTriggerSignal() {
}

std::string CsTriggerSignal::GetName() {
    return this->_tname;
}

int CsTriggerSignal::GetBit() {
    return this->_tbit;
}

std::vector< pair<double,int> > CsTriggerSignal::GetSignal() {
    return this->_tsignal;
}
bool CsTriggerSignal::IsBitSet()
{
    return _tsignal.size();
}

void CsTriggerSignal::Print() const
{
    if(!_tsignal.size()) {

        cout << "##### Summary: Trigger \"" << _tname << "\" not fired #####" << endl;
        return;
    }

    cout << "##### Summary: Trigger signal for \"" << _tname << "\" #####" << endl;
    for(int i = 0, N = _tsignal.size(); i < N; i++) {
        cout << " Trigger signal at t = " << _tsignal[i].first << " ns, with multiplicity = " << _tsignal[i].second << endl;
    }

    cout << endl;
}

void CsTriggerSignal::Reset()
{
    _tsignal.clear();
}

vector< pair<double,int> > CsTriggerSignal::GetGandalfMatrix(std::vector<CsDigit*> vDigitsX, std::vector<CsDigit*> vDigitsY)
{
    vector<pair<double,int> > gm;
    vector< vector<CsDigit*> > vDigitsXY = cartesian_product(vDigitsX, vDigitsY);
    for(int i = 0, N = vDigitsXY.size(); i < N; i++) {

        int idX = _pTriggerMatrix->GetIndex(vDigitsXY[i][0] -> getDet()->GetTBName());
        int channelX  = vDigitsXY[i][0] -> getAddress() + _pTriggerMatrix->GetOffsetX(vDigitsXY[i][0]);
        double timeX  = vDigitsXY[i][0] -> getDatum();

        int idY = _pTriggerMatrix->GetIndex(vDigitsXY[i][1] -> getDet()->GetTBName());
        int channelY  = vDigitsXY[i][1] -> getAddress() + _pTriggerMatrix->GetOffsetX(vDigitsXY[i][0]);
        double timeY  = vDigitsXY[i][1] -> getDatum();

        if(idX != idY) continue;

        if( abs(timeY-timeX) <= 195*CsTriggerLogic::tdc_res_hodo) {

            double t = (timeY+timeX)/2;
            int x = channelX;
            int y = channelY;

            CsErrLog::msg(elInfo,__FILE__,__LINE__, "Candidate hit pair (%d,%d)..", x, y);
            if(_pTriggerMatrix->GetPattern()[idX][channelX][channelY]) {

                CsErrLog::msg(elInfo,__FILE__,__LINE__, "Hit pair (%d,%d) accepted by the trigger matrix.. at t = %f", x, y, t);
                if(!gm.size()) gm.push_back( make_pair(t, 1) );
                else {

                    for(int j = 0, J = gm.size(); j < J; j++) {

                        if(t > gm[j].first-20*CsTriggerLogic::tdc_res_hodo && t < gm[j].first+20*CsTriggerLogic::tdc_res_hodo) {
                            gm[j].first  = (gm[j].first*gm[j].second + t) / ++gm[j].second;
                        } else {
                            gm.push_back( make_pair(t, 1) );
                        }
                    }
                }
            }
        }
    }

    return gm;
}

bool CsTriggerSignal::Compute(std::vector<CsDigit*> vDigits)
{
    CsErrLog::msg(elInfo, __FILE__,__LINE__,"Computing software trigger signal \"%s\"..", _tname.c_str());
    if(_tname.compare("LAST2") == 0) this->ComputeLAST2(vDigits);
    // elseif ... // You can implement here other trigger logic
    else {

        CsErrLog::msg(elWarning,__FILE__,__LINE__, "Unknown trigger name.. cannot compute trigger logic"); // To be set as elFatal once DAQ decoding applied
    }

    return true;
}

bool CsTriggerSignal::ComputeLAST2(std::vector<CsDigit*> vDigits) {

    vector<pair<double,int> > gm;

    for(int i = 0; i < 2; i++) {

        string tbname0 = this->LAST[i].first;
        string tbname1 = this->LAST[i].second;
        vector<CsDigit*> v0 = CsTriggerLogic::GetDigits(tbname0, vDigits);
        vector<CsDigit*> v1 = CsTriggerLogic::GetDigits(tbname1, vDigits);
        vector<pair<double,int> > gm0 = GetGandalfMatrix(v0, v1);
        gm.insert(gm.end(), gm0.begin(), gm0.end());
    }

    if(gm.size() >= 2) _tsignal = gm;
    gm.clear();

    return 1;
}
