/*!
        \file    CsVetoSignal.h
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */


#ifndef CsVetoSignal_h
#define CsVetoSignal_h

#include "CsDigit.h"
#include <array>
#include <list>
#include <bitset>

/*! \class CsVetoSignal
    \brief Trigger Logic Class.

    Contains some method to rebuild trigger logic signals
 */

typedef struct {

    std::string tbname;
    unsigned int ch;
    double time;

} ScalerStruct;

class CsVetoSignal {

private:
    std::vector<ScalerStruct> VTsum;
    std::vector<double> Vtot;
    std::vector<double> VNtot;

public:

    CsVetoSignal();
    ~CsVetoSignal();

    void Reset();

    // Methods related to veto signal
    bool Compute(std::vector<unsigned int>);
    void Print() const;

    double GetDuration() const;
    int GetMultiplicity() const;
    bool IsVetoApplied(double) const;
};

#endif // CsVetoSignal_h
