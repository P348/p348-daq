// TraFDic on NA64 2016 data @ CERN

Experiment NA64

//                                         ***** PHAST OUTPUT
// - Active only if used on a PHAST-compatible executable, cf. "./README"
// - Cf. "$PHAST/coral/README" for the details of the options
mDST file		mDST.root
// - Information reduction bit pattern: "mDST select".
mDST select		0	// all-out unconditional output
// - One may want to output more info, as exemplified infra:
mDST hits		GM MX ST // Store hits in mDST
mDST Digits		SRD ECAL HCAL WCAL // Raw information after calibrations are applied: only useful for non-tracking detectors, else "mDST hits" is good enough.
mDST DAQdigits  	SRD ECAL HCAL WCAL // Raw information as read from raw data stream, no calibrations applied)
//mDST selectTrigger	1024	// Random trigger: unconditional output (Note: the option is not useful when "mDST select 0", i.e. all-out unconditional output.)
//mDST AllScalersTriggerMask	1024	// Write out all beam scalers for random trigger

//					// ***** WRITE-BACK of RAW DATA
//				<File name>	<Type>
//CsKalmanFitting WriteBack	evtDump.raw	Physics		// As of 07/01: D*, excl. phi, K0, J/psi
//CsEvent         WriteBack	evtDump.raw	Randoms		// <File name> may differ from CsKalmanFitting's


//					   ***** HISTOGRAMMING
histograms package	ROOT
histograms home		coral_hist.root



//					   ***** THIS IS A DATA RUN

Data job


Data file	/eos/experiment/na64/data/cdr/cdr01001-002363.dat

// 					   ***** DETECTOR TABLE...
//  A full directory (path name terminated by a slash "/") or the full path to
// a particular file can be specified.

//detector table        	/afs/cern.ch/work/k/ketzer/public/na64/detector/geometry/2016/detectors.001996.dat 
//detector table		/afs/cern.ch/user/m/mhosgen/public/geometry/2016/detectors.001996.dat
//detector table		./detectors.002211.dat
//detector table		/afs/cern.ch/user/v/vkolosa/public/NA64_Setup_ALL/NA64_Setup_2016/detectors/donskov_det.dat

detector table			/afs/cern.ch/user/m/mhosgen/public/geometry/2016/detectors.002211.dat

// 					   ***** ROOTGeometry
CsROOTGeometry file	$COMPASS_FILES/geometry/DVCS.2012/ROOTGeometry/detectors.dvcs.r446.C 	// Temporary setting, until 2016 file becomes available

//					   ***** CALIBRATION
use calibration

//CDB use 	MySQLDB
//CDB server	wwwcompass	// This is an alias, pointing, as of 10/11, to "compass02" located in computer center (reachable outside Cern)
// "CDB entrytime Y-M-D-H:M:S" can be entered to reject all calibrations issued
// after specified date. CDB being constantly evolving, leaving "CDB entrytime"
// disabled may give rise to unpredictable behaviour.
//CDB	entrytime	2010-12-01-00:00:00
// "entrytime" entry can be made to affect only a (set of) specific TBName(s):
// "TBname(or TB) CDBentrytime(sic!)"
//FI	CDBentrytime 	2010-10-31-23:59:59	// Disregarding the FI calibs entered in 201/11: not yet finalised.
// CDB can be replaced by a File DB, for any given (set of) TBname(s):

CDB use 	FileDB

GM FileDB       /afs/cern.ch/work/m/mhosgen/public/detector/gem/2016/pedestals/
MX FileDB       /afs/cern.ch/work/m/mhosgen/public/detector/mm/2016/pedestals/

EC FileDB       /afs/cern.ch/user/v/vkolosa/public/NA64_Setup_ALL/NA64_Setup_2016/DB_NA64_OLD.coral
HC FileDB       /afs/cern.ch/user/v/vkolosa/public/NA64_Setup_ALL/NA64_Setup_2016/DB_NA64_OLD.coral

//					   ***** MAPPING
decoding map    /afs/cern.ch/work/m/mhosgen/public/maps/2016.xml/


no trigger time decoding

//					   ***** EVENT SELECTION
events to read 100000			// # of events to read
events to skip 1
//selection trigger mask 20f		// Trigger selection (hexadecimal)
selection zero trigger mask 		// Events with zero trigger mask will be selected


pattern method 1        // not used yet
tracking method 1       // not used yet

// 					   ***** DECODING
make decoding 	 	// <nothing>, MCExact
decode NoTriggerTime	// No TriggerTime decoding and subtraction 

//  	 	 	 	 	   ***** CLUSTERING
make clustering 	// <nothing>, MCExact, MCSmeared, MCQuantized

//					   ***** RECONSTRUCTION SCHEMA
reconstruction schema 1

//					   ***** TRACKING
make tracking
track prepattern method traffic
track bridging   method traffic
track fitting    method traffic

//					   ***** CALORIMETERS
make	calorimeters	reconstruction
include ${CORAL}/src/pkopt/calorim_na64_2016.opt

// Comment next two lines to switch OFF use of LED/Laser calibrations for ECAL1/2 (in that case, also switch off pi0 calib below)
//EC01P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=YES USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=NO
//EC02P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=YES USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=YES
// Comment next two lines to switch OFF use of pi0 calibrations for ECAL1/2
//EC01P1__ MoreRecoOptions USE_EDEPCORR=_EdepCorr
//EC02P1__ MoreRecoOptions USE_EDEPCORR=_EdepCorr_RecoCombined USE_TISDEPCORR=_TiSdepCorr_RecoCombined

//include ${CORAL}/src/pkopt/ecal0_2012_rd.opt

//  	 	 	 	 	   ***** GEOMETRICAL ZONES
define	 zone	0        4417.0	        before magnet
define	 zone	4417.0   30000.0	after magnet

//  	  	  	  	  	   ***** MAGNETIC FIELD MAPS
//CsField SM1m_field_measured $COMPASS_FILES/maps/mag_fields/SM1m/SM1M.map.172.data
//CsField SM2_field	 $COMPASS_FILES/maps/mag_fields/SM2/FSM.map.4000.data

//					   ***** MAGNETs INFO

//CsMagInfo	MySQLDB

//CsMagInfo	SM2	1


//					   ***** LOGGER
error logger log level		error	// debugging, verbose, info, anomaly, warning, error, fatal
error logger store level	none
error logger verbosity		normal	// low, normal, high. 

//	  	  	  	  	   ***** TraFDic (+DET OPTIONS+...)

TraF    iCut    [15]    -1       // Beam charge. N.B.: has to be overwritten in the main options file for mu- data.
TraF	dCut	[2]	1000	 // Chi2 cut for track (TEvPrePattern.cc)

TraF	dCut	[7]	10000	 // Chi2/ndf preliminary cut for track pair candidate in TEvBrigeSegments3.cc
TraF	dCut	[9]	100	 // Chi2/ndf final       cut for track pair candidate in TEvBrigeSegments3.cc
TraF	dCut	[17]	0	 // Chi2/ndf cut for fitted track in TEvTracksFit3.cc (== 0 means "no cut")


// Pattern Reco Parameters (1 line fo every groupe)
				// MIN. # OF HITS for PROJ. TRACKS
				    // MAX. # OF COMMON HITS for 2 SPACE TRACKS
				        // MIN. # OF HITS FOR SPACE TRACK
						// SUBSEQUENT ITERATIONS
								// MAX. # OF COMMON HITS for 2 PROJ. TRACKS (iter. 1 and 2 sqq, for short and long segments)
TraF	iPRpar [  0 -  9 ]	2   2   4	2  2  4		1 1   3 2
TraF	iPRpar [ 10 - 17 ]	2   2   5	2  2  4		1 1
//TraF	iPRpar [  0 -  9 ]	2   2   4	2  2  4		1 1   3 2
//TraF	iPRpar [ 10 - 17 ]	2   2   4	2  2  4		1 1

TraF	iPRpar [50]   	 	1

// CUTS and PARAMETERS for PATTERN RECOGNITION (TEv::PrePattern;TAlgo::FindProj,FindSpace)
				// HIT SEARCH WINDOW IN SIGMAS OF RESOLUTION
                        	         // MAX PROJECTION TRACK SLOPE (tan(alpha))
						  // EXTRA TO ACCOMODATE CURVATURE in X and Y
TraF	dPRpar [  0 -  3 ]	400.   0.100	0.110	0.020
TraF	dPRpar [ 10 - 13 ]	400.   0.100 	0.110	0.050



// TBNAMES of DETECTORS to be EXCLUDED FROM PATTERN RECOGNITION:
//TraF	DetNameOff


TraF	Graph	[0]	0	// coral's graphics switches off automatically under BATCH @ CERN, gridKa and Lyon. This option forces it to do so in any case.
TraF	Graph	[3]	3       // countur plot of mag field. 3 - draw field intensity.
TraF	Graph	[6]	0	// graphical debugging: stop at each tracking step
TraF 	DefView	[0-3]	-180  2100.  -50.  50.	// Default field of view

//TraF	iCut	[0]	7	// Trigger selection in TraFDic

TraF	Print	[0]	1	// if == 0 all prints are OFF
TraF	Print	[3]	1       // TSetupInit
TraF	Print	[4]	0       // TEvBridgeSegments

// Histograms 
TraF	Hist	[0]	1       // if == 0, all histograms are OFF
TraF	Hist	[3]	1       // TAlgoAlignment
TraF	Hist	[4]	1       // TEvBridgeSegments
TraF	Hist	[1]	2	// residual test???

// Overwrite trafdic.2016.opt
TraF    ReMode	[3]	0	// if != 0 - Bridging is off
TraF    ReMode	[11]	0	// if != 0 - Alternative PrePattern
TraF    ReMode	[12]	3	// if != 0 - Alternative Bridging
TraF    ReMode	[13]	3	// if != 0 - Alternative Fitting
TraF    ReMode	[14]	0	// Dico fit: 0x7: in PR, 0x8: in Bridging	(TEv::PrePattern2,BridgeSegments2)
TraF	ReMode	[17]	0	// GEM correlations
Traf	ReMode	[6]	0	// if > 0 "ideal" bridging in the case of MC	

// From: Vladimir Kolosov
// Sent: Thu 2/28/2008 9:14 AM
// Let's decide to calculate track position 10 cm upstream front surface of a calorimeter.
// calo  EC01P1__  1111.500 - 45./2 - 10. = 1111.500 - 32.5 = 1079.0
// calo  EC02P1__  3325.200 - 45./2 - 10. = 3325.200 - 32.5 = 3292.7
// calo  HC01P1__  1267.500 -100./2 - 10. = 1267.500 - 60.0 = 1207.5
// calo  HC02P1__  3576.000 -112./2 - 10. = 3576.000 - 66.0 = 3510.0
// (NOTA BENE: The positions of E/HCAL1 may in fact differ from what is quoted supra and corresponds to the 2006/7 setup.)

//			SMOOTHING POINTS
// TraF	SmoothPos [0]	745.		// RICH
//TraF	SmoothPos [1]	1079.0	// Smooth track at ECAL1
//TraF	SmoothPos [2]	1207.5	// Smooth track at HCAL1
//TraF	SmoothPos [3]	3292.7	// Smooth track at ECAL2
//TraF	SmoothPos [4]	3510.0	// Smooth track at HCAL2

//
// GEM related option
//
// Thresholds for strip and cluster amplitudes in units of sigma
GM       Threshold [0-1] 3.  5.

// Cut on Cluster Size
GM ClusterSize [0-1] 2. 30.

/ Polygon cut on amplitude ratios in A2/A3 (x) - A1/A3 (y) plane: (x,y) pairs
// (These are taken as is from "./gem.2008.opt,v1.7".)
GM01X1__ AmplitudeRatio [0-7] .1  .0 1.35 .0 1.35 1.5  .1  1.5
GM01Y1__ AmplitudeRatio [0-7] .15 .0 1.31 .0 1.31 1.42 .15 1.42
GM01U1__ AmplitudeRatio [0-7] .1  .0 1.3  .0 1.3  1.26 .1  1.26
GM01V1__ AmplitudeRatio [0-7] .15 .0 1.25 .0 1.25 1.21 .15 1.21

//  Polynomial corrections to be applied to C = (X,Y,U,V) so that its
// amplitude matches that of P=(Y,X,V,U)
// Obtained by fitting measured 2D distributions of the amplitudes of
// "W27/cdr2200[2,5]-85821.raw" with "~compass/detector/saclay/root/GEMPolys.C".
// ( Raw, cf. "RDMon_GM_DEFAUT" in "TEvRDMonitor.cc", amplitudes are 1st fitted,
//  yielding the AmplitudeCorr entries infra. 2UV, 3XY, 4XY,4UV, 5UV, 6XY
//   Then, as a check, the data chunks are re-processed, w/ ammplitudes
//  corrected by these AmplitudeCorr polynomials, yielding the correlations
//  which are listed as commented out entries and found to closely approach
//  (0,1,0) , as expected.)
//GM01X1__ AmplitudeCorr [0-2]     -1.2   1.038    4.49e-05
//GM01Y1__ AmplitudeCorr [0-2]     13.9   0.926   -2.22e-05
//GM01V1__ AmplitudeCorr [0-2]     11.5   1.107   -9.12e-05
//GM01U1__ AmplitudeCorr [0-2]      4.6   0.859    0.000103

// Clustering method: 0=simple clustering (1 hit=1 cluster), 1=full clustering
GM Clustering 1

// Hit time window for MC hits (hit time smeared with 12 ns RMS)
GM       MCHitTime [0-1] -50. 50.

// Debugging for GEM clustering
//GM01X1__ debug level 10
//GM01Y1__ debug level 10
//GM02X1__ debug level 10
//GM02Y1__ debug level 10

GM02X1__ hist level high

//
// MuxMicroMegas options for 2016: 4 active planes 
//
// Thresholds for strip and cluster amplitudes in units of sigma

MX       Threshold [0-1] 3.  5.

// Polygon cut on amplitude ratios in A2/A3 (x) - A1/A3 (y) plane: (x,y) pairs
// (These are taken as is from "./gem.2008.opt,v1.7".)
// MX01U1__ AmplitudeRatio [0-7] .2  .06 1.32 .06 1.32 1.21 .2  1.21

// Cluster size (to be tuned!)
MX01U1       ClusterSize [0-1] 2. 50.
MX01V1       ClusterSize [0-1] 2. 70.
MX02U1       ClusterSize [0-1] 2. 50.
MX02V1       ClusterSize [0-1] 2. 70.
MX03U1       ClusterSize [0-1] 2. 50.
MX03V1       ClusterSize [0-1] 2. 70.
MX04U1       ClusterSize [0-1] 2. 50.
MX04V1       ClusterSize [0-1] 2. 70.

// Clustering method: 0=simple clustering (1 hit=1 cluster), 1=full clustering
MX Clustering 1

// Multiplexed Cluster Cleaning: 1=clean "ghost" clusters, 0=no cleaning (default)
MX doClusterCleaning 1

// Hit time window for MC hits (hit time smeared with 12 ns RMS)
MX       MCHitTime [0-1] -50. 50.

// Debugging for MX clustering
//MX01U1__ debug level 10
//MX01V1__ debug level 10
//MX02U1__ debug level 10
//MX02V1__ debug level 10
//MX03U1__ debug level 10
//MX03V1__ debug level 10
//MX04U1__ debug level 10
//MX04V1__ debug level 10


GM hist level high
MX hist level high

// Misc
TraF	MuWall	[ 0- 9]	1474.5  0.   0.   0     0     0     0.    0.  70.   35.		// MuWall#1: MU1F and central hole MU1H		(TEv::FitSegments)
TraF	MuWall	[10-19]	3820.   0.   0. 120   400   210     0     0.   0    19.		// MuWall#2: MW2S (central hole not specified)	(TSetup::Init,TDisplay::DrawMisc)
TraF	Calo	[ 0- 9]	1267.5  0    0    0     0     0     1.1  -0.9 62.   31.		// HCAL1   : HC1V and central hole HC1K		(TEv::FitSegments)

CsBuildPart	Hist		5	// Calorimeter

// seed for random number generations:
random number engine JamesEngine
random number seed 19990102

// ===========================================================================
//end
