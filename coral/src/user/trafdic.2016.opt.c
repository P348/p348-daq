C2OPT_HEADER
#if 0

// coral options c file for TraFDic on DVCS 2016 data
//
// NOTA.BENE.:c file is to be processed with:
//
//        "gcc -E -P -C -D<defines>"
//
// before being supplied to coral. 

// From "./trafdic.m2012.opt.c,v13860"
// Modified...

#endif
#if !defined C2OPT_COMPUTERFARM
#  define C2OPT_COMPUTERFARM 1
#endif
#if   C2OPT_COMPUTERFARM == 1
// TraFDic on 2016 DVCS data @ CERN
#elif C2OPT_COMPUTERFARM == 2
// TraFDic on 2016 DVCS data @ gridKa.
#elif C2OPT_COMPUTERFARM == 3
// TraFDic on 2016 DVCS data @ Lyon.
#elif C2OPT_COMPUTERFARM == 4
// TraFDic on 2016 DVCS data @ Compass ONLINE.
#else
// TraFDic on 2016 DVCS data.
#endif

#ifdef C2OPT_BARE_FILE
//  ``Bare'' options file to be included in "$CORAL/src/alignment/traf.*.opt"
// files for processing 2016 FIELD ON data for alignment purposes:
//  - It lacks input data specification (this must be entered in the main
//   "traf.*.opt" file)...
//  - ...as well as mDST output, all high level (vertex and the like)
//   reconstruction options, etc..
//  - More generally, this file should not be edited: any modification to the
//   option entries it contains must instead be implemented in the main
//   "traf.*.opt" by overwriting.
#else

// - Restricted to magnets ON data.
// - Default options settings are for mu+.
// - Alternatives in blocks of SPECIAL SETTINGS, commented out by default
//   - For mu-, see: SPECIAL SETTINGS FOR MU-
//   - For pi-, see: SPECIAL SETTINGS FOR PI-

//  This file sums up the main options. Other options are specified elsewhere
// and referenced here by "include <path_to_2ndary_options_file>" statements.
// All options can be entered several times, whether in main or secondary files.
// The last one, in the stream of entries and "include" statements, always wins.

//  For the preparation of a mass production, cf. the dedicated block in fine
// entitled "TEMPLATE FOR MASS PRODUCTION".
//  For any other particular application, this file has to be edited or, better,
// ``included and overwritten''. Cf. the TWiki page:
// "wwwcompass.cern.ch/twiki/bin/view/DataReconstruction/CoralSoftware#Options_files"
#endif

#ifndef C2OPT_BARE_FILE
//                                         ***** PHAST OUTPUT
// - Active only if used on a PHAST-compatible executable, cf. "./README"
// - Cf. "$PHAST/coral/README" for the details of the options
mDST file		phast.root
// - Information reduction bit pattern: "mDST select".
mDST select		9	// At least one vertex or particle | First and last helices only.
// - One may want to output more info, as exemplified infra:
//mDST hits		HL	// MegaDST...
//mDST Digits		EC	// Raw information after calibrations are applied: only useful for non-tracking detectors, else "mDST hits" is good enough.
mDST DAQdigits  	CA FI15	HO04 // Raw information as read from raw data stream, no calibrations applied)
mDST selectTrigger	1024	// Random trigger: unconditional output (Note: the option is not useful when "mDST select 0", i.e. all-out unconditional output.)
mDST AllScalersTriggerMask	1024	// Write out all beam scalers for random trigger

//					// ***** WRITE-BACK of RAW DATA
//				<File name>	<Type>
//CsKalmanFitting WriteBack	evtDump.raw	Physics		// As of 07/01: D*, excl. phi, K0, J/psi
//CsEvent         WriteBack	evtDump.raw	Randoms		// <File name> may differ from CsKalmanFitting's
#endif

//					   ***** HISTOGRAMMING
histograms package	ROOT
histograms home		trafdic.root

#ifdef C2OPT_BARE_FILE
//					   ***** THIS IS A DATA RUN...
//					   ***** ...W/O ANY INPUT SPECIFIED!
#else
//					   ***** THIS IS A DATA RUN
#endif
Data job

BCS run					// ...of the BeamCharge&Spin type
#ifndef C2OPT_BARE_FILE
//					   ***** INPUT DATA...
#  if   C2OPT_COMPUTERFARM == 1
// (Note default input W15#275975 belongs to sup-period P10.)
Data file	/castor/cern.ch/compass/data/2016/raw/W15/cdr11008-275975.raw
#  elif C2OPT_COMPUTERFARM == 2
Data file 	/grid/fzk.de/compass/compass2/data/raw/2016/cdr11008-269055.raw
#  elif C2OPT_COMPUTERFARM == 3
Data file	cchpsscompass:/hpss/in2p3.fr/group/compass/data/2016/raw/cdr11008-273670.raw
#  elif C2OPT_COMPUTERFARM >= 4
Data file	/castor/cern.ch/compass/data/2016/raw/W15/cdr11008-275975.raw
#  endif
#endif


// 					   ***** DETECTOR TABLE...
//  A full directory (path name terminated by a slash "/") or the full path to
// a particular file can be specified.
detector table   	$COMPASS_FILES/geometry/2016/

// 					   ***** ROOTGeometry
// (Note: ROOTG files are available on a per period basis. Infra, the file
// corresponding to the default "Data file" input specified in the "INPUT DATA"
// block above.)
CsGDMLGeometry file	$COMPASS_FILES/geometry/2016/ROOTGeometry/2016P09.plus.root

//					   ***** CALIBRATION
#if C2OPT_COMPUTERFARM > 4
use calibration
CDB use  	FileDB
CDB location  	$COMPASS_FILES/calibrations/2016
#else
use calibration
CDB use 	MySQLDB
#  if   C2OPT_COMPUTERFARM == 1
CDB server	compassvm-userdb01
CDB username	coral
CDB userpasswd	na58coral
#  elif C2OPT_COMPUTERFARM == 2
CDB server compass.gridka.de	// Located at gridKa (internal network)
CDB specialplace GRIDKA 	// Location of calibration files at gridKa
#  elif C2OPT_COMPUTERFARM == 3
CDB server	ccmycompa	// Located at Lyon   (internal network), new MySQL server
CDB portnumber  3317		// Port to be used in place of the std one, new server
CDB specialplace LYON	 	// Location of calibration files at Lyon
#  elif C2OPT_COMPUTERFARM == 4
CDB server	pccodb00
CDB specialplace DAQ	 	// Location of calibration files in ONLINE
#  endif
// "CDB entrytime Y-M-D-H:M:S" can be entered to reject all calibrations issued
// after specified date. CDB being constantly evolving, leaving "CDB entrytime"
// disabled may give rise to unpredictable behaviour.
//CDB	entrytime	2010-12-01-00:00:00
// "entrytime" entry can be made to affect only a (set of) specific TBName(s):
// "TBname(or TB) CDBentrytime(sic!)"
//FI	CDBentrytime 	2010-10-31-23:59:59	// Disregarding the FI calibs entered in 201/11: not yet finalised.
// CDB can be replaced by a File DB, for any given (set of) TBname(s):
#  if   C2OPT_COMPUTERFARM == 1
//DW FileDB	/afs/cern.ch/user/l/leberig/public/marcin/
#  else
//DW FileDB	$COMPASS_FILES/fileDBs/DW/marcin
#  endif
#endif

//					   ***** MAPPING
decoding map	$COMPASS_FILES/maps/2016.xml


//					   ***** EVENT SELECTION
events to read 40000			// # of events to read
//events to skip 7
// BoS Veto:
// - It has to be enforced software-wise to ensure data reco be uniform, if forgotten in hardware. (Although could be argued that this could be done in PHAST.)
// - It's commented out here.
//events BOS_skip	1.151
//selection trigger mask 20f		// Trigger selection (hexadecimal)
//selection trigger strict


// seed for random number generations: is it really used in RD reconstuction?
random number engine	JamesEngine
random number seed	19990102
//reset random seed every new event

DC 	make always two clusters	// Even if they coincide, so that their re-evaluation w/ an event time differing from the trigger might yield 2 distinct clusters
DR 	make always two clusters
DW 	make always two clusters
MB 	make always two clusters
ST 	make always two clusters

pattern  method 1 	// not used yet
tracking method 1 	// not used yet


// 					   ***** DECODING
make decoding 	 	// <nothing>, MCExact

//  	 	 	 	 	   ***** CLUSTERING
make clustering 	// <nothing>, MCExact, MCSmeared, MCQuantized

//					   ***** RECONSTRUCTION SCHEMA
reconstruction schema 1

//					   ***** TRACKING
make tracking
track prepattern method traffic
track bridging   method traffic
track fitting    method traffic

#ifndef C2OPT_BARE_FILE
//					   ***** Beam RECONSTRUCTION
make beam reconstruction
include ${CORAL}/src/pkopt/beam_2016.opt
// ***** Beam PID
PID_doBeamID	selAllBeams	1	// Allow beams w/o BMS in the vertexing

//					   ***** Vertex RECONSTRUCTION
make	vertex	reconstruction
vertex pattern method	averaging
vertex fitting method	kalman
include ${CORAL}/src/pkopt/vertex.2002.opt
//                   Overwrite what's set in "vertex.opt"
CsAverPattern	findSec		1	// 0 - Off, 1 - ON
CsAverPattern	Print [ 0 ]	0	// Prefilter info.
CsAverPattern	Print [ 1 ]	0	// Error info.
// Track is considered as accepted if its momentum is smaller
// than "AcceptTill" percents of beam track
CsAverPattern	AcceptTill	105	// In %
CsAverPattern	TimePrimCut	10 	// Time diff between track and beam, in sigmas
// 	 	 	Z Dist 	DCA(number of sigmas)
CsAverPattern	CUTS	1600	10
CsAverPattern	Refit		0	// 0 - Off, 1 - ON
CsAverPattern	Retrack		0	// 0 - Off, !0 = cut on event time
CsKalmanFitting Print [ 0 ]	0	// Kalman filter info.
CsKalmanFitting Print [ 1 ]	0	// Global fit info.
CsKalmanFitting Print [ 2 ]	0	// IKF info.
CsKalmanFitting RefPlane 	500	// in mm
CsKalmanFitting RefBeam  	-4000	// in mm
CsKalmanFitting RefMargin	500	// in mm
// Chi2 increment cuts:		DirChi2Trk	InvChi2Trk
// (Note: "InvChi2Trk" is discarding tracks w/ large chi2 increment, even when
// overall chi2 is w/in "Chi2VCut". Original setting had been "10.75". Proposed
// here is "20". Could be made even larger (30? 50?). To be tuned, by looking
// at vertex chi2 (in RD as well as MC), and in MC, by looking at fake
// associations (of, e.g., V0 decays and retinteraction products).)
CsKalmanFitting	CUTS		7		20
// Vertex Chi2 cut:
CsKalmanFitting	Chi2VCut	10.5
CsKalmanFitting	Retrack		3	// 0 - Off, !0 = cut on event timeC
CsKalmanFitting BeamP0		160	// Beam reference momentum (for histo purposes)


//					   ***** RICH1
make rich1 reconstruction
include ${CORAL}/src/pkopt/rich1.m2012.opt	

CsRICH1UpGrade  CONFIG		// RICH upgrade: 12 APV 4 MAPMT
RICHONE 	PrintDetMirr	0
RICHONE 	AcknoMethods	0	// prints methods in use
RICHONE 	PrintKeys	0	// 1 = print keys read,  0 = No print
RICHONE 	PrintConsts	0	// 1 = print rec. constants read,  0 = No print
RICHONE 	DoThetaLikeMax 	NO 	// Faster but "the output buffer of rich won't have the angle that maximizes the likelihood anymore" (according to Giulia, cf. mail from 2009/06/29)

//					   ***** CALORIMETERS
make	calorimeters	reconstruction
include ${CORAL}/src/pkopt/calorim_2012.opt
EC01P1__ SADC_DECODE_VERSION 1
EC02P1__ SADC_DECODE_VERSION 1
// Comment next two lines to switch OFF use of LED/Laser calibrations for ECAL1/2 (in that case, also switch off pi0 calib below)
//EC01P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=YES USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=NO
//EC02P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=YES USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=YES
EC01P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=NO USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=NO
EC02P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=NO USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=NO
// Comment next two lines to switch OFF use of pi0 calibrations for ECAL1/2
//EC01P1__ MoreRecoOptions USE_EDEPCORR=_EdepCorr
//EC02P1__ MoreRecoOptions USE_EDEPCORR=_EdepCorr_RecoCombined USE_TISDEPCORR=_TiSdepCorr_RecoCombined
EC01P1__ MoreRecoOptions USE_EDEPCORR=NO
EC02P1__ MoreRecoOptions USE_EDEPCORR=NO USE_TISDEPCORR=NO

//CsBuildPart	Hist		5	// Calo histos. Disabled by default, because too big (several MB). But programmed to be enabled in mass production: cf. in fine.

include ${CORAL}/src/pkopt/ecal0_2016_rd.opt

//					   ***** MU' PID +  FLUX SCALERS
// Earlier periods (P01-03) have X planes involved in the MT trigger decision.
// => Have to there use "m2012":
//include ${CORAL}/src/pkopt/trigger.m2012.opt
// Later periods: no X planes:
include ${CORAL}/src/pkopt/trigger.2016.opt
#else
//					   ***** NO Beam RECONSTRUCTION

//					   ***** NO Vertex RECONSTRUCTION

//					   ***** NO RICH1

//					   ***** NO CALORIMETERS...
// ...still one sillily needs to include the ECAL0 options file (it's probably
// required to instantiate the calorimeter object...)
include ${CORAL}/src/pkopt/ecal0_2016_rd.opt

//					   ***** NO MU' PID...
// ...still one needs the trigger options file, which also contains scalers,
// which description is, for some reason, mandatory...
include ${CORAL}/src/pkopt/trigger.m2012.opt
#endif

//  	 	 	 	 	   ***** GEOMETRICAL ZONES
define	 zone	0	 3500	 before M1
define	 zone	3500	 17000	 between M1 and M2
define	 zone	17000	 32500	 between M2 and Muon Wall
define	 zone	32500	 99999	 after Muon Wall

define	 zone	 -8000	 0	 before the target
define	 reZone	 435	 1460	 reZoning FI03/MP01UV

//  	  	  	  	  	   ***** MAGNETIC FIELD MAPS
CsField SM1m_field_measured $COMPASS_FILES/maps/mag_fields/SM1m/SM1M.map.172.data
CsField SM2_field	 $COMPASS_FILES/maps/mag_fields/SM2/FSM.map.4000.data

//					   ***** MAGNETs INFO
#if C2OPT_COMPUTERFARM <= 4
CsMagInfo	MySQLDB
#else
CsMagInfo	File	~ybedfer/public/magInfo.2016.txt	// ...if MySQL not avail
#endif
CsMagInfo	SM2	1	// Do rescale SM2 w/ NMR (retrieved from mySQLDB) \times correcting factor specified as argument to this "CsMagInfo SM2" option.




//					   ***** LOGGER
error logger log level		error	// debugging, verbose, info, anomaly, warning, error, fatal
error logger store level	none
error logger verbosity		normal	// low, normal, high. 

//	  	  	  	  	   ***** TraFDic (+DET OPTIONS+...)

include ${CORAL}/src/pkopt/trafdic.2016.opt

//		***** OVERWRITE WHAT'S SET IN "../pkopt/trafdic.2016.opt" *****

// SPECIAL (TEMPORARY?) SETTING for ST03
// To be enabled for periods where ST03s keep being inefficient despite improved
// calibration
//TraF	ReMode [55]	1	// Suppress ST03 hit association efficiency	(TAlgo2::FindSpace)
//TraF	iPRpar	[ 10 - 17 ]	3   2   10	3  2  8		2 2

// SPECIAL SETTINGS FOR MU-
//TraF	iCut	[15]	-1	// Beam charge.
// - Beam charge has to be specified to the BMS reconstruction package (whether it is otherwise known to coral or not).
//beam	beam_charge	-1	// Beam charge specified to the BMS reco.

// SPECIAL SETTINGS FOR PI-
// (One would have also to comment out BMS reconstruction.)
//hadron run	// => Matching detectors.dat and dico files w/ ".hadron" extension 
//TraF	iCut	[15]	-1	// Beam charge.
//TraF	iCut	[30]	1	// Beam particle iD: 1=hadron
//TraF	dCut	[ 4]	190	// Beam momentum (GeV).
//TraF	dCut	[ 5]	1.80e-4 // Beam cop spread (c/GeV). Assigned to d(1/p). Here corresponds to the 6.5GeV/sqrt(12) of the 190 Gev beam.
//CsKalmanFitting BeamP0  	190	// Beam reference momentum (for histo purposes)
//CsField SM2_field	$COMPASS_FILES/maps/mag_fields/SM2/FSM.map.5000.data

// SPECIAL SETTINGS for 12-MP SETUP
// Scifi option
TraF	ReMode	[54]	1	// Account for scifi timing in 1st iteration of PR in space	(TAlgo2FindSpace)
// MP option: 
TraF	ReMode	[48]	4	// Suppress pixMPs in space PR		(TEvPrePattern2,TAlgo2FindProjQ/Space)
TraF	iCut	[39]	13	// Min. # of hits in space for preliminary PR in zone 0x1	(TAlgo2FindSpace)
TraF	iCut	[33]	20	// Min. # of hits to enable preliminary PR in zone 0x1		(TEvPrePattern2)
TraF	iCut	[41]	1	// PixMPs disregarded for iteration# >=1	(TEvPrePattern2)
// Temporary (until good alignment along v-axis) MP setting
TraF	dCut	[98]	.2	// Enlarge search road along v-axis (in cm, depending upon v-alignment quality)	(TAlgo2FindSpace)

// SPECIAL SETTING for MPs/MP03XUV
// MPs turn out to have better resolution than the pitch/sqrt(12) used in
// "CsPixeMP" (hit uncertainty) and TraFDic (resolutions defining search roads)
// => Let's redefine resolutions/uncertainties:
// - via rescaling, which allows to act on all pixels/inner/outer regions,
// - .866: yielding inner/average/outer = 100/111/120 um (pitch=400/446/480 um).
TraF	dCut	[106]	-13.4	// Rescaling factor (%) for all but MP03XUV	(TEv::PrePattern2,ImportClusters)
// In 2016: Voltage diff. across conversion gap low => Lorentz effect in 3XUV
// => Poorer resolution
// - 1.3: yielding inner/average/outer = 150/167/180 um
// - Note: UV are /= sqrt(2) => 140/156/168 um
//TraF	dCut	[107]	30	// Rescaling factor (%) for resolutions	(TEv::ImportClusters)
TraF	dCut	[108]	30	// Rescaling factor (%) for uncertainties	(TEv::PrePattern2)

// SPECIAL TEMPORARY SETTING for SIs
// Following commits to coral SVN (r14435) and to MySQLDB by Christian, some
// SI hits end up w/ extremely small uncertainties (few microns), which may
// be either intrinsically too small or too demanding for our present alignment.
// => Let's put a safeguard here:
TraF	dCut	[85]	.0008	// SI position uncertainties: additive correction term (cm)	(TEv::ImportClusters)

// SPECIAL SETTINGS for MF3/HI05 muID RECONSTRUCTION:
// I) Options to make for bad alignment in zone 0x8: ideally they are commented out when good
//  alignment (including Dead Zone) becomes available.
TraF	dCut	[109]	1	// Worsen resolution assigned to GM11. =1: no worsening	(TSetup::Init)
TraF	dCut	[110]	2.5	// Chi2 increment cut on track extension to HI05	(TEv::Foretrack2Hs)
TraF	dCut	[111]	.5	// Hit association efficiency cut on track extension to HI05	(TEv::Foretrack2Hs)
// II) Options to explore the quality of alignment and dead zone alignment in zone 0x8: ideally they
//  should have no impact when good alignment (including Dead Zone) becomes available.
//TraF	dCut	[111]	.25	// Only 25% efficiency!
//TraF	ReMode	[57]	1	// Relax hist association basic requirements	(TEv::Foretrack2Hs)


// Redefine the TBNAMES of DETECTORS to be EXCLUDED FROM PATTERN RECOGNITION:
// - Some detectors may have to be left turned off in any case, refer to "../pkopt/trafdic.2016.opt" to check if any such (nothing as of 2017/12/20)
// - But one may want, or may have, to turn off some more...
// - Also, in coral jobs dedicated to detectors studies, one usually turns off the particular detector under exam. Note that then your redefinition will overwrite all "DetNameOff" lists previously entered: take care to include in it all the detectors that have to be turned off in any case.
//TraF	DetNameOff	nothing as of 2017/12/20...

//		==> ==> IMPLICATIONS:
// DETECTORS being EXCLUDED may imply updating some PATTERN RECOGNITION options:
//TraF	ReMode	[17]	0	// Disable GEM amplitude correlations, if any GEM is among the turned-off detectors.

//TraF	Graph	[0]	0	// coral's graphics switches off automatically under BATCH @ CERN, gridKa and Lyon. This option forces it to do so in any case.
//TraF	iCut	[0]	7	// Trigger selection in TraFDic
//TraF	Print	[0]	3	// Event-by-event numbering
//TraF	Print	[8]	3	// EoE

#ifndef C2OPT_BARE_FILE
//			SMOOTHING POINTS
// The points @ RICH (resp. Calos) are to be used by RICH (resp. Calo) softwares.
TraF	SmoothPos [0]	745.		// RICH
// From: Vladimir Kolosov
// Sent: Thu 2/28/2008 9:14 AM
// Let's decide to calculate track position 10 cm upstream front surface of a calorimeter.
// calo  EC01P1__  1111.500 - 45./2 - 10. = 1111.500 - 32.5 = 1079.0
// calo  EC02P1__  3325.200 - 45./2 - 10. = 3325.200 - 32.5 = 3292.7
// calo  HC01P1__  1267.500 -100./2 - 10. = 1267.500 - 60.0 = 1207.5
// calo  HC02P1__  3576.000 -112./2 - 10. = 3576.000 - 66.0 = 3510.0
// (NOTA BENE: The positions of E/HCAL1 may in fact differ from what is quoted supra and corresponds to the 2006/7 setup.)
TraF	SmoothPos [1]	1079.0	// Smooth track at ECAL1
TraF	SmoothPos [2]	1207.5	// Smooth track at HCAL1
TraF	SmoothPos [3]	3292.7	// Smooth track at ECAL2
TraF	SmoothPos [4]	3510.0	// Smooth track at HCAL2
#endif

//                 STRAW options: X-ray and signal propagation correction
// - X-ray can only be enabled:
//   - if alignment (cf. "detector table" entry supra) did also take X-ray correction into account.
//   - if it's is compatible w/ the "VarPitch" option chosen infra.
// - NOTA BENE: A unique entry for all "STRAW settings".
STRAW settings spacers=NO signal_propagation=YES T0=YES

//                 Enabling variable pitch
//  This can be enabled, for any given detector, only if alignment (cf.
// "detector table" entry supra) takes VarPitch correction into account.
//  And in any case, as far as straws are concerned, is incompatible w/ X-ray
// correction, cf. supra.
//VarPitch    DetOffVarP	DC	ST	DW	FI	GM
//VarPitch	table	~corallib/public/PitchTable/PitchTable_FIGMDCSTDW_2003.dat

#if   C2OPT_FLAG == 1
// ********** SPECIAL SETTING TO HISTOGRAM GEMs Amplitude Correlation **********
// (in "TEv::RDMonitor", which need be recompiled w/ "RDMon_DIGIT_DATA" and
// "RDMon_DIGIT_GM", and, in a first iteration, "RDMon_GM_DEFAULT" defined)
TraF	Hist	[ 1]	17	// Require high level histo in TraFDic
TraF	Hist	[16]	0	// Turn off histo'ing active detectors 
TraF	Hist	[17]	14	// OR of zones histo'ed = 0xe
TraF	Hist	[18]	2	// Tracks used: zones &= 0x2
// (in "TEv::Quadruples", which need be recompiled w/ "Quadruples_HISTO" defined)
TraF	Hist	[ 6]	1	// In PrePattern et al. (FindSpace, Quadruples)
#elif C2OPT_FLAG == 2
// ********** SPECIAL SETTING TO HISTOGRAM RESIDUALS IN HIs AND MBs **********
TraF	Hist	[ 1]	17	// Monitor, &0x10:Residuals
TraF	Hist	[17]	12	// Residuals: |Groups of inactive det's histo'ed
TraF	Hist	[18]	14	// Residuals: &Groups of tracks histo'ed
TraF	DetNameOff	VO	VI	BM	SI03	H	MB
TraF	SmoothPos [1]	2600.	// Smoothing point, to extrapolate from, when in zone 0x4
#elif C2OPT_FLAG == 3 || C2OPT_FLAG == 4
// ********** SPECIAL SETTING TO TRACK OFF-TIME MUONS **********
// ********** AND HISTOGRAM their residuals in FI01:2s and SIs **********
FI HitTime [0-1]	5     20
#  if C2OPT_FLAG == 3
// *****  I) Tracks reconstructed in spectrometer
TraF	Hist	[ 1]	17	// Monitor, &0x10:Residuals
TraF	Hist	[16]	16	// Residuals: |Groups of det's  histo'ed
TraF	Hist	[17]	0	// Residuals: |Groups of inactive det's histo'ed
TraF	Hist	[18]	7	// Residuals: &Groups of tracks histo'ed
TraF	iPRpar [  0 -  5 ]	2   3   6	2  3  6
TraF	iPRpar [ 10 - 15 ]	2   3   5	2  3  5
TraF	iPRpar [ 20 - 25 ]	2   2   4	2  2  4
TraF	iPRpar [ 30 - 35 ]	3   1   9	3  1  7
TraF	iPRpar [ 40 - 45 ]	2   4  11	3  2 10		// W/out FI15
TraF	DetNameOff	V	M	D	ST	P	H	GM01	GM02	GM03	GM04	GM05	GM06
#  elif C2OPT_FLAG == 4
// ***** II) Tracks reconstructed in both spectro and scifi/Si telescope,...
TraF	ReMode	[26]	1	// != 0 - Bridging over target
TraF	Hist	[ 1]	17	// Monitor, &0x10:Residuals
TraF	Hist	[16]	16	// Residuals: |Groups of det's  histo'ed
TraF	Hist	[17]	0	// Residuals: |Groups of inactive det's histo'ed
TraF	Hist	[18]	23	// Residuals: &Groups of tracks histo'ed
TraF	DetNameOff	V	M	D	ST	P	H	GM01	GM02	GM03	GM04	GM05	GM06	SI03
TraF	Det2Go2Fit	FI02	SI02X	SI02Y	FI03	FI04	FI05	FI06	FI07	FI08	GM
#  endif
#endif

//  ===== TEMPLATE FOR MASS PRODUCTION (uncomment, and modify if needed) =====

//					***** PLACEHOLDERS for I/O
// (Note: One has to uncomment next 6 entries.)
//events to read 200000
//Data		file	TMPRAWFILE
//mDST		file	TMPMDSTFILE
//histograms	home	TMPHISTFILE
//CsKalmanFitting WriteBack	testevtdump.raw	Physics
//TMPRICHFILE

//					***** mDST OPTIONS
// (Note: One has to systematically uncomment next 3 entries. For special MegaDST production, one has to uncomment also the 4th one.)
//mDST	select	0	// 0: no selection
//mDST	Digits	        VI VO
//mDST	DAQdigits	CA FI12 FI13 FI01 FI02 HO04 VI VO

//mDST	hits	ALL		// SPECIAL OPTION! for the production of MegaDST


//					***** CDB OPTIONS
// (Note: Mass production resorts to several different MySQL servers. One of them is selected at execution time via the environment variable $CDBSERVER. => One has to uncomment to following 2 entries to enable this scheme.)
//CDB server	$CDBSERVER           // Share load between compass02 and compass22
//CDB username	anonbatch
// (Note: One has to freeze the CDB info accessible to the production, so that the latter be independent of CDB later developments, and hence stable. A reasonable choice for the "entrytime" is the date of the start of production. One may still want to be more restrictive on all of the CDB. Or on the part of it concerning a particular detector or set of detectors: in that case, use the option "<generic name> CDBentrytime" (cf. examples in the body of this options file).)
//CDB	entrytime	2017-03-18-12:00:00	// TO BE MODIFIED!


//					***** DETECTOR MONITORING
// (Note: Have to uncomment next 4 entries.)
//Monitoring	Directory	ON	// Paolo's data monitor graphs.
//Monitoring	Residuals	ON	// Albert's monitoring histos.
//Monitoring	Efficiency	ON
//CsBuildPart	Hist		5	// Calorimeter


//					***** SETUP FILES
// (Note: One has to freeze the setup info accessible to the production, so that the latter be independent of the modifications later brought to the setup files, and hence stable. This is achieved by copying the setup files to sub-directories of the production directory.) 
//decoding	map	/cvmfs/compass.cern.ch/generalprod/testcoral/dvcs2016P10t4/maps
//detector	table	/cvmfs/compass.cern.ch/generalprod/testcoral/dvcs2016P10t4/alignement/
//TraF		Dicofit	/cvmfs/compass.cern.ch/generalprod/testcoral/dvcs2016P10t4/dico/
//CsROOTGeometry file	/cvmfs/compass.cern.ch/generalprod/testcoral/dvcs2016P10t4/ROOTGeometry/detectors.dvcs.r446.C

//					***** MAGINFO
// (Note: MagInfo is missing for some of the 2016 runs. When it's the case, let's enable the "CsMagInfo File" option. One has to care to compile the appropriate file, based on file output by "$CORAL/scripts/magInfo.pl", patching it to provide the missing info, see "~/public/2017/magInfo.278251.278349.txt" as an example.)
//CsMagInfo	MySQLDB	0
//CsMagInfo	File	/cvmfs/compass.cern.ch/generalprod/testcoral/dvcs2016P10t4/magFile.275478-275908.txt


//					***** BEAM CHARGE (mu- instead of mu+)
// (Note: One has to uncomment the following 2 entries for mu- (instead of mu+) data. For pi- data, there are some other options to enable, see "SPECIAL SETTINGS FOR PI-" supra.)
//TraF	iCut	[15]	-1	// Beam charge.
// - Beam charge has to be specified to the BMS reconstruction package (whether it is otherwise known to coral or not).
//beam	beam_charge	-1	// Beam charge specified to the BMS reco.


//					***** DETECTORS EXCLUDED FROM TRACKING
// (Note: One has to uncomment the following entry depending upon the data taking period one is about to produce. It allows to exclude some detectors from the track reconstruction, had they been failing during a, large, part (then production gets more stable) or all (tracking performs better if it is told not to expect hits from a particular detector) of the period. (Caveat: one has to supply the full list of excluded detectors each time one re-specifies this "TraF DetNameOff" option.))
//					***** UPDATE PATTERN RECOGNITION options
// (Note: Excluding detectors may imply updating some pattern recognition options.)
// ===========================================================================
//end
