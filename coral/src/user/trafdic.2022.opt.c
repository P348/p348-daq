C2OPT_HEADER
#if 0

// coral options c file for TraFDic on 2022 (transversity) data
//
// NOTA.BENE.:c file is to be processed with:
//
//        "gcc -E -P -C -D<defines>"
//
// before being supplied to coral. 

// Derived from "./trafdic.2021.opt.c"

#endif
#if !defined C2OPT_COMPUTERFARM
#  define C2OPT_COMPUTERFARM 1
#endif
#if   C2OPT_COMPUTERFARM == 1
// TraFDic on 2022 Transv data @ CERN.
#elif C2OPT_COMPUTERFARM == 2
// TraFDic on 2022 Transv data @ gridKa.
#elif C2OPT_COMPUTERFARM == 3
// TraFDic on 2022 Transv data @ Lyon.
#elif C2OPT_COMPUTERFARM == 4
// TraFDic on 2022 Transv data @ Compass ONLINE.
#else
// TraFDic on 2022 Transv data.
#endif

#ifdef C2OPT_BARE_FILE
//  ``Bare'' options file to be included in "$CORAL/src/alignment/traf.*.opt"
// files for processing 2022 FIELD ON data for alignment purposes:
//  - It lacks input data specification (this must be entered in the main
//   "traf.*.opt" file)...
//  - ...as well as mDST output, all high level (vertex and the like)
//   reconstruction options, etc..
//  - More generally, this file should not be edited: any modification to the
//   option entries it contains must instead be implemented in the main
//   "traf.*.opt" by overwriting.
#else
//  This file sums up the main options. Other options are specified elsewhere
// and referenced here by "include <path_to_2ndary_options_file>" statements.
// All options can be entered several times, whether in main or secondary files.
// The last one, in the stream of entries and "include" statements, always wins.

//  For the preparation of a mass production, cf. the dedicated block in fine
// entitled "TEMPLATE FOR MASS PRODUCTION".
//  For any other particular application, this file has to be edited or, better,
// ``included and overwritten''. Cf. the TWiki page:
// "wwwcompass.cern.ch/twiki/bin/view/DataReconstruction/CoralSoftware#Options_files"
#endif

#ifndef C2OPT_BARE_FILE
//                                         ***** PHAST OUTPUT
// - Active only if used on a PHAST-compatible executable, cf. "./README"
// - Cf. "$PHAST/coral/README" for the details of the options
mDST file		phast.root
// - Information reduction bit pattern: "mDST select".
mDST select		0	// No selection
mDST hits               HG HO HM HL DC05//save hits from hodoscopes
// - One may want to output more info, as exemplified infra:
//mDST hits		ALL	// MegaDST...
//mDST Digits		EC	// Raw information after calibrations are applied: only useful for non-tracking detectors, else "mDST hits" is good enough.
//mDST DAQdigits  	CA FI15	HO04 	// Raw information as read from raw data stream, no calibrations applied)
//mDST	selectTrigger	1152	// beamT and RandomT: unconditional output (Note: the option is not useful when "mDST select 0", i.e. all-out unconditional output.)
mDST AllScalersTriggerMask	1024	// Write out all beam scalers for random trigger

//					// ***** WRITE-BACK of RAW DATA
//				<File name>	<Type>
//CsKalmanFitting WriteBack	evtDump.raw	Physics		// D*, excl. phi, K0, J/psi
//CsEvent         WriteBack	evtDump.raw	Randoms		// All 0xc00 trigger. <File name> may differ from CsKalmanFitting's
#endif

//					   ***** HISTOGRAMMING
histograms package	ROOT
histograms home		trafdic.root

#ifdef C2OPT_BARE_FILE
//					   ***** THIS IS A DATA RUN...
//					   ***** ...W/O ANY INPUT SPECIFIED!
#else
//					   ***** THIS IS A DATA RUN
#endif
Data job

#ifndef C2OPT_BARE_FILE
//					   ***** INPUT DATA...
#  if   C2OPT_COMPUTERFARM == 1
Data file 	/eos/ctapublicdisk/archive/compass/data/2022/raw/W02/cdr11005-295547.raw
#  elif C2OPT_COMPUTERFARM == 2
Data file 	/grid/fzk.de/compass/compass2/data/raw/ //To be completed
#  elif C2OPT_COMPUTERFARM == 3
Data file	/hpss/in2p3.fr/group/compass/data/2021/raw/ //To be completed
#  elif C2OPT_COMPUTERFARM >= 4
Data file       /eos/ctapublicdisk/archive/compass/data/2022/raw/tmp/cdr11004-294625.raw
#  endif
#endif


// 					   ***** DETECTOR TABLE...
//  A full directory (path name terminated by a slash "/") or the full path to
// a particular file can be specified.
detector table  	$COMPASS_FILES/geometry/2022/

// 					   ***** ROOTGeometry
// (Note:
// - ROOTG files are available on a per period basis. Infra, the file 
// corresponding to the default "Data file" input  specified in the "INPUT DATA" block above.)
CsGDMLGeometry file     $COMPASS_FILES/geometry/2022/ROOTGeometry/geom.294553.root

//CsROOTGeometry massDefault	.105658367 Not present in 
CsROOTGeometry simpleELoss	0
CsROOTGeometry ELossStraggling	0

//					   ***** CALIBRATION
#if C2OPT_COMPUTERFARM > 4
use calibration
CDB use  	FileDB
CDB location  	$COMPASS_FILES/calibrations/2021
#else
use calibration
CDB use 	MySQLDB
#  if   C2OPT_COMPUTERFARM == 1
CDB server	compassvm-userdb01
CDB username	coral
CDB userpasswd	na58coral
#  elif C2OPT_COMPUTERFARM == 2
CDB server compass.gridka.de	// Located at gridKa (internal network)
CDB specialplace GRIDKA 	// Location of calibration files at gridKa
#  elif C2OPT_COMPUTERFARM == 3
CDB server	ccmycompa	// Located at Lyon   (internal network), new MySQL server
CDB portnumber  3317		// Port to be used in place of the std one, new server
CDB specialplace LYON	 	// Location of calibration files at Lyon
#  elif C2OPT_COMPUTERFARM == 4
CDB server	pccodb00
CDB specialplace DAQ	 	// Location of calibration files in ONLINE
#  endif
// "CDB entrytime Y-M-D-H:M:S" can be entered to reject all calibrations issued
// after specified date. CDB being constantly evolving, leaving "CDB entrytime"
// disabled may give rise to unpredictable behaviour.
//CDB	entrytime	2010-12-01-00:00:00
// "entrytime" entry can be made to affect only a (set of) specific TBName(s):
// "TBname(or TB) CDBentrytime(sic!)"
//FI	CDBentrytime 	2010-10-31-23:59:59	// Disregarding the FI calibs entered in 201/11: not yet finalised.
// CDB can be replaced by a File DB, for any given (set of) TBname(s):
#  if   C2OPT_COMPUTERFARM == 1
//DW FileDB	/afs/cern.ch/user/l/leberig/public/marcin/
#  else
//DW FileDB	$COMPASS_FILES/fileDBs/DW/marcin
#  endif
#endif

//					   ***** MAPPING
decoding map	$COMPASS_FILES/maps/2022.xml

//					   ***** EVENT SELECTION
events to read 500000			// # of events to read
//events to skip 7
// BoS Veto:
// - It has to be enforced software-wise to ensure data reco be uniform, if forgotten in hardware. (Although could be argued that this could be done in PHAST.)
// - It's commented out here.
//events BOS_skip	1.151
//selection trigger mask 20f		// Trigger selection (hexadecimal)
//selection trigger strict


// seed for random number generations: is it really used in RD reconstuction?
random number engine	JamesEngine
random number seed	19990102
//reset random seed every new event

DC 	make always two clusters	// Even if they coincide, so that their re-evaluation w/ an event time differing from the trigger might yield 2 distinct clusters
DR 	make always two clusters
DW 	make always two clusters
MB 	make always two clusters
ST 	make always two clusters

pattern  method 1 	// not used yet
tracking method 1 	// not used yet


// 					   ***** DECODING
make decoding 	 	// <nothing>, MCExact

//  	 	 	 	 	   ***** CLUSTERING
make clustering 	// <nothing>, MCExact, MCSmeared, MCQuantized

//					   ***** RECONSTRUCTION SCHEMA
reconstruction schema 1

//					   ***** TRACKING
make tracking
track prepattern method traffic
track bridging   method traffic
track fitting    method traffic

#ifndef C2OPT_BARE_FILE
//					   ***** Beam RECONSTRUCTION
make beam reconstruction
include ${CORAL}/src/pkopt/beam_2016.opt
// ***** Beam PID
PID_doBeamID	selAllBeams	1	// Allow beams w/o BMS in the vertexing

//					   ***** Vertex RECONSTRUCTION
make	vertex	reconstruction
vertex pattern method	averaging
vertex fitting method	kalman
include ${CORAL}/src/pkopt/vertex.2002.opt
//                   Overwrite what's set in "vertex.opt"
CsAverPattern	findSec		1	// 0 - Off, 1 - ON
CsAverPattern	Print [ 0 ]	0	// Prefilter info.
CsAverPattern	Print [ 1 ]	0	// Error info.
// Track is considered as accepted if its momentum is smaller
// than "AcceptTill" percents of beam track
CsAverPattern	AcceptTill	105	// In %
CsAverPattern	TimePrimCut	10 	// Time diff between track and beam, in sigmas
// 	 	 	Z Dist 	DCA(number of sigmas)
CsAverPattern	CUTS	1600	10
CsAverPattern	Refit		0	// 0 - Off, 1 - ON
CsAverPattern	Retrack		0	// 0 - Off, !0 = cut on event time
CsKalmanFitting Print [ 0 ]	0	// Kalman filter info.
CsKalmanFitting Print [ 1 ]	0	// Global fit info.
CsKalmanFitting Print [ 2 ]	0	// IKF info.
CsKalmanFitting RefPlane 	500	// in mm
CsKalmanFitting RefBeam  	-4000	// in mm
CsKalmanFitting RefMargin	500	// in mm
// Chi2 increment cuts:		DirChi2Trk	InvChi2Trk
// (Note: "InvChi2Trk" is discarding tracks w/ large chi2 increment, even when
// overall chi2 is w/in "Chi2VCut". Original setting had been "10.75". Proposed
// here is "20". Could be made even larger (30? 50?). To be tuned, by looking
// at vertex chi2 (in RD as well as MC), and in MC, by looking at fake
// associations (of, e.g., V0 decays and retinteraction products).)
CsKalmanFitting	CUTS		7		20
// Vertex Chi2 cut:
CsKalmanFitting	Chi2VCut	10.5
CsKalmanFitting	Retrack		3	// 0 - Off, !0 = cut on event timeC
CsKalmanFitting BeamP0		160	// Beam reference momentum (for histo purposes)


//					   ***** RICH1
make rich1 reconstruction
include ${CORAL}/src/pkopt/rich1.m2012.opt
// RICH upgrade: 12 APV 4 MAPMT
// (Note: not sure this is need for RD, as opposed to MC)
CsRICH1UpGrade	CONFIG
RICHONE 	PrintDetMirr	0
RICHONE 	AcknoMethods	0	// prints methods in use
RICHONE 	PrintKeys	0	// 1 = print keys read,  0 = No print
RICHONE 	PrintConsts	0	// 1 = print rec. constants read,  0 = No print
// Option for faster processing: but "the output buffer of rich won't have the angle that maximizes the likelihood anymore" (according to Giulia, cf. mail from 2009/06/29)
RICHONE 	DoThetaLikeMax	NO

//					   ***** CALORIMETERS
make	calorimeters	reconstruction
include ${CORAL}/src/pkopt/calorim_2012.opt
EC01P1__ SADC_DECODE_VERSION 1
EC02P1__ SADC_DECODE_VERSION 1
EC01P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=NO USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=NO
EC02P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=NO USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=NO
// Comment next two lines to switch OFF use of LED/Laser calibrations for ECAL1/2 (in that case, also switch off pi0 calib below)
//EC01P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=YES USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=NO
//EC02P1__ MoreRecoOptions USE_LED_REF_CALIB_CORRECTION=YES USE_LED_REF_CALIB_CORRECTION_IN_SPILLS=YES

EC01P1__ MoreRecoOptions USE_EDEPCORR=NO
EC02P1__ MoreRecoOptions USE_EDEPCORR=NO USE_TISDEPCORR=NO
// Comment next two lines to switch OFF use of pi0 calibrations for ECAL1/2
//EC01P1__ MoreRecoOptions USE_EDEPCORR=_EdepCorr
//EC02P1__ MoreRecoOptions USE_EDEPCORR=_EdepCorr_RecoCombined USE_TISDEPCORR=_TiSdepCorr_RecoCombined


//					   ***** MU' PID +  FLUX SCALERS
include ${CORAL}/src/pkopt/trigger.2016.opt

// probably different flux scalers than in trigger.2016.opt (errors otherwise):
FluxScalers SCFI01X1 SCFI01X2 SCFI01X3 SCFI01Y1 SCFI01Y2 SCFI01Y3 SCFI15X1 SCFI15X2 SCFI15Y1 SCFI15Y2

#else
//					   ***** NO Beam RECONSTRUCTION

//					   ***** NO Vertex RECONSTRUCTION

//					   ***** NO RICH1

//					   ***** NO CALORIMETERS...

//					   ***** NO MU' PID...
// ...still one needs the trigger options file, which also contains scalers,
// which description is, for some reason, mandatory...
include ${CORAL}/src/pkopt/trigger.2016.opt
// probably different flux scalers than in trigger.2016.opt (errors otherwise):
FluxScalers SCFI01X1 SCFI01X2 SCFI01X3 SCFI01Y1 SCFI01Y2 SCFI01Y3 SCFI15X1 SCFI15X2 SCFI15Y1 SCFI15Y2
#endif

//  	 	 	 	 	   ***** RECONSTRUCTION ZONES
define	 zone	    0	  3500	 before M1
define	 zone	 3500	 17000	 between M1 and M2
define	 zone	17000	 32500	 between M2 and Muon Wall
define	 zone	32500	 99999	 after Muon Wall

define	 zone	-8000	     0	 before the target
//define	 reZone	435     1460   reZoning FI03/MP01UV, preliminarily commented out because seems not to work when dipole field is ON

//  	  	  	  	  	   ***** MAGNETIC FIELD MAPS
//CsField SOL_field	 $COMPASS_FILES/maps/mag_fields/SOL/SOL_map_fabrice.dat
CsField SOL_field	 $COMPASS_FILES/maps/mag_fields/SOL/OD_dipole.fieldmap
CsField SM1m_field_measured $COMPASS_FILES/maps/mag_fields/SM1m/SM1M.map.172.data
CsField SM2_field	 $COMPASS_FILES/maps/mag_fields/SM2/FSM.map.4000.data

//					   ***** MAGNETs INFO
#if C2OPT_COMPUTERFARM <= 4
CsMagInfo	MySQLDB
#else
//CsMagInfo	File	~ybedfer/public/magInfo.2022.txt	// ...if MySQL not avail
#endif
CsMagInfo	SM2	1



//					   ***** LOGGER
error logger log level		error	// debugging, verbose, info, anomaly, warning, error, fatal
error logger store level	none
error logger verbosity		normal	// low, normal, high. 

//	  	  	  	  	   ***** TraFDic (+DET OPTIONS+...)

include ${CORAL}/src/pkopt/trafdic.2021.opt

//		***** OVERWRITE WHAT'S SET IN "../pkopt/trafdic.2016.opt" *****

TraF Dicofit    $COMPASS_FILES/geometry/2022/dico/

// SPECIAL SETTINGS for 12-MP SETUP
// Scifi option
TraF	ReMode	[54]	1	// Account for scifi timing in 1st iteration of PR in space	(TAlgo2FindSpace)
// MP option: 
TraF	ReMode	[48]	4	// Suppress pixMPs in space PR		(TEvPrePattern2,TAlgo2FindProjQ/Space)
TraF	iCut	[39]	13	// Min. # of hits in space for preliminary PR in zone 0x1	(TAlgo2FindSpace)
TraF	iCut	[33]	20	// Min. # of hits to enable preliminary PR in zone 0x1		(TEvPrePattern2)
TraF	iCut	[41]	1	// PixMPs disregarded for iteration# >=1	(TEvPrePattern2)
// Temporary (until good alignment along v-axis) MP setting
TraF	dCut	[98]	.2	// Enlarge search road along v-axis (in cm, depending upon v-alignment quality)	(TAlgo2FindSpace)

// SPECIAL SETTING for MPs/MP03XUV
// MPs turn out to have better resolution than the pitch/sqrt(12) used in
// "CsPixeMP" (hit uncertainty) and TraFDic (resolutions defining search roads)
// => Let's redefine resolutions/uncertainties:
// - via rescaling, which allows to act on all pixels/inner/outer regions,
// - .866: yielding inner/average/outer = 100/111/120 um (pitch=400/446/480 um).
TraF	dCut	[106]	-13.4	// Rescaling factor (%) for all but MP03XUV	(TEv::PrePattern2,ImportClusters)
// In 2016: Voltage diff. across conversion gap low => Lorentz effect in 3XUV
// => Poorer resolution
// - 1.3: yielding inner/average/outer = 150/167/180 um
// - Note: UV are /= sqrt(2) => 140/156/168 um
//TraF	dCut	[107]	30	// Rescaling factor (%) for resolutions	(TEv::ImportClusters)
TraF	dCut	[108]	30	// Rescaling factor (%) for uncertainties	(TEv::PrePattern2)

// SPECIAL TEMPORARY SETTING for SIs
// Following commits to coral SVN (r14435) and to MySQLDB by Christian, some
// SI hits end up w/ extremely small uncertainties (few microns), which may
// be either intrinsically too small or too demanding for our present alignment.
// => Let's put a safeguard here:
TraF	dCut	[85]	.0008	// SI position uncertainties: additive correction term (cm)	(TEv::ImportClusters)

// SPECIAL SETTINGS for MF3/HI05 muID RECONSTRUCTION:
// I) Options to make for bad alignment in zone 0x8: ideally they are commented out when good
//  alignment (including Dead Zone) becomes available.
TraF	dCut	[109]	1	// Worsen resolution assigned to GM11. =1: no worsening	(TSetup::Init)
TraF	dCut	[110]	2.5	// Chi2 increment cut on track extension to HI05	(TEv::Foretrack2Hs)
TraF	dCut	[111]	.5	// Hit association efficiency cut on track extension to HI05	(TEv::Foretrack2Hs)
// II) Options to explore the quality of alignment and dead zone alignment in zone 0x8: ideally they
//  should have no impact when good alignment (including Dead Zone) becomes available.
//TraF	dCut	[111]	.25	// Only 25% efficiency!
//TraF	ReMode	[57]	1	// Relax hist association basic requirements	(TEv::Foretrack2Hs)


// Redefine the TBNAMES of DETECTORS to be EXCLUDED FROM PATTERN RECOGNITION:
// - One may want, or may have, to turn off some detectors...
// - Also, in coral jobs dedicated to detectors studies, one usually turns off the particular detector under exam. Note that then your redefinition will overwrite all "DetNameOff" lists previously entered: take care to include in it all the detectors that have to be turned off in any case.
// - Last, take note that "TraF" options do not understand '_' underscores.
TraF	DetNameOff	DW03V2	DW02X2	//PA05	//For debugging but not really efficient yet
LR	DetNameOff	DW03V2__	DW02X2__

//		==> ==> IMPLICATIONS:
// DETECTORS being EXCLUDED may imply updating some PATTERN RECOGNITION options:
//TraF	ReMode	[17]	0	// Disable GEM amplitude correlations, if any GEM is among the turned-off detectors.

//TraF	Graph	[0]	0	// coral's graphics switches off automatically under BATCH @ CERN, gridKa and Lyon. This option forces it to do so in any case.
//TraF	iCut	[0]	7	// Trigger selection in TraFDic
//TraF	Print	[0]	3	// Event-by-event numbering
//TraF	Print	[8]	3	// EoE

#ifndef C2OPT_BARE_FILE
//			SMOOTHING POINTS
// - Point @ RICH is to be used by RICH software.
// - Points at hodos to be used in µ'ID, see "PaTrack::PointsHodoscopes".
TraF	SmoothPos [0]	584.7	// Smooth track at HG01
TraF	SmoothPos [1]	745.	// RICH
TraF	SmoothPos [2]	1079.0	// Smooth track at ECAL1
TraF	SmoothPos [3]	1207.5	// Smooth track at HCAL1
TraF	SmoothPos [4]	1613.7	// Smooth track at HG02
TraF	SmoothPos [5]	2148.0	// Smooth track at HO03
TraF	SmoothPos [6]	3292.7	// Smooth track at ECAL2
TraF	SmoothPos [7]	3510.0	// Smooth track at HCAL2
TraF	SmoothPos [8]	3974.15	// Smooth track at HO04
TraF	SmoothPos [9]	4023.00	// Smooth track at HM04
TraF	SmoothPos [10]	4784.40	// Smooth track at HM05
#endif

//                 STRAW options: X-ray and signal propagation correction
// - X-ray can only be enabled:
//   - if alignment (cf. "detector table" entry supra) did also take X-ray correction into account.
//   - if it's is compatible w/ the "VarPitch" option chosen infra.
// - NOTA BENE: A unique entry for all "STRAW settings".
STRAW settings spacers=NO signal_propagation=YES T0=YES


#if   C2OPT_FLAG == 1
// **********  SPECIAL SETTING TO HISTOGRAM GEMs Amplitude Correlation **********
// (in "TEv::RDMonitor", which need be recompiled w/ "RDMon_DIGIT_DATA" and
// "RDMon_DIGIT_GM", and, in a first iteration, "RDMon_GM_DEFAULT" defined)
TraF	Hist	[ 1]	17	// Require high level histo in TraFDic
TraF	Hist	[16]	0	// Turn off histo'ing active detectors 
TraF	Hist	[17]	14	// OR of zones histo'ed = 0xe
TraF	Hist	[18]	2	// Tracks used: zones &= 0x2
// (in "TEv::Quadruples", which need be recompiled w/ "Quadruples_HISTO" defined)
TraF	Hist	[ 6]	1	// In PrePattern et al. (FindSpace, Quadruples)
#elif C2OPT_FLAG == 2
// ********** SPECIAL SETTING TO HISTOGRAM RESIDUALS IN HIs AND MBs **********
TraF	Hist	[ 1]	17	// Monitor, &0x10:Residuals
TraF	Hist	[17]	12	// Residuals: |Groups of inactive det's histo'ed
TraF	Hist	[18]	14	// Residuals: &Groups of tracks histo'ed
TraF	DetNameOff	VO   VI  SI03	H	MB
TraF	SmoothPos [1]	2600.	// Smoothing point, to extrapolate from, when in zone 0x4
#elif C2OPT_FLAG == 3 || C2OPT_FLAG == 4
// ********** SPECIAL SETTING TO TRACK OFF-TIME MUONS **********
// ********** AND HISTOGRAM their residuals in FI01:2s and SIs **********
//   I) Tracks reconstructed in spectrometer
//  II) Tracks reconstructed in both spectro and scifi/Si telescope,...
//          ...bridged over target, refit with the sole FI02s and SI01XYs.
FI01X1__ HitTime [0-1] 50 200
FI01Y1__ HitTime [0-1] 50 200
FI02X1__ HitTime [0-1] 50 200
FI02Y1__ HitTime [0-1] 50 200
FI03X1__ HitTime [0-1] 50 200
FI03Y1__ HitTime [0-1] 50 200
FI03U1__ HitTime [0-1] 50 200
FI04X1__ HitTime [0-1] 50 200
FI04Y1__ HitTime [0-1] 50 200
FI04U1__ HitTime [0-1] 50 200
FI05X1__ HitTime [0-1] 100 400
FI05Y1__ HitTime [0-1] 100 400
FI06X1__ HitTime [0-1] 100 400
FI06Y1__ HitTime [0-1] 100 400
FI06V1__ HitTime [0-1] 100 400
FI07X1__ HitTime [0-1] 100 400
FI07Y1__ HitTime [0-1] 100 400
FI08X1__ HitTime [0-1] 100 400
FI08Y1__ HitTime [0-1] 100 400
#  if C2OPT_FLAG == 3
// *****  I) Tracks reconstructed in spectrometer
TraF	Hist	[ 1]	17	// Monitor, &0x10:Residuals
TraF	Hist	[16]	16	// Residuals: |Groups of det's  histo'ed
TraF	Hist	[17]	0	// Residuals: |Groups of inactive det's histo'ed
TraF	Hist	[18]	7	// Residuals: &Groups of tracks histo'ed
TraF	iPRpar [  0 -  5 ]	2   3   6	2  3  6
TraF	iPRpar [ 10 - 15 ]	2   3   5	2  3  5
TraF	iPRpar [ 20 - 25 ]	2   2   4	2  2  4
TraF	iPRpar [ 30 - 35 ]	3   1   9	3  1  7
TraF	iPRpar [ 40 - 45 ]	2   4  11	3  2 10		// W/out FI15
TraF	DetNameOff	V   M	D	ST	P	H	GM01	GM02	GM03	GM04	GM05	GM06
#  elif C2OPT_FLAG == 4
// ***** II) Tracks reconstructed in both spectro and scifi/Si telescope,...
TraF	ReMode	[26]	1	// != 0 - Bridging over target
TraF	Hist	[ 1]	17	// Monitor, &0x10:Residuals
TraF	Hist	[16]	16	// Residuals: |Groups of det's  histo'ed
TraF	Hist	[17]	0	// Residuals: |Groups of inactive det's histo'ed
TraF	Hist	[18]	23	// Residuals: &Groups of tracks histo'ed
TraF	DetNameOff	M	D	ST	P	H	GM01	GM02	GM03	GM04	GM05	GM06	SI03
TraF	Det2Go2Fit	FI02	SI02X	SI02Y	FI03	FI04	FI05	FI06	FI07	FI08	GM
#  endif
#endif

//  ===== TEMPLATE FOR MASS PRODUCTION (uncomment, and modify if needed) =====

//					***** PLACEHOLDERS for I/O
// (Note: One has to uncomment next 7 entries.)
//events to read 500000
//Data		file	TMPRAWFILE
//mDST		file	TMPMDSTFILE
//histograms	home	TMPHISTFILE
//CsEvent         WriteBack	testevtdump.raw	Randoms		// All 0x1ffefc00 triggers
//CsKalmanFitting WriteBack	testevtdump.raw	Physics		// D*, excl. phi, K0, J/psi
//TMPRICHFILE

//					***** mDST OPTIONS
// (Note: One has to systematically uncomment next two entries. For special MegaDST production, one has to uncomment also the 3rd one.)
//mDST	select	0	// 0: no selection
//mDST	hits	FI HG HO HM HL 	// Save all scifi hits (for investigating flux measurement, I(Y.B.) guess)

//mDST	hits	ALL		// SPECIAL OPTION! for the production of MegaDST


//					***** CDB OPTIONS
// (Note: Mass production resorts to several different MySQL servers. One of them is selected at execution time via the environment variable $CDBSERVER. => One has to uncomment to following 2 entries to enable this scheme.)
//CDB server	$CDBSERVER           // Share load between compass02 and compass22
//CDB username	anonbatch
//CDB userpasswd   na58coral
//CDB specialplace 
// (Note: One has to freeze the CDB info accessible to the production, so that the latter be independent of CDB later developments, and hence stable. A reasonable choice for the "entrytime" is the date of the start of production. One may still want to be more restrictive on all of the CDB. Or on the part of it concerning a particular detector or set of detectors: in that case, use the option "<generic name> CDBentrytime" (cf. examples in the body of this options file).)
//CDB	entrytime	2021-07-01-00:00:00	// TO BE MODIFIED!


//					***** DETECTOR MONITORING
// (Note: Have to uncomment next 4 entries.)
//Monitoring	Directory	ON	// Paolo's data monitor graphs.
//Monitoring	Residuals	ON	// Albert's monitoring histos.
//Monitoring	Efficiency	ON
//CsBuildPart	Hist		5	// Calorimeter


//					***** SETUP FILES
// (Note: One has to freeze the setup info accessible to the production, so that the latter be independent of the modifications later brought to the setup files, and hence stable. This is achieved by copying the setup files to sub-directories of the production directory.) 
//decoding	map	/cvmfs/compass.cern.ch/generalprod/testcoral/transv2021PXXt1/maps
//detector	table	/cvmfs/compass.cern.ch/generalprod/testcoral/transv2021PXXt1/alignment/
//TraF		Dicofit	/cvmfs/compass.cern.ch/generalprod/testcoral/transv2021PXXt1/dico/
//CsGDMLGeometry file	/cvmfs/compass.cern.ch/generalprod/testcoral/transv2021PXXt1/ROOTGeometry/2021default.root


//					***** MAGINFO
// (Note: MagInfo is missing for some of the 2015 runs (e.g. in W10). When it's the case, let's enable the "CsMagInfo File" option. One has to care to compile the appropriate file, based on file output by "$CORAL/scripts/magInfo.pl", patching it to provide the required info, see "~ybedfer/public/2015/magInfo.261512-261759.txt" as an example.)
//CsMagInfo	MySQLDB	0
//CsMagInfo	File	/cvmfs/compass.cern.ch/generalprod/testcoral/dy2018P03t7TrigEff/MagInfo_dy2018P03t6TrigEff.txt

//					 DETECTORS EXCLUDED FROM TRACKING
// (Note: One has to uncomment the following entry depending upon the data taking period one is about to produce. It allows to exclude some detectors from the track reconstruction, had they been failing during a, large, part (then production gets more stable) or all (tracking performs better if it is told not to expect hits from a particular detector) of the period. (Caveat: one has to supply the full list of excluded detectors each time one re-specifies this "TraF DetNameOff" option.))

//					 UPDATE PATTERN RECOGNITION options
// (Note: Excluding detectors may imply updating some pattern recognition options. In the present case, there's nothing we can do to make for the loss of DC04Y.)


//                                      UNSTABLE CHANNELS LIST
//decoding UnstableChannelsList $PWD/unstable_channels/UnStableChannelsList_P03.txt
// ===========================================================================
//end
