/*!
   \file    CsMicroMegaDetector.h
   \brief   Compass Micro-Mega like detector Class.
   \author  Benigno Gobbo
*/

#ifndef CsMicroMegaDetector_h
#define CsMicroMegaDetector_h

#include "coral_config.h"
#include <CLHEP/Matrix/Matrix.h>
#include <list>
#include <set>
#include "CsDetector.h"
#include "MMLibrary.h"


//#include "CsDecMap.h"
#include "CsHistograms.h"

/*! Leading Edge parity for ChipF1Data.
  1 if leading corresponds to odd data;
  0 if leading corresponds to even data
*/

class CsZone;

/*! \class CsMicroMegaDetector
    \brief Compass Micro-Mega like detector Class.
*/

class CsMicroMegaDetector : public CsDetector {

 public:


  /*! \fn CsMicroMegaDetector( const int row,
    const int id, const char* name, const int unit,
    const int type, const double rdLen, const double xsiz,
    const double ysiz,  const double zsiz, const double xcm,
    const double ycm, const double zcm, const HepMatrix rotDRS,
    const HepMatrix rotWRS, const double wirD, const double ang,
    const int nWir, const double wirP, const double eff, const double bkg,
    const double tGate );
    \brief Constructor for tracking detector types.
    \param row   Detector file raw number
    \param id    Detector identification number
    \param name  Detector name (see Comgeant)
    \param unit  Detector number in station
    \param type  Detector type (see Comgeant)
    \param rdLen Radiation lenght
    \param xsiz  Detector X size (DRS) (mm)
    \param ysiz  Detector Y size (DRS) (mm)
    \param zsiz  Detector Z size (DRS) (mm)
    \param xcm   Detector centre X position (MRS) (mm)
    \param ycm   Detector centre Y position (MRS) (mm)
    \param zcm   Detector centre Z position (MRS) (mm)
    \param rotDRS Rotation matrix of DRS w.r.t MRS
    \param rotWRS Rotation matrix of WRS w.r.t MRS
    \param wirD  1st wire offset (WRS) (mm)
    \param ang   Rotation angle of WRS w.r.t MRS
    \param nWir  Number of wires
    \param wirP  Wires pitch
    \param eff   Detector efficiency
    \param bkg   Detector background
    \param tGate Detector gate
  */
  CsMicroMegaDetector( const int    row,
        const int    id,    const char* name,    const char *TBname,
        const int    unit,  const int    type,
        const double rdLen, const double xsiz,
        const double ysiz,  const double zsiz,
        const double xcm,   const double ycm,
        const double zcm,   const CLHEP::HepMatrix rotDRS,
        const CLHEP::HepMatrix rotWRS,
        const double wirD,  const double ang,
        const int    nWir,  const double wirP,
        const double eff,   const double bkg,
              const double tGate );

  ~CsMicroMegaDetector();

  bool operator==( const CsMicroMegaDetector& ) const; //!< "equal to" operator

  bool operator<( const CsMicroMegaDetector& ) const; //!< "less than" operator

  //! Returns true if TCD informations are available
  inline bool hasTDC() const { return( false ); }

  //! Returns the time resolution (improperly called TDC resolution)
  inline double getTDCResol() const { return tRes_; }

  //! Returns true if Drift informations are available
  inline bool hasDrift() const { return( false ); }

  //! Decode the MC data for this detector
  void makeMCDecoding();

  //! Clusterize the digits
  void clusterize();

  /// Decode raw data
  void  DecodeChipDigit (const CS::Chip::Digit &digit);

  /// Read calibration data.
  std::string  readCalibration(time_t timePoint);

  /// Book histograms
  void BookHistograms();

 private:

  // will be const next year !
  unsigned int parityLead_;

  /// returns true if data is a leading time
  inline bool IsLeading( const unsigned data ) { return ((data & 1) == (unsigned int) parityLead_) ? true : false; }

  /// returns true if data is a trailing time
  inline bool IsTrailing( const unsigned data ) { return ((data & 1) == (unsigned int) parityLead_) ? false : true; }

  bool     decodeCard_;       //!< Decoding set by cards
  bool     useCalib_;         //!< Calibrations ON/OFF flag
  bool     associate_;        //!< Associated clusterisation
  int     splitClustersMin_; //!< Cluster splitting min chan
  int     splitClustersMax_; //!< Cluster splitting max chan
  float    clTMin_, clTMax_;  //!< Cut on Cluster Time (ns)
  float    cltoffMin_, cltoffMax_;  //!< Cuts for off-time clusters (ns)
  float    hitTCut_;          //!< Cut on Hit Time (TDC ticks) (derived from previous)
  float    hitTMin_, hitTMax_;//!< Cut on Hit Time (TDC ticks)
  float    hitTOffMin_, hitTOffMax_;//!< Cut on Hit Time for offtime clusters (TDC ticks)
  float    cresolution_;      //!< Cluster resolution ( by default pitch/sqrt(12) )
  double   tRes_;             //!< Time resolution (default = 9 ns, modified by option)

  // Members brought into play upon macro defined
  float    ToTMin_;           //!< Cut on ToT           (ifdef uM_DETECTOR_STUDIES)
  int      clsMax_;           //!< Cut on cluster size  (ifdef uM_DETECTOR_STUDIES)
  int      nclMax_;           //!< Cut on cluster multi (ifdef uM_SINGLE_TRACK)
  int      channel0_,nWtot_;  //!< Channel # and offset (ifdef uM_PARTIALLY_EQUIPPED)


  std::vector<CsHist1D*>  hists1D_;  //!< 1D histograms
  std::vector<CsHist2D*>  hists2D_;  //!< 2D histograms

  /// 1D histograms
  CsHist1D *hch_, *ht_, *hrates_, *htoff_, *cch_, *cs_, *ct_ , *ctot_, *lt_, *tt_, *crates_, *ctoff_;
  
  /// 2D histograms
  CsHist2D *htvsch_, *ctotvst_;

  int      lastChan_;        //!< channel in last digit
  double   *digitData_;      //!< data for digit creation data[0]=chan, data[1]=time
  std::map<int, std::list<CsDigit*>::iterator >  oDigits_;    //!<digits selected for clustering (ordered)

  
  double W2P(double wire);
  double w2pLow, w2pUp, w2pP1, w2pO1, w2pP2, w2pO2, w2pP3, w2pO3; // W2p parameterisation

  std::vector< MM::ChannelCalibExt >   calib_data_ext;
  bool                                 removeBadChannels_;
  
  MM::ECutTriangle *cutTriangle_;

  static const float leadtWght_;
  static const float f1Tick_;
};


//------------------------------------------------------------------------------


#endif // CsMicroMegaDetector_h



