#
# CsGeom Library
#

file (GLOB SRC "CsDet.cc" "CsDetector.cc" "CsDWDetector.cc" "CsCEDARDetector.cc"
            "CsStrawTubesDetector.cc" "CsStrawTubesDetector-calib.cc" "CsDriftChamberDetector.cc"
            "CsMWPCDetector.cc" "CsMW1Detector.cc" "CsGEMDetector.cc" "CsPixelGEMDetector.cc"
            "CsMicroMegaDetector.cc" "CsMuxMicroMegaDetector.cc" "CsPixelMumegaDetector.cc" "MMLibrary.cc"
            "CsSiTrackerDetector.cc" "CsDFiberHodoDetector.cc" "CsJFiberHodoDetector.cc" "CsTriggerHodoDetector.cc"
            "CsBMSDetector.cc" "CsRICH1Detector.cc" "CsDriftTubeDetector.cc" "CsRichWallDetector.cc" "CsRwRecons.cc"
            "CsRwChargeRecons.cc" "CsMiscDetector.cc" "CsRPDetector.cc" "CsField.cc" "CsZone.cc" "CsGeom.cc"
            "CsMaterialMap.cc" "CsRTRelation.cc" "CsRICH1UpGrade.cc" "CathodeAPV.cc" "CathodeMAPMT.cc" "RayTrace.cc")

add_library(CsGeom ${SRC})

#
# CsGEM Library + Dictionary
#

set  (GEM_LINKDEF "gem/CsGEMPlaneLinkDef.h")
file (GLOB GEM_HEADERS "gem/CsGEM*.h")
list (REMOVE_ITEM GEM_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/gem/CsGEMPlaneLinkDef.h")
ROOT_GENERATE_DICTIONARY(CsGEMPlaneDict ${GEM_HEADERS} LINKDEF ${GEM_LINKDEF})

set  (PIXELGEM_LINKDEF "gem/CsPixelGEMPlaneLinkDef.h")
file (GLOB PIXELGEM_HEADERS "gem/CsPixelGEM*.h")
list (REMOVE_ITEM PIXELGEM_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/gem/CsPixelGEMPlaneLinkDef.h")
ROOT_GENERATE_DICTIONARY(CsPixelGEMPlaneDict ${PIXELGEM_HEADERS} LINKDEF ${PIXELGEM_LINKDEF})

file (GLOB SRC "gem/CsGEM*.cc" "" "gem/CsPixelGEM*.cc" "")
add_library(CsGEM ${SRC} CsPixelGEMPlaneDict CsGEMPlaneDict)

#
# CsMumega
#

set  (MUMEGA_LINKDEF "mumega/CsMumegaPlaneLinkDef.h")
file (GLOB MUMEGA_HEADERS "mumega/CsMumega*.h")
list (REMOVE_ITEM MUMEGA_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/mumega/CsMumegaPlaneLinkDef.h")
ROOT_GENERATE_DICTIONARY(CsMumegaPlaneDict ${MUMEGA_HEADERS} LINKDEF ${MUMEGA_LINKDEF})

set  (PIXELMUMEGA_LINKDEF "mumega/CsPixelMumegaPlaneLinkDef.h")
file (GLOB PIXELMUMEGA_HEADERS "mumega/CsPixelMumega*.h")
list (REMOVE_ITEM PIXELMUMEGA_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/mumega/CsPixelMumegaPlaneLinkDef.h")
ROOT_GENERATE_DICTIONARY(CsPixelMumegaPlaneDict ${PIXELMUMEGA_HEADERS} LINKDEF ${PIXELMUMEGA_LINKDEF})

set  (RECTPIXELMUMEGA_LINKDEF "mumega/CsRectPixelMumegaPlaneLinkDef.h")
file (GLOB RECTPIXELMUMEGA_HEADERS "mumega/CsRectPixelMumega*.h")
list (REMOVE_ITEM RECTPIXELMUMEGA_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/mumega/CsRectPixelMumegaPlaneLinkDef.h")
ROOT_GENERATE_DICTIONARY(CsRectPixelMumegaPlaneDict ${RECTPIXELMUMEGA_HEADERS} LINKDEF ${RECTPIXELMUMEGA_LINKDEF})

file (GLOB SRC "mumega/CsMumega*.cc" "mumega/CsPixelMumega*.cc" "mumega/CsRectPixelMumega*.cc")
add_library(CsMumega ${SRC} CsRectPixelMumegaPlaneDict CsPixelMumegaPlaneDict CsMumegaPlaneDict)

#
# Install headers
#
file (GLOB HEADER "*.h" "gem/*.h" "mumega/*.h")
install(FILES ${HEADER} DESTINATION ./include/)
