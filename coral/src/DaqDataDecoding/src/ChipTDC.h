#ifndef __CHIP_TDC_H__
#define __CHIP_TDC_H__

#include <vector>
#include "Chip.h"

namespace CS {

  class TriggerTimeConfig;

  class ChipTDC : public Chip
  {
  public:
    class Map : public Chip::Map
      {
      public:
      
	Map                     (const ObjectXML &o);

      public:

        /// Print the map definition
        void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

        void            AddToMaps               (Maps &maps,DaqOption &option) const;

        /*! @brief Read trigger time configuration. */
        void            ReadTTConfig            (TriggerTimeConfig &tt_conf) const;
        
        int8            GetMode                 (void) const {return mode;}
        int16           GetPort                 (void) const {return port;}
        
        /// The cost of one time bin
        double          GetTimeUnit             (void) const {return time_unit;}
        void            SetTimeUnit             (double t) {time_unit=t;}
        
        /// Reference time for this chip (in units of time bins)
        double          GetTimeReference        (void) const {return time_reference;}
        void            SetTimeReference        (double t) {time_reference=t;}

        /// Check the map
        virtual void    Check                   (void);

      private:

        int8            mode;        /// 't' for trigger time 'w' for wire time
        int16           port;        	        
        double          time_unit;
	double          time_reference;
	bool            trigger_time_subtraction;
      };
    class DataID
    {
    public:
      DataID(const Chip::DataID &d) {u.data_id=d;}
      DataID(uint16 a,uint16 b,uint16 c,uint16 d) {u.s.src_id=a; u.s.mode=b; u.s.port=c; u.s.channel=d;}
      operator Chip::DataID (void) const {return u.data_id;}
      union
      {
	Chip::DataID data_id;
	struct
	{
	  Chip::DataID channel:16, port:16, mode:16, src_id:16;
	} s;
      } u;
    };
    class Hit
    {
      uint32 coarse_time;
      uint32 hit_time;
      uint32 channel;
    public:
      Hit(const Hit& h) { coarse_time = h.coarse_time; hit_time = h.hit_time; channel = h.channel; }
      Hit(uint32 _coarse_time, uint32 _hit_time, uint32 _channel) 
	{ coarse_time = _coarse_time; hit_time = _hit_time; channel = _channel; }
      
      uint32 GetHitTime()    const { return hit_time;    }
      uint32 GetCoarseTime() const { return coarse_time; }
      uint32 GetChannel()    const { return channel;     }
    }; // Hit

    class Digit : public Chip::Digit
      {
      public:
	virtual ~Digit(void) {};
      Digit(const DataID &data_id, const DetID &id, int16 w, double t)
	: Chip::Digit(data_id,id), wire(w), time_unit(t), time_reference(0), trigger_time_subtraction(true) {}

        virtual void Print(std::ostream &o=std::cout,const std::string &prefix="") const;

        virtual const char* GetNtupleFormat(void) const {return "wire:time:unit:tns";}
        virtual std::vector<float> GetNtupleData(void) const;

	/// \return number of wire associated with the channel
        int16 GetWire(void) const {return wire;}
	/// \return number of wire associated with the channel
        int16 GetChannel(void) const {return wire;}

        /// \return coarse time without trigger time subtracted associated with
        ///         the complete TDC in units of time bins
        int64 GetCoarseTime(void) const {return coarseTime;}
        void  SetCoarseTime(int64 t)    {coarseTime=t;}

        /// \return raw time without trigger time subtracted associated with
        ///         the channel in units of time bins
        int64 GetTime(void) const {return hitTime;}
        void  SetTime(int64 t)    {hitTime=t;}

        /// \return decoded time in units of ns
        double GetTimeDecoded(void) const { return time_decoded; }
        void   SetTimeDecoded(double t)   {time_decoded=t;}

        /// The cost of one time bin
        double          GetTimeUnit              (void) const {return time_unit;}

        double          GetTimeReference         (void) const {return time_reference;}
        void            SetTimeReference         (double t) {time_reference=t;}

        double          DoTriggerTimeSubtraction (void) const {return trigger_time_subtraction;}
        void            SetTriggerTimeSubtraction(bool t) {trigger_time_subtraction=t;}

       private:

        // fixed at construction time
	const int16     wire;                      ///< number of wire
        const double    time_unit;                 ///< unit of time measurement

        // set during initialization
	double          time_reference;            ///< reference time in time bins
	bool            trigger_time_subtraction;  ///< subtract trigger time

        // set for each event
        int64           coarseTime;
	int64           hitTime;
        double          time_decoded;              ///< time in ns with the respect to trigger time
      };

    class Data
    {
      union {
	struct {
	  uint32 size : 16, // data size in 32-bits words (12 bits)
	    port      : 15, // port number (4 bits)
	    zero      : 1;
	    } h0; // first word of the header
	struct {
	  uint32 event_num : 31,
	    one : 1;
	    } h1; // second one
	struct {
	  uint32 coarse_time : 31,
	    one : 1;
	    } h2; // third one
	struct {
	  uint32 time : 24,
	    channel : 7,
	    one :  1;
	    } d; // TDC channel data
	struct {
	  uint32 crc32 : 31,
	    one :  1;
	    } t; // Trailer
	uint32 all;
      } data;
    public:
      Data() {};
      Data(uint32 d) { data.all = d; }
      
      Data&      operator =  (uint32 d) {data.all=d; return *this;}

      operator uint32         (void) const {return  data.all;}
      
      std::string     GetName(void) const {return "ChipTDC_data";}

      /// \return \b true if it is indead a header
      bool            IsHeader                (void) const {return (data.h0.zero == 0);}
      
      /// \ return chip's data size (12 bits) size in words
      uint32          GetSize                 (void) const {return data.h0.size;}

      /// \ return chip's port number (4 bits)
      uint32          GetPort                 (void) const {return data.h0.port;}

      /// \return event number
      uint32          GetEventNumber          (void) const {return data.h1.event_num;}

      /// \return coarse trigger time
      uint32          GetCoarseT              (void) const {return data.h2.coarse_time;}

      /// \return channel number
      uint32          GetChannel              (void) const {return data.d.channel;}

      /// \return channel hit time
      uint32          GetHitTime              (void) const {return data.d.time;}

      /// \return port data CRC32 checksum
      uint32           GetCRC32              (void) const {return data.t.crc32;}

      /// Print properties.
      void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

      /// Print-operator
      friend std::ostream    &operator <<     (std::ostream &o,const Data &e) {e.Print(o); return o;}
      
      friend class ChipTDC;
    };
    
    
    class SpeakTimeData
    {
      union {
	struct {
	  uint32 size : 16, // data size in 32-bits words (12 bits)
	    port      : 15, // port number (4 bits)
	    zero      : 1;
	    } h0; // first word of the header
	struct {
	  uint32 event_num : 31,
	    one : 1;
	    } h1; // second one
	struct {
	  uint32 coarse_time : 31,
	    one : 1;
	    } h2; // third one
	struct {
	  uint32 time : 30,
	    edge : 1,
	    one :  1;
	    } d; // TDC channel data
	struct {
	  uint32 crc32 : 31,
	    one :  1;
	    } t; // Trailer
	uint32 all;
      } data;
    public:
      SpeakTimeData() {};
      SpeakTimeData(uint32 d) { data.all = d; }
      
      SpeakTimeData&      operator =  (uint32 d) {data.all=d; return *this;}

      operator uint32         (void) const {return  data.all;}
      
      std::string     GetName(void) const {return "ChipTDC_SpekaingTimeData";}

      /// \return \b true if it is indead a header
      bool            IsHeader                (void) const {return (data.h0.zero == 0);}
      
      /// \ return chip's data size (12 bits) size in words
      uint32          GetSize                 (void) const {return data.h0.size;}

      /// \ return chip's port number (4 bits)
      uint32          GetPort                 (void) const {return data.h0.port;}

      /// \return event number
      uint32          GetEventNumber          (void) const {return data.h1.event_num;}

      /// \return coarse trigger time
      uint32          GetCoarseT              (void) const {return data.h2.coarse_time;}

      /// \return channel number
      uint32          GetChannel              (void) const {return data.d.edge;}

      /// \return channel hit time
      uint32          GetHitTime              (void) const {return data.d.time;}

      /// \return port data CRC32 checksum
      uint32           GetCRC32              (void) const {return data.t.crc32;}

      /// Print properties.
      void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

      /// Print-operator
      friend std::ostream    &operator <<     (std::ostream &o,const SpeakTimeData &e) {e.Print(o); return o;}
      
      friend class ChipTDC;
    };

  //============================================================================
  // Constructors and destructor
  //============================================================================

  public:
    
    /// Destructor
    ~ChipTDC() {};
    
    /*! \brief Base constructor.
      \sa DaqEvent::DaqEvent(void const * const buf,bool copy_buf)
    */
    ChipTDC(void const * const buf, bool copy_buf, DaqOption &opt, DaqEvent &ev);

  private:

    /// Copy constructor
    ChipTDC(const ChipTDC &e);
    
    //============================================================================
    // Operators
    //============================================================================

  private:

    /// Assignment operator
    ChipTDC& operator = (const ChipTDC &e);

  private:

    //============================================================================
    // Methods
    //============================================================================


  public:
  
     /// \return \b true if the data are in \c speak time moed 
    bool                IsSpeakTimeMode             (void) const {return ((GetSLink().GetFormat()&(0x0e)) == (0x0e));}

  
    /// Print equipment
    void                Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

    /// Clear the chip status (do \b not modify the real data buffer).
    void                Clear                   (void) { data_all.clear(); }

    /// \return Chip name. This is "ChipTDC"
    std::string         GetName                 (void) const {return "ChipTDC";}

    /// Decode data and \b add new digits to \b digits_list.
    void                Decode                  (const Maps &maps,Digits &digits_list,DaqOption &opt);
    
  private:

  protected:

    /*! \brief Scan data, fill \c data_all attribute, detect errors.    
     */
    void                Scan                    (DaqOption &opt);

    /*! \brief Scan Normal data, fill \c data_all attribute, detect errors.    
     */
    void                ScanNormalMode          (DaqOption &opt);

    /*! \brief Scan Normal data, fill \c data_all attribute, detect errors.    
     */
    void                ScanSpeakingTimeMode    (DaqOption &opt);

    //============================================================================
    // Attributes
    //============================================================================

  private:
    
    std::vector< std::pair<DataID, Hit> >     data_all;         ///< Data for decoding
  }; //ChipTDC

}  // namespace CS

#endif //__CHIP_TDC_H__
