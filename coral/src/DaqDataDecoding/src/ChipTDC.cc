#include <math.h>
#include <stdint.h>

#include "ChipTDC.h"
#include "DaqError.h"
#include "DaqOption.h"
#include "TriggerTime.h"
#include "DaqEvent.h"
#include "utils.h"

using namespace std;

namespace CS {

  inline uint32_t COMPUTE(uint32_t crc, uint8_t ch)
  {
    static const uint32_t crctab[] = {
      0x0,
      0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
      0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6,
      0x2b4bcb61, 0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd,
      0x4c11db70, 0x48d0c6c7, 0x4593e01e, 0x4152fda9, 0x5f15adac,
      0x5bd4b01b, 0x569796c2, 0x52568b75, 0x6a1936c8, 0x6ed82b7f,
      0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3, 0x709f7b7a,
      0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
      0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58,
      0xbaea46ef, 0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033,
      0xa4ad16ea, 0xa06c0b5d, 0xd4326d90, 0xd0f37027, 0xddb056fe,
      0xd9714b49, 0xc7361b4c, 0xc3f706fb, 0xceb42022, 0xca753d95,
      0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1, 0xe13ef6f4,
      0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
      0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5,
      0x2ac12072, 0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16,
      0x018aeb13, 0x054bf6a4, 0x0808d07d, 0x0cc9cdca, 0x7897ab07,
      0x7c56b6b0, 0x71159069, 0x75d48dde, 0x6b93dddb, 0x6f52c06c,
      0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08, 0x571d7dd1,
      0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
      0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b,
      0xbb60adfc, 0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698,
      0x832f1041, 0x87ee0df6, 0x99a95df3, 0x9d684044, 0x902b669d,
      0x94ea7b2a, 0xe0b41de7, 0xe4750050, 0xe9362689, 0xedf73b3e,
      0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2, 0xc6bcf05f,
      0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
      0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80,
      0x644fc637, 0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb,
      0x4f040d56, 0x4bc510e1, 0x46863638, 0x42472b8f, 0x5c007b8a,
      0x58c1663d, 0x558240e4, 0x51435d53, 0x251d3b9e, 0x21dc2629,
      0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5, 0x3f9b762c,
      0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
      0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e,
      0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65,
      0xeba91bbc, 0xef68060b, 0xd727bbb6, 0xd3e6a601, 0xdea580d8,
      0xda649d6f, 0xc423cd6a, 0xc0e2d0dd, 0xcda1f604, 0xc960ebb3,
      0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7, 0xae3afba2,
      0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
      0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74,
      0x857130c3, 0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640,
      0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b, 0x43cdc09c, 0x7b827d21,
      0x7f436096, 0x7200464f, 0x76c15bf8, 0x68860bfd, 0x6c47164a,
      0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e, 0x18197087,
      0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
      0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d,
      0x2056cd3a, 0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce,
      0xcc2b1d17, 0xc8ea00a0, 0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb,
      0xdbee767c, 0xe3a1cbc1, 0xe760d676, 0xea23f0af, 0xeee2ed18,
      0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4, 0x89b8fd09,
      0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
      0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf,
      0xa2f33668, 0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
    };
    crc = (crc << 8) ^ crctab[((crc>>24) ^ ch) & 0xff];
    return crc;
  }
  static uint32_t CheckSum(uint32_t init_val, uint32_t data)
  {
    uint8_t* p = (uint8_t*)(&data);
    uint32_t crc32 = init_val;
    for(int i = 3; i >= 0; i--)
      {
	crc32 = COMPUTE(crc32,p[i]);
      }
    return crc32;
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  ChipTDC::ChipTDC(void const* const buf, bool copy_buf, DaqOption &opt, DaqEvent &ev) : Chip(buf,copy_buf,opt,ev)
  {
    Clear();
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  

  void ChipTDC::Decode(const Maps &maps, Digits &digits_list, DaqOption &opt)
  {
    // Scan the chip data if it was not done yet.
    if( !is_scaned )
      Scan(opt);

    size_t size = digits_list.size();

    // Loop for all found data lines
    for( vector< pair<DataID, Hit> >::iterator it = data_all.begin(); it != data_all.end(); it++ )
      {
        typedef Maps::const_iterator m_it; // Create a short name for map's iterator
        pair<m_it,m_it> m_range = maps.equal_range(it->first); // all maps with given data ID

	/*printf("ifTDC Hit. Channel = %u, Time = %u, c.time = %u, id = %lu\n",
	       it->second.GetChannel(), it->second.GetHitTime(), it->second.GetCoarseTime(),it->first);
	*/
        for( m_it c = m_range.first; c != m_range.second; c++ )
	  {
            const Digit *digit1 = dynamic_cast<Digit*>(c->second);
            if( digit1 == NULL )
	      throw Exception("ChipTDC::Decode(): Internal error");
            
            Digit *digit2 = new Digit(*digit1);

	    // lowest 4 bits of coarse time are not in data stream
	    const int64 trigTime = it->second.GetCoarseTime() << 4;

	    // correct for overrolling of 24 bit counter, hitTime might then
	    // be negative
	    const int64 hitTime = TimeDifference<int64>(it->second.GetHitTime(), 0, 0x1000000, trigTime&0xffffff);

	    // hit time is then the sum of hitTime and trigTime with the lowest
	    // 24 bits being zero
	    digit2->SetCoarseTime(trigTime);
	    digit2->SetTime(hitTime + (trigTime&0x7ff000000));

	    digits_list.insert(pair<DetID,Digit*>(digit2->GetDetID(),digit2));
	  }

        if( size>=digits_list.size() )
	  Exception("ChipTDC::Decode():WW: some maps were not found for srcID=%d port=%d chan=%d",
		    it->first.u.s.src_id,it->first.u.s.port,it->first.u.s.channel);
        
        size=digits_list.size();
      }
  }

  ////////////////////////////////////////////////////////////////////////////////


  void ChipTDC::Scan(DaqOption &opt)
  {
    
    if( is_scaned )
      return;
    else
      is_scaned=true;

    if (GetSLink().IsFirstEventInRun())
      {
        Exception("ChipTDC::Scan():WW: first event of run").Print();
        return;
      }
            
    if ( IsSpeakTimeMode() ) {
      ScanSpeakingTimeMode(opt);
      }
    else 
      {
	ScanNormalMode(opt);
      }
  }

  void ChipTDC::ScanNormalMode(DaqOption &opt)
  {
    //if( is_scaned )
      //return;
    //else
      //is_scaned=true;

    //if (GetSLink().IsFirstEventInRun())
      //{
        //Exception("ChipTDC::Scan():WW: first event of run").Print();
        //return;
      //}
    uint32 source = GetSLink().GetSourceID();
    int stage = 0;
    int16 port = -1;
    int16 size = -1;
    uint32 ev_num = 0;
    uint32 coarse_t = 0;
    uint32 src_sz = 0;
    uint32 port_sz = 0;
    uint32 CRC32 = 0;
    std::vector<Hit> port_hits;
    for( const uint32* p = GetDataStart(); p < GetDataEnd(); p++ )
      {
	src_sz++;
	port_sz++;
	if(src_sz > GetSLink().GetEventSize()-3)
	  {
	    AddError(DaqError(DaqError::ChipTDC_BAD_DATA_SIZE,
			      DaqError::SOURCE_ID, source,
			      DaqError::VALUE, src_sz), opt);
	    break;
	  }
	Data data = *p;
  
	if(stage == 0)
	  {
	    if(!data.IsHeader()) // unexpected data word
	      {
		AddError(DaqError(DaqError::ChipTDC_UNEXPECTED_DATA,
				  DaqError::SOURCE_ID, source), opt);
		break;
	      }
	    port = data.GetPort();
	    if(port >= 15)
	      {
		AddError(DaqError(DaqError::ChipTDC_BAD_PORT_NUM,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port), opt);
		break;
	      }
	    size = data.GetSize();
	    if(size > (int)GetSLink().GetEventSize()-2-(int)src_sz || size > 0xFFF)
	      {
		AddError(DaqError(DaqError::ChipTDC_BAD_PORT_DATA_SIZE,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port,
				  DaqError::VALUE, size), opt);
		break;
	      }
	    stage = 1;
	    continue;
	  }
	if(data.IsHeader()) // unexpected header word
	  {
	    AddError(DaqError(DaqError::ChipTDC_UNEXPECTED_HEADER,
		     DaqError::SOURCE_ID, source,
		     DaqError::PORT, data.GetPort()), opt);
	    break;
	  }
	if(stage == 1) // we are waiting for 2-nd port header word
	  {
	    uint32_t d = *p;
	    CRC32 = CheckSum(0,d);
	    ev_num = data.GetEventNumber();
	    if(ev_num != GetSLink().GetEventNumber())
	      {
		AddError(DaqError(DaqError::ChipTDC_EV_NUM_MISMATCH,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port), opt);
		break;
	      }
	    stage = 2;
	    continue;
	  }
	if(stage == 2) // we are waiting for 3-rd port header word
	  {
	    uint32_t d = *p;
	    CRC32 = CheckSum(CRC32,d);
	    coarse_t = data.GetCoarseT();
	    if((int)port_sz == size-1) // it is last data word for the port
	      stage = 4;
	    else
	      stage = 3;
	    continue;
	  }
	if(stage == 3) // this is TDC channel data
	  {
	    uint32_t d = *p;
	    CRC32 = CheckSum(CRC32,d);
	    if(data.GetChannel() > 64)
	      {
		AddError(DaqError(DaqError::ChipTDC_BAD_CHAN_NUM,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port,
				  DaqError::CHANNEL, data.GetChannel()), opt);
		if((int)port_sz == size-1) // it is last data word for the port
		  stage = 4; 
		continue;
	      }
	    uint32 chan = data.GetChannel();
	    uint32 t = data.GetHitTime();
	    Hit h(coarse_t, t, chan);
	    port_hits.push_back(h);
	    if((int)port_sz == size-1) // it is last data word for the port
	      stage = 4; 
	    continue;
	  }
	if(stage == 4)
	  {
	    uint32 d = data.GetCRC32();
	    if(d != (CRC32 & (0x7fffffff)))
	      {
		AddError(DaqError(DaqError::ChipTDC_CRC32_CHECKSUM_ERROR,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port,
				  DaqError::VALUE, d), opt);
	      }
	    else
	      {
		for(unsigned i = 0; i < port_hits.size(); i++)
		  {
		    DataID data_id(GetSourceID(),0,port,port_hits[i].GetChannel());
		    data_all.push_back(std::pair<DataID,Hit>(data_id,port_hits[i]));
		    //printf("SrcID: %d, port: %d, channel: %d, time: %u\n", GetSourceID(),port, port_hits[i].GetChannel() ,port_hits[i].GetHitTime());
		  }
	      }
	    port_hits.clear();
	    port_sz = 0;
	    stage = 0;
	    continue;
	  }
      }
  }

  ////////////////////////////////////////////////////////////////////////////////

  void ChipTDC::ScanSpeakingTimeMode(DaqOption &opt)
  {
    uint32 source = GetSLink().GetSourceID();
    int stage = 0;
    int16 port = -1;
    int16 size = -1;
    uint32 ev_num = 0;
    uint32 coarse_t = 0;
    uint32 src_sz = 0;
    uint32 port_sz = 0;
    uint32 CRC32 = 0;
    std::vector<Hit> port_hits;
    for( const uint32* p = GetDataStart(); p < GetDataEnd(); p++ )
      {

	src_sz++;
	port_sz++;
	if(src_sz > GetSLink().GetEventSize()-3)
	  {
	    AddError(DaqError(DaqError::ChipTDC_BAD_DATA_SIZE,
			      DaqError::SOURCE_ID, source,
			      DaqError::VALUE, src_sz), opt);
	    break;
	  }
	SpeakTimeData data = *p;
    
	if(stage == 0) // we are analyzing 1st port header word
	  {
	    //Check for right header structure
	    if(!data.IsHeader()) // unexpected data word
	      {
		AddError(DaqError(DaqError::ChipTDC_UNEXPECTED_DATA,
				  DaqError::SOURCE_ID, source), opt);
		break;
	      }
	    port = data.GetPort();
	    //Check for right port range
	    if(port >= 15)
	      {
		AddError(DaqError(DaqError::ChipTDC_BAD_PORT_NUM,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port), opt);
		break;
	      }
      
	    //Check for port data size
	    size = data.GetSize();
	    if(size > (int)GetSLink().GetEventSize()-2-(int)src_sz || size > 0xFFF)
	      {
		AddError(DaqError(DaqError::ChipTDC_BAD_PORT_DATA_SIZE,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port,
				  DaqError::VALUE, size), opt);
		break;
	      }
	    stage = 1;
	    continue;
	  }
    
	if(data.IsHeader()) // unexpected header word
	  {
	    AddError(DaqError(DaqError::ChipTDC_UNEXPECTED_HEADER,
			      DaqError::SOURCE_ID, source,
			      DaqError::PORT, data.GetPort()), opt);
	    break;
	  }
   
	if(stage == 1) // we are waiting for 2-nd port header word
	  {
	    uint32_t d = *p;
	    CRC32 = CheckSum(0,d);
	    ev_num = data.GetEventNumber();
	    if(ev_num != GetSLink().GetEventNumber())
	      {
		AddError(DaqError(DaqError::ChipTDC_EV_NUM_MISMATCH,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port), opt);
		break;
	      }
	    stage = 2;
	    continue;
	  }
    
	if(stage == 2) // we are waiting for 3-rd port header word
	  {
	    uint32_t d = *p;
	    CRC32 = CheckSum(CRC32,d);
	    coarse_t = data.GetCoarseT();
	    if((int)port_sz == size-1) // it is last data word for the port
	      stage = 4;
	    else
	      stage = 3;
	    continue;
	  }
	if(stage == 3) // this is TDC channel data
	  {
      
	    uint32_t d = *p;
	    CRC32 = CheckSum(CRC32,d);
	    if(data.GetChannel() > 64)
	      {
		AddError(DaqError(DaqError::ChipTDC_BAD_CHAN_NUM,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port,
				  DaqError::CHANNEL, data.GetChannel()), opt);
		if((int)port_sz == size-1) // it is last data word for the port
		  stage = 4; 
		continue;
	      }
	    uint32 chan = data.GetChannel();
	    uint32 t = data.GetHitTime();
	    Hit h(coarse_t, t, chan);
	    port_hits.push_back(h);
	    if((int)port_sz == size-1) // it is last data word for the port
	      stage = 4; 
	    continue;
	  }
  
	//CRC data word 
	if(stage == 4)
	  {
	    uint32 d = data.GetCRC32();
	    //cout << "transmitted" << d << " calculated: " << CRC32 << endl;
      
	    if(d != (CRC32 & (0x7fffffff)))
	      {
		AddError(DaqError(DaqError::ChipTDC_CRC32_CHECKSUM_ERROR,
				  DaqError::SOURCE_ID, source,
				  DaqError::PORT, port,
				  DaqError::VALUE, d), opt);
	      }
	    else
	      {
		for(unsigned i = 0; i < port_hits.size(); i++)
		  {
		    DataID data_id(GetSourceID(),0,port,port_hits[i].GetChannel());
		    data_all.push_back(std::pair<DataID,Hit>(data_id,port_hits[i]));
		  }
	      }
	    port_hits.clear();
	    port_sz = 0;
	    stage = 0;
	    continue;
	  }
      }
  }

  ////////////////////////////////////////////////////////////////////////////////


  ChipTDC::Map::Map(const ObjectXML &o) : Chip::Map(o),
					  mode('?'),
					  port(-1),
					  time_unit(1.0),
					  time_reference(0),
					  trigger_time_subtraction(true)
  {
    if( version==0 )
      version=1;

    if( GetName()!="ChipTDC" )
      throw Exception("ChipTDC::Map::Map(): Internal error.");

    if( GetVersion() != 1 )
      throw Exception("ChipTDC::Map::Map(): unknown version %d",GetVersion());

    // If "time_unit" has been set in a mapping file, then use it ....
    if( NULL==GetAttribute("time_unit",time_unit) )
      {
        SetAttribute("time_unit","1.0");
        GetAttribute("time_unit",time_unit);
      }

    if( NULL==GetAttribute("time_reference",time_reference) )
      {
        SetAttribute("time_reference","0.0");
        GetAttribute("time_reference",time_reference);
      }

    istringstream s(dec_line.c_str());

    // get detector name and detector ID
    string name;
    s >> name;
    if( s.fail() )
      throw Exception("ChipTDC::Map::Map(): bad name format in line: \"%s\"",map_line.c_str());
    id = DetID(name);

    // get source ID and port
    s >> source_id >> port;
    if( s.fail() )
      throw Exception("ChipTDC::Map::Map(): bad srcID format in line: \"%s\"",map_line.c_str());

    // three options how the mapping line could look like:
    // 1. only contains a single channel
    //    one value to be read
    // 2. contains blocks of channels and wires with steps +-1
    //    four values to be read
    // 3. contains first, last and step for channels and wires
    //    six values to be read

    // read all available values
    std::vector<int32> values;
      {
        int32 value;
        while( s>>value )
          values.push_back(value);
      }
    if( s.fail() && !s.eof() )
      throw Exception("ChipTDC::Map::Map(): bad values format in line: \"%s\"",map_line.c_str());

    if( values.size() == 1)
      {
        chanF = values[0];
        chanL = chanF;
        chanS = 1;

        wireF = 0;
        wireL = 0;
        wireS = 1;
      }
    else if( values.size() == 2)
      {
        chanF = values[0];
        chanL = chanF;
        chanS = 0;
        wireF = values[1];
        wireL = wireF;
        wireS = 0;
      }
    else if( values.size() == 4)
      {
        chanF = values[0];
        chanL = values[1];
        chanS = (chanL >= chanF) ? 1 : -1;

        wireF = values[2];
        wireL = values[3];
        wireS = (wireL >= wireF) ? 1 : -1;
      }
    else if( values.size() == 6)
      {
        chanF = values[0];
        chanL = values[1];
        chanS = values[2];

        wireF = values[3];
        wireL = values[4];
        wireS = values[5];
      }
    else
      throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\"",map_line.c_str());


    if(chanS == 0 && wireS == 0) // single channel map
      {
        if(chanF != chanL || wireF != wireL)
          throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": "
			  "first channel or wire is not equil last one.",map_line.c_str());
      }
    else
      {
	// some sanity checks:
	// * steps should not be zero
	if( chanS==0 )
	  throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": channel step is zero",map_line.c_str());
	if( wireS==0 )
	  throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": wire step is zero",map_line.c_str());

	// * sign of steps should be in agreement with difference between last and
	//   first wire
	if( (chanL-chanF)*chanS < 0)
	  throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": range and sign of step not in agreement for channels",map_line.c_str());
	if( (wireL-wireF)*wireS < 0)
	  throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": range and sign of step not in agreement for wires",map_line.c_str());

	// * difference between last and first wire should be a multiple of the
	//   step size
	if( (chanL-chanF)%chanS != 0)
	  throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": range and step size not in agreement for channels",map_line.c_str());
	if( (wireL-wireF)%wireS != 0)
	  throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": range and step size not in agreement for wires",map_line.c_str());

	// * number of channels should agree with number of wires
	if( ((chanL-chanF)/chanS) != ((wireL-wireF)/wireS) )
	  throw Exception("ChipTDC::Map::Map(): bad format in line: \"%s\": number of channels not in agreement with number of wires",map_line.c_str());
      }
    if( IsOption("MWPC-TDC"))
      {
        mode = 'w';
      }
    else if( IsOption("TDC-T0") )
      {
        mode = 't';
      }
    else if(IsOption("MWPC-TDC+T0"))
      {
	mode = 'm';
      }
    else if( IsOption("SPEAKINGTIME-TDC") )
      {
        mode = 's';
      }
    else
      throw Exception("ChipTDC::Map::Map(): unknown option(s) \"%s\" for line \"%s\"",
                      options.c_str(),map_line.c_str());

    if( IsOption("no_trigger_time_subtraction") )
        trigger_time_subtraction = false;

    Check();
  }

  ////////////////////////////////////////////////////////////////////////////////
  void ChipTDC::Map::Check()
  {
    // todo
  }

  ////////////////////////////////////////////////////////////////////////////////

  void ChipTDC::Map::AddToMaps(Maps &maps, DaqOption &options) const
  {
    // check wether this detector is to be used to retrieve the trigger mask
    string trigger_mask_data;
    if( NULL!=GetAttribute("trigger_mask_data",trigger_mask_data) )
      {
        size_t n = trigger_mask_data.find('-');
        if( n==string::npos )
          throw Exception("ChipTDC::Map::AddToMaps(): Bad attribute \"trigger_mask_data\"=\"%s\"",trigger_mask_data.c_str());
        unsigned
          min = atoi(trigger_mask_data.substr(0,n).c_str()),
          max = atoi(trigger_mask_data.substr(n+1,string::npos).c_str());

        float
          jitter  = options.GetTTConfig().time_jitter,
          precise = options.GetTTConfig().time_precise,
          sigma   = options.GetTTConfig().triggers_diff_sigma;
        GetAttribute("trigger_time_jitter",jitter);
        GetAttribute("trigger_time_precise",precise);
        GetAttribute("triggers_diff_sigma",sigma);

        const TriggerTimeConfig::TTC *ttc = options.GetTTConfig().Find(time_unit);
        if( ttc==NULL )
          throw Exception("ChipTDC::Map::AddToMaps(): TT for the trigger_mask time unit %g is not found!",time_unit);

        if( DetID("")==options.GetTTConfig().trigger_mask_DetID ) // trigger time config not yet set
          {
            options.GetTTConfig().trigger_mask_DetID    = GetDetID();
            options.GetTTConfig().trigger_mask_srcID    = GetSourceID();
            options.GetTTConfig().trigger_mask_port     = port;
            options.GetTTConfig().SetTriggerMaskDataLimit(min,max);
            options.GetTTConfig().trigger_mask_TT_index = ttc->index;
            options.GetTTConfig().time_jitter           = jitter;
            options.GetTTConfig().time_precise          = precise;
            options.GetTTConfig().triggers_diff_sigma   = sigma;
          }
        else
          {
            if( options.GetTTConfig().trigger_mask_DetID      !=GetDetID()    ||
                options.GetTTConfig().trigger_mask_srcID      !=GetSourceID() ||
                options.GetTTConfig().trigger_mask_port       !=port          ||
                options.GetTTConfig().trigger_mask_sources_min!=min           ||
                options.GetTTConfig().trigger_mask_sources_max!=max           ||
                options.GetTTConfig().trigger_mask_TT_index   !=ttc->index    ||
                options.GetTTConfig().time_jitter             !=jitter        ||
                options.GetTTConfig().time_precise            !=precise       ||
                options.GetTTConfig().triggers_diff_sigma     !=sigma )
              throw Exception("ChipTDC::Map::AddToMaps(): second set of trigger_mask options with non-compatible options found");
          }
      }

    // read option "time_in_spill" which contains the list of tbnames which
    // are to be used for time in spill measurement
    std::string tis;
    if( GetAttribute("time_in_spill", tis) )
      {
        istringstream s(tis); string str;
        while (s >> str)
            options.GetTTConfig().tis_tbnames.insert(str);
      }

    size_t size=maps.size();
    int factor=1;

    switch( mode )
      {
      case 't':
	{
	  if( (chanL-chanF) != 0 )
	    throw Exception("ChipTDC::Map::AddToMaps(): more than one channel mapped for trigger time \"%s\"!", id.GetName().c_str());

	  // This is TDC T0 measurement mode.
	  DataID data_id(GetSourceID(),0,port,chanF);
	  Digit* digit = new Digit(data_id,GetDetID(),0,time_unit);
	  
	  if( maps.end()!=maps.find(data_id) && !IsMultiDigit() )
	    {
	      Print();
	      Exception("ChipTDC::Map::AddToMaps(): map already exists!").Print();
	    }
	  
	  maps.insert( pair<DataID,Digit*>(data_id,digit) );
	  ReadTTConfig(options.GetTTConfig());
	  break;
	}
        
      case 'w':
	{
	  if( GetVersion() == 1)
	    {
	      int i = 0;
	      for(int _chan = chanF; _chan <= chanL; _chan++, i++)
		{
		  DataID data_id(GetSourceID(),0, port, _chan);
		  Digit *digit = new Digit(data_id, GetDetID(), wireF + i*wireS, time_unit);
		  digit->SetTimeReference(time_reference);
		  digit->SetTriggerTimeSubtraction(trigger_time_subtraction);
		  if( maps.end()!=maps.find(data_id) && !IsMultiDigit() )
		    {
		      Print();
		      Exception("ChipTDC::Map::AddToMaps():a: map already exists!").Print();
		    }

		  maps.insert( pair<DataID,Digit*>(data_id,digit) );
		}
	    }
	  else
	    throw Exception("ChipTDC::Map::AddToMaps(): unknown version %d for latch mode",GetVersion());
	  
	  break;
	}
  //Case for speaking time mode 
      case 's':
	{
	  if( GetVersion() == 1)
	    {
	      int i = 0;
	      for(int _chan = chanF; _chan <= chanL; _chan++, i++)
		{
		  DataID data_id(GetSourceID(),0, port, _chan);
		  Digit *digit = new Digit(data_id, GetDetID(), wireF + i*wireS, time_unit);
		  digit->SetTimeReference(time_reference);
		  digit->SetTriggerTimeSubtraction(trigger_time_subtraction);
		  if( maps.end()!=maps.find(data_id) && !IsMultiDigit() )
		    {
		      Print();
		      Exception("ChipTDC::Map::AddToMaps():a: map already exists!").Print();
		    }

		  maps.insert( pair<DataID,Digit*>(data_id,digit) );
		}
	    }
	  else
	    throw Exception("ChipTDC::Map::AddToMaps(): unknown version %d for latch mode",GetVersion());
	  
	  break;
	}

      default:
        throw Exception("ChipTDC::Map::AddToMaps(): unknown mode %c",mode);
      }
    if(GetChanS() != 0)
      maps.SetWires( GetDetID(), maps.GetWires(GetDetID())+GetChanN());
    else
      maps.SetWires( GetDetID(), maps.GetWires(GetDetID())+1);
  }

  ////////////////////////////////////////////////////////////////////////////////

  void ChipTDC::Map::ReadTTConfig(TriggerTimeConfig &tt_conf) const
  {
    // Check for the trigger time decoding options.
    int TT_index=-1, TT_index_recover=-1, TT_channels=0;
    int64 TT_overolling=0;
    double TT_sigma=0, time_unit=0;

    if( NULL!=GetAttribute("TT_index",TT_index) )
      {
        bool ok = GetAttribute("TT_channels",  TT_channels  ) &&
	  GetAttribute("TT_overolling",TT_overolling) &&
	  GetAttribute("time_unit",    time_unit);
        if(!ok)
	  throw Exception("ChipTDC::Map::Map(): Bad settings for the trigger time");
        
        TriggerTimeConfig::TTC ttc(id, time_unit, TT_index,TT_index_recover,TT_channels,TT_overolling,TT_sigma);
        ttc.srcID = GetSourceID();
        ttc.port  = port;
        GetAttribute("MT_shift", ttc.MT_shift);
        tt_conf.Add(ttc);
      }
  }

  ////////////////////////////////////////////////////////////////////////////////

  void ChipTDC::Print(ostream &o,const string &prefix) const
  {
  }

  ////////////////////////////////////////////////////////////////////////////////

  void ChipTDC::Data::Print(ostream &o,const string &prefix) const
  {
  }
  
  ////////////////////////////////////////////////////////////////////////////////

  void ChipTDC::SpeakTimeData::Print(ostream &o,const string &prefix) const
  {

    if(IsHeader()) {
        cout << "Header" << endl;
    }
    else {
    cout << "Edge:" << GetChannel();
    cout << " HitTime:" << GetHitTime()<<endl;
    }
  }

  ////////////////////////////////////////////////////////////////////////////////

  void ChipTDC::Digit::Print(ostream &o,const string &prefix) const
  {
    Chip::Digit::Print(o,prefix);
    o<<prefix;

    char s[256];
    sprintf(s,"ChipTDC::digit: wire = %d hitTime = %010lx time_unit = %lf\n",
	    int(wire),hitTime,time_unit);
    o << s;
  }

  void ChipTDC::Map::Print(ostream &o,const string &prefix) const
  {
    Chip::Map::Print(o,prefix);
    o<<prefix;

    char s[256];
    sprintf(s,"ChipTDC::Map: mode=\'%c\' port=%d time_unit=%lf\n",
	    mode,int(port),time_unit);
    o << s;
  }

  ////////////////////////////////////////////////////////////////////////////////

  std::vector<float> ChipTDC::Digit::GetNtupleData(void) const
  {
    std::vector<float> v;
    v.push_back(wire);
    v.push_back(hitTime);
    v.push_back(time_unit);
    v.push_back(time_decoded);
    return v;
  }

  ////////////////////////////////////////////////////////////////////////////////

}; // namespace CS
