#include <cassert>

#include "DaqEvent.h"
#include "ChipNA64WaveBoard.h"


using namespace std;

namespace CS {




////////////////////////////////////////////////////////////////////////////////
// A.C. looks the only function that I should really change..
void ChipNA64WaveBoard::Scan(DaqOption &opt)
{
    if( is_scaned )
        return;
    else
        is_scaned=true;


    if( !pre_digits.empty() )
        throw "ChipNA64WaveBoard::Scan(): an attempt to call Scan() without Clear()!";

    if( opt.GetWaveBoardDataVer2().count(GetSourceID())>0 )
    {
        ScanDataVer2(opt);
        return;
    }


    const uint32 *p;
    for( p=GetDataStart(); p<GetDataEnd(); )
    {
        // We expect an ADC header (three words)
        const HeaderADC_v1 *h_adc = (HeaderADC_v1*) p;
         // Now we check HeaderADC.
        {

          // Check h_adc format
            if ((h_adc->spare1!=0)||(h_adc->port>15)||(h_adc->spare2!=1)||(h_adc->spare3!=1)){

              AddError(DaqError(DaqError::SADC_BAD_DATA, DaqError::SOURCE_ID,GetSourceID(),
                                                            DaqError::SL_EVENT_N,event->GetHeader().GetEventNumberInBurst(),
                                                            DaqError::EVENT_N,h_adc->eventNum),opt);
              return;
            }
            // Check the event number.
            if( h_adc->eventNum!=(GetSLink().GetEventNumber()) )
            {
                AddError(DaqError(DaqError::SADC_WRONG_EVENT, DaqError::SOURCE_ID,GetSourceID(),
                    DaqError::SL_EVENT_N,event->GetHeader().GetEventNumberInBurst(),
                    DaqError::EVENT_N,h_adc->eventNum),opt);
                return;
            }
        }

        if (uint32(GetDataEnd()-p)<h_adc->size)
        {
            AddError(DaqError(DaqError::SADC_H_ADC_BAD_SIZE,DaqError::SOURCE_ID,GetSourceID(),DaqError::COUNTER,h_adc->size,DaqError::VALUE,1),opt);
            return;
        }

        if( h_adc->size<1 || (p+h_adc->size)>GetDataEnd() )
        {
            AddError(DaqError(DaqError::SADC_H_ADC_BAD_SIZE,DaqError::SOURCE_ID,GetSourceID(),DaqError::COUNTER,h_adc->size,DaqError::VALUE,2),opt);
            return;
        }

        p++;    // go to the fourth word after the ADC header (that is 3 words)
        p++;
        p++;


        // Decode ADC data block
        const int size=+h_adc->size-4;
        while( p<(((uint32*)h_adc)+size)){
          const ChannelInfo *h_ch=(ChannelInfo*)p;
          //check channel header
          if (h_ch->spare!=0xFED){
            AddError(DaqError(DaqError::SADC_BAD_DATA, DaqError::SOURCE_ID,GetSourceID(),
                                                       DaqError::SL_EVENT_N,event->GetHeader().GetEventNumberInBurst(),
                                                       DaqError::EVENT_N,h_adc->eventNum),opt);
            return;
          }
          p++;
          
          // fill digit
          Digit* digit = new Digit(DataID(GetSourceID(),h_adc->port,0,h_ch->channel), DetID(""));
          auto& samples = digit->GetSamples();
          
          for (int is=0;is<h_ch->samples/2;is++){
            const Data *d=(Data*)p;
            //check data
            if ((d->spare0!=2)||(d->spare1!=2)){
              AddError(DaqError(DaqError::SADC_BAD_DATA, DaqError::SOURCE_ID,GetSourceID(),
                  DaqError::SL_EVENT_N,event->GetHeader().GetEventNumberInBurst(),
                  DaqError::EVENT_N,h_adc->eventNum),opt);
              return;
            }
            samples.push_back(d->sample0);
            samples.push_back(d->sample1);
            p++;
          }
          
          // and add to the list
          pre_digits.push_back(digit);
        }
        //end of the loop on the channels
        
        //last word is CRC, skip
        p++;
    }

    // We must have at least 3 words for channel data samples.
    /*if (p + 3 > GetDataEnd()) {
      AddError(
          DaqError(DaqError::SADC_H_ADC_BAD_SIZE, DaqError::SOURCE_ID, GetSourceID(), DaqError::CHIP, h_adc->chip, DaqError::COUNTER, h_adc->size,
              DaqError::VALUE, 3), opt);
      return;
    }*/



    if( uint32(GetDataEnd()-p)!=0 )
        AddError(DaqError(DaqError::SADC_H_ADC_BAD_SIZE,DaqError::SOURCE_ID,GetSourceID(),DaqError::VALUE,4),opt);
}



void ChipNA64WaveBoard::ScanDataVer2(DaqOption &opt)
{


    const uint32 *p;
    for( p=GetDataStart(); p<GetDataEnd(); )
    {
        // We expect an ADC header (one word2)
        const HeaderADC_v2 *h_adc = (HeaderADC_v2*) p;

        // Check h_adc format
        if ((h_adc->spare1!=0)||(h_adc->port>15)){
          AddError(DaqError(DaqError::SADC_BAD_DATA, DaqError::SOURCE_ID,GetSourceID(),
              DaqError::SL_EVENT_N,event->GetHeader().GetEventNumberInBurst(),
              DaqError::EVENT_N,GetSLink().GetEventNumber()),opt);
          return;
        }


        if (uint32(GetDataEnd()-p)<h_adc->size)
        {
            AddError(DaqError(DaqError::SADC_H_ADC_BAD_SIZE,DaqError::SOURCE_ID,GetSourceID(),DaqError::COUNTER,h_adc->size,DaqError::VALUE,1),opt);
            return;
        }

        if( h_adc->size<1 || (p+h_adc->size)>GetDataEnd() )
        {
            AddError(DaqError(DaqError::SADC_H_ADC_BAD_SIZE,DaqError::SOURCE_ID,GetSourceID(),DaqError::COUNTER,h_adc->size,DaqError::VALUE,2),opt);
            return;
        }

        p++;    // go to the next word after the ADC header


        // Decode ADC data block
        const int size=+h_adc->size-1;
        while( p<(((uint32*)h_adc)+size)){
          const ChannelInfo *h_ch=(ChannelInfo*)p;
          if (h_ch->spare!=0xFED){
            AddError(DaqError(DaqError::SADC_BAD_DATA, DaqError::SOURCE_ID,GetSourceID(),
                DaqError::SL_EVENT_N,event->GetHeader().GetEventNumberInBurst(),
                DaqError::EVENT_N,GetSLink().GetEventNumber()),opt);
            return;
          }
          p++;

          // fill digit
          Digit* digit = new Digit(DataID(GetSourceID(),h_adc->port,0,h_ch->channel), DetID(""));
          auto& samples = digit->GetSamples();

          for (int is=0;is<h_ch->samples/2;is++){
            const Data *d=(Data*)p;
            //check data
            if ((d->spare0!=2)||(d->spare1!=2)){
              AddError(DaqError(DaqError::SADC_BAD_DATA, DaqError::SOURCE_ID,GetSourceID(),
                  DaqError::SL_EVENT_N,event->GetHeader().GetEventNumberInBurst(),
                  DaqError::EVENT_N,GetSLink().GetEventNumber()),opt);
              return;
            }
            samples.push_back(d->sample0);
            samples.push_back(d->sample1);
            p++;
          }

          // and add to the list
          pre_digits.push_back(digit);
        }
        //end of the loop on the channels
    }

    // We must have at least 3 words for channel data samples.
    /*if (p + 3 > GetDataEnd()) {
      AddError(
          DaqError(DaqError::SADC_H_ADC_BAD_SIZE, DaqError::SOURCE_ID, GetSourceID(), DaqError::CHIP, h_adc->chip, DaqError::COUNTER, h_adc->size,
              DaqError::VALUE, 3), opt);
      return;
    }*/



    if( uint32(GetDataEnd()-p)!=0 )
        AddError(DaqError(DaqError::SADC_H_ADC_BAD_SIZE,DaqError::SOURCE_ID,GetSourceID(),DaqError::VALUE,4),opt);
}




////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::Decode(const Maps &maps,Digits &digits_list,DaqOption &opt)
{
    // Scan the chip data if it was not done yet.
    if( !is_scaned )
        Scan(opt);



    for( std::list<Digit*>::iterator it=pre_digits.begin(); it!=pre_digits.end(); it++ )
    {
        Chip::Maps::const_iterator c = maps.find((*it)->GetDataID());
        // no mapping for this data channel, skip
        if (maps.end() == c) continue;

        const Digit *digit_map = dynamic_cast<Digit*>(c->second);
        if( digit_map==NULL )
          throw Exception("ChipNA64WaveBoard:Decode(): Internal error");

        Digit *digit2 = new Digit(*digit_map);
        *digit2 = **it;
        digit2->SetDetID(digit_map->GetDetID());
        digit2->SetX(digit_map->GetX());
        digit2->SetY(digit_map->GetY());

        assert(digit2->GetChip()==digit_map->GetChip());
        assert(digit2->GetChannel()==digit_map->GetChannel());

        digits_list.insert(pair<DetID,Digit*>(digit2->GetDetID(),digit2));

    }
}



////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::Clear(void)
{
    Chip::Clear();
    for( std::list<Digit*>::iterator it=pre_digits.begin(); it!=pre_digits.end(); it++ )
        delete *it;
    pre_digits.clear();
}

////////////////////////////////////////////////////////////////////////////////

vector<float> ChipNA64WaveBoard::Digit::GetNtupleData(void) const
{
    vector<float> v;
    v.push_back(GetX());
    v.push_back(GetY());

    // Push samples after the (x,y) coordinate.
    // This is 'feature' has been requested for PHAST SADC digits export.
    for( vector<uint16>::const_iterator it=GetSamples().begin(); it!=GetSamples().end(); it++ )
        v.push_back(*it);

    return v;
}


////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::HeaderADC_v1::Print(const char *prefix) const
{
    printf("%s eventNum=%4.4d trigTime=%4.4d size=%.4d port=%2d\n",
            prefix,eventNum,trigTime,size,port);
}

void ChipNA64WaveBoard::HeaderADC_v2::Print(const char *prefix) const
{
    printf("%s size=%.4d port=%2d\n",
            prefix,size,port);
}

////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::Data::Print(const char *prefix) const
{
    printf("%s samples=(%d,%d)\n",prefix,sample0,sample1);
}


////////////////////////////////////////////////////////////////////////////////

ChipNA64WaveBoard::Map::Map(const ObjectXML &o) :
  Chip::Map(o),
  port(0),
  chip(0),
  channel(0),
  x(0),
  y(0)
{

    if( GetName()!=o.GetName() || GetName()!="ChipNA64WaveBoard" )
        throw Exception("ChipNA64WaveBoard::Map::Map(): Internal error.");

    istringstream s(dec_line.c_str());

    string name;

    if( !IsOption("xy") )
        throw Exception("ChipNA64WaveBoard::Map::Map(): option \"xy\" is mandatory!");


    switch( GetVersion() )
    {
    case 1:
    {
      s >> name >> source_id >> port >> channel >> x >> y;
      if (channel>=12){
        throw Exception("ChipNA64WaveBoard:::Map::Map(): bad channel %d for x=%d y=%d",channel,x,y);
      }
      chip = 0;   // we have one WB (aka one "chip") per port
      chanF=chanL=channel;
      chanS=1;
      wireF=wireL=channel;
      wireS=1;

      break;
    }
    default:
      throw Exception("ChipNA64WaveBoard:::Map::Map(): unknown version %d",GetVersion());
    }




    if( s.fail() )
        throw Exception("ChipNA64WaveBoard:::Map::Map(): bad format in line: %s",map_line.c_str());

    id=DetID(name);
    Check();
}



////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::Map::AddToMaps(Maps &maps,DaqOption &option) const
{
  const DataID data_id(GetSourceID(),port,chip,channel);

  Chip::Maps::const_iterator i = maps.find(data_id);

  if (maps.end() != i)
  {
      cout << "WARNING: duplicate mapping definition detected\n"
           << "New definition:\n";
      Print();

      cout << "Existing definition:\n";

      const ChipNA64WaveBoard::Digit* d = (ChipNA64WaveBoard::Digit*) i->second;

      //d->Print();
      cout << d->GetDetID() << " x=" << d->GetX() << " y=" << d->GetY() << " ";
      static_cast<ChipNA64WaveBoard::DataID>(d->GetDataID()).Print();

      Exception("ChipNA64WaveBoard::Map::AddToMaps(): map already exists").Print();
      cout << endl;
  }


  Digit *digit = new Digit(data_id,GetDetID());
  digit->SetX(x);
  digit->SetY(y);
  maps.insert( pair<DataID,Digit*>(data_id,digit));
  maps.IncWires(GetDetID());

  // Check the data format version number
  if( IsOption("data_format_2") )
    option.GetWaveBoardDataVer2().insert(GetSourceID());
   
}

////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::Map::Print(ostream &o,const string &prefix) const
{
  DaqMap::Print(o,prefix);
  o<< prefix << "  ";
  char s[300];
  sprintf(s,"detname=\"%s\" detnum=%d srcID=%d port=%d chip=%d channel=%d x=%d y=%d\n", id.GetName().c_str(),id.GetNumber(),GetSourceID(),port,chip,channel,x,y);
  o << s;
}




///////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::Digit::Print(ostream &o,const string &prefix) const
{
    Chip::Digit::Print(o,prefix);
    o << prefix << " x=" << x << "   y=" << y << "\n";

    static_cast<ChipNA64WaveBoard::DataID>(GetDataID()).Print(prefix);

    if( !samples.empty() )
    {
        o << prefix << "Samples:";
        for( vector<uint16>::const_iterator it=samples.begin(); it!=samples.end(); it++ )
            o << "   " << *it;
        o << "\n";
    }
}


////////////////////////////////////////////////////////////////////////////////

ChipNA64WaveBoard::ChipNA64WaveBoard(void const * const buf,bool copy_buf,DaqOption &opt,DaqEvent &ev)
:   Chip(buf,copy_buf,opt,ev)
{
    Clear();
}


////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::DataID::Print(const string &prefix) const
{
    printf("%ssrcID=%d port=%d chip=%d channel=%d\n",
            prefix.c_str(),
            unsigned(u.s.src_id),unsigned(u.s.port),unsigned(u.s.chip),unsigned(u.s.chan));
}

////////////////////////////////////////////////////////////////////////////////

void ChipNA64WaveBoard::ChannelInfo::Print(const char *prefix) const
{
    printf("%schannel=%d  samples=%d\n",prefix,channel,samples);
}



} // namespace
