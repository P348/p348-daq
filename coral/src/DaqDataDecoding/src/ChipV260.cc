#include "ChipV260.h"
#include "DaqEvent.h"

using namespace std;

namespace CS {

ChipV260::ChipV260(void const * const buf,bool copy_buf,DaqOption &opt,DaqEvent &ev)
:   Chip(buf,copy_buf,opt,ev)
{
    Clear();
}

void ChipV260::Clear(void)
{
    Chip::Clear();
    for (Digit* i : pre_digits) delete i;
    pre_digits.clear();
}

void ChipV260::Scan(DaqOption &opt)
{
    if (is_scaned) return;
    is_scaned = true;

    if (!pre_digits.empty())
      throw "ChipV260::Scan(): an attempt to call Scan() without Clear()!";

    //cout << "ChipV260::Scan()" << endl;
    //Dump();

    // payload data block
    const Format* data = (Format*) GetDataStart();

    // check spill number integrity between V260 and SLink data blocks
    if (data->header[2] != GetSLink().GetSpillNumber()) {
        AddError(DaqError(DaqError::UNKNOWN_TYPE,
                          DaqError::SOURCE_ID,GetSourceID(),
                          DaqError::COUNTER,data->header[2],
                          DaqError::COUNTER,GetSLink().GetSpillNumber()),
                 opt);
        return;
    }
    
    // create digits for all counters
    for (int i = 0; i < 16; ++i) {
      Digit* d = new Digit(DataID(GetSourceID(), i, 0), DetID(""));
      d->counter = data->counters[i];
      //d->Print();
      pre_digits.push_back(d);
    }
}

void ChipV260::Decode(const Maps &maps,Digits &digits_list,DaqOption &opt)
{
    // Scan the chip data if it was not done yet.
    if( !is_scaned )
        Scan(opt);

    //cout << "ChipV260::Decode()" << endl;
    
    for (const Digit* d : pre_digits) {
      //d->Print();
      
      const int ch = d->GetChannel();
      
      // search mapping
      // - first try for nwide=1
      Chip::Maps::const_iterator c = maps.find(DataID(GetSourceID(), ch, 1));
      if (maps.end() == c) {
        // - second try with nwide=2
        c = maps.find(DataID(GetSourceID(), ch, 2));
      }
      
      // no mapping, skip
      if (maps.end() == c) continue;
      
      
      const Digit* digit_map = dynamic_cast<Digit*>(c->second);
      if (!digit_map)
          throw Exception("ChipV260::Decode(): Internal error");
      
      Digit* digit2 = new Digit(*digit_map);
      
      digit2->counter = pre_digits[ch]->counter;
      if (digit2->GetNwide() == 2)
        digit2->counter += pre_digits[ch+1]->counter << 24;
      
      digits_list.insert(pair<DetID,Digit*>(digit2->GetDetID(),digit2));
    }
}

////////////////////////////////////////////////////////////////////////////////

ChipV260::Map::Map(const ObjectXML &o) :
  Chip::Map(o),
  channel(0),
  nwide(0)
{
    if (GetName() != o.GetName())
        throw Exception(("ChipV260::Map::Map(): name mismatch: chipname="+GetName()+" mapname="+o.GetName()).c_str());
    
    // 'chan' is real input channel of V260
    // 'wire' is logical channel, could be associated with 'nwide' of real 'chan' (following the chain capability)
    
    istringstream iss(dec_line.c_str());
    string det_name;
    iss >> det_name >> source_id >> channel >> nwide >> counter_name;
    
    chanF=chanL=channel;
    chanS=1;
    wireF=wireL=channel;
    wireS=1;
    
    // check parsing
    if (iss.fail())
        throw Exception("ChipV260::Map::Map(): bad format in line: %s",map_line.c_str());

    // check the channel range
    const int last = channel + nwide-1;
    const bool isOK =
      (0 <= channel) && (channel <= 15) &&
      (0 <= last) && (last <= 15) &&
      (1 <= nwide) && (nwide <= 2);
    if (!isOK) {
      throw Exception("Chip::Map::Check(): %s: channel or channel+nwide is out of range [0,15], or nwide is out of range [1,2]: %s",
                      GetName().c_str(),map_line.c_str());
    }
    
    id = DetID(det_name);
    
    //Print();
}



void ChipV260::Map::AddToMaps(Maps &maps,DaqOption &options) const
{
  const DataID data_id(GetSourceID(), channel, nwide);
  
  Chip::Maps::const_iterator i = maps.find(data_id);

  if (maps.end() != i) {
      cout << "WARNING: duplicate mapping definition detected\n"
           << "New definition:\n";
      Print();
      
      cout << "Existing definition:\n";
      
      const ChipV260::Digit* d = (ChipV260::Digit*) i->second;
      
      //d->Print();
      static_cast<ChipV260::DataID>(d->GetDataID()).Print();
      
      Exception("ChipV260::Map::AddToMaps(): map already exists").Print();
      cout << endl;
  }

  Digit* digit = new Digit(data_id,GetDetID());
  digit->name = counter_name;
  maps.insert( pair<DataID,Digit*>(data_id,digit));
  maps.IncWires(GetDetID());
}

void ChipV260::Map::Print(ostream &o,const string &prefix) const
{
  DaqMap::Print(o,prefix);
  o<< prefix << "  ";
  char s[300];
  sprintf(s,"detname=%s detnum=%d srcID=%d channel=%d nwide=%d\n",
          id.GetName().c_str(),id.GetNumber(),GetSourceID(),channel,nwide);
  o << s;
}

////////////////////////////////////////////////////////////////////////////////

void ChipV260::Digit::Print(ostream &o,const string &prefix) const
{
    Chip::Digit::Print(o,prefix);
    o << " ";

    static_cast<ChipV260::DataID>(GetDataID()).Print(o,prefix);

    o << prefix << " Counter=" << counter << " Name=" << name;
    o << "\n";
}

////////////////////////////////////////////////////////////////////////////////

void ChipV260::DataID::Print(std::ostream &o,const std::string &prefix) const
{
    o << prefix
      << "DataID=("
      << "srcID=" << GetSourceID()
      << " channel=" << GetChannel()
      << " nwide=" << unsigned(u.s.nwide)
      << ")";
}

} // namespace
