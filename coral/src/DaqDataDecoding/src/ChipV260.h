#pragma once

#include "Chip.h"

namespace CS {

////////////////////////////////////////////////////////////////////////////////

/*! \brief CAEN V260 counter card chip

    The card has 16 counting channels.
    The depth of each channel is 24 bits.
    Supports cascading capability to increase the depth.

    \author Anton Karneyeu
*/
class ChipV260 : public Chip
{
  //============================================================================
  // Types
  //============================================================================

  public:

    class DataID
    {
      public:
        DataID(const Chip::DataID &d) {u.data_id=d;}
        DataID(uint16 srcID,uint16 chan,uint16 nwide) {
          u.s.src_id=srcID;
          u.s.chan=chan;
          u.s.nwide=nwide;
          u.s.none=0;
        }
        operator Chip::DataID (void) const {return u.data_id;}
        
        uint16          GetSourceID             (void) const {return u.s.src_id;}
        void            SetSourceID             (uint16 s)   {u.s.src_id=s;}
        uint16          GetChannel              (void) const {return u.s.chan;}
        void            SetChannel              (uint16 s)   {u.s.chan=s;}
        union
        {
            Chip::DataID data_id;
            struct
            {
                Chip::DataID none:32,nwide:8,chan:8,src_id:16;
            } s;
        } u;
        void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;
    };

    class Digit: public Chip::Digit
    {
      public:

                        Digit                   (const DataID &data_id,const DetID &id)
                                                : Chip::Digit(data_id,id), counter(0)
                                                   {}

        void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

        uint16          GetSourceID             (void) const {return static_cast<ChipV260::DataID>(GetDataID()).GetSourceID();}
        
        uint16          GetChannel              (void) const {return static_cast<ChipV260::DataID>(GetDataID()).GetChannel();}
        uint16          GetNwide                (void) const {return static_cast<ChipV260::DataID>(GetDataID()).u.s.nwide;}

        uint64       counter;
        std::string  name;
    };

    class Map : public Chip::Map
    {
      public:
                        Map                     (const ObjectXML &o);
                        
        void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

        void            AddToMaps               (Maps &maps,DaqOption &option) const;

      public:

        int          channel;
        int          nwide;
        std::string  counter_name;
    };

    // Data format description
    struct Format
    {
        uint32  header[3];     /// header words: {time, 0, spill}
        uint32  counters[16];  /// 16 counters, each is 24 bits
    };

  public:
    
                        ChipV260                (void const * const buf,bool copy_buf,DaqOption &opt,DaqEvent &ev);
                       ~ChipV260                (void) {Clear();}
  
    void                Scan                    (DaqOption &opt);

    void                Decode                  (const Maps &maps,Digits &digits_list,DaqOption &opt);
    
  private:

    /// \return Chip name.
    std::string         GetName                 (void) const {return "ChipV260";}

    /// Clear the chip status (do \b not modify the real data buffer).
    void                Clear                   (void);

  private:
  
    std::vector<Digit*>   pre_digits;
};


} // namespace CS
