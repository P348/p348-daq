
#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <unistd.h>

#include "CollectErrs.h"

#include "monitor.h"

using CS::uint32;
using CS::DaqError;
using CS::DaqOption;
using CS::ObjectXML;
using CS::Chip;


bool Connect(const char *source){
  static char *table[6]={"All","no","PHY","yes",0};
 
  int a=monitorSetDataSource((char *)source);
  if (a==0) {
    monitorSetNowait();
    a=monitorDeclareMp("errorDumpAll");
    if (a==0) {
      a=monitorDeclareTable(&table[0]);  //We only want physics events
      if (a==0) {
        printf("Data source '%s' is connected.\n", source);
        return true;
      }
      else printf("Date error:%s \n",monitorDecodeError(a));
    } 
    else printf("Date error:%s \n",monitorDecodeError(a));
  } 
  else printf("Date error:%s \n",monitorDecodeError(a));
 
  return false;
}


int main(int argc,char **argv){
  setlinebuf(stdout);
  printf("errorDumpAll started with following (%d) parameters: \n", argc);
//  for(int i = 0; i < argc; i++)
//    printf("--- %d: %s\n", i, argv[i]);
  char *mfile=0;
  char *ofile=0;
  int nrevents=-1;
  int run_number = -1;
  bool qar=false;
  vector <string> datafiles;
  set <int> srcids;

  Chip::SetFillStatistics(false);


  bool se=false; 
  for (int a=1;a<argc;a++) {
    size_t al=strlen(argv[a]);
    char *ra=argv[a];
    char b='s';
    if (argv[a][0]=='-') {
      if (al<2) {se=true;break;}
      if (argv[a][1]=='q') {
        if (al==2) qar=true; else {se=true;break;}
        continue;
      }
      b=argv[a][1];
      if (al<=2) {
        a++;
        if (a>=argc) {se=true;break;}
        ra=argv[a];
      } else ra=argv[a]+2;
    }      
   
    if (b=='s') datafiles.push_back(ra);
    else if (b=='m') mfile=ra;
    else if (b=='o') ofile=ra;
    else if (b=='n') nrevents=atoi(ra);
    else if (b=='i') srcids.insert(atoi(ra));
    else if (b=='r') run_number=atoi(ra);
    else {se=true;break;}

  }
  

  se= se || datafiles.size()==0 || mfile==0;
  if (se) { 
    cout << "usage: errorDumpAll -s <data source> -m <mapping directory> [-o output file]" <<endl
	 << "[-n #events] [-i SrcIDs] [-q] [-r <run number>]" << endl
         << "-q means exit when run finished (for online)" << endl;
    return 0;
  }

  uint32 runn=0;
  CollectErrs *trash=new CollectErrs(mfile);
  trash->ResetAtNewRun(false);
  for (vector<string>::iterator fni=datafiles.begin();fni!=datafiles.end();fni++) {
    if (Connect(fni->c_str())) {
      char *ptr;
      int en=0;
      int st;
      uint32 waiting=0;
      bool brk=false;
      time_t start_time = time(NULL);
      bool is_selected_run = false;
      bool is_bad_run_number = false;
      if(run_number > 0)
        printf("start time: %s Waiting for run number %d...\n", ctime(&start_time),run_number);
      bool start = true;
      int nnn = 0;
      while ((en<nrevents || nrevents<0) && !brk) {
	st=monitorGetEventDynamic((void **)&ptr); //get raw event from DATE 
	if(nnn < 10)
	{
	    nnn++;
	    printf("nnn = %d, status = %d, ptr = %p\n", nnn, st, ptr);
	}
	if (0==st) {
	  if (0==ptr) {
	    usleep(100000); // 100 msec. sleep, prevent too high CPU usage when idle
	    time_t cur_time = time(NULL);
	    if(waiting%1000 == 0)
	    {
		printf("%u: Current time = %s, ptr = %p\n", waiting, ctime(&cur_time), ptr);
	    }
	    waiting++;
	    if(waiting>1000 && waiting < 1500)
	    {
		printf("Seems no connection. Try to reconnect...\n");
		if(!Connect(fni->c_str()))
		{
		    printf("Connection error.\n");
		    brk = true;
		}
		waiting += 1000;
		nnn = 0;
		continue;
	    }
	    if (cur_time - start_time > 600 && waiting > 6000) // 10 minutes
	    {
		printf("Timeout error. Current time = %s\n", ctime(&cur_time));
	        brk=true; //quit after 10 minutes waiting
	    }
	  }
	  else {
	    waiting = 0;
	    en++;
	    DaqEvent *evnt=new DaqEvent(ptr);
	    if (runn==0) runn=evnt->GetRunNumber();
	    if(start)
	    {
	      time_t cur_time = time(NULL);
	      start = false;
	      printf("First event of run %d is received at %s.\n", runn, ctime(&cur_time));
	    }
	    if(run_number < 0)
	    {
	      if (runn==evnt->GetRunNumber() || !qar) {
	        trash->DecodeEvent(evnt);
	        trash->HandleNewEvent(evnt);
	      } else brk=true;
	    }
	    else if(!is_selected_run && run_number > 0) // wait for specified run number
	    {
	      static int count = 0;
	      int drn = runn-run_number;
	      if(drn > 100000 || drn == -1) // it is, probably, still dry run or previous run. let's wait a little.
	      {
	         time_t cur_time = time(NULL);
	         if(cur_time - start_time > 120) // wait 2 minutes
	         {
	           brk = true;
	           printf("Run number %d waiting timeout. Program is stopping.\n", run_number);
	         }
	         else
	           usleep(100000);
	         runn = 0; // reread run number next event
	         if(count%10 == 0)
	            printf("... %d s\n", (int)(cur_time-start_time));
	         count++;
	      }
	      else if(drn != 0)
	      {
	        printf("Bad current run number %d, the run %d is expecting. Program stopping.\n", runn, run_number);
	        brk = true;
	        is_bad_run_number = true;
	      }
	      else // this is an expecting run number
	        is_selected_run = true;
	    }
	    if(is_selected_run)
	    {
	      if (runn==evnt->GetRunNumber()) {
	        trash->DecodeEvent(evnt);
	        trash->HandleNewEvent(evnt);
	      } else brk=true;
	    }
	    if((en%1000) == 0 || brk)
	      printf("Run %d, event %d\n", evnt->GetRunNumber(), en);
	    delete evnt;
	    free(ptr);
	  }  
	} else {
	  cerr << "Date error: " << monitorDecodeError(st);
	  brk=true;
	}
      }
    }
  }


  if (ofile) {
    ofstream out(ofile);
    if (out) {
      trash->PrintReport(out,srcids);
    }
  }
  else trash->PrintReport(cout,srcids);

  monitorLogout();  
}
