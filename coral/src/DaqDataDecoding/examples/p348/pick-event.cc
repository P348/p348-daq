// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// c++
#include <iostream>
#include <fstream>
using namespace std;

struct EventID {
  uint32 Run;
  uint32 Spill;
  uint32 Event;
};

ostream& operator<<(ostream& os, const EventID& id)
{
  os << id.Run << "-" << id.Spill << "-" << id.Event;
  return os;
}

istream& operator>>(istream& is, EventID& id)
{
  char sep;
  is >> id.Run >> sep >> id.Spill >> sep >> id.Event;
  
  if (!is) {
    id.Run = 0;
    id.Spill = 0;
    id.Event = 0;
  }
  
  return is;
}

bool operator<(const EventID& left, const EventID& right)
{
  if (left.Run != right.Run)
    return (left.Run < right.Run);
  
  if (left.Spill != right.Spill)
    return (left.Spill < right.Spill);
  
  return (left.Event < right.Event);
}

// convert string to value
template<typename T>
T cast(const string& s)
{
  istringstream iss(s);
  T x;
  iss >> x;
  return x;
}

int main(int argc, char *argv[])
{
  set<EventID> ids;
  string fout_name = "output.dat";
  DaqEventsManager manager;
  bool verbose = false;
  
  // parse command line
  for (int i = 1; i < argc; ++i) {
    const string param = argv[i];
    
    // input data file name
    if (param[0] != '-') {
      manager.AddDataSource(param);
      continue;
    }
    
    const string value = (i + 1 < argc) ? argv[i + 1] : "";
    
    if (param == "-e") ids.insert(cast<EventID>(value));
    if (param == "-o") fout_name = value;
    if (param == "-v") verbose = cast<bool>(value);
    i++;
  }
  
  // print usage
  if (ids.empty() || manager.GetDataSources().empty()) {
    cerr << "Usage: ./pick-event (options) (data files)\n"
         << "options:\n"
         << "  -e EventID   - event number in format 'run-spill-event', use several times to pick multiple events\n"
         << "  -o file_name - output file name (default: output.dat)\n"
         << "  -v level     - verbosity level (default: 0)\n"
         << "data files (list of input data files):\n"
         << "  file_name [file_name ...]"
         << endl;
    return 1;
  }
  
  // print input parameters
  cout << "EventIDs:";
  for (auto i = ids.begin(); i != ids.end(); ++i)
    cout << " " << *i;
  cout << " (total " << ids.size() << " events)" << endl;
  
  const vector<string>& files = manager.GetDataSources();
  cout << "Input files (total " << files.size() << " files):" << endl;
  for (size_t i = 0; i < files.size(); ++i)
    cout << "  " << files[i] << endl;
  
  cout << endl;
  
  // output file
  ofstream fout(fout_name);
  
  uint64_t totalSize = 0;
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 10000 == 1)
      cout << "===> Event #" << nevt << ", total size = " << (totalSize >> 20) << " Mb" << endl;
    
    const DaqEvent& event = manager.GetEvent();
    
    const EventID id = {event.GetRunNumber(), event.GetBurstNumber(), event.GetEventNumberInBurst()};
    totalSize += event.GetLength();
    
    if (verbose) {
      cout << "run=" << event.GetRunNumber()
           << " runevent=" << event.GetEventNumberInRun()
           << " spill=" << event.GetBurstNumber()
           << " spillevent=" << event.GetEventNumberInBurst()
           << " time=" << event.GetTimeStr()
           << " buffer=" << event.GetBuffer()
           << " size=" << event.GetLength()
           << endl;
    }
    
    if (ids.count(id) > 0) {
      cout << "Output event " << id
           << " | time = " << event.GetTimeStr()
           << ", event size = " << event.GetLength() << " bytes"
           << endl;
      fout.write((const char*) event.GetBuffer(), event.GetLength());
      fout.flush();
      ids.erase(id);
    }
    
    // events list is empty = all events are picked, search complete
    if (ids.empty()) break;
  }
  
  cout << "Event loop is complete." << endl;
  
  if (!ids.empty()) {
    cout << endl;
    cout << "List of missing EventIDs:";
    for (auto i = ids.begin(); i != ids.end(); ++i)
      cout << " " << *i;
    cout << " (total " << ids.size() << " events)" << endl;
    
    cout << "ERROR: some requested event IDs are missing!" << endl;
    return 1;
  }
  
  cout << "Output data file name: " << fout_name << endl;
  
  return 0;
}
