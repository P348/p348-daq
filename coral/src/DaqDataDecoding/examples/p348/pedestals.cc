#include "DaqEventsManager.h"
#include "DaqEvent.h"
#include "Chip.h"
#include "ChipSADC.h"
using namespace CS;

#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

// ROOT
#include "TFile.h"
#include "TH1D.h"
#include "TH2I.h"
#include "TF1.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TError.h"

void pedfit(TH2I* histo, const char* fname)
{
  gStyle->SetOptStat("emruo");
  gStyle->SetOptFit(111);
  gStyle->SetLineScalePS(1);
  gErrorIgnoreLevel = kWarning;
  
  TCanvas c("pedfit", "pedfit");
  c.SetGrid();
  c.SetLogy();
  c.Print(".pdf[");
  
  // output file
  ofstream fout(fname);
  
  fout << left
       << setw(26) << "# channel "
       << setw(10) << "maxpos "
       << setw(10) << "mean "
       << setw(10) << "stddev "
       << setw(10) << "fit_mean "
       << setw(11) << "fit_meane "
       << setw(10) << "fit_sigma "
       << setw(11) << "fit_sigmae "
       << setw(12) << "fit_chi2ndf "
       << setw(10) << "nentries "
       << setw(10) << "nevents "
       << endl;
  
  for (int k = 1; k <= histo->GetNbinsX(); ++k) {
    const TString name = histo->GetXaxis()->GetBinLabel(k);
    //cout << name << endl;
    
    TH1D* pr = histo->ProjectionY(name, k, k, "");
    pr->SetTitle(name);
    
    const int maxbin = pr->GetMaximumBin();
    const double maxpos = pr->GetXaxis()->GetBinCenter(maxbin);
    
    pr->Fit("gaus", "QW", "", maxpos-10, maxpos+5);
    TF1* ft = pr->GetFunction("gaus");
    
    fout << setprecision(5)
         << setw(25) << name << " "
         << setw(9) << maxpos << " "
         << setw(9) << pr->GetMean() << " "
         << setw(9) << pr->GetStdDev() << " "
         << setw(9) << ft->GetParameter(1) << " "
         << setprecision(3)
         << setw(10) << ft->GetParError(1) << " "
         << setprecision(5)
         << setw(9) << ft->GetParameter(2) << " "
         << setprecision(3)
         << setw(10) << ft->GetParError(2) << " "
         << setw(11) << ft->GetChisquare()/ft->GetNDF() << " "
         << setprecision(15)
         << setw(9) << pr->GetEntries() << " "
         << setw(0) << pr->GetEntries()/4       // TODO: use constant nsamples/2
         << endl;
    
    // print problematic channels
    const double maxerr = 1. / sqrt(pr->GetEntries());
    if ((ft->GetParError(1) > maxerr*ft->GetParameter(1)) ||
        (ft->GetParError(2) > maxerr*ft->GetParameter(2))) {
      cout << "WARNING: pedfit(): problematic fit:"
           << " name=" << name
           << " entries=" << pr->GetEntries()
           << " mean_err=" << ft->GetParError(1)
           << " sigma_err=" << ft->GetParError(2)
           << endl;
    }
    
    // plot fit
    ft->SetNpx(500);
    pr->SetAxisRange(maxpos-2*50, maxpos+2*50, "X");
    pr->Draw();
    c.Print(".pdf");
  }
  
  c.Print(".pdf]");
}

// test function to be used in ROOT session for fit debug
void runpedfit()
{
  TFile* f = new TFile("pedestals.root");
  TH2I* histo = (TH2I*) f->Get("pedestals");
  pedfit(histo, "pedestals.txt");
}


int main(int argc, char *argv[])
{
  if (argc < 4) {
    cerr << "Usage: ./pedestals.exe <file name with chunks list> <output histofile> <output textfile> [nevents]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../../../../../maps");
  
  std::ifstream file(argv[1]);
  while (file) {
    std::string line;
    std::getline(file, line);
    if (!line.empty()) 
        manager.AddDataSource(line);
  }
  
  // process only several first events
  if (argc == 5)
    manager.SetEventsMax(atoi(argv[4]));
  
  // print file read settings
  manager.Print();
  
  
  TFile *rFile = new TFile(argv[2], "RECREATE");
  
  // X axis: number of bins is set to the expected number of channels
  // Y axis: range shift by -0.5 to have integer bin centers
  TH2I* histo = new TH2I("pedestals", "sample values;channel;sample value, ADC counts", 500, 0, 500, 4100, 0-0.5, 4100-0.5);

  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
   
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    typedef Chip::Digits::const_iterator Digi_it;
    for (Digi_it i = manager.GetEventDigits().begin(); i != manager.GetEventDigits().end(); ++i ) {

      const Chip::Digit* d = i->second;

      const ChipSADC::Digit* sadc = dynamic_cast<const ChipSADC::Digit*>(d);
      if (sadc) {
         string channelName =   sadc->GetDetID().GetName() + "-" 
                              + to_string((long long int)sadc->GetX()) + "-" 
                              + to_string((long long int)sadc->GetY());
         
         string channelNameEven = channelName + ".pedestal0";
         string channelNameOdd = channelName + ".pedestal1";
         
         const vector<uint16>& wave = sadc->GetSamples();
         for (int j = 0; j < 8; ++j) { // TODO: 8 as a constant variable
             if (j % 2)
                histo->Fill(channelNameOdd.c_str(), wave[j], 1.);
             else
                histo->Fill(channelNameEven.c_str(), wave[j], 1.);
         }
         
      }

    } // EventDigits
  }
  
  cout << "End of the event loop, total events processed = " << manager.GetEventsCounter() << endl;
  
  histo->LabelsDeflate("X");
  histo->LabelsOption("a v", "X");
  
  pedfit(histo, argv[3]);
  
  histo->Write();
  rFile->Close();
  
  // print events read and decoding summary
  CS::Exception::PrintStatistics();
  CS::Chip::PrintStatistics();
  
  return 0;
}
