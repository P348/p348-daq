// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
#include "Chip.h"
//#include "ChipSADC.h"
#include "ChipAPV.h"
using namespace CS;

// c++
#include <iostream>
using namespace std;

// ROOT
#include "TTree.h"
#include "TFile.h"
#include "TInterpreter.h"
#include "TSystem.h"

int main(int argc, char *argv[])
{
  if (argc != 3) {
    cerr << "Usage: ./apv_dat2root input.dat output.root" << endl;
    return 1;
  }
  
  // setup read of input .dat file
  DaqEventsManager manager;
  manager.SetMapsDir("../../../../../maps");
  manager.AddDataSource(argv[1]);
  manager.Print();
  
  // magic to support vector of vectors in ROOT tree:
#if !defined(__CINT__)
  if (!(gInterpreter->IsLoaded("vector")))
    gInterpreter->ProcessLine("#include <vector>");
  gSystem->Exec("rm -f AutoDict*vector*vector*short*");
  gInterpreter->GenerateDictionary("vector<vector<short> >", "vector");
  //gInterpreter->GenerateDictionary("std::vector<std::vector<short> >", "vector");
#endif
  
  // prepare output .root file
  TFile f(argv[2], "RECREATE");
  
  ULong64_t evt;
  Int_t daqTimeSec;
  Int_t daqTimeMicroSec;
  vector<unsigned int>* srsChip = new vector<unsigned int>;
  vector<unsigned int>* srsChan = new vector<unsigned int>;
  vector<string>* mmChamber = new vector<string>;
  vector<vector<short> >* raw_q = new vector<vector<short> >;
  
  TTree t("apv_raw", "APVRawData");
  t.Branch("evt", &evt, "evt/l");
  t.Branch("daqTimeSec", &daqTimeSec, "daqTimeSec/I");
  t.Branch("daqTimeMicroSec", &daqTimeMicroSec, "daqTimeMicroSec/I");
  t.Branch("srsChip", &srsChip);
  t.Branch("srsChan", &srsChan);
  t.Branch("mmChamber", &mmChamber);
  t.Branch("raw_q", &raw_q);
  
  // event loop, read next event
  while (manager.ReadEvent()) {
    // print progress each 1000 events
    if (manager.GetEventsCounter() % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // print event header information
    //manager.GetEvent().GetHeader().Print();
    
    // skip auxialy (non-physics) events
    const DaqEvent::EventType type = manager.GetEvent().GetType();
    if (type != DaqEvent::PHYSICS_EVENT) continue;
    
    //int trigger = manager.GetEvent().GetTrigger();
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    //cout << "Decode: " << (decoded ? "OK" : "failed") << endl;
    
    // skip events with decoding problems
    if (!decoded) continue;
    
    // set tree variables
    evt = manager.GetEventsCounter();
    daqTimeSec = manager.GetEvent().GetTime().first;
    daqTimeMicroSec = manager.GetEvent().GetTime().second;
    srsChip->clear();
    srsChan->clear();
    mmChamber->clear();
    raw_q->clear();
    
    // print digits from all detectors
    //cout << "Event digits:" << endl;
    typedef Chip::Digits::const_iterator Digi_it;
    for (Digi_it i = manager.GetEventDigits().begin(); i != manager.GetEventDigits().end(); ++i ) {
      const Chip::Digit* d = i->second;
      //d->Print();
      
      // skip data from non-APV detectors
      const ChipAPV::Digit* apv = dynamic_cast<const ChipAPV::Digit*>(d);
      if (apv == 0) continue;
      
      // description of ChipAPV::Digit:
      // https://svnweb.cern.ch/trac/coral/browser/trunk/src/DaqDataDecoding/src/ChipAPV.h#L50
      
      //apv->Print();
      
      // APV readout have 3 samples
      const string& detector = apv->GetDetID().GetName();
      const unsigned int chip = apv->GetChip();
      const unsigned int channel = apv->GetChipChannel();
      const uint32* A = apv->GetAmplitude();
      
      // set tree variables
      srsChip->push_back(chip);
      srsChan->push_back(channel);
      mmChamber->push_back(detector);
      vector<short> chan_data(3);
      chan_data[0] = A[0];
      chan_data[1] = A[1];
      chan_data[2] = A[2];
      raw_q->push_back(chan_data);
      
      //cout << "detector=" << detector
      //     << " chip=" << chip
      //     << " channel=" << channel
      //     << " amplitude[]=" << A[0] << " " << A[1] << " " << A[2]
      //     << endl;
    }
    
    // fill tree
    t.Fill();
    
    // print event errors
    /*
    DaqErrors& e = manager.GetEvent().GetDaqErrors();
    cout << "Event errors: total = " << e.errors.size()
         << " problems(NO,WARN,ERR,CRIT) = "
         << e.errors_level[DaqError::OK].size() << ","
         << e.errors_level[DaqError::MINOR_PROBLEM].size() << ","
         << e.errors_level[DaqError::WARNING].size() << ","
         << e.errors_level[DaqError::SEVERE_PROBLEM].size() << ","
         << " actions(NO,GEO,SRC,EVENT) = "
         << e.errors_action[DaqError::Action::NOTHING].size() << ","
         << e.errors_action[DaqError::Action::DISCARD_EVENT_ON_GEOID].size() << ","
         << e.errors_action[DaqError::Action::DISCARD_EVENT_ON_SRCID].size() << ","
         << e.errors_action[DaqError::Action::DISCARD_EVENT].size()
         << endl;
    */
  }
  
  f.Write();
  
  cout << "End of the event loop" << endl;
  
  // print summary
  CS::Exception::PrintStatistics();
  CS::Chip::PrintStatistics();
  
  return 0;
}
