// coral options file for field-OFF reconstruction for alignment purposes.

// - By default, the file is valid for: 2017.
// - Alternative entries are provided for the other cases, commented out by
//  default and to be commented back in.
//   - For the data taking year:
//     - Almost everything is dealt w/ by a reference to a ``bare'' version of
//      the main options file for TraFDic reco, taken from the "./user"
//      directory, simplified and copied to this (alignment) directory:
//               "./trafdic.<year>.bare.opt". 
//     - Except for the list of detectors to be excluded from tracking...
//     - ...and few other year-dependent alignment specifics tunings.
// - The case of the detector table ("detectors.dat"), and corresponding dico,
//  is special: since what one has in view is to tune the table's parameter, an
//  explicit reference to the file containing the initial parameters is to be
//  given, instead of the traditional reference to a directory.
// - Specifics to field-OFF
//   - "trafdic.addendum.fieldoff.opt" disables tracking options incompatible w/
//    field-off.
//   - Single reconstruction zone, w/ extension starting upstream of target or
//    not. The former being more appropriate to already well-advanced alignment.
//     PR requirements tuned correspondingly.
//   - DetNameOff:
//     - The detector planes to be excluded for the purpose of alignment
//      have to be added to the, year- and period-dependent, list to be
//      excluded in any case (see relevant "../pkopt/trafdic.xxx.opt").
//     - They e.g., are the detector planes liable to upset the
//      magnets-off, momentum-less, tracking, e.g. downstream of absorbers.

// This file's options are therefore to be overriden (and better do this in a
// 3rd party file) by specifying:
//   I)  Input/ouput
//  II)  Year/period-dependent special settings:
//     i) "./trafdic.<year>.bare.opt" options file.
//    ii) Others...
// III)  detectors.dat and dico.
//  IV)  Settings specific to alignment:
//     i) Name of detectors to be excluded from tracking. Those to be excluded
//       for the purpose of alignment have to be added to the, period-dependent,
//       list of detectors to be excluded in any case. Typically, the former are
//       detectors liable to upset the field off, momentum-less, reco, like
//       those downstream of hadron absorbers.
//    ii) Criteria for selecting tracks to be written to output TTree (chi2
//       cut, ...)
//   iii) Extension of (always unique) zone: including beamTelescope or not. 

include ${CORAL}/src/alignment/trafdic.2017.bare.opt

//					   ***** HISTOGRAMS + ALIGN TREE
histograms home		traf.fieldoff.root
//					   ***** INPUT DATA...
Data job
Data file	/castor/cern.ch/compass/data/2017/raw/tmp/cdr11008-278250.raw

//					   ***** EVENT SELECTION
events to read 40000			// # of events to read
//events to skip 7
selection trigger mask e0		// Hexadecimal. And year-dependent: here = VI+VO+BT

//	********** OVERWRITE WHAT'S IN "../pkopt/trafdic.????.opt"... **********
// (Note: the reference to the "../pkopt/trafdic.????.opt" is embedded in the
// "./trafdic.????.bare.opt" referenced supra.)

//	 	 ...SETTINGS SPECIFIC to FIELD-OFF RECONSTRUCTION
include ${CORAL}/src/pkopt/trafdic.addendum.fieldoff.opt
//  	 	 	 	 	   ***** SINGLE GEOMETRICAL ZONE
// (Definition: extent depends upon whether one wants to include beamTelescope,
// and implication in terms of PR requirements)
define zone	0	0	// Clear list of zones
// - W/o beamTelescope
define zone	50	99999	from FI03 down
TraF	iPRpar	[  0-  9]	5   0   20	4  0  16	0 0	2 1
//TraF	iCut	[16]	0	// No VSAT enhancement
// - W/ beamTelescope
// (Let's make the PR requirements demanding enough that only tracks extending
// inboth the beamT and the spectrometer.) 
//define zone	-8000	99999	from FI01 down
//TraF	iPRpar	[  0-  9]	5   0   20	4  0  16	0 0	2 1
// - In any case, redundancy is high enough that we do not need to give more
//  weight to VSAT hits
TraF	iCut	[16]	0	// No VSAT enhancement
// - Activate dead zone
TraF	DZisActive	DC	GM


//	 	 ...SETTINGS SPECIFIC to ALIGNMENT
// DetNameOff:
//  I) Detectors excluded from standard tracking (year-, period-dependent).
// II) Detectors excluded from field-OFF alignment, because:
//  - difficult to align, given their crude ganularity and or their low
//   sampling rate (case H, MA, ST03***a, ...),
//  - liable to upset the tracking rather than contributes to it (case MB, MA
//   (and PB?), which are downstream an absorber that the momentum-less
//   reconstruction cannot take into account.
//  (Note that whether these are valid reasons for exclusion is debatable...)
//			(I)		(II)
TraF	DetNameOff			H	MA	MB	ST03***a	ST03***c 

//	 	 ...SETTINGS SPECIFIC to EARLIER STAGES OF ANALYSIS
// The following eases the reco in the context of an initially bad alignment.
// May be kept commented out in later passes of the alignment procedure.
// - Drift-like detectors as MWPC
//ST IS_MWPC
//MB IS_MWPC
//DW IS_MWPC
//DC IS_MWPC
//DR IS_MWPC
// - Si uncertainties: alignment of Sis is tricky, because of their sharp
//  precision, which translates tiny misalignments into huge chi2.
// => Enlarge artificially these uncertainties
TraF	dCut	[84]	.0025	// SI position uncertainties correction term (when badly aligned)	!!! N.B.: SPECTRO ONLY!!!
// - Special temporary setting for MPs
//TraF	dCut	[98]	.4	// Enlarge search road along v-axis (in cm, depending upon v-alignment quality)

//	 	 ...obsolete
// Loose-proj. option "TraF dCut[22]" does not exist any more as such.


// 					   ***** DETECTOR TABLE...
// (Not necessarily the one retrieved from the official directory by automatic
// search, which may not be the most appropriate starting point.)
//detector	table	$COMPASS_FILES/geometry/2017/
detector table	$COMPASS_FILES/geometry/2017/detectors.275589.OFF.dat


//    ********** OPTIONS SPECIFIC to the FILLING the ALIGNMENT TTree **********
main do alignment	// Book/fill alignment TTree
main magnets	off	// (on|off) default is off
// Upper limit on the chi2/NDF of selected tracks:
//  One has to make a trade-off between too loose a selection cut that would let
// through ghost tracks that might upset the minimisation and too tight a one
// that might bias the alignment by restricting it to the sub-region of phase
// space where the initial alignment is already good enough.
main chi2 cut	30	// Cut on chi2/ndf
main require	cop	// Keep only track with momentum

//end
