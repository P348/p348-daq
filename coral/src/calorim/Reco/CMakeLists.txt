#
# Library Reco
#
file (GLOB SRC "src/*.cc" "src/fortran_reco/*.F")
if(NOT Qt4_FOUND AND NOT Qt5_FOUND)
     file (GLOB GUI_SRC "src/GUIBaseCalorimeter.cc" "src/GUICalorimeterMove.cc")
     list(REMOVE_ITEM SRC ${GUI_SRC})
endif()

include_directories(${CMAKE_CURRENT_BINARY_DIR})

#
# Prepare Qt Wrappers
#
if(NOT CORAL_DISABLE_QT)
if(Qt4_FOUND)

    ##### Prepare Headers and files for GUIBaseCalorimeter
    qt4_wrap_cpp(GUIBaseCalorimeter_HEADERS_MOC "${CMAKE_CURRENT_SOURCE_DIR}/src/GUIBaseCalorimeter.h")
    qt4_wrap_ui(GUIBaseCalorimeter_FORMS_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/src/GUIBaseCalorimeter.ui")
    add_custom_target(GUIBaseCalorimeter_Wrappers DEPENDS ${GUIBaseCalorimeter_HEADERS_MOC} ${GUIBaseCalorimeter_FORMS_HEADERS})

    ##### Prepare Headers and files for GUICalorimeterMove
    qt4_wrap_cpp(GUICalorimeterMove_HEADERS_MOC "${CMAKE_CURRENT_SOURCE_DIR}/src/GUICalorimeterMove.h")
    qt4_wrap_ui(GUICalorimeterMove_FORMS_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/src/GUICalorimeterMove.ui")
    add_custom_target(GUICalorimeterMove_Wrappers DEPENDS ${GUICalorimeterMove_HEADERS_MOC} ${GUICalorimeterMove_FORMS_HEADERS})

else()

    ##### Prepare Headers and files for GUIBaseCalorimeter
    qt5_wrap_cpp(GUIBaseCalorimeter_HEADERS_MOC "${CMAKE_CURRENT_SOURCE_DIR}/src/GUIBaseCalorimeter.h")
    qt5_wrap_ui(GUIBaseCalorimeter_FORMS_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/src/GUIBaseCalorimeter.ui")
    add_custom_target(GUIBaseCalorimeter_Wrappers DEPENDS ${GUIBaseCalorimeter_HEADERS_MOC} ${GUIBaseCalorimeter_FORMS_HEADERS})

    ##### Prepare Headers and files for GUICalorimeterMove
    qt5_wrap_cpp(GUICalorimeterMove_HEADERS_MOC "${CMAKE_CURRENT_SOURCE_DIR}/src/GUICalorimeterMove.h")
    qt5_wrap_ui(GUICalorimeterMove_FORMS_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/src/GUICalorimeterMove.ui")
    add_custom_target(GUICalorimeterMove_Wrappers DEPENDS ${GUICalorimeterMove_HEADERS_MOC} ${GUICalorimeterMove_FORMS_HEADERS})

endif()

#
# Main Interface
#
EXECUTE_PROCESS (COMMAND unlink ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUIBaseCalorimeter_moc.cpp ERROR_QUIET)
EXECUTE_PROCESS (COMMAND ln -s ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/moc_GUIBaseCalorimeter.cpp ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUIBaseCalorimeter_moc.cpp ERROR_QUIET)
EXECUTE_PROCESS (COMMAND unlink ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUIBaseCalorimeter_uic.h ERROR_QUIET)
EXECUTE_PROCESS (COMMAND ln -s ${CMAKE_BINARY_DIR}/src/calorim/Reco/ui_GUIBaseCalorimeter.h ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUIBaseCalorimeter_uic.h ERROR_QUIET)
install(CODE "execute_process(COMMAND cp ${CMAKE_BINARY_DIR}/src/calorim/Reco/ui_GUIBaseCalorimeter.h ./include)")

EXECUTE_PROCESS (COMMAND unlink ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUICalorimeterMove_moc.cpp ERROR_QUIET)
EXECUTE_PROCESS (COMMAND ln -s ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/moc_GUICalorimeterMove.cpp ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUICalorimeterMove_moc.cpp ERROR_QUIET)
EXECUTE_PROCESS (COMMAND unlink ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUICalorimeterMove_uic.h ERROR_QUIET)
EXECUTE_PROCESS (COMMAND ln -s ${CMAKE_BINARY_DIR}/src/calorim/Reco/ui_GUICalorimeterMove.h ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/GUICalorimeterMove_uic.h ERROR_QUIET)
install(CODE "execute_process(COMMAND cp ${CMAKE_BINARY_DIR}/src/calorim/Reco/ui_GUICalorimeterMove.h ./include)")

EXECUTE_PROCESS (COMMAND ln -s ${CMAKE_BINARY_DIR}/src/calorim/Reco/src/*.h ${CMAKE_BINARY_DIR}/src/calorim/Reco ERROR_QUIET)

add_library(Reco ${SRC} ${GUICalorimeterMove_HEADERS_MOC} ${GUICalorimeterMove_FORMS_HEADERS} ${GUIBaseCalorimeter_HEADERS_MOC} ${GUIBaseCalorimeter_FORMS_HEADERS})
if(Qt4_FOUND OR Qt5_FOUND)
	add_dependencies(Reco GUICalorimeterMove_Wrappers GUIBaseCalorimeter_Wrappers)
endif()

else()
	add_library(Reco ${SRC})
endif()

#
# Install headers
#
file(GLOB RECO_INC ${CMAKE_CURRENT_SOURCE_DIR}/src/*.h)
install(FILES ${RECO_INC} DESTINATION include/Reco)
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/src/fortran_reco DESTINATION ./include/Reco/fortran_reco)
