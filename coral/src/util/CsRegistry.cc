/*!
   \file    CsRegistry.cc
   \brief   Compass Registry Class
   \author  Benigno Gobbo 
*/

#include "CsRegistry.h"

#include "CsRegistrySing.h"

CsRegistry::CsRegistry() {
}

bool CsRegistry::EOJRegistration( CsEndOfJob* ptr ) {
  CsRegistrySing* reg = CsRegistrySing::Instance();
  bool status = reg->EOJRegistration( ptr );
  return status;
}

bool CsRegistry::EOERegistration( CsEndOfEvent* ptr ) {
  CsRegistrySing* reg = CsRegistrySing::Instance();
  bool status = reg->EOERegistration( ptr );
  return status;
}

bool CsRegistry::SORRegistration( CsStartOfRun* ptr ) {
  CsRegistrySing* reg = CsRegistrySing::Instance();
  bool status = reg->SORRegistration( ptr );
  return status;
}





