/*!
   \file    CsGauss.h
   \brief   Gaussian-Distrubuted random number generator
   \author  Benigno Gobbo 
*/

/*! \class CsGauss CsGauss.h
  \brief Gaussian-Distrubuted random number generator.

  This is a C++ porting of the MATHLIB RG32 function.
 */

class CsGauss {

public:

  //! creator
  inline CsGauss() { seed_ = 875949887; }

  //! set feed
  inline void setSeed( int seed ) { seed_ = seed; }

  //! get feed
  inline int  getSeed() { return( seed_ ); }

  //! get random number
  inline float random() {
    const float c = 1.1920928955078e-07;
    const int    m = 0x7fffffff; int j = 0;
    for( int i=0; i<12; i++ ){
      seed_ = (seed_ * 69069) & m;
      j = j + seed_/256;
    }
    return( c * ( ( j + 134 ) / 256 * 256 ) - 6.0 );
  }

private:

  int seed_;

};

