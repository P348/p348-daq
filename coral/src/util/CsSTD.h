/*!
   \file    CsSTD.h
   \brief   Compass STD include file
   Used to include, in a correct way, the STD include files.

   \author  Benigno Gobbo
*/

#ifndef CsSTD_h
#define CsSTD_h

#include "coral_config.h"

#ifdef __HP_aCC
#   include <iostream.h>
#   include <fstream.h>
#   include <iomanip.h>
#   include <strstream.h>
#else
#   include <iostream>
#   include <fstream>
#   include <iomanip>
#   include <sstream>
#endif // __HP_aCC

#include <string>
#include <list>
#include <vector>
#include <map>

#endif // CsSTD_h
