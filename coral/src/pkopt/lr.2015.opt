//------------------------------
//
//   LR (Left/Right ambiguities) for Drift Detectors in 2008
//
//------------------------------

// Copied from "../lr.2012.opt,13610"

LR	detector association	// Execute detector association requested. Cluster associated delayed until otherwise requested.
//LR	make histograms		// Book and fill L/R histograms

//--------------------------------------
//
// DETECTOR ASSOCIATION TABLE. 
// FORMAT : TBNAME0 TBNAME1  ASSOCIATION_CUT(mm)  LRPROB_CUT LRMODE
//
// LRProbCut is supposed to be in [0,0.5]
// drift-like clusters for which LRProb < LRProbCut can be removed
//
// LRMODE : optional parameter 
//		nothing - 	Target pointing
//		0 - 		Target pointing; 
//		1 - 		Cell overlap (i.e. infinity pointing); 
// 		other - 	Nothing;
//-----------------------------

// DRIFT CHAMBERS
LR associate  DC00X1__   DC00X2__  8.0	  0.5 	1
LR associate  DC00Y1__   DC00Y2__  8.0    0.5   1
LR associate  DC00U1__   DC00U2__  8.0    0.5   1
LR associate  DC00V1__   DC00V2__  8.0    0.5   1

LR associate  DC01X1__   DC01X2__  8.0	  0.5 	1
LR associate  DC01Y1__   DC01Y2__  8.0    0.5   1
LR associate  DC01U1__   DC01U2__  8.0    0.5   1
LR associate  DC01V1__   DC01V2__  8.0    0.5   1

LR associate  DC04X1__   DC04X2__  8.0	  0.5 	1
LR associate  DC04Y1__   DC04Y2__  8.0    0.5   1
LR associate  DC04U1__   DC04U2__  8.0    0.5   1
LR associate  DC04V1__   DC04V2__  8.0    0.5   1

LR associate  DC05X1__   DC05X2__  8.0	  0.5 	1
LR associate  DC05Y1__   DC05Y2__  8.0    0.5   1
LR associate  DC05U1__   DC05U2__  8.0    0.5   1
LR associate  DC05V1__   DC05V2__  8.0    0.5   1

// WD (association cut set a priori = 12, i.e. somewhat larger than pitch. Y.B)
LR associate  DR01X1__   DR01X2__ 12.0    0.5   0
LR associate  DR02X1__   DR02X2__ 12.0    0.5   0
LR associate  DR01Y1__   DR01Y2__ 12.0    0.5   0
LR associate  DR02Y1__   DR02Y2__ 12.0    0.5   0

// DW (association cut set a priori = 45, i.e. somewhat larger than pitch. Y.B)
LR associate  DW01X1__   DW01X2__ 45.0	  0.5 	0
LR associate  DW01Y1__   DW01Y2__ 45.0	  0.5 	0
LR associate  DW02X1__   DW02X2__ 45.0	  0.5 	0
LR associate  DW02Y1__   DW02Y2__ 45.0	  0.5 	0
LR associate  DW03Y1__   DW03Y2__ 45.0	  0.5 	0
LR associate  DW03V1__   DW03V2__ 45.0	  0.5 	0
LR associate  DW04Y1__   DW04Y2__ 45.0	  0.5 	0
LR associate  DW04U1__   DW04U2__ 45.0	  0.5 	0
LR associate  DW05X1__   DW05X2__ 45.0	  0.5 	0
LR associate  DW05V1__   DW05V2__ 45.0	  0.5 	0
LR associate  DW06X1__   DW06X2__ 45.0	  0.5 	0
LR associate  DW06U1__   DW06U2__ 45.0	  0.5 	0

// 6mm STRAW TUBES
LR associate  ST03X1ub   ST03X1db  7.0    0.5   1
LR associate  ST03Y1ub   ST03Y1db  7.0    0.5   1
LR associate  ST03U1ub   ST03U1db  7.0    0.5   1
LR associate  ST03X2ub   ST03X2db  7.0    0.5   1
LR associate  ST03Y2ub   ST03Y2db  7.0    0.5   1
LR associate  ST03V1ub   ST03V1db  7.0    0.5   1
LR associate  ST05X1ub   ST05X1db  7.0    0.5   1
LR associate  ST05Y2ub   ST05Y2db  7.0    0.5   1
LR associate  ST05U1ub   ST05U1db  7.0    0.5   1

// 10mm
LR associate  ST03X1ua   ST03X1da 11.0    0.5   0
LR associate  ST03Y1ua   ST03Y1da 11.0    0.5   0
LR associate  ST03U1ua   ST03U1da 11.0    0.5   0
LR associate  ST03X2ua   ST03X2da 11.0    0.5   0
LR associate  ST03Y2ua   ST03Y2da 11.0    0.5   0
LR associate  ST03V1ua   ST03V1da 11.0    0.5   0
LR associate  ST05X1ua   ST05X1da 11.0    0.5   0
LR associate  ST05Y2ua   ST05Y2da 11.0    0.5   0
LR associate  ST05U1ua   ST05U1da 11.0    0.5   0

LR associate  ST03X1uc   ST03X1dc 11.0    0.5   0
LR associate  ST03Y1uc   ST03Y1dc 11.0    0.5   0
LR associate  ST03U1uc   ST03U1dc 11.0    0.5   0
LR associate  ST03X2uc   ST03X2dc 11.0    0.5   0
LR associate  ST03Y2uc   ST03Y2dc 11.0    0.5   0
LR associate  ST03V1uc   ST03V1dc 11.0    0.5   0
LR associate  ST05X1uc   ST05X1dc 11.0    0.5   0
LR associate  ST05Y2uc   ST05Y2dc 11.0    0.5   0
LR associate  ST05U1uc   ST05U1dc 11.0    0.5   0

//=== DRIFT TUBES ===
//------  Large parts ----
LR associate  MB01X1ub   MB01X1db 35.0    0.5   1
LR associate  MB01Y1ur   MB01Y1dr 35.0    0.5   1
LR associate  MB01V1ub   MB01V1db 35.0    0.5   1

LR associate  MB02X1ub   MB02X1db 35.0    0.5   1
LR associate  MB02Y1ur   MB02Y1dr 35.0    0.5   1
LR associate  MB02V1ub   MB02V1db 35.0    0.5   1

LR associate  MB02X2ub   MB02X2db 35.0    0.5   1
LR associate  MB02Y2ur   MB02Y2dr 35.0    0.5   1
LR associate  MB02V2ub   MB02V2db 35.0    0.5   1

//------- Small parts ----------
LR associate  MB01X1uc   MB01X1dc 35.0    0.5   1
LR associate  MB01Y1ul   MB01Y1dl 35.0    0.5   1
LR associate  MB01V1uc   MB01V1dc 35.0    0.5   1

LR associate  MB02X1uc   MB02X1dc 35.0    0.5   1
LR associate  MB02Y1ul   MB02Y1dl 35.0    0.5   1
LR associate  MB02V1uc   MB02V1dc 35.0    0.5   1

LR associate  MB02X2uc   MB02X2dc 35.0    0.5   1
LR associate  MB02Y2ul   MB02Y2dl 35.0    0.5   1
LR associate  MB02V2uc   MB02V2dc 35.0    0.5   1

// end
