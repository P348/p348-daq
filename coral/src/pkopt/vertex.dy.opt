// Vertex options for DY data, both 2015 and 2018

// Based on old "vertex.2002.opt", w/:
// - Updates concerning all data taking years.
// - Updates specific to hadron data
//  - No scatttered muon precedence.
// - W/ updates specific to DY, as implemented in #967c55b version of
//  "trafdic.201(5|8).opt.c", viz.
//  - Specific settings of Reference Planes.
//  - Strict time cut (6 ns) not implemented.
//  - Special DY options for BPV.
//  - No refit/retrack at the pattern recognition stage, where no good
//   determination of the BestprimaryVertex is available. Retracking at the
//   fitting stage.

// Originally excerpted from "trafdic.2018.opt.c,v#967c55b".

vertex pattern method averaging
vertex fitting method kalman
include ${CORAL}/src/pkopt/vertex.2002.opt
//                   Overwrite what's set in "vertex.opt"
CsAverPattern  Hist	1	// 0 - Off, 1 - ON
CsAverPattern  findSec	1	// 0 - Off, 1 - ON
CsAverPattern  Print [ 0 ]	0	// Prefilter info.
CsAverPattern  Print [ 1 ]	0	// Error info.
// Track is considered as accepted if its momentum is smaller
// than "AcceptTill" percents of beam track
CsAverPattern	AcceptTill	105	// In %
CsAverPattern	TimePrimCut	6 	// Time diff between track and beam, in sigmas
// 	 	 	Z Dist 	DCA(number of sigmas)
CsAverPattern	CUTS	1600	10
CsKalmanFitting Specials	0	// 0 - Off, 1 -  mu' takes precedence
CsAverPattern	Refit		0	// 0 - Off, 1 - ON
CsAverPattern	Retrack		0	// 0 - Off, !0 = cut on event time
CsKalmanFitting	Hist		1	// 0 - Off, 1 - ON
CsKalmanFitting	Print [ 0 ]	0	// Kalman filter info.
CsKalmanFitting	Print [ 1 ]	0	// Global fit info.
CsKalmanFitting	Print [ 2 ]	0	// IKF info.
CsKalmanFitting	RefPlane	1350	// in mm
CsKalmanFitting	RefBeam 	-8000	// in mm
CsKalmanFitting	RefMargin	500	// in mm
// Chi2 increment cuts:		DirChi2Trk	InvChi2Trk
CsKalmanFitting	CUTS		7		17
// Vertex Chi2 cut:
CsKalmanFitting	Chi2VCut	15
CsKalmanFitting	Retrack		2	// 0 - Off, !0 = cut on event time
CsKalmanFitting BeamP0		190	// Beam reference momentum (for histo purposes)

CsKalmanFitting	BpVDY		1	// Special BestPrimaryVertex for DY	
CsKalmanFitting	BpVTimeW	1	// Weight of timing in BestPrimaryVertex for DY
