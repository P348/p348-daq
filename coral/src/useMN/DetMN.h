#ifndef DetMN_h
#define DetMN_h

#include <iostream>
#include <string>
#include <vector>


#include "StatInfoMN.h"
#include "DetCalibDB.h"

namespace MN {

class DigSADC;
class ManagerCellsShapeTableSADC;

////////////////////////////////////////////////////////////////////////////////

class DetMN
{
  public:
    DetMN ( const std::string &name, size_t ncells );

  public:
   // Detector 
    void   Clear ( void );

   // Detector name
    const std::string & GetNameMN                 (void) const { return name_;}

   // Cells name
    size_t               NCellsMN                  (void) const { return ncells_mn_;}
    void                SetNCellsMN               ( const size_t &ncells_mn ) {ncells_mn_=ncells_mn;}
    const std::string &GetCellNameMN ( int icell ) const;
    void    SetCellNames ( const std::vector<std::string> &cell_names );
    virtual int FindCellByNameMN (const std::string &cell_name) const;

   // XY structure initialization and usage
    void InitXY( int nx, int ny );
    bool XYRegularGridMN( void ) const { return (map_cell_xy_.size() > 0);}
    void SetCellXY( int ic, int x, int y);
    int  GetCellOfColumnRowMN     (int x, int y) const;
    int  GetColumnOfCell(int icell) const;
    int  GetRowOfCell(int icell) const;
    /// \return number of columns
    int                     GetNColumns            ( void ) const { return fNcols_; }
    /// \return number of rows
    int                     GetNRows               ( void ) const { return fNrows_; }

   // Calibrations usage
    void     AddCalib3( const MN::AnyID &calib_id, const std::vector<MN::StatInfoMN >&vcf );
    bool      SetCellCalib3( const  MN::AnyID &calib_id, size_t &icell, const MN::StatInfoMN &cf );
    MN::DetCalibDB3                     &GetCalibDB3 (void ) { return det_calib_db3_;}  
    const MN::DetCalibDB3               &GetCalibDB3c (void )const  { return det_calib_db3_;}
    const MN::StatInfoMN GetCellCalib3( const MN::AnyID &calib_id, size_t &icell) const;  
    virtual double GetTimeCalibrated ( int icell, double time ) const;
    virtual double GetEnergyCalibrated ( int icell, double amp ) const;

   // SADC usage
    int GetNDigSADC ( void ) const;
    const std::vector<MN::DigSADC*>    &GetDigSADC ( void ) const { return myDigSADC_;}
    std::vector<MN::DigSADC*>           &GetDigSADC2Modify ( void ){ return myDigSADC_;}
    MN::DigSADC*    GetDigSADC2Modify ( int icell );
    const MN::DigSADC*    GetDigSADC ( int icell ) const;

   // Decode Calibrations Input
    void     ReadCalibrationByTags(const std::string &tag, const std::string &s);
    int      InputDetTimeCalibInfoXY( const MN::AnyID &calib_tag, const std::string &s);
    int      InputDetTimeCalibInfo(const MN::AnyID &calib_tag, const std::string &s);
    int      InputDetCalibInfo(const MN::AnyID &calib_tag, const std::string &s );
//    virtual int InputShapeTableSADC(const std::string &s);
    int InputCellShapeTableSADC( int icell, const std::string &s);
    virtual int InputSADCInfo( const std::string &s);
    virtual int InputSADCInfoXY( const std::string &s);
    void CheckCellShapeTableSADC( void );
    MN::ManagerCellsShapeTableSADC * GetManagerCellsShapeTableSADC ( void ) { return mantab_cells_sadc_;}

// Complete calibration procedure
    void     SetAutoDecodeVersion( void ); 
    void     CheckCalibrations( void ) const; 

  private:

  // ---------------------------------------------------------------------------
  // Attributes, data
  // ---------------------------------------------------------------------------
   std::string name_;
   size_t ncells_mn_;
   MN::DetCalibDB3                     det_calib_db3_;
  protected:
    std::vector<MN::DigSADC*>    myDigSADC_;
    std::vector<std::string>     cell_names_;
    MN::ManagerCellsShapeTableSADC *mantab_cells_sadc_;
    /// Dets XY structure
    std::vector<int>                        map_cell_xy_;
    std::vector<int>                        map_xy_cell_[2];
    int fNcols_; 
    int fNrows_;
    double fXStep_; 
    double fYStep_; 
};

////////////////////////////////////////////////////////////////////////////////

} // namespace MN

#endif  //         DetMN_h
