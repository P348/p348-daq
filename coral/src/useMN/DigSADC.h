#ifndef DigSADC_h
#define DigSADC_h

#include <sstream>
#include <map>
#include <vector>
#include <iostream>
#include <math.h>

// #include "MN_config.h"
#include "CsTypes.h"

namespace MN {

////////////////////////////////////////////////////////////////////////////////

// class ManagerCellsShapeTableSADC
// {
//   private:
//     class ProfileMaps 
//      {
//         public:
//            ProfileMaps(  std::map <double,double> * pmap,  std::map <double,std::pair<double,double > > *  wmap ) : pmap_(pmap), wmap_(wmap) { }
// 
//         public:
//           std::map <double,double> * pmap_;
//           std::map <double,std::pair<double,double > > * wmap_;
//      };
// 
//   public:
// //     ManagerCellsShapeTableSADC( Det *calo ) : calorimeter_(calo) { stepX_=(Xmax-Xmin)/NXbins;}
//      ManagerCellsShapeTableSADC( void ) {}
//      int           InputShapeTableSADC ( const std::string &s );
//      int           InputCellShapeTableSADC ( const std::string &s );
//      bool        CheckTables( void ) const;
//      double    Value( int cell, double x ) const;
//      std::pair<double, double>    WValue( int cell, double x ) const;
//      const std::map<double,double> *  GetTab( int cell ) const;
// 
//   private:
//      bool        AddTable( int cell , const std::vector <int> &profile );
//      const std::map <double,std::pair<double,double > > *  GetWTab( int cell ) const;
// //     const  ProfileMaps &GetTabs( double hfw ) const;
// 
//      double     ValueTab( double x, const std::map<double,double> * tab ) const;
//      std::pair<double,double> WValueTab( double v, const std::map<double, std::pair<double,double> > * tab ) const;
// 
//   private:
//      double      stepX_;
//      double      Xmin_;
//      double      Xmax_;
//      int         NXbins_;
// 
//      std::map <int,  ProfileMaps   > vtab_;
// //     Det                        *calorimeter_;
// };

// Delaem prosto. Slozhno budem delat POTOM
class ProfileMap 
{
   public:
      ProfileMap(  std::map <double,double> * pmap ) : stat_(0.), fwhm_(0.), mprfw_(0.), pmap_(pmap) {}
      double GetStat ( void ) const { return stat_;}
      double GetFWHM ( void ) const { return fwhm_;}
      double GetMPRFW ( void ) const { return mprfw_;}
      const std::map <double,double> * GetTab( void ) const { return pmap_;}

      void SetStat (double stat ) { stat_=stat;}
      void SetFWHM (double fwhm ) { fwhm_=fwhm;}
      void SetMPRFW (double mprfw ) { mprfw_=mprfw;}

   public:
     double stat_;
     double fwhm_;
     double mprfw_;
     std::map <double,double> * pmap_;
}; 

class ManagerCellsShapeTableSADC
{
  public:
     ManagerCellsShapeTableSADC( void ) : stepX_(0.),Xmin_(0.),Xmax_(0.),NXbins_(0) {}

     int         InputCellShapeTableSADC ( const std::string &s );
     bool        CheckTables( void ) const;

     bool        AddTable( int cell , const std::vector <int> &profile, double entries, double fwhm, double mprfw );
     double     Value( int cell, double x ) const;
     const ProfileMap *  GetProfile( int cell ) const;
     const std::map <double,double> *  GetTab( int cell ) const;
  private:
     double ValueTab( double v, const std::map<double,double> * tab ) const;
  private:
     double      stepX_;
     double      Xmin_;
     double      Xmax_;
     int         NXbins_;
     std::map <int, ProfileMap *    > vtab_;
};

// More and more development I need. Yes . At some point it may replace DigitizerSADCBase. Some ideas of DigitizerSADCN I will drop.
class DigSADC
{
  public:
   enum TypeDigSADC   { COMPASS_SADC=0, COMPASS_MSADC=1};
   class FitPar
   {
     public:
        FitPar  ( double a, double sa, double t, double st, int maxp, int maxg ) : signal(a), ssignal(sa), time(t), stime(st), time2(1000.), stime2(0.), hi2(0.), ndf(0), max_position(maxp), maxgate(maxg) {}
        void SetTime2 ( double t ) { time2 = t;}
        double GetTime2 ( void ) const { return time2;}
     public:
       double  signal;
       double  ssignal;
       double  time;
       double  stime;
       double  time2;
       double  stime2;
       double  hi2;
       int     ndf; 
       int     max_position;
       int     maxgate; 
   };
  protected:
   const double                     sadc_clock_      = 12.86;
   const double                     TCS_T0             = 40.;
  public:
    DigSADC ( const int  &type );
  public:

    int  GetType     ( void ) const { return type_; }
    void SetTab( const ProfileMap *tab) { tab_=tab;}
    const ProfileMap * GetTab( void ) const { return tab_;}

    double  GetTabValue( double x ) const;
    void SetDecodeVersion ( int decode_version ) { decode_version_=decode_version;}
    int GetDecodeVersion ( void ) const { return decode_version_;}
// Just a way to fool myself and have garanti that read calibrations from the file
    double GetTrueCalibPed( void ) const { return calib_true_[2];}
    double GetTrueCalibPedOdd( void ) const { return calib_true_[0];}
    double GetTrueCalibPedEven( void ) const { return calib_true_[1];}
    bool   CheckCalib (void) const; 
    void   ResetCalib (void); 
    void   SetCalib (double ped_odd, double ped_even);
    const std::vector< double >  &GetCSample ( void ) const { return csample_;} 
    const std::vector< std::pair<int, int> >  &GetOverflow ( void ) const { return over_;} 
    double GetClockSADC( void ) const { return sadc_clock_;}
  public:
    // static methods
    static void PrintSample ( const std::vector<uint16> &sample );
    static void PrintCSample ( const std::vector<double> &csample );
    static std::vector<double> ReorderALaSaw( const std::vector<double> &csample ); 
  public:
    void Clear ( void ); 
    const std::vector < FitPar>  &GetSignals( void ) const { return signals_;}
//    bool Fit(  const std::vector<uint16> &sample , const unsigned int icell );
    bool Fit(  const std::vector<uint16> &sample );
    bool FitPlusSomeMonitoring(  const std::vector<uint16> &sample ); 
    bool FitGoGo(  const std::vector<uint16> &sample ); 
// This we will keep as internal stuff. Do not need to initialize this.
    std::pair< double, double> CalcTime2_halfMaxDigSADC (const std::vector<double > &csample, const unsigned int max_position, double max_value_true );
    std::vector< std::pair<int,int> > DetectOverflow ( const std::vector<uint16> &sample ) const;
  protected:
    void FitShapePrecise (  const std::vector<double> &csample, FitPar &par ) const;

    void FitShape (  const std::vector<double> &csample, FitPar &par, const double &stept, const size_t &nfitp ) const;
    void ShapeHi2 (  const std::vector<double> &csample, FitPar &par ) const; 

// Monitoring Data    
  public:
    bool     MonitorInfoIsStored ( void ) const { return monitor_info_stored_;}
    double   GetDPed        ( void ) const { return (ped_odd_dynamic_-ped_even_dynamic_)/2.;}
    double   GetDynamicPed  ( void ) const { return (ped_odd_dynamic_+ped_even_dynamic_)/2.;}
    double   GetSawPar      ( void ) const { return saw_;}

  public:
// Data members from the former base class
   int                              type_;
  private:
   int                              decode_version_;
  public:
   int                              overflow_amplitude_;

// Parameters for algorithms of signals extraction   
   int                              par_sadc_ped_max_;
   
// Just a way to fool myself and have garanti that i really read calibrations from the file. And to have easy check for this. Do not survive I'm sure.
   std::vector <double>             calib_true_;

// intermediat result of sample data processing
  private:
   std::vector< double >             csample_;
   std::vector< std::pair<int,int> > over_;
// Cleaning ?   
   double                            ped_odd_dynamic_;
   double                            ped_even_dynamic_;
   double                            saw_;
   bool                              monitor_info_stored_;  
// Final result of sample data processing
   std::vector < FitPar >  signals_;
// TODO replace by ProfileMap !!
   const ProfileMap        * tab_;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace MN

#endif  //         DigSADC_h
