#ifndef AnyID__include
#define AnyID__include

#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <map>

// #include "StatInfoMN.h"

////////////////////////////////////////////////////////////////////////////////

/*! \brief Class with description of basic properties of any general purpose identifier .


    \author Vladimir Kolosov
*/


namespace MN {
//class StatInfoMN;
////////////////////////////////////////////////////////////////////////////////

class AnyID 
{

  public:

    class ElementaryID : public  std::pair < bool , std::string >
    {
      public:

  /// Default constructor
//       ElementaryID ( void ) : std::pair< bool , std::string > ( false, std::string("") ) {id_ = (unsigned int)(-1);} 
       ElementaryID ( void ) : std::pair< bool , std::string > ( false, std::string("") ) {id_ = -1;} 
  /// Copy constructor
       ElementaryID ( const ElementaryID &id)  : std::pair< bool , std::string >(id) {*this=id;} 

//       ElementaryID ( const std::string  &name ) : std::pair< bool , std::string > (false,name) {id_ = (unsigned int)(-1);} 
       ElementaryID ( const std::string  &name ) : std::pair< bool , std::string > (false,name) {id_ = -1;} 
//       ElementaryID ( unsigned int name ); 
       ElementaryID ( int name ); 
       const bool &IsInteger ( void ) const {return first;} 
       bool Empty ( void ) const {return ( second == std::string("") );}

       unsigned int  GetIntID ( void ) const { if(!IsInteger() ) std::cerr <<" Warning! in ElementaryID::GetIntID() of not integer value " << second <<std::endl; return id_;}

      public:
       bool operator == ( const ElementaryID &id) const;
       bool operator != ( const ElementaryID &id ) const;
       bool operator >  (const ElementaryID &id ) const;
       inline  bool operator <  (const ElementaryID &id) const {return id > *this;} 
       inline  bool operator >= (const ElementaryID &id) const {return (*this==id) || (*this > id);} 
       inline  bool operator <= (const ElementaryID &id) const {return (*this==id) || (*this < id);};
//        bool operator <  (const ElementaryID &id) const; 
//        bool operator >= (const ElementaryID &id) const;
//        bool operator <= (const ElementaryID &id) const;
    // Assignment operator 
       ElementaryID   &operator = (const ElementaryID &id);
      /// Print 
       void	       Print   (std::ostream &o=std::cout) const { if(IsInteger() ) 
                                                                     o << id_;
								   else  
                                                                     o << second;
								 }
       friend std::ostream &operator << (std::ostream &o,const ElementaryID &id) {id.Print(o);return o;}
       private:
//       unsigned int  id_;
       int  id_;
    };

  public:
  
  /// Default constructor
    AnyID ( void ) {}
  /// Copy constructor
    AnyID ( const AnyID &id ) {*this=id;}
  
    AnyID ( const std::vector < ElementaryID > &names ) : names_(names) {}

    AnyID ( const std::vector < ElementaryID > &names, size_t i0, size_t i1 ) { for( size_t i=0; i<names.size(); i++) { if( i>= i0 && i < i1) names_.push_back(names[i]);}}


// Constructors must be templated
    AnyID ( const std::vector < std::string > &names );

//    AnyID ( const std::vector < unsigned int > &names );
    AnyID ( const std::vector < int > &names );

//    AnyID ( const std::string &name, const std::vector < unsigned int > &names );
    AnyID ( const std::string &name, const std::vector < int > &names );
  
    AnyID ( const std::string &name ) { AddLayer(name); }
 //    AnyID ( unsigned int id ) { AddLayer(id); }
    AnyID ( int id ) { AddLayer(id); }
   
//    AnyID ( unsigned int id0, unsigned int id1) { AddLayer(id0); AddLayer(id1);}
    AnyID ( int id0, int id1) { AddLayer(id0); AddLayer(id1);}
   
//    AnyID ( unsigned int id0, unsigned int id1, unsigned int id2) { AddLayer(id0); AddLayer(id1); AddLayer(id2);}
    AnyID ( int id0, int id1, int id2) { AddLayer(id0); AddLayer(id1); AddLayer(id2);}
   
//    AnyID ( unsigned int id0, unsigned int id1, unsigned int id2, unsigned int id3) { AddLayer(id0); AddLayer(id1); AddLayer(id2); AddLayer(id3);}
    AnyID ( int id0, int id1, int id2, int id3) { AddLayer(id0); AddLayer(id1); AddLayer(id2); AddLayer(id3);}
 
//    AnyID ( const std::string &name, unsigned int id ) { AddLayer(name); AddLayer(id); }
    AnyID ( const std::string &name, int id ) { AddLayer(name); AddLayer(id); }

//    AnyID ( const std::string &name, unsigned int id0, unsigned int id1 ) { AddLayer(name); AddLayer(id0);  AddLayer(id1);}
    AnyID ( const std::string &name, int id0, int id1 ) { AddLayer(name); AddLayer(id0);  AddLayer(id1);}

    AnyID ( const AnyID &id1,  const AnyID &id2 ) { for( size_t l=0; l<id1.Size(); l++) AddLayer(id1.GetLayer(l)); for( size_t l=0; l<id2.Size(); l++) AddLayer(id2.GetLayer(l));}

  public:
  // Bug SelectLayers(i,i) makes an inf loop
    AnyID SelectLayers(size_t i0, size_t i1 ) const { return AnyID(names_,i0,i1);}

    void AddLayer ( const ElementaryID  &id ) { names_.push_back(id);}

    void AddLayer ( const std::string  &name );

    void AddLayer ( int  name );

    bool Empty ( void ) const { return (names_.size() == 0);} 

    size_t Size ( void ) const { return names_.size();} 

    const std::string &GetName ( const int &layer ) const;
    const std::string &GetName ( void ) const { return GetName (0);}
    std::string GetFullNameWithSpacers ( void ) const;
    std::string GetFullNameWithUnderscores ( void ) const;

    const ElementaryID  &GetLayer( const int &layer ) const;
    void  ReSetLayer( const int &layer, const ElementaryID  &id ) { 
                     if( (int)Size() <= layer ) {std::cerr <<" AnyID::ReSetLayer Error Layer" <<layer <<" Not exist Max ="<< Size() << std::endl; return;}
                     names_[layer] = id;
		                                                  }
                                                                     

    bool EqualInRange ( const size_t &layer, const AnyID &id ) const; 

  public:
	    bool operator == (const AnyID &id) const;
    inline  bool operator != (const AnyID &id) const { return !(*this == id);}
	    bool operator >  (const AnyID &id) const;
    inline  bool operator <  (const AnyID &id) const {return id > *this;} 
    inline  bool operator >= (const AnyID &id) const {return (*this==id) || (*this > id);} 
    inline  bool operator <= (const AnyID &id) const {return (*this==id) || (*this < id);};
    // Assignment operator 
    AnyID   &operator = (const AnyID &id);
    // Not commutative 
    AnyID   &operator + (const AnyID &id);
    void    Print   (std::ostream &o=std::cout) const { for ( size_t i=0; i<Size(); i++){ o <<" " << names_[i];} }
    friend std::ostream &operator << ( std::ostream &o, const AnyID &id) { id.Print(o); return o;}

  private:

    std::vector < ElementaryID > names_;

};

// ////////////////////////////////////////////////////////////////////////////////
// 
// class DetCalibDB 
// {
//   public:
//     DetCalibDB ( void ) {}
//     bool                          CalibExist ( const AnyID &calib_id ) const;
//     void                          AddCalib ( const AnyID &calib_id, const std::vector<double> &calib_data );
//     bool                          SetCalib (const AnyID &calib_id, size_t &icell, const double &cf );
//     const std::vector<double>  &GetCalib ( const AnyID &calib_id) const;
//     std::vector<double>  &GetCalib2Modify ( const AnyID &calib_id);
//     double			 GetCalib ( const AnyID &calib_id, size_t &icell) const;
//   public:
//     std::map < AnyID, std::vector<double> >		 calib_db_;
// };
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// class DetCalibDB3 
// {
//   public:
//     DetCalibDB3 ( void ) {}
//     void                          PrintTags( void ) const;
//     bool                          CalibExist ( const AnyID &calib_id ) const;
//     void                          AddCalib ( const AnyID &calib_id, const std::vector<MN::StatInfoMN> &calib_data );
//     bool                          SetCalib (const AnyID &calib_id, size_t &icell, const MN::StatInfoMN &cf );
//     const std::vector<MN::StatInfoMN>  &GetCalib ( const AnyID &calib_id) const;
//     std::vector<MN::StatInfoMN>  &GetCalib2Modify ( const AnyID &calib_id);
//     MN::StatInfoMN			 GetCalib ( const AnyID &calib_id, size_t &icell) const;
//   public:
//     std::map < AnyID, std::vector<MN::StatInfoMN> >		 calib_db_;
// };
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
//  class TimeDB
//  {
//    public:
//      TimeDB ( void ) {Clear(); Configure(true);}
//      void Clear ( void ) {map_time2data_.clear();}
//      void Configure (bool interpolate ) { interpolate_ = interpolate; }
//      std::vector< double >  GetData ( const time_t  &time ) const;
//      void  Insert ( const time_t &t,  const std::vector<double> &v ) { std::pair< time_t,  std::vector<double> > d(t,v); map_time2data_.insert( d); }
// //     void Insert (const std::pair< time_t, std::vector<double> > &data) { map_time2data_.push_back(data);}
//    static std::string TimeConvert2string( const time_t  &time );
//    public:
//    bool interpolate_;
//    std::map< time_t, std::vector< double>   >          map_time2data_;
//  };
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// class TimeDB2
// {
//    public:
//      TimeDB2 ( void ) {}
//       const TimeDB * GetTimeDB ( const AnyID &dbid ) const;
//       void  Insert ( const AnyID &dbid, TimeDB *db ) {  std::pair<AnyID,TimeDB *> p(dbid,db); map_timedb_.insert(p); }
//    private:
//    std::map<AnyID, TimeDB *  >          map_timedb_;
// };
// 
// ////////////////////////////////////////////////////////////////////////////////
// // Mozhet eto uzhe i lishnee
// class DBT
// {
//    public:
//      DBT ( void ) {}
//       const TimeDB2 * GetDB ( const AnyID &dbid ) const;
//       void  Insert ( const AnyID &dbid, TimeDB2 *db ) {  std::pair<AnyID,TimeDB2 *> p(dbid,db); map_db_.insert(p); }
//    private:
//    std::map<AnyID, TimeDB2 *  >          map_db_;
// };
// 
// ////////////////////////////////////////////////////////////////////////////////

} // namespace MN

#endif // AnyID__include
