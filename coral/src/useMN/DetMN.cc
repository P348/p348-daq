#include <stdio.h>
#include <stdlib.h>
#include <sstream>

#include "DetMN.h"
#include "AnyID.h"
#include "DigSADC.h"

#define CDB_LINE_MAX 132

using namespace std;

namespace MN {

////////////////////////////////////////////////////////////////////////////////

DetMN::DetMN( const std::string &name, size_t ncells ) : name_(name), ncells_mn_(ncells), mantab_cells_sadc_(0)
{
  mantab_cells_sadc_=new MN::ManagerCellsShapeTableSADC();
}

////////////////////////////////////////////////////////////////////////////////

void DetMN::InitXY( int nx, int ny )
{
  fNcols_=nx;
  fNrows_=ny;
  map_cell_xy_.clear();
  map_xy_cell_[0].clear();
  map_xy_cell_[1].clear();
  for( int it=0; it!=fNcols_*fNrows_; it++ ) {
    map_cell_xy_.push_back(-1);
  }
  for( size_t it=0; it<NCellsMN(); it++ ) {
    map_xy_cell_[0].push_back(-1);
    map_xy_cell_[1].push_back(-1);
  }
}

////////////////////////////////////////////////////////////////////////////////

void DetMN::SetCellXY( int ic, int x, int y)
{
  if( x >= 0 && x <fNcols_ && y >= 0 && y < fNrows_ && ic >= 0 && ic < (int)NCellsMN() )
  {
    map_cell_xy_[x+y*fNcols_]=ic;
    map_xy_cell_[0][ic]=x;
    map_xy_cell_[1][ic]=y;
  }
  else
  {
    cerr <<" DetMN::SetCellXY " << GetNameMN() <<" ic =" << ic <<" x =" << x <<" y =" << y <<" failed " << endl;
    exit(1);
  }
}

////////////////////////////////////////////////////////////////////////////////

int  DetMN::GetCellOfColumnRowMN     (int x, int y) const
{
  if ( !XYRegularGridMN() ) return -1;
  if ( 0 <= x && x < fNcols_ && 0 <= y && y < fNrows_ )
    return map_cell_xy_[x+y*fNcols_];
  return -1;
}

////////////////////////////////////////////////////////////////////////////////

int  DetMN::GetColumnOfCell(int icell) const
{
  if ( !XYRegularGridMN() )
    return -1;

  if ( icell >= 0 && icell < (int)NCellsMN() )
    return map_xy_cell_[0][icell];

  return -1;
}

////////////////////////////////////////////////////////////////////////////////

int DetMN::GetRowOfCell(int icell) const
{
  if ( !XYRegularGridMN() )
    return -1;

  if ( icell >= 0 && icell < (int)NCellsMN() )
    return map_xy_cell_[1][icell];

  return -1;
}

////////////////////////////////////////////////////////////////////////////////

void DetMN::ReadCalibrationByTags(const std::string &tag, const std::string &s) 
{
  bool debug = false;
//   bool debug = true;
  if( tag == std::string("_ShapeTableSADC") ) 
  {
    cerr <<" DetMN::ReadCalibrationByTags Calibration tagged by  _ShapeTableSADC is NOT USED AMNYMORE !! Please consider to replace option by _CellShapeTableSADC " << endl;
    exit(1);
//     if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Yes We know how to Decode Tag " << tag << " input Minutochku! " << endl;
//     bool ok = InputShapeTableSADC( s );
//     if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Decode Tag " << tag << " ok = " << ok << endl;
  }
  else if( tag == std::string("_SADCInfo") ) 
  {
    if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Yes We know how to Decode Tag " << tag << " input Minutochku! " << endl;
    bool ok = InputSADCInfo( s );
    if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Decode Tag " << tag << " ok = " << ok << endl;
  }
  else if( tag == std::string("_CALIB") ) 
  {
    if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Yes We know how to Decode Tag " << tag << " input Minutochku! " << endl;
    MN::AnyID id(tag);
    bool ok = InputDetCalibInfo( id, s );
    if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Decode Tag " << tag << " ok = " << ok << endl;
  }
  else if( tag == std::string("_TimeCALIB") ) 
  {
    if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Yes We know how to Decode Tag " << tag << " input Minutochku! " << endl;
    MN::AnyID id(tag);
    bool ok = InputDetTimeCalibInfo( id,  s );
    if( debug ) cout <<" DetMN::readCalibrationByTags " << GetNameMN() <<" Decode Tag " << tag << " ok = " << ok << endl;
  }
}

////////////////////////////////////////////////////////////////////////////////

void DetMN::SetCellNames ( const std::vector<std::string> &cell_names )
{
  if( NCellsMN() != cell_names.size() )
  {
    cerr <<" Fatal Error in DetMN::SetCellNames NCellsMN() = " << NCellsMN() <<" but cell_names.size() = " << cell_names.size() << endl;
    exit(1);
  }
  cell_names_=cell_names;
}

////////////////////////////////////////////////////////////////////////////////

const std::string &DetMN::GetCellNameMN ( int icell ) const
{
  if( icell < 0 || icell >= (int)NCellsMN()  )
  {
    cerr <<" Fatal Error in DetMN::GetCellNameMN " << GetNameMN() <<" NCellsMN() = " << NCellsMN() <<" but icell = " << icell << endl;
    exit(1);
  }
  return cell_names_[icell];
}

////////////////////////////////////////////////////////////////////////////////

int DetMN::FindCellByNameMN( const std::string &cellname ) const
{
  for(int ic=0; ic<(int)NCellsMN(); ic++)
  {
    if(cellname == cell_names_[ic] ) return ic;
  }
  return -1;
}

////////////////////////////////////////////////////////////////////////////////

double DetMN::GetTimeCalibrated ( int icell, double time ) const
{ 
  double t0=0.;
  MN::AnyID tcalbid("_TimeCALIB");
  bool print_warning = false;
  size_t ic=icell;
  if( GetCalibDB3c().CalibExist(tcalbid) )
  {
    t0 = GetCellCalib3(tcalbid, ic).GetMean();
  }
  else
  {
    if( print_warning )
      std::cerr <<" CsDet::GetTimeCalibrated No Calibrations " << tcalbid  <<" for CsDet " << GetNameMN() << std::endl;
  }  
  return time-t0;
}

////////////////////////////////////////////////////////////////////////////////

double DetMN::GetEnergyCalibrated ( int icell, double amp ) const
{
//   bool debug = true;
  bool debug = false;
  if( debug ) cout <<"  DetMN::GetEnergyCalibrated " << GetNameMN() <<" cell " << icell <<" amp " << amp <<" NCells " << NCellsMN() << endl;
// TODO implement LED corrections etc.
  double cf=1.;
  MN::AnyID tcalbid("_CALIB");
  bool print_warning = false;
  size_t ic=icell;
  if( GetCalibDB3c().CalibExist(tcalbid) )
  {
    cf = GetCellCalib3(tcalbid,ic).GetMean();
  }
  else
  {
    if( print_warning )
      std::cerr <<" CsDet::GetTimeCalibrated No Calibrations " << tcalbid  <<" for CsDet " << GetNameMN() << std::endl;
  }  
  return amp*cf;
}

////////////////////////////////////////////////////////////////////////////////

void DetMN::SetAutoDecodeVersion ( void )
{
  bool debug = false;
//  if( GetNameMN() == std::string("ECAL0") ) debug = true;

  if( GetNDigSADC() == 0 ) return;
  
  if( debug ) cout <<" DetMN::SetAutoDecodeVersion Implement Auto Decode  version Setting for DigSADC  in " << GetNameMN() << std::endl;
  int notint_cnt=0;
  int nsadc_cnt=0;
  int notshapecalib_cnt=0;
  for( int idg=0; idg < (int)NCellsMN(); idg++ )
  {
    if( GetDigSADC(idg) != 0 )
    {
      nsadc_cnt++;
      if( !GetDigSADC(idg)->CheckCalib() ) notshapecalib_cnt++;
    }
    else
    {
      notint_cnt++;
    }
  }
// Tupo sdelano. Nado tschatelnee
  if(nsadc_cnt == 0 ) return;

  if( debug )  cerr <<" DetMN::SetAutoDecodeVersion DigSADC in " << GetNameMN() <<" NCellsMN()=" << NCellsMN() <<
                             " notint_cnt=" << notint_cnt <<" notshapecalib_cnt=" << notshapecalib_cnt << endl;

  if(notint_cnt != 0 )
  {
    cerr <<" Not initialized DigSADC in " << GetNameMN() <<"  " << notint_cnt <<" out of " << NCellsMN() << endl;
    exit(1);
  }
  
  if( notshapecalib_cnt > 0 )
  {
    if( notshapecalib_cnt == (int)NCellsMN() )
    {
      if( debug ) cout <<" DetMN::SetAutoDecodeVersion Set Decode  version for DigSADC  in " << GetNameMN() <<" to 1 " << std::endl;
      for( int idg=0; idg < (int)NCellsMN(); idg++ )
      {
        GetDigSADC2Modify(idg)->SetDecodeVersion(1);
      }
    }
    else
    {
      cerr <<" Not all DigSADC in " << GetNameMN() <<" has proper calibration " << notshapecalib_cnt <<" out of " << NCellsMN() << endl;
      exit(1);
    }
  }
  else
  {
// Use default DigSADC decoding
    if( debug ) cout <<" DetMN::SetAutoDecodeVersion Set Decode  version for DigSADC  in " << GetNameMN() <<" to 4 " << std::endl;
    for( int idg=0; idg < (int)NCellsMN(); idg++ )
    {
      GetDigSADC2Modify(idg)->SetDecodeVersion(4);
    }
  }
  
//   if( debug )
//   {
//     cout <<" DetMN::SetAutoDecodeVersion  " << GetName() <<" exit4debug " << endl;
//     exit(0); 
//   }
  
  
//   if( debug ) cout <<" DetMN::SetAutoDecodeVersion Implement Auto Decode  version Setting for NDigSADC=" << GetDigSADC().size() << " in " << GetNameMN() << std::endl;
//   int notint_cnt=0;
//   int notshapecalib_cnt=0;
//   for( int idg=0; idg < (int)GetDigSADC().size(); idg++ )
//   {
//     if( GetDigSADC()[idg] != 0 )
//     {
//       if( !GetDigSADC()[idg]->CheckCalib() ) notshapecalib_cnt++;
//     }
//     else
//     {
//       notint_cnt++;
//     }
//   }
// 
//   if( debug )  cerr <<" DetMN::SetAutoDecodeVersion DigSADC in " << GetNameMN() <<" NCellsMN()=" << NCellsMN() <<" NDigSADC=" <<GetDigSADC().size() <<
//                              " notint_cnt=" << notint_cnt <<" notshapecalib_cnt=" << notshapecalib_cnt << endl;
//   if(notint_cnt != 0 )
//   {
//     cerr <<" Not initialized DigSADC in " << GetNameMN() <<"  " << notint_cnt <<" out of " << NCellsMN() << endl;
//     exit(1);
//   }
//   
//   if( notshapecalib_cnt > 0 )
//   {
//     if( notshapecalib_cnt == (int)NCellsMN() )
//     {
//       for( int idg=0; idg < (int)GetDigSADC().size(); idg++ )
//       {
//         cerr <<" It was a bug ?? " << GetNameMN() <<" SetDecodeVersion(4) !!! XCheck!!!!!! " << endl;
//         GetDigSADC()[idg]->SetDecodeVersion(1);
// //        GetDigSADC()[idg]->SetDecodeVersion(4);
//       }
//     }
//     else
//     {
//       cerr <<" Not all DigSADC in " << GetNameMN() <<" has proper calibration " << notshapecalib_cnt <<" out of " << NCellsMN() << endl;
//       exit(1);
//     }
//   }
//   else
//   {
// // Using ShapeTable decoding variant
//     for( int idg=0; idg < (int)NCellsMN(); idg++ )
//     {
//         cerr <<" It was a bug ?? " << GetNameMN() <<" Now SetDecodeVersion(4) !!! XCheck!!!!!! " << endl;
//       GetDigSADC2Modify(idg)->SetDecodeVersion(4);
//     }
//   }
//   
//   if( debug )
//   {
//     cout <<" DetMN::SetAutoDecodeVersion  " << GetNameMN() <<" exit4debug " << endl;
//     exit(0); 
//   }
  
}

////////////////////////////////////////////////////////////////////////////////

void DetMN::CheckCalibrations ( void ) const
{
  int notint_cnt=0;
  int notshapecalib_cnt=0;
  for( int idg=0; idg < (int)GetDigSADC().size(); idg++ )
  {
    if( GetDigSADC()[idg] != 0 )
    {
      if( !GetDigSADC()[idg]->CheckCalib() ) notshapecalib_cnt++;
    }
    else
    {
      notint_cnt++;
    }
  }

  if(notint_cnt != 0 )
  {
    cerr <<" Not initialized DigSADC in " << GetNameMN() <<"  " << notint_cnt <<" out of " << NCellsMN() << endl;
    exit(1);
  }
  
  if( notshapecalib_cnt > 0 )
  {
    if( notshapecalib_cnt == (int)NCellsMN() )
    {
//       for( int idg=0; idg < (int)GetDigSADC().size(); idg++ )
//       {
//         GetDigSADC()[idg]->GetDecodeVersion(4);
//       }
    }
    else
    {
      cerr <<" Not all DigSADC in " << GetNameMN() <<" has proper calibration " << notshapecalib_cnt <<" out of " << NCellsMN() << endl;
      exit(1);
    }
  }
  else
  {
// Use default DigSADC decoding
  }
} 

////////////////////////////////////////////////////////////////////////////////

const MN::StatInfoMN  DetMN::GetCellCalib3( const MN::AnyID &calib_id, size_t &icell ) const
{
  return GetCalibDB3c().GetCalib(calib_id, icell ); 
}

////////////////////////////////////////////////////////////////////////////////

int DetMN::InputCellShapeTableSADC( int icell, const string &s)
{
  if( mantab_cells_sadc_ != 0 )
  {
//     cout <<"  DetMN::InputCellShapeTableSADC " <<GetNameMN() <<" cell " << icell <<" mantab_cells_sadc_ " << mantab_cells_sadc_ << endl;
    int iret = mantab_cells_sadc_->InputCellShapeTableSADC(s);
    { 
      MN::DigSADC *dg = GetDigSADC2Modify( icell );
      if( dg != 0 )
      {
        const ProfileMap *tab = mantab_cells_sadc_->GetProfile(icell);
	dg->SetTab(tab);
//        dg->SetDecodeVersion(4); // Once we read the table we will use the table
      }
    }
    return iret;
  }   
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////

void DetMN::CheckCellShapeTableSADC(  void )
{
//   bool debug = true;
  bool debug = false;
  bool checkfailed = false;
  if( debug ) cout <<" Det::CheckCellShapeTableSADC " << GetNameMN() << endl;
  if( mantab_cells_sadc_ != 0 )
  {
    bool ok = mantab_cells_sadc_->CheckTables();
    if( ok )
    {
      const ProfileMap * pm = mantab_cells_sadc_->GetProfile(0);
      int maxic = 0;
      double maxst = 0;
      if(pm != 0 ) maxst = pm->GetStat();
      for(  size_t ic=0; ic < NCellsMN(); ic++ )
      { 
        pm = mantab_cells_sadc_->GetProfile(ic);
        double st = 0;
        if(pm != 0 ) st = pm->GetStat();
        if( st > maxst )
	{
          maxst=st;
	  maxic=ic;
	}
      }
      const ProfileMap *tabmax = mantab_cells_sadc_->GetProfile(maxic);
      if( debug ) cout <<" TabMax defined " << tabmax << endl;

      int nfailed =0;
      for(  size_t ic=0; ic < NCellsMN(); ic++ )
      { 
        MN::DigSADC *dg = GetDigSADC2Modify( ic );
        if( dg != 0 )
        {
          const ProfileMap *tab = mantab_cells_sadc_->GetProfile(ic);
          const ProfileMap *tabdg  = dg->GetTab();
//	  if( debug ) cout <<" Cell " << ic <<" tab " << tab <<" tabdg " << tabdg << endl;
	  if( tabdg == 0 )
	  {
	    dg->SetTab(tabmax);
	    nfailed++;
//	    checkfailed = true;
	    if( debug ) cout <<" new tabdg " << dg->GetTab() << endl;
	  }  
          const ProfileMap *tabdgrepare  = dg->GetTab();
	  if( debug ) cout <<" Cell " << ic <<" tab " << tab <<" tabdg " << tabdg <<" tabdgrepare " << tabdgrepare << endl;
	  if( tabdgrepare == 0 )
	  {
	    checkfailed = true;
	  }  
//	  if( tabdg == 0 || tab != tabdg ) checkfailed = true;
        }
      }
      if( debug ) cout <<" Det::CheckCellShapeTableSADC " << GetNameMN() <<" nfailed = " << nfailed << endl;
    }
    else
    {
      checkfailed = true;
    }
  }
  else
  {
    checkfailed = true;
  }
     
  if( checkfailed )
  {
//     if( nfailed == (int)NCellsMN() )
//     {
//       cerr <<" DetMN::CheckCellShapeTableSADC " << GetNameMN() <<" Check failed No Tab calibrations at All " << endl;
//       return;
//     }
    cout <<" DetMN::CheckCellShapeTableSADC " << GetNameMN() <<" Check failed! Perform Cross Checks!  " << endl;
    exit(1);
//    return;
  }
  if( debug ) cout <<" Det::CheckCellShapeTableSADC " << GetNameMN() <<" Check passed OK " << endl;
//   if( debug )
//   { 
//     cout <<" Det::CheckCellShapeTableSADC " << GetNameMN() <<" exit4debug " << endl;
//     exit(0);
//   }






//   bool checkfailed = false;
//   int nfailed=0;
//   if( debug ) cout <<" DetMN::CheckCellShapeTableSADC " << GetNameMN() << endl;
//   if( mantab_cells_sadc_ != 0 )
//   {
//     bool ok = mantab_cells_sadc_->CheckTables();
//     if( ok )
//     {
//       cerr <<" DetMN::CheckCellShapeTableSADC Setting tables for All DigSADC is commented !!!!!!!!!!!!!!!!! How it is done that case ??? " << endl;
// //       const ProfileMap * pm = mantab_cells_sadc_->GetProfile(0);
// //       int maxic = 0;
// //       double maxst = 0;
// //       if(pm != 0 ) maxst = pm->GetStat();
// //       for(  size_t ic=0; ic < NCellsMN(); ic++ )
// //       { 
// //         pm = mantab_cells_sadc_->GetProfile(ic);
// //         double st = 0;
// //         if(pm != 0 ) st = pm->GetStat();
// //         if( st > maxst )
// // 	{
// //           maxst=st;
// // 	  maxic=ic;
// // 	}
// //       }
// //       const ProfileMap *tabmax = mantab_cells_sadc_->GetProfile(maxic);
// // 
// //       for(  size_t ic=0; ic < NCellsMN(); ic++ )
// //       { 
// //         MN::DigSADC *dg = GetDigSADC2Modify( ic );
// //         if( dg != 0 )
// //         {
// //           const ProfileMap *tab = mantab_cells_sadc_->GetProfile(ic);
// //           const ProfileMap *tabdg  = dg->GetTab();
// // 	  if( debug ) cout <<" Cell " << ic <<" tab " << tab <<" tabdg " << tabdg << endl;
// // 	  if( tabdg == 0 )
// // 	  {
// // 	    dg->SetTab(tabmax);
// // 	    nfailed++;
// // 	    checkfailed = true;
// // 	    if( debug ) cout <<" new tabdg " << dg->GetTab() << endl;
// // 	  }  
// // //	  if( tabdg == 0 || tab != tabdg ) checkfailed = true;
// //         }
// //       }
//     }
//     else
//     {
//       checkfailed = true;
//     }
//   }
//   else
//   {
//     checkfailed = true;
//   }
//      
//   if( checkfailed )
//   {
// //     if( nfailed == (int)NCellsMN() )
// //     {
// //       cerr <<" DetMN::CheckCellShapeTableSADC " << GetNameMN() <<" Check failed No Tab calibrations at All " << endl;
// //       return;
// //     }
// //     cout <<" DetMN::CheckCellShapeTableSADC " << GetNameMN() <<" Check failed! Perform Cross Checks!  " << endl;
//     exit(1);
// //    return;
//   }
//   if( debug ) cout <<" DetMN::CheckCellShapeTableSADC " << GetNameMN() <<" Check passed OK " << endl;
}

// ///////////////////////////////////////////////////////////////////////////////
// 
// int DetMN::InputShapeTableSADC(const string &s)
// {
//   if( mantab_cells_sadc_ != 0 )
//   { 
//     int iret = mantab_cells_sadc_->InputShapeTableSADC(s);
//     if( iret == 0 )
//     {
//       for(  size_t ic=0; ic < NCellsMN(); ic++ )
//       {
//         MN::DigSADC *dg = GetDigSADC2Modify( ic );
//         if( dg != 0 )
//         {
//           const std::map<double,double> *tab = mantab_cells_sadc_->GetTab(ic);
//           dg->SetTab(tab);
//           dg->SetDecodeVersion(4); // Once we read the table we will use the table
//         }
//       }
//     }
//     return iret;
//   }   
//   return 0;
// }
// 
////////////////////////////////////////////////////////////////////////////////

int DetMN::InputSADCInfo( const string &s)
{
//   bool debug = true;
  bool debug = false;
  bool ignore_cell_name_check = true;
  
  if( debug )
  { 
    cout << " Det::InputSADCInfo " << GetNameMN() << " debug  XY?= " << XYRegularGridMN() << endl;
    cout << s;
  }
  if( XYRegularGridMN() )
  {
    return InputSADCInfoXY( s );
  }
  istringstream is(s.c_str());
  char calorim_name[132],dummy[132],cellname[132];
  string str;

//  Read line of comments
  getline(is,str);
  getline(is,str);
  sscanf(str.c_str(),"%s %s ",calorim_name,dummy);  
  getline(is,str);

  getline(is,str);

  int not_matched_cells_cnt = 0;
  while( getline(is,str) )
  {
    // there is one type of calibration with 9 rows of data, and one with 7
    // call the one with 7 the old one
//    bool oldformat(false);

    int icell,idsadc, iledev;
    float ped, dped, convert, unknown_entry_a, unknown_entry_b;

    if ( sscanf(str.c_str()," %d %d %d %g %g %g %g %g %s \n", &icell, &idsadc, &iledev,  &ped,  &dped,  &convert, &unknown_entry_a, &unknown_entry_b, cellname)!=9 )
    {
//      oldformat = true;

      int ret = sscanf(str.c_str()," %d %d %d %g %g %g %s \n", &icell, &idsadc, &iledev,  &ped,  &dped,  &convert, cellname );
      if( ret != 7 )
      {
        cerr <<" CalorimeterMN::InputSADCInfo format error " << endl;
	cerr << str << endl;
	exit(1);
      }
//      assert(ret==7);
    }

    if( !ignore_cell_name_check )
    {
      int jcell = FindCellByNameMN( cellname );
      if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
      if( !(jcell >=0 && jcell < (int)NCellsMN()) )
      {
        cerr << " Unexpected Cell name " << cellname << " in CalorimeterMN::InputSADCInfo " <<
                                                                        GetNameMN() << "  " << endl;
        cerr << " Input strig: " << endl;
        cerr << str << endl;
        cerr << " It well might be that you use wrong calibration file for this detector " << GetNameMN() << endl;    
        stringstream ss;
        ss << " Unexpected Cell name " << GetNameMN() << " CalorimeterMN::InputSADCInfo " << str;
        cerr << ss.str() << endl;
        exit(1);
//      throw Exception(ss.str().c_str());
      }

      if( icell != jcell )
      {
        icell = jcell;
        not_matched_cells_cnt ++;
      }
    }
    
    MN::DigSADC *dig_sadc = GetDigSADC2Modify( icell );

    if( dig_sadc != NULL )
    {
//       double ped_odd = ped - dped;
//       double ped_even = ped + dped;
      double ped_odd = ped  + dped;
      double ped_even = ped - dped;
//       double dpedd = dped;
//       double cfcv = convert;
    
//      dig_sadc->SetCalibInfo( ped_odd, ped_even, dpedd, cfcv);
      dig_sadc->SetCalib( ped_odd, ped_even);
    }
  }
  if( not_matched_cells_cnt > 0 )
  {
    cerr << " WARNING!!! CalorimeterMN::InputSADCInfo " << GetNameMN() <<
               " Not matching in cells id was detected " << not_matched_cells_cnt << " times " << endl;
    cerr << " You use wrong calibaration file or calibrations were produced with different geometry descriptor !!! " << endl;
    exit(1);
  }

  return 0;
}

////////////////////////////////////////////////////////////////////////////////

int DetMN::InputSADCInfoXY( const string &s)
{
//   bool debug = true;
  bool debug = false;
  if( !XYRegularGridMN() )
  {
    cerr <<" Det::InputSADCInfoXY called in " << GetNameMN() <<" but No XY structure in det " << endl;
    exit(1);
  }
  if( debug )
  { 
    cout << " Det::InputSADCInfoXY " << GetNameMN() << " debug " << endl;
    cout << s;
  }
  istringstream is(s.c_str());
  char calorim_name[132],dummy[132],cellname[132];
  string str;

//  Read line of comments
  getline(is,str);
  getline(is,str);
  sscanf(str.c_str(),"%s %s ",calorim_name,dummy);  
  getline(is,str);

  getline(is,str);

  int not_matched_cells_cnt = 0;
  while( getline(is,str) )
  {
    // there is one type of calibration with 9 rows of data, and one with 7
    // call the one with 7 the old one
//    bool oldformat(false);

    int icell, x, y, idsadc, iledev;
    float ped, dped, convert, unknown_entry_a, unknown_entry_b;
    int iret = sscanf(str.c_str()," %d %d %d %d %g %g %g %g %g %s \n", &x, &y, &idsadc, &iledev,  &ped,  &dped,  &convert, &unknown_entry_a, &unknown_entry_b, cellname);
    if( iret != 10 )
    {
//      oldformat = true;

//       int ret = sscanf(str.c_str()," %d %d %d %g %g %g %s \n", &icell, &idsadc, &iledev,  &ped,  &dped,  &convert, cellname );
//       if( ret != 7 )
//       {
        cerr <<" Det::InputSADCInfoXY format error " << endl;
	cerr << str << endl;
	exit(1);
//       }
//      assert(ret==7);
    }

    icell = GetCellOfColumnRowMN( x, y);
    if( icell < 0 ) continue;
    if(debug) cout << " Cell x=" << x << " y=" << y <<" icell " << icell <<" True Cell name " << GetCellNameMN(icell) << " But we are looking for:" << std::string(cellname) << endl;
    if( icell < 0 ) continue;

    int jcell = FindCellByNameMN( std::string(cellname) );
    if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
    if( !(jcell >=0 && jcell < (int)NCellsMN()) )
    {
      cerr << " Unexpected Cell name " << cellname << " in CalorimeterMN::InputSADCInfo " <<
                                                                        GetNameMN() << "  " << endl;
      cerr << " Input strig: " << endl;
      cerr << str << endl;
      cerr << " It well might be that you use wrong calibration file for this detector " << GetNameMN() << endl;    
      stringstream ss;
      ss << " Unexpected Cell name " << GetNameMN() << " CalorimeterMN::InputSADCInfo " << str;
      cerr << ss.str() << endl;
      exit(1);
//      throw Exception(ss.str().c_str());
    }

    if( icell != jcell )
    {
      icell = jcell;
      not_matched_cells_cnt ++;
    }
    
    MN::DigSADC *dig_sadc = GetDigSADC2Modify( icell );

    if( dig_sadc != NULL )
    {
//       double ped_odd = ped - dped;
//       double ped_even = ped + dped;
      double ped_odd = ped + dped;
      double ped_even = ped - dped;
//       double dpedd = dped;
//       double cfcv = convert;
    
//      dig_sadc->SetCalibInfo( ped_odd, ped_even, dpedd, cfcv);
       dig_sadc->SetCalib( ped_odd, ped_even);
   }
  }
  if( not_matched_cells_cnt > 0 )
  {
    cerr << " WARNING!!! Det::InputSADCInfoXY " << GetNameMN() <<
               " Not matching in cells id was detected " << not_matched_cells_cnt << " times " << endl;
    cerr << " You use wrong calibaration file or calibrations were produced with different geometry descriptor !!! " << endl;
    exit(1);
  }

  return 0;
}

/////////////////////////////////////////////////////////////////////////////////

int DetMN::InputDetTimeCalibInfoXY( const MN::AnyID &calib_tag, const string &s)
{
//   bool debug = true;
  bool debug = false;
  if( !XYRegularGridMN() )
  {
    cerr <<" Det::InputTimeCalibInfoXY called in " << GetNameMN() <<" but No XY structure in det " << endl;
    exit(1);
  }

  if(! GetCalibDB3().CalibExist (calib_tag) )
  {
    std::vector <MN::StatInfoMN> vc( NCellsMN() );
    AddCalib3( calib_tag,vc);
  }

  if( debug ) cout <<" Det::InputTimeCalibInfoXY " << GetNameMN() <<" calib_tag " << calib_tag << endl;
  if( debug ) cout <<" DB3 exist for this tag? " << GetCalibDB3().CalibExist (calib_tag) << endl;
//   cout <<" CalorimeterMN::InputCalibInfoXY exit4debug " << GetNameMN() << endl;
// 
//   exit(0);
  if( debug ) cout << " Calorimeter::InputTimeCalibInfoXY " << GetNameMN() << " debug " << endl;
  istringstream is(s);
  char calorim_name[CDB_LINE_MAX],dummy[CDB_LINE_MAX],offset_id[CDB_LINE_MAX],try_to_read_offset[CDB_LINE_MAX];
  string str;

//  Read line of comments
  getline(is,str);
  if( str.length() >= CDB_LINE_MAX )    // avoid buffer overflow
  {
    cerr <<" DetMN::InputDetCalibInfo too long string " << str << endl;
    exit(1);
  }
  int ret = sscanf(str.c_str(), "%s %s", calorim_name, dummy);
  if( ret != 2 )
  {
    cerr <<" Det::InputTimeCalibInfoXY " << GetNameMN() <<" format error " << str << endl;
    exit(1);
  }

//  Read 2 lines of comments
  getline(is,str);
  float t_offset = 0.;
  getline(is,str);
  if( str.length() >= CDB_LINE_MAX )    // avoid buffer overflow
  {
    cerr <<" DetMN::InputDetCalibInfo too long string " << str << endl;
    exit(1);
  }
  ret = sscanf(str.c_str(), "%s %s", offset_id, try_to_read_offset);
  if( ret != 2 )
  {
    cerr <<" Det::InputTimeCalibInfoXY " << GetNameMN() <<" format error " << str << endl;
    exit(1);
  }
  if( std::string(offset_id) == "Time_Offset" )
  {
    if( debug )
    {
      cout << " Time_Offset = " << try_to_read_offset << " was identified " << endl;
      cout <<" Try to recognize once more " << str << endl;
    } 
    ret = sscanf(str.c_str(), " %s %g", offset_id, &t_offset);
    if( ret != 2 )
    {
      cerr <<" Det::InputTimeCalibInfoXY " << GetNameMN() <<" format error " << str << endl;
      exit(1);
    }
    if( debug ) cout << " And indeed we read Time_Offset = " << t_offset << " Ura !!! " << endl;
  } 


  while( getline(is,str) )
  {
    int x,y;
    float t,st,w;
    ret = sscanf(str.c_str()," %d %d %g %g %g", &x, &y, &t, &st, &w);
    if( ret != 5 )
    {
      cerr <<" Det::InputTimeCalibInfoXY " << GetNameMN() <<" format error " << str << endl;
      exit(1);
    }
    t += t_offset;
    if( w <= 0.) w = 1.;
    int icell = GetCellOfColumnRowMN( x, y);
    MN::StatInfoMN cf(w,t,st);
    if ( icell >= 0 )
    {
      size_t sztcell = icell;
      bool setok = SetCellCalib3( calib_tag,sztcell,cf);
      if( !setok )
      {
        cerr <<" unknown problem in calibrations setting " << endl;
        exit(1);
      }
    }  
  }
  return 0;
}

////////////////////////////////////////////////////////////////////////////////

int DetMN::InputDetTimeCalibInfo(const MN::AnyID &calib_tag, const string &s)
{
//   bool debug = true;
  bool debug = false;
//   if( GetTBName() == std::string("S2") ) debug = true;
  if( debug ) cout <<" DetMN::InputDetTimeCalibInfo tag " << calib_tag << endl;
  
  if(! GetCalibDB3().CalibExist (calib_tag) )
  {
    if( debug ) cout <<" Create new Calibration for tag " << calib_tag << endl;
    std::vector <MN::StatInfoMN> vc( NCellsMN() );
    AddCalib3(calib_tag,vc);
  }
//  cerr <<" Det::InputTimeCalibInfo Now copy code from int Det::InputTimeCalibInfo( const AnyID &calib_tag, const string &s) " << endl;

  if( XYRegularGridMN() ) return InputDetTimeCalibInfoXY( calib_tag, s);


  istringstream is(s);
  char calorim_name[CDB_LINE_MAX],dummy[CDB_LINE_MAX],cellname[CDB_LINE_MAX];
  string str;

//  Read line of comments
  getline(is,str);
  if( str.length() >= CDB_LINE_MAX )
  {
    cerr <<" Too long in line: " << endl;
    cerr << str << endl;
    exit(1);
  }
//   int ret = sscanf(str.c_str(), "%s %s", calorim_name, dummy);
//   if( ret != 2 )
//   {
//     cerr <<" Det::InputDetTimeCalibInfo format error in line: " << endl;
//     cerr << str << endl;
//     exit(1);
//   }
 
//  Read 2 lines of comments
  getline(is,str);
  getline(is,str);

// read line with offset
  getline(is,str);
  float t_offset;
  int ret = sscanf(str.c_str(), " %g", &t_offset);
  if( ret != 1 )
  {
    cerr <<" Det::InputDetTimeCalibInfo format error in line: " << endl;
    cerr << str << endl;
    exit(1);
  }
  if( debug ) cout << " Time offset " << t_offset << endl;
//  Read calibration
  uint not_matched_cells_cnt = 0;
  while( getline(is,str) )
  {
    int icell;
    float t,st,w;
//    assert( str.length() < CDB_LINE_MAX );  // avoid buffer overflow
    if( str.length() >= CDB_LINE_MAX )
    {
      cerr <<" Too long in line: " << endl;
      cerr << str << endl;
      exit(1);
    } 
//    ret = sscanf(str.c_str(), " %d %g %g %g %s", &icell, &w, &t, &st, cellname);
    ret = sscanf(str.c_str(), " %d %g %g", &icell,  &t, &st);
    if( ret != 3 )
    {
      cerr <<" Det::InputDetTimeCalibInfo format error in line: " << endl;
      cerr << str << endl;
      exit(1);
    }
    w=1.;

// Ignore Cell names
//     int jcell = FindCellByNameMN( cellname );
//     if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
//     if( !(jcell >=0 && jcell < (int)NCellsMN()) )
//     {
//       cerr << " Unexpected Cell name " << cellname << " in Calorimeter::InputTimeCalibInfo " <<
//                                                         GetNameMN() << " " << calib_tag  << endl;
//       cerr << " Input string: " << endl;
//       cerr << str << endl;
//       cerr << " It well might be that you are use wrong calibration file for this detector " << GetNameMN() << endl;    
//       stringstream ss;
//       ss << " Unexpected Cell name " << GetNameMN() << "Calorimeter::InputTimeCalibInfo " << str;
//       cerr << ss.str() << endl;
//       exit(1);
// //      throw Exception( ss.str().c_str() );
//     }

//     if( icell != jcell ) {
//       icell = jcell;
//       not_matched_cells_cnt++;
//       if (not_matched_cells_cnt==1) {
// 	cerr << "Notice: " <<  __func__ << " " << GetNameMN() << " "
// 	     << "cell id and cell name do not match.  (The calibration file "
// 	     << "was produced with a different geometry description, which "
// 	     << "might be an indication that you're using the wrong file.)"
// 	     << endl;
//       }
//     }
      
    if(icell >=0 && icell < (int)NCellsMN() )
    {
      if( debug )
      {
        cout << "  cell " << icell << " w " << w << " t0 " << t+t_offset <<" st " << st << endl;
      }
      
      if( w == 0. ) w=1.; 
      MN::StatInfoMN cf((double)w,(double)(t+t_offset),(double)st);
      if( debug )
      {
        cout << "  cell " << icell << " t0 " << t+t_offset <<" Xcheck=" << cf.GetMean() << endl;
      }
      size_t sztcell = icell;
      bool setok = SetCellCalib3(calib_tag,sztcell,cf);
      if( !setok )
      {
        cerr <<" unknown problem in calibrations setting " << endl;
        exit(1);
      }
    }
  }

  if( debug )
  { 
    cout << " Det::InputTimeCalibInfo stop for debug " << GetNameMN() << endl;
    exit(0);
  }
  return 0;

}

////////////////////////////////////////////////////////////////////////////////

int DetMN::InputDetCalibInfo(const MN::AnyID &calib_tag, const string &s )
{
//   bool debug = true;
  bool debug = false;
//  if( GetTBName() == std::string("S2") ) debug = true;

  if(! GetCalibDB3().CalibExist (calib_tag) )
  {
    std::vector <MN::StatInfoMN> vc( NCellsMN() );
    AddCalib3(calib_tag,vc);
  }

  istringstream is(s);
//  char calorim_name[CDB_LINE_MAX],dummy[CDB_LINE_MAX],cellname[CDB_LINE_MAX];
  char calorim_name[CDB_LINE_MAX],dummy[CDB_LINE_MAX];
  string str;
//   int default_format_level = 1;
//    
//   if( XYRegularGrid() && default_format_level == 1 )
//   {
//     cerr <<" WARNING!! CalorimeterMN::InputCalibInfo called for Calorimeter with XY structure redirect to InputCalibInfoXY is droped for a while" << endl;
// //    return InputCalibInfoXY(when, s);
//   }  

  //  Read line of comments
  getline(is,str);
  getline(is,str);
  if( debug ) cout << str << endl;
  if( str.length() >= CDB_LINE_MAX )    // avoid buffer overflow
  {
    cerr <<" DetMN::InputDetCalibInfo too long string " << str << endl;
    exit(1);
  }
  int ret = sscanf(str.c_str(), "%s %s", calorim_name, dummy);
  if( ret != 2 )
  {
    cerr <<" DetMN::InputDetCalibInfo format error " << endl;
    cerr << str << endl;
    exit(1);
  }  
  getline(is,str);
  
  if( debug )
      cout << " Det " << calorim_name <<" with " << dummy  << endl;
  
  //  Read calibration
//  uint not_matched_cells_cnt = 0;
  while( getline(is,str) )
    {
      if( debug ) cout << str << endl;
      int icell;
      float w,c,sc;
      // avoid buffer overflow
      if( str.length() == 0 ) continue; // skip empty line
      if( str.length() >= CDB_LINE_MAX )
      {
        cerr << " FORMAT ERROR TOO LONG (" <<str.length() <<") INPUT LINE MAX(" << CDB_LINE_MAX << ") for InputCalibInfo in line: " << str << endl;
        exit(1);
      }
//      int iret = sscanf(str.c_str(), " %d %g %g %g %s", &icell, &c, &sc, &w, cellname);
      int iret = sscanf(str.c_str(), " %d %g %g %g ", &icell, &c, &sc, &w);
      if( iret == 4 )
      {
//        int jcell = FindCellByNameMN( cellname );
//        if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
//         if( jcell < 0 || (int)NCellsMN() <= jcell )
// 	{
// 	  cout << "Unexpected Cell name " << cellname << " in Calorimeter::InputCalibInfo "
// 	     << GetName() << " " << when << endl;
// 	  cout << "Input string: " << endl;
// 	  cout << str << endl;
// 	  cout << "It well might be that you use wrong calibration file for this detector "
// 	     << GetName() << endl;
// 	  exit(1);       
// 	}
      
//         if( icell != jcell ) {
// 	  icell = jcell;
// 	  not_matched_cells_cnt++;
// 	  if (not_matched_cells_cnt==1) {
// 	    cerr << "Notice: " <<  __func__ << " " << GetName() << " "
// 	       << "cell id and cell name do not match.  (The calibration file "
// 	       << "was produced with a different geometry description, which "
// 	       << "might be an indication that you're using the wrong file.)"
// 	       << endl;
// 	  }
//         }
      }
      else if( iret == 5 )
      {
        cerr << "iret = " << iret <<" but 5 expected " << endl;
        cerr <<" " << GetNameMN() <<" FORMAT ERROR for InputCalibInfo in line " << str << endl;
        exit(1);
//         int jcell = FindCellByNameMN( cellname );
//         if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
//         if( jcell < 0 || (int)NCellsMN() <= jcell )
// 	{
// 	  cout << "Unexpected Cell name " << cellname << " in Calorimeter::InputCalibInfo "
// 	     << GetName() << " " << when << endl;
// 	  cout << "Input string: " << endl;
// 	  cout << str << endl;
// 	  cout << "It well might be that you use wrong calibration file for this detector "
// 	     << GetName() << endl;
// 	  exit(1);       
// 	}
//       
//         if( icell != jcell ) {
// 	  icell = jcell;
// 	  not_matched_cells_cnt++;
// 	  if (not_matched_cells_cnt==1) {
// 	    cerr << "Notice: " <<  __func__ << " " << GetNameMN() << " "
// 	       << "cell id and cell name do not match.  (The calibration file "
// 	       << "was produced with a different geometry description, which "
// 	       << "might be an indication that you're using the wrong file.)"
// 	       << endl;
// 	  }
//         }
      }
      else
      {
        cerr << "iret = " << iret <<" but 5 expected " << endl;
        cerr <<" " << GetNameMN() <<" FORMAT ERROR for InputCalibInfo in line " << str << endl;
        exit(1);
      }  
//       int jcell = FindCellByNameMN( cellname );
//       if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
//       if( jcell < 0 || (int)NCellsMN() <= jcell )
// 	{
// 	  stringstream ss;
// 	  ss << "Unexpected Cell name " << cellname << " in Calorimeter::InputCalibInfo "
// 	     << GetName() << " " << when << endl;
// 	  ss << "Input string: " << endl;
// 	  ss << str << endl;
// 	  ss << "It well might be that you use wrong calibration file for this detector "
// 	     << GetName() << endl;    
// 	  throw Exception( ss.str().c_str() );
// 	}
//       
//       if( icell != jcell ) {
// 	icell = jcell;
// 	not_matched_cells_cnt++;
// 	if (not_matched_cells_cnt==1) {
// 	  cerr << "Notice: " <<  __func__ << " " << GetName() << " "
// 	       << "cell id and cell name do not match.  (The calibration file "
// 	       << "was produced with a different geometry description, which "
// 	       << "might be an indication that you're using the wrong file.)"
// 	       << endl;
// 	}
//       }
      

      if( icell >=0 && icell < (int)NCellsMN() )
      {
        MN::StatInfoMN cf(w,c/1000.,sc/1000.);
//        SetCellInfo(CALIB, when,icell,cf);
        size_t sztcell = icell;
      if( debug )
	  cout << " icell " << icell <<" " << GetCellNameMN(icell) << " c " << c/1000.
	       << " sc " << sc/1000. << " stat " << w <<" Xcheck=" << cf.GetMean() << endl;
        bool setok = SetCellCalib3(calib_tag,sztcell,cf);
        if( !setok )
        {
          cerr <<" unknown problem in calibrations setting " << endl;
          exit(1);
        }
      }
    }
  if( debug )
  { 
    cout << " Det::InputTimeCalibInfo stop for debug " << GetNameMN() << endl;
    exit(0);
  }
  
  return 0;
}

////////////////////////////////////////////////////////////////////////////////

void DetMN::AddCalib3( const MN::AnyID &calib_id, const std::vector<MN::StatInfoMN >&vcf )
{
  GetCalibDB3().AddCalib(calib_id, vcf ); 
}

///////////////////////////////////////////////////////////////////////////////

bool DetMN::SetCellCalib3( const MN::AnyID &calib_id, size_t &icell, const MN::StatInfoMN &cf )
{
  return GetCalibDB3().SetCalib(calib_id, icell, cf ); 
}

///////////////////////////////////////////////////////////////////////////////

void DetMN::Clear( void )
{
  for( int i=0; i< (int)myDigSADC_.size(); i++)
  {
    myDigSADC_[i]->Clear();
  }
}

////////////////////////////////////////////////////////////////////////////////
    
int DetMN::GetNDigSADC ( void ) const
{
  return (int)GetDigSADC().size();
}

/////////////////////////////////////////////////////////////////////////////////

MN::DigSADC* DetMN::GetDigSADC2Modify ( int icell )
{ 
  if(icell <0 || icell >= (int)GetDigSADC().size() ) return 0;
  return GetDigSADC2Modify()[icell];
}

///////////////////////////////////////////////////////////////////////////////

const MN::DigSADC* DetMN::GetDigSADC ( int icell ) const 
{ 
  if(icell <0 || icell >= (int)GetDigSADC().size() ) return 0;
  return GetDigSADC()[icell];
}

////////////////////////////////////////////////////////////////////////////////

} // namespace MN
