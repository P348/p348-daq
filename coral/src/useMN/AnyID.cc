#include <stdio.h>
#include <iostream>

#include "AnyID.h"
// #include "StatInfoMN.h"
// // For CalibDB must be separate file
// // Nu neeetuuuu
// #include "FitInfoMN.h"

using namespace std;
using namespace MN;

namespace MN {

/////////////////////////////////////////////////////////////////////////////////

AnyID::AnyID ( const std::vector < std::string > &names )
{
  for ( size_t i = 0; i < names.size(); i++ )
  {
    names_.push_back( ElementaryID(names[i]) );
  }
}

///////////////////////////////////////////////////////////////////////////////

//AnyID::AnyID ( const std::vector < unsigned int > &names )
AnyID::AnyID ( const std::vector < int > &names )
{
  for ( size_t i = 0; i < names.size(); i++ )
  {
    names_.push_back( ElementaryID(names[i]) );
  }
}

////////////////////////////////////////////////////////////////////////////////

//AnyID::AnyID ( const std::string &name, const std::vector < unsigned int > &names )
AnyID::AnyID ( const std::string &name, const std::vector < int > &names )
{
  names_.push_back( ElementaryID(name) );
  for ( size_t i = 0; i < names.size(); i++ )
  {
    names_.push_back( ElementaryID(names[i]) );
  }
}

////////////////////////////////////////////////////////////////////////////////

const std::string & AnyID::GetName ( const int &layer ) const 
{ 
  if( layer <0 || layer >= (int)names_.size() )
  { 
    cerr <<" AnyID::GetName " << layer <<" bad parameter " << endl;
    exit(1);
  }  
  return names_[layer].second;
}

/////////////////////////////////////////////////////////////////////////////////

std::string AnyID::GetFullNameWithSpacers ( void ) const
{
  std::string full_name;
  for ( size_t i = 0; i < Size(); i++ )
  {
    if( i != 0 ) full_name += std::string(" ");
    full_name += GetName(i);
  }
  return full_name;
}

/////////////////////////////////////////////////////////////////////////////////

std::string AnyID::GetFullNameWithUnderscores ( void ) const
{
  std::string full_name;
  for ( size_t i = 0; i < Size(); i++ )
  {
    if( i != 0 ) full_name += std::string("_");
    full_name += GetName(i);
  }
  return full_name;
}

////////////////////////////////////////////////////////////////////////////////

void AnyID::AddLayer ( const std::string  &name )
{
  names_.push_back( ElementaryID(name) );
}

////////////////////////////////////////////////////////////////////////////////

void AnyID::AddLayer ( int name )
{
  names_.push_back( ElementaryID(name) );
}

////////////////////////////////////////////////////////////////////////////////

bool AnyID::EqualInRange ( const size_t &layer, const AnyID &id ) const
{ 
  if( Size()-layer < id.Size() ) return false;
  for( size_t i=0; i< id.Size(); i++ )
  {
    if( names_[layer+i] != id.names_[i] ) return false;
  }
  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool AnyID::operator == ( const AnyID &id) const
{ 
  if( Size() != id.Size() ) return false;
  for( size_t i=0; i<Size(); i++ )
  {
    if( names_[i] != id.names_[i] ) return false;
  }
  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool AnyID::operator > ( const AnyID &id) const
{ 
  if( Size() > id.Size() ) return true;
  for( size_t i=0; i<Size(); i++ )
  {
    if( names_[i] > id.names_[i] ) 
      return true;
    else if(names_[i] < id.names_[i] )
      return false;
  }
  return false;
}

////////////////////////////////////////////////////////////////////////////////

AnyID &AnyID::operator = ( const AnyID &id)
{
  if( &id!=this )
  {
    names_ = id.names_;
  }
  return *this;  
}

////////////////////////////////////////////////////////////////////////////////

AnyID &AnyID::operator + ( const AnyID &id)
{
  if( &id!=this )
  {
    for( size_t i=0; i<id.Size(); i++ )
    {
      names_.push_back(id.names_[i]);
    }  
  }
  else
  {
    std::vector < ElementaryID > names = names_; 
    for( size_t i=0; i<names.size(); i++ )
    {
      names_.push_back(names[i]);
    }  
  }
  return *this;  
}

////////////////////////////////////////////////////////////////////////////////

const AnyID::ElementaryID  &AnyID::GetLayer( const int &layer ) const
{
  if( layer <0 || layer >= (int)Size() )
  {
    cerr <<" AnyID::GetLayer " << layer <<" bad parameter " << endl;
    exit(1);
  } 
  return names_[layer];
}

////////////////////////////////////////////////////////////////////////////////

//AnyID::ElementaryID::ElementaryID ( unsigned int name ) : id_(name)
AnyID::ElementaryID::ElementaryID ( int name ) : id_(name)
{
  bool debug = false;
  first = true;
  char s[200];
// TODO implement format settings  
//  sprintf(s,"%x",name);
  sprintf(s,"%d",name);
  second = std::string(s);
  if( debug ) cout << " ElementaryID " << name << " presented as " << second << endl;
}

////////////////////////////////////////////////////////////////////////////////

bool AnyID::ElementaryID::operator == ( const ElementaryID &id) const
{ 
  if( first != id.first ) return false;
  if( second != id.second ) return false;
  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool AnyID::ElementaryID::operator != ( const ElementaryID &id) const
{ 
  if( first != id.first ) return true;
  if( second != id.second ) return true;
  return false;
}

////////////////////////////////////////////////////////////////////////////////

bool AnyID::ElementaryID::operator > ( const ElementaryID &id) const
{ 
  if( first > id.first ) return true;
  if( first )
  {
    return id_ > id.id_;
  }
  else
  {
    if( second > id.second ) return true;
    return false;
  }
}

// ////////////////////////////////////////////////////////////////////////////////
// 
// bool AnyID::ElementaryID::operator < ( const ElementaryID &id) const
// { 
//   if( first < id.first ) return true;
//   if( second < id.second ) return true;
//   return false;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// bool AnyID::ElementaryID::operator <= ( const ElementaryID &id) const
// { 
//   if( first > id.first ) return false;
//   if( second > id.second ) return false;
//   return true;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// bool AnyID::ElementaryID::operator >= ( const ElementaryID &id) const
// { 
//   if( first < id.first ) return false;
//   if( second < id.second ) return false;
//   return true;
// }
// 
////////////////////////////////////////////////////////////////////////////////

AnyID::ElementaryID &AnyID::ElementaryID::operator = ( const ElementaryID &id)
{
  if( &id!=this )
  {
    first = id.first;
    second = id.second;
    id_ = id.id_;
  }
  return *this;  
}

// ////////////////////////////////////////////////////////////////////////////////
// 
// bool DetCalibDB::CalibExist( const AnyID &calib_id ) const
// {
//   std::map<AnyID,std::vector<double> >::const_iterator it;
//   if ((it = calib_db_.find(calib_id)) != calib_db_.end())
//     return true;
//   else
//     return false;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void DetCalibDB::AddCalib( const AnyID &calib_id, const std::vector<double> &calib_data )
// {
//   calib_db_.insert( std::pair<AnyID, std::vector<double> >(calib_id,calib_data ) );
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// bool DetCalibDB::SetCalib(const AnyID &calib_id, size_t &icell, const double &cf )
// {
//   if( CalibExist(calib_id) )
//   {
//     std::vector<double>  &vc = GetCalib2Modify(calib_id);
//     if( vc.size() <=icell )
//     {
//       cerr <<" FATAL!!! DetCalibDB3::SetCalib " << calib_id <<" cell  " << icell << " not consistent with calibrations size "<< vc.size() << endl;
//       exit(1);
//     }
//     vc[icell]=cf;
//     return true;
//   }
//   return false;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// const std::vector<double>  &DetCalibDB::GetCalib ( const AnyID &calib_id) const
// {
//   std::map<AnyID,std::vector<double> >::const_iterator it;
//   if ((it = calib_db_.find(calib_id)) != calib_db_.end())
//   {
//     return ( it->second);
//   }
//   else
//   {
//     cerr <<" FATAL!!! DetCalibDB::GetCalib (" << calib_id <<") not exist. failed to find vector "<< endl;
//     exit(1);
//   }
// }
// 
// /////////////////////////////////////////////////////////////////////////////////
// 
// std::vector<double>  &DetCalibDB::GetCalib2Modify ( const AnyID &calib_id)
// {
//   std::map<AnyID,std::vector<double> >::iterator it;
//   if ((it = calib_db_.find(calib_id)) != calib_db_.end())
//   {
//     return ( it->second);
//   }
//   else
//   {
//     cerr <<" FATAL!!! DetCalibDB::GetCalib " << calib_id <<" not exist "<< endl;
//     exit(1);
//   }
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// double DetCalibDB::GetCalib( const AnyID &calib_id, size_t &icell) const
// {
//   std::map<AnyID,std::vector<double> >::const_iterator it;
//   if ((it = calib_db_.find(calib_id)) != calib_db_.end())
//   {
//     if( (it->second).size() > icell )
//       return ( it->second)[icell];
//     else
//     {
//       cerr <<" FATAL!!! DetCalibDB::GetCalib " << calib_id <<", icell=" <<icell <<" not proper format cell  "<< icell << " but max " << (it->second).size() << endl;
//       exit(1);
//     }
//   }
//   else
//   {
//     cerr <<" FATAL!!! DetCalibDB::GetCalib (" << calib_id <<", icell = " <<icell <<" ) not exist "<< endl;
//     exit(1);
//   }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void DetCalibDB3::PrintTags( void ) const
// {
//   cout <<" DetCalibDB3::PrintTags N tags = "<< calib_db_.size() << endl;
//   std::map<AnyID,std::vector<MN::StatInfoMN> >::const_iterator it;
//   for( it=calib_db_.begin(); it!=calib_db_.end(); it++)
//   {
//      cout <<" tag:   " << (it->first ); 
//   }
//   cout << endl;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// bool DetCalibDB3::CalibExist( const AnyID &calib_id ) const
// {
// //   std::map<AnyID,std::vector<MN::StatInfoMN> >::const_iterator it = calib_db_.find(calib_id);
// //   if ( it != calib_db_.end())
// //     return true;
// //   else
// //     return false;
// //   bool debug = true;
//   bool debug = false;
//   if( debug ) cout <<" DetCalibDB3::CalibExist debug search for tag " << calib_id << endl;
//   std::map<AnyID,std::vector<MN::StatInfoMN> >::const_iterator it;
//   for( it=calib_db_.begin(); it!=calib_db_.end(); it++)
//   {
//      if( debug ) cout <<" Compare:   " << (it->first ) ; 
//      if( (it->first ) == calib_id )
//      {
//        if( debug ) cout <<" DetCalibDB3::CalibExist tag found in DB!!!! " << endl;
//        return true;
//      }
//      if( debug ) cout <<" DetCalibDB3::CalibExist NO " << endl;
//   }
//   return false;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void DetCalibDB3::AddCalib( const AnyID &calib_id, const std::vector<MN::StatInfoMN> &calib_data )
// {
//   calib_db_.insert( std::pair<AnyID, std::vector<MN::StatInfoMN> >(calib_id,calib_data ) );
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// bool DetCalibDB3::SetCalib(const AnyID &calib_id, size_t &icell, const MN::StatInfoMN &cf )
// {
//   if( CalibExist(calib_id) )
//   {
//     std::vector<MN::StatInfoMN>  &vc = GetCalib2Modify(calib_id);
//     if( vc.size() <=icell )
//     {
//       cerr <<" FATAL!!! DetCalibDB3::SetCalib " << calib_id <<" cell  " << icell << " not consistent with calibrations size "<< vc.size() << endl;
//       exit(1);
//     }
//     vc[icell]=cf;
//     return true;
//   }
//   return false;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// std::vector<MN::StatInfoMN>  &DetCalibDB3::GetCalib2Modify ( const AnyID &calib_id)
// {
//   std::map<AnyID,std::vector<MN::StatInfoMN> >::iterator it;
//   for( it=calib_db_.begin(); it!=calib_db_.end(); it++)
//   {
//      if( (it->first ) == calib_id )
//      {
//        return (it->second);
//      }
//   }
//   {
//     cerr <<" FATAL!!! DetCalibDB3::GetCalib2Modify " << calib_id <<" not exist "<< endl;
//     exit(1);
//   }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// const std::vector<MN::StatInfoMN>  &DetCalibDB3::GetCalib ( const AnyID &calib_id) const
// {
//   std::map<AnyID,std::vector<MN::StatInfoMN> >::const_iterator it;
//   for( it=calib_db_.begin(); it!=calib_db_.end(); it++)
//   {
//      if( (it->first ) == calib_id )
//      {
//        return (it->second);
//      }
//   }
// 
//   {
//     cerr <<" FATAL!!! DetCalibDB3::GetCalib " << calib_id <<" not exist "<< endl;
//     bool final_check = CalibExist(calib_id);
//     if( final_check )
//       cerr <<" Result is not consistent CalibExist = " << final_check << endl;
//     else  
//      cerr <<" Result is consistent CalibExist = " << final_check << endl;
//     exit(1);
//   }
// 
// //   std::map<AnyID,std::vector<MN::StatInfoMN> >::const_iterator it;
// //   if ((it = calib_db_.find(calib_id)) != calib_db_.end())
// //   {
// //     return ( it->second);
// //   }
// //   else
// //   {
// //     cerr <<" FATAL!!! DetCalibDB3::GetCalib " << calib_id <<" not exist "<< endl;
// //     exit(1);
// //   }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// MN::StatInfoMN DetCalibDB3::GetCalib( const AnyID &calib_id, size_t &icell) const
// {
//   std::map<AnyID,std::vector<MN::StatInfoMN> >::const_iterator it;
//   if ((it = calib_db_.find(calib_id)) != calib_db_.end())
//   {
//     if( (it->second).size() > icell )
//       return ( it->second)[icell];
//     else
//     {
//       cerr <<" FATAL!!! DetCalibDB3::GetCalib " << calib_id <<" not proper format cell  "<< icell << " but max " << (it->second).size() << endl;
//       exit(1);
//     }
//   }
//   else
//   {
//     cerr <<" FATAL!!! DetCalibDB3::GetCalib " << calib_id <<" not exist "<< endl;
//     exit(1);
//   }
// }

////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////////////////////////////////////////////
// 
// void FitInfoStore::Set ( const AnyID & time, AnyInfoMN *info )
// {
// //   AnyInfoMN *fitnew = NULL;
// //   cerr <<" Change interface FitInfoStore::Set " << endl;
// //   exit(1);
// //   AnyInfoMN *fitnew = new FitInfoMN();
// //   *fitnew = info;
//   fit_data_.insert ( std::pair<AnyID, AnyInfoMN *>(time,info) );
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// // Algoritm ne prozrachnyi i skoree vsego nepravil'nyi. Trebuet revizii.
// 
// 
// AnyInfoMN * FitInfoStore::GetFitInfo ( const AnyID & time )
// {
// //  return GetFitInfoTupoPerebor(time);
// //   bool debug = true;
//   bool debug = false;
//   if( debug ) cout <<" GetFitInfo time = " << time << endl;
//   std::map < AnyID, AnyInfoMN * > ::iterator hi = fit_data_.lower_bound(time);
// // ???? Nado-by vniknut' poluchshe v eti lower_bound !
//   std::map < AnyID, AnyInfoMN * > ::const_iterator lob = hi;
//   if(lob == fit_data_.begin() )
//   {
//     if( debug ) cout <<" this is fit_data_.begin element " << endl;
//     if( time == lob->first )
//     {
// //      cout << " FitInfoStore::GetFitInfo EQ! NOPROBLEMSWITH!!!!" << time << endl;
//       return lob->second;
//     }
//     if( debug ) cout <<" Return FitInfo map imestamp in future! THINKABOUT! " << endl;
//     cout <<" Return FitInfo map imestamp in future! THINKABOUT! " << endl;
//     return  NULL;
//   }
//   if(lob != fit_data_.end() )
//   {
//     if( debug ) cout <<" Scan FitInfo in the middle ? time = " << lob->first << endl;
//     if( time == lob->first )
//     {
//       return lob->second;
//     }
//   }
// 
//   if(lob == fit_data_.end() )
//   {
//      if( debug ) cout <<" Scan FitInfo till the end ? " << fit_data_.size() << endl;
//   }
// 
//   lob--;
//   if(lob == fit_data_.end() )
//   {
//     if( debug ) cout <<" Return FitInfo UNKNOWN value for UNKNOWN? time " << endl;
//     cout <<" Return FitInfo UNKNOWN value for UNKNOWN? time " << endl;
//     return  NULL;
//   }
//   if( debug ) cout <<" Return FitInfo for time = " << lob->first << endl;
//   return lob->second;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// AnyInfoMN * FitInfoStore::GetFitInfoTupoPerebor ( const AnyID & time )
// {
//   bool debug = true;
// //   bool debug = false;
// 
//   if(fit_data_.size() == 0 ) return NULL;
//   if( debug ) cout <<" GetFitInfoTupoPerebor time = " << time << endl;
//   std::map < AnyID, AnyInfoMN * > ::iterator it;
//   for( it=fit_data_.begin(); it != fit_data_.end(); it++)
//   { 
//     if( debug ) cout <<" Compare with time = " << it->first << endl;
//     if( it->first >= time ) cout <<" OK Break at time = " << it->first << endl;
//   }
//   if(it ==fit_data_.end()) it--;
//   if( debug ) cout <<" Return FitInfo for time = " << it->first << endl;
//   return it->second;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMN FitInfoStore::SummInRange ( const AnyID & timemin, const AnyID & timemax ) const
// {
// //   bool debug = true;
//   bool debug = false;
//   typedef std::map < AnyID, AnyInfoMN * > ::const_iterator itinfo;
// 
//   if( debug ) cout <<" FitInfoStore::SummInRange debug timemin = " << timemin <<" timemax " << timemax << endl;
//   int ip=0;
//   StatInfoMN summ;
//   for( itinfo itd = fit_data_.begin(); itd != fit_data_.end(); itd++ )
//   {
//     AnyID timepoint = itd->first;
//     AnyInfoMN *datapoint = itd->second;
//     bool pointok = false;
//     if( timepoint >= timemin && timepoint < timemax ) pointok = true;
//     if( pointok ) summ.Add((reinterpret_cast<CalibInfoMN *>(datapoint))->calib_info_);
//     if( debug )
//     { 
//       cout <<" Point " << ip <<" time = " << timepoint <<" Data "<< (reinterpret_cast<CalibInfoMN *>(datapoint))->calib_info_ <<" OK " << pointok << endl;
//     }
//     ip++;
//   }
//   if( debug ) cout <<" Sum " << summ << endl;
//   return summ;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMN FitInfoStore::SummTotal ( void ) const
// {
// //   bool debug = true;
//   bool debug = false;
//   typedef std::map < AnyID, AnyInfoMN * > ::const_iterator itinfo;
// 
//   if( debug ) cout <<" FitInfoStore::SummTotal debug  " << endl;
// //   int ip=0;
//   StatInfoMN summ;
//   for( itinfo itd = fit_data_.begin(); itd != fit_data_.end(); itd++ )
//   {
//     AnyID timepoint = itd->first;
//     AnyInfoMN *datapoint = itd->second;
//     summ.Add((reinterpret_cast<CalibInfoMN *>(datapoint))->calib_info_);
// //     if( debug )
// //     { 
// //       cout <<" Point " << ip <<" time = " << timepoint <<" Data "<< (reinterpret_cast<CalibInfoMN *>(datapoint))->calib_info_ << endl;
// //     }
// //     ip++;
//   }
//   if( debug ) cout <<"  FitInfoStore::SummTotal Sum " << summ << endl;
//   return summ;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// void FitInfoDB::SetFitStore ( const AnyID & calibname )
// {
//   FitInfoStore *s = new FitInfoStore();
//   fit_store_.insert( std::pair< AnyID,FitInfoStore *> (calibname,s) );
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// void FitInfoDB::Set ( const AnyID & calibname, const AnyID & time, AnyInfoMN *info )
// {
// //   cerr <<" Change interface FitInfoDB::Set " << endl;
// //   exit(1);
//   if( GetFitStore( calibname ) == NULL ) SetFitStore(calibname);
//   GetFitStore2Modify( calibname )->Set( time, info);
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// const FitInfoStore * FitInfoDB::GetFitStore( const AnyID & calibname ) const
// {
//   std::map < AnyID, FitInfoStore * > ::const_iterator lob = fit_store_.find(calibname);
//   if ( lob == fit_store_.end() ) return  NULL;
//   return lob->second;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// FitInfoStore * FitInfoDB::GetFitStore2Modify( const AnyID & calibname )
// {
//   std::map < AnyID, FitInfoStore * > ::iterator lob = fit_store_.find(calibname);
//   if ( lob == fit_store_.end() ) return  NULL;
//   return lob->second;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// AnyInfoMN * FitInfoDB::GetFitInfo ( const AnyID & calibname, const AnyID & time )
// {
//   FitInfoStore * store = GetFitStore2Modify( calibname );
//   if( store == NULL )
//    return NULL;
//  return store->GetFitInfo( time );
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// std::string TimeDB::TimeConvert2string( const time_t  &time )
// {
//   char timechar[132];
//   time_t  ltime = time;
//   size_t nt = strftime(timechar, 132, "%a %b %d %T %Y", gmtime( &ltime));
//   return std::string (timechar);
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// std::vector<double> TimeDB::GetData( const time_t  &time ) const
// {
// //   bool debug = true;
//   bool debug = false;
//   vector<double> data;
// //   bool ok= false;
//   if( debug )
//   {
//     string timestr = TimeConvert2string( time );
//     cout << " Calorimeter::AlignmentInTime::GetPosition time = " << timestr <<" MapSize " << map_time2data_.size() << endl;
//   }
//   if( map_time2data_.size() <= 0 ) return data;
// 
//   std::map< time_t, std::vector< double>   >:: const_iterator itb =map_time2data_.begin();
//   std::map< time_t, std::vector< double>   >:: const_iterator ite =map_time2data_.end();
//   std::map< time_t, std::vector< double>   >:: const_iterator itl =ite--;
// 
//   if( debug )
//   {
//     cout << " Compare from time " << TimeConvert2string(itb->first) <<" to " << TimeConvert2string(itl->first) << endl;
//   }
//   
//   std::map< time_t, std::vector< double>   >:: const_iterator itlob = map_time2data_.lower_bound(time);
//   if( itlob !=  ite )
//   {
//     if( debug )
//     {
//       cout << " lower bound time " << TimeConvert2string(itlob->first) << endl;
//     }
//     return (itlob->second);
//      
//   }
//   else
//   {
//     if( debug )
//     {
//       cout << " lower bound at map.end() " << endl;
//     }
//     return data;
//      
//   }
//   return data;
//   
//   
// //   for( size_t it=0; it < map_time2data_.size(); it++ )
// //   {
// //     if( map_time2data_[it].first >= time )
// //     {
// //       if( debug )
// //       {
// //         cout <<" At it = " << it <<" Got it ! " <<  TimeConvert2string( map_time2data_[it].first ) << endl;
// //         if( it > 0 ) cout <<" Previous " << it-1 << TimeConvert2string( map_time2data_[it-1].first ) << endl;
// //       }
// //       assert( map_time2data_[it].second.size() == 4 );
// //       if( it == 0 )
// //       {
// //         const pair < time_t, vector < double > > &p1 =  map_time2data_[it];
// //         position[0]= p1.second[ip_x];
// //         position[1]= p1.second[ip_y];
// //         ok = true;
// //         return pair<bool,vector<double> >(ok,position);
// //       }
// //       else
// //       {
// //         const pair < time_t, vector < double > > &p1 =  map_time2data_[it];
// //         const pair < time_t, vector < double > > &p0 =  map_time2data_[it-1];
// // 
// //         double dpx = p1.second[ip_x]-p0.second[ip_x];
// //         double dpy = p1.second[ip_y]-p0.second[ip_y];
// //         time_t dte = time - p0.first;
// //         time_t dt = p1.first - p0.first;
// //         double drt = 0.;
// //         if( dt > 0 ) drt = double(dte)/double(dt);
// //         dpx = dpx*drt;
// //         dpy = dpy*drt;
// //         if( debug )
// //         {
// //           cout << "  Time = " << TimeConvert2string(p0.first) <<" < " <<
// //                                       TimeConvert2string(time) << " < " <<
// //                                              TimeConvert2string(p1.first) << endl;
// //           cout << " Position is found (mm) x_rel=" << p1.second[0] << " y_rel=" << p1.second[1] <<
// //                                                   " x_abs=" << p1.second[2] << " y_abs=" << p1.second[3] << endl;
// //           cout << " Previous position  x_rel=" << p0.second[0] << " y_rel=" << p0.second[1] <<
// //                                                   " x_abs=" << p0.second[2] << " y_abs=" << p0.second[3] << endl;
// //           cout << " it map = " << it <<" rel time " << dte <<" time gate " << dt << " dpx = " << dpx <<" dpy " << dpy  <<  endl;
// //           cout << " Calculated position  x_rel=" << p1.second[0]-dpx << " y_rel=" << p1.second[1]-dpy <<
// //                                                   " x_abs=" << p1.second[2]-dpx << " y_abs=" << p1.second[3]-dpy << endl;
// //         }
// // //         position[0]= p1.second[0]-dpx;
// // //         position[1]= p1.second[1]-dpy;
// //         position[0]= p0.second[ip_x]+dpx;
// //         position[1]= p0.second[ip_y]+dpy;
// //         ok = true;
// //         return pair<bool,vector<double> >(ok,position);
// //       }
// //     }
// //   }
// // // pick-up last element
// //   const vector < double > &v = map_time2data_.back().second;
// //   if( v.size() == 4 )
// //   {
// //     if( debug ) cout << " Position is found (mm) x_rel=" << v[0] << " y_rel=" << v[1] <<
// //                                                 " x_abs=" << v[2] << " y_abs=" << v[3] << endl;
// //     position[0]= v[ip_x];
// //     position[1]= v[ip_y];
// //     ok = true;
// //     return pair<bool,vector<double> >(ok,position);
// //   }
// //   else
// //   {
// //     cerr << " Error in UpdatePositionInTime wrong vector size = " << v.size() << endl;
// //     assert(false);
// //   }
// //   cerr << " Internal bug in UpdatePositionInTime should never be here " << endl;
// //   assert(false);
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
//       
// const TimeDB * TimeDB2::GetTimeDB ( const AnyID &dbid ) const
// {
//   std::map<AnyID,TimeDB * >::const_iterator it;
//   if ((it = map_timedb_.find(dbid)) != map_timedb_.end())
//     return it->second;
//   else
//     return NULL;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
//       
// const TimeDB2 * DBT::GetDB ( const AnyID &dbid ) const
// {
//   std::map<AnyID,TimeDB2 * >::const_iterator it;
//   if ((it = map_db_.find(dbid)) != map_db_.end())
//     return it->second;
//   else
//     return NULL;
// }
// 
// ///////////////////////////////////////////////////////////////////////////////

} // namespace MN
