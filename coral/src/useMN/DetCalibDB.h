#ifndef DetCalibDB__include
#define DetCalibDB__include

#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <map>

#include "StatInfoMN.h"
#include "AnyID.h"

namespace MN {

////////////////////////////////////////////////////////////////////////////////

class DetCalibDB 
{
  public:
    DetCalibDB ( void ) {}
    bool                          CalibExist ( const AnyID &calib_id ) const;
    void                          AddCalib ( const AnyID &calib_id, const std::vector<double> &calib_data );
    bool                          SetCalib (const AnyID &calib_id, size_t &icell, const double &cf );
    const std::vector<double>  &GetCalib ( const AnyID &calib_id) const;
    std::vector<double>  &GetCalib2Modify ( const AnyID &calib_id);
    double			 GetCalib ( const AnyID &calib_id, size_t &icell) const;
  public:
    std::map < AnyID, std::vector<double> >		 calib_db_;
};

////////////////////////////////////////////////////////////////////////////////

class DetCalibDB3 
{
  public:
    DetCalibDB3 ( void ) {}
    void                          PrintTags( void ) const;
    bool                          CalibExist ( const AnyID &calib_id ) const;
    void                          AddCalib ( const AnyID &calib_id, const std::vector<MN::StatInfoMN> &calib_data );
    bool                          SetCalib (const AnyID &calib_id, size_t &icell, const MN::StatInfoMN &cf );
    const std::vector<MN::StatInfoMN>  &GetCalib ( const AnyID &calib_id) const;
    std::vector<MN::StatInfoMN>  &GetCalib2Modify ( const AnyID &calib_id);
    MN::StatInfoMN			 GetCalib ( const AnyID &calib_id, size_t &icell) const;
  public:
    std::map < AnyID, std::vector<MN::StatInfoMN> >		 calib_db_;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace MN

#endif // DetCalibDB__include
