#include "CsBeamParametrization.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "CsErrLog.h"
#include "CsOpt.h"
#include "CsTrack.h"
#include "CsHelix.h"
#include "CsGeom.h"
#include "TOpt.h"

CsBeamParametrization* CsBeamParametrization::instance_ = 0;

CsBeamParametrization::~CsBeamParametrization() {
  clear();
}

CsBeamParametrization::CsBeamParametrization() {

  CsOpt::Instance()->getOpt("beamParametrization", "params", params);
  if(params.size() != 441) {
    char buffer[10];
    sprintf(buffer, "%u", (unsigned int)params.size());
    std::string message  = "Read " + std::string(buffer) + " parameters ";
    message += "expected 441";
    CsErrLog::Instance()->mes( elFatal, message);
  }

  if(!CsOpt::Instance()->getOpt("beamParametrization", "zRef", zRef)) {
    CsErrLog::Instance()->mes( elFatal, "No zRef specified");
  }
  zRef *= 10.;

  {
    std::string parNamesMean[4] = {"dxMean", "dyMean", "xMean", "yMean"};
    std::string parNamesCut[4] = {"dxCut", "dyCut", "xCut", "yCut"};
    for(unsigned int i = 0; i < 4; ++i) {
      if(!CsOpt::Instance()->getOpt("beamParametrization", parNamesMean[i], meanValues[i])) {
        CsErrLog::Instance()->mes( elFatal, "Cut "+ parNamesMean[i] + " not specified");
      }
      if(!CsOpt::Instance()->getOpt("beamParametrization", parNamesCut[i], cutValues[i])) {
        CsErrLog::Instance()->mes( elFatal, "Cut "+ parNamesCut[i] + " not specified");
      }
    }
  }

  int nmags = CsGeom::Instance()->getCsField()->getNumOfMags();
  CsMagInfo* m =CsGeom::Instance()->getCsField()->getMagInfo();
  for(int i = 0; i < nmags; ++i) {
    if(m[i].mag == 4)
      zSM1 = m[i].zcm;
  }

}

void CsBeamParametrization::make_beam_parametrization(std::list<CsTrack*> & tracks) {
  clear();
  std::list<CsTrack*>::iterator It;
  for(It = tracks.begin(); It != tracks.end(); ++It){
    makeBeam(*It);
  }
}

void CsBeamParametrization::makeBeam(CsTrack* track) {
  //only use tracks which end before SM1 and hence have no measured momentum
  std::list<CsZone*>::const_iterator It;
  for(It = track->getZones().begin(); It != track->getZones().end(); ++It) {
    if((*It)->getZMin() > zSM1)
      return;
  }
  //only use tracks which have a momentum set to the defalut value (cop=TOpt::dCut[4])
  double pTrack = std::fabs(1. / track->getHelices()[0].getCop());
  if( std::fabs(pTrack - TOpt::dCut[4]) > 0.1)
    return;

  CsBeam* new_beam = new CsBeam(*track);

  //parametrize beam using its parameters close to the reference z-position
  const CsHelix* closestHelix = new_beam->getHelixUpstreamOf( zRef );
  double eBeam = getEBeam(closestHelix);
  //in case the parametrization does not give a reliable result(to big values in dX, dY, X, Y)
  //set the beam energy back to the default value and set a flag that the momentum is unreliable
  if( eBeam < 0){
    eBeam = pTrack;
    new_beam->setChi2CutFlag(1);
    new_beam->setBackPropLH(0);
  }
  else {
    new_beam->setChi2CutFlag(0);
    new_beam->setBackPropLH(1);
  }

  double cop = ( (closestHelix->getCop() > 0) ? 1. :  -1. ) / eBeam;
  new_beam->updateHelices(cop);
  new_beam->setTrackTime(new_beam->getMeanTime());
  _beamtracks.push_back(new_beam);

}

//returns Ebeam, negative values in case of not fullfilling the limits
double CsBeamParametrization::getEBeam(const CsHelix* helix) {
  double X = (helix->getX() + (zRef - helix->getZ()) * helix->getDXDZ()) / 10.;
  double Y = (helix->getY() + (zRef - helix->getZ()) * helix->getDYDZ()) / 10.;
  double dX = helix->getDXDZ() * 1000.;
  double dY = helix->getDYDZ() * 1000.;
  if(std::fabs(helix->getDXDZ() - meanValues[0]) > cutValues[0])
    return -1;
  if(std::fabs(helix->getDYDZ() - meanValues[1]) > cutValues[1])
    return -2;
  if(std::fabs(X - meanValues[2]) > cutValues[2])
    return -3;
  if(std::fabs(Y - meanValues[3]) > cutValues[3])
    return -4;
  double X2 = X * X;   double XY = X * Y;     double Y2 = Y * Y;     double X3 = X2 * X;
  double X2Y = X2 * Y; double XY2 = X * Y2;   double Y3 = Y2 * Y;    double X4 = X2 * X2;
  double X3Y = X3 * Y; double X2Y2 = X2 * Y2; double XY3 = X * Y3;   double Y4 = Y2 * Y2;
  double X5 = X3 * X2; double X4Y = X4 * Y;   double X3Y2 = X3 * Y2; double X2Y3 = X2 * Y3;
  double XY4 = X * Y4; double Y5 = Y3 * Y2;
  double XnYm[21];
  XnYm[0] = 1;    XnYm[1] = X;    XnYm[2] = Y;
  XnYm[3] = X2;   XnYm[4] = XY;   XnYm[5] = Y2;
  XnYm[6] = X3;   XnYm[7] = X2Y;  XnYm[8] = XY2;   XnYm[9] = Y3;
  XnYm[10] = X4;  XnYm[11] = X3Y; XnYm[12] = X2Y2; XnYm[13] = XY3;
  XnYm[14] = Y4;
  XnYm[15] = X5;  XnYm[16] = X4Y; XnYm[17] = X3Y2; XnYm[18] = X2Y3;
  XnYm[19] = XY4; XnYm[20] = Y5;
  double dX2 = dX * dX;      double dXdY = dX * dY;   double dY2 = dY * dY;
  double dX3 = dX2 * dX;     double dX2dY = dX2 * dY; double dXdY2 = dX * dY2;
  double dY3 = dY2 * dY;     double dX4 = dX2 * dX2;  double dX3dY = dX3 * dY;
  double dX2dY2 = dX2 * dY2; double dXdY3 = dX * dY3; double dY4 = dY2 * dY2;
  double dX5 = dX3 * dX2;    double dX4dY = dX4 * dY; double dX3dY2 = dX3 * dY2;
  double dX2dY3 = dX2 * dY3; double dXdY4 = dX * dY4; double dY5 = dY3 * dY2;
  double dXndYm[21];
  dXndYm[0] = 1;       dXndYm[1] = dX;     dXndYm[2] = dY;
  dXndYm[3] = dX2;     dXndYm[4] = dXdY;   dXndYm[5] = dY2;
  dXndYm[6] = dX3;     dXndYm[7] = dX2dY;  dXndYm[8] = dXdY2;   dXndYm[9] = dY3;
  dXndYm[10] = dX4;    dXndYm[11] = dX3dY; dXndYm[12] = dX2dY2; dXndYm[13] = dXdY3;
  dXndYm[14] = dY4;    dXndYm[15] = dX5;   dXndYm[16] = dX4dY;  dXndYm[17] = dX3dY2;
  dXndYm[18] = dX2dY3; dXndYm[19] = dXdY4; dXndYm[20] = dY5;
  double Eb = 0;
  for (int i = 0; i < 21; i++) {
    double pp = 0;
    for (int j = 0; j < 21; j++) {
      pp += params[i * 21 + j] * dXndYm[j];
    }
    Eb += pp * XnYm[i];
  }
  if(Eb < 0)
    return -5;
  return Eb;
}

CsBeamParametrization* CsBeamParametrization::Instance() {
  if( instance_ == 0 ){
    instance_ = new CsBeamParametrization();
  }
  return( instance_ );
}

void CsBeamParametrization::clear() {
  std::list<CsBeam*>::iterator beam;
   for(beam = _beamtracks.begin(); beam != _beamtracks.end(); beam++){
     delete *beam;
     *beam = NULL;
   }
   _beamtracks.clear();
}
