#ifndef CsBeamParametrization_h
#define CsBeamParametrization_h
#include <vector>
#include <list>
#include "CsBeam.h"

// class CsTrack;
// class CsBeam;
// class CsHelix;
class CsBeamParametrization{
  public:
    static CsBeamParametrization* Instance();
    void make_beam_parametrization(std::list<CsTrack*> & tracks);
    const std::list<CsBeam*> getBeam() const {return _beamtracks;}

  private:
    CsBeamParametrization();
    ~CsBeamParametrization();
    static CsBeamParametrization* instance_;
    void clear();
    void makeBeam(CsTrack* track);
    double getEBeam(const CsHelix* helix);

    double zRef;
    double zSM1;
    std::vector<double> params;
    std::list<CsBeam*> _beamtracks;
    double meanValues[4];
    double cutValues[4];


};

#endif
