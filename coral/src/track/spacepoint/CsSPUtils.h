/*!
   \file    CsSPUtils.h
   \brief   Compass Space Points Utilities Class.
   \author  Hugo Pereira 
*/


#ifndef CsSPUtils_h
#define CsSPUtils_h

#include "CsSTD.h"

/*! \class CsSPUtils 
    \brief Space Points Utilities Class.

    Contains some utilities to easy handle space points
*/

//class CsSpacePoint;
class CsDetFamily;
class CsSPUtils{
 public:

  static CsSPUtils* Instance( void );

  bool getOptForFamily( const CsDetFamily* pf, std::string tag, std::string key, double &par ); 
  bool getOptForFamily( const CsDetFamily* pf, std::string tag, std::string key, std::string &word ); 
  bool getOptForFamily( const CsDetFamily* pf, std::string tag, std::string key, std::list<std::string> &words);
  std::string getDate( void );
  std::string getTime( void ); 		
protected:

  CsSPUtils( void );  //!< The Constructor

private:

	static CsSPUtils* instance_;			        //!< the singleton instanciation
};

#endif //CsSPUtils_h
