/*!
   \file    CsCalSpacePoint.h 
   \brief   Calibrarion spacepoint, derived from CsSpacePoint Class.
   \author  Hugo Pereira
*/

#ifndef CsCalSpacePoint_h
#define CsCalSpacePoint_h

#include <list>
#include "CsSpacePoint.h"

class CsCluster;
class CsDetector;

class CsCalSpacePoint: public CsSpacePoint {
  
  public: 
	  CsCalSpacePoint( const CsDetFamily &df, std::list<CsCluster*> c, double z, int mode);  	//!< default constructor.				
	  CsCalSpacePoint( const CsCalSpacePoint& sp):CsSpacePoint( sp ) { *this = sp; }  //!< copy constructor.
	  CsCalSpacePoint& operator = ( const CsCalSpacePoint& );   	 //!< assignment operator.
    
    CsDetector* detOff_;
    bool found_;
    CsCluster* clFound_;
};

#endif
