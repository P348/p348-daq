/*!
  \file    CsTrafficUtils.h
  \brief   Coral interface to Traffic Tracking Utils == TLatticeGen (so far)
*/

#ifndef CsTrafficUtils_h
#define CsTrafficUtils_h

#include "CsSTD.h"
#include "CsTrkUtils.h"

/*! \class CsTrafficUtils 
    \brief Coral interface to Traffic Tracking Utils
*/

class CsTrafficUtils : public CsTrkUtils {

 public:

  /*! \fn CsTrafficUtils()
    \brief ...
  */
  CsTrafficUtils();

  /*! \fn ~CsTrafficUtils()
    \brief ...
  */
  ~CsTrafficUtils();

  /*! \fn bool genLattice()
    \brief ... 
  */
  bool genLattice();

 private:



};

#endif //CsTrafficUtils_h








