/*!
  Method for debug purposes: dump hits to screen
  \param mode &=0x1: dico residuals, &=0x4: more info
*/

#include <stdio.h>
#include "TSetup.h"
#include "TEv.h"
#include "TTrack.h"
#include "THit.h"
#include "TLattice.h"

using namespace std;

void TTrack::DumpHits(int mode = 0)
{
  const TSetup &setup = TSetup::Ref();
  TEv &ev = TEv::Ref();

  printf("TTrack.Id %d\n",Id);

  // ***** PRINT A 1ST LISTING: Plane_#  Hit_#  Hit_Kine                *****
  // *****   OR if NOT GENUINE:                                         *****
  // *****                      Plane_#  Hit_#  Hit_Kine.LRProb ...     *****
  // *****                  ...          U      U_counterpart           *****
  // ***** (BEFOREHAND RECAST as GENUINE a MIRROR CLOSE TO COUNTERPART) *****

  vector<THit>& vHit = const_cast<vector<THit>&>(ev.vHit());
  list<int>::iterator ih; int igr, igrp, nht = 0;
  for (int iter = 0; iter<((mode&0x4) ? 2 : 1); iter++) {
    igr = 0; ih = lHitPat.begin(); while(ih != lHitPat.end()) {
      if (*ih<0) { ih++; continue; } 

      THit &h = vHit[*ih];  // *************** LOOP OVER HITS ***************

      igrp = igr;                                 // ***** NEW ZONE? => NEW LINE
      while (igr<int(setup.vIplFirst().size())) {
	if (setup.vIplFirst()[igr]<=h.IPlane && h.IPlane<=setup.vIplLast()[igr])
	  break;
	igr++;
      }
      if (igr!=igrp) { printf("\n"); nht = 0; }

      if (h.IKine<-1) {                           // ***** NON GENUINE MIRROR...
	printf(" %3d %4d %6.2f",h.IPlane,h.IHit,
	       -2-h.IKine+h.PtrClus()->getLRProb());
	if (!(++nht%5)) printf("\n");  // New line?
	if (h.Mirror>=0) printf(" %7.2f %7.2f",h.U,ev.vHit()[h.Mirror].U);
	else             printf(" ??????? ???????");
      }
      else {                                                    // ***** ...ELSE
	if (iter) printf(" %3d %4d %6.2f",h.IPlane,h.IHit,h.U);
	else      printf(" %3d %4d %6d",  h.IPlane,h.IHit,h.IKine);
      }
      if (!(++nht%5)) printf("\n");  // New line?

      const TPlane &p = setup.vPlane(h.IPlane);  // ********** PIXEL **********
      const TDetect &d = setup.vDetect(p.IDetRef);
      if (d.IType==29 || d.IType==32) {
	if (iter) printf(" %3d %4d %6.2f",h.IPlane,h.IHit,h.V);
	else      printf(" %3d %4d %6d",  h.IPlane,h.IHit,h.IKine);
	if (!(++nht%5)) printf("\n");  // New line?
      }
      ih++;
    }                                  // End of loop over hits
    printf("\n#Hits,#NDFs %d,%d",NHits,NDFs);
    if (Type==0x10 && (mode&0x1)) printf(", chi2 %.2f\n",Chi2tot/(NDFs-4));
    else printf("\n");
  }

  double chi2u = 0, chi2v = 0;
  if (mode&0x1) {
    if (!vGuests.empty()) {

      // Reference to "TLattice". In view of determining whether given plane
      // has meaningfull guestimates. This is not very satisfying to have a
      // refernce to "TLattice". Best would have been to have "TTrack" (which
      // could had guestimates determined by other mean than "TLattice")
      // return the answer. But for the time being...
      const TLattice *lat = TLattice::Ptr();    

      // ***** RESIDUALS
      // ***** PRINT A 2ND LISTING: Plane_#  Hit_#  Residual             *****
      // *****      IF NOT GENUINE: ...      Hit_#  Residual_counterpart *****

      int ipl0 = -1;
      for (igr = 0; igr<2; igr++)           // First plane where guestimate
	if (GuestsGroups&(1<<igr)) { ipl0 = setup.vIplFirst()[igr]; break; }
      if (ipl0<0) return;

      ih = lHitPat.begin() ; nht=igr = 0; while (ih!=lHitPat.end()) {
	if (*ih<0) { ih++; continue; }

	const THit& h = vHit[*ih]; int jpl = h.IPlane-ipl0;

	if (jpl>=(int)vGuests.size()) break;
	// Guestimates end w/ QNewtonFit range
	if      (lat->dico_idx[h.IPlane]==-1) { ih++; continue; }
	else if (lat->dico_idx[h.IPlane]==-2) break;

	//        *************** LOOP OVER HITS w/in DICO ***************

	igrp = igr;                                 // ***** NEW ZONE? => NEW LINE
	while (igr<int(setup.vIplFirst().size())) {
	  if (setup.vIplFirst()[igr]<=h.IPlane && h.IPlane<=setup.vIplLast()[igr])
	    break;
	  igr++;
	}
	if (igr!=igrp) { printf("\n"); nht = 0; }

	printf(" %3d %4d %6.2f",h.IPlane,h.IHit,(h.U-vGuests[jpl])/h.SigU);

	if (h.IKine<-1) {                           // ***** NON GENUINE MIRROR...
	  if (!(++nht%5)) printf("\n");
	  if (h.Mirror>=0)
	    printf("     %4d %6.2f",h.IHit,vGuests[jpl]-ev.vHit()[h.Mirror].U);
	  else
	    printf(" ???????? ??????");
	}
	if (!(++nht%5)) printf("\n");

	const TPlane &p = setup.vPlane(h.IPlane);  // ********** PIXEL **********
	const TDetect &d = setup.vDetect(p.IDetRef);
	if (d.IType==29 || d.IType==32) {
	  printf(" %3d %4d %6.2f",h.IPlane,h.IHit,(h.V-vGuests[jpl-2])/h.SigV);
	  if (!(++nht%5)) printf("\n");  // New line?
	}
	ih++;
      }                     // End of loop over hits
      printf("\n");
    }
    else if (Type==0x10 || (mode&0x2)) {
      // ***** RESIDUALS FOR STRAIGHT FIT
      // ***** PRINT A 2ND LISTING: Hit#  Time  Residual             *****
      // *****      IF NOT GENUINE: Hit#  ...   Residual_counterpart *****
      float x0 = Hfirst(0), y0 = Hfirst(1), z0 = Hfirst(2);
      float                 yp = Hfirst(3), zp = Hfirst(4);

      // Track time: first backup existing timing, to be later restored so that
      // the "DumpHits" debugging tool does not affect reconstruction
      float meanT = MeanTime, sigmaT = SigmaTime, dispT = DispTime;
      UseHitTime();
      if (SigmaTime>0) printf(" %.2f+/-%.2f(disp) ns\n",MeanTime,DispTime);

      ih = lHitPat.begin(); nht = 0;
      while (ih!=lHitPat.end()) {
	if (*ih<0) { ih++; continue; }

	//        *************** LOOP OVER HITS ***************

	const THit &h = vHit[*ih]; int ipl = h.IPlane;
	const TDetect &d = setup.vDetect(ipl);
	float cu = d.Ca, su = d.Sa;
	float y = y0+yp*(d.X(0)-x0), z = z0+zp*(d.X(0)-x0);
	float u = y*cu+z*su, v = z*cu-y*su;

	double du = (h.U-u)/h.SigU; chi2u += du*du;
	if (h.SigT>0 && (d.IType==22 /* scifi */ || d.IType>40 /* hodo */))
	  printf("   %6.2f %6.2f", (h.Time-MeanTime)/h.SigT,du);
	else
	  printf(" %3d %4d %6.2f", h.IPlane,h.IHit,         du);

	if (!(++nht%5)) printf("\n");

	if (d.IType==29 || d.IType==32) { // ********** PIXEL **********
	  double dv = (h.V-v)/h.SigV; chi2v += dv*dv;
	  printf("     %4d %6.2f",           h.IHit,         dv);
	  if (!(++nht%5)) printf("\n");  // New line?
	}

	ih++;
      }                     // End of loop over hits
      printf("\n");
      // Restore original timing
      float MeanTime = meanT, SigmaTime = sigmaT, DispTime = dispT;
    }
  }

  // ***** TTrack Kine, CHI2

  FindKine(); printf("Kine = %d, 0x%02x",IKine,IFit);
  if (IFit&0x8) {
    if (NGroups()==1) {
      if (Chi2aux)    printf(" Dico Chi2 %.2f",Chi2aux/(NDics-4));
      else            printf(" Dico Chi2 %.2f",Chi2tot/(NDics-4));
    }
    else              printf(" Dico Chi2 %.2f",Chi2tot/(NDics-5));
  }
  if (IFit&0x67) {
    if (NGroups()==1) {
      if (NDFs>4)    printf(" KF Chi2 %.2f",Chi2tot/(NDFs-4));
    }
    else             printf(" KF Chi2 %.2f",Chi2tot/(NDFs-5));
  }
  if (Hfirst(5)) printf(" 1st=%.3f GeV",1/Hfirst(5));
  if (Hlast(5))  printf(" last=%.3f GeV",1/Hlast(5));
  if (chi2u && NDFs>4) {
    printf(" chi2 %.2f",chi2u/(NDFs-4));
    if (chi2v) printf(" %.2f",chi2v/(NDFs-4));
  }
  printf(" ----------------------\n");
  return;
}

