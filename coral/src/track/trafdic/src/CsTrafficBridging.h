/*!
   \file    CsTrafficBridging.h
   \author  Benigno.Gobbo@cern.ch
*/

#ifndef CsTrafficBridging_h
#define CsTrafficBridging_h

#include "CsSTD.h"
#include "CsTrkBridging.h"

/*! \class CsTrafficBridging 
    Coral interface to Traffic segment bridging
*/

class CsTrafficBridging : public CsTrkBridging {

 public:

  CsTrafficBridging();           //!< constructor
  ~CsTrafficBridging();          //!< destructor

  /*!
    Interface to Traffic segment bridging
    \sa TEv::BridgeSegments()
  */
  bool doBridging( std::list<CsTrack*>& tracks, std::list<CsCluster*>& clusters );
};

#endif //CsTrafficBridging_h
