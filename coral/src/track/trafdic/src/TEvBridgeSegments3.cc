
#include <iostream>
#include "TOpt.h"
#include "TDisplay.h"
#include "TSetup.h"
#include "TEv.h"
#include "CsHistograms.h"

using namespace std;

/*! 
  \brief Pair of track segments

  Local class of TEv::BridgeSegments function
  used for sorting of track segment pair candidates
*/
class TPairToBridge {
public:
  double Chi2;                //!< track candidate Chi2
  double Pinv;                //!< track candidate q/P
  double ErrP;                //!< track candidate Sigma(q/P)^2
  list<TTrack>::iterator iL;  //!< "upstream"   track segment iterator
  list<TTrack>::iterator iR;  //!< "downstream" track segment iterator
  //! "Less" operator
  bool operator < (const TPairToBridge& tp) const { return (Chi2 < tp.Chi2);} 
};

/*!

  Bridge track segments through the NA64 magnet 

*/

void TEv::BridgeSegments3()
{
  if(TOpt::ReMode[3] > 0) return; // bridging is OFF

  const double max_step = 100.; // [cm]
  
  const TSetup& setup     = TSetup::  Ref();
  TDisplay& display = TDisplay::Ref();

  bool hist(false); 
  if(TOpt::Hist[4] > 0) hist=true;
  int print = TOpt::Print[4]; 

  static bool first=true;
  static CsHist1D*   h[55];
  static CsHist2D* hh[50];
  bool ret;


  if(first && hist){
    cout<<endl<<"NA64 magnet bridging"<<endl<<endl;
    first=false;
    CsHistograms::SetCurrentPath("/Traffic/BridgeSegment3");    
    h[ 0]  = new CsHist1D("h00", "All pairs Delta Y (cm)",     400, -20, 20);
    h[ 1]  = new CsHist1D("h01", "All pairs Delta Z (cm)",     400, -20, 20);
    h[ 2]  = new CsHist1D("h02", "All pairs Delta Azi (mrad)", 400, -40,  0);
    h[ 3]  = new CsHist1D("h03", "All pairs Delta Dip (mrad)", 400, -20, 20);
    h[ 4]  = new CsHist1D("h04", "All pairs Delta Yp * 1000",  400, -40,  0);
    h[ 5]  = new CsHist1D("h05", "All pairs Delta Zp * 1000",  400, -20, 20);
    h[ 8]  = new CsHist1D("h08", "P estimated [GeV]",          800, 80, 120);
    h[ 9]  = new CsHist1D("h09", "Y_left_first - Y_extrapolated_right [cm] (assumming -100 GeV)", 400, -10, 10);
    h[10]  = new CsHist1D("h10", "Y_left_last  - Y_extrapolated_right [cm] (assumming -100 GeV)", 400, -10, 10);
    h[11]  = new CsHist1D("h11", "Y_right_first - Y_extrapolated_left [cm] (assumming -100 GeV)", 400, -10, 10);
    h[12]  = new CsHist1D("h12", "Y_right_last  - Y_extrapolated_left [cm] (assumming -100 GeV)", 400, -10, 10);
    h[16]  = new CsHist1D("h16", "All pairs Chi2/ndf",         200,   0, 1000);
    h[17]  = new CsHist1D("h17", "All pairs Chi2/ndf (zoom)",  200,   0,  100);

    h[28]  = new CsHist1D("LTxatm",  "Left Tracks Y (cm) at mag. center",        400, -20, 20);
    h[29]  = new CsHist1D("LTyatm",  "Left Tracks Z (cm) at mag. center",        400, -20, 20);
    h[30] = new CsHist1D("LTdxdz",   "Left Tracks dy/dx (mrad)",  600, -30, 30);
    h[31] = new CsHist1D("LTdydz",   "Left Tracks dz/dx (mrad)",  600, -30, 30);
    h[32] = new CsHist1D("RTxatm",   "Right Tracks Y (cm) at mag. center",       400, -20, 20);
    h[33] = new CsHist1D("RTyatm",   "Right Tracks Z (cm) at mag. center",       400, -20, 20);
    h[34] = new CsHist1D("RTdxdz",   "Right Tracks dy/dx (mrad)", 600, -30, 30);
    h[35] = new CsHist1D("RTdydz",   "Right Tracks dz/dx (mrad)", 600, -30, 30);

    h[47]  = new CsHist1D("zLTxatm",  "ZOOM: Left Tracks Y (cm) at mag. center",        400, -5, 5);
    h[48]  = new CsHist1D("zLTyatm",  "ZOOM: Left Tracks Z (cm) at mag. center",        400, -5, 5);
    h[49] = new CsHist1D("zLTdxdz",   "ZOOM: Left Tracks dy/dx (mrad)",  600, -5, 5);
    h[50] = new CsHist1D("zLTdydz",   "ZOOM: Left Tracks dz/dx (mrad)",  600, -5, 5);
    h[51] = new CsHist1D("zRTxatm",   "ZOOM: Right Tracks Y (cm) at mag. center",       400, -5, 5);
    h[52] = new CsHist1D("zRTyatm",   "ZOOM: Right Tracks Z (cm) at mag. center",       400, -5, 5);
    h[53] = new CsHist1D("zRTdxdz",   "ZOOM: Right Tracks dy/dx (mrad)", 600, -5, 5);
    h[54] = new CsHist1D("zRTdydz",   "ZOOM: Right Tracks dz/dx (mrad)", 600, -30, -20);


    h[36] = new CsHist1D("h36", "Momentum from angle", 800, 80, 120);
    h[37] = new CsHist1D("h37", "ZOOM: All pairs Delta Y (cm)",     1000, -5, 5);
    h[38] = new CsHist1D("h38", "ZOOM: All pairs Delta Z (cm)",     1000, -5, 5);
    h[39] = new CsHist1D("h39", "ZOOM: All pairs Delta Yp*1000 (mrad)", 1000, -26, -16);
    h[40] = new CsHist1D("h40", "ZOOM: All pairs Delta Zp*1000 (mrad)", 1000, -5, 5);


    h[41] = new CsHist1D("h41", "NHits in Left Track", 9,-0.5,8.5);
    h[42] = new CsHist1D("h42", "NHits in Right Track", 17,-0.5,16.5);
    h[43] = new CsHist1D("h43", "Number of Left Tracks", 5,-0.5,4.5);
    h[44] = new CsHist1D("h44", "Number of Right Tracks", 12,-0.5,11.5);
    h[45] = new CsHist1D("h45", "Number of Good Track Pairs", 5,-0.5,4.5);
    h[46] = new CsHist1D("h46", "Track Status", 16,-0.5,15.5);


    hh[1] = new CsHist2D("hh01","Estimated momentum [GeV] VS incoming track Azi angle [mrad]", 200, -2.0, 2.0, 200, 90., 110.);
    hh[2] = new CsHist2D("hh02","Estimated momentum [GeV] VS incoming track Dip angle [mrad]", 200, -2.0, 2.0, 200, 90., 110.);
    hh[3] = new CsHist2D("hh03","Fitted Y  VS X  [cm]",   200, -2.5, 2.5, 200, -2.5, 2.5);
    CsHistograms::SetCurrentPath("/");
  }
 
  TPairToBridge tp;
  list<TPairToBridge> lTrackPair;
  list<TPairToBridge>::iterator itp;
  
  int imag=0;


  TTrack tr,tl,tx;

  double x0(0), fldint(0);
  bool good_pair;
  bool bad_pair ;
  float pimc(0), pinv, pinv_prev;
  double chi2, chi2_prev;
  int iter;
  int ntl,ntr;
  ntl=0;
  ntr=0;
  
  if(print > 0) cout<<" \n\n================> TEvBridgeSegments3 ============"<<endl;

  list<TTrack>::iterator il,ir;
  for(il=listTrack.begin(); il != listTrack.end(); il++) {  // loop over left track pieces
    if((*il).Type != 1) {
        ntr++;
        continue;
    }
    ntl++;
    tl = (*il); // copy left track

    for(ir=listTrack.begin(); ir != listTrack.end(); ir++) {  // loop over right track pieces
      if(il == ir) continue; // the same
      if((*ir).Type != 2) continue;
      tr = (*ir); // copy right track

      good_pair=false; // = true for correct pair of well reconstructed tracks (MC only)
      bad_pair =false; // = true for wrong   pair of well reconstructed tracks (MC only)

      if(tl.IKine >=0 && tr.IKine >=0 && tl.IKine == tr.IKine) {
	good_pair = true;
	pimc = float(vecKine[tl.IKine].Pinv()); 
      } 

      if(TOpt::ReMode[6] > 0 && (!good_pair) ) continue; // "ideal" bridging (MC case)

      if(tl.IKine >=0 && tr.IKine >=0 && tl.IKine != tr.IKine) {
	bad_pair  = true;
	pimc = float(vecKine[tl.IKine].Pinv()); // take left track piece mom. 
      } 

      if(hist){
	if(good_pair) {
	  float finteg=(tl.Hfirst(3)-tr.Hlast(3))/(1.E-3 * 0.3 * vecKine[tl.IKine].Pinv());
	}
	if(bad_pair) {
	  ///HF1(TOpt::Hoffs+imag*100+15,(1./fabs(pimc)),1.);
	}
      }

      x0 =   setup.MagCenter(imag, 0);
      fldint=setup.MagFieldInt[imag];
	
      // Rough track par. comparisson
      double dazi= tr.Hfirst.azi() - tl.Hlast.azi();
      double ddip= tr.Hfirst.dip() - tl.Hlast.dip();
      double dyp= tr.Hfirst(3) - tl.Hlast(3);
      double dzp= tr.Hfirst(4) - tl.Hlast(4);
      double dy  = (tr.Hfirst(1) + (x0-tr.Hfirst(0))*tr.Hfirst(3)) 
	- (tl.Hlast (1) + (x0-tl.Hlast (0))*tl.Hlast (3));
      double dz  = (tr.Hfirst(2) + (x0-tr.Hfirst(0))*tan(tr.Hfirst.dip())) 
	- (tl.Hlast (2) + (x0-tl.Hlast (0))*tan(tl.Hlast.dip()));

      //Estimate momentum
      pinv=(tl.Hlast(3)-tr.Hfirst(3))/(1.E-3 * 0.3 * fldint);
      double mom_estim= -4.*0.2994*1.7/dazi;

      //
      // Propagate (assuming momentum -100 GeV) right track to the left track's first and last points 
      // to look on difference in y (bending plane) coordinate
      //

      THlx hr = tr.Hfirst;
      hr(5)=(1./-100.); // -100 GeV
      THlx hr_ex;
      hr.Extrap(tl.Hfirst(0), hr_ex, max_step);
      double dy_l_first = tl.Hfirst(1)-hr_ex(1);
      //display.SetRkutraj(true);
      hr.Extrap(tl.Hlast (0), hr_ex, max_step);
      //display.SetRkutraj(false);
      double dy_l_last  = tl.Hlast(1)- hr_ex(1);

      //
      // Propagate (assuming momentum -100 GeV) left track to the right track's first and last points 
      // to look on difference in y (bending plane) coordinate
      //
      THlx hl = tl.Hlast;
      hl(5)=(1./-100.); // -100 GeV
      THlx hl_ex;

      //display.SetRkutraj(true);
      hl.Extrap(tr.Hfirst(0), hl_ex, max_step);
      double dy_r_first = tr.Hfirst(1)-hl_ex(1);
      //display.SetRkutraj(false);
      hl.Extrap(tr.Hlast (0), hl_ex, max_step);
      double dy_r_last  = tr.Hlast(1)- hl_ex(1);

      if(hist){
	if(tr.NHits >= 8){ // only the best tracks after magnet
	  h[0]->Fill(dy);
	  h[1]->Fill(dz);
	  h[2]->Fill(dazi*1000.);
	  h[3]->Fill(ddip*1000.);
	  h[4]->Fill(dyp*1000.);
	  h[5]->Fill(dzp*1000.);
	  h[ 9]->Fill(dy_l_first);
	  h[10]->Fill(dy_l_last);
	  h[11]->Fill(dy_r_first);
	  h[12]->Fill(dy_r_last);
	}

	h[28]->Fill((tl.Hlast (1) + (x0-tl.Hlast (0))*tl.Hlast (3)));
	h[29]->Fill((tl.Hlast (2) + (x0-tl.Hlast (0))*tl.Hlast (4)));
	h[30]->Fill(1000*(tl.Hlast(3)));
	h[31]->Fill(1000*(tl.Hlast(4)));
	
	h[32]->Fill((tr.Hfirst(1) + (x0-tr.Hfirst(0))*tr.Hfirst(3)));
	h[33]->Fill((tr.Hfirst(2) + (x0-tr.Hfirst(0))*tr.Hfirst(4)));
	h[34]->Fill(1000*(tr.Hfirst(3)));
	h[35]->Fill(1000*(tr.Hfirst(4)));

	h[47]->Fill((tl.Hlast (1) + (x0-tl.Hlast (0))*tl.Hlast (3)));
	h[48]->Fill((tl.Hlast (2) + (x0-tl.Hlast (0))*tl.Hlast (4)));
	h[49]->Fill(1000*(tl.Hlast(3)));
	h[50]->Fill(1000*(tl.Hlast(4)));
	
	h[51]->Fill((tr.Hfirst(1) + (x0-tr.Hfirst(0))*tr.Hfirst(3)));
	h[52]->Fill((tr.Hfirst(2) + (x0-tr.Hfirst(0))*tr.Hfirst(4)));
	h[53]->Fill(1000*(tr.Hfirst(3)));
	h[54]->Fill(1000*(tr.Hfirst(4)));

	h[36]->Fill(mom_estim);
	h[37]->Fill(dy);
	h[38]->Fill(dz);
	h[39]->Fill(dyp*1000.);
	h[40]->Fill(dzp*1000.);
	h[41]->Fill(tl.NHits);
	h[42]->Fill(tr.NHits);
	hh[1]->Fill(tl.Hfirst.azi()*1000., fabs(1./pinv));
	hh[2]->Fill(tl.Hfirst.dip()*1000., fabs(1./pinv));
	hh[3]->Fill(tl.Hfirst(1), tl.Hfirst(2));
      }
	
      if(print > 0){
	cout <<"\n\n BridgeSegments3 ==> "<<tl.Id<<"("<<tl.IKine<<")   "<<tr.Id<<"("<<tr.IKine<<")";
	if(good_pair) cout<<"  Pmc = "<<1./pimc;
	cout<<"  mag = "<<imag<<" FldInt = "<<fldint<<" x0 = "<<x0<<endl;
	cout<<" Delta Azi (mrad) "<<dazi*1000.<<"   Delta Dip (mrad) = "<<ddip*1000 <<"    Delta Z = "<<dz<<endl;
      }
      if(hist){
	if(good_pair){ // correct bridge
	  ///HF2(TOpt::Hoffs+100*imag+4,(1./fabs(pimc)), float(fabs(ddip)),1.);
	  ///HF2(TOpt::Hoffs+100*imag+5,(1./fabs(pimc)), float(fabs(dz)),  1.);
	}
	if(bad_pair){ // incorrect bridge
	  ///HF1(TOpt::Hoffs+100*imag+7,float(fabs(ddip)),1.);
	  ///HF1(TOpt::Hoffs+100*imag+8,float(fabs(dz)),1.);
	}
      }

      // Cuts
      if(TOpt::ReMode[6] == 0 ){
    if(fabs(ddip*1000.) > 5.0)   {
        if (hist) h[46]->Fill(0);
        continue;}
    if(fabs(dz)         > 5.0)   {
        if (hist) h[46]->Fill(1);
        continue;}
      }

      if(hist){
	if(good_pair) {
	  ///HF1(TOpt::Hoffs+imag*100+11,(1./fabs(pimc)),1.);
	}
      }


      if(print > 0) {
	cout<<" P estim 1. =  "<<1./pinv<<endl;
	cout<<" P estim 2. =  "<<1./pinv<<endl;
      }

      if(hist){
	h[8]->Fill(fabs(1./pinv));
      }

	
      const int idir = -1; // direction of fit (-1 - backward)

      // Cut
      if(TOpt::ReMode[6] == 0 ){
    if(fabs(1./pinv) > 500. || fabs(1./pinv) < 0.1) {
        if (hist) h[46]->Fill(2);
        goto next_pair;}
      }

      if(hist){
	if(good_pair) {
	  ///HF1(TOpt::Hoffs+imag*100+12,(1./fabs(pimc)),1.);
	}
      }

      // Build merged track
      tx=tl; tx.Append(tr,"tmp"); tx.Chi2tot = 1e6;

      
      // Initial values
      chi2=1.E10; 
      if(idir == -1) {
	tx.Hfirst(5) = pinv; tx.Hfirst(5,5) = 1.E-4;
      } else if (idir == 1){
	tx.Hlast(5)  = pinv; tx.Hlast(5,5) = 1.E-4;
      } else {
	assert(false);
      }

      // Super ideal bridge: get init value from MC
      if (TOpt::ReMode[6] > 1) tx.Hfirst(5) = pimc;


      iter = 0;
      while(iter < 15) { //  <---------- iteration loop


	chi2_prev = chi2;
	if(idir == -1) {
	  pinv_prev     = tx.Hfirst(5); 
	  tx.Hlast(5)   = tx.Hfirst(5);  
	  tx.Hlast(5,5) = tx.Hfirst(5,5);  // set momentum and error
	} else if (idir == 1){
	  pinv_prev      = tx.Hlast(5);
	  tx.Hfirst(5)   = tx.Hlast(5);  
	  tx.Hfirst(5,5) = tx.Hlast(5,5);  // set momentum and error
	} else {
	  assert(false);
	}

	if((TOpt::Graph[6] & 0x1) != 0) display.SetRkutraj(true);
	ret = tx.QuickKF3(idir,1);
	if((TOpt::Graph[6] & 0x1) != 0) display.SetRkutraj(false);


    if( ! ret ) {
        if (hist) h[46]->Fill(3);
        goto next_pair;}  // Kalman fit failed. Next pair.

	chi2=tx.Chi2tot/tx.NHits;

	if(print > 0){
	  cout<<" Iteration # :  "<<iter<<endl;
	  cout<<" Chi2/Nhits = "<<chi2<<endl;
	}
	if(hist && iter == 0){
	  h[16]->Fill(tx.Chi2tot/(tx.NHits - 5));
	  h[17]->Fill(tx.Chi2tot/(tx.NHits - 5));
	  if(good_pair){
	    ///HF1(TOpt::Hoffs+100*imag+17,float(chi2),1.);
	  }
	  if(bad_pair){
	    ///HF1(TOpt::Hoffs+100*imag+18,float(chi2),1.);
	  }
	}

	// Cuts
	if(TOpt::ReMode[6] == 0 ) {
	  if(fabs(1./tx.Hfirst(5)) < 0.1) {
	    if(print > 1) cout<<" Too small momentum: "<<1./tx.Hfirst(5)<<endl;
        if (hist) h[46]->Fill(4);
        goto next_pair;
	  }
	  if(fabs(1./tx.Hfirst(5)) > 500.) {
	    if(print > 1) cout<<" Too big momentum: "<<1./tx.Hfirst(5)<<endl;
        if (hist) h[46]->Fill(5);
        goto next_pair;
	  }
	  if(chi2 > TOpt::dCut[7]) {
	    if(print > 1) cout<<" Didn't passed preliminary Chi2 cut (dCut[7]) "<<TOpt::dCut[7]<<endl;
        if (hist) h[46]->Fill(6);
        goto next_pair;
	  }
	}

	if(print > 0){
	  cout<<" P = "<<1./tx.Hfirst(5)<<"\t  Err = "<<sqrt(tx.Hfirst(5,5))<<endl;
	}

	// Convergency test
	int ichi2(0), ichi2_prev(0);
	ichi2      = int(chi2      * 100); // keep only 2 decimal digit
	ichi2_prev = int(chi2_prev * 100); // keep only 2 decimal digit

	if(ichi2 == ichi2_prev) {
	  if(print > 1) cout<<" ---> Converged!  Chi2_prev = "<<chi2_prev<<"  chi2 = "<<chi2<<endl;
      if (hist) h[46]->Fill(7);
      break; // converged
	}
	  
	iter++;
	if(TOpt::ReMode[6] > 0 ) break; // only one iteration for "ideal" bridging
      }; // <---------- end of iteration loop 

      if(iter == 15 && TOpt::Print[0] != 0) {
	cout<<"TEv::BridgeSegments3() ==> Maximum number of iterations ("<<iter<<") is reached. Mag = "<<imag;
	if(good_pair) cout<<"  (good  pair) ";
	if(bad_pair ) cout<<"  (wrong pair) ";
	cout<<endl;
	cout<<"1/P  prev = "<<pinv_prev<<"\t   1/P  last = "<<tx.Hfirst(5)<<" ("<<1./tx.Hfirst(5)<<" Gev)"<<endl;
	cout<<"Chi2 prev = "<<chi2_prev<<"\t   Chi2 last = "<<chi2<<endl;
    if (hist) h[46]->Fill(8);
    goto next_pair; // Next track pair.
      }

      if(tx.Hfirst(5,5) < 0) { // abnormal cov. matrix element
	cout<<"TEvBridgeSegments3() ==> COV(1/P,1/P) < 0 !"<<endl;
    if (hist) h[46]->Fill(9);
    goto next_pair; // Next track pair.
      }

      if(hist){
	if(good_pair){
	  ///HF1(TOpt::Hoffs+100*imag+6,float(chi2),1.);
	  ///HF1(TOpt::Hoffs+imag*100+13,(1./fabs(pimc)),1.);
	}
      }

      // Cut ( final Chi2)
      if(TOpt::ReMode[6] == 0 ) {
	if(chi2 > TOpt::dCut[9]) {
	  if(print > 1) cout<<"Didn't passed final Chi2 cut (dCut[9]) "<<TOpt::dCut[9]<<endl;
      if (hist) h[46]->Fill(10);
      goto next_pair;
	}
      }
      // Histograms
      if(hist){
	float pi, sig, dpinv;
	if(good_pair){ // well reconstructed track
	  int ikin = tr.IKine;
	  pi = float(tx.Hfirst(5));
	  sig = float(sqrt(tx.Hfirst(5,5)));
	  dpinv=fabs(pi)-fabs(pimc);
	  ///HF1(TOpt::Hoffs+imag*100+1,dpinv,1.);
	  ///HF1(TOpt::Hoffs+imag*100+2,dpinv/sig,1.);
	  ///HF1(TOpt::Hoffs+imag*100+14,(1./fabs(pimc)),1.);
	  ///HF1(TOpt::Hoffs+imag*100+30,(iter+0.5),1.);
	}
	if(bad_pair){ // if wrong pair was bridged
	  ///HF1(TOpt::Hoffs+imag*100+16,(1./fabs(pimc)),1.);
	}
      }
	
      // Store track pair candidates

      tp.Chi2 = chi2; 
      tp.iL   = il;
      tp.iR   = ir;
      tp.Pinv = tx.Hfirst(5);
      tp.ErrP = tx.Hfirst(5,5);
      lTrackPair.push_back(tp);
	

    next_pair:;
    }// end of loop over right track pieces
  }  // end of loop over left  track pieces

  if(hist){
    h[43]->Fill(ntl);
    h[44]->Fill(ntr);
    h[45]->Fill(lTrackPair.size());
  }

  tr.Id = ++TTrack::TrackCounter;// make IDs of temporary tracks unique
  tl.Id = ++TTrack::TrackCounter;// before destructors will be called
  tx.Id = ++TTrack::TrackCounter;// (to prevent removal of it's IDs from sTrackIDs of TKine)
    

  // Sort merged pairs by "Chi2 per hit" of the fit

  lTrackPair.sort();

  // Build global tracks

  for(itp=lTrackPair.begin(); itp!= lTrackPair.end(); itp++){     // loop over magnet track pairs
    if((*(*itp).iL).Type != 1 || (*(*itp).iL).IMark == -1) continue;
    if((*(*itp).iR).Type != 2 || (*(*itp).iR).IMark == -1) continue;
    (*(*itp).iL).Append((*(*itp).iR));  // append track type 10 to track type 01
    (*(*itp).iL).Hlast(5)  =(*itp).Pinv;// store momentum in last point Helix
    (*(*itp).iL).Hlast(5,5)=(*itp).ErrP;
  }
  

  // Erase tracks pieces and make backward/forward global fit 
  
  list<TTrack>::iterator it = listTrack.begin();
  while(it != listTrack.end()) { // track loop 
    if((*it).IMark == -1){
      listTrack.erase(it++); // erase appended track piece
    } else {
      if((*it).Type == 3 || (*it).Type == 6 || (*it).Type == 7){ // bridged track only
	if(
	   ! (*it).QuickKF3(-1,1) ||  // global Kalman refit backward
	   ! (*it).QuickKF3( 1,1)     // global Kalman refit  forward
	   )
	  {
        if (hist) h[46]->Fill(11);
        listTrack.erase(it++); // erase track if the fit had been failed
	  }
      }
      it++;
    }
  }// end of track loop

  /*
  it = listTrack.begin();
  while(it != listTrack.end()) { 
      cout<<"after TTrackBridgeSegments3  ID = "<<(*it).Id
	<<" Type = "<<(*it).Type<<" Nhit "<<(*it).NHits<<endl;
    it++;
  }
  */
  if(print) cout<<"======> End of TEvBridgeSergents3"<<endl;
}












