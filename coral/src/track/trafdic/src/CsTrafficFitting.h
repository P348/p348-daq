/*!
   \file    CsTrafficFitting.h
   \author  Benigno.Gobbo@cern.ch
*/

#ifndef CsTrafficFitting_h
#define CsTrafficFitting_h

#include "CsSTD.h"
#include "CsTrkFitting.h"

/*! \class CsTrafficFitting 
  Coral interface to Traffic track fit
*/

class CsTrafficFitting : public CsTrkFitting {

 public:

  CsTrafficFitting();          //!< constructor
  ~CsTrafficFitting();         //!< destructor

  /*! 
    Interface to Traffic track fit
    \sa TEv::TracksFit()
  */
  bool doFitting( std::list<CsTrack*>& tracks, const std::list<CsCluster*>& clusters );
};

#endif //CsTrafficFitting_h
