#include "CsCluster.h"
#include "TOpt.h"
#include "TSetup.h"
#include "TEv.h"
#include "TTrack.h"

using namespace std;

/*!
  \file   TTrackUseTime.cc
  \brief  Set track mean time and uncertainty (if track has time measurements). Calculate also time dispersion.
  \param \e clean = if !=0, "clean" worst outlier, defined as worst scifi or hodo hit outside of ( mean +/- \e clean * uncertainty ). "Clean" means disregard worst outlier and recompute track's mean time and uncertainty accordingly. And, if in addition track's chi2 is bad, remove it.
*/

/*
  Returns false when cleaning goes wrong and track has to be erased.
*/
bool TTrack::UseHitTime(int clean, bool subHit)
{
  const TSetup& setup = TSetup::Ref();
  const TEv &ev = TEv::Ref();

  list<int>::const_iterator ihp; int n1; double s1, st, z1, zt, zt2;
  for (ihp = lHitPat.begin(), n1 = 0, s1=st=z1=zt=zt2 = 0;
       ihp!=lHitPat.end(); ihp++) {
    // ***** LOOP OVER HIT REFERENCES *****
    int ihit = *ihp; if (ihit<0) continue;
    const THit &h = ev.vHit(ihit);
    if (h.SigT>0) {              // Hit does have time measurement
      double w = 1/h.SigT/h.SigT;
      // Compute time dispersion
      n1++; z1 += w; zt += h.Time*w; zt2 += h.Time*h.Time*w;
      // Compute time uncertainty: disregard outlier
      if (ihit!=Outlier) { s1 += w; st += h.Time*w; }
    }
  }

  if (n1) {
    MeanTime = st/s1; SigmaTime = sqrt(1/s1);
    if (n1>1) DispTime = sqrt(zt2*z1-zt*zt)/z1;
    else      DispTime = -1;
  }
  else {
    // Time may have been set at an earlier stage of the reco when
    // TTrack had some time info, which it subsequently lost...
    SigmaTime=DispTime = -1;  //  ...=> Reset "SigmaTime", "DispTime"
  }

  if (0<SigmaTime && SigmaTime<.5 && // Precisely enough timed
      clean) {

    //                    ********** CLEANING **********
    // (Note: In principle scifi hit timing is checked in the PR. But this
    // only applies to scifi-only or almost-only segments. When segments are
    // concatenated via bridging we may still face inconsistent hit times.
    // E.g. 07W27/cdr32001-58926, event #1055129.
    // There's also the case of DY 2015, where due to the bad perfromance of the
    // beamT, many outliers show up.)

    int nHits[5], nScifis[5]; // We include a fifth zone: to be on the safe side
    const vector<int> &iplF = setup.vIplFirst(), &iplL = setup.vIplLast();
    int nZones = iplF.size(), zone;
    const THit *worstHit; double worstDev; 
    for (ihp = lHitPat.begin(), worstHit = 0, worstDev = clean,
	   memset((void*)nScifis,0,sizeof(nScifis)),
	   memset((void*)nHits,0,sizeof(nHits)); ihp!=lHitPat.end(); ihp++) {
      // ***** LOOP OVER HIT REFERENCES *****
      int ihit = *ihp; if (ihit<0) continue;
      const THit &h = ev.vHit(ihit); int ipl = h.IPlane;
      const TDetect &d = setup.vDetect(ipl);
      int iz; for (iz=zone = 0; iz<nZones; iz++) {
	if (iplF[iz]<=ipl && ipl<=iplL[iz]) { zone = iz; break; }
      }
      nHits[zone]++;
      if (h.SigT<=0) continue;
      double time = h.Time, sigt = h.SigT;
      if (d.IType==22 /* scifi */|| d.IType>=41 /* hodos */) {
	if (d.IType==22) nScifis[zone]++;
	double dev = fabs(time-MeanTime)/sqrt(sigt*sigt+SigmaTime*SigmaTime);
	if (dev>worstDev) {   // ***** BADLY TIMED HIT... *****
	  worstHit = &h; worstDev = dev;
	}
      }
    }
    const THit *prvOutlier = Outlier>=0 ? &ev.vHit(Outlier) : 0;
    if (prvOutlier && prvOutlier!=worstHit) {
      // Previous outlier is somehow no longer so...
      double w = 1/prvOutlier->SigT/prvOutlier->SigT;
      s1 += w; st += prvOutlier->Time*w;
    }
    if (worstHit) {           // ***** WORST TIMED HIT... *****
      int iplWorst = worstHit->IPlane;
      double w = 1/worstHit->SigT/worstHit->SigT;
      if (prvOutlier!=worstHit) {      // ***** ...UPDATE "outlier" FLAG
	s1 -= w; st -= worstHit->Time*w;
      }
      MeanTime = st/s1; SigmaTime = sqrt(1/s1);
      bool badChi2Track = NDFs<5 || Chi2tot/(NDFs-5)>TOpt::dCut[16];
      if (badChi2Track) {
	// Processing depends upon whether bad or good track:
	// - If the latter => simply disregard the badly timed hit, so that it
	//  does not weight any longer on track's mean time.
	// - Else try and erase it. Do erase if track is evaluated to have
	//  enough hits in the relevant zone. The evaluation is done crudely.
	//  The integrity of the track will have to be re-evaluated later on.
	const TDetect &d = setup.vDetect(iplWorst);
	int iz; for (iz=zone = 0; iz<nZones; iz++) {
	  if (iplF[iz]<=iplWorst && iplWorst<=iplL[iz]) { zone = iz; break; }
	}
	int nHs = nHits[zone]-1;
	if (d.IType==22 && (1<<zone&TOpt::iCut[16])) { // Enhance scifis
	  // (Note that this is done crudely, and not in accordance w/ what's
	  // in "Talgo2::FindSpace".)
	  if (nScifis[zone]>1) nHs += 2*(nScifis[zone]-1);
	}
	if (nHs>=TOpt::iPRpar[10*zone+5]) {
	  // Enough hits otherwise => we can safely discard the hit
	  if (subHit) SubHit(iplWorst);
	  else        CancelHit_Clip(iplWorst,true);
	  nScifis[zone]--; if (!nScifis[zone]) Scifi &= ~(1<<zone);
	  if (--n1>1) {	                          // Update dispersion
	    z1 -= w; zt -= worstHit->Time*w; zt2 -= worstHit->Time*worstHit->Time*w;
	    DispTime = sqrt(zt2*z1-zt*zt)/z1;
	  }
	  else DispTime = -1;
	  iplWorst = -2; // Track is to be refit
	}
      }
      if (iplWorst==-2) {       // ***** WORST HIT DISCARDED: REFIT
	bool ret = true;
	// Disregard QN fit. (Can be done since cleaning is either performed in
	// later stage of track reconstruction, where QN fit is no longer
	// prevailing, or where straight tracks and no QN.)
	IFit &= ~0x8;
	if (IFit&0x20) {
	  ret = FullKF(-1);    if (ret && (IFit&0x40)) ret = FullKF(1);
	}
	else if (IFit&0x2) {
	  ret = QuickKF(-1,1); if (ret && (IFit&0x4))  ret = QuickKF(1,1);
	}
	else if (IFit&0x1) {
	  ret = QuickKF(-1,0);                         ret = QuickKF(1,0);
	}
	return ret;
      }
      Outlier = worstHit->IHit;
    }
  }

  return true;
}
