
#include "THlx.h"
#include <iostream>

using namespace std;

/*!
   Find Closest Distance of Approach of 2 tracks, represented by "this" helix
  and helix H.
   Upon success, returned helices are extrapolated to abscissa of CDA.
   Success implies found CDA turns out in argmument range w/in uncertainty.
  extrapolating them backward.
   Caveat: if guess for vertex, supplied as argument is bad, method can fail.
*/

// - Try to find CDA
// - If found value+/-uncertainty w/in "[Xmin,Xmax]":
// + Return true.
// + Extrapolate input helices to X of CDA.
// - Uncertainty is of great importance for forward tracks, i.e. tracks
//  parallel to incident track (e.g. low Q2 scattered muon in an event
//  triggered by muon from secondary pion decay).
// - Uncertainty is determined as the uncertainty of the CDA (i.e. common
//  perpendicular) of helix tangents. Ideally, those tangents should be
//  evaluated at vertex. Which is a priori not known. Best working option is
//  to evaluate it at "X0". This handles nicely the case of a track not
//  bridged over SM1 (or more generally not reco'd upstream of a bending
//  field). This, provided interaction does take place at or near (from a
//  bending field point of view) "X0". Bad option: X of CDA, which can be
//  infinitely (see case of parallel track supra) away from vertex.
// - NOTA BENE: In case "X0" is a bad vertex guess (e.g. interaction
//  downstream of SM1), the present method is not guaranted to work well.


extern "C"  // C-like prototypes
{
  void minvar_(float& x, float& y, float& r, float& eps, float& step, int& maxf, 
	       float& a, float& b, float f2minimize_(float& x, int& i) ); // CERNLIB routine
  float f2minimize_(float& x, int& i);
}

void commonPerp(const double *p1, const double *p2,
		const double *dp1, const double *dp2,
		double &x, double &dx,
		bool print = false) {
  const double &a1 = p1[1], &b1 = p1[2], &t1 = p1[3], &u1 = p1[4], &c1 = p1[0];
  const double &a2 = p2[1], &b2 = p2[2], &t2 = p2[3], &u2 = p2[4], &c2 = p2[0];
  double A1 = a1-t1*c1, B1 = b1-u1*c1;
  double A2 = a2-t2*c2, B2 = b2-u2*c2;
  double A = A2-A1, B = B2-B1, T = t2-t1, U = u2-u1;
  double N = -(A*T+B*U), D = T*T+U*U, D2 = D*D; x = N/D;
  N *= -1; // Let's drop the minus sign...
  const double &aa1 = dp1[0], &bb1 = dp1[2], &tt1 = dp1[5], &uu1 = dp1[9];
  const double &aa2 = dp2[0], &bb2 = dp2[2], &tt2 = dp2[5], &uu2 = dp2[9];
  const double &ba1 = dp1[1], &ta1 = dp1[3], &tb1 = dp1[4], &ua1 = dp1[6], &ub1 = dp1[7], &ut1 = dp1[8];
  const double &ba2 = dp2[1], &ta2 = dp2[3], &tb2 = dp2[4], &ua2 = dp2[6], &ub2 = dp2[7], &ut2 = dp2[8];
  double dAda1 = -1, dAdt1 =  c1, dBdb1 = -1, dBdu1 =  c1, dTdt1 = -1, dUdu1 = -1;
  double dAda2 =  1, dAdt2 = -c2, dBdb2 =  1, dBdu2 = -c2, dTdt2 =  1, dUdu2 =  1;
  double dxda1 = dAda1*T/D, dxdb1 = dBdb1*U/D;
  double dxda2 = dAda2*T/D, dxdb2 = dBdb2*U/D;
  double dxdt1 = (dAdt1*T+A*dTdt1)*D-N*2*T*dTdt1, dxdu1 = (dBdu1*U+B*dUdu1)*D-N*2*U*dUdu1;
  double dxdt2 = (dAdt2*T+A*dTdt2)*D-N*2*T*dTdt2, dxdu2 = (dBdu2*U+B*dUdu2)*D-N*2*U*dUdu2;
  dxdt1 /= D2; dxdu1 /= D2; dxdt2 /= D2; dxdu2 /= D2;
  double dx2
    =    dxda1*(dxdb1*ba1+dxdt1*ta1+dxdu1*ua1)+dxdb1*(dxdt1*tb1+dxdu1*ub1)+dxdt1*dxdu1*ut1;
  dx2 += dxda2*(dxdb2*ba2+dxdt2*ta2+dxdu2*ua2)+dxdb2*(dxdt2*tb2+dxdu2*ub2)+dxdt2*dxdu2*ut2;
  dx2 *= 2;
  dx2 += dxda1*dxda1*aa1+dxdb1*dxdb1*bb1+dxdt1*dxdt1*tt1+dxdu1*dxdu1*uu1;
  dx2 += dxda2*dxda2*aa2+dxdb2*dxdb2*bb2+dxdt2*dxdt2*tt2+dxdu2*dxdu2*uu2;
  dx = sqrt(dx2);
  if (print) {
    printf("dxda1 %f\n",dxda1);
    printf("dxdb1 %f\n",dxdb1);
    printf("dxdt1 %f\n",dxdt1);
    printf("dxdu1 %f\n",dxdu1);
    printf("dxda2 %f\n",dxda2);
    printf("dxdb2 %f\n",dxdb2);
    printf("dxdt2 %f\n",dxdt2);
    printf("dxdu2 %f\n",dxdu2);
  }
}

namespace HELIX4CDA {
  THlx H1,H2;
}

bool THlx::FindCDA(THlx& H, float X0, float Xmin, float Xmax, double &dXCDA)
{
  THlx& H1 = *this;
  THlx& H2 = H;

  float x,cda,r,step,eps;
  int maxf; 

  // pass 2 helices to "f2minimize_" routine
  HELIX4CDA::H1 = H1;
  HELIX4CDA::H2 = H2;

  // first aproximation of min. position
  x = X0;
  // convergency citeria
  r    = 0.1;
  eps  = 0.01;
  // first step size
  step = 50.;
  // man number of itterations (calls to f2minimize_)
  maxf = 200;

  // ***** EVALUATE UNCERTAINTY ON CDA
  // ***** AS UNCERTAINTY ON COMMON PERPENDICULAR OF TANGENTS AT ARG. GUESS
  double xp;
  // Extrapolate to "X0"
  //#define FCDA_DEBUG_EXTRAP
#ifdef FCDA_DEBUG_EXTRAP
  static int debugExtrap = 1;
  if (debugExtrap) {
    if (!H1.Extrap(X0,H1)) return false;
    if (!H2.Extrap(X0,H2)) return false;
  }
#else
  if (!H1.Extrap(X0,H1)) return false;
  if (!H2.Extrap(X0,H2)) return false;
#endif
  //#define FCDA_DEBUG
#ifdef FCDA_DEBUG
  static int debugFCDA = 0;
  if (debugFCDA)
    commonPerp(H1.Hpar,H2.Hpar,H1.Hcov,H2.Hcov,xp,dXCDA,true);
  else
    commonPerp(H1.Hpar,H2.Hpar,H1.Hcov,H2.Hcov,xp,dXCDA,false);
#else
  commonPerp(H1.Hpar,H2.Hpar,H1.Hcov,H2.Hcov,xp,dXCDA,false);
#endif
  double dX = dXCDA*3;  // Three sigmas

  // Extend X range beyond "[XMn,XMx]" to acount for uncertainty
  float xLow = xp-dX; if (Xmin<xLow) xLow = Xmin;
  float xUp =  xp+dX; if (Xmax>xLow) xUp =  Xmax;

  minvar_(x, cda, r, eps, step, maxf, xLow, xUp, f2minimize_); // call CERNLIB minimum search


#ifdef FCDA_DEBUG
  // Check that derivatives of XCDA in "commonPerp" are free of mistakes,
  // by comparing them to derivatives obtained from finite differences
  printf("%.1f %.3f  %.1f %.3f  %.4f %.4f  %.2f  %.2f %.2f %.2f %.2f\n",
	 H1.Hpar[0],1/H1.Hpar[5],H2.Hpar[0],1/H2.Hpar[5],
	 xp,dX,Xmin,Xmax,x-dX-eps*(Xmax-Xmin),x+dX+eps*(Xmax-Xmin));
  THlx H1p, H2p = H2; double xs, dxs, dp;
  const double *dp1 = H1.Hcov, *dp2 = H2.Hcov;
  const double &aa1 = dp1[0], &bb1 = dp1[2], &tt1 = dp1[5], &uu1 = dp1[9];
  const double &aa2 = dp2[0], &bb2 = dp2[2], &tt2 = dp2[5], &uu2 = dp2[9];
  H1p = H1; dp = sqrt(aa1); H1p.Hpar[1] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxda1 %f\n",xs,(xs-xp)/dp*32);
  H1p = H1; dp = sqrt(bb1); H1p.Hpar[2] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxdb1 %f\n",xs,(xs-xp)/dp*32);
  H1p = H1; dp = sqrt(tt1); H1p.Hpar[3] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxdt1 %f\n",xs,(xs-xp)/dp*32);
  H1p = H1; dp = sqrt(uu1); H1p.Hpar[4] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxdu1 %f\n",xs,(xs-xp)/dp*32);
  H1p = H1; dp = sqrt(aa2); H2p.Hpar[1] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxda2 %f\n",xs,(xs-xp)/dp*32);
  H2p = H2; dp = sqrt(bb2); H2p.Hpar[2] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxdb2 %f\n",xs,(xs-xp)/dp*32);
  H2p = H2; dp = sqrt(tt2); H2p.Hpar[3] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxdt2 %f\n",xs,(xs-xp)/dp*32);
  H2p = H2; dp = sqrt(uu2); H2p.Hpar[4] += dp/32;
  commonPerp(H1p.Hpar,H2p.Hpar,H1.Hcov,H2.Hcov,xs,dxs);
  printf("%.4f  dxdu2 %f\n",xs,(xs-xp)/dp*32);
#endif

  dX += eps*(Xmax-Xmin);
  if (x+dX<Xmin || Xmax<x-dX) {
    //cout<<"THlx::FindCDA() ==> end up at one of the limits"<<endl;
    return(false); // no local minimum had been found
  }

  // Extrapolate to found min.
  if(! HELIX4CDA::H1.Extrap(x,*this)) return false;
  if(! HELIX4CDA::H2.Extrap(x,H))     return false;
  
  //cout<<"THlx::FindCDA() ==>  MINIMUM is reached at X = "<< x <<" with CDA = "<<cda<<endl;

  return(true);
}


extern "C" float f2minimize_(float& x, int& i)
{
  static map<float,float> m;
  if(i == 0) m.clear();

  if(m.find(x) == m.end()) {
    THlx H1ext, H2ext;
    float dist;
    HELIX4CDA::H1.Extrap(x, H1ext);
    HELIX4CDA::H2.Extrap(x, H2ext);
    dist = H1ext.Dist(H2ext);
    m[x]=dist;
    //cout<<"f2minimize_ called with \t X = "<<x<<"  \t Dist = "<<dist<<endl;;
    return(dist);
  } else {
    //cout<<"f2minimize_ called with \t X = "<<x<<"  \t Mapped value is used : "<<m[x]<<endl;
    return(m[x]);
  }
}


















