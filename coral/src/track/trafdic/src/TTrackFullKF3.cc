#include <iostream>
#include <iomanip>
#include "TEv.h"
#include "TSetup.h"
#include "TTrack.h"
#include "THit.h"
#include "TDisplay.h"

using namespace std;

/*!
 Kalman fit through already found hits (no hits re-assignment)

 \param dir  fit direction

 \verbatim
 Input:

   dir =  1 - fit in  forward direction (resulting helix is Hlast )
   dir = -1 - fit in backward direction (resulting helix is Hfirst)

 \endverbati
*/

/*
  Version for NA64
*/

bool TTrack::FullKF3(int dir, int ipl, bool scaleUp)
{

  bool print = false;
  bool ok = false;
  const double max_step = 10.; // [cm]

  if(print) cout<<" \n\n================> TTrackFullKF3 ============ dir = "<<dir<<endl;

  if((dir > 0) && !Hfirst.with_mom() || (dir < 0) && !Hlast.with_mom()) return this->QuickKF(dir,0);


  const TSetup &setup = TSetup::Ref(); 
  TEv &ev = TEv::Ref();
  TDisplay& display = TDisplay::Ref();

  const vector<THit>& vHit = ev.vHit(); 
  if (dir!=1 && dir!=-1) {
    cout<<"TTrack::FullKF ==> wrong direction argument : "<<dir<<endl;
    assert(false);
  }
  if (lPlnRef.size()!=lHitPat.size()) {
    cout<<"TTrack::FullKF ==> lPlnRef.size() != lHitPat.size() "<<endl;
    assert(false);
  }

  if (TOpt::Graph[6] & 0x8) {
    // Switch ON debug drawing of calls to field and material.
    display.SetMmaptraj(true); display.SetRkutraj(true);
  }

  if (dir<0) { // go backward
    lPlnRef.reverse(); lHitPat.reverse(); swap(Hfirst,Hlast);
  }

  list<int>::iterator ip = lPlnRef.begin(), ih = lHitPat.begin(); THlx H;
  Chi2tot = 0;
  mChi2.clear();
  if (dir>0) { mHef.clear(); mHuf.clear(); }
  else {       mHeb.clear(); mHub.clear(); }
  H = Hfirst;
  if (scaleUp) H *= 100;      
  
  if (print) cout<<endl<<endl;
  THlx Hext, Hupd;
  int nstep(0); double tot_len(0), tot_rad_len(0), tot_eloss(0);

  while (ip!=lPlnRef.end()) { // loop over planes
    nstep++;
    if (print) cout<<"-----> pl "<<(*ip)<<"  proj = "<<setup.vPlane(*ip).IProj<<endl;
    
    const TDetect &det = setup.iPlane2Detect(*ip);
    Hext(0) = det.X(0);
    
    ok = H.Extrapolate(Hext,true, max_step);  // Extrapolate (with use of material map)
    
    if (!ok) {                                            // ***** EXIT IF FAILS
      cout<<"TTrack::FullKF() ==> Extrapolate error for track ID = "
	  <<Id<<"  dir = "<<dir<<" at step # "<<nstep<<endl;
      if (dir<0) { // Restore before exiting...
	lPlnRef.reverse(); lHitPat.reverse(); swap(Hfirst,Hlast);
      }
      return false;
    }

    if (print) Hext.Print("Hext");
    
    // Cumulate pass, fraction of rad. length and ELoss
    tot_len     += Hext.Path();
    tot_rad_len += Hext.RadLenFr();
    tot_eloss   += Hext.ELoss();

    // Add scattering on detector if it's not yet done inside extrapolation
    // function, i.e. if map is OFF or map is ON but we are out of material map.
    if (TOpt::ReMode[20]<=0 ||
	TOpt::ReMode[20]>0 && !setup.InMaterialMap(Hext(0))) {
      if (det.InMassive(Hext(1),Hext(2))) {
	// If inside detector massive area, i.e. active zone + possibly central
	// dead zone if massive (case of e.g. MM, as opposed to ST, DR, MB).
	double Len    = det.Siz(0)/Hext.DirCos(1); // det. thickness
	double RadLen = det.RadLen;                // det. rad. len.
	tot_rad_len += Len/RadLen;
	Hext.AddNoise(Len, RadLen);
	if (print) Hext.Print("Hext + noise");
      }
      else if (print) cout<<"   track is out of plane. No noise added\n";
    }

    if (Hext.RadLenFr()<0) {
      cout<<"TTrack::FullKF3() ==> Extrapolating from "<<H(0)<<" to "<<Hext(0)
	  <<" gives negative X/X0 = "<<Hext.RadLenFr()<<" (tot_rad_len = "<< tot_rad_len <<")"<<endl;
      assert(false);
    }

    // store extrapolated helix

    if (dir==1) mHef[*ip] = Hext;
    else        mHeb[*ip] = Hext;

    if ((*ih) < 0) {      // No associated hit, no update
      Hupd = Hext;
    } else {
      const THit &h = vHit[(*ih)];      // Associated hit
      double chi2 = Hext.HitChi2(h);  // chi2 increment
      mChi2[(*ip)] = chi2;              // Store chi2 increment
      Chi2tot += chi2;
      if (print) {
	cout<<"     hit # "<<*ih
	    <<" det = "<<sqrt(1./(h.G0*h.G2 - h.G1*h.G1))
	    <<" chi2 "<<chi2<<endl;
      }
      Hext.Update(h,Hupd);     // <--------- Update
    }

    if (print) Hupd.Print("Hupdated");
    if (print) cout<<endl;

    // store updated helix

    if (dir==1) mHuf[*ip] = Hupd;
    else        mHub[*ip] = Hupd;

    H = Hupd; 
    ip++; ih++;  // Next plane, next hit

  } // End of loop over planes and hits

  Hlast = H;  // Store results into track
  this->radLenFr = tot_rad_len;
  this->eLoss = tot_eloss;
  // Set fit flags
  if (dir==1) fFitDone = true;
  else        bFitDone = true;
  
  if (dir < 0) { // restore order of planes
    lPlnRef.reverse(); lHitPat.reverse(); swap(Hfirst,Hlast);
  }

  if (TOpt::Graph[6] & 0x8) {
    display.SetMmaptraj(false); display.SetRkutraj(false); // Switch OFF debug drawing
  }

  if(print) cout<<" \n\n===========> End of TTrackFullKF3 ============"<<endl;

  return true;
}
