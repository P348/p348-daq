#include "Coral.h"
#include "CsGeom.h"
// tracking stuff
#include "CsTrkUtils.h"
#include "CsTrafficUtils.h"

/*!
  \fn int main( int argc, char *argv[] ) {
  \brief Generate Dico == Lattice file (Lattice is a tracks look-up table
    used for track fitting. Generation uses Traffic track propagation
  \author Y.B
*/

int main( int argc, char *argv[] ) {

  // Package Initialization 
  Coral* coral        = Coral::init( argc, argv );

  std::cout << "Comgeant geometry version :"
       << CsGeom::Instance()->getGeomVers() << std::endl;

  // Define tracking procedures (by hand for the moment)...
  CsTrafficUtils DicoUtil;

  CsTrkUtils*    trkutil = &DicoUtil;

  return (trkutil->genLattice());
}


