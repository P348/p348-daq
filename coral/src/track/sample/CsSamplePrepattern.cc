/*!
   \file    CsSamplePrepattern.cc
   \brief   Sample of track prepattern class.
   \author  Benigno Gobbo

*/

#include "CsSamplePrepattern.h"
#include "CsErrLog.h"

CsSamplePrepattern::CsSamplePrepattern() {
}

CsSamplePrepattern::~CsSamplePrepattern() {
}

bool CsSamplePrepattern::doPrepattern( const list<CsCluster*> clusters, 
				       const list<CsZone*> zones) {
  CsErrLog::Instance()->mes( elError, "doPrepattern method not yet implemented" );
  return( false );
}

bool CsSamplePrepattern::getPatterns( list<CsTrack*>& tracks ) {
  CsErrLog::Instance()->mes( elError, "getPatterns method not yet implemented" );
  return( false );
}

const list<CsCluster*> &CsSamplePrepattern::getUnusedClusters(void) {
  list<CsCluster*> clusters;
  CsErrLog::Instance()->mes( elError, 
			     "getUnusedClusters method not yet implemented" );
  return( clusters );
}


