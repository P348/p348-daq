/*!
   \file    CsSampleFitting.cc
   \brief   Sample of track fitting class
   \author  Benigno Gobbo

*/

#include "CsSampleFitting.h"
#include "CsErrLog.h"

CsSampleFitting::CsSampleFitting() {
}

CsSampleFitting::~CsSampleFitting() {
}

bool CsSampleFitting::doFitting( list<CsTrack*>& tracks,
				 const list<CsCluster*>& clusters ) {
  CsErrLog::Instance()->mes( elError, "doFitting method not yet implemented" );
  return( false );
}



