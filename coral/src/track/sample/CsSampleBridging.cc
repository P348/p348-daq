/*!
   \file    CsSampleBridging.cc
   \brief   Sample of track bridging class
   \author  Benigno Gobbo

*/

#include "CsSampleBridging.h"
#include "CsErrLog.h"

CsSampleBridging::CsSampleBridging() {
}

CsSampleBridging::~CsSampleBridging() {
}

bool CsSampleBridging::doBridging( list<CsTrack*>& tracks, 
				   list<CsCluster*>& clusters ) {
  CsErrLog::Instance()->mes( elError, "doBridging method not yet implemented" );
  return( false );
}

