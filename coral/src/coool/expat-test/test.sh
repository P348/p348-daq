#!/bin/bash -e

echo "Checking daq.xml ..."
xmlwf daq.xml

echo "Running example daq.cc ..."
g++ -o daq.exe daq.cc -lexpat
./daq.exe < daq.xml
