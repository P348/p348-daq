#ifndef __GroupMM__
#define __GroupMM__

#include "Group.h"
#include "PlaneMM.h"

#include "conddb.h"

class GroupMM : public Group {

private:

  const PlaneMM* planeX;
  const PlaneMM* planeY;

  TH2F* histoXY;
  TH2F* histoXY_phys;
  TH2F* histoXY_beam;
  TH2F* histoXY_muTrans;
  TH2F* histoXY_LED;
  TH2F* histoXY_rand;
  TH2F* dummyXY;    
  TH1F* histoX;
  TH1F* histoY;
  TProfile* histoX_vs_spill;
  TProfile* histoY_vs_spill;
  TProfile* histoEff_vs_spill;
  TProfile* histoEff_vs_spill_phys;
  TProfile* histoEff_vs_spill_beam;
  TProfile* histoEff_vs_spill_muTrans;
  
  DetGeo* geom;
  
public:
  GroupMM(const char* name);

  void Init();

  // hook for rotation matrix update
  virtual void NewRun(int runnumber);
  
#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event, const EventFlags& flags);
#endif

  //ClassDef(GroupMM,0)
};

#endif
