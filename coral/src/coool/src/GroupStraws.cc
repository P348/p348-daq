#include <cstdio>
#include "TThread.h"
#include "GroupStraws.h"
#include "PlaneStrawTubes.h"
#include "DaqEvent.h"
#include "ChipNA64TDC.h"

ClassImp(GroupStraws);

using namespace std;
using namespace CS;


GroupStraws::GroupStraws(const char* name) :
    Group(name)
{
    fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT};
}

int getRandCoord(int x,  int seed1)
{
    return seed1%x;
}

void GroupStraws::Init(void)
{
    if (fName.find("StrawAll") == 0) return;


    const bool isUV = (fName.find("UV") != std::string::npos);
    // UV planes is 384 channels
    // XY and T0 planes is 64 channels
    const int nBin = isUV ? 384 : 64;
    const TString name = fName;

    if (isUV)
    {
        fBeamImagery           = new TH2F(name+"_Beam_Imagery",           name+" Beam imagery;mm;mm",                      nBin, -3*nBin/2, 3*nBin/2,   nBin/2, -3*nBin/4, 3*nBin/4);
        fBeamImagery_onehit    = new TH2F(name+"_Beam_Imagery_onehit",    name+" Beam imagery only one hit;mm;mm",         nBin, -3*nBin/2, 3*nBin/2,   nBin/2, -3*nBin/4, 3*nBin/4);
        fBeamImagery_rt        = new TH2F(name+"_Beam_Imagery_rt",        name+" Beam imagery rt;mm;mm",                 3*nBin, -3*nBin/2, 3*nBin/2, 3*nBin/2, -3*nBin/4, 3*nBin/4);
        fBeamImagery_rt_onehit = new TH2F(name+"_Beam_Imagery_rt_onehit", name+" Beam imagery rt only one hit;mm;mm",    3*nBin, -3*nBin/2, 3*nBin/2, 3*nBin/2, -3*nBin/4, 3*nBin/4);

        fBeamImagery_phys      = new TH2F(name+"_Beam_Imagery_phys",      name+" Beam imagery Physics trigger;mm;mm",      nBin, -3*nBin/2, 3*nBin/2,   nBin/2, -3*nBin/4, 3*nBin/4);
        fBeamImagery_rt_phys   = new TH2F(name+"_Beam_Imagery_rt_phys",   name+" Beam imagery rt Physics trigger;mm;mm", 3*nBin, -3*nBin/2, 3*nBin/2, 3*nBin/2, -3*nBin/4, 3*nBin/4);
        
        fBeamImagery_beam      = new TH2F(name+"_Beam_Imagery_beam",      name+" Beam imagery Beam trigger;mm;mm",         nBin, -3*nBin/2, 3*nBin/2,    nBin/2, -3*nBin/4, 3*nBin/4);
        fBeamImagery_rt_beam   = new TH2F(name+"_Beam_Imagery_rt_beam",   name+" Beam imagery rt Beam trigger;mm;mm",    3*nBin, -3*nBin/2, 3*nBin/2,  3*nBin/2, -3*nBin/4, 3*nBin/4);


    }
    else 
    {
        fBeamImagery = new TH2F(name+"_Beam_Imagery", name+" Beam imagery;mm;mm", nBin, 0, 3*nBin,  nBin, 0, 3*nBin);

        fBeamImagery_onehit = new TH2F(name+"_Beam_Imagery_onehit", name+" Beam imagery only one hit;mm;mm", nBin, 0, 3*nBin,  nBin, 0, 3*nBin);

        fBeamImagery_rt = new TH2F(name+"_Beam_Imagery_rt", name+" Beam imagery rt;mm;mm", 5*3*nBin, 0, 3*nBin,  5*3*nBin, 0, 3*nBin);
        fBeamImagery_rt_onehit = new TH2F(name+"_Beam_Imagery_rt_onehit", name+" Beam imagery rt only one hit;mm;mm", 5*3*nBin, 0, 3*nBin,  5*3*nBin, 0, 3*nBin);
        fBeamImagery_phys = new TH2F(name+"_Beam_Imagery_phys", name+" Beam imagery Physics trigger;mm;mm", nBin, 0, 3*nBin,  nBin, 0, 3*nBin);
        fBeamImagery_rt_phys = new TH2F(name+"_Beam_Imagery_rt_phys", name+" Beam imagery rt Physics trigger;mm;mm", 5*3*nBin, 0, 3*nBin,  5*3*nBin, 0, 3*nBin);
        fBeamImagery_beam = new TH2F(name+"_Beam_Imagery_beam", name+" Beam imagery Beam trigger;mm;mm", nBin, 0, 3*nBin,  nBin, 0, 3*nBin);
        fBeamImagery_rt_beam = new TH2F(name+"_Beam_Imagery_rt_beam", name+" Beam imagery rt Beam trigger;mm;mm", 5*3*nBin, 0, 3*nBin,  5*3*nBin, 0, 3*nBin);

    }


    fBeamImagery->SetOption("COLZ");
    AddHistogram(fBeamImagery);

    fBeamImagery_onehit->SetOption("COLZ");
    AddHistogram(fBeamImagery_onehit);

    fBeamImagery_phys->SetOption("COLZ");
    AddHistogram(fBeamImagery_phys);
    fBeamImagery_beam->SetOption("COLZ");
    AddHistogram(fBeamImagery_beam);

    fBeamImagery_rt->SetOption("COLZ");
    AddHistogram(fBeamImagery_rt);

    fBeamImagery_rt_onehit->SetOption("COLZ");
    AddHistogram(fBeamImagery_rt_onehit);


    fBeamImagery_rt_phys->SetOption("COLZ");
    AddHistogram(fBeamImagery_rt_phys);
    fBeamImagery_rt_beam->SetOption("COLZ");
    AddHistogram(fBeamImagery_rt_beam);

 
    // vs. spill plots
    TString name1;
    name1 = name + "_X_vs_spill";
    TProfile_Ref* p1 = new TProfile_Ref(name1, name1+";spill;X mean #pm stddev,mm", 200, 0, 200);
    p1->SetReference(fReferenceDirectory);
    p1->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p1->SetMarkerStyle(kFullDotMedium);
    //p1->SetOption("P");
    histoX_vs_spill = p1;
    AddHistogram(histoX_vs_spill);
    
    name1 = name + "_Y_vs_spill";
    TProfile_Ref* p2 = new TProfile_Ref(name1, name1+";spill;Y mean #pm stddev,mm", 200, 0, 200);
    p2->SetReference(fReferenceDirectory);
    p2->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p2->SetMarkerStyle(kFullDotMedium);
    //p2->SetOption("P");
    histoY_vs_spill = p2;
    AddHistogram(histoY_vs_spill);
  
    name1 = fName + TString("_Eff_vs_spill");
    TProfile_Ref* p3 = new TProfile_Ref(name1, name1+";spill;efficiency", 200, 0, 200);
    p3->SetReference(fReferenceDirectory);
    p3->SetMaximum(1.1);
    p3->SetMarkerStyle(kFullDotMedium);
    histoEff_vs_spill = p3;
    AddHistogram(histoEff_vs_spill);
    
    
    fChannels = new TH1F_Ref(name+"_hits", name+" channels distribution;channel",nBin,0,nBin,fRateCounter);
    AddHistogram(fChannels);

    //tg1 = new TGraph(histoY_vs_spill);	
    //AddHistogram(tg1);	

    //fChannel32 = new TH1F_Ref((fName+"_X channel32 profile").c_str(),(fName+" channel 32 profile").c_str(),120,-6,6,fRateCounter);
    //fChannel32 -> GetXaxis() -> SetTitle("mm");
    //AddHistogram(fChannel32);


    fTime1 = new TH1F_Ref((fName+"_time").c_str(),(fName+" drift time").c_str(),2000,0,2000,fRateCounter); AddHistogram(fTime1);
    fTimeX = new TH1F_Ref((fName+"_Xtime").c_str(),(fName+" drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeX);
    fTimeY = new TH1F_Ref((fName+"_Ytime").c_str(),(fName+" drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeY);

    fTimeY28 = new TH1F_Ref((fName+"28Y_time").c_str(),(fName+"28 Y channel drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeY28);
    fTimeY30 = new TH1F_Ref((fName+"30Y_time").c_str(),(fName+"30 Y channel drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeY30);
    fTimeY32 = new TH1F_Ref((fName+"32Y_time").c_str(),(fName+"32 Y channel drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeY32);

    fTimeX28 = new TH1F_Ref((fName+"28X_time").c_str(),(fName+"28 X channel drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeX28);
    fTimeX30 = new TH1F_Ref((fName+"30X_time").c_str(),(fName+"30 X channel drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeX30);
    fTimeX32 = new TH1F_Ref((fName+"32X_time").c_str(),(fName+"32 X channel drift time").c_str(),2000,0,2000,fRateCounter);AddHistogram(fTimeX32);



    fChannelDoubleClustersX = new TH2F((fName+"_double_clusters_X").c_str(),(fName+" _double_clusters_X").c_str(),1000, 000, 2000,  1000, 000,2000);
    fChannelDoubleClustersX->SetOption("CONT");
    AddHistogram(fChannelDoubleClustersX);

    fChannelDoubleClustersY = new TH2F((fName+"_double_clusters_Y").c_str(),(fName+" _double_clusters_Y").c_str(),1000, 000, 2000,  1000, 000,2000);
    fChannelDoubleClustersY->SetOption("CONT");
    AddHistogram(fChannelDoubleClustersY);    
}

double GroupStraws::rtStraw(int time)
{
//      return pitch*(double)time/60.0;
	if (time > 0)      return  1.0/11.23*(pow((double)time, 1.0/1.203));
		else  if (time < 0)    return  -1.0/11.23*(pow((double)(-time), 1.0/1.203));
		else return 0.0;
}


void GroupStraws::EndEvent(const CS::DaqEvent &event, const EventFlags &flags)
{
    Group::EndEvent();

    if (fName.find("StrawAll") == 0) return;

    if(thr_flag)
        MYLOCK();
    
    double st0X = -1;
    double st0Y = -1;
    double st0U = -1;
    double st0V = -1;

    double st0X_rt = -1;
    double st0Y_rt = -1;
    double st0U_rt = -1;
    double st0V_rt = -1;

    /// TODO: Remove?
    //double STT0_ch33 = 0;
    //double STT0_ch37 = 0;
    //double STT0_ch41 = 0;
    double STT0_corr = 0;
    int xHitCount =0;
    int yHitCount =0;

    vector<tuple<double,double>> xHits;
    vector<tuple<double,double>> yHits;

    const int spill = event.GetBurstNumber();
    bool hasHit = false;
    //printf("ReadEvent #### \n");

    for( const Plane* pp : fPlanes)
    {
        const PlaneStrawTubes *p=dynamic_cast<const PlaneStrawTubes *>(pp);
        if( p==NULL )
        {
            printf("GroupStraws::EndEvent(): Not PlaneStrawTubes found!\n");
            continue;
        }
        if (TString(p->GetName()) != "STT0XY") continue;
        list<int> corr_channels;
        for( CS::Chip::Digit* it : p->GetDigits())
        {
            const CS::ChipNA64TDC::Digit *d=dynamic_cast<const CS::ChipNA64TDC::Digit *>(it);
            if( d==NULL )
            {
                printf("GroupStraws::EndEvent(): bad digits was found!\n");
                continue;
            }

            int newChannel =  d->GetWire();
            int newTime = d->GetTime();
            if (newChannel==33 )
            {
                //STT0_ch33= newTime;
                corr_channels.push_back(newTime);
            }
            if (newChannel==37)
            {
                //STT0_ch37= newTime;
                corr_channels.push_back(newTime);	
            }
            if (newChannel==41)
            {
                //STT0_ch41= newTime;
                corr_channels.push_back(newTime);	
            }
        }
    
        if (corr_channels.size()>0)
        {
            corr_channels.sort();
            STT0_corr = corr_channels.front();
        }
    }

    for( const Plane* pp : fPlanes)
    {
        const PlaneStrawTubes *p=dynamic_cast<const PlaneStrawTubes *>(pp);
        if( p==NULL )
        {
            printf("GroupStraws::EndEvent(): Not PlaneStrawTubes found!\n");
            continue;
        }

        list<tuple<int,int>> listChannelsX;
        list<tuple<int,int>> listChannelsY;

        double channel222 = -1;
        double channel222_rt = -1;
        string detName = "";

        for( CS::Chip::Digit* it : p->GetDigits())
        {
            const CS::ChipNA64TDC::Digit *d=dynamic_cast<const CS::ChipNA64TDC::Digit *>(it);
            if( d==NULL )
            {
                printf("GroupStraws::EndEvent(): bad digits was found!\n");
                continue;
            }
            detName = d->GetDetID().GetName();


            if (detName.find("STT0XY") != std::string::npos)
            {
                continue;
            }

            //listChannels.push_back(make_tuple(d->GetWire(),d->GetTime()));
            if (detName.find("X") != std::string::npos || detName.find("V") != std::string::npos )
            {
                listChannelsX.push_back(make_tuple(d->GetWire(), d->GetTime()-STT0_corr));
                fTimeX->Fill(d->GetTime()-STT0_corr);
                if (d->GetWire()==28)
                {
                    fTimeX28->Fill(d->GetTime()-STT0_corr);
                }
                if (d->GetWire()==30)
                {
                    fTimeX30->Fill(d->GetTime()-STT0_corr);
                }
                if (d->GetWire()==32)
                {
                    fTimeX32->Fill(d->GetTime()-STT0_corr);
                }
            }
            if (detName.find("Y") != std::string::npos || detName.find("U") != std::string::npos)
            
            listChannelsY.push_back(make_tuple(d->GetWire(), d->GetTime()-STT0_corr));
            fTimeY->Fill(d->GetTime()-STT0_corr);
            if (d->GetWire()==28)
            {
                fTimeY28->Fill(d->GetTime()-STT0_corr);
            }
            if (d->GetWire()==30)
            {
                fTimeY30->Fill(d->GetTime()-STT0_corr);
            }
            if (d->GetWire()==32)
            {
                fTimeY32->Fill(d->GetTime()-STT0_corr);
            }
            
            
            fChannels->Fill(d->GetWire());
            fTime1->Fill(d->GetTime()-STT0_corr);

        }
        if (detName.find("STT0XY") != std::string::npos)
        {
            continue;
        }


        listChannelsX.sort();
        listChannelsY.sort();
  

        //fyMultiplicity->Fill(listChannelsY.size());

        int clusterSize =0;
        if (listChannelsX.size()>0)
        clusterSize=1;
    
        int oldChannel = -1;
        int old_old_Time=0;	
 
        int oldTime = 0;
        int newTime = 0;
        int newChannel =  -1;
        int old_old_time =0;
        int old_old_channel = 0;
        //xHitCount =0;
        //yHitCount =0;
        //printf("Event start channel  \n" );

        int XtimeMean = fTimeX->GetMean();
        int YtimeMean = fTimeY->GetMean();

        for (std::list<tuple<int,int>>::iterator it=listChannelsX.begin(); it != listChannelsX.end(); ++it)
        { 
            //newChannel =  get<0>(*it);
            //newTime = get<1>(*it); 
            newChannel =  get<0>(*it);
            newTime = get<1>(*it); 
            if (newChannel==oldChannel+1 )
            {
                clusterSize++;
            }
            else 
            {
                if (oldChannel!=-1)        
                {
                    if (clusterSize!=2 && clusterSize>0)
                    {
                        //int randchange = fRateCounter%2;
                        int randdir = fRateCounter%2 ? -1. : +1.;

                        //channel222 = (3*newChannel-randchange);
                        channel222 = (3*newChannel);
                        channel222_rt = (3*newChannel+randdir*rtStraw(XtimeMean+30-newTime));

                        if (detName.find("X") != std::string::npos) 
                        {
                            st0X = channel222;
                            st0X_rt = channel222_rt;
                            xHits.push_back(make_tuple(st0X,st0X_rt));
                        }

                        if (detName.find("V") != std::string::npos) 
                        {
                            channel222_rt = (3*newChannel+randdir*rtStraw(XtimeMean+30-newTime));

                            st0V = channel222;
                            st0V_rt = channel222_rt;
                            xHits.push_back(make_tuple(st0V,st0V_rt));
                        }
                        //fChannelDoubleClustersX->Fill(newTime,oldTime);
                        //yHitCount++;
                        //yHits.push_back(make_tuple(st0V,st0V));

                    }
                    if (clusterSize==2)
                    {
                        channel222 = (3*oldChannel+3*newChannel)/2;
                        channel222_rt = (3*oldChannel+rtStraw(XtimeMean+30-oldTime) + 3*newChannel-rtStraw(XtimeMean+30-newTime))/2;

                    if (detName.find("X") != std::string::npos)
                    {
                        st0X = channel222;
                        st0X_rt = channel222_rt;
                        xHits.push_back(make_tuple(st0X,st0X_rt));
                    }
                    if (detName.find("V") != std::string::npos)
                    {
                        st0V = channel222;
                        st0V_rt = channel222_rt;
                        xHits.push_back(make_tuple(st0V,st0V_rt));
                    }
                    xHitCount++;
                    fChannelDoubleClustersX->Fill(newTime,oldTime);
                    clusterSize=1;
                }
            }
        }
        old_old_time = oldTime;
        old_old_channel = oldChannel;
        oldChannel = newChannel;
        old_old_Time = oldTime;
        oldTime = newTime;
        }
        if (clusterSize==2)
        {
            channel222 = (3*old_old_channel+3*newChannel)/2;
            channel222_rt = (3*old_old_channel+rtStraw(XtimeMean+30-old_old_time) + 3*newChannel-rtStraw(XtimeMean+30-newTime))/2;
            
            if (detName.find("X") != std::string::npos)
            {
                st0X = channel222;
                st0X_rt = channel222_rt;
                xHits.push_back(make_tuple(st0X,st0X_rt));
            }
            //if (detName.find("X") != std::string::npos)
            if (detName.find("V") != std::string::npos) 
            {
                st0V = channel222;
                st0V_rt = channel222_rt;
                xHits.push_back(make_tuple(st0V,st0V_rt));
            }
            fChannelDoubleClustersX->Fill(oldTime,old_old_Time);
            //st0V = channel222;
            xHitCount++;
        }
        if (clusterSize!=2 && clusterSize>0)
        {
            //int randchange = fRateCounter%2;
            int randdir = fRateCounter%2 ? -1. : +1.;

            //channel222 = (3*newChannel-randchange);
            channel222 = (3*newChannel);
            channel222_rt = (3*newChannel+randdir*+randdir*rtStraw(XtimeMean+30-newTime));
            
            if (detName.find("X") != std::string::npos) 
            {
                st0X = channel222;
                st0X_rt = channel222_rt;
                xHits.push_back(make_tuple(st0X,st0X_rt));
            }
            if (detName.find("V") != std::string::npos) 
            {
                st0V = channel222;
                st0V_rt = channel222_rt;
                xHits.push_back(make_tuple(st0V,st0V_rt));
            }
            //fChannelDoubleClustersY->Fill(newTime,oldTime);
            //xHitCount++;
            //yHits.push_back(make_tuple(st0V,st0V));
       }
       clusterSize =0;
       if (listChannelsY.size()>0)
           clusterSize=1;
       //clusterSize =0; 
       oldChannel = -1;
       old_old_Time=0;
 
       oldTime = 0;	
       newTime = 0;
       newChannel =  -1;

       for (std::list<tuple<int,int>>::iterator it=listChannelsY.begin(); it != listChannelsY.end(); ++it)
       {
            newChannel =  get<0>(*it);
            newTime = get<1>(*it); 
       
            if (newChannel==(oldChannel+1) )
            {
                clusterSize++;
            }
            else 
            {
                if (oldChannel!=-1)        
                {

                    if (clusterSize!=2 && clusterSize>0)
                    {
                        //int randchange = fRateCounter%2;
                        int randdir = fRateCounter%2 ? -1. : +1.;

                        channel222 = (3*newChannel);
                        channel222_rt = (3*newChannel+randdir*rtStraw(YtimeMean+30-newTime));
 
                        if (detName.find("Y") != std::string::npos) 
                        {
                            st0Y = channel222;
                            st0Y_rt = channel222_rt;
                            yHits.push_back(make_tuple(st0Y,st0Y_rt));

                        }

                        if (detName.find("U") != std::string::npos) 
                        {
                            channel222_rt = (3*newChannel+randdir*rtStraw(YtimeMean+30-newTime));

                            st0U = channel222;
                            st0U_rt = channel222_rt;
                            yHits.push_back(make_tuple(st0U,st0U_rt));

                        }
                        //fChannelDoubleClustersY->Fill(newTime,oldTime);

                        //yHitCount++;
                        //yHits.push_back(make_tuple(st0V,st0V));

                    }

                    if (clusterSize==2)
                    {
                        channel222 = (3*oldChannel+3*newChannel)/2;
                        channel222_rt = (3*old_old_channel+rtStraw(YtimeMean+30-old_old_time) + 3*newChannel-rtStraw(YtimeMean+30-newTime))/2;
 
                        if (detName.find("Y") != std::string::npos) 
                        {
                            st0Y = channel222;
                            st0Y_rt = channel222_rt;
                            yHits.push_back(make_tuple(st0Y,st0Y_rt));
                        }

                        if (detName.find("U") != std::string::npos) 
                        {
                            st0U = channel222;
                            st0U_rt = channel222_rt;
                            yHits.push_back(make_tuple(st0U,st0U_rt));

                        }
                        fChannelDoubleClustersY->Fill(newTime,oldTime);

                        yHitCount++;
                        //yHits.push_back(make_tuple(st0V,st0V));

                    }
                    clusterSize=1;

                }
            }
            old_old_time = oldTime;
            old_old_channel = oldChannel;
            oldChannel = newChannel;
            old_old_Time = oldTime;
            oldTime = newTime;
        }
        if (clusterSize==2)
        {
            channel222 = (3*old_old_channel+3*newChannel)/2;
            channel222_rt = (3*old_old_channel+rtStraw(YtimeMean+30-old_old_time) + 3*newChannel-rtStraw(YtimeMean+30-newTime))/2;

            if (detName.find("Y") != std::string::npos) 
            {
                st0Y = channel222;
                st0Y_rt = channel222_rt;
                yHits.push_back(make_tuple(st0Y,st0Y_rt));
            }

            if (detName.find("U") != std::string::npos) 
            {
                st0U = channel222;
                st0U_rt = channel222_rt;
                yHits.push_back(make_tuple(st0U,st0U_rt));
            }
            fChannelDoubleClustersY->Fill(oldTime,old_old_Time);
            yHitCount++;
        }
        if (clusterSize!=2 && clusterSize>0)
        {
            //int randchange = fRateCounter%2;
            int randdir = fRateCounter%2 ? -1. : +1.;

            //channel222 = (3*newChannel-randchange);
            channel222 = (3*newChannel);
            channel222_rt = (3*newChannel+randdir*rtStraw(YtimeMean+30-newTime));

            if (detName.find("Y") != std::string::npos) 
            {
                st0Y = channel222;
                st0Y_rt = channel222_rt;
                yHits.push_back(make_tuple(st0Y,st0Y_rt));
            }

            if (detName.find("U") != std::string::npos) 
            {
                st0U = channel222;
                st0U_rt = channel222_rt;
                yHits.push_back(make_tuple(st0U,st0U_rt));
            }
        }
    }

    //if ((yHits.size()>1 || xHits.size()>1) &&(yHits.size()>0 && xHits.size()>0))
    if ( yHits.size()>0 && xHits.size()>0)
    {
        int seed1 =  fRateCounter;
        int seed2 =  fBeamImagery->GetEntries();
        int xCoord = getRandCoord(xHits.size(),seed1);
        int yCoord = getRandCoord(yHits.size(),seed2);
        if (st0X != -1 || st0Y != -1)
        {
            st0X = get<0>(xHits[xCoord]);
            st0X_rt = get<1>(xHits[xCoord]);
            st0Y = get<0>(yHits[yCoord]);
            st0Y_rt = get<1>(yHits[yCoord]);
        }
        if (st0U != -1 || st0V != -1)
        {
            st0V = get<0>(xHits[xCoord]);
            st0V_rt = get<1>(xHits[xCoord]);
            st0U = get<0>(yHits[yCoord]);
            st0U_rt = get<1>(yHits[yCoord]);
        }
    }
    //else 
    //getchar();
    //------------------------------------------------------------------
    //------------------- ALL TRIGGER TYPES ----------------------------
    //------------------------------------------------------------------
    if (st0X != -1 && st0Y != -1 &&xHitCount==1 &&yHitCount==1)
    {
        fBeamImagery_onehit->Fill(st0X, st0Y);

    }
    
    //if (st0X != -1 && st0Y != -1 )
    if (st0X != -1 && st0Y != -1)// &&xHitCount==1 &&yHitCount==1)
    {
        fBeamImagery->Fill(st0X, st0Y);
    }
    //if (st0X != -1 && st0Y != -1 )
    if (st0X_rt != -1 && st0Y_rt != -1)// &&xHitCount==1 &&yHitCount==1)
    {
        fBeamImagery_rt->Fill(st0X_rt, st0Y_rt);

        hasHit = true;
        histoX_vs_spill->Fill(spill, st0X_rt);
        histoY_vs_spill->Fill(spill, st0Y_rt);
    }

    //if (st0X != -1 && st0Y != -1 )
    if (st0X_rt != -1 && st0Y_rt != -1 &&xHitCount==1 &&yHitCount==1)
    {
        fBeamImagery_rt_onehit->Fill(st0X_rt, st0Y_rt);

        //hasHit = true;
        //histoX_vs_spill->Fill(spill, st0X_rt);
        //histoY_vs_spill->Fill(spill, st0Y_rt);
    }

    //------------------------------------------------------------------
    //------------------- ONLY PHYSICS TRIG ----------------------------
    //------------------------------------------------------------------
    if (flags.isTriggerPhys) 
    {
        if (st0X != -1 && st0Y != -1)// &&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_phys->Fill(st0X, st0Y);
        }
        if (st0X_rt != -1 && st0Y_rt != -1)// &&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_rt_phys->Fill(st0X_rt, st0Y_rt);
        }
    }
    
    //------------------------------------------------------------------
    //------------------- ONLY BEAM TRIGGER ----------------------------
    //------------------------------------------------------------------
    if (flags.isTriggerBeamOnly()) 
    {
        if (st0X != -1 && st0Y != -1)// &&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_beam->Fill(st0X, st0Y);
        }
        if (st0X_rt != -1 && st0Y_rt != -1)// &&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_rt_beam->Fill(st0X_rt, st0Y_rt);
        }
    }
    
    // UV STRAWS
    //carthesian coordinates (from p348reco/straw.h)
    const double stereoAngle=M_PI*7.0/180.0;
    // hit position shifted with respect to centre of plane
    const double u_com = st0U-600.;
    const double v_com = st0V-600.;
    const double s = (v_com-u_com*cos(2*stereoAngle))/sin(2*stereoAngle); // "y" component in U plane; must be within [-300,300]
    const double t = (v_com*cos(2*stereoAngle)-u_com)/sin(2*stereoAngle); // "y" component in V plane; must be within [-300,300]
    bool physical = (t>-300 && t<300) && (s>-300 && s<300);
    const double xxx = physical ? v_com*cos(stereoAngle)-t*sin(stereoAngle) : nan(""); // position in carthesian coordinates
    const double yyy = physical ? v_com*sin(stereoAngle)+t*cos(stereoAngle) : nan(""); // position in carthesian coordinates

    // hit position shifted with respect to centre of plane
    const double u_com_rt = st0U_rt-600.;
    const double v_com_rt = st0V_rt-600.;
    const double s_rt = (v_com_rt-u_com_rt*cos(2*stereoAngle))/sin(2*stereoAngle); // "y" component in U plane; must be within [-300,300]
    const double t_rt = (v_com_rt*cos(2*stereoAngle)-u_com_rt)/sin(2*stereoAngle); // "y" component in V plane; must be within [-300,300]
    bool physical_rt = (t_rt>-300 && t_rt<300) && (s_rt>-300 && s_rt<300);
    const double xxx_rt = physical_rt ? v_com_rt*cos(stereoAngle)-t_rt*sin(stereoAngle) : nan(""); // position in carthesian coordinates
    const double yyy_rt = physical_rt ? v_com_rt*sin(stereoAngle)+t_rt*cos(stereoAngle) : nan(""); // position in carthesian coordinates
    

    //------------------------------------------------------------------
    //------------------- ALL TRIGGER TYPES ----------------------------
    //------------------------------------------------------------------
    if (st0U != -1 && st0V != -1 && xHitCount==1 &&yHitCount==1)
    {
        fBeamImagery_onehit->Fill(xxx, yyy);
    }

    if (st0U != -1 && st0V != -1) // &&xHitCount==1 &&yHitCount==1)
    { 
        fBeamImagery->Fill(xxx, yyy);
    }

    if (st0U_rt != -1 && st0V_rt != -1)//&&xHitCount==1 &&yHitCount==1)
    {
        fBeamImagery_rt->Fill(xxx_rt, yyy_rt);
    
        hasHit = true;
        histoX_vs_spill->Fill(spill, xxx_rt);
        histoY_vs_spill->Fill(spill, yyy_rt);
    }

    if (st0U_rt != -1 && st0V_rt != -1&&xHitCount==1 &&yHitCount==1)
    {
        fBeamImagery_rt_onehit->Fill(xxx_rt, yyy_rt);
    }

    //------------------------------------------------------------------
    //------------------- ONLY PHYSICS TRIG ----------------------------
    //------------------------------------------------------------------
    if (flags.isTriggerPhys) {
        if (st0U != -1 && st0V != -1)//&&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_phys->Fill(xxx, yyy);
        }
        if (st0U_rt != -1 && st0V_rt != -1)//&&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_rt_phys->Fill(xxx_rt, yyy_rt);
        }
    }
    //------------------------------------------------------------------
    //------------------- ONLY BEAM TRIGGER ----------------------------
    //------------------------------------------------------------------
    if (flags.isTriggerBeamOnly()) 
    {
        if (st0U != -1 && st0V != -1)//&&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_beam->Fill(xxx, yyy);
        }
        if (st0U_rt != -1 && st0V_rt != -1)//&&xHitCount==1 &&yHitCount==1)
        {
            fBeamImagery_rt_beam->Fill(xxx_rt, yyy_rt);
        }
    }

    histoEff_vs_spill->Fill(spill, hasHit);

    // calc efficiency
    if (needRateHistoUpdate()) 
    {
        //const double eff = fBeamImagery->Integral() / (fRateCounter + 1);
        const double eff_onehit = fBeamImagery_onehit->GetEntries() / (fRateCounter + 1);
        const TString eff_str_onehit = Form("%s Beam imagery only one hit eff (Nhit > 0) = %.3f", fName.c_str(), eff_onehit);
        fBeamImagery_onehit->SetTitle(eff_str_onehit);
        
        const double eff = fBeamImagery->GetEntries() / (fRateCounter + 1);
        const TString eff_str = Form("%s Beam imagery eff (Nhit > 0) = %.3f", fName.c_str(), eff);
        fBeamImagery->SetTitle(eff_str);
        
        if (fBeamImagery_phys) 
        {
            const double eff_phys = fBeamImagery_phys->GetEntries() / (fRateCounterPhysicsOnly + 1);
            const TString eff_str_phys = Form("%s Beam imagery eff_{phys} (Nhit > 0) = %.3f", fName.c_str(), eff_phys);
            fBeamImagery_phys->SetTitle(eff_str_phys);
        }

        if (fBeamImagery_beam) 
        {
            const double eff_beam = fBeamImagery_beam->GetEntries() / (fRateCounterBeamOnly + 1);
            const TString eff_str_beam = Form("%s Beam imagery eff_{beam} (Nhit > 0) = %.3f", fName.c_str(), eff_beam);
            fBeamImagery_beam->SetTitle(eff_str_beam);
        }

        //const double eff_rt = fBeamImagery_rt->Integral() / (fRateCounter + 1);
        const double eff_rt = fBeamImagery_rt->GetEntries() / (fRateCounter + 1);
        const TString eff_str_rt = Form("%s Beam imagery rt eff (Nhit > 0) = %.3f", fName.c_str(), eff_rt);

        fBeamImagery_rt->SetTitle(eff_str_rt);

        const double eff_rt_check = fBeamImagery_rt_onehit->GetEntries() / (fRateCounter + 1);
        const TString eff_str_rt_check = Form("%s Beam imagery rt only one hit eff (Nhit > 0) = %.3f", fName.c_str(), eff_rt_check);
        fBeamImagery_rt_onehit->SetTitle(eff_str_rt_check);

        if (fBeamImagery_rt_phys)
        {
            const double eff_rt_phys = fBeamImagery_rt_phys->GetEntries() / (fRateCounterPhysicsOnly + 1);
            const TString eff_str_rt_phys = Form("%s Beam imagery rt eff_{phys} (Nhit > 0) = %.3f", fName.c_str(), eff_rt_phys);
            fBeamImagery_rt_phys->SetTitle(eff_str_rt_phys);
        }
        if (fBeamImagery_rt_beam)
        {
            const double eff_rt_beam = fBeamImagery_rt_beam->GetEntries() / (fRateCounterBeamOnly + 1);
            const TString eff_str_rt_beam = Form("%s Beam imagery rt eff_{beam} (Nhit > 0) = %.3f", fName.c_str(), eff_rt_beam);
            fBeamImagery_rt_beam->SetTitle(eff_str_rt_beam);
        }

    }
    
    if(thr_flag)
        MYUNLOCK();
}

