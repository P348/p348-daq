#ifndef __GroupDAQ__
#define __GroupDAQ__

#include "Group.h"

class GroupDAQ : public Group {

 public:
  GroupDAQ(const char* name) : Group(name) {}
  void Init();
  
  void SetCenterLabelsX(TH1* h);

#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event, const EventFlags& flags);
#endif
  ClassDef(GroupDAQ,0)
};

#endif
