#include "PlaneV260.h"
#include "PlanePanel.h"
#include "ChipV260.h"
#include "TProfile.h"
#include <string>

ClassImp(PlaneV260);

const int PlaneV260::fNchan = 16;

PlaneV260::PlaneV260(const char *detname, int nch)
: Plane(detname)
{
  fAcceptedEventTypes = {CS::DaqEvent::END_OF_BURST};
}

void PlaneV260::Init(TTree* tree)
{
  TString name;
  
  name = fName + "_counters";
  hCounters = new TProfile(name, name+";channel;counts", PlaneV260::fNchan, 0, PlaneV260::fNchan);
  hCounters->SetOption("P TEXT0");
  hCounters->SetMarkerStyle(kFullDotMedium);
  hCounters->SetNdivisions(PlaneV260::fNchan, "X");
  hCounters->GetXaxis()->CenterLabels();
  hCounters->SetStats(0);
  //hCounters = new TH1F_Ref(name, name, PlaneV260::fNchan, 0, PlaneV260::fNchan, fRateCounter);
  //((TH1F_Ref*)hCounters)->SetReference(fReferenceDirectory);
  AddHistogram(hCounters);
  
  for (int i = 0; i < PlaneV260::fNchan; ++i) {
    name = fName + TString::Format("_ch%d_vs_spill", i);
    hCounters_vs_spill[i] = new TProfile(name, name+";spill;counts", 201, 0, 201, "S");
    hCounters_vs_spill[i]->SetOption("P");
    hCounters_vs_spill[i]->SetMarkerStyle(kFullDotMedium);
    AddHistogram(hCounters_vs_spill[i]);
  }
}

void PlaneV260::StoreDigit(CS::Chip::Digit* digit)
{
  //cout << "PlaneV260::StoreDigit()" << endl;
  
  // sanity check
  CS::ChipV260::Digit* d = dynamic_cast<CS::ChipV260::Digit*>(digit);
  if (!d) return;
  
  lDigits.push_back(digit);
}

void PlaneV260::EndEvent(const CS::DaqEvent &event)
{
  //cout << "PlaneV260::EndEvent()" << endl;
  
  if (thr_flag) MYLOCK();
  
  const int spill = event.GetBurstNumber();
  
  for (const auto i : lDigits) {
    CS::ChipV260::Digit* d = dynamic_cast<CS::ChipV260::Digit*>(i);
    const int ch = d->GetChannel();
    
    hCounters->Fill(ch, d->counter);
    
    TProfile* hi = hCounters_vs_spill[ch];
    hi->Fill(spill, d->counter);
    if (!d->name.empty()) {
      TString s;
      s.Form("%s_ch%d / %s", fName.c_str(), ch, d->name.c_str());
      hi->SetTitle(s);
    }
  }
  
    //TODO: calc efficiency(dead time) vs spill, like
    //    ~ R = counter[4]/counter[1]
    //      R = 1 - R
  
  if (thr_flag) MYUNLOCK();
}

void PlaneV260::ControlPanel(const TGWindow* p, const TGWindow* main)
{
  if (!fControlPanel) fControlPanel = new PlanePanel(p, main, 100, 100, this);
}
