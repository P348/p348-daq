#ifndef __PlaneHCAL2__
#define __PlaneHCAL2__

#include "Plane.h"

#include "TThread.h"
//=========== HCAL2 parameters =========================
#define nb_HCAL2_SADCcnl  240
#define nMAX_HCAL2_SADC_Samples 32
#define Ncols_number 22
#define Nrows_number 10
//=======================================================

class PlaneHCAL2 : public Plane {

  private:
/// maximum multiplicity per adc channel
  static const int fMAX_MULT;   

// number of HCAL2 rows/columns
    const int    Nrows, Ncols, fNchan;
    int Nchan, Nsamples;
    int pedstart, pedend;
    int pulsstart, pulsend;
    int calpedstart, calpedend;
    int ledstart, ledend;
    int SADCampcut, SADCLEDcut, SADCTimingcut;

  /// data for each SADC channel
  int  fSADCRow[nb_HCAL2_SADCcnl], fSADCCol[nb_HCAL2_SADCcnl];
  int  fSADCev[nb_HCAL2_SADCcnl][nMAX_HCAL2_SADC_Samples];
  void Init(TTree* tree);


//  histograms

  TH1F *hnSADChit,*hnSADChitcut,*hamp;
  TH2F *hnhitXY, *hnhitXYcut;
  TH2F *hampXY, *hampXYcut, *hamp_vs_adr;
  TH2F *hPhysTm_adr, *hLedTm_adr, *hTmvsTrig; 
  TH1F *hhit_vs_adr;
  TH1F *hPed, *hPedRMS;
  TH2F *hPedXY;
  TH2F *hLedXY, *hLedChnl;
  TH1F *hledamp;
  TH2F *hoverflow;
  TH1F *hSADCsum, *hSADCcutsum;
  TH1F *hSADCcuthits;
  TH2F *hxynoise, *hxynoise_amp;
  TH1F *hrnd, *hledlen;
  TH1F *hLedTm, *hPhysTm;
  TH2F *hpedrmsXY;
#if !defined(__CINT__) && !defined(__CLING__)
  TH2F *hsampl[Ncols_number][Nrows_number],*hled_sampl[Ncols_number][Nrows_number];
#endif
double SADCped(int ipbg, int  ipend, int *data,double &RMS);
double SADCpuls(int ipbg, int  ipend, int *data,double ped,
                                            double &sum,double &time);

 public:
 /*! \brief constructor
    \param detname detector name
    \param nchan number of channels
    \param center center of the time band
    \param width width of the time band
  */
  PlaneHCAL2(const char *detname,int ncols, int nrows, int center, int width);
  virtual ~PlaneHCAL2();

  /// Passes a digit to the plane
  void StoreDigit(int col, int row,  std::vector<float>& data);

#if !defined(__CINT__) && !defined(__CLING__)
   void StoreDigit(CS::Chip::Digit* digit);
   void EndEvent(const CS::DaqEvent &event);
#endif

  ClassDef(PlaneHCAL2,1)
};

#endif



