#ifndef __PlaneTrigHodo__
#define __PlaneTrigHodo__

#include "Plane1V.h"
#include "TThread.h"

#include "TTree.h"

/// Trigger Hodoscope plane

class PlaneTrigHodo : public  Plane1V {


private:

#if !defined(__CINT__) && !defined(__CLING__)
  // SciFiJ calibration reading: 1 calibration variable per channel
  class ChannelCalib {
  public:
    int ch;
    float t0;
    int flag;
    ChannelCalib() : ch(0),t0(0),flag(0) {}
    ChannelCalib(const char *s) {
      if(3 != sscanf(s,"%d%f%d",&ch,&t0,&flag)) {
	throw CS::Exception("PlaneTrigHodo::ChannelCalib : bad line \"%s\"",s);
	std::cerr<<"bad line, exception not caught !"<<std::endl;
      }
    }
  };
  
  /// calibration for every channel
  std::vector<ChannelCalib> calib_data;

  friend istream& operator>>(istream& in, PlaneTrigHodo::ChannelCalib &c) {
    in>>c.ch;
    in>>c.t0;
    in>>c.flag;
    return in;
  }
#endif  // __CINT__

  Variable  *fVtc, *fVhch, *fVht, *fVhtcalib, *fVt_vs_spill;
  TH1F *fHtc;
  TH2F *fHtcVSch; 
  TH2F *fHtVSspill;




 public:
  
  PlaneTrigHodo(const char *detname,int nchan, int center, int width):
    Plane1V(detname,nchan,center,width) {}
  
  ~PlaneTrigHodo() {}

  void Init(TTree* tree =0);  

#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event);
#endif

#if USE_DATABASE == 1
  void ReadCalib(const tm& t);
#endif

  ClassDef(PlaneTrigHodo,1)
};

#endif
