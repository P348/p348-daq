#ifndef __PlaneECAL2FEM__
#define __PlaneECAL2FEM__

#include "Plane.h"
#include "PlanePanel.h"
#include "TProfile2D.h"
#include "TThread.h"
//=========== ECAL2FEM parameters =========================
#define nb_FEM_ECAL2cnl  28
#define nSADC_Sampl 32       // number of SADC samples
//=======================================================

class PlaneECAL2FEM : public Plane {
  
  private:
// number of FEM rows/columns
  const int    fNrows, fNcols;

//  SADC Ped/LED windows  
    int pedstart, pedend;
    int ledstart, ledend;

  /// data for each SADC channel        
  int  fSADCRow[nb_FEM_ECAL2cnl], fSADCCol[nb_FEM_ECAL2cnl];
  int  fSADCev[nb_FEM_ECAL2cnl][nSADC_Sampl];

  // markers for histograms
  TH1F *hFEMhit, *hFEMmeanamp, *hFEMdisp, *hFEMamp[nb_FEM_ECAL2cnl];
  TH2F *hPed, *hPedRMS, *hTime, *hSADCsmpl[nb_FEM_ECAL2cnl];
  TH1F *hFEMallamp;

double SADCped(int ipbg, int  ipend, int *data,double &RMS);
double SADCpuls(int ipbg, int  ipend, int *data,double ped,
                                            double &sum,double &time);

 public:
 /*! \brief constructor
    \param detname detector name
    \param nchan number of channels
  */
  PlaneECAL2FEM(const char *detname,int ncols, int nrows, int center, int width);
 ~PlaneECAL2FEM() {}
  
  void StoreDigit(int col, int row,  std::vector<float>& data);
#if !defined(__CINT__) && !defined(__CLING__)
   void StoreDigit(CS::Chip::Digit* digit);
   void EndEvent(const CS::DaqEvent &event);  
#endif

  /// book histograms and branch the tree
  void Init(TTree* tree =0);

  ClassDef(PlaneECAL2FEM,1)
};

#endif



