#include "GroupDAQ.h"
#include "TThread.h"
#include "TStyle.h"
#include "DaqError.h"
#include "Reference.h"
#include "Plane.h"

#include <vector>
#include <bitset>

using std::vector;

ClassImp(GroupDAQ);

void GroupDAQ::SetCenterLabelsX(TH1* h)
{
  const int nx = h->GetXaxis()->GetNbins();
  h->SetNdivisions(nx, "X");
  h->GetXaxis()->CenterLabels();
}

void GroupDAQ::Init() {
  fRateCounter = 0;
  fHistList.push_back(new TH1F("events_per_spill", "events per spill;spill",200,0,200));
  fHistList.push_back(new TH1F_Ref("trigger_bits","trigger bits;bit",12,0,12,fRateCounter));
  SetCenterLabelsX(fHistList[1]);
  fHistList.push_back(new TH1F_Ref("eventsize","Event size;size,bytes",100,0,1E5,fRateCounter));
  
  fHistList.push_back(new TH1F_Ref("errors_number","DAQ Errors number;errors per event",100,0,100,fRateCounter));
  fHistList.push_back(new TH1F_Ref("errors_level","DAQ Errors levels (0=OK,1=MINOR,2=WARNING,3=SEVERE);level",4,0,4,fRateCounter));
  SetCenterLabelsX(fHistList[4]);
  fHistList[4]->SetOption("HIST TEXT0");
  fHistList[4]->SetStats(0);
  
  fHistList.push_back(new TH1F_Ref("trigger_rates","trigger rates",12,0,12,fRateCounter));
  fHistList.push_back(new TH1F_Ref("event_type","event type (6=ENDBURST,7=PHY,8=CAL);type",12,0,12,fRateCounter));
  SetCenterLabelsX(fHistList[6]);
  fHistList[6]->SetOption("HIST TEXT0");
  fHistList[6]->SetStats(0);
  
  fHistList.push_back(new TH1F("time_in_spill","time in spill;time",500,0,20));
  fHistList.push_back(new TH1F("evtnum_in_spill","event number in spill;event",250,0,25000));
  
  fHistList.push_back(new TProfile("errors_sourceID","errors per source ID;sourceID",1024,0,1024));
  fHistList[9]->SetMarkerStyle(kFullDotMedium);
  fHistList[9]->SetOption("P");
  
  fHistList.push_back(new TProfile("errors_in_spill","errors vs spill event;event",5000,0,25000));
  fHistList[10]->SetMarkerStyle(kFullDotMedium);
  fHistList[10]->SetOption("P");
  
  fHistList.push_back(new TProfile("spilltime","spill time stamp;spill;timestamp",200,0,200));
  fHistList[11]->SetMarkerStyle(kFullDotMedium);
  fHistList[11]->SetOption("P");
  
  fHistList.push_back(new TH1F("led_type","led type (0=Off-spill,1=On-spill,2=Total);type",10,0,10));
  SetCenterLabelsX(fHistList[12]);
  fHistList[12]->SetOption("HIST TEXT0");
  fHistList[12]->SetStats(0);
  
  fHistList.push_back(new TH1F("phys_trigger_type","physics trigger type (0=PHYS,1=BEAM,2=RAND,3=BEAMONLY,4=ANY,5=Total 7=ECAL 8=MUONINV);type",10,0,10));
  SetCenterLabelsX(fHistList[13]);
  fHistList[13]->SetOption("HIST TEXT0");
  fHistList[13]->SetStats(0);
  
#if USE_DATABASE == 1
  setDBpt(fDataBase);
#endif
  OpenReference();

  if (fReferenceDirectory) {
    for (int i=1; i<=6;i++) {
      ((TH1F_Ref*)fHistList[i])->SetReference(fReferenceDirectory);
    }
  }
}

void GroupDAQ::EndEvent(const CS::DaqEvent &event, const EventFlags& flags)
{
  fHistList[0]->Fill(event.GetBurstNumber());
  
  //CS::DaqEvent::Header head=event.GetHeader();
  //cout <<head.typeAttribute[0]<<" "<<head.typeAttribute[1]<<dec<<endl;
  const unsigned int triggerwd = event.GetTrigger() ;
  
  const bitset<32> bits = triggerwd;
  for (size_t i = 0; i < bits.size(); ++i)
    if (bits[i]) fHistList[1]->Fill(i);
  
  fHistList[2]->Fill(event.GetLength());
  
  map<int,int> errors_in_src;
  Int_t nof_errors = 0;
  for (const auto& i : event.GetDaqErrors().errors)
  {
    const CS::DaqError::SeverityLevel level = i.GetDaqErrorType().GetSeverityLevel();
    fHistList[4]->Fill(level);
    
    const bool isError = (level != CS::DaqError::OK);
    if (isError)
    {
      nof_errors++;
      
      const int sourceID = i.HasArg(CS::DaqError::SOURCE_ID) ? i.GetArg(CS::DaqError::SOURCE_ID) : 0;
      errors_in_src[sourceID]++;
    }
  }
  
  fHistList[3]->Fill(nof_errors);
  
  for (const auto& i : errors_in_src)
    fHistList[9]->Fill(i.first, i.second);
  
  fHistList[10]->Fill(event.GetEventNumberInBurst(), nof_errors);

  fHistList[6]->Fill(event.GetType());
  fHistList[7]->Fill(event.GetTT().GetTimeInSpill());
  fHistList[8]->Fill(event.GetEventNumberInBurst());
  
  // timestamp value is very large number,
  // explicitly set min Y axis range to make things visible
  const uint32_t t_evt = event.GetTime().first;
  const double Min = fHistList[11]->GetMinimum();
  const bool isMinSet = (Min > 0.);
  if (!isMinSet || (Min > t_evt)) fHistList[11]->SetMinimum(t_evt-5);
  fHistList[11]->Fill(event.GetBurstNumber(), t_evt);
  if (!isMinSet) fHistList[11]->GetYaxis()->SetTimeDisplay(kTRUE);
  
  // led type
  if (flags.isCalibration) {
    fHistList[12]->Fill(2);
    if (flags.isLED0()) fHistList[12]->Fill(0);
    if (flags.isLED1()) fHistList[12]->Fill(1);
  }
  
  // trigger type
  if (flags.isPhysics) {
    fHistList[13]->Fill(5);
    if (flags.isTriggerPhys) fHistList[13]->Fill(0);
    if (flags.isTriggerBeam) fHistList[13]->Fill(1);
    if (flags.isTriggerRand) fHistList[13]->Fill(2);
    if (flags.isTriggerECAL) fHistList[13]->Fill(7);
    if (flags.isTriggerMuonInv) fHistList[13]->Fill(8);
 
   if (flags.isTriggerBeamOnly()) fHistList[13]->Fill(3);
    const bool isANY = flags.isAny();
    if (isANY) fHistList[13]->Fill(4);
  }
};
