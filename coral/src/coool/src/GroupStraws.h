#ifndef __GroupStraws__
#define __GroupStraws__

#include "Group.h"
#include "TGraph.h"

namespace CS { class DaqEvent; }

class TH1F;
class TH2F;

class GroupStraws : public Group
{
  public:
                        GroupStraws             (const char* name);
    void                Init                    (void);
    double rtStraw(int time);
    #if !defined(__CINT__) && !defined(__CLING__)
    /// Fills the histograms
    void                EndEvent                (const CS::DaqEvent &event, const EventFlags &flags);
    #endif

    TH2F* 		fBeamImagery           = nullptr;     //channel Cluster
    TH2F* 		fBeamImagery_onehit    = nullptr;     //channel Cluster
    TH2F* 		fBeamImagery_rt        = nullptr;     //channel Cluster
    TH2F*	    fBeamImagery_rt_onehit = nullptr;

    TH2F* 		fBeamImagery_phys    = nullptr;	      //channel Cluster
    TH2F* 		fBeamImagery_rt_phys = nullptr;       //channel Cluster
    TH2F* 		fBeamImagery_beam    = nullptr;       //channel Cluster
    TH2F* 		fBeamImagery_rt_beam = nullptr;       //channel Cluster
    TH1F*               fChannels = nullptr;  
  //TH1F*               fChannel32 = nullptr;  
    TH1F*               fTime1 = nullptr;
    TH1F*               fTimeX = nullptr;
    TH1F*               fTimeY = nullptr;

    TH1F*               fTimeY28 = nullptr;
    TH1F*               fTimeY30 = nullptr;
    TH1F*               fTimeY32 = nullptr;

    TH1F*               fTimeX28 = nullptr;
    TH1F*               fTimeX30 = nullptr;
    TH1F*               fTimeX32 = nullptr;


  

//    TGraph* tg1;
    TProfile* histoX_vs_spill = nullptr;
    TProfile* histoY_vs_spill = nullptr;
    TProfile* histoEff_vs_spill = nullptr;
    TH2F* 		fChannelDoubleClustersX = nullptr;	        //channel Cluster
    TH2F* 		fChannelDoubleClustersY = nullptr;	        //channel Cluster

    
  ClassDef              (GroupStraws,1)
};

#endif
