//official version 26 August 2009
//official version 20 September 2009
// 3 points around i_max 26.oct.09 
// parabola around i_max 28.oct.09
////   official version 22 Aprile 2015
//
//  version 19 May 2016
 //
 //          
#include <cmath>
  
#include "PlaneHCAL1.h"
#include "ChipSADC.h"
#include "GroupDAQ.h"

#include "DaqEvent.h"

#include <string.h>
#include <cstring>
#include <cstdlib>
#include <sstream>

//#include "TColor.h"
#include <cstdio>
#include "Plane2V.h"
#include "PlaneHCAL1.h"
//#include <strstream>
#include <iomanip>
#include <iostream>
 
#include <TProfile.h>
#define  fNsamples 32
#define  pedlen 5
//---------------------------- my def -----------------

#define  POROG 10
#define AMPMAX 1024
#define HITMAX 100
 
using namespace std;

ClassImp(PlaneHCAL1);
//const int PlaneHCAL1::fAmpCut = 5;
const int PlaneHCAL1::fAmpCut = 15;

//--------------------------------------------------------------------------------------------  
void PlaneHCAL1::Init(TTree* tree) {
//--------------------------------------------------------------------------------------------  
  Plane2V::Init(tree);

      if(strncmp(GetName(),"HC01P1S1",8)== 0) {fNrows = 5; fNcols=6;}
      if(strncmp(GetName(),"HC01P1S2",8)== 0) {fNrows = 4; fNcols=7;}
      if(strncmp(GetName(),"HC01P1S3",8)== 0) {fNrows = 5; fNcols=7;}
      if(strncmp(GetName(),"HC01P1S4",8)== 0) {fNrows = 4; fNcols=6;}
      if(strncmp(GetName(),"HC01P1__",8)== 0) {fNrows = 20; fNcols=28;}


      if(strncmp(GetName(),"HC02P1S1",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1S2",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1S3",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1S4",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1__",8)== 0) {fNrows = 10; fNcols=22;}


//--------------------------------------------------------------------------

  char cell_name[132];

  std::string amprowcol;
  std::string name, title;

// 6---------------------------
  std::string ampname = fName + "_Sumampcut";
  fHsac=new TH1F(ampname.c_str(),ampname.c_str(), AMPMAX, 0, AMPMAX);  		 
  fHsac->SetFillColor(8);
  AddHistogram(fHsac);
// 7 --------------------------------------------------------------------------
  std::string adrname = fName + "_adr";
  fHch=new TH1F_Ref( adrname.c_str(),adrname.c_str(), fNrows*fNcols, 0,
                     fNrows*fNcols, fRateCounter);
  ((TH1F_Ref*)fHch)->SetReference(fReferenceDirectory);
  fHch->SetFillColor(3);
  AddHistogram(fHch);
// 8 --------------------------------------------------------------------------  
  std::string aadrcut = fName + "_adrVSampcut";
  fHchac = new TH2F(aadrcut.c_str(), aadrcut.c_str(), 
		    fNrows*fNcols, 0, fNrows*fNcols,
		    fVamp->GetNbins(), fVamp->GetMin(), 
		    fVamp->GetMax());
               fHchac->SetOption("boxcol");
  AddHistogram(fHchac);
// 9 ------------------------------------------------------------------------------   
  std::string ampmaxname = fName + "_maxamp";
  fHma=new TH1F_Ref(ampmaxname.c_str(),ampmaxname.c_str(),AMPMAX, 0, AMPMAX,fRateCounter); 
 //                      fVamp->GetNbins(), fVamp->GetMin(),
 //                      fVamp->GetMax(), fRateCounter);
  ((TH1F_Ref*)fHma)->SetReference(fReferenceDirectory);
  fHma->SetFillColor(5);
  AddHistogram(fHma);
// 10---------------------------------------------------------------------------------------------------------    
  std::string hitsthname = fName + "_hitsth";
  fHhth=new TH1F_Ref(hitsthname.c_str(),hitsthname.c_str(), 50, 0, 50, fRateCounter);     // like in first histo
  //   fNrows*fNcols, 0, fNrows*fNcols);
  ((TH1F_Ref*)fHhth)->SetReference(fReferenceDirectory);
  fHhth->SetFillColor(7);
  AddHistogram(fHhth);
  
// 11-------------------------------------------------------------------------------

  std::string xyacut = fName + "_xVSyAcut";
  fHxyac = new TH2F(xyacut.c_str(), xyacut.c_str(),
                      fNcols, 0, fNcols, fNrows, 0, fNrows);
//               fHxyac->SetOption("boxcolz");
               fHxyac->SetOption("boxcol");
               fHxyac->SetFillColor(4);
  AddHistogram(fHxyac);

// 12 -------------------------------------------- 
  std::string xynoise = fName + "_xVSynoise";
  fHxyn = new TH2F(xynoise.c_str(), xynoise.c_str(),
                      fNcols, 0, fNcols, fNrows, 0, fNrows);
//  fHxyn->SetOption("boxcolz");
  fHxyn->SetOption("boxcol");
  AddHistogram(fHxyn);

//---------------------------------  amp -----------------------
name = fName + "_Sum_amp";
fSum_amp = new TH1F(name.c_str(),name.c_str(),AMPMAX,0,AMPMAX);
AddHistogram(fSum_amp);
//------------------------------------------------------------------------------
name = fName + "_Bad";
   fBad = new TH1F(name.c_str(),name.c_str(),AMPMAX,0,AMPMAX);
  AddHistogram(fBad);
   
name = fName + "_Good";
   fGood = new TH1F(name.c_str(),name.c_str(),AMPMAX,0,AMPMAX);
  AddHistogram(fGood);
//---------------------------------------------------------------



//  if(strncmp(GetName(),"HC01P1__",8)== 0) 
{

//if(strncmp(GetName(),"HC01P1__",8)== 0)   PlaneHCAL1::Calib_coef();

// 13 --------------------------- led --------------------------------------------------
//   
name = fName + "_LED_sum";
  fSum_led = new TH1F(name.c_str(),name.c_str(), AMPMAX/4, 0, AMPMAX);
  fSum_led->SetFillColor(30);
  AddHistogram(fSum_led);
// 14-------------------- 
name = fName + "_LED_xy";
   fAmpLed = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
//   fAmpLed->SetOption("colz");
   fAmpLed->SetOption("col");
 //  fAmpLed->SetFillColor(2);
 //    fAmpLed->SetMinimum(0.0);
  AddHistogram(fAmpLed);
// 15 -----------------------------------
name = fName + "_LED_adr";
   fHchampled = new TH2F(name.c_str(),name.c_str(),fNrows*fNcols,0,fNrows*fNcols,AMPMAX/2,0,AMPMAX);
//  Int_t ci;   // for color index setting
//  ci = TColor::GetColor("#c82dd2");

//   fHchampled->SetMarkerColor(ci);
//   fHchampled->SetMarkerColor(1);
//   fHchampled->SetMarkerStyle(20);
//   fHchampled->SetMarkerSize(0.4);
   fHchampled->SetOption("boxcol");
   fHchampled->SetMinimum(0.0);
   fHchampled->SetMaximum(1200.0);
      AddHistogram(fHchampled);
//
//16 ---------------------------- ped ----------------------------------------------------------

name = fName + "adr_Ped";
fPed_ch = new TH2F(name.c_str(),name.c_str(),fNrows*fNcols,0,fNrows*fNcols,AMPMAX/4,0,AMPMAX/2);
   fPed_ch->SetOption("boxcol");
   fPed_ch->SetMinimum(0.0);
   AddHistogram(fPed_ch);
// 17 -----------------
name = fName + "_xy_Ped";
fPed_xy = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
//fPed_xy->SetOption("boxcolz");
fPed_xy->SetOption("boxcol");
     AddHistogram(fPed_xy);
// 18     
name = fName + "_sum_Ped";
fPed_sum = new TH1F(name.c_str(),name.c_str(),AMPMAX/4,0,AMPMAX/2);
   AddHistogram(fPed_sum);
     
//------------------------------------- rms ped ---------------------------------
// 19
name = fName + "adr_RMS_Ped";
   fRMS_Ped_ch = new TH2F(name.c_str(),name.c_str(),fNrows*fNcols,0,fNrows*fNcols,100,0,100);
   fRMS_Ped_ch->SetOption("boxcol");
  AddHistogram(fRMS_Ped_ch);
// 20
name = fName + "_xy_RMS_Ped";
fRMS_Ped_xy = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
//     fRMS_Ped_xy->SetOption("boxcolz");
     fRMS_Ped_xy->SetOption("boxcol");
     AddHistogram(fRMS_Ped_xy);
// 21
name = fName + "_sum_RMS_Ped";
fRMS_Ped_sum = new TH1F(name.c_str(),name.c_str(),100,0,100);
   AddHistogram(fRMS_Ped_sum);
//
//----------------------------- Time ------------------------------------------
//	22
 name = fName + "xy_Time";
fTime_xy = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
//     fTime_xy->SetOption("boxcolz");
     fTime_xy->SetOption("boxcol");
     AddHistogram(fTime_xy);
//	23

name = fName + "sum_time";
   fSum_time = new TH1F_Ref(name.c_str(),name.c_str(),320,0,32,fRateCounter);
  AddHistogram(fSum_time);
((TH1F_Ref*)fSum_time)->SetReference(fReferenceDirectory);

//-----------------------------------------------------------------------------

name = fName + "_Sum_sampl";
fSum_sampl = new TH2F(name.c_str(),name.c_str(),32,0,32,AMPMAX/4,0,AMPMAX);
AddHistogram(fSum_sampl);
//-------------------------------------------------------


if (fExpertHistos)
 {

//  
//------------------------------------------------------------------------------
//

  for( int x = 0; x < fNcols; x++) {
  for( int y = 0; y < fNrows; y++) {

if(strncmp(GetName(),"HC01P1__",8)== 0){
if (y > 7 && y < 12 && x > 9 && x < 18) continue;
if ( (x==0&&y==0)||(x==0&&y==1)||(x==0&&y==2)||(x==0&&y==3) ) continue;
if ( (x==1&&y==0)||(x==1&&y==1)||(x==1&&y==2)||(x==1&&y==3) ) continue;
if ( (x==2&&y==0)||(x==2&&y==1) ) continue;
if ( (x==3&&y==0)||(x==3&&y==1) ) continue;

if ( (x==24&&y==0)||(x==24&&y==1) ) continue;
if ( (x==25&&y==0)||(x==25&&y==1) ) continue;
if ( (x==26&&y==0)||(x==26&&y==1)||(x==26&&y==2)||(x==26&&y==3) ) continue;
//if ( (x==27&&y==0)||(x==27&&y==1)||(x==27&&y==2)||(x==27&&y==3) ) continue;
if ( (x==27&&y==1)||(x==27&&y==2)||(x==27&&y==3) ) continue;

if ( (x==0&&y==19)||(x==0&&y==18)||(x==0&&y==17)||(x==0&&y==16) ) continue;
if ( (x==1&&y==19)||(x==1&&y==18)||(x==1&&y==17)||(x==1&&y==16) ) continue;
if ( (x==2&&y==19)||(x==2&&y==18) ) continue;
if ( (x==3&&y==19)||(x==3&&y==18) ) continue;

if ( (x==24&&y==19)||(x==24&&y==18) ) continue;
if ( (x==25&&y==19)||(x==25&&y==18) ) continue;
if ( (x==26&&y==19)||(x==26&&y==18)||(x==26&&y==17)||(x==26&&y==16) ) continue;
if ( (x==27&&y==19)||(x==27&&y==18)||(x==27&&y==17)||(x==27&&y==16) ) continue;
}

// int adr=y+x*fNrows;

//-----------------------------------------------------------
 
sprintf(cell_name,"Time_X_%d_Y_%d",x,y);
name  = fName + cell_name;
title = fName + cell_name;
  fCH_time[x][y] = new TH1F(name.c_str(), title.c_str(), 320, 0, 32);
  AddHistogram(fCH_time[x][y]);

//-----------------------------------------------------------


sprintf(cell_name,"Ampl_X_%d_Y_%d",x,y);
name  = fName + cell_name;
title = fName + cell_name;
  fCH_amp[x][y] = new TH1F(name.c_str(), title.c_str(),AMPMAX,0,AMPMAX);
  AddHistogram(fCH_amp[x][y]);

//-----------------------------------------------------------

sprintf(cell_name,"Samp_X_%d_Y_%d",x,y);
name  = fName + cell_name;
title = fName + cell_name;
    fSProfile[x][y] = new TH2F(name.c_str(),title.c_str(),32,0,32,AMPMAX/4,0,AMPMAX);
    fSProfile[x][y]->SetMinimum(0.0);  
    AddHistogram(fSProfile[x][y]);

//-------------------------------------------------

   }}
   
 } // expert

} // end HC01P1__

}  // end void PlaneHCAL1::Init(TTree* tree)
//
//
//
//
//
//

//-----------------------------------------------------------------------------

void PlaneHCAL1::EndEvent(const CS::DaqEvent &event) {
//double ampl;
//double ampl0;
//Stat_t bb1;                         
//Stat_t ee1;                         
//Stat_t limit;                            

if(fNhits==0) return;

// if(fNhits>1) return;  // mu 1 conditions;

  if (thr_flag) MYLOCK();

      if(strncmp(GetName(),"HC01P1S1",8)== 0) fNrows = 5;
      if(strncmp(GetName(),"HC01P1S2",8)== 0) fNrows = 4;
      if(strncmp(GetName(),"HC01P1S3",8)== 0) fNrows = 5;
      if(strncmp(GetName(),"HC01P1S4",8)== 0) fNrows = 4;
      if(strncmp(GetName(),"HC01P1__",8)== 0) {fNrows = 20; fNcols=28;}


      if(strncmp(GetName(),"HC02P1S1",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1S2",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1S3",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1S4",8)== 0) {fNrows = 2;  fNcols=5;}
      if(strncmp(GetName(),"HC02P1__",8)== 0) {fNrows = 10; fNcols=22;}

         const CS::DaqEvent::Header head = event.GetHeader();
         const CS::DaqEvent::EventType eventType = head.GetEventType();

//         const CS::uint32 Atr = head.GetTypeAttributes()[0]&0xf8000000;
//       if(eventType == 8 && Atr==0x68000000) 
// ----------------- calibration event -----------------
//       if(1)//eventType == 8 && Atr==0x68000000)  

  if(eventType == 8) {   // LEDs = calibration event ------

//if(fNhits==0) return;
// if(strncmp(GetName(),"EC00P1__",8)== 0) 
     {
      for (int i=0; i<fNhits; i++) {
	int row=fRow[i];    // y
	int col=fCol[i];   // x

	//cout<<" x = "<<col<<"  y = "<<row<<endl; 
	
//if (row<=32  && row >=18 && col<=35 && col>=15) continue; // ECAL0 window 
//	  printf("row = %d, col = %d\n",row,col); 
//	int amp=fAmp[i];

       double amp=(double)fAmp[i]/100;	
	  int adr=fRow[i]+fCol[i]*fNrows; 

if(fVrow->Test(row) && fVcol->Test(col) && fVamp->Test(amp))
	{
if(amp>1) {
	  //cout<<"ECAL0 LED --> x = "<<col<<"  y = "<<row<<"  led = "<<ECAL0_Led[col][row]<<endl; 
	  //--------------------------------------------------------------------------------------
	  //
	  //   ----------------- LED filling -----------------------------------------------------
	  //
	  fHchampled->Fill(adr,amp); // original LED values
	  fSum_led->Fill(amp); 
	  //                                fProf->Fill(adr,amp);
	  if (amp>5)                   fAmpLed ->Fill(col,row,amp);                       
	  //-------------------------------------------------
	  // bb1 = fProf->GetBinContent(adr);                           
	  // ee1 = fProf->GetBinError(adr);                           
	  // ee1 = fProf->GetEntries()/476.;                           
			} // amp >1
		}//if(fVrow->Test(row) && fVco
	}//for (int i=0; i<fNhits; i++
}//if(strncmp(GetName(),"HC01P1__",8)== 0)
               
  } // if(eventType == 8 && Atr==0x68000000) // LEDs = calibration event -----------------
  else 
{

//  int adrmax=0;
  int hcut = 0;
  int sumcut = 0; 
  int ampmax = 0; 
  int xampmax = -1;
  int yampmax = -1;
//---------------------------------------------------------------------------

for (int i=0; i<fNhits; i++) {
    int row=fRow[i];    int col=fCol[i];    

    double amp=(double)fAmp[i]/100;

if (amp==0) continue;

if(fVrow->Test(row) && fVcol->Test(col) && fVamp->Test(amp)) {
      int adr=fRow[i]+fCol[i]*fNrows; 

//      if(strncmp(GetName(),"HC01P1S1",8)== 0) cerr<<"col= "<<col<<" row= "<<row<<" adr= "<<adr<<" fNrows= "<<fNrows<<endl;

      if (amp> ampmax)  {  ampmax =amp;  xampmax = col; yampmax = row;}

if(fNhits>HITMAX+1) continue;

      if (amp > fAmpCut) {
         hcut++;     
         sumcut+=amp;
         fHchac->Fill(adr,amp);
         fHxyac->Fill(col,row,amp);
         fHch->Fill(adr);

fSum_amp->Fill(amp);

if(fNhits<=2) fGood->Fill(amp); else fBad->Fill(amp) ;


      }
    
       fHrca->Fill(col,row,amp);
       fHrc1->Fill(col,row);
       fHa->Fill(amp);
      

    fHavsadr->Fill(adr,amp);
      
      fVrow->Store(row);
      fVcol->Store(col);
      fVamp->Store(amp);

      fNhitsKept++;
    }
  //cout<<"amp = "<<amp<<endl;
}

  if(sumcut>0) fHsac->Fill(sumcut);
  if(ampmax>0) fHma->Fill(ampmax);
  fHhth->Fill(hcut);
  fHhit->Fill(fNhitsKept);
  if (ampmax < fAmpCut) fHxyn->Fill(xampmax,yampmax);

}
  if (thr_flag) MYUNLOCK();

} // end void


//--------------------------------------------------------
//
void PlaneHCAL1::StoreDigit(int col, int row, std::vector<float>& data) {
  //

  if(strncmp(GetName(),"HC01P1S1",8)== 0) {fNrows = 5; fNcols=6;}
  if(strncmp(GetName(),"HC01P1S2",8)== 0) {fNrows = 4; fNcols=7;}
  if(strncmp(GetName(),"HC01P1S3",8)== 0) {fNrows = 5; fNcols=7;}
  if(strncmp(GetName(),"HC01P1S4",8)== 0) {fNrows = 4; fNcols=6;}
  if(strncmp(GetName(),"HC01P1__",8)== 0) {fNrows = 20; fNcols=28;}

  if(strncmp(GetName(),"HC02P1S1",8)== 0) {fNrows = 2;  fNcols=5;}
  if(strncmp(GetName(),"HC02P1S2",8)== 0) {fNrows = 2;  fNcols=5;}
  if(strncmp(GetName(),"HC02P1S3",8)== 0) {fNrows = 2;  fNcols=5;}
  if(strncmp(GetName(),"HC02P1S4",8)== 0) {fNrows = 2;  fNcols=5;}
  if(strncmp(GetName(),"HC02P1__",8)== 0) {fNrows = 10; fNcols=22;}

  int sadclen=data.size()-2;
  if(sadclen<1) return;
  //

  double ped,ped2,rms_ped;
  int aaa;
  double amp = 0., y_sum;
  int min_amp;
  int max_amp;
  int i_max,i_max_2;
  int i_min;
  int sadc_amp;
  double time_mean0;

  double a_par,b_par,c_par,y_max;
  double x1,x2,x3;
  double y1,y2,y3;
  int i,i1,i2,i3;

  //char cell_m[100];

  if (fNhits < fNchan*fMAX_MULT) {
    fRow[fNhits]=row;
    fCol[fNhits]=col;
    int fSADCev[fNsamples];
    int len=fNsamples;
    if(sadclen<len){ len=sadclen;
      for(i=len;i<fNsamples;i++) fSADCev[i]=0; }
    for(i=0;i<len;i++) {fSADCev[i]=(int)data[i+2];}


    //
    //------------------------ pedestalls  --------------------------------------------    

    ped=0;
    for(i=0;i<pedlen;i++) ped+=fSADCev[i];  
    ped=ped/pedlen;
    //cout<<"ped = "<<ped<<endl;    

    aaa=row+fNrows*col;
    ped2 = ped;
    if(ped>1020) ped2=1020;
    fPed_sum->Fill(ped2);    //  total pedestall filling
    fPed_ch->Fill(aaa,ped2); // pedestall filling
    fPed_xy->Fill(col,row,ped2); // pedestall x-y filling
    //sprintf(cell_m,"cell_%d",aaa);
    //rms_ped=fPed_ch->ProjectionY(cell_m,aaa,aaa)->GetRMS(1);

    rms_ped=fPed_sum->GetRMS(1);

    fRMS_Ped_ch  -> Fill(aaa,rms_ped); // RMS ped filling
    fRMS_Ped_sum -> Fill(rms_ped);
    fRMS_Ped_xy  -> Fill(col,row,rms_ped);
    //-----------------------------------------------------------------------




    //----------------------------- maximum & minimum samle search -------------
    //
    y_sum=0;
    //int treshould=10;
    //int diff;
    //int k_sampl=0;
    y_max=0;
    min_amp=10000;
    max_amp=0;
    i_max_2=0;
    i_max=40;
    i_min=-1;
    for(i=pedlen; i<len-pedlen; i++) {     
      sadc_amp=fSADCev[i];
      if(sadc_amp < min_amp )  {min_amp = sadc_amp;  i_min = i;}
      if(sadc_amp > max_amp )  {max_amp = sadc_amp;  i_max = i;}


    } 
    //
    //--------------------------- parabola to obtain time and amp -------------------------
    //
    // y=a*x**2 + b*x +c ;
    // x_max=-b/(2*a);
    // y_max=(4*a*c-b*b)/(4*a) ;

    i1=i_max-1; i2=i_max;  i3=i_max   + 2;

    x1=(double)i1; x2=(double)i2; x3=(double)i3;
    y1=(double)fSADCev[i1]; y2=(double)fSADCev[i2]; y3=(double)fSADCev[i3];



    a_par=(y3-(x3*(y2-y1)+x2*y1-x1*y2)/(x2-x1))/(x3*(x3-x1-x2)+x1*x2);
    b_par=a_par*(x1+x2)-(y2-y1)/(x2-x1);
    c_par=(x1*y2-x2*y1)/(x1-x2) + a_par*x2*x1 ;

    time_mean0=0;

    if(a_par != 0) {time_mean0=b_par/(2.*a_par);y_max=(4.0*a_par*c_par - b_par*b_par)/(4.0*a_par) ;}
    //
    //------------------------------------------------------
    //

    if(time_mean0>5 && time_mean0<20)
      {

	fSum_time->Fill(time_mean0);
	fTime_xy->Fill(col,row,time_mean0);

	double t1=fSum_time->GetMean(1); double s1=fSum_time->GetRMS(1);
	if(fabs(time_mean0-t1)<3.*s1) 
	  {
	    for(i=0; i<fNsamples; i++)  fSum_sampl->Fill(i,fSADCev[i]-ped);   
	  }
	amp=y_max-ped;
      }
    //-----------------------------------------------------------------------
    //
    if (fExpertHistos && amp>fAmpCut) 
      { 
	fCH_time[col][row]->Fill(time_mean0);    // fill time_mean0

	double time_mean=fCH_time[col][row]->GetMean(); // new time arounn mean
	double time_sigm=fCH_time[col][row]->GetRMS(); // new time arounn mean

	if(fabs(time_mean0-time_mean)<3.*time_sigm) fCH_amp[col][row] ->Fill(amp); 

	for(i=0; i<sadclen; i++)  fSProfile[col][row]->Fill(i,fSADCev[i]-ped);  

      } // end Expert histos  




 
    fAmp[fNhits]=(int)(amp*100);
    fNhits++;

  }//if (fNhits <
}// void StoreDigit

//---------------------------------------------------------------------------

void PlaneHCAL1::StoreDigit(CS::Chip::Digit* digit) {
  std::vector<float> data=digit->GetNtupleData();
  if(data.size()<3) return;
  const CS::ChipSADC::Digit* sadcdig = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
  if(sadcdig != NULL ) {
         int ix=sadcdig->GetX(); int iy=sadcdig->GetY();
         if(ix < 0||iy<0||ix>fNcols||iy>fNrows) return; 
         this->StoreDigit(ix,iy,data);
  }
}
