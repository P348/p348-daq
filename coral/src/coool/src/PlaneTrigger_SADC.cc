#include "PlaneTrigger_SADC.h"

ClassImp(PlaneTrigger_SADC)

#define AMPMAX 1024

const int PlaneTrigger_SADC::fMAX_MULT    =   8;
const int PlaneTrigger_SADC::fRATE_UPDATE = 100;

PlaneTrigger_SADC::PlaneTrigger_SADC(const char *detname, int nAdcChan, int center, int width) : 
    Plane(detname), fNAdcChan(nAdcChan), fCenter(center), fWidth(width), inTime(false) {
    max_sadc = new float[fNAdcChan];
    integral_sadc = new float[fNAdcChan];
}

PlaneTrigger_SADC::~PlaneTrigger_SADC(void) {
    delete [] max_sadc;
    delete [] integral_sadc;
}

void PlaneTrigger_SADC::Init(TTree* tree) {
    //booking histograms

    // only SADC stuff to follow
    std::string name = fName + "sadc_ch";
    std::string title = fName + "SADC nbr of entries";
    std::string sadc_ch =name;
    h1_adc_ch = new TH1F_Ref(name.c_str(), title.c_str(),fNAdcChan,0.5,fNAdcChan+0.5,fRateCounter);
    h1_adc_ch->SetMinimum(0);
    if (fNAdcChan > 0)
        AddHistogram(h1_adc_ch);


    name = fName + "_Sum_sampl";
    fSum_sampl = new TH2F(name.c_str(),name.c_str(),32,0,32,AMPMAX/4,0,AMPMAX);
    AddHistogram(fSum_sampl);

    //single histograms per channel
    for(int ch=1; ch<=fNAdcChan; ch++)
    {
        char buff1[255], buff2[255];

        sprintf(buff1,"%s_sadc_ch%d",fName.c_str(),ch);
        sprintf(buff2,"%s_SADC Samples for channel %d",fName.c_str(),ch);
        h2_ADC_samples.push_back(new TH2F(buff1, buff2,32,0,32,1024,0,1024));
        if (fNAdcChan > 0)
            AddHistogram(h2_ADC_samples.back());

        sprintf(buff1,"%s_sadc_ch%d_max",fName.c_str(),ch);
        sprintf(buff2,"%s_SADC Maximum amplitude for channel %d",fName.c_str(),ch);
        h1_ADC_samples_max.push_back(new TH1F(buff1, buff2,1024,0,1024));
        if (fNAdcChan > 0)
            AddHistogram(h1_ADC_samples_max.back());

        sprintf(buff1,"%s_sadc_ch%d_integral",fName.c_str(),ch);
        sprintf(buff2,"%s_SADC Amplitude integral for channel %d",fName.c_str(),ch);
        h1_ADC_samples_integral.push_back(new TH1F(buff1, buff2,2000,0,2000));
        if (fNAdcChan > 0)
            AddHistogram(h1_ADC_samples_integral.back());
    }

    
    if(tree) {
        fIsInTree = true;

        for (int ch = 1; ch <= fNAdcChan; ch++) {

            char buff1[255], buff2[255];
            sprintf(buff1,"%s_sadc_ch%d_max",fName.c_str(),ch);
            sprintf(buff2,"%s_sadc_ch%d_max/F",fName.c_str(),ch);

            tree->Branch(buff1,&(max_sadc[ch-1]),buff2,32000);

            char buff3[255], buff4[255];
            sprintf(buff3,"%s_sadc_ch%d_integral",fName.c_str(),ch);
            sprintf(buff4,"%s_sadc_ch%d_integral/F",fName.c_str(),ch);

            tree->Branch(buff3,&(integral_sadc[ch-1]),buff4,32000);

        }

    }

}

void PlaneTrigger_SADC::put_in_hist(TH2F *h2,bool subtract_offset ,const ChannelInfo& ci)
{
    if (!ci.digit_sadc)
        return;
    const vector<CS::uint16> vsamples = ci.digit_sadc->GetSamples();

    float offs = subtract_offset ? ci.offs : 0;

    double nb_entries = h2->GetEntries();
    for(size_t i = 0; i < vsamples.size(); i++) {
        if ( vsamples.size() != 32 ) continue;
        h2->Fill(i, vsamples[i] - offs);
    }
    h2->SetEntries(nb_entries +1);
}


void PlaneTrigger_SADC::StoreDigitSADC(const CS::ChipSADC::Digit* d_sadc)
{
    digits_sadc.push_back(d_sadc);

    int ch = d_sadc->GetX()*8 + d_sadc->GetY()+1;

    if(ch < 1 || ch > fNAdcChan)
    {
        cerr<<"Bad Channel Number: "<< ch <<endl;
        return;
    }

    const size_t window_width = 3;  // First and last window_width samples are used to determine the offset.
    double sum=0;
    double sum2 = 0;
    float integral = 0;
    CS::uint16 ampl_max = 0;
    CS::uint16 ampl_min = 1023;
    const vector<CS::uint16>& vsamples = d_sadc->GetSamples();
    for( size_t i=0; i<vsamples.size(); i++ )
    {
        if (vsamples.size() != 32) {
            integral = -2000;
            continue;
        }
        // Calculate average + fluctuation only for first few samples
        if( i < window_width )
        {
            sum += vsamples[i];
            sum2 += vsamples[i]*vsamples[i];
        }
        if( i > 31-window_width )
        {
            sum += vsamples[i];
            sum2 += vsamples[i]*vsamples[i];
        }

        integral += vsamples[i];

        if( ampl_max <= vsamples[i] )
            ampl_max = vsamples[i];
        if( ampl_min >= vsamples[i] )
            ampl_min = vsamples[i];


    //Fill fSum_sampl 
    fSum_sampl->Fill(i,vsamples[i]);   

    }

    

    float offset = sum/(window_width*2);
    float rms = sum2/(window_width*2) - offset*offset;

    //if (ch == 4) {
    //     cerr << ch << " sum : " << sum << " offset: "<< offset << endl;
    //}

    integral -= vsamples.size()*offset;


    ChannelInfo& chi = channel_infos[ch-1];
    assert (!chi.digit_sadc);

    chi.min = ampl_min;
    chi.max = ampl_max;
    chi.offs = offset;
    chi.rms = rms;
    chi.integral = integral+100;
    chi.pileup = rms >= 6;
    chi.digit_sadc = d_sadc;

}

void PlaneTrigger_SADC::StoreDigit(CS::Chip::Digit* digit)
{
    const CS::ChipSADC::Digit* d_sadc = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
    if (d_sadc != NULL)
    {
        StoreDigitSADC(d_sadc);
        return;
    }

}

void PlaneTrigger_SADC::EndEvent(const CS::DaqEvent &event)
{
    if (thr_flag) TThread::Lock();


    //Get SADC informations

    for (int ch = 1; ch <= fNAdcChan; ch++)
    {
        const ChannelInfo& chi = channel_infos[ch-1];

        if (!chi.digit_sadc) {
            max_sadc[ch-1] = -3000;
            integral_sadc[ch-1] = -3000;
            continue;
        }
        max_sadc[ch-1] = chi.max;
        integral_sadc[ch-1] = chi.integral;

        //----------Drawing histos----------------------

        h1_adc_ch->Fill(ch);

        h1_ADC_samples_max[ch-1]->Fill(chi.max);
        h1_ADC_samples_integral[ch-1]->Fill(chi.integral);

        put_in_hist(h2_ADC_samples[ch-1], false,chi);

        //cerr<<"Channel Integral: "<< ch << " : " << integral_sadc[ch-1] <<endl;
    }


    if (thr_flag) TThread::UnLock();
}

void PlaneTrigger_SADC::Reset(void)
{
    Plane::Reset();

    for(int i = 0; i < fNAdcChan; i++) {
        max_sadc[i] = 0;
        integral_sadc[i] = 0;
    }

    for (map<int, ChannelInfo>::iterator it = channel_infos.begin();
            it != channel_infos.end(); it++)
    {
        it->second.clear();
    }

    channel_infos.clear();

    digits_sadc.clear();
}


void PlaneTrigger_SADC::ControlPanel(const TGWindow* p, const TGWindow* main) {
    if (!fControlPanel) fControlPanel = new PlanePanel(p, main, 100, 100, this);
}



