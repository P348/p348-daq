#ifndef __MY_LOCK_H__
#define __MY_LOCK_H__

#define PRINT_LINE 0

#define MYLOCK() { if(PRINT_LINE) printf( "Lock in %d, file(%s)\n", __LINE__, __FILE__ ); TThread::Lock();}
#define MYUNLOCK() {  if(PRINT_LINE) printf( "UnLock in %d, file(%s)\n", __LINE__, __FILE__ ); TThread::UnLock();}

#endif
