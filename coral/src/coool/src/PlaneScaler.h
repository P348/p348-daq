#ifndef __PlaneScaler__
#define __PlaneScaler__

#include "TProfile.h"
#include "Plane.h"
#include <vector>

/*! \brief class for Scalers
  \author Colin Bernet
*/
class PlaneScaler : public  Plane {
  
 private:
  static const int fDepth;
  static const int fNchan;

  bool fIsEbe;
  
  //          don't delete comment-looking "[fNchan]" (needed by rootcint)
  //Int_t*  fChanMult;    //[fNchan]
  //Int_t*  fLastCounts;  //[fNchan]
  //Int_t*  fCounts;      //[fNchan]
  std::vector<int> fChanMult;
  std::vector<int> fLastCounts;
  std::vector<int> fCounts;
  
  int  fPattern;
  int  fTime;
  int  fLastTime;
  Float_t fDeltaTime;
  int fNEvents;

  /// times histogram
  TH1F *fHtimes;

  /// counts histogram
  TH1F *fHcounts;
  
  /// rates histogram 
  TProfile *fHrates;

  /// beam profile
  TProfile *fHSpillProfile;

  /// number of muons per spill 
  TH1F *fHIntensity;

  // Histogram borders
  float spill_start = 0; 
  float spill_end =  5.6;
  int spill_bins = 56;

  // beam duty factor calculation
  TProfile* fHMiddle;
  TProfile* fHMiddleT;
  TProfile* fHMiddleTD;
  TProfile* fHOuter;
  TProfile* fHOuterT;
  TProfile* fHOuterTD;
  TProfile* fHRandom;
  TH1D* fHDutyFactor;

  // veto counts in spill 
  TProfile* fHVeto;

  // number of muons per spill
  int  fBeamhitspspill;      

  // spill number
  int  fSpillnum;      


 public:
  
  /*! \brief constructor
    \param detname detector name
    \param ebe 1 if the scaler is read event by event, 0 otherwise

    \todo only event by event mode implemented right now
  */
  PlaneScaler(const char *detname,int ebe);
  virtual ~PlaneScaler();
  
#if !defined(__CINT__) && !defined(__CLING__)
  void StoreDigit(CS::Chip::Digit* digit);
  void EndEvent(const CS::DaqEvent &event);
#endif
  void Init(TTree* tree =0);

  void Reset();

  virtual void NewRun(int runnumber = 0);
  void End();
 
  void ControlPanel(const TGWindow* p, const TGWindow* main); 

  int GetTime() const {return fTime;}
  float GetTimeSeconds() const {return fTime/38.88E6;}
  float GetTimeInterval() const {return fDeltaTime;}
  int GetPattern() const {return fPattern;}
  //int* GetCounts() const {return fChanMult;}

  ClassDef(PlaneScaler,0)
};

#endif




