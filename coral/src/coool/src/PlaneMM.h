#ifndef __PlaneMM__
#define __PlaneMM__

#include "PlaneAPV.h"
#include "TThread.h"

#include "conddb.h"

// forward declaration of ROOT classes
class TProfile;

/// Plane for NA64 MicroMega detector (readout with APV chips)

class PlaneMM : public PlaneAPV {
  
 protected:
  // hit histograms
  TH1F*     fHoccup;      // occupancy
  TH2F*     fHchvsa0;     // channel vs amplitude - A0
  TH2F*     fHchvsa1;     // channel vs amplitude - A1
  TH2F*     fHchvsa2;     // channel vs amplitude - A2
  TProfile* fHmeanAmpa0;    // mean hit amplitude per strip - A0
  TProfile* fHmeanAmpa1;    // mean hit amplitude per strip - A1
  TProfile* fHmeanAmpa2;    // mean hit amplitude per strip - A2
  TH2F*     fHampRatio;   // amplitude ratio

  // cluster variables
  Variable *fVcPos, *fVcAmp, *fVcSize;

  //hitposition variable
  bool has_hit;
  double hit_position;
  
  MM_plane_calib* calib;

  // cluster histograms
  TH1F *fHcHits               ; // multiplicity
  TH1F *fHcPos                ; // position
  TH1F *fHcAmp                ; // amplitudes
  TH1F *fHcSize               ; // size
  TProfile *fHcMeanAmp        ; // mean cluster amplitude
  
  // best cluster histograms
  TH1F* fHbcPos                ; // position
  TH1F* fHbcAmp                ; // amplitudes
  TH1F* fHbcSize               ; // size
  TProfile* fHbcMeanAmp        ; // mean cluster amplitude
  TH2F* fHbcAmpRatio;   // amplitude ratio

 public:
  
  double HasHit() const { return has_hit; }
  double GetHitPos() const { return hit_position; }
  const MM_plane_calib* GetCalib() const { return calib; }

  PlaneMM(const char *detname, int nchan);

  virtual void Reset();
  
#if !defined(__CINT__) && !defined(__CLING__)
  virtual void EndEvent(const CS::DaqEvent &event);
#endif 

  virtual void Init(TTree* tree = 0);
  
  virtual void Clusterize();

  ClassDef(PlaneMM,0)
};

#endif
