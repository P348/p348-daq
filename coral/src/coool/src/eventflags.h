#pragma once

struct EventFlags {
  // event type
  //int DddEventType;
  bool isPhysics;
  bool isCalibration;
  
  bool isOffSpill;
  bool isOnSpill;
  bool isAfterSpill;
  
  bool isLED0() const { return (isCalibration && isOffSpill); }
  bool isLED1() const { return (isCalibration && isOnSpill); }
  
  // trigger type
  bool isTriggerPhys;
  bool isTriggerBeam;
  bool isTriggerBeamOnly() const { return (isTriggerBeam && !isTriggerPhys && !isTriggerMuonInv && !isTriggerECAL); }
  bool isTriggerRand;
  bool isTriggerECAL;
  bool isTriggerMuonInv;
  bool isTriggerMuonInvOnly() const { return (isTriggerMuonInv && !isTriggerPhys && !isTriggerECAL); }

  bool isAny() const {
    return (isTriggerPhys || isTriggerBeam || isTriggerRand || isTriggerECAL || isTriggerMuonInv);
  }

};
