#include "PlaneStrawTubes.h"
#include "TThread.h"
#include "ChipNA64TDC.h"
#include "TriggerTime.h"
#include <string> 
#include "TString.h"
ClassImp(PlaneStrawTubes);

////////////////////////////////////////////////////////////////////////////////
//PlaneStrawTubes::PlaneStrawTubes(const char* detname) :
//GroupStraws             (const char* name);


	PlaneStrawTubes::PlaneStrawTubes(const char* detname, int nchan, int center, int width) :
	  Plane(detname)
{
	 fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT};
//	 fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT,CS::DaqEvent::CALIBRATION_EVENT};
	 
	 nBin=64;
	isBig=false;
	isSTTO=false; 
	 if (fName.find("V")!=std::string::npos)
	 {
		isBig=true;		
		nBin=384;
	}	 
	if (fName.find("U")!=std::string::npos)
	{
		isBig=true;		

		nBin=384;

	}
        if(fName.find("STT")!=std::string::npos)
	{
		nBin=64;
		isBig=false;
		isSTTO = true;		


	}
	if (nchan>65)
	{
		isBig=true;		

		nBin=384;

	}
 	bias=0;


}

double PlaneStrawTubes::rtStraw(int time)
{
//      return pitch*(double)time/60.0;
	if (time > 0)      return  1.0/11.23*(pow((double)time, 1.0/1.203));
		else  if (time < 0)    return  -1.0/11.23*(pow((double)(-time), 1.0/1.203));
		else return 0.0;
}


////////////////////////////////////////////////////////////////////////////////


void PlaneStrawTubes::Init(TTree* tree)
{


    fChannels = new TH1F_Ref((fName+"_hits").c_str(),(fName+" channels distribution;channel").c_str(),nBin,0,nBin,fRateCounter);
    AddHistogram(fChannels);

    fChannels_rt = new TH1F_Ref((fName+"_rt").c_str(),(fName+" profle with double hit;mm").c_str(),10*3*nBin,0,3*nBin,fRateCounter);
    AddHistogram(fChannels_rt);



    fmultiplicity = new TH1F_Ref((fName+"_multiplicity").c_str(),(fName+" multiplicity;multiplicity").c_str(),nBin,0,nBin,fRateCounter);
    AddHistogram(fmultiplicity);	

    fTime = new TH1F_Ref((fName+"_time_wide").c_str(),(fName+" drift time;time,ns").c_str(),700,-70000,70000,fRateCounter);
    AddHistogram(fTime);
    fTime1 = new TH1F_Ref((fName+"_time").c_str(),(fName+" drift time;time,ns").c_str(),2500,-500,2000,fRateCounter);
    AddHistogram(fTime1);
    fTime2 = new TH1F_Ref((fName+"_clusters").c_str(),(fName+" cluster size;size").c_str(),20,0,20,fRateCounter);
    fTime2->SetOption("HIST TEXT0");
    fTime2->SetNdivisions(20, "X");
    fTime2->GetXaxis()->CenterLabels();
    AddHistogram(fTime2);
    
    fTCSPhase = new    	TH1F_Ref((fName+"_TCSPhase").c_str(),(fName+" TCS Phase TDC tick").c_str(),4000,-2000,2000,fRateCounter); AddHistogram(fTCSPhase);

    
    fChannelDoubleClusters = new TH2F((fName+"_double_clusters").c_str(),(fName+" _double_clusters").c_str(),1000, 000, 2000,  1000, 000,2000);
    fChannelDoubleClusters->SetOption("CONT");
    AddHistogram(fChannelDoubleClusters);
     
    fTime33 = new TH1F_Ref((fName+"_time33").c_str(),(fName+" drift time channel 33").c_str(),2000,-100,2100,fRateCounter); AddHistogram(fTime33);
   	
    if((fName.find("X")!=std::string::npos || fName.find("Y")!=std::string::npos || fName.find("U")!=std::string::npos || fName.find("V")!=std::string::npos)&&!isSTTO)
    {
 	fTime29 = new TH1F_Ref((fName+"_time29").c_str(),(fName+" drift time channel 29").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime29);
 	fTime30 = new TH1F_Ref((fName+"_time30").c_str(),(fName+" drift time channel 30").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime30);
    }	
 
    if (isSTTO)
    {		

	fTime21 = new TH1F_Ref((fName+"_ECALTrigger").c_str(),(fName+" ECAL trigger").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime21);
	fTime20 = new TH1F_Ref((fName+"_MuonInvTrigger").c_str(),(fName+" Muon invisble Trigger").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime20);
	fTime19 = new TH1F_Ref((fName+"_ECALTrigger_afterPSL").c_str(),(fName+" ECAL Trigger_afterPSL").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime19);
	fTime18 = new TH1F_Ref((fName+"_MuonInvTrigger_afterPSL").c_str(),(fName+" Muon Invisible Trigger_afterPSL").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime18);


	fTime22 = new TH1F_Ref((fName+"_S2").c_str(),(fName+" S2").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime22);
	fTime23 = new TH1F_Ref((fName+"_Veto1").c_str(),(fName+" Veto1").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime23);
 	fTime24 = new TH1F_Ref((fName+"_S3").c_str(),(fName+" S3").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime24);

	fTime25 = new TH1F_Ref((fName+"_AcceptedTCS").c_str(),(fName+" AcceptedTCS").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime25);
 	fTime26 = new TH1F_Ref((fName+"_Beam").c_str(),(fName+" Beam").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime26);

	fTime27 = new TH1F_Ref((fName+"_Random").c_str(),(fName+" Random").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime27);
 	fTime28 = new TH1F_Ref((fName+"_Physical").c_str(),(fName+" Physical").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime28);
 	fTime29 = new TH1F_Ref((fName+"_Beam_afterPSL").c_str(),(fName+" Beam_afterPSL").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime29);
 	fTime30 = new TH1F_Ref((fName+"_Random_afterPSL").c_str(),(fName+" Random_afterPSL").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime30);

 	fTime31 = new TH1F_Ref((fName+"__Physical_afterPSL").c_str(),(fName+" Physical_afterPSL").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime31);
	fTime64 = new TH1F_Ref((fName+"_time64").c_str(),(fName+" drift time channel 64").c_str(),2000,-500,1500,fRateCounter); AddHistogram(fTime64);


        fmultiplicity26 = new TH1F_Ref((fName+"_multiplicity26").c_str(),(fName+" multiplicity26;multiplicity").c_str(),nBin,0,nBin,fRateCounter);
   	AddHistogram(fmultiplicity26);	

    	fmultiplicity27 = new TH1F_Ref((fName+"_multiplicity27").c_str(),(fName+" multiplicity27;multiplicity").c_str(),nBin,0,nBin,fRateCounter);
    	AddHistogram(fmultiplicity27);	

    	fmultiplicity28 = new TH1F_Ref((fName+"_multiplicity28").c_str(),(fName+" multiplicity28;multiplicity").c_str(),nBin,0,nBin,fRateCounter);
    	AddHistogram(fmultiplicity28);	

    	fmultiplicity64 = new TH1F_Ref((fName+"_multiplicity64").c_str(),(fName+" multiplicity64;multiplicity").c_str(),nBin,0,nBin,fRateCounter);
    	AddHistogram(fmultiplicity64);	

    }

    fSpillTime = new TH2F((fName+"_spilltime").c_str(),(fName+" - ST12 T0 time diff;  Spill; Time diff").c_str(),200, 000, 200,  2000, -2000, 2000); AddHistogram(fSpillTime);
    TProfile_Ref* p2 = new TProfile_Ref((fName+"_Mean").c_str(),(fName+ " Mean;spill;mean #pm stddev,mm").c_str(), 200, 0, 200);
    p2->SetReference(fReferenceDirectory);
    p2->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p2->SetMarkerStyle(kFullDotMedium);
    //p2->SetOption("P");
    histo_vs_spill = p2;
    AddHistogram(histo_vs_spill);


    end_processing = false;
}

////////////////////////////////////////////////////////////////////////////////



void PlaneStrawTubes::StoreDigit(CS::Chip::Digit* digit) {
  
  lDigits.push_back(digit);
}





void PlaneStrawTubes::EndEvent(const CS::DaqEvent &event)
{

  const CS::DaqEvent::Header  header = event.GetHeader();

  uint32_t  eventN = header.GetEventNumberInRun();

  uint32_t spillN = event.GetBurstNumber();

  if (end_processing) return;

  int multiplicity= 0;  
  int multiplicity26= 0;  
  int multiplicity27= 0;  
  int multiplicity28= 0;  
  int multiplicity64= 0;  
  int ttt = 0;
    
    list<tuple<int,int32>> listChannels;	
    int nnn =0;	
    for( list<CS::Chip::Digit*>::const_iterator it=GetDigits().begin(); it!=GetDigits().end(); it++ )
    {
        const CS::ChipNA64TDC::Digit *d=dynamic_cast<const CS::ChipNA64TDC::Digit *>(*it);
        if( d==NULL )
        {
            printf("PlaneStrawTubes::EndEvent(): bad digits was found!\n");
            continue;
        }

	int channel = d->GetWire();
	float t16= float(d->GetTime());
	
		
        multiplicity++;
	fChannels->Fill(channel);

        fTime->Fill(t16);
	fTime1->Fill(t16);
	if (channel == 33)
	{	
		fTime33->Fill(t16);
	}
   if(fName.find("X")!=std::string::npos || fName.find("Y")!=std::string::npos || fName.find("U")!=std::string::npos || fName.find("V")!=std::string::npos)
       {
		if (channel == 29)	fTime29->Fill(t16);
		if (channel == 30)	fTime30->Fill(t16);


       }	


	if (isSTTO)
	{
//		if (channel == 0)	fTime0->Fill(t16);

		if (channel == 18)	fTime18->Fill(t16);
		if (channel == 19)	fTime19->Fill(t16);
		if (channel == 20)	fTime20->Fill(t16);

		if (channel == 21)	fTime21->Fill(t16);



		if (channel == 22)	fTime22->Fill(t16);
		if (channel == 23)	fTime23->Fill(t16);
		if (channel == 24)	fTime24->Fill(t16);

		if (channel == 25)	fTime25->Fill(t16);
		if (channel == 26)	fTime26->Fill(t16);
		if (channel == 27)	fTime27->Fill(t16);
		if (channel == 28)	fTime28->Fill(t16);
		if (channel == 31)	fTime31->Fill(t16);
		if (channel == 64)	fTime64->Fill(t16);
		if (channel==26)
		{
			multiplicity26++;
		} else
			if (channel==27)
			{
				multiplicity27++;
			} else		
				if (channel==28)
				{
					multiplicity28++;
				} 	
				else
					if (channel==64)
					{
						multiplicity64++;
					} 	
	
		if (channel==64)
		{
			fTCSPhase->Fill(d->GetTCSPhase());

		}
	//if (channel == 60)	fTime60->Fill(t16);
	}
	if (channel == 64) fSpillTime->Fill(spillN, t16);

	listChannels.push_back(make_tuple(channel,d->GetTime()));
//	listChannels.push_back(make_tuple(d->GetWire()+bias,d->GetTime()));
    }

	

    listChannels.sort();



    int clusterSize =0;
    if (listChannels.size()>0)
	clusterSize=1;
    
    int oldChannel = -1;
    int16 old_old_Time=0;	
    int old_old_Channel = -1;	 
    int16 oldTime = 0;	
    int16 newTime = 0;
    int newChannel =  -1;	 	  	
    for (std::list<tuple<int,int32>>::iterator it=listChannels.begin(); it != listChannels.end(); ++it)
    {	 
        newChannel =  get<0>(*it);
 	newTime = get<1>(*it); 
       
	if (newChannel==oldChannel+1 )
	{
		clusterSize++;	

	}
	else 
	{
		if (oldChannel!=-1)        	
		{		
			fTime2->Fill(clusterSize);
			if (isSTTO)
			{
			/*	if (clusterSize==2 && oldChannel==4) fChannelCluster4->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==7) fChannelCluster7->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==41) fChannelCluster41->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==38) fChannelCluster38->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==39) fChannelCluster39->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==40) fChannelCluster40->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==32) fChannelCluster32->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==33) fChannelCluster33->Fill(oldTime,old_old_Time);
				if (clusterSize==2 && oldChannel==37) fChannelCluster37->Fill(oldTime,old_old_Time);
			*/
                        }
			if (clusterSize==2 )
			{
				double coord = 	(3*oldChannel+rtStraw(fTime1->GetMean()+30-old_old_Time) + 3*newChannel-rtStraw(fTime1->GetMean()+30-oldTime))/2;			
				fChannelDoubleClusters->Fill(oldTime,old_old_Time);
//				fChannelDoubleClusters->Fill(newTime,oldTime);
				fChannels_rt->Fill((3*oldChannel+rtStraw(fTime1->GetMean()+30-old_old_Time) + 3*newChannel-rtStraw(fTime1->GetMean()+30-oldTime))/2);
				histo_vs_spill->Fill(spillN,coord);
				

			}
			clusterSize=1;
		}
	}
	old_old_Channel = oldChannel;

	oldChannel = newChannel;
	old_old_Time = oldTime;
	oldTime = newTime;
    }
    fTime2->Fill(clusterSize);
    if (clusterSize==2 )
    {
		 double coord=(3*old_old_Channel+rtStraw(fTime1->GetMean()+30-old_old_Time) + 3*oldChannel-rtStraw(fTime1->GetMean()+30-oldTime))/2;
 		 fChannelDoubleClusters->Fill(oldTime,old_old_Time);	
		 fChannels_rt->Fill((3*old_old_Channel+rtStraw(fTime1->GetMean()+30-old_old_Time) + 3*oldChannel-rtStraw(fTime1->GetMean()+30-oldTime))/2);
   		 histo_vs_spill->Fill(spillN,coord);
		 


    }	
/*    if (!isBig)
    {		
	    if (clusterSize==2 && oldChannel==4) fChannelCluster4->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==7) fChannelCluster7->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==41) fChannelCluster41->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==38) fChannelCluster38->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==39) fChannelCluster39->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==40) fChannelCluster40->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==32) fChannelCluster32->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==33) fChannelCluster33->Fill(oldTime,old_old_Time);
	    if (clusterSize==2 && oldChannel==37) fChannelCluster37->Fill(oldTime,old_old_Time);
    }	
*/
    //if (clusterSize==2 ) fChannelDoubleClusters->Fill(oldTime,old_old_Time);	
    //fTime2->SetTitle(fName+"_clusters 0: "+ 	
	
   	
  if (needRateHistoUpdate()) {
    const float_t bin0 = fTime2->GetBinContent(1);
    const float_t bin1 = fTime2->GetBinContent(2);
    const float_t bin2 = fTime2->GetBinContent(3);
    
    const double eff = bin2 ? 1.0/(0.5*(bin1/bin2)+1) : 0.;
    const TString eff_str = Form("%s cluster size eff? (size=2) = %.3f", fName.c_str(), eff);
    fTime2->SetTitle(eff_str);
  }

	
    fmultiplicity->Fill(multiplicity);
    if (isSTTO)
    {		
	    fmultiplicity26->Fill(multiplicity26);
	    fmultiplicity27->Fill(multiplicity27);
	    fmultiplicity28->Fill(multiplicity28);
	    fmultiplicity64->Fill(multiplicity64);
    }	
 

}

////////////////////////////////////////////////////////////////////////////////

