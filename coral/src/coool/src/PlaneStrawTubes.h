#ifndef __PlaneStrawTubes__
#define __PlaneStrawTubes__

#include "Plane.h"

#include "TTree.h"

/*! \brief Plane for Straw Tubes detectors
    \todo solve the problem in Monitor
    \author Alexander Zvyagin and VV and PV
*/

class PlaneStrawTubes : public  Plane
{
  public:

    PlaneStrawTubes         (const char* detname, int nchan, int center, int width);
    void                Init                    (TTree* tree = 0);

    double rtStraw(int time); 

////PlaneStrawTubes         (const char* detname);
////void                Init                    (void);


    TH1F*               fChannels = nullptr;              // hits profile
    TH1F*               fChannels_rt = nullptr;              // hits profile
    TH1F*               fmultiplicity = nullptr;              // hits profile
    TH1F*               fmultiplicity26 = nullptr;              // hits profile
    TH1F*               fmultiplicity27 = nullptr;              // hits profile
    TH1F*               fmultiplicity28 = nullptr;              // hits profile
    TH1F*               fmultiplicity64 = nullptr;              // hits profile
    TH1F*               fRates = nullptr;                 // rates profile
    TH1F*               fHits = nullptr;                  // hits multiplicity
    TH1F*               fTime = nullptr;                  //
    TH1F*               fTime1 = nullptr;                  //
    TH1F*               fTime2 = nullptr;                  //
    TH1F*               fTime6mm = nullptr;               //
    TH1F*               fTime10mm = nullptr;              //
      TH1F*               fTime25 = nullptr;              //

    TH1F*               fTime18 = nullptr;              //
    TH1F*               fTime19 = nullptr;              //
    TH1F*               fTime20 = nullptr;              //
    TH1F*               fTime21 = nullptr;              //
    TH1F*               fTime22 = nullptr;              //
    TH1F*               fTime23 = nullptr;              //
    TH1F*               fTime24 = nullptr;              //
    TH1F*               fTime26 = nullptr;              //
    TH1F*               fTime27 = nullptr;              //
    TH1F*               fTime28 = nullptr;              //
    TH1F*               fTime29 = nullptr;              //
    TH1F*               fTime30 = nullptr;              //
    TH1F*               fTime31 = nullptr;              //
    TH1F*               fTime33 = nullptr;              //

    //TH1F*               fTime41;              //
    TH1F*               fTime64 = nullptr;              //
    TH1F*               fTCSPhase = nullptr;             //

    
    TH2F* 		fChannelDoubleClusters = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster4 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster7 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster41 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster38 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster39 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster40 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster32 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster33 = nullptr;	        //channel Cluster
    TH2F* 		fChannelCluster37 = nullptr;	        //channel Cluster
    TH2F* 		fSpillTime = nullptr;	        //channel Cluster
    TH2F* 		fSpillMean = nullptr;	        //channel Cluster
    int 		bias;
    int                 nBin;      
    bool		isBig;	
    bool		isSTTO;	
    TProfile* histo_vs_spill;

//    uint32_t event_number;
    bool end_processing;
  private:
   
    #if !defined(__CINT__) && !defined(__CLING__)
    void                StoreDigit(CS::Chip::Digit* digit);
  	
    void                EndEvent                (const CS::DaqEvent &event);
    #endif

  ClassDef(PlaneStrawTubes,2)
};

#endif

