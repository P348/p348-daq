#ifndef __PlaneCalo__
#define __PlaneCalo__

#include "config.h"
#include "Plane.h"

#include "TTree.h"
#include "TProfile.h"
#include "TProfile2D.h"

#include <deque>
#include <map>
#include <utility>
#include "ChipSADC.h"
#include "ChipNA64WaveBoard.h"

/// Plane class for calorimeters

struct CaloHit {
  int x;  // X for each digit
  int y;  // Y for each digit
  int channel;
  
  double amp;  // amplitude for each digit
  double time;
  double ped0;
  double ped1;
  
  bool isWB;

  double ped(const int i) const {
    return (i % 2 == 0) ? ped0 : ped1;
  }
  double pulse(const int i) const {
    double A = (*raw)[i] - ped(i);
    if (isWB) A*=-1; //WB does not invert signal polarity at input
    return A >= 0. ? A : 0.;
  }
  
  const std::vector<uint16>* raw;
};

struct HistoGroup {
  TString name;
  TH1F*       hSumamp;
  TProfile2D* fH_meanamp_xy;
  TProfile*   fH_meanamp_ch;
  TH2F*       fH_amp_channel;
  TH1F*       fCH_amp[50][50];
  TH2F*       fCH_rawshape[50][50];
  TProfile*   fCH_vs_spill[50][50];
  TProfile*   fCH_vs_event[50][50];
};

//Struct to hold histogram parameters
struct histoParams{
  int maxSample; //number of samples for time histograms
  int maxADC;    //max adc for A-vs-T
  int maxAmpl;   //max value for 1D histograms
  int binsADC_RawShape;
  int binsADC_Ampl;
  int binsSample;
};

class PlaneCalo : public  Plane {
private:
  
  int fNy;  // number of rows
  int fNx;  // number of columns
  int fNch; // number of channels
  
  vector<CaloHit> hits;
  
  // variables
  Variable *fVy, *fVx, *fVamp, *fVtime;
  
  // markers for histograms
  TH1F* hHits;
  TH1F* hHits_nc;
  TProfile2D* fH_meanamp_xy; // amplitude vs XY
  TProfile* fH_meanamp_ch;
  TH2F *fHrc1;
  TH2F* fH_amp_channel;
  TH1F *fHa;
  
  TH1F* hSumamp;
  TH1F* hMaxamp;
  TH1F* hSumamp_nc;
  TH1F* hMaxamp_nc;
  TH1F* fHch;
  //TH1F *fBad, *fGood;
  TH2F *fHxyn;
  
  TProfile *fCH_prof[50][50];
  TH2F *fCH_rawshape[50][50];
  TH1F *fCH_amp[50][50];
  TH1F *fCH_time[50][50];
  TProfile* fCH_vs_spill[50][50];
  TProfile* fCH_vs_event[50][50];
  
  
  HistoGroup led;  // event=calibration, led any
  HistoGroup led0; // event=calibration, led off-spill
  HistoGroup led1; // event=calibration, led on-spill
                   // event=physics, trigger = any   - all non-HistoGroup histograms
  HistoGroup phys; // event=physics, trigger = physics
  HistoGroup beam; // event=physics, trigger = beam
  HistoGroup rnd;  // event=physics, trigger = random
  HistoGroup muinv; // event=physics, trigger = mu to invisible
  HistoGroup ecal; // event=physics, trigger = ecal missing energy
  
  unsigned int lastspill;
  
  TH1F *fTime;
  TProfile2D* fTime_xy;
  TProfile* fTime_ch;
  
  TH2I* fAmpTime;
  
  TProfile2D* fPed_xy;
  TProfile* fmeanPed_ch;
  TH2F *fPed_ch; 
  TH1F *fPed_sum;
  
  TH2I* fHitPos;
  TH2I* fHitPosLast;
  static const unsigned int LASTSIZE = 2000;
  std::deque<int> lastbuf;
  TProfile* fHitPosX_vs_spill;
  TProfile* fHitPosY_vs_spill;
  
  void SetCenterLabels(TH2* h);
  
  void FillPhysicsHistograms(uint32 spill,uint32 spill_event);
  void FillGroupHistograms(HistoGroup& grp, uint32 spill,uint32 spill_event);
  void BookGroupHistograms(HistoGroup& grp, TString name);
  
  CaloHit RecoWaveformSADC(const CS::ChipSADC::Digit* sadc) const;
  CaloHit RecoWaveformWB(const CS::ChipNA64WaveBoard::Digit* wb) const;

public:
  
  vector<float> GetLastEvent() const;
  
  float GetVampMin() const { return fVamp->GetMin(); }
  int GetNch() const { return fNch; }
  
  PlaneCalo(const char* name, int nx, int ny,map<pair<int,int>,bool> isWB,int minamp = 0, int maxamp = 16384); //modified by A.C. for WB max amp
  
  void Init(TTree* tree = 0);

  virtual void ControlPanel(const TGWindow *p, const TGWindow *main);
  
  void Reset();
  void ResetHistograms();
  
#if !defined(__CINT__) && !defined(__CLING__)
  CaloHit RecoWaveform(CS::Chip::Digit* digit) const;
  void StoreDigit(CS::Chip::Digit* digit);
  void EndEvent(const CS::DaqEvent &event, const EventFlags& flags);
#endif


  map<pair<int,int>,histoParams> histosParams;


  ClassDef(PlaneCalo,0)
};

#endif
