// last modification: 30.06.17

#ifndef __PlaneECAL0__
#define __PlaneECAL0__

#include "Plane2V.h"
#include <TTree.h>
#include <TProfile.h>
#include <TLatex.h> 
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>     
        
/// Plane class for ECAL0 ////////////////////////////////////////////////

#define ECAL0_MAP_FILE        "/online/detector/calo/ecal0/maps/ECAL0.map"
#define ECAL0_MOD             194
#define ECAL0_CH              9
#define ECAL0_XCELLS          51
#define ECAL0_YCELLS          51
#define ECAL0_PED_INTERVAL    5		// 5 samples
#define ECAL0_SAMPLE_TIME     12.5	// time per one sample in ns
#define ECAL0_SAMPLES_NUMBER  32
#define ECAL0_THR_00          8
#define ECAL0_MAX_AMPLITUDE   1024
#define ECAL0_MAX_HIT         100

//////////////////////////////////////////////////////////////////////////

class PlaneECAL0 : public  Plane2V 
{   
 private:  
  static const int fAmpCut;
  double ECAL0_Led[51][51];
  
  float fAmpSum_ecal0[ECAL0_XCELLS][ECAL0_YCELLS];  
  int   iHitSum_thr1_ecal0[ECAL0_XCELLS][ECAL0_YCELLS]; 
  int   iHitSum_thr2_ecal0[ECAL0_XCELLS][ECAL0_YCELLS];
  int   iAllHitSum_ecal0[ECAL0_XCELLS][ECAL0_YCELLS]; 
  
  int   iModArr[ECAL0_XCELLS][ECAL0_YCELLS];
  int   iChArr[ECAL0_XCELLS][ECAL0_YCELLS];
  
  int   iAbsChArr[ECAL0_MOD][ECAL0_CH];

  // PHYSICS
  TH1F/*_Ref*/ *HistoPhysAbsCh;  
  TH1F *HistoPhysSum;
  TH2F *HistoHitMapThr;  
  
  // LED
  TH1F/*_Ref */*HistoLedAmpAbsCh;
  TH1F_Ref *HistoLedAbsCh;
  TH1F/*_Ref*/ *HistoLedAmpAbsChSrcID[4];
  TH1F_Ref *HistoLedAbsChSrcID[4];
  TH1F_Ref *HistoLedTimeSum; 
  TH1F_Ref *HistoLedTimeSumSam; 
  TH1F_Ref *HistoLedSum;
  TH2F *HistoLedMap;
  TH2F *HistoLedTimeMap;
  TH2F *HistoLedTimeAbsCh;
  TH2F *HistoLedMapText;
  TH2F *HistoLedTimeMapText;
  TH2F *HistoLedSampleSum;
  
  // PEDESTALS
  TH2F *HistoPedAbsCh; 
  TH2F *HistoPedRmsAbsCh;
  TH1F *HistoPedSum;  
  TH1F *HistoPedRmsSum;
  TH2F *HistoPedMap;  
  TH2F *HistoPedRmsMap_01;  
  TH2F *HistoPedRmsMap_02;
  
  
  TH1F *fHch, *fHma, *fBad, *fGood;
  TH1F *fHit_all, *fSum_amp_hit;
  TH2F *fHchac, *fHxyac, *fHxyn;
  
  #if !defined(__CINT__) && !defined(__CLING__)
  TH1F *fCH_amp[51][51];
  TH1F *fCH_ped[51][51];	// DOBAVIL fCH_AMP  	
  TH1F *fCH_time[51][51];     
  // TProfile *fSProfile[51][51];
  TH2F *fSProfile[51][51];
  #endif

  // TH1F *fSum_time;
   
  // TH2F *fHrefled_low,*fHrefled_hig;  
  TH2F *fHchampled;
  TProfile *fProf;
   
 public:  
  PlaneECAL0(const char *detname,int nrow, int ncol, int center, int width):
  Plane2V(detname,nrow, ncol, center, width) {}

  ~PlaneECAL0() {}

  void Init(TTree* tree =0);  
  bool GetMap(const char *MapFileName);
  void StoreDigit(int col, int row,  std::vector<float>& data);

  #if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event);
  void StoreDigit(CS::Chip::Digit* digit);
  #endif

  ClassDef(PlaneECAL0,0)
};
#endif
