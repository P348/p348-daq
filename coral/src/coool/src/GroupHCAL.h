#ifndef __GroupHCAL__
#define __GroupHCAL__

#include "Group.h"
#include "PlaneCalo.h"

class GroupHCAL : public Group {

private:

  const PlaneCalo* hcal[4];

  TH2F* histo0vs1;
  TH1F* histo01;
  TH1F* histo012;
  TH1F* histo0123;
  
public:
  GroupHCAL(const char* name);

  void Init();

#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event);
#endif

  //ClassDef(GroupHCAL,0)
};

#endif
