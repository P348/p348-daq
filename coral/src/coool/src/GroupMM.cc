#include "GroupMM.h"

#include "TThread.h"
#include "conddb.h"

//ClassImp(GroupMM);

GroupMM::GroupMM(const char* name)
: Group(name),
  planeX(0), planeY(0),
  histoXY(0), histoXY_phys(0), histoXY_beam(0), histoXY_muTrans(0), histoXY_LED(0), histoXY_rand(0),
  dummyXY(0),histoX(0), histoY(0),
  histoEff_vs_spill(0), histoEff_vs_spill_phys(0), histoEff_vs_spill_beam(0), histoEff_vs_spill_muTrans(0),
  geom(0)
{
 fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT};
}

void GroupMM::NewRun(int runnumber)
{
  const string mm = fName;
  const int idx = cast<int>(mm.substr(2, 2));
  
  switch(idx)
  {
    case  1: geom = &MM1_pos; break;
    case  2: geom = &MM2_pos; break;
    case  3: geom = &MM3_pos; break;
    case  4: geom = &MM4_pos; break;
    case  5: geom = &MM5_pos; break;
    case  6: geom = &MM6_pos; break;
    case  7: geom = &MM7_pos; break;
    case  8: geom = &MM8_pos; break;
    case  9: geom = &MM9_pos; break;
    case 10: geom = &MM10_pos; break;
    case 11: geom = &MM11_pos; break;
    case 12: geom = &MM12_pos; break;
    case 13: geom = &MM13_pos; break;
    default:
      cerr << "ERROR: GroupMM::GroupMM() No DetGeo found for MM name=" << mm << " idx=" << idx << endl;
      geom = 0;
      break;
  }

  if (geom) {
    const double angle = geom->Angle;
    const double Kx = cos(angle);
    const double Ky = sin(angle);

    cout << "GroupMM() name = " << mm
      << ", idx = " << idx
      << ", angle = " << angle
      << ", Kx = " << Kx
      << ", Ky = " << Ky
      << endl;
  }
}

void GroupMM::Init()
{
  const size_t n = fPlanes.size();
  if (n != 2) {
    cout << "GroupMM::Init(): name = " << GetName() << ", #planes = " << n << endl;
  }

  // search X and Y planes of MM
  for (size_t i = 0; i < n; ++i) {
    const PlaneMM* p = dynamic_cast<const PlaneMM*>(fPlanes[i]);
    if (!p) continue;
    
    const TString name = p->GetName();
    if (name.Contains("X")) planeX = p;
    if (name.Contains("Y")) planeY = p;
  }

  // load reference data
  OpenReference();
  
  if (planeX && planeY) {

    // MMs dimensions: either 55 (smallMM) or 135 (largeMM)
    const double xlimits = planeX->GetCalib()->MM_Nstrips*planeX->GetCalib()->MM_strip_step/2.0 + 15.;
    const double ylimits = planeY->GetCalib()->MM_Nstrips*planeY->GetCalib()->MM_strip_step/2.0 + 15.;
    const int xlen = 10*xlimits;
    const int ylen = 10*ylimits;

    TString name;
    name = planeX->GetName() + TString("Y");
    histoXY = new TH2F(name, name+";X,mm;Y,mm", xlen, -xlimits, xlimits, ylen, -ylimits, ylimits);
    histoXY->SetOption("BOX COLZ");
    AddHistogram(histoXY);
    histoXY_phys = new TH2F(name+"_phys", name+" phys trigger;X,mm;Y,mm", xlen, -xlimits, xlimits, ylen, -ylimits, ylimits);
    histoXY_phys->SetOption("BOX COLZ");
    AddHistogram(histoXY_phys);
    histoXY_beam = new TH2F(name+"_beam", name+" beam trigger only;X,mm;Y,mm", xlen, -xlimits, xlimits, ylen, -ylimits, ylimits);
    histoXY_beam->SetOption("BOX COLZ");
    AddHistogram(histoXY_beam);
    histoXY_muTrans = new TH2F(name+"_muTrans", name+" muTrans trigger only;X,mm;Y,mm", xlen, -xlimits, xlimits, ylen, -ylimits, ylimits);
    histoXY_muTrans->SetOption("BOX COLZ");
    AddHistogram(histoXY_muTrans);
    histoXY_rand = new TH2F(name+"_rand", name+" random trigger only;X,mm;Y,mm", xlen, -xlimits, xlimits, ylen, -ylimits, ylimits);
    histoXY_rand->SetOption("BOX COLZ");
    AddHistogram(histoXY_rand);
    histoXY_LED = new TH2F(name+"_LED", name+" all LED triggers;X,mm;Y,mm", xlen, -xlimits, xlimits, ylen, -ylimits, ylimits);
    histoXY_LED->SetOption("BOX COLZ");
    AddHistogram(histoXY_LED);

    // projections plots
    name = planeX->GetName() + TString("proj");
    TH1F_Ref* h1 = new TH1F_Ref(name, name+";X,mm", xlen/2, -xlimits, xlimits, fRateCounter);
    h1->SetReference(fReferenceDirectory);
    histoX = h1;
    AddHistogram(histoX);

    name = planeY->GetName() + TString("proj");
    TH1F_Ref* h2 = new TH1F_Ref(name, name+";Y,mm", ylen/2, -ylimits, ylimits, fRateCounter);
    h2->SetReference(fReferenceDirectory);
    histoY = h2;
    AddHistogram(histoY);

    // vs. spill plots
    name = fName + TString("_X_vs_spill");
    TProfile_Ref* p1 = new TProfile_Ref(name, name+";spill;X mean #pm stddev,mm", 200, 0, 200);
    p1->SetReference(fReferenceDirectory);
    p1->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p1->SetMarkerStyle(kFullDotMedium);
    //p1->SetOption("P");
    histoX_vs_spill = p1;
    AddHistogram(histoX_vs_spill);

    name = fName + TString("_Y_vs_spill");
    TProfile_Ref* p2 = new TProfile_Ref(name, name+";spill;Y mean #pm stddev,mm", 200, 0, 200);
    p2->SetReference(fReferenceDirectory);
    p2->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p2->SetMarkerStyle(kFullDotMedium);
    //p2->SetOption("P");
    histoY_vs_spill = p2;
    AddHistogram(histoY_vs_spill);

    name = fName + TString("_Eff_vs_spill");
    TProfile_Ref* p3 = new TProfile_Ref(name, name+";spill;efficiency", 200, 0, 200);
    p3->SetReference(fReferenceDirectory);
    p3->SetMaximum(1.1);
    p3->SetMarkerStyle(kFullDotMedium);
    p3->SetOption("P");
    histoEff_vs_spill = p3;
    AddHistogram(histoEff_vs_spill);

    name = fName + TString("_Eff_vs_spill_phys");
    TProfile_Ref* p4 = new TProfile_Ref(name, name+";spill;efficiency", 200, 0, 200);
    p4->SetReference(fReferenceDirectory);
    p4->SetMaximum(1.1);
    p4->SetMarkerStyle(kFullDotMedium);
    p4->SetOption("P");
    histoEff_vs_spill_phys = p4;
    AddHistogram(histoEff_vs_spill_phys);

    name = fName + TString("_Eff_vs_spill_beam");
    TProfile_Ref* p5 = new TProfile_Ref(name, name+";spill;efficiency", 200, 0, 200);
    p5->SetReference(fReferenceDirectory);
    p5->SetMaximum(1.1);
    p5->SetMarkerStyle(kFullDotMedium);
    p5->SetOption("P");
    histoEff_vs_spill_beam = p5;
    AddHistogram(histoEff_vs_spill_beam);

    name = fName + TString("_Eff_vs_spill_muTrans");
    TProfile_Ref* p6 = new TProfile_Ref(name, name+";spill;efficiency", 200, 0, 200);
    p6->SetReference(fReferenceDirectory);
    p6->SetMaximum(1.1);
    p6->SetMarkerStyle(kFullDotMedium);
    p6->SetOption("P");
    histoEff_vs_spill_muTrans = p5;
    AddHistogram(histoEff_vs_spill_muTrans);

  }
}

void GroupMM::EndEvent(const CS::DaqEvent &event, const EventFlags& flags)
{
  if (thr_flag) TThread::Lock();
  
  //cout << "GroupMM::EndEvent() name = " << GetName() << endl;
  
  const bool hasXhit = planeX ? planeX->HasHit() : false;
  const bool hasYhit = planeY ? planeY->HasHit() : false;
  const bool hasHit = hasXhit && hasYhit;
  const int spill = event.GetBurstNumber();
  
  if (histoEff_vs_spill) histoEff_vs_spill->Fill(spill, hasHit);
  if (flags.isTriggerPhys && histoEff_vs_spill_phys) histoEff_vs_spill_phys->Fill(spill, hasHit);
  if (flags.isTriggerBeamOnly() && histoEff_vs_spill_beam) histoEff_vs_spill_beam->Fill(spill, hasHit);
  if (flags.isTriggerMuonInvOnly() && histoEff_vs_spill_muTrans) histoEff_vs_spill_muTrans->Fill(spill, hasHit);
  
  if (geom && hasHit) {

    // translate into center and scale to mm units
    const double xoffset = planeX->GetCalib()->MM_Nstrips/2.0 - 0.5;
    const double yoffset = planeY->GetCalib()->MM_Nstrips/2.0 - 0.5;
    const double hitx = (planeX->GetHitPos() - xoffset) * planeX->GetCalib()->MM_strip_step;
    const double hity = (planeY->GetHitPos() - yoffset) * planeY->GetCalib()->MM_strip_step;

    // Rotate planes
    const double angle = geom->Angle;
    const double Kx = cos(angle);
    const double Ky = sin(angle);

    // +x points towards Jura, +y points upwards, +z points along the beam axis
    double x = +Kx*hitx -Ky*hity;
    const double y = +Ky*hitx +Kx*hity;
    if(geom->invertAxis) x = -x;

    // Fill histograms
    histoXY->Fill(x, y);
    if (flags.isTriggerPhys) histoXY_phys->Fill(x, y);
    if (flags.isTriggerBeamOnly()) histoXY_beam->Fill(x, y);
    if (flags.isTriggerMuonInvOnly()) histoXY_muTrans->Fill(x, y);
    if (flags.isTriggerRand) histoXY_rand->Fill(x, y);
    if (flags.isLED0() || flags.isLED1()) histoXY_LED->Fill(x, y);
    histoX->Fill(x);
    histoY->Fill(y);
    
    histoX_vs_spill->Fill(spill, x);
    histoY_vs_spill->Fill(spill, y);
  }
  
  if (needRateHistoUpdate()) {
  if(histoXY)
    {
      const double eff = histoXY->Integral() / (fRateCounter + 1);
      const TString eff_str = Form("%s eff_{total} (Nhit > 0) = %.3f", fName.c_str(), eff);
      histoXY->SetTitle(eff_str);
    }
  if(histoXY_phys)
    {
      const double eff_phys = histoXY_phys->Integral() / (fRateCounterPhysicsOnly + 1);
      const TString eff_str = Form("%s eff_{phys} (Nhit > 0) = %.3f", fName.c_str(), eff_phys);
      histoXY_phys->SetTitle(eff_str);
    }
  if(histoXY_beam)
    {
      const double eff_beam = histoXY_beam->Integral() / (fRateCounterBeamOnly + 1);
      const TString eff_str = Form("%s eff_{beam} (Nhit > 0) = %.3f", fName.c_str(), eff_beam);
      histoXY_beam->SetTitle(eff_str);
    }
  if(histoXY_muTrans)
    {
      const double eff_muTrans = histoXY_muTrans->Integral() / (fRateCounterMuonInvOnly + 1);
      const TString eff_str = Form("%s eff_{muTrans} (Nhit > 0) = %.3f", fName.c_str(), eff_muTrans);
      histoXY_muTrans->SetTitle(eff_str);
    }
  if(histoXY_rand)
    {
      const double eff_rand = histoXY_rand->Integral() / (fRateCounterRand + 1);
      const TString eff_str = Form("%s eff_{rand} (Nhit > 0) = %.3f", fName.c_str(), eff_rand);
      histoXY_rand->SetTitle(eff_str);
    }
  if(histoXY_LED)
    {
      const double eff_led = histoXY_LED->Integral() / (fRateCounterLED + 1);
      const TString eff_str = Form("%s eff_{LED} (Nhit > 0) = %.3f", fName.c_str(), eff_led);
      histoXY_LED->SetTitle(eff_str);
    }
  }
  
  if (thr_flag) TThread::UnLock();
}
