#ifndef __Group__
#define __Group__

#include <string>
#include <vector>
#include "Plane.h"
#include "Reference.h"

#if !defined(__CINT__) && !defined(__CLING__)
#include "DaqEvent.h"
#endif

#include "eventflags.h"

class GroupPanel;

class Group {

 private:

  /// default reference directory is a fairly good run
  static std::string defaultRefFileName;

  /// forced reference file is given by user
  static std::string forcedRefFileName;
  
  /// working environment string which tags the environment where Coool should run
  static std::string wkEnvStr;

 protected:

  /// where reference histos can be found
  TDirectory *fReferenceDirectory;

  /// Group name
  std::string fName;

  /// Group type 
  std::string fType;

  /// vector of Plane's belonging to the group
  std::vector<const Plane*> fPlanes;

  /// vector of histograms declared in this group
  std::vector<TH1*> fHistList;

  /// Associated GroupPanel
  GroupPanel *fControlPanel;

  /// Number of events seen by Monitor
  int fRateCounter;
  int fRateCounterPhysics;
  int fRateCounterCalibration;
  int fRateCounterPhysicsOnly;
  int fRateCounterBeamOnly;
  int fRateCounterMuonInvOnly;
  int fRateCounterLED;
  int fRateCounterRand;

  // Period in event numbers of rate histo update
  static const int fRATE_UPDATE;
  
  bool needRateHistoUpdate() const { return (fRateCounter % fRATE_UPDATE) == 0; }
  bool needRateHistoUpdate(const EventFlags& flags) const { return flags.isAfterSpill; }
  
  /// if true, activate expert histograms
  static bool fExpertHistos;

  /// do we do tracking ? needed for initializations
  static bool fDoTracking;

#if USE_DATABASE == 1
  /// pointer to database
  MySQLDB* fDataBase;

  /// returns name of the reference file from the database
  const char* GetRefDirNameDB();
#endif

  /// give a directory pointer to the default reference file
  TDirectory *GetDefaultRefDir() {return fReferenceDirectory;}

  /// opens root reference file
  virtual void OpenReference();

  /// returns name of the reference file
  const char* GetRefDirName();

  /// add one histogram to the histogram list
  void AddHistogram(TH1* hist);


 public:
  Group(const char* name):
    fReferenceDirectory(0), fName(name), fType("NOTYPE"), fControlPanel(0),
      fRateCounter(0),fRateCounterPhysics(0),fRateCounterCalibration(0), fRateCounterPhysicsOnly(0), fRateCounterBeamOnly(0), fRateCounterMuonInvOnly(0), fRateCounterLED(0), fRateCounterRand(0) {}
  virtual ~Group();

  /// the list of supported/accepted event types
  /// empty list means any event type
  std::set<int> fAcceptedEventTypes;

  bool IsAccepted(const int type) const
  {
    // empty list means any event type is supported
    if (fAcceptedEventTypes.empty()) return true;
    
    return (fAcceptedEventTypes.count(type) != 0);
  }

  /// must be called after group is complete
  virtual void Init() {
    fRateCounter = 0;
    fRateCounterPhysics = 0;
    fRateCounterCalibration = 0;
    fRateCounterPhysicsOnly = 0;
    fRateCounterBeamOnly = 0;
    fRateCounterMuonInvOnly = 0;
    fRateCounterLED = 0;
    fRateCounterRand = 0;
  }

  /// sets group type (yes, this should be in the constructors. yes I'm lazy)
  void SetType(const char* type) {fType = type;}

  /// resets all histograms
  virtual void ResetHistograms();

  /// resets all histograms if one plane was modified
  void ResetIfNeeded();

  /// called at new run
  virtual void NewRun(int /*runnumber*/) {}

  /// Add a plane to the list of contained Planes
  void Add(const Plane* plane);

  /// Fills the histograms
  virtual void EndEvent() {};    

#if !defined(__CINT__) && !defined(__CLING__)
  /// Fills the histograms
  virtual void EndEvent(const CS::DaqEvent &) {};
  virtual void EndEvent(const CS::DaqEvent &e, const EventFlags& flags) {EndEvent(e);};
#endif

  /// Do the tracking
  virtual void Tracking() {}

  /// \return vector of Plane's
  std::vector<const Plane*>& GetPlane() {return fPlanes;}

  /// \return vector of histograms
  std::vector<TH1*>& GetHistoList() {return fHistList;}

  /// \return name of the group
  const char* GetName() const {return fName.c_str();}

  /// \return type of the group
  const char* GetType() const;

  /// Creates the GroupPanel
  virtual void ControlPanel(const TGWindow *p, const TGWindow *main);

  /// called by the associated GroupPanel when it is closed
  void PanelClosed() {fControlPanel=0;}

  /// update fRateCounter (increase it by 1 basically...)
  void UpdateRateCounter(const int etype, const EventFlags& flags) {
    fRateCounter++;
    // event type constants: DDD/DaqEvent.h
    if (etype == 7) fRateCounterPhysics++;
    if (etype == 8) fRateCounterCalibration++;
    if (flags.isTriggerPhys) fRateCounterPhysicsOnly++;
    if (flags.isTriggerBeamOnly()) fRateCounterBeamOnly++;
    if (flags.isTriggerMuonInvOnly()) fRateCounterMuonInvOnly++;
    if (flags.isLED0() || flags.isLED1()) fRateCounterLED++;
    if (flags.isTriggerRand) fRateCounterRand++;
  }

  /// find an histo from the histo list
  TH1* GetHisto( const std::string hName );

#if USE_DATABASE == 1
  void setDBpt(MySQLDB* dbpt)  { fDataBase = dbpt; }
#endif

  /// gives a file name which will be used as reference instead of DB ones
  static void ForceRefFileName(const char* filename)
    { forcedRefFileName = filename; }

  /// set the working environment string
  static void setWkEnvStr(const char* wkEnvStrChr)
    { wkEnvStr = wkEnvStrChr; }

  /// set expert histo flag
  static void setExpertHistoFlag(bool experthisto)
    { fExpertHistos = experthisto; }

  /// set dotracking flag
  static void setDoTrackingFlag(bool state)
    { fDoTracking = state; }

  ClassDef(Group,0)

};

#endif




