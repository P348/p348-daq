#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TVirtualX.h>
#include <TError.h>

#include "MainFrame.h"


TROOT root("GUI", "GUI test environement");

char ApplicationName[32] = "COOOL main version";

bool thr_flag = false;
//bool thr_flag = true;


int main(int argc, char **argv)
{
  char PID[32];
  sprintf(PID,"pid: %u", getpid());
  setenv("DATE_SITE",PID,1);

  gStyle->SetOptStat("emriou");
  gStyle->SetLineScalePS(1.);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);
  gStyle->SetGridColor(kGray);
  // display bin content as integer value for "TEXT" draw option
  gStyle->SetPaintTextFormat(".0f");

  MainFrame *mainframe=0;
  try{
    char *root_argv[]={argv[0],NULL};
    int root_argc=1;
    gErrorIgnoreLevel = kError+1; // bump ignore level to suppress tons of the TCling::LoadPCM messages
    TApplication theApp("App",&root_argc, root_argv);
    gErrorIgnoreLevel = kUnset; // restore ignore level to default

    if (gROOT->IsBatch()) {
      fprintf(stderr, "%s: cannot run in batch mode\n", argv[0]);
      return 1;
    }

    
    mainframe=new MainFrame(gClient->GetRoot(),argc,argv, 400, 220);   

    theApp.Run();
  }   
  // Something is wrong...
  // Print error message and finish this data file.
      
  catch( const std::exception &e ) {
    std::cerr << "exception:\n" << e.what() << "\n";
  }
  catch( const char * s ) {
    std::cerr << "exception:\n" << s << "\n";
  }
  catch( ... ) {
    std::cerr << "Oops, unknown exception!\n";
  }
  
#ifndef __CINT__
  CS::Chip::PrintStatistics();
  CS::Exception::PrintStatistics();
#endif
  
  delete mainframe;
  exit(0);
}



