#include <cmath>

#include "PlaneHCAL2.h"
#include "TProfile.h"
#include "ChipSADC.h"

#define nSADC_Sampl 32
#define Dpedstart  0    
#define Dpedend    5
#define Dpulsstart 6
#define Dpulsend   31
#define Dcalpedstart 0
#define Dcalpedend  6
#define Dledstart   7
#define Dledend    31

const int SADCoverflow=1020;   // overflow for ADC channels
const int PlaneHCAL2::fMAX_MULT = 1;

ClassImp(PlaneHCAL2); \

PlaneHCAL2::PlaneHCAL2(const char *detname,int ncol, int nrow, int center, int width)
  : Plane(detname),Nrows(nrow),Ncols(ncol),fNchan(nrow*ncol) 
{

  Nchan=nb_HCAL2_SADCcnl;
  Nsamples=nSADC_Sampl;
  fNhits=0;
  pedstart=Dpedstart;       pedend=Dpedend;
  pulsstart=Dpulsstart;     pulsend=Dpulsend;
  calpedstart=Dcalpedstart; calpedend=Dcalpedend;
  ledstart=Dledstart;       ledend=Dledend ;

  SADCampcut = 20;
  SADCLEDcut = 10;
  SADCTimingcut = 10;

  hnSADChit=NULL, hnSADChitcut=NULL;
  hnhitXY=NULL, hnhitXYcut=NULL;
  hampXY=NULL,hampXYcut=NULL, hamp_vs_adr=NULL;
  hamp_vs_adr=NULL, hhit_vs_adr=NULL;
  hamp=NULL; hLedXY=NULL, hLedChnl=NULL, hledlen=NULL, hLedTm=NULL;
  hPhysTm_adr=NULL,hLedTm_adr=NULL, hTmvsTrig=NULL; 
  hoverflow=NULL; 
  hxynoise=NULL, hxynoise_amp=NULL, hrnd=NULL; 
  hledamp=NULL, hpedrmsXY=NULL;
  for(int i=0;i<Ncols; i++) {
   for(int j=0;j<Nrows; j++) { hsampl[i][j]=NULL; hled_sampl[i][j]=NULL;}
  }
}

void PlaneHCAL2::Init(TTree* tree) {
  std::string myname= "HC02_";
  std::string name,title;
  char cutamp[10],cutled[10];
  sprintf(cutamp,"%3.1i",SADCampcut);
  sprintf(cutled,"%3.1i",SADCLEDcut);

  name = myname + "SADC_hit number";
  hnSADChit=new TH1F_Ref(name.c_str(),name.c_str(),100,0,100, fRateCounter);
  ((TH1F_Ref*)hnSADChit)->SetReference(fReferenceDirectory);
  AddHistogram(hnSADChit);

  name = myname + "SADC_hits number(amp>thr)";
  title = myname + "SADC_hits#,amp>"+cutamp;
  hnSADChitcut=new TH1F_Ref(name.c_str(),title.c_str(), 50, 0, 50, fRateCounter);   
  ((TH1F_Ref*)hnSADChitcut)->SetReference(fReferenceDirectory);
  AddHistogram(hnSADChitcut);

  name = myname+"Nhits_vs_XY";
  title = fName+" Number of hits vs XY";
  hnhitXY=new TH2F(name.c_str(),title.c_str(),Ncols,0.,Ncols,Nrows,0.,Nrows);
  hnhitXY->SetOption("box");
  AddHistogram(hnhitXY);

  name = myname+"Nhits_vs_XY(amp>thr)";
  title = fName+" Number of hits vs XY, amp>"+cutamp;
  hnhitXYcut=new TH2F(name.c_str(),title.c_str(),Ncols,0.,Ncols,Nrows,0.,Nrows);
  hnhitXYcut->SetOption("box");
  AddHistogram(hnhitXYcut);

  name = myname + "Ampl_vs_XY";
  title = fName + "Amplitude vs XY";
  hampXY=new TH2F(name.c_str(),title.c_str(),Ncols,0.,Ncols,Nrows,0.,Nrows);
  hampXY->SetOption("col");
  AddHistogram(hampXY);

  name = myname + "Ampl_vs_XY(amp>thr)";
  title = myname + "Ampl. vs XY,amp>"+cutamp;
  hampXYcut = new TH2F(name.c_str(), title.c_str(),Ncols, 0, Ncols, Nrows, 0, Nrows);
  hampXYcut->SetOption("col");
  AddHistogram(hampXYcut);

  name = myname + "SumAmpl";
  title = fName + " SADC Sum Amplitude";
  hSADCsum=new TH1F_Ref(name.c_str(),title.c_str(),300,0.,3000., fRateCounter);
  ((TH1F_Ref*)hSADCsum)->SetReference(fReferenceDirectory);
  AddHistogram(hSADCsum);

  name = myname + "SumAmp(amp>thr)";
  title = fName + "SADC Sum Amplitude with amp>"+cutamp;
  hSADCcutsum=new TH1F_Ref(name.c_str(),title.c_str(), 300,0.,3000., fRateCounter);
  ((TH1F_Ref*)hSADCcutsum)->SetReference(fReferenceDirectory);
  AddHistogram(hSADCcutsum);

  name = myname + "SADC_amp";
  title = fName + "SADC amplitudes";
  hamp=new TH1F_Ref(name.c_str(),title.c_str(), 1025,0., 1025, fRateCounter);
  ((TH1F_Ref*)hamp)->SetReference(fReferenceDirectory);
  AddHistogram(hamp);

  name = myname + "Nhits_vs_chnl#";
  hhit_vs_adr=new TH1F_Ref(name.c_str(),name.c_str(), Nrows*Ncols, 0,
                     Nrows*Ncols, fRateCounter);
  ((TH1F_Ref*)hhit_vs_adr)->SetReference(fReferenceDirectory);
  AddHistogram(hhit_vs_adr);

  name = myname + "Ampl_vs_chnl#";
  hamp_vs_adr = new TH2F(name.c_str(), name.c_str(),
		Nrows*Ncols, 0, Nrows*Ncols,1024,0,1024);
  AddHistogram(hamp_vs_adr);

  name = myname + "Phys_ev_timing";
  title = fName + "SADC max.sample#,amp>"+cutamp;
  hPhysTm=new TH1F_Ref(name.c_str(),title.c_str(),32,0.,32., fRateCounter);
 ((TH1F_Ref*)hPhysTm)->SetReference(fReferenceDirectory);
  AddHistogram(hPhysTm);

  name = myname + "Phys_ev_timing vs adr";
  title = fName + "SADC max.sample# vs channel#,amp>"+cutamp;
  hPhysTm_adr=new TH2F(name.c_str(),title.c_str(),220,0,220,32,0.,32.);
  hPhysTm->SetOption("col");
  AddHistogram(hPhysTm_adr);

  name = myname + "Leds";
  title = fName + "Led amplitudes";
  hledamp=new TH1F(name.c_str(),title.c_str(),110,0.,1100.);
  AddHistogram(hledamp);
  
  name = myname + "Leds_vs_XY";
  title = fName + " Leds vs XY, led>"+cutled;
  hLedXY=new TH2F(name.c_str(),title.c_str(),Ncols,0.,Ncols,Nrows,0.,Nrows);
  hLedXY->SetOption("col");
  AddHistogram(hLedXY);

  name = myname + "Led_ev_length";
  title = fName + "Led event length";
  hledlen=new TH1F_Ref(name.c_str(),title.c_str(),250,0,250, fRateCounter);
  ((TH1F_Ref*)hledlen)->SetReference(fReferenceDirectory);
  AddHistogram(hledlen);

  name = myname + "SADC_Led_timing";
  title = fName + "SADC Led max.sample#,amp>"+cutled;
  hLedTm=new TH1F_Ref(name.c_str(),title.c_str(),32,0.,32., fRateCounter);
  ((TH1F_Ref*)hLedTm)->SetReference(fReferenceDirectory);
  AddHistogram(hLedTm);

  name = myname + "SADC_Led_timing vs chnl";
  title = fName + "SADC Led max. sample# vs channel#,amp>"+cutled;
  hLedTm_adr=new TH2F(name.c_str(),title.c_str(),220,0,220.,32,0.,32.);
  hLedTm->SetOption("col");
  AddHistogram(hLedTm_adr);

  name = myname + "Led_vs_chnl#";
  title = fName + "LED vs channel#";
  hLedChnl=new TH2F(name.c_str(),title.c_str(), 
                              Nrows*Ncols, 0., Nrows*Ncols,204,0.,1020.);
  AddHistogram(hLedChnl);
  
  name = myname + "Peds";
  title = fName + "SADC pedestals";
  hPed=new TH1F_Ref(name.c_str(),title.c_str(), 110, 0., 330., fRateCounter);
  ((TH1F_Ref*)hPed)->SetReference(fReferenceDirectory);
  AddHistogram(hPed);

  name = myname + "Peds_vs_XY";
  title = fName + "Peds vs XY";
  hPedXY=new TH2F(name.c_str(),title.c_str(),Ncols,0.,Ncols,Nrows,0.,Nrows);
  hPedXY->SetOption("col");
  AddHistogram(hPedXY);

  name = myname + "PedRMS";
  title = fName + " SADC pedestal RMS (counts)";
  hPedRMS=new TH1F_Ref(name.c_str(),title.c_str(), 100, 0., 20., fRateCounter);
  ((TH1F_Ref*)hPedRMS)->SetReference(fReferenceDirectory);
  AddHistogram(hPedRMS);

//
  if (fExpertHistos) {

    name = myname + "PedRMS vs XY";
    title = fName + " SADC pedestal RMS vs XY";
    hpedrmsXY=new TH2F(name.c_str(),title.c_str(), Ncols,0.,Ncols,Nrows,0.,Nrows);
    hpedrmsXY->SetOption("col");
    AddHistogram(hpedrmsXY);

    name = myname + "overflow";
    title = fName + "SADC overfloved channels";
    hoverflow=new TH2F(name.c_str(),title.c_str(),Ncols,0.,Ncols,Nrows,0.,Nrows);
    hoverflow->SetOption("col");
    AddHistogram(hoverflow);

    name = myname + "Random_ev_length";
    title = fName + "Random trigger event length";
    hrnd=new TH1F(name.c_str(),title.c_str(),50,0,50);
    AddHistogram(hrnd);

    name = myname + "Nhits_vs_XY_rnd_trig";
    title = fName + " hits vs XY, Random trigger";
    hxynoise = new TH2F(name.c_str(), title.c_str(),Ncols, 0, Ncols, Nrows, 0, Nrows);
    hxynoise->SetOption("box");
    AddHistogram(hxynoise);

    name = myname + "Ampl_vs_XY_rnd_trig";
    title = fName + " Aplitude vs XY, Random trigger";
    hxynoise_amp = new TH2F(name.c_str(), title.c_str(),Ncols, 0, Ncols, Nrows, 0, Nrows);
    hxynoise_amp->SetOption("col");
    AddHistogram(hxynoise_amp);

    name = myname + "TrigTime vs trigger";
    title = fName + " Trigger time vs trigger type";
    hTmvsTrig = new TH2F(name.c_str(), title.c_str(),32,0.,32.,12, 0, 12);
    hTmvsTrig->SetOption("col");
    AddHistogram(hTmvsTrig);

    for(int i=0; i<Ncols;i++){
       for(int j=0;j<Nrows;j++){
               char nmb[30];
               sprintf(nmb," %2.2i-%2.2i",i,j);
               name = myname + "Ev.sampl,chnl  "+nmb;
               title = fName + nmb+" SADC ampl vs sample#,amp>"+cutamp;
               hsampl[i][j] = new TH2F(name.c_str(), title.c_str(),Nsamples, 0, Nsamples, 1025, 0, 1025);;
               AddHistogram(hsampl[i][j]);

               name = myname + "Led sampl,chnl"+nmb;
               title = fName + nmb+" SADC Led ampl vs sample#,amp>"+cutled;
               hled_sampl[i][j] = new TH2F(name.c_str(), title.c_str(),Nsamples, 0, Nsamples, 1025, 0, 1025);;
               AddHistogram(hled_sampl[i][j]);
       }
    }
  }
}

void PlaneHCAL2::EndEvent(const CS::DaqEvent &event) {
  int evtype=event.GetType();
  if(!(evtype==7||evtype==8))  return;
  int trmsk=event.GetTrigger();   
  const CS::uint32 Atr = event.GetHeader().GetTypeAttributes()[0]&0xf8000000;
  if(evtype==8&&Atr!=0x68000000) return;           // select calorimeter calib.ev.

//  if(evtype==7&&trmsk&0x100) return;                          //remove GANDALF calibration trigger for CAMERA
//  if(evtype==7&&trmsk&0x1) return;                          //remove GANDALF calibration trigger for CAMERA

  int NbInRun = (int)event.GetEventNumberInRun();
  int rndtr=trmsk&0xc00;     // random trigger
//  if(fNhits>100&&evtype==7) cout<<"---"<<NbInRun<<"  "<<std::hex<<trmsk<<"  "<<std::dec<<evtype<<"  "<<fNhits<<endl;
//  if(fNhits>215&&evtype==8) cout<<"---"<<std::hex<<trmsk<<"  "<<std::dec<<evtype<<"  "<<fNhits<<endl;
  if(hrnd && evtype==7&&rndtr)  hrnd->Fill(fNhits);
  if(hledlen && evtype==8) { hledlen->Fill(fNhits);
			     if(hLedXY) hLedXY->Reset();
                             if(hPedXY) hPedXY->Reset();
  }			     
  if(fNhits<=0) return;


  if (thr_flag) MYLOCK();


  double sum=0., time=0.;
  double SADCsum=0., SADCcutsum=0.;
  int SADCcuthits=0; 
  for ( int i=0; i<fNhits; i++) {
    int col=fSADCCol[i];
    int row=fSADCRow[i];
    int adr=row + col*Nrows;
//     
    double RMS=0.,ped=0.,puls=0.;
    if(evtype==8) {                // Leds
         ped=SADCped(calpedstart,calpedend,&fSADCev[i][0],RMS);
         puls=SADCpuls(ledstart,ledend,&fSADCev[i][0], ped,sum,time);
         if(puls<0.) puls=0.;
         hPed->Fill(ped);
         hPedXY->Fill(col,row,ped);
         hPedRMS->Fill(RMS);
	 hledamp->Fill(puls);
         hLedChnl->Fill(adr,puls);
         if(puls>SADCLEDcut) { hLedXY->Fill(col,row,puls);
                               hLedTm->Fill(time); 
			       hLedTm_adr->Fill(adr,time);
	 }
         if (fExpertHistos) {
           hpedrmsXY->Fill(col,row,RMS);
           for(int j=0;j<Nsamples;j++){
               if(hled_sampl[col][row]) hled_sampl[col][row]->Fill(j,fSADCev[i][j]);
           }
         }
         continue;
    }

    ped=SADCped(pedstart, pedend, &fSADCev[i][0],RMS);
    puls=SADCpuls(pulsstart, pulsend,&fSADCev[i][0], ped,sum,time);
    if(puls<0.) puls=0.;
//
    if(evtype==7&&rndtr)  {
      if(fExpertHistos)  {
         hxynoise->Fill(col,row);
         hxynoise_amp->Fill(col,row,puls);
       }
       continue;
    }                                       //random trigger
    hhit_vs_adr->Fill(adr);
    hamp_vs_adr->Fill(adr,puls);
    hnhitXY->Fill(col,row);
    hampXY->Fill(col,row,puls);
    hamp->Fill(puls);
    SADCsum+=puls;
    if(puls>SADCampcut){
       SADCcuthits++;
       SADCcutsum+=puls;
       if (hnhitXYcut) hnhitXYcut->Fill(col,row);
       if (hampXYcut) hampXYcut->Fill(col,row,puls);
       if (hPhysTm) hPhysTm->Fill(time);
       if (hPhysTm_adr) hPhysTm_adr->Fill(adr,time);
    }
    if(fExpertHistos)  {
       for(int j=0;j<Nsamples;j++){
               if(hsampl[col][row]) hsampl[col][row]->Fill(j,fSADCev[i][j]);  // Samples
               if(hoverflow && fSADCev[i][j]>SADCoverflow) hoverflow->Fill(col,row);  //  Overflow
       }
       if(puls>SADCampcut){  
               int k=1;
               for(int j=0; j<12; j++){
                   if( trmsk&k) hTmvsTrig->Fill(time,j);
	           k=2*k;
               }
	}	  
    }
}

  if(evtype==7&&!rndtr){
     hnSADChitcut->Fill(SADCcuthits);
     hnSADChit->Fill(fNhits);
     hSADCsum->Fill(SADCsum);
     hSADCcutsum->Fill(SADCcutsum);
  }

  if (thr_flag) MYUNLOCK();
}

//==============================================================================

double PlaneHCAL2::SADCped(int ipbg, int  ipend,int *data, double &RMS) {
   double mnped=0.;
   RMS=0.;
   for (int i=ipbg;i<=ipend;i++)  mnped+=data[i];
   mnped/=(ipend-ipbg+1);
   for (int i=ipbg;i<=ipend;i++)   { RMS+=(data[i]-mnped)*(data[i]-mnped);
}
   RMS=sqrt(RMS/(ipend-ipbg+1));
 return mnped;
}

//==============================================================================

double PlaneHCAL2::SADCpuls(int ipbg, int  ipend,int *data,
                         double ped,double &sum,double &time) {
   double puls=0., pulspos=0.;
   sum=0.; time=0.;
   for (int i=ipbg;i<=ipend;i++) {
              double amp=data[i]-ped; 
              if(amp>puls) {puls=amp; pulspos=i;}
              if(amp<0.) amp=0.;
              sum+=amp;  time+=amp*i;
   }
   time=pulspos;
 return puls;
}
//==============================================================================

void PlaneHCAL2::StoreDigit(int col, int row, std::vector<float>& data) {
//
  int sadclen=data.size()-2;
  if(sadclen<1) return;
//
  if (fNhits < Nchan) {
    fSADCRow[fNhits]=row;
    fSADCCol[fNhits]=col;
    int len=Nsamples;
    if(sadclen<len){ len=sadclen;
                    for(int i=len;i<Nsamples;i++) fSADCev[fNhits][i]=0; }
    for(int i=0;i<len;i++) fSADCev[fNhits][i]=(int)data[i+2];
    fNhits++;
  }
}
//==============================================================================

void PlaneHCAL2::StoreDigit(CS::Chip::Digit* digit) {
  std::vector<float> data=digit->GetNtupleData();
  if(data.size()<3) return;
  const CS::ChipSADC::Digit* sadcdig = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
  if(sadcdig != NULL ) {
         int ix=sadcdig->GetX(); int iy=sadcdig->GetY();
         if(ix < 0||iy<0||ix>Ncols||iy>Nrows) return; 
         this->StoreDigit(ix,iy,data);
  }
}

//==============================================================================

PlaneHCAL2::~PlaneHCAL2() {
}




