#include "PlaneTrigHodo.h"

ClassImp(PlaneTrigHodo);

// implement your member functions here
void PlaneTrigHodo::Init(TTree* tree) {
  Plane1V::Init(tree);

  fVtc = AddVariable("_tcorr",100,-1000,1000,fNchan*fMAX_MULT);

  fVt_vs_spill = AddVariable("t_vs_spill",1000,-8000,2000,fNchan*fMAX_MULT);

  std::string tvspillname = fName+"_t_vs_spill";
  fHtVSspill=new TH2F(tvspillname.c_str(),tvspillname.c_str(), fVt_vs_spill->GetNbins(), fVt_vs_spill->GetMin(), fVt_vs_spill->GetMax(), 200, 0, 200);
  AddHistogram(fHtVSspill);
  fHtVSspill->SetOption("col");
  fHtVSspill->GetXaxis()->SetTitle("time vs spill");

  std::string tcname = fName+"_tcorr";
  fHtc=new TH1F(tcname.c_str(),tcname.c_str(), 400, -80, 80);
  AddHistogram(fHtc);
  fHtc->GetXaxis()->SetTitle("corrected time (ns)");

  std::string tcvcname = fName+"_tcorr_vs_ch";
  fHtcVSch=new TH2F(tcvcname.c_str(),tcvcname.c_str(), 200, -20, 20,
		    fVch->GetNbins(), fVch->GetMin(), fVch->GetMax());
  fHtcVSch->SetOption("col");
  AddHistogram(fHtcVSch);

}


void PlaneTrigHodo::EndEvent(const CS::DaqEvent &event) {
  Plane1V::EndEvent(event);
  if (thr_flag) TThread::Lock();

  fDigits.clear();

//   const float reftime = fIsHR ? event.GetTT().GetTimeHigh():
//     event.GetTT().GetTimeNorm();
//   const float factor = fIsHR ? CS::ChipF1::GetUnitHigh() :
//     CS::ChipF1::GetUnitNorm();

//   for (int i=0; i<fNhits; i++) {
//
//     double time = CS::ChipF1::TimeDifference(fTime[i],reftime);
//     int channel=fChannel[i];
  typedef std::list<CS::Chip::Digit*>::iterator lDIter;
  for (lDIter ii = lDigits.begin(); ii != lDigits.end(); ii++) {
    CS::ChipF1::Digit* iii = dynamic_cast<CS::ChipF1::Digit*> (*ii);
    if (!iii) {
      std::cerr<<"PlaneScifiJ::EndEvent: a digit is not a F1 one, strange...\n";
      continue;
    }
    float factor = iii->GetTimeUnit();
    double time = (double) (iii->GetTimeDecoded() / factor);
    int channel = iii->GetChannel();


    float timeT0 = -3000;
    if (fUseCalib && channel<(int)calib_data.size())
      timeT0 = calib_data[channel].t0;
    double tcorr = (time - timeT0)*factor;

    //cout << "TrigHodo tcorr: " << fName.c_str() << " " << channel << " " << tcorr << " " << timeT0 << endl; 

    std::vector<double> data;
    data.push_back(tcorr);
    fDigits.insert(make_pair(channel,CDigit1(channel,data)));	

    if( fVch->Test(channel) ) {

      fHtc->Fill(tcorr); //corrected time
      fHtcVSch->Fill(tcorr, channel); //corrected time

      fHtVSspill->Fill(time,event.GetBurstNumber());
    }
  }
  if (thr_flag) TThread::UnLock();
}


#if USE_DATABASE == 1
void PlaneTrigHodo::ReadCalib(const tm &t)
{
  // read-in corresponding calibration constants
  try{
    ReadFromDataBase(calib_data,t);
    //   std::cout<<"PlaneScifiJ::ReadCalib() ==> "
    //	<<this->GetName()<<" calibrations are found !"<<std::endl;

    if(calib_data.size() <  (unsigned) fNchan) {
      //    std::cerr<<"Size of Calibration File is not correct ! Should be : "
      //	  <<fNchan<<" Is "<<calib_data.size()<<" "
      //	  <<t.tm_mday<<"."<<t.tm_mon+1<<"."<<t.tm_year+1900<<" "
      //  <<t.tm_hour<<":"<<t.tm_min<<":"<<t.tm_sec<<std::endl;
    }
    else
      fUseCalib = true;

    //is not ok!!!!!!!
    fUseCalib=true;
  }

  catch(CS::Exception& e) {
    std::cerr<<e.what()<<std::endl;
  }
  catch(const std::exception &e) {
    std::cerr<<e.what()<<std::endl;
  }
  catch(...) {
    std::cout<<"PlaneTrigHodo::ReadCalib() ==> "<<GetName()
	<<" calibrations, valid for ";
    std::cout<<t.tm_mday<<"."<<t.tm_mon+1<<"."<<t.tm_year+1900<<" "
	<<t.tm_hour<<":"<<t.tm_min<<":"<<t.tm_sec
	<<", not found in DB"<<std::endl;
  }

}
#endif //USE_DATABASE


