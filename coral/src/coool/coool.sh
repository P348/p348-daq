#!/bin/bash -e

export CONDDB=../../../p348reco/conddb
export COOOL_CFGFILES_DIR=monitor

./src/coool \
  -map ../../../maps \
  -group ./monitor/groups.xml \
  -geom lstrack/detectors.dat \
  $@
