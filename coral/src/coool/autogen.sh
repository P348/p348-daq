#!/bin/bash -e

# Rebuild package build system
# Skip autoheader run (substitute `autoheader` command by `true` command) to
# avoid automatic generation of src/config.h.in
AUTOHEADER=true autoreconf --install --force

# Remove cache
rm -rf autom4te.cache
