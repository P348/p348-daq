#!/bin/bash -e

export CONDDB=../../../p348reco/conddb

./src/bcoool \
  -map ../../../maps \
  -group ./monitor/groups.xml \
  -geom lstrack/detectors.dat \
  -root bcoool.root \
  -text bcoool.txt \
  -notree -experthistos \
  $@
