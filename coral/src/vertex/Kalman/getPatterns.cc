/*!
   \file    getPatterns.cc
   \brief   Compass Vertex Pattern Class.
   \author  Alexandre Korzenev

*/

#include "CsAverPattern.h"

bool CsAverPattern::getPatterns( std::list<CsVertex*>& vrts, std::map<CsTrack*,bool>& specials )
{
  
  if( vrts_.size() ) {
    vrts = vrts_;
    specials = specials_;
    return true;
  } else
    return false;

}
