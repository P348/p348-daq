/*!
   \file    getPatterns.cc
   \brief   Compass Vertex Pattern Class.
   \author  Alexandre Korzenev

*/
#include "CsRolandPattern.h"
#include "CsEvent.h"
#include "CsGeant3.h"


bool CsRolandPattern::getPatterns( std::list<CsVertex*>& vrts, std::map<CsTrack*,bool>& specials )
{
  if( vrts_.size() ) {
    vrts = vrts_;
    return true;
  } else
    return false;
}
