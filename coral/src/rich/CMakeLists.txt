
#
# CsRCEvdis
#
set  (LINKDEF "evdis/LinkDef.h")
file (GLOB HEADERS "evdis/Cs*.h")
list (REMOVE_ITEM HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/evdis/LinkDef.h")
ROOT_GENERATE_DICTIONARY(EvdisDict ${HEADERS} LINKDEF ${LINKDEF})

file (GLOB SRC "evdis/Cs*.cc")
add_library(CsRCEvdis ${SRC} EvdisDict)

#
# CsRich1 Library
#

file (GLOB SRC  CsRCUty.cc CsRCCathode.cc CsRCDetectors.cc CsRCPhotonDet.cc
                CsRCMirrors.cc CsRCMirrorNom.cc CsRCMirrorElem.cc CsRCHistos.cc CsRCEventParticles.cc CsRCParticle.cc CsRCEventPads.cc CsRCPad.cc CsRCEventClusters.cc CsRCCluster.cc CsRCEventPartPhotons.cc CsRCPartPhotons.cc CsRCPhoton.cc
                CsRCEventRings.cc CsRCRing.cc CsRCEventAnalysis.cc CsRCEventDisplay.cc CsRCExeKeys.cc CsRCRecConst.cc CsRichOne.cc CsRCChiSqFit.cc CsRCCircleFit.cc CsRCEllipseFit.cc CsRCEllipseFitTest.cc CsRCGauPolFit.cc CsRCnTup.cc
                CsRCLikeAll.cc CsRCLikeAll02.cc CsRCLikeAll03.cc CsRCLikeAll04.cc CsRCLikeAll05.cc CsRCLikeAllMap.cc CsRCLikeRing.cc CsRCLikeRing02.cc CsRCLikeRing03.cc CsRCLikeRing04.cc CsRCLikeRing05.cc
                CsRCTrackMomFit.cc CsRCOptCorr.cc cforOpen.cc f-digits.F writeapp.F writebuff.F readbuff.F openfile.F)

add_library(CsRich1 ${SRC})

#
# Install heaeders
#
file (GLOB HEADER "*.h" "evdis/*.h")
install(FILES ${HEADER} DESTINATION ./include/)
