/*!
   \file    LinkDef.h
   \brief   CORAL Event Display Package.
   \author  Take-Aki TOEDA
   Link defintion file for ROOT
*/

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

//#pragma link C++ global theApp;

#pragma link C++ class CsRCDisplay;
#pragma link C++ class CsRCGHit;
#pragma link C++ class CsRCGCathode;
#pragma link C++ class CsRCGRing;
#pragma link C++ class CsRCGTrack;
#pragma link C++ class CsRCGRingTrack;
#pragma link C++ class CsRCEDisplay;
#pragma link C++ class CsRichOneDisplay;
#endif

