/*!
   \file    CsRCGRing.cc
   \brief   CORAL Event Display Package.
   \author  Take-Aki TOEDA
*/

#include "coral_config.h"
#include "CsRCGRing.h"

using namespace std;

ClassImp(CsRCGRing)
CsRCGRing::CsRCGRing(Int_t n, Double_t *ptr , Int_t mcolor, Int_t mtype) : TPolyLine3D(){

  SetLineColor(mcolor);
  SetLineStyle(mtype);
  SetPolyLine( n , ptr );

}
CsRCGRing::~CsRCGRing(){}

int CsRCGRing::GetType(){
   return Type;
}

void CsRCGRing::SetType(int type){
   Type = type;
}

void CsRCGRing::GetInfo(){
} 

void CsRCGRing::ExecuteEvent(Int_t event, Int_t px, Int_t py){
   if (event == kButton1Down){
     GetInfo();
   }
}
