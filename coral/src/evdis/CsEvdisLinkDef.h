/*!
   \file    CsEvdisLinkDef.h
   \brief   CORAL Event Display Package.
   \author  Sebastian Uhl
*/

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class CsEvdis;
#pragma link C++ class CsEvdisEvent;
#pragma link C++ class CsEvdisMagField;
#pragma link C++ class CsEvdisTrack;
#pragma link C++ class CsEndOfEvent;

#endif
