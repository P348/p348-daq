/*!
   \file    CsEvdisTrack.h
   \brief   CORAL Event Display Package.
   \author  Sebastian Uhl
*/

#ifndef CsEvdisTrack_h
#define CsEvdisTrack_h

#include <TEveTrack.h>

class CsTrack;

class CsEvdisTrack : public TEveTrack {
    public:
        CsEvdisTrack();

        void    ImportTrack(const CsTrack* track, TEveTrackPropagator* prop);

    ClassDef(CsEvdisTrack, 0)
};

#endif // CsEvdisTrack_h
