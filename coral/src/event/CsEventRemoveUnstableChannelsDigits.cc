#include <iostream>
#include <sstream>
#include <map>
#include "CsEvent.h"
#include "Reco/Calorimeter.h"
#include "CsCalorimeter.h"
#include "CsOpt.h"

using namespace std;


/*!
    \brief Remove CsDigits of unstable or low-efficient or noisy channels
    
    Information about unstable/low-efficient/noisy channels (i.e. wires/straws/fibers/strips/pads/cristals etc.)
    is taken from the text file, specified by option "decoding UnstableCannelsList  file_name.txt"
    (see comments inside this file for details of the format)
    
    \author Sergei.Gerassimov@cern.ch
*/




// simple class to store information from 
// UnstableChannelsList file

class UnstableChannels
{
  public:

  // Constructor with parameters
  UnstableChannels(int c0, int c1, int r0, int r1): 
    ch0(c0), ch1(c1), run0(r0), run1(r1) 
  {};

  // Methods
  bool ToBeRemoved(int ich, int irun){
    bool in_ch_range  = false;
    bool in_run_range = false;
    if((ch0 == ch1) && (ch1 == -1)) {
      in_ch_range  = true;
    } else {
      if(ch0 == -1){
	if((ich <= ch1)) in_ch_range     = true;
      } else if(ch1 == -1) {
	if((ich >= ch0)) in_ch_range     = true;
      } else {
	if((ich >= ch0) && (ich <= ch1)) in_ch_range = true;
      }
    }
    if((run0==run1) && (run1==-1))  {
      in_run_range = true;
    } else {
      if(run0 == -1){
	if((irun <= run1)) in_run_range     = true;
      } else if(run1 == -1) {
	if((irun >= run0)) in_run_range     = true;
      } else {
	if((irun >= run0) && (irun <= run1)) in_run_range = true;
      }
    }

    if(in_ch_range &&  in_run_range) {
      // cout<<"ToBeRemoved  ich = "<<ich<<"   irun = "<<irun<<"  TRUE"<<endl;
      return (true);
    }
    // cout<<"ToBeRemoved  ich = "<<ich<<"   irun = "<<irun<<"  FALSE"<<endl;
    return(false);
  };

  // Data members
  int ch0;
  int ch1;
  int run0;
  int run1;
};




bool CsEvent::RemoveUnstableChannelsDigits(){

  const  bool print = false;

  static bool first    = true;
  static bool inactive = false;
  if(inactive) return (true); // digits removing is OFF

  static multimap<string, UnstableChannels> mmUnstableChannels; // det. name -> unstable channels map
  int run_number = this->getRunNumber();
 

  if(first){ // "first call" block

    CsOpt::Instance();
    string filename;
    bool iret  =  CsOpt::Instance()->getOpt("decoding", "UnstableChannelsList", filename);
    if(!iret) {
      inactive = true; // this option is missing. Skip all other calls of this function.
      return (true);
    }
    
    cout<<endl;
    cout<<"================== CsEvent::RemoveUnstableChannelsDigits() =================="<<endl;
    ifstream ifs(filename.c_str()); // open file
    if(ifs){ // ifs stream state is OK

      cout<<"Parsing "<<filename<<" file..."<<endl;
      bool skip=false;
      string line;
      while(getline(ifs,line)){ // loop over lines in file
	if(line.empty()) continue;           // skip empty line
	if(line.find("/*")==0) {
	  skip = true;  // comment starts
	  continue;
	}
	if(line.find("*/")==0) {
	  skip = false; // comment ends
	  continue;
	}
	if(skip) continue;                   // skip if inside commented block
	// skip commented lines
	if(line.find("#" )==0) continue;
	if(line.find("//")==0) continue;
	stringstream ss(line);
	string detnam = "";
	int ch0  = -1;
	int ch1  = -1;
	int run0 = -1;
	int run1 = -1;
	ss>>detnam>>ch0>>ch1>>run0>>run1;
	if(!ss){
	  cout<<"Format error in the line: "<<endl;
	  cout<<line<<endl;
	  cout<<"(expected input types are: 'string, int, int, int, int')"<<endl;
	  cout<<"============================================================================="<<endl;
	  return (false);
	}

	// check if detnam is valid detector name for this geometry
	map<std::string,CsDet*>& all_CsDet = CsDet::GetAllDetectors();
	map<std::string,CsDet*>::iterator it_CsDet;
	bool correct_name     = false;
	for(it_CsDet =  all_CsDet.begin(); it_CsDet !=  all_CsDet.end();  it_CsDet++){ // loop over all CsDet
	  string nam = (*it_CsDet).first;
	  CsDet* det = (*it_CsDet).second;
	  if(detnam == nam) {
	    correct_name = true;
	    break;
	  }
	} // end of loop over all CsDet
	if(!correct_name){
	  cout<<"Error. Wrong detector TBName in the line: "<<endl;
	  cout<<line<<endl;
	  cout<<"(check if detector with this TBName exists in this setup)"<<endl;
	  cout<<"============================================================================="<<endl;
	  return (false);
	}

	int nwires = -1;
	int ncells = -1;
	bool correct_ch_range = true;
	bool correct_md_range = true;
	for(it_CsDet =  all_CsDet.begin(); it_CsDet !=  all_CsDet.end();  it_CsDet++){ // loop again over all CsDet
	  string nam = (*it_CsDet).first;
	  CsDet* det = (*it_CsDet).second;
	  if(detnam != nam) continue; // not the detector with specified name in current line  

 	  CsDetector* detector = dynamic_cast<CsDetector*>(det);
	  if(detector != NULL) { // if tracking detector
	    nwires = detector->getNWir();
	    if(ch0 != -1) {
	      if(ch0 < 0 || ch0 >= nwires)  correct_ch_range = false;
	    }
	    if(ch1 != -1) {
	      if(ch1 < 0 || ch1 >= nwires)  correct_ch_range = false;
	    }
	  } // end of if tracking detector

 	  CsCalorimeter* calorim = dynamic_cast<CsCalorimeter*>(det);
	  if(calorim != NULL) { // if calorimeter
	    ncells = calorim->NCells();
	    if(ch0 != -1) {
	      if(ch0 < 0 || ch0 >= ncells)  correct_md_range = false;
	    }
	    if(ch1 != -1) {
	      if(ch1 < 0 || ch1 >= ncells)  correct_md_range = false;
	    }
	  } // end of calorimeter


	} // end of loop over all CsDet
 	if(!correct_ch_range){
	  cout<<"Error.  Wrong channel number in the line: "<<endl;
	  cout<<line<<endl;
	  cout<<"Channel (if != -1) must be within [0 ; "<<nwires-1<<"] for this detector"<<endl;
	  cout<<"============================================================================="<<endl;
	  //return (false);
	}
 	if(!correct_md_range){
	  cout<<"Error.  Wrong channel number in the line: "<<endl;
	  cout<<line<<endl;
	  cout<<"Channel (if != -1) must be within [0 ; "<<ncells-1<<"] for this calorimrter"<<endl;
	  cout<<"============================================================================="<<endl;
	  return (false);
	}

	printf("%s:  Channels:  %6d --> %6d      Runs:  %7d --> %7d \n",detnam.c_str(),ch0,ch1,run0,run1);
	
	// create an object of UnstableChannels class
	UnstableChannels uc(ch0,ch1,run0,run1);
	// store the object into static multimap
	pair<string, UnstableChannels> pp(detnam, uc);
	mmUnstableChannels.insert(pp);
      } // end of loop over lines in file

    } else { // ifs stream state is NOT OK
      cout<<"Failed to open file with the list of unstable channels: "<<filename<<endl;
      cout<<"============================================================================="<<endl;
      return (false);
    }

    ifs.close(); // close the files
    first = false;
    cout<<"============================================================================="<<endl;
  } // end of "first call" block

  if(print) cout<<"============= CsEvent::RemoveUnstableChannelsDidits() ============="<<endl;
  if(print)cout<<"multimap<string, UnstableChannels> size = "<< mmUnstableChannels.size()<<endl;

  map<std::string,CsDet*>& all_CsDet = CsDet::GetAllDetectors();
  if(print) cout<<"Size of all_CsDet map = "<< all_CsDet.size()<<endl;
  map<std::string,CsDet*>::iterator it_CsDet;
  for(it_CsDet =  all_CsDet.begin(); it_CsDet !=  all_CsDet.end();  it_CsDet++){ // loop over all CsDet
    string nam = (*it_CsDet).first;
    CsDet* det = (*it_CsDet).second;

    pair< multimap<string, UnstableChannels>::iterator, multimap<string, UnstableChannels>::iterator> it_pair; 
    it_pair = mmUnstableChannels.equal_range(nam); // find all elements with detector name "nam"
    if(it_pair.first == it_pair.second) continue;  // nothing about det "nam"

    list<CsDigit*>& det_digits = const_cast<list<CsDigit*>&> (det->getMyDigits()); // list of CsDigit* of current CsDet
    assert(nam == string(det->GetTBName()));
    if(print) cout<<nam<<"   N digits = "<<det_digits.size()<<endl;

    // loop over digits on the detector
    int n_removed = 0;
    list<CsDigit*>::iterator idig = det_digits.begin();
    while(true){
      if(idig == det_digits.end()) break;
      int iWire = (*idig)->getAddress();

      // check if ths channel of this detector is in the list of unstable/low-efficien/noisy
      bool to_remove = false;
       for(multimap<string, UnstableChannels>::iterator it = it_pair.first; it != it_pair.second; ++it) { // loop over multimap elements 
	 if((*it).second.ToBeRemoved(iWire, run_number)) { to_remove = true; break; }
       } // end of loop over multimap elements

       if(print){
	 double* data = (*idig)->getData();
	 if(to_remove) cout<<"  *";
	 else          cout<<"   ";
	 cout<<iWire<<"  data size = "<<(*idig)->getDataSize()<<"   : ";
	 for(int i = 0; i < (*idig)->getDataSize(); i++)  cout<<"  "<<data[i]; // print digit's data
	 cout<<endl;
       }
       if(to_remove) { // delete this CsDigit
	 delete (*idig);
	 det_digits.erase(idig++); 
	 n_removed++;
       } else {
	 idig++; // next digit
       }
    } // end of loop over digits on the detector
    if(print) cout<<"   N removed digits = "<<n_removed<<endl;
  } // end of loop over CsDet
  if(print) cout<<"=================================================================="<<endl;
  
  return (true);
}
