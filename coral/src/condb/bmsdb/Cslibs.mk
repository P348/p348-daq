CS_LIB = -L$(top_srcdir)/lib/$(OS) \
                -lCsBase     \
                -lCsBeam        \
                -lCsCond        \
                -lCsTransMgr    \
                -lCsEvdis     \
                -lCsHist     \
                -lTraffic       \
                -lCsDecod    \
                -lCsEvent    \
                -lCsTrack    \
                -lCsEvmc     \
                -lCsObjStore  \
                -lCsGeom     \
				-lCsGEM      \
                -lCsRaw      \
                -lCsUtils    

#C_INCLUDES += -I$(top_srcdir)/include/ 
# Again a trouble fro ROOT. This is a temporary solution...
C_INCLUDES += -I$(top_srcdir)/include/ -I$(top_srcdir)/src/evdis/ -I$(top_srcdir)/src/hist/
