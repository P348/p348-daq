#ifndef CDB_h
#define CDB_h

#include <string>

class CDB {

 public:
  typedef std::pair<time_t,unsigned> Time;

 public:
  virtual ~CDB(void){};

  std::string read(const std::string &folder, std::string &data, Time tPoint, const std::string &keyword) {
         return read(folder, data, tPoint, keyword.c_str());
	  }

  virtual std::string read(const std::string &folder, std::string &data, Time tPoint, const char* keyword=0) = 0;

  virtual bool   ConnectDB(void)        { return true; }
  virtual void   DisconnectDB(void)     {}
  virtual bool   isConnected(void)      { return true; }
};

#endif
