/*!
   \file    CoralRelease.h
   \brief   Compass Release File.
   \author  Benigno Gobbo 
*/


#ifndef CoralRelease_h
#define CoralRelease_h

#define CORAL_MAJOR_RELEASE (1)
#define CORAL_MINOR_RELEASE (2)
#define CORAL_BUILD         (1)

char coralLogo[6][69] = {
  {"     MMMMMM                                                        "},
  {"   MMM    MMM     MMMMMM     MMMMMMMM         MMM      MMM         "},
  {"  MMM           MMM----MMM          MMM      MMMMM     MMM         "},
  {"  MMM           MMM----MMM   MMMMMMMM       MMM-MMM    MMM         "}, 
  {"   MMM    MMM   MMM----MMM   MMM MMM       MMM---MMM   MMM         "},
  {"     MMMMMM       MMMMMM     MMM    MMM   MMM-----MMM  MMMMMMMMM   "},
};

#endif // CoralRelease_h
