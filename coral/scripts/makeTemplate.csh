#!/bin/csh

# makeTemplate: CONVERT trafdic.xxx.opt OPTIONS FILE INTO A template.opt
# Usage: makeTemplate.csh [options] <trafdicOptFile> <prodDir> <templateOpt>
# <trafdicOptFile>
# - Well-formed trafdic.xxx.opt, equipped w/ a block of entries entitled
#            "===== TEMPLATE FOR MASS PRODUCTION"
# - Specified via either of the following two command line options:
#   -n <trafdicOptName> for <prodDir>/coral/src/user/<trafdicOptName>
#   -p <trafdicOptPath> 
# <prodDir> = Directory where to get maps, detectors.dat and the like...
#     It is checked to be equipped w/ directories maps, alignement, etc...
# <templateOpt> = Output file name
# Options:
# -c <beamCharge>: Reset beam charge
# -d <YYYY-MM-DD>: Date for CDB entrytime (Default is today)
# -i <magInfoName>: CsMagInfo file (Default is no CsMagInfo entry)     
# -h: Help

# - The script uncomments the MASS PRODUCTION entries, modifies what needs be
#  and outputs.
# - Caveats:
#   - Reliance on well-formed inputs => User need perform an a posteriori
#    double-checking, by e.g. making a diff w/ another, similar template.opt.
#   - Some specifics are not covered: e.g. DetNameOff, and implied TraF options,
#    special reconstruction settings, etc...
# - Example: makeTemplate.csh -d 2001-11-23 -n trafdic.2015.opt \ 
#      /afs/cern.ch/compass/production/testcoral/dy15W10t3 template.opt


# PURPOSE: Bring into action the option settings in the TEMPLATE BLOCK
# //  ===== TEMPLATE FOR MASS PRODUCTION (uncomment, and modify if needed) =====

# RATIONALE
# - Options files needed in mass production have some distinctive features:
# i) Setup files taken from mass prod. directory instead of $COMPASS_FILES.
# ii) IO option entries have w/ a placeholder for the argument field.
# iii) ...
# - A block of entries, introduced by above-mentioned "===== TEMPLATE...",
# and all commented out by default, allows for enabling these features,
# provided one uncomments them and adjusts them to the foreseen mass prod.
# - The present script does precisely this uncommenting+ajusting...
# ...at least, partially.

# STEPS TAKEN
# I) SPLIT AT "===== TEMPLATE..." LINE AND SAVE SEPARATELY head AND tail
# - Subsequently, except for the final remerging of head and tail, all
#  modifications concern the latter.
# II) MODIFICATIONS
# - Either mandatory: when not possible, execution aborts.
# - Or optional: only if option exists, as A COMMENTED OUT ENTRY, in the
#  TEMPLATE BLOCK of input trafdic.xxx.opt.
# II,1) ***** PLACEHOLDERS for I/O
# => Uncomment mandatorily 6 entries.
#    Uncomment optionally "CsEvent WriteBack"
# II,2) ***** mDST OPTIONS
# => Uncomment mandatorily one, first, entry.
#    Uncomment optionally "mDST (selectTrigger|Digits|DAQdigits|AllScalersTriggerMask)"
# II,3) ***** CDB OPTIONS
# => Uncomment mandatorily all 3 entries.
# => Change "entrytime" for either today or argument date
# II,4) ***** DETECTOR MONITORING
# => Uncomment mandatorily all 4 entries.
# II,5) ***** SETUP FILES
# => Uncomment mandatorily 4 entries
# => First three entries: erase or build again using this time $prodDir
# => Geometry file: first retrieve file name from <prodDir>. Can be either
#   CsROOT or CsGDML
# II,6) ***** MAGINFO (upon command line option)
# => Uncomment 2 entries.
# => Build CsMagInfo file path out of $prodDir and command line argument.
# II,7) ***** BEAM CHARGE (upon command line option)
# => Uncomment the (necessarily present) "TraF iCut" entry.
# => Uncomment "beam beam_charge", if it's there in <trafdicOptFile>.
# III) WARNING
# If "TraF DetNameoff" present in TEMPLATE block, emit a warning.
# IV) MERGING BACK head AND tail AND OUTPUT

########## PARSE COMMAND LINE
##### INIT OPTION SETTINGS
set entryTime = 20`date +%y-%m-%d`
set trafdicOptName = 0; set trafdicOptFile = 0
set magInfoName = 0
set beamCharge = 0

##### PARSE COMMAND LINE FOR OPTIONS
set moreHelp = 0
while ( $# > 0 && `printf "%s" $1 | egrep -c "^-"` != 0 )
    set arg = `printf "%s" $1 | sed -e {s/-//}`   # Strip arg of its leading "-"
    ##### OPTIONAL ARG: h=HELP,f=TRAFDICNAME,p=TRAFDICPATH,d=DATE,i=MAGINFO,c=BEAMCHARGE
    if ( $arg == h ) then    ##### OPTION h=HELP
	set moreHelp = 1
	##### USAGE MESSAGE
	usage:
 printf '# makeTemplate: CONVERT trafdic.xxx.opt OPTIONS FILE INTO A template.opt\n'
 printf 'Usage: makeTemplate.csh [options] <trafdicOptFile> <prodDir> <templateOpt>\n'
 printf ' <trafdicOptFile>\n'
 printf ' - Well-formed trafdic.xxx.opt, equipped w/ a block of entries entitled\n'
 printf '            "===== TEMPLATE FOR MASS PRODUCTION"\n'
 printf ' - Specified via either of the following two command line options:\n'
 printf '   -n <trafdicOptName> for <prodDir>/coral/src/user/<trafdicOptName>\n'
 printf '   -p <trafdicOptPath> \n'
 printf ' <prodDir> = Directory where to get maps, detectors.dat and the like...\n'
 printf '     It is checked to be equipped w/ directories maps, alignement, etc...\n'
 printf ' <templateOpt> = Output file name\n'
 printf 'Options:\n'
 printf ' -c <beamCharge>: Reset beam charge\n'
 printf ' -d <YYYY-MM-DD>: Date for CDB entrytime (Default is today)\n'
 printf ' -i <magInfoName>: CsMagInfo file (Default is no CsMagInfo entry)     \n'
 printf ' -h: Help\n'
	if ( $moreHelp == 1 ) then
	    ##### CONTINUATION OF HELP MESSAGE
 printf '\n'
 printf ' - The script uncomments the MASS PRODUCTION entries, modifies what needs be\n'
 printf '  and outputs.\n'
 printf ' - Caveats:\n'
 printf '  - Reliance on well-formed inputs => User need perform an a posteriori\n'
 printf '   double-checking, by e.g. making a diff w/ another, similar template.opt.\n'
 printf '   - Some specifics are not covered: e.g. DetNameOff, and implied TraF options,\n'
 printf '    special reconstruction settings, etc...\n'
 printf ' - Example: makeTemplate.csh -d 2001-11-23 -n trafdic.2015.opt \ \n'
 printf '     /afs/cern.ch/compass/production/testcoral/dy15W10t3 template.opt\n'
	endif
	exit 1
    else if ( $arg == c || $arg == d || $arg == n || $arg == p || $arg == i ) then
	##### ALL OPTIONS REQUIRING AN ARGUMENT
	if ( $# < 2 ) then
 printf '** makeTemplate:\a Wrong arg list!\n\n'; goto usage
	endif
	shift
	if ( $arg == d ) then    ##### OPTION d=DATE
	    set entryTime = $1
	    # Check that it's only digits and dash
	    set badChars = `echo $entryTime | tr -d "[:digit:]-"`
	    echo $badChars
	    if ( $#badChars != 0 ) then
 printf '** makeTemplate:\a Wrong argument date (="%s"): not a "YYYY-MM-DD" construct!\n' $entryTime
		exit 1
	    endif
	    # A more sophisticated check...
	    set fields = `echo $entryTime | awk 'BEGIN {FS="-"} {for(i=1;i<=NF;i++){printf "%s ", $i}; printf "\n"}'`
	    if ( $#fields != 3 ) then
 printf '** makeTemplate:\a Wrong argument date (="%s"): not a "YYYY-MM-DD" construct!\n' $entryTime
		exit 1
	    endif
	    # Check, crudely, numerics
	    set year = $fields[1]; set month = $fields[2]; set day = $fields[3]
	    if ( $year < 2000 || 2030 < $year || $month < 1 || 12 < $month || $day < 1 || 31 < $day ) then
 printf '** makeTemplate:\a Parsing argument date (="%s") yields y,m,d = %d,%d,%d which does not seem to be valid date!\n' $entryTime $year $month $day
		exit 1
	    endif
	else if ( $arg == n ) then    ##### OPTION n=TRAFDIC FILE NAME
	    set trafdicOptName = $1
	    # Require that it's a simple file w/o any "/"
	    if ( `echo $trafdicOptName | grep -c /` != 0 ) then
 printf '** makeTemplate:\a Bad <trafdicOptName> (="%s") argument: not a file name but a path\n' \
		    $trafdicOptName
		goto usage
	    endif

	else if ( $arg == p ) then    ##### OPTION p=TRAFDIC FILE PATH
	    set trafdicOptFile = $1
	else if ( $arg == i ) then    ##### OPTION i=MAGINFO
	    set magInfoName = $1
	    # Require that it's a simple name, w/ any "/"
	    if ( `echo $magInfoName | grep -c /` != 0 ) then
 printf '** makeTemplate:\a Bad <magInfoName> (="%s") argument: not a file name but a path\n' \
		    $magInfoName
		goto usage
	    endif
	else if ( $arg == c ) then    ##### OPTION c=BEAM CHARGE
	    set beamCharge = $1
	    if ( $beamCharge != 1 && $beamCharge != +1 && $beamCharge != -1 ) then
 printf '** makeTemplate:\a Bad <beamCharge> (="%s") argument: must be +/-1\n' \
		    $beamCharge
		goto usage
	    endif
	endif
    else
	printf '** makeTemplate:\a Wrong option "-%s" in arg list\!\n\n' $arg
	goto usage
    endif
    shift
end
# Check <trafdicOptFile> was specified...
if      ( $trafdicOptName == 0 && $trafdicOptFile == 0 ) then
    printf '** makeTemplate:\a Bad command line: no <trafdicOptFile> specified!\n\n'
    goto usage
else if ( $trafdicOptName != 0 && $trafdicOptFile != 0 ) then
    printf '** makeTemplate:\a Bad command line: <trafdicOptFile> specified twice!\n\n'
    goto usage
else if ( $trafdicOptFile != 0 ) then
    if !( -f $trafdicOptFile && -r $trafdicOptFile ) then
	if !( -e $trafdicOptFile ) then
	    printf '** makeTemplate:\a Argument <trafdicOptPath> (="%s") does not exist!\n\n' $trafdicOptFile
	else
	    printf '** makeTemplate:\a Argument <trafdicOptPath> (="%s") not a readable file!\n\n' $trafdicOptFile
	endif
	goto usage
    endif
endif

##### PARSE COMMAND LINE FOR ARGUMENTS
if ( $# != 2 ) then
    printf '** makeTemplate:\a Wrong command line: not all of <prodDir> <templateOpt> specified!\n\n'
    goto usage
endif
# <prodDir>
set prodDir = $1
# Check that it exists, it's a directory and it's readable
if !( -d $prodDir && -r $prodDir ) then
    if !( -e $prodDir ) then
	printf '** makeTemplate:\a Argument <prodDir> (="%s") does not exist!\n\n' $prodDir
    else
	printf '** makeTemplate:\a Argument <prodDir> (="%s") not a readable directory!\n\n' $prodDir
    endif
    goto usage
endif
# Strip away any trailing "/"
set prodDir = `echo $prodDir | sed -e 's%/$%%'` 
# Check that it contains "maps, alignement, dico, ROOTGeometry" subdir
set subDirs = ( maps alignement dico ROOTGeometry )
foreach subDir ( $subDirs )
    if !(  -d $prodDir/$subDir && -r $prodDir/$subDir ) then
    if !( -e $prodDir/$subDir ) then
	printf '** makeTemplate:\a Argument <prodDir> (="%s") has no "%s" subdirectory!\n\n' $prodDir $subDir
    else
	printf '** makeTemplate:\a Argument <prodDir> (="%s") has non-readable "%s" subdirectory!\n\n' $prodDir $subDir
    endif
    goto usage
endif
end
# <templateOpt>
# Check that it does not already exist
set templateOpt = $2
if ( -e $templateOpt ) then
    printf '** makeTemplate:\a Argument <templateOpt> (="%s") already exists!\n' $templateOpt
    exit 1
endif

##### TRAFDIC OPT FILE
# If SPECIFIED via -n <trafdicOptName>,
# check that it exists, it's not a directory and it's readable
if ( $trafdicOptFile == 0 ) then
    set trafdicOptFile = $prodDir/coral/src/user/$trafdicOptName
    if !( -f $trafdicOptFile && -r $trafdicOptFile ) then
	printf '** makeTemplate:\a Bad <trafdicOptFile> specified via <trafdicOptName> ...\n'
	if !( -e $trafdicOptFile ) then
	    printf ' ... File "%s" does not exist!\n\n' $trafdicOptFile
	else
	    printf ' ... File "%s" not a readable file!\n\n' $trafdicOptFile
	endif
	goto usage
    endif
endif
# In any case, check that it's by mistake a ".opt.c" file...
if ( `echo $trafdicOptFile | egrep -c '\.opt\.c'` != 0 ) then
    printf '** makeTemplate:\a Argument <trafdicOptFile> is a ".opt.c" file, whereas we expect a ".opt" file...\n'
    exit 1
endif

########## I) SPLIT AND SAVE
# CHECK 1ST THAT TEMPLATE BLOCK EXISTS!...
if ( `grep -c "===== TEMPLATE FOR MASS PRODUCTION" $trafdicOptFile` == 0 ) then
    printf '** makeTemplate:\a Argument file (="%s") not a true "trafdicOptFile"!:\n' $trafdicOptFile
    printf '"===== TEMPLATE FOR MASS PRODUCTION" is missing...\n\n'
    goto usage
endif    
if !( -e /tmp/$USER ) mkdir -d /tmp/$USER
if !( -e /tmp/$USER ) then
    set tmpDir = `mktemp -d --tmpdir=/tmp makeTemplate.XXXXXX`;
else
    set tmpDir = `mktemp -d --tmpdir=/tmp/$USER makeTemplate.XXXXXX`;
endif
\cp $trafdicOptFile $tmpDir/head
sed -i '/===== TEMPLATE FOR MASS PRODUCTION/,$d' $tmpDir/head
\cp $trafdicOptFile $tmpDir/tail
sed -i '/===== TEMPLATE FOR MASS PRODUCTION/,$\!d' $tmpDir/tail

##### CHECK FOR "key tag" EITHER MANDATORY or OPTIONAL
##### OPTIONAL BEING "CsEvent Writeback" and "mDST DAQdigits"
# Note that several entries are left aside:
# - TMPRICHFILE
# - geometry, because it can come inf different flavours: ROOTG or GDMLG
set keys = ( events Data mDST histograms mDST   CDB    CDB      CDB       Monitoring Monitoring Monitoring CsBuildPart decoding detector TraF )
set tags = ( to     file file home       select server username entrytime Directory  Residuals  Efficiency Hist map      table    Dicofit )
if ( $magInfoName != 0 ) then
    set keys = ( $keys CsMagInfo CsMagInfo ); set tags = ( $tags MySQLDB File )
endif
set nMandatories = $#keys
# Extra, optional, entries
set keys = ( $keys CsKalmanFitting mDST          mDST   mDST      )
set tags = ( $tags WriteBack       selectTrigger Digits DAQdigits )
set nRecommendeds = $#keys
# Extra, optional, very specific, entries
set keys = ( $keys CsEvent   )
set tags = ( $tags WriteBack )
# Loop on all ( key tag )'s
set error = 0; set missings = ( ); set mDSTselect = -1
set inFile = $tmpDir/tail
set nEntries = $#keys; set i = 0
while ( $i < $nEntries )
    set i = `expr $i + 1`; set key = $keys[$i]; set tag = $tags[$i]
    set j = `expr $i \- $nMandatories`
    set k = `expr $i \- $nRecommendeds`
    setenv PATTERN "//$key"'[ \t]*'"$tag"'[ \t]'
    set arg = `awk '$0 ~ ENVIRON["PATTERN"] {print $3}' $inFile`
    if ( ( $#arg != 1 && $j <= 0 ) || $#arg > 1 ) then
	# MANDATORY FAILED or ANYWAY AMBIGUOUS
	set error = 1
	if      ( $#arg == 0 ) then
 printf '** makeTemplate:\a Bad <trafdicOptFile>: No "%s %s <arg>" entry\n' \
	    $key $tag
	else if ( $#arg > 1 ) then
 printf '** makeTemplate:\a Ambiguous <trafdicOptFile>: More than one "%s %s <arg>" entry\n' \
	    $key $tag
	endif
    else if ( $#arg != 1 ) then
        # NOT MANDATORY and INEXISTING
	if ( $key == mDST && $tag == selectTrigger ) then
	    # Option "mDST selectrigger <mask>", i.e. unconditional output upon
	    # triggers in argument <mask>. The option is useless if option
	    # "mDST select" is set =0, i.e. all-out unconditional output.
	    if ( $mDSTselect != 0 ) set missings = ( $missings $i )
	else if ( $k < 0 ) then
	    set missings = ( $missings $i )
	endif
    else
	if ( $key == mDST && $tag == select ) set mDSTselect = $arg
    endif
end
unsetenv PATTERN
##### CHECK INPUT FILE FOR "TMPRICHFILE"
set tmpRichFile = 1
set arg = `awk '/[/][/]TMPRICHFILE/ {print $1}' $inFile`
if      ( $#arg == 0 ) then
    set tmpRichFile = 0
else if ( $#arg > 1 ) then
    printf '** makeTemplate:\a Ambiguous <trafdicOptFile>: More than one "TMPRICHFILE" entry\n'
    set error = 1
endif
if ( $error != 0 ) then
    printf ' => Exiting...\n'
    exit 1
endif
##### CHECK INPUT FILE FOR "DetNameOff"
set arg = `awk '/[/][/]TraF[ \t]DetNameOff/ {print $3}' $inFile`
if ( $#arg != 0 ) then
    set detNameOff = 1
else
    set detNameOff = 0
endif

printf '\n * makeTemplate: <trafdicOptFile> <prodDir> <templateOpt>\n'
printf '<trafdicOptFile> = "%s"\n' $trafdicOptFile
printf '<prodDir> = "%s"\n' $prodDir
printf '<templateOpt> = "%s"\n' $templateOpt
if ( $#missings != 0 || $tmpRichFile == 0 ) then
    printf 'NOTA BENE: You may consider the following option entries (not found in <trafdicOptFile>):'
    if ( $#missings != 0 && $tmpRichFile == 0 ) then
	printf ' TMPRICHFILE,'
    else if ( $tmpRichFile == 0 ) then
	printf ' TMPRICHFILE\n'
    endif
    if ( $#missings != 0 ) then
	set i = 0
	while ( $i < $#missings )
	    set i = `expr $i + 1`; set j = $missings[$i]
	    printf ' %s %s' $keys[$j] $tags[$j]
	    if ( $i == $#missings ) then
		printf '\n'
	    else
		printf ','
	    endif
	end
    endif
endif
if ( $detNameOff != 0) then
    printf "NOTA BENE: <trafdicOptFile> includes a list of DetNameOff'd detectors...\n"
    printf ' ...please check whether it need be enabled and edit <templateOpt> accordingly.\n'
endif
printf '\n'

########## II,1) PLACEHOLDERS for I/O
# => Uncomment mandatorily 6 entries.
#    Uncomment optionally "CsEvent WriteBack"
printf ' * makeTemplate: PLACEHOLDERS for I/O\n'
set inFile = $tmpDir/tail; set outFile = $tmpDir/tail2
if ( $error != 0 ) then
    printf ' => Exiting...\n'
    exit 1
endif
sed -e "\%//events to read% s%^//%%" \
    -e "\%//Data[ 	]*file% s%^//%%" \
    -e "\%//mDST[ 	]*file% s%^//%%" \
    -e "\%//histograms[ 	]*home% s%^//%%" \
    -e "\%//CsEvent[ 	]*WriteBack% s%^//%%" \
    -e "\%//CsKalmanFitting[ 	]*WriteBack% s%^//%%" \
    -e "\%//TMPRICHFILE% s%^//%%" $inFile >! $outFile

########## II,2) mDST OPTIONS
# => Uncomment mandatorily one, first, entry.
#    Uncomment optionally "mDST (selectTrigger|Digits|DAQdigits|AllScalersTriggerMask)"
# NOTA BENE: We do not want to uncomment the "mDST hits ALL" entry!...
printf ' * makeTemplate: mDST OPTIONS\n'
set inFile = $tmpDir/tail2; set outFile = $tmpDir/tail
sed -e "\%//mDST[ 	]*select% s%^//%%" \
    -e "\%//mDST[ 	]*selectTrigger% s%^//%%" \
    -e "\%//mDST[ 	]*hits[ 	]\([^A]\|A[^L]\)% s%^//%%" \
    -e "\%//mDST[ 	]*Digits% s%^//%%" \
    -e "\%//mDST[ 	]*DAQdigits% s%^//%%" \
    -e "\%//mDST[ 	]*AllScalersTriggerMask% s%^//%%" $inFile >! $outFile

########## II,3) CDB OPTIONS
# => Uncomment mandatorily all 3 entries.
# => Change "entrytime" for either today or argument date
printf ' * makeTemplate: CDB OPTIONS\n'
set inFile = $tmpDir/tail; set outFile = $tmpDir/tail2
sed -e "\%//CDB[ 	]*server% s%^//%%" \
    -e "\%//CDB[ 	]*username% s%^//%%" \
    -e "\%//CDB[ 	]*entrytime% s%.*%CDB entryTime 	$entryTime%" $inFile >! $outFile

########## II,4) DETECTOR MONITORING
# => Uncomment mandatorily all 4 entries.
printf ' * makeTemplate: DETECTOR MONITORING\n'
set inFile = $tmpDir/tail2; set outFile = $tmpDir/tail
sed -e "\%//Monitoring[ 	]*Directory% s%^//%%" \
    -e "\%//Monitoring[ 	]*Residuals% s%^//%%" \
    -e "\%//Monitoring[ 	]*Efficiency% s%^//%%" \
    -e "\%//CsBuildPart[ 	]*Hist% s%^//%%" $inFile >! $outFile

########## II,5) SETUP FILES
# => Uncomment mandatorily 4 entries
# => First three entries: erase or build again using this time $prodDir
# => Geometry file: first retrieve file name from <prodDir>. Can be either
#   CsROOT or CsGDML
printf ' * makeTemplate: SETUP FILES\n'
set inFile = $tmpDir/tail; set outFile = $tmpDir/tail2
##### CHECK (ROOT|GDML)Geometry ENTRY and RETRIEVE FILENAME
set CsROOT = `awk '/[/][/]CsROOTGeometry[ \t]*file/ {print $3}' $inFile`
set CsGDML = `awk '/[/][/]CsGDMLGeometry[ \t]*file/ {print $3}' $inFile`
set nGeos = `expr $#CsROOT + $#CsGDML`
if ( $nGeos == 0 ) then
    printf '** makeTemplate:\a Bad <trafdicOptFile>: No "%s %s" entry, nor "%s %s"\n' \
	CsROOTGeometry file CsGDMLGeometry file
    exit 1
else if ( $nGeos > 1 ) then
    printf '** makeTemplate:\a Ambiguous <trafdicOptFile>: More than one "%s %s" or "%s %s" entry"\n' \
	CsROOTGeometry file CsGDMLGeometry file
    exit 1
endif
# Check Geometry file exists in $prodDir
if ( $#CsROOT != 0 ) then
    set geometryFile = `\ls $prodDir/ROOTGeometry/*.C`
    set geometryType = CsROOT; set geometryExtension = C
else
    set geometryFile = `\ls $prodDir/ROOTGeometry/*.root`
    set geometryType = CsGDML; set geometryExtension = root
endif
if      ( $#geometryFile == 0 ) then
    printf '** makeTemplate:\a <prodDir> has no %s file (w/ extension ".%s") file in its "ROOTGeometry" subdirectory, whereas it is requested by <trafdicOptFile>!\n' \
	$geometryType $geometryExtension
    exit 1
else if ( $#geometryFile > 1 ) then
    printf '** makeTemplate:\a <prodDir> has more than one %s file (w/ extension ".%s") file in its "ROOTGeometry" subdirectory!\n' \
	$geometryType $geometryExtension
    exit 1
endif
if !( -f $geometryFile && -r $geometryFile ) then 
    printf '** makeTemplate:\a %sGeometry file (="%s") in <prodDir>/ROOTGeometry is not a readable file!\n' $geometryFile
    exit 1
endif
##### IMPLEMENT
sed -e "\%//decoding[ 	]*map% s%.*%decoding	map	$prodDir/maps%" \
    -e "\%//detector[ 	]*table% s%.*%detector	table	$prodDir/alignement/%" \
    -e "\%//TraF[ 	]*Dicofit% s%.*%TraF		Dicofit	$prodDir/dico/%" \
    -e "\%//CsROOTGeometry[ 	]*file% s%.*%CsROOTGeometry	file	$geometryFile%" \
    -e "\%//CsGDMLGeometry[ 	]*file% s%.*%CsGDMLGeometry	file	$geometryFile%" $inFile >! $outFile

if ( $magInfoName != 0 ) then
    ########## II,6) MAGINFO (upon command line option)
    # => Build CsMagInfo file path out of $prodDir and command line argument.
    # => Uncomment also the "CsMagInfo MySQLDB 0" entry (that resets the
    #  assigment of CsMagInfo to MySQLDB that is specified in the head).
    printf ' * makeTemplate: MAGINFO\n'
    set inFile = $tmpDir/tail2; set outFile = $tmpDir/tail
    # Check magInfo file exists in $prodDir
    set magInfoFile = $prodDir/$magInfoName
    if !( -f $magInfoFile && -r $magInfoFile ) then 
	if !( -e $magInfoFile ) then
	    printf '** makeTemplate:\a No <magInfoName> (="%s") in <prodDir>, whereas it is requested from command line!\n' $magInfoName
	else
	    printf '** makeTemplate:\a <magInfoName> (="%s") in <prodDir> is not a readable file!\n\n' $magInfoName $prodDir
	endif
	exit 1
    endif
    sed -e "\%//CsMagInfo[ 	]*MySQLDB% s%^//%%" \
        -e "\%//CsMagInfo[ 	]*File% s%.*%CsMagInfo	File	$prodDir/$magInfoName%" $inFile >! $outFile
endif

if ( $beamCharge != 0 ) then
    ########## II,7) BEAM CHARGE (upon command line option)
    # => Uncomment the (necessarily present) "TraF iCut" entry.
    # => Uncomment "beam beam_charge", if it's there in <trafdicOptFile>.
    printf ' * makeTemplate: BEAM CHARGE\n'
    set tmpFile = $inFile; set inFile = $outFile; set outFile = $tmpFile
    ##### CHECK FOR "TraF iCut [15]
    set charge = `awk '/[/][/]TraF[ \t]*iCut[ \t]*\[15\]/ {print $4}' $inFile`
    if      ( $#charge == 0 ) then
	printf '** makeTemplate:\a Bad <trafdicOptFile>: No "TraF iCut [15]" entry, while beam charge resetting is requested via command line option "-c"\n'
	exit 1
    else if ( $#charge > 1 ) then
	printf '** makeTemplate:\a Ambiguous <trafdicOptFile>: More than one "TraF iCut [15]" entry\n'
	exit 1
    endif
    set bms = `awk '/[/][/]beam[ \t]*beam_charge/ {print $3}' $inFile`
    sed -e "\%//TraF[ 	]*iCut[ 	]*\[15\]% s%.*%TraF	iCut	[15]	$beamCharge%" \
	-e "\%//beam[ 	]*beam_charge% s%.*%beam	beam_charge	$beamCharge%" $inFile >! $outFile
endif

##### OUTPUT
printf ' * makeTeamplate: Output to "./%s"\n' $templateOpt
\cp $tmpDir/head $templateOpt
cat $outFile >> $templateOpt
exit 0
