
#
# CORAL Project
#

#
# TODO-list: @meyerma
# - Implementation of CsOracle library (I don't have Oracle)
# - Qt interface for calorimeter (disabled for the moment due to issue in the linkage of Reco library with Qt wrappers)
#

project (CORAL)
set(CORAL_DATE 20200507)
string(ASCII 27 Esc)

message(STATUS "------------------------")
message(STATUS "---- CORAL -------------")
message(STATUS "------------------------")

#
# Specific CMAKE config
#

cmake_minimum_required (VERSION 3.0)
message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")

if(${CMAKE_BUILD_TYPE} MATCHES "Debug")
	add_compile_options(-O0 -g -Wall -Wextra)   #-Wextra -Wl,--no-undefined") # @meyerma: Remove these flags because of GCC 8.1
	set(CMAKE_C_FLAGS "-O0 -g -Wall -Wextra")   #-Wextra -Wl,--no-undefined")
	set(CMAKE_CXX_FLAGS "-O0 -g -Wall -Wextra") #-Wextra -Wl,--no-undefined")
else()
	add_compile_options(-w -O2)
	set(CMAKE_C_FLAGS "-w -O2")
	set(CMAKE_CXX_FLAGS "-w -O2")
endif()

list(APPEND CMAKE_MODULE_PATH $ENV{ROOTSYS}/etc/cmake)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/Modules/ ${CMAKE_SOURCE_DIR}/cmake/Inc/)

# Detect autoconf installation
if(EXISTS ${CMAKE_SOURCE_DIR}/GNUmakefile)

        message(FATAL_ERROR "${Esc}[33mAutoconf configuration detected.${Esc}[m Please use `make distclean` first, if you want to compile using CMake.")
endif()

if (CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR)
    message(FATAL_ERROR "In-source builds are not allowed.")
endif()

#
# Compile CORAL libraries
#

include_directories(src)
include_directories(src/alignment)
include_directories(src/util)
include_directories(src/geom)
include_directories(src/geom/mumega)
include_directories(src/geom/gem)
include_directories(src/base)
include_directories(src/hist)
include_directories(src/DaqDataDecoding)
include_directories(src/DaqDataDecoding/src)
include_directories(src/beam)
include_directories(src/beam/rec_method_0)
include_directories(src/beam/rec_method_1)
include_directories(src/beam/rec_method_2)
include_directories(src/ppi/include)
include_directories(src/ppi/tools)
include_directories(src/event)
include_directories(src/evdb)
include_directories(src/evmc)
include_directories(src/track)
include_directories(src/track/trafdic/src)
include_directories(src/track/trafdic/src/Aux)
include_directories(src/track/trafdic/src/Fortran)
include_directories(src/vertex)
include_directories(src/vertex/Kalman)
include_directories(src/vertex/Roland)
include_directories(src/triggers)
include_directories(src/rich)
include_directories(src/rich/evdis)
include_directories(src/condb)
include_directories(src/condb/filedb)
include_directories(src/condb/handler)
include_directories(src/condb/mysqldb/Utils)
include_directories(src/condb/mysqldb/MyInterface)
include_directories(src/oracle)
include_directories(src/evdis)
include_directories(src/useMN)
include_directories(src/calorim)
include_directories(src/calorim/Reco)
include_directories(src/calorim/Reco/src)
include_directories(src/calorim/Reco/src/fortran_reco/commonblocks)
include_directories(src/calorim/Reco/src/fortran_reco/functions)

#
# Additional includes
#
include(DumpCmakeVariables)
include(AutoDetectCompiler)
include(AddRecursiveSubdirectory)
include(LoadRequirements)
include(CompassRequirements)

add_subdirectory(src/track)
add_subdirectory(src/calorim)
add_subdirectory(src/calorim/Reco)
add_subdirectory(src/util)
add_subdirectory(src/base)
add_subdirectory(src/geoutil)
add_subdirectory(src/vertex)
add_subdirectory(src/triggers)
add_subdirectory(src/condb/filedb)
add_subdirectory(src/event)
add_subdirectory(src/evmc)
add_subdirectory(src/evdb)
add_subdirectory(src/rich)
add_subdirectory(src/geom)
add_subdirectory(src/beam)
add_subdirectory(src/hist)
add_subdirectory(src/DaqDataDecoding)
add_subdirectory(src/user)
add_subdirectory(src/alignment)

if(ALL)
	add_subdirectory(src/tests)
	add_subdirectory(src/track/trafdic/makeDico)
endif()

if(USE_MN)
	add_subdirectory(src/useMN)
endif()

if(USE_MySQL)
    message(STATUS "Prepare Coral MySQL dependencies")
    add_subdirectory(src/condb/mysqldb)
    add_subdirectory(src/condb/handler)
    add_subdirectory(src/ppi)
endif()

if(USE_ORACLE) # To be done.. first install oracle.......
    message(STATUS "Prepare Coral Oracle dependencies")
    add_subdirectory(src/oracle)
    if(NOT ALL)
	    add_subdirectory(src/tests/oracle)
    endif()
endif()

if(USE_NewEDIS)
    add_subdirectory(src/evdis)
endif(USE_NewEDIS)

#
# Prepare makefiles for CORAL in PHAST compilation
#
CONFIGURE_FILE( ${CMAKE_SOURCE_DIR}/cmake/Templates/General.mk.in ${CMAKE_BINARY_DIR}/makefiles/General.mk)
CONFIGURE_FILE( ${CMAKE_SOURCE_DIR}/cmake/Templates/Include.mk.in ${CMAKE_BINARY_DIR}/makefiles/Include.mk)

#
# Prepare ./build to be run standalone without "make install"
#
add_custom_target(project_structure ALL
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/bin
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/src
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/include
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/share
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/lib/${OS}
)

file (GLOB HEADER "src/*.h")
add_custom_target(project_structure_include ALL
	DEPENDS project_structure
	COMMAND cp ${HEADER} ${CMAKE_BINARY_DIR}/include
)

add_custom_target(project_structure_share ALL
	DEPENDS project_structure
	COMMAND cp -Rf ${CMAKE_SOURCE_DIR}/src/pkopt ${CMAKE_BINARY_DIR}/share/
	COMMAND cp -Rf ${CMAKE_SOURCE_DIR}/doc ${CMAKE_BINARY_DIR}/share/
	COMMAND cp -Rf ${CMAKE_SOURCE_DIR}/scripts ${CMAKE_BINARY_DIR}/share/
	COMMAND cp -Rf ${CMAKE_SOURCE_DIR}/tools ${CMAKE_BINARY_DIR}/share/
	COMMAND ln -sf ../share/pkopt ${CMAKE_BINARY_DIR}/src/
	COMMAND ln -sf ../src/user ${CMAKE_BINARY_DIR}/share/
)

#
# Prepare coral.exe executable
#
if(PHAST_FOUND)

    file (GLOB PHAST_SRC ${PHAST}/coral/*.cc)
    add_executable(coral.exe ${PHAST_SRC})
    target_include_directories(coral.exe PUBLIC ${PHAST}/lib)
    target_include_directories(coral.exe PUBLIC ${PHAST}/src)
    target_include_directories(coral.exe PUBLIC ${PHAST}/graph)
    target_include_directories(coral.exe PUBLIC ${PHAST}/rich)
    target_include_directories(coral.exe PUBLIC ${ROOT_INCLUDE_DIR})
    target_include_directories(coral.exe PUBLIC ${CERNLIB_INCLUDE_DIRS})
    target_link_libraries(coral.exe ${PHAST_LIBRARIES})
    target_link_libraries(coral.exe ${CORAL_LIBRARIES})
    target_link_libraries(coral.exe ${CORAL_THIRD_PARTY_LIBRARIES})

    add_custom_target(project_structure_phast ALL
	DEPENDS project_structure
		COMMAND ln -sf ${PHAST}/phast ${CMAKE_BINARY_DIR}/bin
		COMMAND ln -sf ${PHAST}/phast-merging ${CMAKE_BINARY_DIR}/bin
		COMMAND find ${PHAST} -name "*.pcm" -exec ln -sf {} ${CMAKE_BINARY_DIR}/lib/${OS} "\;"
		COMMAND find ${PHAST} -name "*.so" -exec ln -sf {} ${CMAKE_BINARY_DIR}/lib/${OS} "\;"
	)

endif()

# Copy pcm/rootmap files from lib to bin
INSTALL(CODE "execute_process (
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin
        COMMAND find ${CMAKE_BINARY_DIR}/bin -name \"*.pcm\" -exec unlink {} \;
        COMMAND find ${CMAKE_BINARY_DIR}/bin -name \"*.rootmap\" -exec unlink {} \;
        COMMAND find ${CMAKE_BINARY_DIR}/lib/${OS} -name \"*.pcm\" -exec bash -c \"ln -s ../lib/${OS}/\$(basename {}) ${CMAKE_BINARY_DIR}/bin\" \;
        COMMAND find ${CMAKE_BINARY_DIR}/lib/${OS} -name \"*.rootmap\" -exec bash -c \"ln -s ../lib/${OS}/\$(basename {}) ${CMAKE_BINARY_DIR}/bin\" \;
        ERROR_QUIET
)")

INSTALL(CODE "execute_process (
	COMMAND mkdir -p ${CMAKE_INSTALL_PREFIX}/share
	COMMAND ln -s -f share ${CMAKE_INSTALL_PREFIX}/src
	ERROR_QUIET)"
)
install(DIRECTORY ${CMAKE_BINARY_DIR}/share/pkopt   DESTINATION share USE_SOURCE_PERMISSIONS)
install(DIRECTORY ${CMAKE_BINARY_DIR}/share/tools   DESTINATION share USE_SOURCE_PERMISSIONS)
install(DIRECTORY ${CMAKE_BINARY_DIR}/share/scripts DESTINATION share USE_SOURCE_PERMISSIONS)
install(DIRECTORY ${CMAKE_BINARY_DIR}/share/doc    DESTINATION share USE_SOURCE_PERMISSIONS)

#
# Installation procedure
#
install(DIRECTORY ${CMAKE_BINARY_DIR}/lib     DESTINATION .     USE_SOURCE_PERMISSIONS)
install(DIRECTORY ${CMAKE_BINARY_DIR}/bin     DESTINATION .     USE_SOURCE_PERMISSIONS)

file (GLOB HEADER "src/*.h")
install(FILES ${HEADER} DESTINATION ./include)

install(CODE "execute_process (COMMAND cp -R ${CMAKE_SOURCE_DIR}/src/DaqDataDecoding/src ${CMAKE_INSTALL_PREFIX}/include/DaqDataDecoding ERROR_QUIET)")
install(CODE "execute_process (COMMAND cp -R ${CMAKE_SOURCE_DIR}/src/calorim/Reco/src ${CMAKE_INSTALL_PREFIX}/include/Reco ERROR_QUIET)")
if(USE_TGEANT)
    install(CODE "execute_process (COMMAND ln -fs ${TGEANT_INCLUDE_DIR} ${CMAKE_INSTALL_PREFIX}/include/includeTGEANT)")
endif()

install(DIRECTORY ${CMAKE_BINARY_DIR}/makefiles DESTINATION . USE_SOURCE_PERMISSIONS)
install(DIRECTORY ${CMAKE_BINARY_DIR}/src/user DESTINATION share
	PATTERN "${CMAKE_BINARY_DIR}/src/user/*.opt"
	PATTERN "${CMAKE_BINARY_DIR}/src/user/CMakeFiles" EXCLUDE
	PATTERN "${CMAKE_BINARY_DIR}/src/user/cmake*" EXCLUDE
	PATTERN "${CMAKE_BINARY_DIR}/src/user/Makefile" EXCLUDE)

if(ALL)
	install(CODE "execute_process (COMMAND cp ${CMAKE_SOURCE_DIR}/src/track/trafdic/makeDico/dicofit.dat ${CMAKE_INSTALL_PREFIX}/bin ERROR_QUIET)")
endif()

