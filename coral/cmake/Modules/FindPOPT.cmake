# try to find POPT
#
# output variables
#   POPT_LIBRARY
#   POPT_LIBRARY_DIR
#   POPT_INCLUDE_DIR

macro(_FIND_POPT)
  message(STATUS "Checking for POPT")

  set(POPT_LIBRARY)
  set(POPT_LIBRARY_DIR)
  set(POPT_INCLUDE_DIR)

  find_library(_poptlib popt PATHS
    ${POPT_DIR}/usr/lib
    ${POPT_DIR}/lib
    ${POPT_DIR}
    NO_DEFAULT_PATH
  )
  find_library(_poptlib popt)
  if(NOT _poptlib STREQUAL "_poptlib-NOTFOUND")
    get_filename_component(POPT_LIBRARY_DIR ${_poptlib} PATH)
    get_filename_component(POPT_LIBRARY     ${_poptlib} NAME_WE)
    string(REGEX REPLACE "^lib" "" POPT_LIBRARY ${POPT_LIBRARY})
  endif(NOT _poptlib STREQUAL "_poptlib-NOTFOUND")

  find_file(_poptinc popt.h PATHS
    ${POPT_DIR}/usr/include
    ${POPT_DIR}/include
    ${POPT_DIR}
    NO_DEFAULT_PATH
  )
  find_file(_poptinc popt.h)
  if(NOT _poptinc STREQUAL "_poptinc-NOTFOUND")
    get_filename_component(POPT_INCLUDE_DIR ${_poptinc} PATH)
  endif(NOT _poptinc STREQUAL "_poptinc-NOTFOUND")

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(POPT DEFAULT_MSG POPT_LIBRARY POPT_LIBRARY_DIR POPT_INCLUDE_DIR)
endmacro(_FIND_POPT)

if(NOT DEFINED POPT_FOUND)
  _FIND_POPT()
  if(POPT_FOUND)
    set(POPT_FOUND       ${POPT_FOUND}       CACHE INTERNAL "Found POPT")
    set(POPT_LIBRARY     ${POPT_LIBRARY}     CACHE INTERNAL "POPT libraries")
    set(POPT_LIBRARY_DIR ${POPT_LIBRARY_DIR} CACHE INTERNAL "POPT library directory")
    set(POPT_INCLUDE_DIR ${POPT_INCLUDE_DIR} CACHE INTERNAL "POPT include directory")
  endif(POPT_FOUND)
endif(NOT DEFINED POPT_FOUND)
