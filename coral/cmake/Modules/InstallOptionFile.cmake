macro(INSTALL_OPTION_FILE opt_file)
  get_filename_component(_rel_dir ${opt_file} PATH)
  add_custom_command(OUTPUT ${opt_file}
                     COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_CURRENT_BINARY_DIR}/${_rel_dir}"
                     COMMAND ${CMAKE_COMMAND} -E create_symlink "${CMAKE_CURRENT_SOURCE_DIR}/${opt_file}" "${CMAKE_CURRENT_BINARY_DIR}/${opt_file}")
endmacro(INSTALL_OPTION_FILE)

macro(INSTALL_OPTION_FILES)
  set(_files)
  foreach(_entry ${ARGN})
    list(APPEND _files ${_entry})
  endforeach(_entry ${ARGN})

  list(REMOVE_DUPLICATES _files)

  foreach(_entry ${_files})
    install_option_file(${_entry})
  endforeach(_entry ${_files})

  file(RELATIVE_PATH _rel_src ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
  set(target_name "install_option_files_${_rel_src}")
  string(REPLACE "/" "_" target_name ${target_name})

  add_custom_target(${target_name} ALL
                    DEPENDS ${_files})
endmacro(INSTALL_OPTION_FILES)

