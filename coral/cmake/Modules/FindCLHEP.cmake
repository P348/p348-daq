################################################################################
#    Copyright (C) 2014 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH    #
#                                                                              #
#              This software is distributed under the terms of the             #
#         GNU Lesser General Public Licence version 3 (LGPL) version 3,        #
#                  copied verbatim in the file "LICENSE"                       #
################################################################################
# - Try to find CLHEP
# Once done this will define
#
#  CLHEP_FOUND - system has CLHEP
#### (not needed)  CLHEP_INCLUDE_DIR - the CLHEP include directory
#### (not needed)  CLHEP_INCLUDE_DIRS - the CLHEP include directory
#  CLHEP_LIBRARIES - The libraries needed to use CLHEP
#### (not needed)  CLHEP_DEFINITIONS - Compiler switches required for using CLHEP
#

if (CLHEP_INCLUDE_DIR AND CLHEP_LIBRARY_DIR)
 SET (CLHEP_INCLUDE_DIR CLHEP_INCLUDE_DIR-NOTFOUND)
 SET (CLHEP_LIB_DIR CLHEP_LIB_DIR-NOTFOUND)
 SET (CLHEP_PLISTS_LIB_DIR CLHEP_PLISTS_LIB_DIR-NOTFOUND)
endif (CLHEP_INCLUDE_DIR AND CLHEP_LIBRARY_DIR)

FIND_PATH(CLHEP_INCLUDE_DIR NAMES CLHEP PATHS
 $ENV{CLHEP}/include
 $ENV{DIR_CLHEP}/include
 $ENV{CLHEP_DIR}/include
 NO_DEFAULT_PATH
)

if (CLHEP_INCLUDE_DIR)
    MACRO(SUBDIRLIST result curdir)
      FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
      SET(dirlist "")
      FOREACH(child ${children})
        IF(IS_DIRECTORY ${curdir}/${child})
          LIST(APPEND dirlist ${curdir}/${child})
        ENDIF()
      ENDFOREACH()
      SET(${result} ${dirlist})
    ENDMACRO()

    SUBDIRLIST(CLHEP_INCLUDE_DIRS ${CLHEP_INCLUDE_DIR})
endif()

FIND_PATH(CLHEP_LIBRARY_DIR NAMES libCLHEP.so PATHS
 $ENV{CLHEP}/lib
 $ENV{CLHEP_DIR}/lib
 $ENV{DIR_CLHEP}/lib
 NO_DEFAULT_PATH
)

if (CLHEP_LIBRARY_DIR)
  set(CLHEP_FOUND TRUE)
endif (CLHEP_LIBRARY_DIR)

if (CLHEP_FOUND)
 set(CLHEP_INCLUDE_DIRS "${CLHEP_INCLUDE_DIR}")
 if (NOT CLHEP_FIND_QUIETLY)
   MESSAGE(STATUS "Looking for CLHEP... - found ${CLHEP_LIBRARY_DIR}")
 endif ()
 FILE(GLOB CLHEP_LIBRARIES "${CLHEP_LIBRARY_DIR}/lib*.so")

else()

 if (CLHEP_FIND_REQUIRED)
   message(FATAL_ERROR "Looking for CLHEP... - Not found (define CLHEP environment variable)")
 else()
   message(STATUS "Looking for CLHEP... - Not found (define CLHEP environment variable)")
 endif()
endif()
