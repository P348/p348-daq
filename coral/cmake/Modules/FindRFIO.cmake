# try to find RFIO
#
# input variables
#   RFIO_DIR
#
# output variables
#   RFIO_LIBRARY
#   RFIO_LIBRARY_DIR
#   RFIO_INCLUDE_DIR

macro(_FIND_RFIO)
  message(STATUS "Checking for RFIO")

  set(RFIO_LIBRARY)
  set(RFIO_LIBRARY_DIR)
  set(RFIO_INCLUDE_DIR)

  find_library(_shiftlib shift PATHS
    ${RFIO_DIR}/usr/lib
    ${RFIO_DIR}/lib
    ${RFIO_DIR}
    NO_DEFAULT_PATH
  )
  if(NOT _shiftlib STREQUAL "_shiftlib-NOTFOUND")
    get_filename_component(RFIO_LIBRARY_DIR ${_shiftlib} PATH)
    get_filename_component(RFIO_LIBRARY     ${_shiftlib} NAME_WE)
    string(REGEX REPLACE "^lib" "" RFIO_LIBRARY ${RFIO_LIBRARY})
  endif(NOT _shiftlib STREQUAL "_shiftlib-NOTFOUND")

  find_file(_shiftinc shift.h PATHS
    ${RFIO_DIR}/usr/include
    ${RFIO_DIR}/include
    ${RFIO_DIR}
    NO_DEFAULT_PATH
  )
  if(NOT _shiftinc STREQUAL "_shiftinc-NOTFOUND")
    get_filename_component(RFIO_INCLUDE_DIR ${_shiftinc} PATH)
  endif(NOT _shiftinc STREQUAL "_shiftinc-NOTFOUND")

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(RFIO DEFAULT_MSG RFIO_LIBRARY RFIO_LIBRARY_DIR RFIO_INCLUDE_DIR)
endmacro(_FIND_RFIO)

if(NOT DEFINED RFIO_FOUND)
  _FIND_RFIO()
  if(RFIO_FOUND)
    set(RFIO_FOUND       ${RFIO_FOUND}       CACHE INTERNAL "Found RFIO")
    set(RFIO_LIBRARY     ${RFIO_LIBRARY}     CACHE INTERNAL "RFIO libraries")
    set(RFIO_LIBRARY_DIR ${RFIO_LIBRARY_DIR} CACHE INTERNAL "RFIO library directory")
    set(RFIO_INCLUDE_DIR ${RFIO_INCLUDE_DIR} CACHE INTERNAL "RFIO include directory")
  endif(RFIO_FOUND)
endif(NOT DEFINED RFIO_FOUND)

