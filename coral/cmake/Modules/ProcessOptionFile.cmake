macro(PROCESS_OPTION_FILE opt_file source)
  set(_farm 1)
  if(CORAL_COMPUTERFARM STREQUAL "GridKA")
    set(_farm 2)
  elseif(CORAL_COMPUTERFARM STREQUAL "Lyon")
    set(_farm 3)
  elseif(CORAL_COMPUTERFARM STREQUAL "Online")
    set(_farm 4)
  endif(CORAL_COMPUTERFARM STREQUAL "GridKA")
  add_custom_command(OUTPUT ${opt_file}
                     COMMAND ${CMAKE_COMMAND} -DSED_SCRIPT=${opt_file}.sed
                                              -DTARGET=${opt_file}
                                              -DSOURCE=${source}
                                              -P ${CMAKE_SOURCE_DIR}/cmake/Modules/ProcessOptionFileSed.cmake
                     COMMAND ${CMAKE_C_COMPILER} -E -C -traditional-cpp
                                 -DC2OPT_COMPUTERFARM=${_farm}
                                 -DC2OPT_FLAG=0
                                 ${ARGN}
                                 ${CMAKE_CURRENT_SOURCE_DIR}/${source}
                           | sed -f ${opt_file}.sed
                           > ${CMAKE_CURRENT_BINARY_DIR}/${opt_file}
                     COMMAND ${CMAKE_COMMAND} -E remove -f ${opt_file}.sed
                     DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${source}
                     VERBATIM)
endmacro(PROCESS_OPTION_FILE)

macro(PROCESS_OPTION_FILES)
  set(i 1)
  set(_expr)
  set(_files)

  set(_state "")
  set(_files)
  set(_patterns)
  set(_extra_defines)
  foreach(_entry ${ARGN})
    if(${_entry} STREQUAL "EXTRA_DEFINES")
      set(_state "EXTRA_DEFINES")
    elseif(${_entry} STREQUAL "PATTERN")
      set(_state "PATTERN")
    else(${_entry} STREQUAL "EXTRA_DEFINES")
      if("${_state}" STREQUAL "EXTRA_DEFINES")
        list(APPEND _extra_defines ${_entry})
      elseif("${_state}" STREQUAL "PATTERN")
        list(APPEND _patterns ${_entry})
        list(LENGTH _patterns _length)
        if(${_length} GREATER 1)
          message(FATAL_ERROR "More than one pattern given.")
        endif(${_length} GREATER 1)
      else("${_state}" STREQUAL "EXTRA_DEFINES")
        list(APPEND _files ${_entry})
      endif("${_state}" STREQUAL "EXTRA_DEFINES")
    endif(${_entry} STREQUAL "EXTRA_DEFINES")
  endforeach(_entry ${ARGN})

  string(REGEX REPLACE ":.*" "" _pattern ${_patterns})
  string(REGEX REPLACE ".*:" "" _replacement ${_patterns})

  set(_extra_define)
  foreach(_entry ${_extra_defines})
    set(_extra_define ${_extra_define} -D${_entry})
  endforeach(_entry ${_extra_defines})

  foreach(_entry ${_files})
    string(REGEX REPLACE ${_pattern} ${_replacement} _orig ${_entry})
    process_option_file(${_entry} ${_orig} ${_extra_define})
  endforeach(_entry ${_files})
endmacro(PROCESS_OPTION_FILES)

