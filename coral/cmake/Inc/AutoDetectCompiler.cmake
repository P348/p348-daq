if(NOT AUTODETECTCOMPILER)
set(AUTODETECTCOMPILER 1)

# Avoid predefined rpath in C++ compiler.. (e.g. ROOT do this by default)
message(STATUS "All C++ RPATH options are disabled.. (linkage at runtime only)")
SET(CMAKE_SKIP_RPATH TRUE)
SET(CMAKE_INSTALL_RPATH "")

if(NOT CMAKE_STANDARD_CXX MATCHES "NO" AND NOT CMAKE_STANDARD_CXX MATCHES "OFF")

# Detect compiler
unset(COMPILER_SUPPORTS_CXX98 CACHE)
unset(COMPILER_SUPPORTS_CXX11 CACHE)
unset(COMPILER_SUPPORTS_CXX14 CACHE)
unset(COMPILER_SUPPORTS_CXX1Y CACHE)
unset(COMPILER_SUPPORTS_CXX17 CACHE)

if(CXX17)
    if(${CMAKE_VERSION} VERSION_LESS 3.8.0)
        message(FATAL_ERROR "CXX17 flag cannot be enabled with cmake version < 3.8.0")
    else()
        message(STATUS "CXX17 flag is enabled")
        set(CXX17 1)
    endif()
elseif(CXX14)
    message("-- CXX14 flag is enabled (use -DCXX14=\"\" to disable)")
    set(CXX14 1)
elseif(CXX11)
    message("-- CXX11 flag is enabled (use -DCXX11=\"\" to disable)")
    set(CXX11 1)
elseif(CXX1Y)
    message("-- CXX1Y flag is enabled (use -DCXX1Y=\"\" to disable)")
    set(CXX1Y 1)
elseif(CXX98)
    message("-- CXX98 flag is enabled (use -DCXX98=\"\" to disable)")
    set(CXX98 1)
else()
        include(CheckCXXCompilerFlag)
        message("-- CXX Standard Auto-Detection..")

        if(${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
	        if(NOT ${CMAKE_VERSION} VERSION_LESS 3.8.0)
	                CHECK_CXX_COMPILER_FLAG("-std=gnu++17" COMPILER_SUPPORTS_CXX17)
		endif()
                if(NOT COMPILER_SUPPORTS_CXX17)
                  CHECK_CXX_COMPILER_FLAG("-std=gnu++14" COMPILER_SUPPORTS_CXX14)
                  if(NOT COMPILER_SUPPORTS_CXX14)
                    CHECK_CXX_COMPILER_FLAG("-std=gnu++11" COMPILER_SUPPORTS_CXX11)
                    if(NOT COMPILER_SUPPORTS_CXX11)
                      CHECK_CXX_COMPILER_FLAG("-std=gnu++98" COMPILER_SUPPORTS_CXX98)
                    endif()
	          endif()
                endif()
        else()
		if(NOT ${CMAKE_VERSION} VERSION_LESS 3.8.0)
	                CHECK_CXX_COMPILER_FLAG("-std=c++17" COMPILER_SUPPORTS_CXX17)
		endif()
                if(NOT COMPILER_SUPPORTS_CXX17)
                  CHECK_CXX_COMPILER_FLAG("-std=c++14" COMPILER_SUPPORTS_CXX14)
       	          if(NOT COMPILER_SUPPORTS_CXX14)
               	    CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
                    if(NOT COMPILER_SUPPORTS_CXX11)
                      CHECK_CXX_COMPILER_FLAG("-std=c++98" COMPILER_SUPPORTS_CXX98)
                    endif()
                  endif()
                endif()
        endif()
endif()

if(COMPILER_SUPPORTS_CXX17 OR CXX17)

    message("-- CXX17 standard is set")
    set(CMAKE_CXX_STANDARD 17)
    if(${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++17")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
    endif()

elseif(COMPILER_SUPPORTS_CXX14 OR CXX14)

    message("-- CXX14 standard is set")
    set(CMAKE_CXX_STANDARD 14)
    if(${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++14")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
    endif()

elseif(COMPILER_SUPPORTS_CXX11 OR CXX11)

    message("-- CXX11 standard is set")
    set(CMAKE_CXX_STANDARD 11)
    if(${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    endif()

elseif(CXX1Y)

    message("-- CXX1Y standard is set")
    set(CMAKE_CXX_STANDARD 1Y)
    if(${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++1y")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y")
    endif()

elseif(COMPILER_SUPPORTS_CXX98 OR CXX98)

    message("-- CXX98 standard is set")
        set(CMAKE_CXX_STANDARD 98)
    if(${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++98")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++98")
    endif()
else()
        message(STATUS "No standard compatible support found for ${CMAKE_CXX_COMPILER}")
endif()

else()
        message(STATUS "Compiler standard support disabled manually (use -DCMAKE_STANDARD_CXX=\"ON\" to enable)")
endif()

add_compile_options(-m64)
add_compile_options(-fPIC)
add_compile_options(-pthread)

#
# Fortran CXX detection
#
enable_language(Fortran)
get_filename_component (Fortran_COMPILER_NAME ${CMAKE_Fortran_COMPILER} NAME)

macro(add_fortran_compiler_flag _name _flag)
  include(CheckFortranCompilerFlag)
  check_fortran_compiler_flag("${_flag}" Fortran_HAVE_${_name})
  if(Fortran_HAVE_${_name})
    set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${_flag}")
  endif()
endmacro(add_fortran_compiler_flag _name _flag)

# flags for Fortran compiler
add_fortran_compiler_flag(PTHREAD                -pthread)
add_fortran_compiler_flag(FORMAT_FIXED_FLAG      -ffixed-form)
add_fortran_compiler_flag(NO_AUTOMATIC           -fno-automatic)
add_fortran_compiler_flag(FIXED_LINE_LENGTH_NONE -ffixed-line-length-none)

foreach(LIB ${CMAKE_Fortran_IMPLICIT_LINK_LIBRARIES})
 if(LIB MATCHES "gfortran")
	set(HAS_GFORTRAN 1)
 endif()
endforeach()

if(HAS_GFORTRAN)
	message(STATUS "Recent fortran libraries found.. -DgFortran")
        add_definitions(-DgFortran)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DgFortran")
else()
	message(WARNING "Old fortran libraries found.. -Df2cFortran")
        add_definitions(-Df2cFortran)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Df2cFortran")
endif()

add_definitions(-m64)


include(CheckTypeSize)
check_type_size         ("char     "    SIZEOF_CHAR)
check_type_size         ("short    "    SIZEOF_SHORT)
check_type_size         ("int      "    SIZEOF_INT)
check_type_size         ("long     "    SIZEOF_LONG)
check_type_size         ("long long"    SIZEOF_LONG_LONG)

endif()
