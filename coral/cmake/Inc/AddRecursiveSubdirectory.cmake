#########################################################################################

macro(add_recursive_subdirectory curdir)
  FILE(GLOB children ${curdir}/*)
  FOREACH(child ${children})
        add_subdirectory(${child})
  ENDFOREACH()
endmacro()

MACRO(include_directories_recursive)
    FILE(GLOB_RECURSE new_list
          "*.hh" "*.h"
    )
    SET(dir_list "")
    FOREACH(file_path ${new_list})
        GET_FILENAME_COMPONENT(dir_path ${file_path} PATH)
        SET(dir_list ${dir_list} ${dir_path})
    ENDFOREACH()
    LIST(REMOVE_DUPLICATES dir_list)
    include_directories(${dir_list})
ENDMACRO()
