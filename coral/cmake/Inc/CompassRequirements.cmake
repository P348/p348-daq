# Set VERBOSITY attributs
if(VERBOSE MATCHES "0")
	message(STATUS "${Esc}[33mMinimum verbosity set (hide built messages).${Esc}[m You can also use -DVERBOSE=1 or 2")
	set_property(GLOBAL PROPERTY TARGET_MESSAGES "OFF")
	set(CMAKE_INSTALL_MESSAGE "LAZY")
elseif(NOT VERBOSE OR VERBOSE MATCHES "1")
	message(STATUS "${Esc}[33mStandard verbosity set (display built messages, lazy cmake).${Esc}[m You can also use -DVERBOSE=0 or 2")
	set_property(GLOBAL PROPERTY TARGET_MESSAGES "ON")
	set(CMAKE_INSTALL_MESSAGE "LAZY")
elseif(VERBOSE MATCHES "2")
	message(STATUS "${Esc}[33mMaximum verbosity set (display all cmake messages).${Esc}[m You can also use -DVERBOSE=0 or 1")
	set_property(GLOBAL PROPERTY TARGET_MESSAGES "ON")
	set(CMAKE_INSTALL_MESSAGE "NORMAL")
else()
	message(FATAL_ERROR "Wrong verbosity variable set. Please use -DVERBOSE={0,1,2}")
endif()

# Add "make debug" command to only compile necessary files
ADD_CUSTOM_TARGET(debug
  COMMAND @$(MAKE) C_FLAGS='-g -O0 -fPIC' CXX_FLAGS='-g -O0 -fPIC' -sC $(CMAKE_BINARY_DIR)
  COMMENT "Enable debug flag for source files not already compiled"
  )

# Default installation directory
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
        set (CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}" CACHE PATH "default install path" FORCE )
endif()

# Shared library options
if(SHARED MATCHES "YES")
	message(STATUS "Shared libraries will be compiled")
	set(BUILD_SHARED_LIBS 1)
else()
	message(STATUS "Static libraries will be compiled")
	set(BUILD_SHARED_LIBS 0)
endif()

# Do not compile all executables
if(ALL MATCHES "YES" OR ALL MATCHES "ON" OR ALL MATCHES "1")
	set(ALL 1)
else()
	set(ALL 0)
endif()

if(ALL)
	message(STATUS "Full distribution compilation including all executables and tests.")
else()
	message(STATUS "${Esc}[33mPartial distribution compilation..${Esc}[m Please use -DALL=\"ON\" to include all executables and tests")
endif()

#
# Environment detection
#

execute_process(COMMAND uname -s OUTPUT_VARIABLE OS OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "Operating system: \"${OS}\"")

configure_file( ${CMAKE_SOURCE_DIR}/cmake/Templates/setup.sh.in ${CMAKE_BINARY_DIR}/setup.sh)
install(FILES ${CMAKE_BINARY_DIR}/setup.sh DESTINATION .
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
configure_file( ${CMAKE_SOURCE_DIR}/cmake/Templates/setup.csh.in ${CMAKE_BINARY_DIR}/setup.csh)
install(FILES ${CMAKE_BINARY_DIR}/setup.csh DESTINATION .
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

set(EXECUTABLE_OUTPUT_PATH "${CMAKE_BINARY_DIR}/bin")
set(ARCHIVE_OUTPUT_PATH    "${CMAKE_BINARY_DIR}/lib/${OS}")
set(LIBRARY_OUTPUT_PATH    "${CMAKE_BINARY_DIR}/lib/${OS}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib/${OS}")
set(CMAKE_INSTALL_LIBDIR ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

#
# Prepare some config files
#
configure_file( ${CMAKE_SOURCE_DIR}/cmake/Templates/coral_config.h.in ${CMAKE_SOURCE_DIR}/src/coral_config.h)
include_directories("${CMAKE_CURRENT_SOURCE_DIR}") # Get the coral_config.h in the header list

execute_process (COMMAND unlink ${CMAKE_SOURCE_DIR}/src/DaqDataDecoding/DaqDataDecoding ERROR_QUIET)
execute_process (COMMAND ln -fs ${CMAKE_SOURCE_DIR}/src/DaqDataDecoding/src ${CMAKE_SOURCE_DIR}/src/DaqDataDecoding/DaqDataDecoding ERROR_QUIET)
configure_file          (${CMAKE_SOURCE_DIR}/src/DaqDataDecoding/src/config.h.cmake ${CMAKE_SOURCE_DIR}/src/DaqDataDecoding/src/config.h)

execute_process (COMMAND unlink ${CMAKE_SOURCE_DIR}/src/calorim/Reco/Reco ERROR_QUIET)
execute_process (COMMAND ln -fs ${CMAKE_SOURCE_DIR}/src/calorim/Reco/src ${CMAKE_SOURCE_DIR}/src/calorim/Reco/Reco ERROR_QUIET)
configure_file( ${CMAKE_SOURCE_DIR}/src/calorim/Reco/src/Reco_config.h.in ${CMAKE_SOURCE_DIR}/src/calorim/Reco/src/Reco_config.h)

#
# Prepare CoralCompilationDate
#
execute_process(COMMAND git rev-parse HEAD
                WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} OUTPUT_VARIABLE REVISION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND stat -c %y ${CMAKE_SOURCE_DIR}/.git/HEAD
                WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} OUTPUT_VARIABLE GET_PULL_DATE OUTPUT_STRIP_TRAILING_WHITESPACE)

string(REPLACE " " ";" PULL_LIST ${GET_PULL_DATE})
list(GET PULL_LIST 0 PULLDATE)
list(GET PULL_LIST 1 PULLTIME)
list(GET PULL_LIST 2 PULLGMT)

set(PULLTIME "${PULLTIME} ${PULLGMT}")

string(TIMESTAMP TODAY "%Y%m%d")
set(CORAL_COMPILATION_DATE ${TODAY})
set(CORAL_GIT_PULL_DATE    ${PULLDATE})
set(CORAL_GIT_PULL_TIME    ${PULLTIME})
set(CORAL_GIT_HEAD_SHA1    ${REVISION})

configure_file(${CMAKE_SOURCE_DIR}/cmake/Templates/CoralCompilationDate.h.in ${CMAKE_SOURCE_DIR}/src/CoralCompilationDate.h)

#
# Additional external libraries
#
if(USE_TGEANT)
    execute_process (COMMAND unlink ${CMAKE_SOURCE_DIR}/src/evmc/includeTGEANT ERROR_QUIET)
    execute_process (COMMAND ln -fs ${TGEANT_INCLUDE_DIR}/ ${CMAKE_SOURCE_DIR}/src/evmc/includeTGEANT ERROR_QUIET)

    execute_process(COMMAND cat ${TGEANT}/share/xercesPath WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} OUTPUT_VARIABLE XERCESC_HOME OUTPUT_STRIP_TRAILING_WHITESPACE)
    link_directories(${XERCESC_HOME}/lib/)
    include_directories(${XERCESC_HOME}/include)
endif()

#
# Coral LIBRARIES
#
list(APPEND CORAL_LIBRARIES CsBase CsBeam)
list(APPEND CORAL_LIBRARIES DaqDataDecoding FileDB)
list(APPEND CORAL_LIBRARIES CsObjStore CsEvent CsEvmc CsGeom CsGEM CsMumega)
list(APPEND CORAL_LIBRARIES CsG3CallFile CsHist CsRich1 CsRCEvdis CsTrack Trafdic)
list(APPEND CORAL_LIBRARIES CsSpacePoint CsTrigger CsUtils CsVertex VrtKalman VrtRoland)
list(APPEND CORAL_LIBRARIES CsCalorim Reco)

set(USE_ObjectsCounter 1)

if(USE_MySQL)
    list(APPEND CORAL_LIBRARIES "MySQLDB" "CsPPI" "z")
    list(APPEND CORAL_THIRD_PARTY_LIBRARIES mysqlclient)
endif()

if(USE_NewEDIS)
    list(APPEND CORAL_LIBRARIES "CsEvdis")
endif()

if(USE_MN)
  list(APPEND CORAL_LIBRARIES "CsUseMN")
endif()

if(USE_ORACLE)
  list(APPEND CORAL_LIBRARIES "CsOraStore")
endif()

if(PHAST_FOUND)
    add_definitions(-DPHASTinCORAL -DCORAL_DATE=${CORAL_DATE})

    if(NOT USE_NewEDIS)
        message(STATUS "Cannot compile the NEW Event Display. ROOT Eve library is missing..")
    elseif(NOT WITH_GRAPH)
        message(STATUS "Cannot compile the NEW Event Display. Phast libGraph.so is missing..")
    else()
	add_definitions(-DWITH_GRAPH)
    endif()
endif()

set(CORAL_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib/${OS})
set(CORAL_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include)

list(APPEND CORAL_THIRD_PARTY_LIBRARIES gfortran X11 expat crypt)

list(APPEND CORAL_THIRD_PARTY_LIBRARIES ${CERNLIB_LIBRARIES})
foreach(LIB ${ROOT_LIBRARIES})
    get_filename_component(LIB ${LIB} NAME_WE)
    string(REGEX REPLACE "^lib" "" LIB ${LIB})
    list(APPEND CORAL_THIRD_PARTY_LIBRARIES ${LIB})
endforeach()
if(CLHEP_FOUND)
    list(APPEND CORAL_THIRD_PARTY_LIBRARIES "CLHEP")
endif()

list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${MYSQL_LIB_DIR})
list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${PHAST_LIBRARY_DIR}  )
list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${CLHEP_LIBRARY_DIR}  )
list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${CERNLIB_LIBRARY_DIR})
list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${ROOT_LIBRARY_DIR})
list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${XROOTD_LIB_DIR})

list(APPEND CORAL_THIRD_PARTY_INCLUDE_DIR ${MYSQL_INCLUDE_DIR} )
list(APPEND CORAL_THIRD_PARTY_INCLUDE_DIR ${PHAST_INCLUDE_DIR}   )
list(APPEND CORAL_THIRD_PARTY_INCLUDE_DIR ${CLHEP_INCLUDE_DIRS}  )
list(APPEND CORAL_THIRD_PARTY_INCLUDE_DIR ${CERNLIB_INCLUDE_DIRS})
list(APPEND CORAL_THIRD_PARTY_INCLUDE_DIR ${XROOTD_INCLUDE_DIR})

if(USE_TGEANT)
    list(APPEND CORAL_THIRD_PARTY_LIBRARIES xerces-c)
    foreach(LIB ${TGEANT_LIBRARIES})
        get_filename_component(LIB ${LIB} NAME_WE)
        string(REGEX REPLACE "^lib" "" LIB ${LIB})
        list(APPEND CORAL_THIRD_PARTY_LIBRARIES ${LIB})
    endforeach()

    list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${TGEANT_LIBRARY_DIR})
    list(APPEND CORAL_THIRD_PARTY_LIBRARY_DIR ${XERCESC_HOME}/lib/ )

    list(APPEND CORAL_THIRD_PARTY_INCLUDE_DIR ${TGEANT_INCLUDE_DIR}   )
    list(APPEND CORAL_THIRD_PARTY_INCLUDE_DIR ${XERCESC_HOME}/include/)
endif()

#
# Prepare variable for the Include.mk
#
foreach(LIB ${CORAL_LIBRARIES})
    set(PHAST_CORAL_LIBRARIES "${PHAST_CORAL_LIBRARIES} -l${LIB}")
endforeach()

foreach(LIB ${CORAL_THIRD_PARTY_LIBRARIES})
    set(PHAST_CORAL_THIRD_PARTY_LIBRARIES "${PHAST_CORAL_THIRD_PARTY_LIBRARIES} -l${LIB}")
endforeach()

foreach(INC ${CORAL_THIRD_PARTY_INCLUDE_DIR})
    set(PHAST_CORAL_THIRD_PARTY_INCLUDE_DIR "${PHAST_CORAL_THIRD_PARTY_INCLUDE_DIR} -I${INC}")
endforeach()

foreach(LIB ${CORAL_THIRD_PARTY_LIBRARY_DIR})
    set(PHAST_CORAL_THIRD_PARTY_LIBRARY_DIR "${PHAST_CORAL_THIRD_PARTY_LIBRARY_DIR} -L${LIB}")
endforeach()
