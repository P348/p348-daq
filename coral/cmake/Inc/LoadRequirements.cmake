#
# Load THREAD
#
find_package(Threads REQUIRED)

#
# Loading ROOT
#
find_package(ROOT REQUIRED Minuit Gdml Geom)
find_package(ROOT COMPONENTS Eve)

include(${ROOT_USE_FILE})
include_directories(${ROOT_INCLUDE_DIR})
link_directories(${ROOT_LIBRARY_DIR})

if(ROOT_Eve_LIBRARY)
    list(APPEND ROOT_LIBRARIES "${ROOT_Eve_LIBRARY}")
    set(USE_NewEDIS 1)
else()
    message(STATUS "ROOT Eve library missing.. Cannot compile the NEW Event Display")
    set(USE_NewEDIS 0)
endif()

list(APPEND ROOT_LIBRARIES "${ROOT_LIBRARY_DIR}/libGui.so" "${ROOT_LIBRARY_DIR}/libMinuit.so" "${ROOT_LIBRARY_DIR}/libGeom.so")

set(USE_ROOT 1)

if(${OS} MATCHES "Linux")
	add_definitions(-Dlinux)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Dlinux")
endif()

#
# CERNLIB
#
find_package(CERNLIB REQUIRED)
if(CERNLIB_FOUND)
    # include_directories(${CERNLIB_INCLUDE_DIRS}) # @meyerma: Better to use cfortran.h from ROOT by default
    link_directories(${CERNLIB_LIBRARY_DIR})
    set(CERNLIB_LIBRARIES graflib grafX11 mathlib pawlib packlib kernlib)
    set(USE_CERN_LIBRARY 1) # @meyerma: CANNOT BE DISABLED.. Coral relies on many CERNLIB functions.. :-(

    # @meyerma: Options enabled by default, but to be improved..
    # At this stage, I tried to disable ZEBRA,HIGZ,HBOOK adding some flags; by default, all are at 1 (backward compatibility)
    # ZEBRA,HIGZ,HBOOK => STATUS
    #     0,   0,    0 => OK
    #     0,   1,    0 => ZOMBIE MODE
    #     0,   0,    1 => OK
    #     0,   1,    1 => OK
    #     1,   0,    0 => ZOMBIE MODE
    #     1,   0,    1 => seg.fault.. (when using SHARED LIBRARIES)
    #     1,   1,    0 => ZOMBIE MODE
    #     1,   1,    1 => OK

    set(USE_HIGZ 1)
    if(HIGZ MATCHES "NO")
	set(USE_HIGZ  0)
    endif()

    set(USE_ZEBRA 1)
    if(ZEBRA MATCHES "NO")
        set(USE_ZEBRA 0)
    endif()

    set(USE_HBOOK 1)
    if(HBOOK MATCHES "NO")
        message(STATUS "${Esc}[33mHBOOK library is disabled.. ZEBRA and HIGZ are also turned off to prevent zombie mode${Esc}[m")
        set(USE_HBOOK 0)
        set(USE_ZEBRA 0)
        set(USE_HIGZ 0)
    else()
	    message(STATUS "HBOOK library will be used..")

	    if(USE_ZEBRA)
	    message(STATUS "ZEBRA library will be used..")
	    else()
	    message(STATUS "${Esc}[33mZEBRA library is disabled..${Esc}[m")
	    endif()
	    if(USE_HIGZ)
	    message(STATUS "HIGZ library will be used..")
	    else()
	    message(STATUS "${Esc}[33mHIGZ library is disabled..${Esc}[m")
	    endif()
    endif()

    if(BUILD_SHARED_LIBS AND USE_HBOOK AND USE_ZEBRA AND NOT USE_HIGZ)
	message(STATUS "${Esc}[33mWarning: Possible segmentation fault, when using the shared librairies option with HBOOK, ZEBRA but HIGZ (Possible linking issue)${Esc}[m")
    endif()

endif()

#
# Loading Geant4
#
find_package(Geant4 QUIET)
if(Geant4_FOUND)

    include(${Geant4_USE_FILE})
    set(USE_GEANT4 1)
else()
    set(USE_GEANT4 0)
endif()

#
# Loading CLHEP
#
if(Geant4_system_clhep_FOUND)

	message(STATUS "Geant4 system clhep found.. ${CLHEP_DIR}")
	if(NOT CLHEP_DIR)
	message(FATAL_ERROR "CLHEP_DIR is not defined in Geant4 configuration.. unexpected error")
	endif()
	# CLHEP VARIABLES SHOULD ALREADY BE DEFINED

elseif(NOT Geant4_FOUND OR Geant4_builtin_clhep_FOUND)

    if(Geant4_FOUND)
	    message(STATUS "Geant4 builtin version is not enough to run with coral (e.g. missing CLHEP/Matrix)")

	    string(ASCII 27 Esc)
	    message(STATUS "${Esc}[33mGeant4 built with external CLHEP would be better..${Esc}[m")
	    message("   ${Esc}[33m(Possible .tbss mismatches non-TLS definition between libG4clhep.so and libCLHEP.so at Coral compilation step, please check out the G4 clhep version)${Esc}[m")
    else()
            message(STATUS "Geant4 not found.. Looking for external CLHEP..")
    endif()

    find_package(CLHEP REQUIRED)
endif()

if(CLHEP_DIR)
        get_filename_component(CLHEP_LIBRARY_DIR ${CLHEP_DIR} DIRECTORY)
endif()

include_directories(${CLHEP_INCLUDE_DIR})
link_directories(${CLHEP_LIBRARY_DIR})

#
# Loading Qt4,5
#

# Disabled for the moment.. Calo Qt interface has to be reviewed.. (Qt4,Qt5)
# Isse with Reco library and Qt MOC wrapper..
unset(CORAL_DISABLE_QT  CACHE)
set(CORAL_DISABLE_QT 1 )

if(CORAL_DISABLE_QT)
    message(STATUS "${Esc}[33mQt has been disabled..${Esc}[m")
    set(USE_Qt 0)
else()
	if(NOT Qt4_FOUND AND NOT Qt5_FOUND)
	        find_package(Qt5 COMPONENTS Core Gui Widgets Xml)
	        if(Qt5_FOUND)
	                message(STATUS "Interface Qt5 found.. ${Qt5_DIR}")
			set(USE_Qt 1)
	        else()
	                find_package(Qt4)
	                if(Qt4_FOUND)
	                        message(STATUS "Interface Qt4 found.. ${Qt4_DIR}")
				set(USE_Qt 1)

	                        include(${QT_USE_FILE})
	                        include_directories(${QT_INCLUDES})
	                        add_definitions(${QT_DEFINITIONS})
	                else()
	                        message(STATUS "Interface Qt4 not found.. Graphics will be disabled..")
				set(USE_Qt 0)
	                endif()
	        endif()
	endif()
endif()

#
# EXPAT
#
find_package(EXPAT REQUIRED)
link_directories(${EXPAT_LIBRARY_DIR})
if(EXPAT_FOUND)
    set(USE_EXPAT 1)
else()
    set(USE_EXPAT 0)
endif()

#
# DATE
#
find_package(DATE)
if(DATE_FOUND)
    set(USE_DATE 1)
else()
    set(USE_DATE 0)
endif()

#
# RFIO
#
if(RFIO MATCHES "ON")
	find_package(RFIO)
	if(RFIO_FOUND)
	    include_directories(${RFIO_INCLUDE_DIR})
	    link_directories(${RFIO_LIBRARY_DIR})
	    set(USE_RFIO 1)
	else()
	    set(USE_RFIO 0)
	endif()
else()
    message(STATUS "${Esc}[33mRFIO manually disabled.. Use -DRFIO='ON'${Esc}[m")
    set(USE_RFIO 0)
endif()


#
# POPt and DL libraries
#
find_package(POPT)
find_package(DL)


#
# Databases
#
find_package(MySQL REQUIRED)
if(MYSQL_FOUND)
    include_directories(${MYSQL_INCLUDE_DIR})
    link_directories(${MYSQL_LIBRARY_DIR})
    set(USE_MySQL 1)
else()
    set(USE_MySQL 0)
endif()

find_package(ORACLE)
if(ORACLE_FOUND)
    include_directories(${ORACLE_INCLUDE_DIR})
    link_directories(${ORACLE_LIBRARY_DIR})
    set(USE_ORACLE 1)
else()
    set(USE_ORACLE 0)
endif()

find_package(SQLite3)
if(SQLite3_FOUND)
    set(USE_SQLITE 1)
endif()

find_package(XRootD)
if(XROOTD_FOUND)

    # Option needed to read files using XROOTD protocol (e.g. root://castorpublic.cern.ch)
    add_compile_options( -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -D_REENTRANT)
    set(CMAKE_C_FLAGS   "-D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -D_REENTRANT ${CMAKE_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "-D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -D_REENTRANT ${CMAKE_CXX_FLAGS}")

    if(XROOTD_PRELOAD)
       message(STATUS "XROOTD preload found: ${XROOTD_PRELOAD}")
       set(XROOTD_PRELOAD "${XROOTD_PRELOAD}")
    else()
       message(STATUS "No XROOTD preload found..")
       set(XROOTD_PRELOAD "")
    endif()
    set(USE_XROOTD 1)
else()
    set(USE_XROOTD 0)
    set(XROOTD_PRELOAD "")
endif()

#
# PHAST
#
if(CORAL_DISABLE_PHAST MATCHES "YES" OR CORAL_DISABLE_PHAST MATCHES "ON" OR CORAL_DISABLE_PHAST MATCHES "1")
	message(STATUS "${Esc}[33mPHAST support manually disabled..${Esc}[m")
	set(USE_PHAST 0)
else()
	find_package(PHAST)
	if(PHAST_FOUND)
	        include_directories(${PHAST_INCLUDE_DIR})
		#link_directories(${PHAST_LIBRARY_DIR})
	    	set(USE_PHAST 1)
	else()
	    	set(USE_PHAST 0)
	endif()
endif()

#
# TGEANT
#
if(CORAL_DISABLE_TGEANT MATCHES "YES" OR CORAL_DISABLE_TGEANT MATCHES "ON" OR CORAL_DISABLE_TGEANT MATCHES "1")
    message(STATUS "${Esc}[33mTGEANT support manually disabled..${Esc}[m")
    set(USE_TGEANT 0)
else()
    find_package(TGEANT)
    if(TGEANT_FOUND)
        include_directories(${TGEANT_INCLUDE_DIR})
        link_directories(${TGEANT_LIBRARY_DIR})
        set(USE_TGEANT 1)
    else()
        set(USE_TGEANT 0)
    endif()

endif()

#
# UseMN
#
if(MN MATCHES "ON")
    message(STATUS "MN is enabled")
    set(USE_MN 1)
else()
    message(STATUS "${Esc}[33mMN manually disabled.. Use -DMN='ON'${Esc}[m")
    set(USE_MN 0)
endif()

#
# Minimum GCC version required
#
if (CMAKE_CXX_COMPILER_VERSION STRLESS 4.7)
    message( FATAL_ERROR "GCC has to be at least 4.7" )
endif()

if (ROOT_VERSION STRLESS 6.00/00 OR NOT ROOT_VERSION)
    message( FATAL_ERROR "ROOT has to be at least 5.00/00" )
endif()
