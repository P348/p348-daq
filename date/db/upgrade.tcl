#!/usr/bin/tclsh

# This script upgrades database structure
# upgrade  1  (02 aug 2005): MEMBANKS changed: ROLE_ROLE/ROLE_ID replaced by ROLE_NAME
# upgrade  6  (01 dec 2005): large modification. Added a DB version number.
# upgrade  7  (avr 2006): added DDL in/out identification tables
# upgrade  8  (21 avr 2006): added forceSplitter param
# upgrade  9  (2 may 2006): added EOD/SOD event types to event building rules

# Load mysqltcl
if { [exec arch ] == "i686" } { 
  lappend auto_path $env(DATE_ROOT)/local
}
if { [exec arch ] == "x86_64" } {
  lappend auto_path $env(DATE_ROOT)/local64
}
if [ catch {package require mysqltcl 3.0} ] {
  puts "Package mysqltcl required"
  exit 1
}

# Check that in db mode
if {"$env(DATE_DB_MYSQL)"!="TRUE"} {
  puts "Nothing to do, DATE_DB_MYSQL not TRUE"
  exit 0
}

# Connect db
set db_user $env(DATE_DB_MYSQL_USER)
set db_pwd $env(DATE_DB_MYSQL_PWD)
set db_host $env(DATE_DB_MYSQL_HOST)
set db_db $env(DATE_DB_MYSQL_DB)
if [ catch {set db [mysqlconnect -host $db_host -user $db_user -password $db_pwd -db $db_db]} ] {
  puts "Failed to connect to database"
  exit 2
}




######################
# Upgrade procedures #
######################


# upgrade #1 - 2 aug 2005
# update in membanks table: ROLE_ROLE/ROLE_ID replaced by ROLE_NAME
proc upgrade_1 {} {
  global db

  mysqlsel $db "CREATE TABLE IF NOT EXISTS MEMBANKS_NEW
      (
        ROLE_NAME CHAR (32),
        SUPPORT ENUM('undefined','ipc','bigphys','heap','physmem'),
        NAME CHAR (128),
        SIZE BIGINT,
        PATTERN SET(
    \"control\",\"readout\",\"readoutReadyFifo\",\"readoutFirstLevelVectors\",\"readoutSecondLevelVectors\",\"readoutDataPages\",\"edmReadyFifo\",\"hltAgent\",\"hltReadyFifo\",\"hltSecondLevelVectors\",\"hltDataPages\",\"eventBuilder\",\"eventBuilderReadyFifo\",\"eventBuilderDataPages\"),
        INDEX (ROLE_NAME),
        FOREIGN KEY (ROLE_NAME) REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT
      ) ENGINE = INNODB
  " -list

  set query "select ROLES.NAME,MEMBANKS.SUPPORT,MEMBANKS.NAME,MEMBANKS.SIZE,MEMBANKS.PATTERN from MEMBANKS,ROLES where ROLES.ID=MEMBANKS.ROLE_ID and ROLES.ROLE=MEMBANKS.ROLE_ROLE"
  foreach row [mysqlsel $db $query -list] {
    set query {"insert into MEMBANKS_NEW VALUES("}
    set i 0
    foreach item $row {
      set val [::mysql::escape $item]
      if {$i} {lappend query ","}
      lappend query "'$val'"
      incr i
    }
    lappend query ")"
    set query [join $query]
    mysqlsel $db $query -list
  }
  
  mysqlsel $db "drop table MEMBANKS" -list
  mysqlsel $db "rename table MEMBANKS_NEW to MEMBANKS" -list  
}


# upgrade #2 - 3 aug 2005
# move of files from DATE_SITE_CONFIG to db
proc upgrade_2 {} {
  global db
  global db_user
  global db_pwd
  global db_host
  global db_db
  global env
  
  # check some values
  if {[catch {puts "Check infoLogger variables: DATE_INFOLOGGER_MYSQL=$env(DATE_INFOLOGGER_MYSQL)"}]} {
    puts "Please type:\n  dateSetup \${DATE_SITE_CONFIG}/infoLogger.config\nand re-run this script" 
    exit 0
  }

  # create tables
  puts "create new tables"
  if {[catch {exec mysql -u $db_user --password=$db_pwd -h $db_host -D $db_db -B -N -e "source /date/db/db_create.sql"} err]} {
    puts "An error occured:\n$err\n\nTry to continue..."
  }
  

  # fill table
  puts "store variables in ENV table"
  mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DIM_DNS_NODE)\" WHERE NAME=\"DIM_DNS_NODE\""
  mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DAQ_ROOT_DOMAIN_NAME)\" WHERE NAME=\"DAQ_ROOT_DOMAIN_NAME\""

  mysqlsel $db "UPDATE ENV SET VALUE=\"TRUE\" WHERE NAME=\"DATE_MYSQL\""
  mysqlsel $db "UPDATE ENV SET VALUE=\"TRUE\" WHERE NAME=\"DATE_DB_MYSQL\""
  mysqlsel $db "UPDATE ENV SET VALUE=\"$db_user\" WHERE NAME=\"DATE_DB_MYSQL_USER\""
  mysqlsel $db "UPDATE ENV SET VALUE=\"$db_pwd\" WHERE NAME=\"DATE_DB_MYSQL_PWD\""
  mysqlsel $db "UPDATE ENV SET VALUE=\"$db_host\" WHERE NAME=\"DATE_DB_MYSQL_HOST\""
  mysqlsel $db "UPDATE ENV SET VALUE=\"$db_db\" WHERE NAME=\"DATE_DB_MYSQL_DB\""

  mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DATE_INFOLOGGER_LOGHOST)\" WHERE NAME=\"DATE_INFOLOGGER_LOGHOST\""
  mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DATE_INFOLOGGER_MYSQL)\" WHERE NAME=\"DATE_INFOLOGGER_MYSQL\""

  if {"$env(DATE_INFOLOGGER_MYSQL)" == "TRUE"} {
    mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DATE_INFOLOGGER_MYSQL_HOST)\" WHERE NAME=\"DATE_INFOLOGGER_MYSQL_HOST\""
    mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DATE_INFOLOGGER_MYSQL_USER)\" WHERE NAME=\"DATE_INFOLOGGER_MYSQL_USER\""
    mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DATE_INFOLOGGER_MYSQL_PWD)\" WHERE NAME=\"DATE_INFOLOGGER_MYSQL_PWD\""
    mysqlsel $db "UPDATE ENV SET VALUE=\"$env(DATE_INFOLOGGER_MYSQL_DB)\" WHERE NAME=\"DATE_INFOLOGGER_MYSQL_DB\""
  }

  # store files
  puts "store EOR/SOR command/files in FILES table"
  puts "looking in $env(DATE_SITE_CONFIG)"
  set res [exec /date/db/copyFileDB.tcl set -i -s -f SOR.commands -f EOR.commands -f SOR.files -f EOR.files]
  puts "$res"
  
  foreach d [glob -d $env(DATE_SITE) *] {
    if {![file isdirectory $d]} continue
    # filter usual entries
    if {[file tail $d]=="tmp"} continue
    if {[file tail $d]=="configurationFiles"} continue    
    if {[file tail $d]=="logFiles"} continue
    puts "looking in $d"
    set res [exec /date/db/copyFileDB.tcl set -i -s -f SOR.commands -f EOR.commands -f SOR.files -f EOR.files -h [file tail $d]]
    puts "$res"
  }

}


# upgrade #3 - 8 aug 2005
# added run number to database
proc upgrade_3 {} {
  global db
    
  # create table
  puts "create new table to store run number"
  mysqlexec $db "CREATE TABLE IF NOT EXISTS GLOBALS
(
NAME CHAR(32),
VALUE TEXT,
DESCRIPTION TEXT,
PRIMARY KEY(NAME)
) ENGINE = INNODB
comment = \"\";
"
 puts "create run number"
 mysqlexec $db "REPLACE INTO GLOBALS VALUES('Run Number','0','The last run number used');"

}


# upgrade #4 - 9 aug 2005
# membanks key files auto increment - no need to enter them
proc upgrade_4 {} {
  global db
    
  # change table definition
  puts "changing membanks definition: key files named automatically"  
  mysqlexec $db "alter table MEMBANKS add ID INT NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY(ID)"
  mysqlexec $db "update MEMBANKS set NAME=NULL where SUPPORT='ipc'"  
}




# upgrade #5
# move runControl parameters in BLOB files
proc upgrade_5 {} {
  global db
  source /date/db/libFileDB.tcl
  
  set names [mysqlsel $db "select distinct DAQ_TASK_NAME, CONFIGURATION_NAME from RC_DAQ_CONFIGURATION" -flatlist]
  set nNames [llength $names]
  set i 0
  while { $i < $nNames} {
     set it $i
     set in $it
     incr in +1
     set task [lindex $names $it]
     set name [lindex $names $in]
     puts "converting configuration $task $name"
     set data [mysqlsel $db "select SELECTION_DATA from RC_DAQ_CONFIGURATION where DAQ_TASK_NAME=\"${task}\" and CONFIGURATION_NAME=\"${name}\"" -list]  
     writeFileDB $db /runControl/config/$task/$name $data
     incr i +2
  }   
  set names [mysqlsel $db "select distinct DAQ_TASK_NAME, SET_NAME from RC_DAQ_PARAMS" -flatlist]
  set nNames [llength $names]
  set i 0
  while { $i < $nNames} {
     set it $i
     set in $it
     incr in +1
     set task [lindex $names $it]
     set name [lindex $names $in]
     puts "converting run parameters $task $name"
     set data [mysqlsel $db "select RP_DATA from RC_DAQ_PARAMS where DAQ_TASK_NAME=\"${task}\" and SET_NAME=\"${name}\"" -list]  
     set newData {}
     foreach item $data {
        set newItem [lindex $item 0]
        lappend newData "$newItem"
     }
     writeFileDB $db /runControl/runp/$task/$name $newData
     incr i +2
  }   
  set names [mysqlsel $db "select distinct DAQ_TASK_NAME, SET_NAME from RC_DAQ_RUN_OPTIONS" -flatlist]
  set nNames [llength $names]
  set i 0
  while { $i < $nNames} {
     set it $i
     set in $it
     incr in +1
     set task [lindex $names $it]
     set name [lindex $names $in]
     puts "converting run options $task $name"
     set data [mysqlsel $db "select RO_DATA from RC_DAQ_RUN_OPTIONS where DAQ_TASK_NAME=\"${task}\" and SET_NAME=\"${name}\"" -list]  
     set newData {}
     foreach item $data {
        set newItem [lindex $item 0]
        lappend newData "$newItem"
     }
     writeFileDB $db /runControl/ropt/$task/$name $newData
     incr i +2
  }   
  set names [mysqlsel $db "select distinct DAQ_TASK_NAME, SET_NAME from RC_DAQ_HI_OPTIONS" -flatlist]
  set nNames [llength $names]
  set i 0
  while { $i < $nNames} {
     set it $i
     set in $it
     incr in +1
     set task [lindex $names $it]
     set name [lindex $names $in]
     puts "converting HI options $task $name"
     set data [mysqlsel $db "select RO_DATA from RC_DAQ_HI_OPTIONS where DAQ_TASK_NAME=\"${task}\" and SET_NAME=\"${name}\"" -list]  
     set newData {}
     foreach item $data {
        set newItem [lindex $item 0]
        lappend newData "$newItem"
     }
     writeFileDB $db /runControl/hopt/$task/$name $newData
     incr i +2
  }  
  
  puts "destroy old tables"
  mysqlexec $db "DROP TABLE IF EXISTS RC_DAQ_CONFIGURATION"
  mysqlexec $db "DROP TABLE IF EXISTS RC_CUR_DAQ_CONFIGURATION"
  mysqlexec $db "DROP TABLE IF EXISTS RC_NEW_DAQ_CONFIGURATION"
  mysqlexec $db "DROP TABLE IF EXISTS RC_CUR_DAQ_CONF_SUMMARY"
  mysqlexec $db "DROP TABLE IF EXISTS RC_EXCLUDED_DET_DAQ_CONF"
  mysqlexec $db "DROP TABLE IF EXISTS RC_DAQ_PARAMS"
  mysqlexec $db "DROP TABLE IF EXISTS RC_CUR_DAQ_PARAMS"
  mysqlexec $db "DROP TABLE IF EXISTS RC_NEW_DAQ_PARAMS"
  mysqlexec $db "DROP TABLE IF EXISTS RC_DAQ_RUN_OPTIONS"
  mysqlexec $db "DROP TABLE IF EXISTS RC_CUR_DAQ_RUN_OPTIONS"
  mysqlexec $db "DROP TABLE IF EXISTS RC_NEW_DAQ_RUN_OPTIONS"
  mysqlexec $db "DROP TABLE IF EXISTS RC_DAQ_HI_OPTIONS"

}




# upgrade #6
# added Cole
# TRIGGERS: rename for MySQL 5 compatibility
# changed NAME to KEY_PATH in MEMBANKS
# added version number
proc upgrade_6 {} {
  global db
  global env
  
  puts "upgrade infoLogger database"
  set infologenv [exec "${env(DATE_DB_DIR)}/loadEnvDB.tcl" -C infoLogger]
  foreach {name eq value} $infologenv {
     set dbenv($name) $value
  }
  if {$dbenv(DATE_INFOLOGGER_MYSQL)=="TRUE"} {
    if [ catch {set dblog [mysqlconnect -host $dbenv(DATE_INFOLOGGER_MYSQL_HOST) -user $dbenv(DATE_INFOLOGGER_MYSQL_USER) -password $dbenv(DATE_INFOLOGGER_MYSQL_PWD) -db $dbenv(DATE_INFOLOGGER_MYSQL_DB)]} ] {
      puts "Failed to connect to infoLogger database"
    } else {
      set ltables [mysqlsel $dblog "show tables;" -list]
      foreach t $ltables {
        if {![string match -nocase "messages*" $t]} {continue}
        catch {mysqlexec $dblog "ALTER TABLE $t ADD COLUMN system varchar(20) AFTER username;"}
        catch {mysqlexec $dblog "ALTER TABLE $t ADD INDEX ix_system(system);"}
      }   
    }
  }
  
  puts "create table for equipment Cole"
  catch {mysqlexec $db "INSERT INTO EQUIP_TYPES VALUES('Cole','10','1','1',DEFAULT);"}
  mysqlexec $db "
    CREATE TABLE IF NOT EXISTS EQUIP_PARAM_Cole(
    LDC CHAR (32),
    EQUIPMENT_NAME CHAR(32),
    PRIMARY KEY(LDC,EQUIPMENT_NAME),
    INDEX(LDC),
    FOREIGN KEY(LDC) REFERENCES ROLES(NAME)
    ON UPDATE CASCADE ON DELETE CASCADE,
    ACTIVE BOOL,
    EqId SMALLINT DEFAULT 100
    ) ENGINE = INNODB
    comment = \"\";
  "
  
  puts "update db for MySQL 5 compatibility"
  catch {
  mysqlexec $db "alter table FILES modify PATH CHAR(255), drop PRIMARY KEY, add PRIMARY KEY(PATH(255),HOST(32));"
  mysqlexec $db "alter table EQUIP_TYPES change TRIGGER GENTRIGGER BOOL;"
  }
  mysqlexec $db "
  CREATE TABLE IF NOT EXISTS newTRIGGERS
    (
      TRIGGER_NAME     CHAR (32),
      DETECTOR    CHAR (32),
      INDEX(TRIGGER_NAME),
      INDEX(DETECTOR),
      PRIMARY KEY (TRIGGER_NAME,DETECTOR),
      FOREIGN KEY (TRIGGER_NAME)  REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT,
      FOREIGN KEY (DETECTOR) REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT
    )  ENGINE = INNODB
    "
   mysqlexec $db "insert into newTRIGGERS (select * from TRIGGERS);"
   mysqlexec $db "rename table TRIGGERS to oldTRIGGERS, newTRIGGERS TO TRIGGERS;"
   mysqlexec $db "drop table oldTRIGGERS;"

  puts "change NAME to KEY_PATH for Membanks"
  catch {
    mysqlexec $db "alter table MEMBANKS change NAME KEY_PATH CHAR (255);"
  }

  puts "upgrade database version number"
  mysqlexec $db "REPLACE INTO GLOBALS VALUES('DB version','v6','The version of the configuration database');"
}




# upgrade #7
proc upgrade_7 {} {

  # check DB version - must be v6
  global db
  set query "select VALUE from GLOBALS where NAME='DB version'"
  set res [mysqlsel $db $query -list]
  if {[llength $res]!=1} {
    puts "DB version not found - should be v6 : upgrade failed"
    return
  }
  set v [lindex $res 0]
  if {$v!="v6"} {
    puts "Wrong DB version '$v' - should be v6 : upgrade failed"
    return
  }

  puts "Add parameter to RorcData and RorcSplitter equipments"
  mysqlexec $db "alter table EQUIP_PARAM_RorcData add DDLin_id SMALLINT UNSIGNED"  
  mysqlexec $db "alter table EQUIP_PARAM_RorcSplitter add DDLout_id SMALLINT UNSIGNED"

  # create tables
  global db_user
  global db_pwd
  global db_host
  global db_db
  puts "Create new tables"
  if {[catch {exec mysql -u $db_user --password=$db_pwd -h $db_host -D $db_db -B -N -e "source /date/db/DDL_create.sql"} err]} {
    puts "An error occured:\n$err\n\nTry to continue..."
  }
  
  # change it (and also in db_create.sql and loadEnvDb.tcl)
  puts "upgrade database version number"
  mysqlexec $db "REPLACE INTO GLOBALS VALUES('DB version','v7','The version of the configuration database');"

}



# upgrade #8
proc upgrade_8 {} {

  # check DB version - must be v7
  global db
  set query "select VALUE from GLOBALS where NAME='DB version'"
  set res [mysqlsel $db $query -list]
  if {[llength $res]!=1} {
    puts "DB version not found - should be v7 : upgrade failed"
    return
  }
  set v [lindex $res 0]
  if {$v!="v7"} {
    puts "Wrong DB version '$v' - should be v7 : upgrade failed"
    return
  }

  puts "Add parameter to RorcSplitter equipments"
  mysqlexec $db "alter table EQUIP_PARAM_RorcSplitter add forceSplitter SMALLINT DEFAULT 0 after CtrlPtr"

 # change it (and also in db_create.sql and loadEnvDb.tcl)
  puts "upgrade database version number"
  mysqlexec $db "REPLACE INTO GLOBALS VALUES('DB version','v8','The version of the configuration database');"

}



# upgrade #9
proc upgrade_9 {} {

  # check DB version - must be v8
  global db
  set query "select VALUE from GLOBALS where NAME='DB version'"
  set res [mysqlsel $db $query -list]
  if {[llength $res]!=1} {
    puts "DB version not found - should be v8 : upgrade failed"
    return
  }
  set v [lindex $res 0]
  if {$v!="v8"} {
    puts "Wrong DB version '$v' - should be v8 : upgrade failed"
    return
  }

  puts "Add SOD/EOD event types to event building rules"
  mysqlexec $db "alter table EVENT_BUILDING_RULES modify EVENT_TYPE ENUM('StartOfRun','EndOfRun','StartOfRunFiles','EndOfRunFiles','StartOfBurst','EndOfBurst','Physics','Calibration','StartOfData','EndOfData')"
  mysqlexec $db "alter table EVENT_BUILDING_PATTERNS modify EVENT_TYPE ENUM('StartOfRun','EndOfRun','StartOfRunFiles','EndOfRunFiles','StartOfBurst','EndOfBurst','Physics','Calibration','StartOfData','EndOfData')"
  mysqlexec $db "insert into EVENT_BUILDING_RULES VALUES ('StartOfData',1,1,0,'Full Build'),('EndOfData',1,1,0,'Full Build')"


 # change it (and also in db_create.sql and loadEnvDb.tcl)
  puts "upgrade database version number"
  mysqlexec $db "REPLACE INTO GLOBALS VALUES('DB version','v9','The version of the configuration database');"

}


# next upgrade
# check DB version - must be vXX
# change it (and also in db_create.sql and loadEnvDb.tcl)
 


#############################
# End of upgrade procedures #
#############################




# Proceed with DB upgrades
foreach opt $argv {
  if {(($opt=="-h")||($opt=="-?")||($opt=="--help"))} {
    puts "Upgrades DB schema. Follow instructions in release notes."
  } else {
    if {[scan $opt "%d" i]==1} {
      puts "Proceeding with upgrade #$i"
      upgrade_$i
    }
  }
}

# Close database
::mysql::close $db
puts "Upgrade completed"
