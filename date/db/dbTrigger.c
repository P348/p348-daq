/*      dbTrigger.c
 *      ===========
 *
 * Module to implement DB access to the trigger database.
 *
 * Revision history:
 *  1.00  25 Aug 00  RD      First release
 *  1.01  29 Jul 05  RD      Added dbGetDetectorsInTriggerPattern
 *  1.02  31 Aug 05  RD      Added check in dbGetDetectorsInTriggerPattern
 *			     for undefined triggers
 */

#define VID "V 1.02"

#define DESCRIPTION "DATE trigger DB access"
#ifdef AIX
static
#endif
char dbTriggerIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
                      """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>

#include "dateDb.h"
#include "dbConfig.h"

#include "dbSql.h"

/* ------------------------------------------------------------------------- */
dbTriggerDescriptor *dbTriggersDb     = NULL;
int                  dbSizeTriggersDb = 0;

/* ------------------------------------------------------------------------- */
static int dbTriggerSyntaxError( const int lineNo, const char * const line ) {
  dbSetLastLine( lineNo, line );
  fprintf( stderr,
	   "DB load: syntax error handling DATE triggers configuration\n" );
  return DB_PARSE_ERROR;
} /* End of dbTriggerSyntaxError */

/* ------------------------------------------------------------------------- */
static int allocateNextTrigger() {
  dbSizeTriggersDb++;
  
  if ( (dbTriggersDb =
	  realloc( dbTriggersDb,
		   sizeof(dbTriggerDescriptor)*dbSizeTriggersDb )) == NULL ) {
    perror( "DB load: cannot allocate dbTriggersDb " );
    return DB_INTERNAL_ERROR;
  }
  
  dbTriggersDb[dbSizeTriggersDb-1].id = -1;
  ZERO_DETECTOR_PATTERN( dbTriggersDb[dbSizeTriggersDb-1].detectorPattern );
  
  return DB_LOAD_OK;
} /* End of allocateNextTrigger */

/* ------------------------------------------------------------------------- */
static int handleTriggers( struct dbSectionStruct *s ) {
  int l;
  int status;
  int c;

  for ( l = 0; l != s->numLines; l++ ) {
    dbTriggerDescriptor *n;
    int k;

    if ( (status = allocateNextTrigger()) != DB_LOAD_OK ) return status;
    
    n = &dbTriggersDb[dbSizeTriggersDb-1];
    
    if ( s->lines[l].tokens[0].keyword != NULL )
      return dbTriggerSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    if ( (c = dbRolesFind( s->lines[l].tokens[0].value,
			   dbRoleTriggerMask )) == -1 ) {
      fprintf( stderr,
	       "DB load: unknown trigger mask \"%s\"\
 in triggers configuration\n",
	       s->lines[l].tokens[0].value );
      return dbTriggerSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    }
    n->id = dbRolesDb[c].id;

    for ( c = 0, k = 1; s->lines[l].tokens[k].value != NULL; k++ ) {
      int d;
      
      if ( s->lines[l].tokens[k].keyword != NULL ) 
	return dbTriggerSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      if ( (d = dbRolesFind( s->lines[l].tokens[k].value,
			     dbRoleDetector)) == -1 ) {
	fprintf( stderr,
		 "DB load: unknown detector \"%s\"\
 in triggers configuration\n",
		 s->lines[l].tokens[k].value );
	return dbTriggerSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }
      SET_DETECTOR_IN_PATTERN( n->detectorPattern, dbRolesDb[d].id );
      c++;
    }
    if ( c == 0 )
      return dbTriggerSyntaxError(s->lines[l].lineNo, s->lines[l].line);
  }
  
  if ( (status = allocateNextTrigger()) != DB_LOAD_OK ) return status;
  dbSizeTriggersDb--;

  return DB_LOAD_OK;
} /* End of handleTriggers */

/* ------------------------------------------------------------------------- */
static int checkTriggers() {
  return DB_LOAD_OK;
} /* End of checkTriggers */

/* ------------------------------------------------------------------------- */
/* Load the trigger database. Return DB_LOAD_OK on success.                  */
int dbLoadTriggers() {
  int status;

  if ( dbTriggersDb != NULL ) return DB_LOAD_OK;
  
  /* Switch for SQL database */
  if (dbsql_is_configured()) {
    if ( dbsql_open()!=0 ) return DB_LOAD_ERROR;
    if ( (status = dbsql_load_triggers()) != DB_LOAD_OK ) return status;
    dbsql_close();
  } else {
  
  if ( (status = dbInitInput()) != DB_UNLOAD_OK ) return status;
  if ( (status = dbInput( dbsTable[ DB_TABLE_TRIGGER ].name )) != DB_LOAD_OK )
    return status;
  if ( (status = handleTriggers( &dbSections[DB_SECTION_TRIGGER_MASK] )) !=
       DB_LOAD_OK ) {
    dbUnloadTriggers();
    return status;
  }
  
  } /* end of Switch for SQL database */
  
  if ( (status = checkTriggers()) != DB_LOAD_OK ) {
    dbUnloadTriggers();
    return status;
  }
  return status;
} /* End of dbLoadTriggers */

/* ------------------------------------------------------------------------- */
/* Unload the TRIGGERS database */
int dbUnloadTriggers() {
  free( dbTriggersDb );
  dbTriggersDb = NULL;
  dbSizeTriggersDb = 0;

  return DB_UNLOAD_OK;
} /* End of dbUnloadTriggers */

/* ------------------------------------------------------------------------- */
int dbGetLdcsInTriggerPattern( const eventTriggerPatternType triggerPattern,
			             dbLdcPatternType        ldcPattern ) {
  dbLdcPatternType pat;
  int tt;
  int status;

  memset( ldcPattern, 0, DB_WORDS_IN_LDC_MASK*4 );

  if ( dbTriggersDb == NULL )
    if ( (status = dbLoadTriggers()) != DB_LOAD_OK ) return status;

  for ( tt = EVENT_TRIGGER_ID_MIN; tt != dbMaxTriggerMaskId+1; tt++ ) {
    if ( DB_TEST_BIT( triggerPattern, tt ) ) {
      int t;
      int d;

      for ( t = 0; t != dbSizeTriggersDb; t++ ) {
	if ( dbTriggersDb[t].id == tt )
	  break;
      }
      if ( t == dbSizeTriggersDb ) return DB_UNKNOWN_ID;

      for ( d = EVENT_DETECTOR_ID_MIN; d != dbMaxDetectorId+1; d++ ) {
	if ( TEST_DETECTOR_IN_PATTERN( dbTriggersDb[t].detectorPattern, d ) ) {
	  int status;
	  int i;

	  if ( (status = dbGetLdcsInDetector( d, pat )) != DB_LOAD_OK )
	    return status;
	  
	  for ( i = 0; i != DB_WORDS_IN_LDC_MASK; i++ )
	    ldcPattern[i] |= pat[i];
	}
      }
    }
  }

  return DB_LOAD_OK;
} /* End of dbGetLdcsInTriggerPattern */

/* ------------------------------------------------------------------------- */
int dbGetDetectorsInTriggerPattern(
	 const eventTriggerPatternType  triggerPattern,
	       eventDetectorPatternType detectorPattern ) {
  int tt;
  int status = DB_LOAD_OK;

  memset( detectorPattern, 0, EVENT_DETECTOR_PATTERN_BYTES );

  if ( dbTriggersDb == NULL )
    if ( (status = dbLoadTriggers()) != DB_LOAD_OK )
      return status;

  for ( tt = EVENT_TRIGGER_ID_MIN; tt != EVENT_TRIGGER_ID_MAX; tt++ ) {
    if ( DB_TEST_BIT( triggerPattern, tt ) ) {
      int t;
      int d;

      for ( t = 0; t != dbSizeTriggersDb; t++ ) {
	if ( dbTriggersDb[t].id == tt )
	  break;
      }
      if ( t == dbSizeTriggersDb ) {
	status = DB_UNKNOWN_ID;
      } else {
	for ( d = 0; d != EVENT_DETECTOR_PATTERN_WORDS; d++ )
	  detectorPattern[d] |= dbTriggersDb[t].detectorPattern[d];
      }
    }
  }

  return status;
} /* End of dbGetDetectorsInTriggerPattern */
/* ------------------------------------------------------------------------- */
