/*      dbRoles.c
 *      ============
 *
 * Module to implement DB access to the DATE roles database.
 *
 * Revision history:
 *  1.00  20 Nov 01  RD      First release
 *  1.01  01 Jul 04  RD      HLT role added
 *  1.02  25 Jul 05  RD      Hostname comparison made case-insensitive
 *  1.03  28 Jul 05  RD      Hostname comparison cleaned up
 */

#define VID "V 1.03"

#define DESCRIPTION "DATE Roles DB access"
#ifdef AIX
static
#endif
char dbRolesIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
                       """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include "dateDb.h"
#include "dbConfig.h"

#include "dbSql.h"

/* ------------------------------------------------------------------------- */
/* The DATE ROLES database and its size                                      */
dbRoleDescriptor *dbRolesDb          = NULL;
int               dbSizeRolesDb      = 0;

/* ------------------------------------------------------------------------- */
/* The MAX id values (they must be <= of their corresponding static values)  */
dbIdType dbMaxLdcId         = -1;
dbIdType dbMaxGdcId         = -1;
dbIdType dbMaxTriggerMaskId = -1;
dbIdType dbMaxDetectorId    = -1;
dbIdType dbMaxSubdetectorId = -1;
dbIdType dbMaxTriggerHostId = -1;
dbIdType dbMaxEdmHostId     = -1;
dbIdType dbMaxDdgId         = -1;
dbIdType dbMaxFilterId      = -1;

/* ------------------------------------------------------------------------- */
/* Supports and routines associated to the hash table functions (for fast
   string searches)                                                          */
int *dbRolesHashTable = NULL;	/* The HASH table                            */
int  dbSizeRolesHashTable;	/* The number of entries in the HASH table   */
const char *findName;           /* The name to find as given by the caller   */
int         findNameLenght;	/* The lenght of the name to find            */
dbRoleType  findRole;           /* The role to find as given by the caller   */
int         findIndex;          /* The current find index                    */

static int loadDbRolesHashTable();

/* ------------------------------------------------------------------------- */
static int hasHostname( const dbRoleType role ) {
  return role == dbRoleLdc
      || role == dbRoleGdc
      || role == dbRoleEdmHost
      || role == dbRoleTriggerHost
      || role == dbRoleDdg
      || role == dbRoleFilter;
} /* End of hasHostname */

/* ------------------------------------------------------------------------- */
static int dbRolesSyntaxError( const int lineNo, const char * const line ) {
  dbSetLastLine( lineNo, line );
  fprintf( stderr,
	   "DB load: syntax error handling DATE roles configuration\n" );
  return DB_PARSE_ERROR;
} /* End of dbRolesSyntaxError */

/* ------------------------------------------------------------------------- */
static int expandRolesDb() {
    dbSizeRolesDb++;
    
    if ( (dbRolesDb = realloc( dbRolesDb,
			       sizeof(dbRoleDescriptor)*dbSizeRolesDb )) ==
	 NULL ) {
      perror( "DB load: cannot allocate dbRolesDb " );
      return DB_INTERNAL_ERROR;
    }

    dbRolesDb[dbSizeRolesDb-1].name = NULL;
    dbRolesDb[dbSizeRolesDb-1].hostname = NULL;
    dbRolesDb[dbSizeRolesDb-1].description = NULL;
    dbRolesDb[dbSizeRolesDb-1].id = -1;
    dbRolesDb[dbSizeRolesDb-1].role = dbRoleUnknown;
    dbRolesDb[dbSizeRolesDb-1].hltRole = dbHltRoleUndefined;
    dbRolesDb[dbSizeRolesDb-1].topLevel = FALSE;
    dbRolesDb[dbSizeRolesDb-1].active = TRUE;
    dbRolesDb[dbSizeRolesDb-1].madeOf = dbRoleUndefined;
    dbRolesDb[dbSizeRolesDb-1].bankDescriptor = -1;

    return DB_LOAD_OK;
} /* End of expandRolesDb */

/* ------------------------------------------------------------------------- */
static int handleRolesSection( struct dbSectionStruct *s ) {
  int l;
  int status;

  for ( l = 0; l != s->numLines; l++ ) {
    dbRoleDescriptor *n;
    int k;
    int topLevelDone = FALSE;
    int activeDone = FALSE;
    int hltRoleDone = FALSE;

    if ( (status = expandRolesDb()) != DB_LOAD_OK ) return status;
    n = &dbRolesDb[dbSizeRolesDb-1];
    n->role = s->role;

    if ( s->lines[l].tokens[0].keyword != NULL )
      return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    n->name = s->lines[l].tokens[0].value;
    s->lines[l].tokens[0].value = NULL;
    
    if ( s->lines[l].tokens[1].keyword != NULL )
      return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    if ( sscanf( s->lines[l].tokens[1].value, ID_FORMAT, &n->id ) != 1 )
      return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    if ( n->id < s->minIdValue || n->id > s->maxIdValue ) {
      return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    }
      
    if ( s->lines[l].tokens[2].keyword != NULL )
      return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    n->description = s->lines[l].tokens[2].value;
    s->lines[l].tokens[2].value = NULL;

    for ( k = 3; s->lines[l].tokens[k].value != NULL; k++ ) {
      if ( strcmp( s->lines[l].tokens[k].keyword, HOSTNAME_LABEL ) == 0 ) {
	if ( n->hostname != NULL ) {
	  fprintf( stderr,
		   "DB load: double hostname for %s\n",
		   n->name );
	  return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}
	n->hostname = s->lines[l].tokens[k].value;
	s->lines[l].tokens[k].value = NULL;
      } else if ( strcmp( s->lines[l].tokens[k].keyword,
			  TOPLEVEL_LABEL) == 0 ) {
	if ( topLevelDone )
	  return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	topLevelDone = TRUE;
	if ( strncasecmp( "yes",
			  s->lines[l].tokens[k].value,
			  strlen(s->lines[l].tokens[k].value)) == 0 ) {
	  n->topLevel = TRUE;
	} else if ( strncasecmp("no",
				s->lines[l].tokens[k].value,
				strlen(s->lines[l].tokens[k].value)) != 0 ) {
	  fprintf( stderr,
		   "DB load: %s accepts value YES or NO\n",
		   TOPLEVEL_LABEL );
	  return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}
      } else if ( strcmp( s->lines[l].tokens[k].keyword,
			  ACTIVE_LABEL) == 0 ) {
	if ( activeDone )
	  return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	activeDone = TRUE;
	if ( strncasecmp( "yes",
			  s->lines[l].tokens[k].value,
			  strlen(s->lines[l].tokens[k].value)) == 0 ) {
	  n->active = TRUE;
	} else if ( strncasecmp("no",
				s->lines[l].tokens[k].value,
				strlen(s->lines[l].tokens[k].value)) == 0 ) {
	  n->active = FALSE;
	} else {
	  fprintf( stderr,
		   "DB load: %s accepts value YES or NO\n",
		   ACTIVE_LABEL );
	  return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}
      } else if ( strcmp( s->lines[l].tokens[k].keyword,
			  HLT_ROLE_LABEL) == 0 ) {
	if ( hltRoleDone )
	  return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	hltRoleDone = TRUE;
	if ( strncasecmp( "hltLdc",
			  s->lines[l].tokens[k].value,
			  strlen(s->lines[l].tokens[k].value)) == 0 ) {
	  n->hltRole = dbHltRoleHltLdc;
	} else if ( strncasecmp("detectorLdc",
				s->lines[l].tokens[k].value,
				strlen(s->lines[l].tokens[k].value)) == 0 ) {
	  n->hltRole = dbHltRoleDetectorLdc;
	} else {
	  fprintf( stderr,
		   "DB load: %s accepts value \"hltLdc\" or \"detectorLdc\"\n",
		   HLT_ROLE_LABEL );
	  return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}
      } else {
	fprintf( stderr,
		 "DB load: unknown keyword %s\n",
		 s->lines[l].tokens[k].keyword );
	return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }
    }
    
    if ( hasHostname( n->role ) ) {
      if ( n->hostname == NULL ) {
	if ( (n->hostname = strdup( n->name )) == NULL ) {
	  perror( "DB load: malloc failed while loading roles " );
	  return DB_INTERNAL_ERROR;
	}
      }
    } else {
      if ( n->hostname != NULL ) {
	fprintf( stderr,
		 "DB load: while loading %s, cannot define hostname for %s\n",
		 n->name,
		 dbDecodeRole( n->role ) );
	return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }
    }

    for ( k = 0; dbSections[k].keyword != NULL; k++ ) {
      if ( strncmp( n->name, dbSections[k].keyword, strlen(n->name) ) == 0 ) {
	fprintf( stderr,
		 "DB load: cannot define %s (reserved keyword)\n",
		 n->name );
	return dbRolesSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }
    }

    for ( k = 0; &dbRolesDb[k] != n; k++ ) {
      if ( strcmp( n->name, dbRolesDb[k].name ) == 0 ) {
	fprintf( stderr,
		 "DB load: duplicated role name \"%s\"\n",
		 n->name );
	return DB_PARSE_ERROR;
      }
      if ( ( dbRolesDb[k].role == n->role )
	|| ( dbGetSuperRole( dbRolesDb[k].role ) == inLdcMask &&
	     dbGetSuperRole( n->role ) == inLdcMask ) ) {
	if ( dbRolesDb[k].id == n->id ) {
	  fprintf( stderr,
		   "DB load: duplicated id %d for %s %s and %s %s\n",
		   n->id,
		   dbDecodeRole( dbRolesDb[k].role ),
		   n->name,
		   dbDecodeRole( dbRolesDb[k].role ),
		   n->name );
	  return DB_PARSE_ERROR;
	}
      }
    }

    if ( s->maxId != NULL )
      if ( n->id > *s->maxId ) *s->maxId = n->id;
  }
  
  if ( (status = expandRolesDb()) != DB_LOAD_OK ) return status;
  dbSizeRolesDb--;
  
  return DB_LOAD_OK;
} /* End of handleRolesSection */

/* ------------------------------------------------------------------------- */
int dbLoadRoles() {
  int status;
  int section;

  if ( dbRolesDb != NULL ) return DB_LOAD_OK;
  
  /* Switch for SQL database */
  if (dbsql_is_configured()) {
    if ( dbsql_open()!=0 ) return DB_LOAD_ERROR;
    if ( (status = dbsql_load_roles()) != DB_LOAD_OK ) return status;
    dbsql_close();
  } else {
  
  if ( (status = dbInitInput()) != DB_UNLOAD_OK ) return status;
  if ( (status = dbInput( dbsTable[ DB_TABLE_ROLES ].name )) != DB_LOAD_OK )
    return status;
  for ( section = 0; dbSections[section].role != dbRoleUndefined; section++ ) {
    if ( (status = handleRolesSection( &dbSections[section] )) !=
	 DB_LOAD_OK ) {
      dbUnloadRoles();
      return status;
    }
  }

  } /* end of Switch for SQL database */  
  
  return loadDbRolesHashTable();
} /* End of dbLoadRoles */

/* ------------------------------------------------------------------------- */
int dbUnloadRoles() {
  int s;
  int r;

  for ( r = 0; r != dbSizeRolesDb; r++ ) {
    free( dbRolesDb[r].name );
    free( dbRolesDb[r].hostname );
    free( dbRolesDb[r].description );
  }
  
  free( dbRolesDb );
  dbRolesDb = NULL;
  dbSizeRolesDb = 0;

  free( dbRolesHashTable );
  dbRolesHashTable = NULL;

  for ( s = 0; dbSections[s].keyword != NULL; s++ )
    if ( dbSections[s].maxId != NULL )
      *dbSections[s].maxId = -1;

  return DB_UNLOAD_OK;
} /* End of dbUnloadRoles */

/* ------------------------------------------------------------------------- */
/* Hash a given name, return a possible index */
static inline int hashRoleName( const char * const name, const int maxValue ) {
  unsigned int result = -1;
  int i;

  for ( i = 0 ; name[i] != 0; i++ ) {
    result = ( result << 8 ) | ((result & 0xff000000) >> 24);
    result = result ^ name[i];
  }
  
  return result % maxValue;
} /* End of hashRoleName */

/* ------------------------------------------------------------------------- */
/* Load our private hash table, used for fast queries */
static int loadDbRolesHashTable() {
  int size;
  int i;
  
  if ( dbSizeRolesDb < 0 ) return DB_INTERNAL_ERROR;
  
  free( dbRolesHashTable );
  dbRolesHashTable = NULL;

  if ( dbSizeRolesDb == 0 ) return DB_LOAD_OK;

  dbSizeRolesHashTable = 2.5 * dbSizeRolesDb + 1;
  size = sizeof(int) * dbSizeRolesHashTable;
  if ( (dbRolesHashTable  = malloc( size )) == NULL )
    return DB_INTERNAL_ERROR;
  for ( i = 0; i != dbSizeRolesHashTable; i++ )
    dbRolesHashTable[i] = -1;
  
  for ( i = 0; i != dbSizeRolesDb; i++ ) {
    int j = hashRoleName( dbRolesDb[i].name, dbSizeRolesHashTable );
    while ( dbRolesHashTable[j] != -1 ) j = (j + 1) % dbSizeRolesHashTable;
    dbRolesHashTable[j] = i;
  }

  if ( FALSE ) {
    printf( "Hash table:\n" );
    for ( size=0, i=0; i != dbSizeRolesHashTable; i++ ) {
      printf( "  %3d) ", i );
      if ( dbRolesHashTable[i] == -1 ) printf( "============\n" );
      else {
        printf( "%d %s\n",
		dbRolesHashTable[i],
		dbRolesDb[dbRolesHashTable[i]].name );
        size++;
      }
    }
    if ( size != dbSizeRolesDb )
      printf( "  ===> MISMATCH dbSizeRolesDb:%d, found:%d <===\n",
              dbSizeRolesDb, size );
  }
  
  return DB_LOAD_OK;
} /* End of loadDbRolesHashTable */

/* ------------------------------------------------------------------------- */
/* Initialise and find a record matching the given condition.
 *
 * Input parameters:
 *  name   name to match (must remain valid throughout all dbRolesFindNext)
 *  role   role to match (unknown: all roles)
 *
 * Return: NULL if invalid parameter(s) or no match found.
 */
int dbRolesFind( const char * const name, const dbRoleType role ) {
  int lenght;
  
  findName = NULL;
  if ( name == NULL ) return -1;
  if ( (lenght = strlen( name )) <= 0 ) return -1;
  
  findName = name;
  findNameLenght = lenght;
  findRole = role;
  findIndex = hashRoleName( name, dbSizeRolesHashTable );
  return dbRolesFindNext();
} /* End of dbRolesFind */

/* ------------------------------------------------------------------------- */
/* Find the next record matching the given condition (if any).
 * Call to be repeated to get multiple matches.
 *
 * Return: NULL if no match found
 */
int dbRolesFindNext() {
  int match = -1;
  int i;
  
  if ( findName == NULL || dbRolesHashTable == NULL ) return -1;

  do {
    if ( (i = dbRolesHashTable[findIndex]) != -1 ) {
      if ( findRole == dbRoleUnknown
	|| dbRolesDb[i].role == findRole ) {
	if ( strncmp( findName, dbRolesDb[i].name, findNameLenght ) == 0 ) {
	  match = i;
	}
      }
      if ( ++findIndex == dbSizeRolesHashTable ) findIndex = 0;
    }
  } while ( i != -1 && match == -1 );
  return match;
} /* End of dbRolesFindNext */
