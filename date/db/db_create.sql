########################################
# Creation of environment table        #
########################################


# Table to store environment variables
CREATE TABLE IF NOT EXISTS ENV
(
NAME CHAR(32),
VALUE TEXT,
CLASS ENUM('General','Database','Infologger','User') DEFAULT 'General',
DESCRIPTION TEXT,
LOAD_BY_DEFAULT BOOL DEFAULT '0',
PRIMARY KEY(NAME)
) ENGINE = INNODB
comment = "";

INSERT INTO ENV VALUES('DIM_DNS_NODE',NULL,DEFAULT,'Host running DIM domain name server','1');
INSERT INTO ENV VALUES('DAQ_ROOT_DOMAIN_NAME','DATE',DEFAULT,'SMI domain for runControl','1');

INSERT INTO ENV VALUES('DATE_MYSQL','TRUE','Database','Set to TRUE to compile DATE with MySQL support','1');

INSERT INTO ENV VALUES('DATE_DB_MYSQL','TRUE','Database','Set to TRUE to load DATE configuration from MySQL','1');
INSERT INTO ENV VALUES('DATE_DB_MYSQL_USER',NULL,'Database','Username to access DATE configuration database','1');
INSERT INTO ENV VALUES('DATE_DB_MYSQL_PWD',NULL,'Database','Password to access DATE configuration database','1');
INSERT INTO ENV VALUES('DATE_DB_MYSQL_HOST',NULL,'Database','Host to access DATE configuration database','1');
INSERT INTO ENV VALUES('DATE_DB_MYSQL_DB',NULL,'Database','Database name for DATE configuration','1');

INSERT INTO ENV VALUES('DATE_INFOLOGGER_LOGHOST',NULL,'InfoLogger','Host running the infoLoggerServer daemon','0');
INSERT INTO ENV VALUES('DATE_INFOLOGGER_MYSQL','TRUE','InfoLogger','Set to TRUE to store DATE logs in MySQL','0');
INSERT INTO ENV VALUES('DATE_INFOLOGGER_MYSQL_USER',NULL,'InfoLogger','Username to access DATE logs database','0');
INSERT INTO ENV VALUES('DATE_INFOLOGGER_MYSQL_PWD',NULL,'InfoLogger','Password to access DATE logs database','0');
INSERT INTO ENV VALUES('DATE_INFOLOGGER_MYSQL_HOST',NULL,'InfoLogger','Host to access DATE logs database','0');
INSERT INTO ENV VALUES('DATE_INFOLOGGER_MYSQL_DB',NULL,'InfoLogger','Database name for DATE logs','0');



# Table to store files
CREATE TABLE IF NOT EXISTS FILES
(
PATH CHAR(255),
HOST CHAR(32),
VALUE LONGBLOB,
DESCRIPTION TEXT,
EDITABLE BOOL DEFAULT 1,
CLASS ENUM('User') DEFAULT 'User',
PRIMARY KEY(PATH(255),HOST(32))
) ENGINE = INNODB
comment = "";



# Table to store persistent runtime variables
CREATE TABLE IF NOT EXISTS GLOBALS
(
NAME CHAR(32),
VALUE TEXT,
DESCRIPTION TEXT,
PRIMARY KEY(NAME)
) ENGINE = INNODB
comment = "";

INSERT INTO GLOBALS VALUES('Run Number','0','The last run number used');
INSERT INTO GLOBALS VALUES('DB version','v9','The version of the configuration database');
