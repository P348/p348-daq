###########################################
# Destruction of environment table        #
###########################################
DROP TABLE IF EXISTS ENV;


###########################################
# Destruction of files table              #
###########################################
DROP TABLE IF EXISTS FILES;


###########################################
# Destruction of globals table            #
###########################################
DROP TABLE IF EXISTS GLOBALS;
