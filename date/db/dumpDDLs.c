/* dumpDbs.c: utility to dump the list of configured DDLs */

#include <stdio.h>
#include <stdlib.h>

#include "dateDb.h"


int main() {
  
  dbLdcPatternType LDCmask;
  int i;
  dbIdType *t_ldcid;
  int size;
  int n;
    
  for (i=0;i<=HOST_ID_MAX;i++) {
    DB_SET_BIT(LDCmask,i);
  }
  n=dbGetDDLtoLDCtable(&LDCmask,&t_ldcid, &size);
  
  if (n==-1) {
    printf("Error - can not load list of active DDLs from database\n");
    return 1;
  }
  if (n==0) {
    printf("No active DDL found\n");
    return 0;
  }
  
  printf("%d DDL ids in table, %d active\n",size,n);
  
  for (i=0;i<size;i++) {
    if (t_ldcid[i]!=LDC_ID_INACTIVE) {
      printf("DDL %d => LDC %d\n",i,t_ldcid[i]);
    }
  }
  
  free(t_ldcid);

  return 0;
}
