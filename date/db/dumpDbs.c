/* dumpDbs.c: utility to dump the content of all DATE databases */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/times.h>

#include "dateDb.h"
#include "dbConfig.h"

/* Internal variables */
int exitStatus = 0;
int doHashTableCheck = FALSE;

#define SEPARATOR \
   printf( ".........................................................\n" )

#define MAX(a,b) ((a)>(b)?(a):(b))

#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

/* ------------------------------------------------------------------------- */
char *stripTrailing( const char * const in ) {
  static char line[1024];
  int c;

  strncpy( line, in, sizeof(line) );
  line[sizeof(line)-1]=0;
  c = strlen(line)-1;
  for (;;) {
    if ( c == -1 ) {
      line[0] = 0;
      return line;
    }
    if ( line[c] != ' ' && line[c] != '\t' ) return line;
    line[c--] = 0;
  }
} /* End of striptTrailing */

/* ------------------------------------------------------------------------- */
const char *decodeEventType( const eventTypeType t ) {
  static char msg[5][ 1024 ];
  static int l = 0;
  const char *stype;
  
  stype=dbDecodeEventType(t);
  if (stype!=NULL) {
    return stype;
  }
  
  snprintf( msg[l], sizeof(msg[l]), "unknownType:%d:0x%08x", t, t );
  exitStatus = 1;
  return msg[l++ % 5];
} /* End of decodeEventType */

#define MAX_PRINTS 5

/* ------------------------------------------------------------------------- */
void dumpDetectorPattern( const eventDetectorPatternType mask ) {
  int d;

  printf( "detectorPattern:" );
  for ( d = 0; d != EVENT_DETECTOR_PATTERN_WORDS; d++ ) {
    printf( "%s%08x", d == 0 ? "" : "+", mask[d] );
  }
} /* End of dumpDetectorPattern */

/* ------------------------------------------------------------------------- */
void dumpLdcPattern( const long32 *pat ) {
  int l;
  int c;

  printf( "ldcPattern:" );
  
  if ( FALSE ) {
    for ( l = 0; l != DB_WORDS_IN_LDC_MASK; l++ )
      printf( "%s%08x", l == 0 ? "" : "+", pat[l] );
    printf( "=>" );
  }
  
  for ( c = 0, l = HOST_ID_MIN; l != MAX( HOST_ID_MAX, dbMaxLdcId )+1 ; l++ ) {
    if ( DB_TEST_BIT( pat, l ) ) {
      int i;

      printf( "%s", c++ == 0 ? "" : "+" );
      for ( i = 0; i != dbSizeRolesDb; i++ )
	if ( dbRolesDb[i].id == l && dbRolesDb[i].role == dbRoleLdc ) break;
      if ( i != dbSizeRolesDb ) {
	printf( "%s", dbRolesDb[i].name );
      } else {
	printf( "??? UNKNOWN:%d ???", l );
	exitStatus = 1;
      }
      if ( l > dbMaxLdcId ) {
	printf( "??? > dbMaxLdcId:%d ???", dbMaxLdcId );
	exitStatus = 1;
      }
      if ( l > HOST_ID_MAX ) {
	printf( "??? > HOST_ID_MAX:%d ???", HOST_ID_MAX );
	exitStatus = 1;
      }
    }
  }
  if ( c > 0 )
    printf( " (%d LDC%s)", c, c != 1 ? "s" : "" );
  else
    printf( " (no LDCs)" );
  printf( "\n" );
} /* End of dumpLdcPattern */

/* ------------------------------------------------------------------------- */
void dumpSubdetectorPattern( const dbLdcPatternType pat ) {
  int l;
  int c;

  printf( "subdetectorPattern:" );

  if ( FALSE ) {
    for ( l = 0; l != DB_WORDS_IN_LDC_MASK; l++ )
      printf( "%s%08x", l == 0 ? "" : "+", pat[l] );
    printf( "=>" );
  }
  
  for ( c = 0, l = HOST_ID_MIN;
	l != MAX( HOST_ID_MAX, dbMaxSubdetectorId )+1 ; l++ ) {
    if ( DB_TEST_BIT( pat, l ) ) {
      int i;

      printf( "%s", c++ == 0 ? "" : "+" );
      for ( i = 0; i != dbSizeRolesDb; i++ )
	if ( dbRolesDb[i].id == l && dbRolesDb[i].role == dbRoleSubdetector )
	  break;
      if ( i != dbSizeRolesDb )
	printf( "%s", dbRolesDb[i].name );
      else {
	printf( "??? UNKNOWN:%d ???", l );
	exitStatus = 1;
      }
      if ( l > dbMaxSubdetectorId ) {
	printf( "??? > dbMaxSubdetectorId:%d ???", dbMaxSubdetectorId );
	exitStatus = 1;
      }
      if ( l > HOST_ID_MAX ) {
	printf( "??? > HOST_ID_MAX:%d ???", HOST_ID_MAX );
	exitStatus = 1;
      }
    }
  }
  if ( c > 0 )
    printf( " (%d subdetector%s)", c, c != 1 ? "s" : "" );
  else
    printf( " (no subdetectors)" );
  printf( "\n" );
} /* End of dumpSubdetectorPattern */

/* ------------------------------------------------------------------------- */
void dumpTriggerPattern( const eventTriggerPatternType pat ) {
  int i, j, k;
  
  printf( "triggerPattern:" );
  for ( i = EVENT_TRIGGER_PATTERN_WORDS-1; i != -1; i-- ) {
    printf( "%08x", pat[i] );
    if ( i != 0 ) printf( "-" );
  }
  for ( k=j=i=0; i != MAX(dbMaxTriggerMaskId, EVENT_TRIGGER_ID_MAX)+1; i++ ) {
    if ( DB_TEST_BIT( pat, i ) ) {

      if ( i > dbMaxTriggerMaskId || i > EVENT_TRIGGER_ID_MAX ) {
	printf( "??? " );
	printf( " trigger:%d", i );
	printf( " dbMaxTriggerMaskId:%d", dbMaxTriggerMaskId );
	printf( " EVENT_TRIGGER_ID_MAX:%d", EVENT_TRIGGER_ID_MAX );
	printf( " ???" );
	exitStatus = 1;
      }
      
      printf( "%c%s%d",
	      j++ == 0 ? '=' : '+',
	      i > dbMaxTriggerMaskId ? " ???" : "",
	      i );
      if ( i > dbMaxTriggerMaskId ) k++;
    }
  }
  if ( k != 0 || dbMaxTriggerMaskId > EVENT_TRIGGER_ID_MAX ) {
    printf( ">ERROR dmMaxTriggerId:%d EVENT_TRIGGER_ID_MAX:%d",
	    dbMaxTriggerMaskId,
	    EVENT_TRIGGER_ID_MAX );
    if ( k != 0 ) printf( "<%d ID(s) above max>", k );
    printf( "<" );
  }
} /* End of dumpTriggerPattern */

/* ===== Event building control database handlers ===== */
int loadEventBuildingControlDB() {
  int status;

  if ( (status = dbLoadEventBuildingControl()) != DB_LOAD_OK ) {
    char msg[ 1024 ];
    
    snprintf( msg,
	      sizeof(msg),
	      "Error during dbLoadEventBuildingControl:%d \"%s\". \
System-dependent status ",
	      status,
	      dbDecodeStatus( status ) );
    perror( msg );
    fprintf( stderr, "\tSource line: %s", dbGetLastLine() );
    exitStatus = 1;
  }
  return status == DB_LOAD_OK;
} /* End of loadEventBuildingControlDB */

void printEventBuildingControlDB() {
  int i;
  
  printf( "Event building control DB:\n" );
  if ( dbEventBuildingControlDb == NULL ) {
    printf( "  UNLOADED\n" );
    if ( dbSizeEventBuildingControlDb != 0 )
      printf( "  ??? dbSizeDeventBuildingControlDb:%d ???\n",
	      dbSizeEventBuildingControlDb );
    exitStatus = 1;
    return;
  }
  for ( i = 0; i != dbSizeEventBuildingControlDb; i++ ) {
    dbEventBuildingRule *rule = &dbEventBuildingControlDb[i];
    
    printf( "  %4d)", i );
    printf( " eventType:%s ", decodeEventType( rule->eventType ) );
    if ( rule->type == fullBuild ) {
      printf( "all-events" );
    } else if ( rule->type == useDetectorPattern ) {
      dumpDetectorPattern( rule->pattern.detectorPattern );
    } else if ( rule->type == useTriggerPattern ) {
      dumpTriggerPattern( rule->pattern.triggerPattern );
    } else {
      printf( "?UNKNOWN-TYPE-%d?", (int)rule->type );
      exitStatus = 1;
    }
    if ( rule->hltDecision ) {
      printf( " hltDecisionNeeded" );
    }
    printf( " %s", rule->build ? "BUILD" : "NO-BUILD" );
    printf( "\n" );
    if ( rule->type == useDetectorPattern ) {
      if ( dbMaxDetectorId == -1 ) {
	printf( "\t??? dbMaxDetectorId=%d ???\n", dbMaxDetectorId );
	exitStatus = 1;
      } else if ( dbSizeDetectorsDb == 0 ) {
	printf( "\t??? dbDetectorsDb is UNLOADED (dbSizeDetectorsDb:%d) ???\n",
		dbSizeDetectorsDb );
	exitStatus = 1;
      } else {
	int n;
      
	for ( n = EVENT_DETECTOR_ID_MIN;
	      n != MAX( dbMaxDetectorId, EVENT_DETECTOR_ID_MAX )+1;
	      n++ ) {
	  if ( DB_TEST_BIT( rule->pattern.detectorPattern, n ) ) {
	    int j;
	  
	    for ( j = 0; j < dbSizeRolesDb; j++ )
	      if ( dbRolesDb[j].role==dbRoleDetector && dbRolesDb[j].id == n )
		break;
	    printf( "\t   %d:", n );
	    if ( i < dbSizeRolesDb ) {
	      printf( "%s", dbRolesDb[j].name );
	    } else {
	      printf( "??? UNDEFINED ???" );
	      exitStatus = 1;
	    }
	    if ( n > dbMaxDetectorId ) {
	      printf( "??? > maxDetectorId (%d) ???",
		      dbMaxDetectorId );
	      exitStatus = 1;
	    }
	    if ( n < EVENT_DETECTOR_ID_MIN || n > EVENT_DETECTOR_ID_MAX ) {
	      printf( "??? out of static range [%d..%d] ???",
		      EVENT_DETECTOR_ID_MIN, EVENT_DETECTOR_ID_MAX );
	      exitStatus = 1;
	    }
	    printf( "\n" );
	  }
	}
      }
    }
    if ( rule->type == useTriggerPattern ) {
      if ( dbMaxTriggerMaskId == -1 ) {
	printf( "\t??? dbMaxTriggerMaskId=%d ???\n", dbMaxTriggerMaskId );
	exitStatus = 1;
      } else if ( dbSizeTriggersDb == 0 ) {
	printf( "\t??? dbTriggersDb is UNLOADED (dbSizeTriggersDb:%d) ???\n",
		dbSizeTriggersDb );
	exitStatus = 1;
      } else {
	int n;

	for ( n = EVENT_TRIGGER_ID_MIN;
	      n != MAX(dbMaxTriggerMaskId,EVENT_TRIGGER_ID_MAX)+1;
	      n++ ) {
	  if ( TEST_TRIGGER_IN_PATTERN( rule->pattern.triggerPattern, n ) ) {
	    int j;
	  
	    for ( j = 0; j < dbSizeRolesDb; j++ )
	      if ( dbRolesDb[j].role == dbRoleTriggerMask &&
		   dbRolesDb[j].id == n )
		break;
	    printf( "\t   %d:", n );
	    if ( i < dbSizeRolesDb ) {
	      printf( "%s", dbRolesDb[j].name );
	    } else {
	      printf( "??? UNDEFINED ???" );
	      exitStatus = 1;
	    }
	    if ( n > dbMaxTriggerMaskId ) {
	      printf( "??? > maxTriggerId (%d) ???",
		      dbMaxTriggerMaskId );
	      exitStatus = 1;
	    }
	    if ( n < EVENT_TRIGGER_ID_MIN || n > EVENT_TRIGGER_ID_MAX ) {
	      printf( "??? out of static range [%d..%d] ???",
		      EVENT_TRIGGER_ID_MIN, EVENT_TRIGGER_ID_MAX );
	      exitStatus = 1;
	    }
	    printf( "\n" );
	  }
	}
      }
    }
  }
  if ( dbEventBuildingControlDb[i].eventType != DB_UNDEFINED_EVENT_TYPE ) {
    printf( "  ??? NON-NULL terminator ???\n" );
    exitStatus = 1;
  }
} /* End of printEventBuildingControlDB */

void checkEventBuildingControlDB() {
} /* End of checkEventBuildingControlDB */

void dumpEventBuildingControl() {
  printEventBuildingControlDB();
  checkEventBuildingControlDB();
} /* End of dumpEventBuildingControl */

/* ===== Trigger database handlers ===== */
int loadTriggerDB() {
  int status;

  if ( (status = dbLoadTriggers()) != DB_LOAD_OK ) {
    char msg[ 1024 ];
    
    snprintf( msg,
	      sizeof(msg),
	      "Error during dbLoadTrigger:%d \"%s\". System-dependent status ",
	      status,
	      dbDecodeStatus( status ) );
    perror( msg );
    exitStatus = 1;
  }
  return status == DB_LOAD_OK;
} /* End of loadTriggerDB */

void printTriggerDB() {
  int i;
  int status;
  eventTriggerPatternType trigPat;
  dbLdcPatternType ldcPat;

  printf( "Trigger DB:\n" );
  if ( dbTriggersDb == NULL ) {
    printf( "  UNLOADED\n" );
    if ( dbSizeTriggersDb != 0 ) {
      printf( "  ??? dbSizeTriggersDb:%d ???\n", dbSizeTriggersDb );
      exitStatus = 1;
    }
    return;
  }
  if ( dbMaxTriggerMaskId < EVENT_TRIGGER_ID_MIN
    || dbMaxTriggerMaskId > EVENT_TRIGGER_ID_MAX ) {
    printf( "  ???" );
    printf( " dbMaxTriggerMaskId:%d",   dbMaxTriggerMaskId );
    printf( " EVENT_TRIGGER_ID_MIN:%d", EVENT_TRIGGER_ID_MIN );
    printf( " EVENT_TRIGGER_ID_MAX:%d", EVENT_TRIGGER_ID_MAX );
    printf( " ???\n" );
    exitStatus = 1;
  }
  for ( i = 0; i != dbSizeTriggersDb; i++ ) {
    dbTriggerDescriptor *ptr = &dbTriggersDb[i];
    int j, n;
    int numDets;
    
    printf( " %4d) ", i );
    printf( "id:%4d", ptr->id );
    if ( ptr->id == -1 ) {
      printf( " ??? INVALID ???" );
      exitStatus = 1;
    } else if ( ptr->id > dbMaxTriggerMaskId ||
	        ptr->id < EVENT_TRIGGER_ID_MIN ||
	        ptr->id > EVENT_TRIGGER_ID_MAX ) {
      printf( "??? out-of-range" );
      printf( " dbMaxTriggerMaskId:%d",   dbMaxTriggerMaskId );
      printf( " EVENT_TRIGGER_ID_MIN:%d", EVENT_TRIGGER_ID_MIN );
      printf( " EVENT_TRIGGER_ID_MAX:%d", EVENT_TRIGGER_ID_MAX );
      printf( " ???\n" );
      exitStatus = 1;
    }
    for ( j = 0; j != dbSizeRolesDb; j++ )
      if ( dbRolesDb[j].role == dbRoleTriggerMask &&
	   dbRolesDb[j].id == ptr->id )
	break;
    if ( j != dbSizeRolesDb ) {
      printf( " %s", dbRolesDb[j].name );
    } else {
      printf( " ??? UNDEFINED ???" );
      exitStatus = 1;
    }
    printf( "\n" );
    
    printf( "\t" );
    dumpDetectorPattern( ptr->detectorPattern );
    if ( dbMaxDetectorId != -1 ) {
      eventDetectorPatternType detectorPattern, detectorPatternCheck;
      eventTriggerPatternType triggerPattern;

      memset( detectorPattern, 0, sizeof( detectorPattern ) );
      for ( numDets = 0, n = 0; n <= dbMaxDetectorId; n++ ) {
	if ( DB_TEST_BIT( ptr->detectorPattern, n ) ) {
	  SET_DETECTOR_IN_PATTERN( detectorPattern, n );
	  printf( "%s", numDets++ == 0 ? " = " : "+" );
	  for ( j = 0; j != dbSizeRolesDb; j++ )
	    if ( dbRolesDb[j].role == dbRoleDetector &&
		 dbRolesDb[j].id == n )
	      break;
	  if ( j == dbSizeRolesDb ) {
	    printf( " ??? UNDEFINED %d ???", n );
	    exitStatus = 1;
	  } else {
	    printf( "%s", dbRolesDb[j].name );
	  }
	}
      }
      memset( triggerPattern, 0, sizeof( triggerPattern ) );
      SET_TRIGGER_IN_PATTERN( triggerPattern, ptr->id );
      status = dbGetDetectorsInTriggerPattern( triggerPattern,
					       detectorPatternCheck );
      if ( status != DB_LOAD_OK ) {
	printf( " - FAILED dbGetDetectorsInTriggerPattern: %s",
		dbDecodeStatus( status ) );
      } else {
	int x;

	for ( x = 0; x != EVENT_DETECTOR_PATTERN_WORDS; x++ ) {
	  if ( detectorPattern[x] != detectorPatternCheck[x] ) break;
	}
	if ( x != EVENT_DETECTOR_PATTERN_WORDS ) {
	  printf( " - Detector pattern mismatch dbGetDetectorsInTriggerPattern:" );
	  dumpDetectorPattern( detectorPatternCheck );
	}
      }
    }
    printf( "\n" );
    
    printf( "\t   " );
    ZERO_TRIGGER_PATTERN( trigPat );
    SET_TRIGGER_IN_PATTERN( trigPat, ptr->id );
    if ( (status = dbGetLdcsInTriggerPattern( trigPat, ldcPat )) !=
	 DB_LOAD_OK ) {
      printf( "Failed to get LDCs in Trigger Pattern status:%d (%s)\n",
	      status,
	      dbDecodeStatus( status ) );
      exitStatus = 1;
    } else {
      printf( " => " );
      dumpLdcPattern( ldcPat );
    }
  }
  if ( dbTriggersDb[i].id != -1 ) {
    printf( "  ??? NON-NULL terminator ???\n" );
    exitStatus = 1;
  }
} /* End of printTriggerDB */

void checkTriggerDB() {
  int i;

  if ( dbTriggersDb == NULL ) return;

  if ( dbTriggersDb == NULL
    || dbMaxTriggerMaskId == 0
    || dbSizeTriggersDb == -1 ) {
    printf( "ERROR:\
 dbTriggersDb:%p dbMaxTriggerMaskId:%d dbSizeTriggersDb:%d\n",
	    dbTriggersDb, dbMaxTriggerMaskId, dbSizeTriggersDb );
    exitStatus = 1;
    return;
  }
  for ( i = 0; i != dbSizeTriggersDb; i++ ) {
    int j;
    
    for ( j = i+1; j != dbSizeTriggersDb; j++ ) {
      if ( dbTriggersDb[i].id == dbTriggersDb[j].id ) {
	printf( "WARNING: repeated trigger id %d (%d - %d)\n",
		dbTriggersDb[i].id, i, j );
	exitStatus = 1;
      }
    }
  }
} /* End of checkTriggerDB */

void dumpTrigger() {
  printTriggerDB();
  checkTriggerDB();
} /* End of dumpTrigger */

/* ==== Detectors database handlers ==== */
int loadDetectorsDB() {
  int status;

  if ( (status = dbLoadDetectors()) != DB_LOAD_OK ) {
    char msg[ 1024 ];
    snprintf( msg,
	      sizeof(msg),
	      "Error during dbLoadDetectors:%d \"%s\".\
 System-dependent status ",
	      status,
	      dbDecodeStatus( status ) );
    perror( msg );
    exitStatus = 1;
  }
  return status == DB_LOAD_OK;
} /* End of loadDetectorsDB */

void dumpDetectorsDB() {
  int i;
  int status;

  printf( "Detectors DB:\n" );
  if ( dbDetectorsDb == NULL ) {
    printf( "  UNLOADED\n" );
    if ( dbSizeDetectorsDb != 0 ) {
      printf( "  ??? dbSizedetectorsDb:%d ???\n", dbSizeDetectorsDb );
      exitStatus = 1;
    }
    return;
  }
  if ( dbMaxDetectorId < EVENT_DETECTOR_ID_MIN
    || dbMaxDetectorId > EVENT_DETECTOR_ID_MAX ) {
    printf( "  ???" );
    printf( " dbMaxDetectorId:%d", dbMaxDetectorId );
    printf( " EVENT_DETECTOR_ID_MIN:%d", EVENT_DETECTOR_ID_MIN );
    printf( " EVENT_DETECTOR_ID_MAX:%d", EVENT_DETECTOR_ID_MAX );
    printf( " ???\n" );
    exitStatus = 1;
  }
  for ( i = 0; i != dbSizeDetectorsDb; i++ ) {
    int j;
    
    printf( "  %4d)", i );
    printf( " %s id:%3d",
	    stripTrailing( dbDecodeRole( dbDetectorsDb[i].role ) ),
	    dbDetectorsDb[i].id );

    for ( j = 0; j != dbSizeRolesDb; j++ )
      if ( dbRolesDb[j].role == dbDetectorsDb[i].role
	&& dbRolesDb[j].id == dbDetectorsDb[i].id ) break;
    if ( j == dbSizeRolesDb ) {
      printf( " ??? UNKNOWN ???\n" );
      exitStatus = 1;
      continue;
    }
    printf( "\t%s (made of:%s)\n",
	    dbRolesDb[j].name,
	    stripTrailing( dbDecodeRole( dbRolesDb[j].madeOf )));
    printf( "\t" );
    if ( dbRolesDb[j].madeOf == dbRoleLdc ) {
      dumpLdcPattern( dbDetectorsDb[i].componentPattern );
    } else if ( dbRolesDb[j].madeOf == dbRoleSubdetector ) {
      dumpSubdetectorPattern( dbDetectorsDb[i].componentPattern );
    } else {
      printf( "Unknown made of: %s\n", dbDecodeRole( dbRolesDb[j].madeOf ) );
    }
    if ( dbDetectorsDb[i].role == dbRoleDetector ) {
      dbLdcPatternType ldcPattern;
      
      printf( "\t" );
      if ( (status = dbGetLdcsInDetector( dbDetectorsDb[i].id, ldcPattern )) !=
	   DB_LOAD_OK ) {
	printf( "Failed to get LDCS error:%d (%s)\n",
		status,
		dbDecodeStatus( status ) );
	exitStatus = 1;
      } else {
	printf( "   => " );
	dumpLdcPattern( ldcPattern );
      }
    }
  }
  if ( dbDetectorsDb != NULL ) {
    if ( dbDetectorsDb[i].id != -1 ) {
      printf( "  ??? NON-NULL terminator ???\n" );
      exitStatus = 1;
    }
  }
} /* End of dumpDetectorsDB */

void checkDetectorsDB() {
  int i;

  for ( i = 0; i != dbSizeDetectorsDb; i++ ) {
    int ri, j;

    for ( ri = 0; ri != dbSizeRolesDb; ri++ )
      if ( dbRolesDb[ri].id == dbDetectorsDb[i].id &&
	   dbRolesDb[ri].role == dbRoleDetector ) break;
    if ( ri == dbSizeRolesDb ) {
      exitStatus = 1;
      continue;
    }
    for ( j = i+1; j != dbSizeDetectorsDb; j++ ) {
      int rj;
      int n;
      dbLdcPatternType andPattern;

      if ( dbDetectorsDb[i].role != dbDetectorsDb[j].role ) continue;
      
      if ( dbDetectorsDb[i].id == dbDetectorsDb[j].id ) {
	printf( " ???" );
	printf( " Rule:%d", i );
	printf( " Duplicated id:%d", dbDetectorsDb[i].id );
	printf( " with rule:%d", j );
	printf( " ???\n" );
	exitStatus = 1;
	continue;
      }
      
      for ( rj = 0; rj != dbSizeRolesDb; rj++ )
	if ( dbRolesDb[rj].id == dbDetectorsDb[j].id &&
	     dbRolesDb[rj].role == dbRoleDetector ) break;
      if ( rj == dbSizeRolesDb ) {
	exitStatus = 1;
	continue;
      }
      
      if ( dbRolesDb[ri].madeOf != dbRolesDb[rj].madeOf ) continue;

      for ( n = 0; n != sizeof( andPattern ) / 4; n++ )
	andPattern[n] = dbDetectorsDb[i].componentPattern[n] &
	                dbDetectorsDb[j].componentPattern[n];
      for ( n = 0; n != dbMaxLdcId; n++ ) {
	if ( DB_TEST_BIT( andPattern, n ) ) {
	  int h;
	  
	  printf( " ???" );
	  printf( " Rule:%d", i );
	  printf( " Shared %s:%d",
		  stripTrailing( dbDecodeRole( dbRolesDb[ri].madeOf ) ),
		  n );
	  for ( h = 0; h != dbSizeRolesDb; h++ )
	    if ( dbRolesDb[h].role == dbRolesDb[ri].madeOf &&
		 dbRolesDb[h].id == n ) break;
	  printf( " %s",
		  h == dbSizeRolesDb ? "UNKNOWN" : dbRolesDb[h].name );
	  printf( " with rule:%d", j );
	  printf( " ???\n" );
	  exitStatus = 1;
	}
      }
    }
  }
} /* End of checkDetectorsDB */

void dumpDetectors() {
  dumpDetectorsDB();
  checkDetectorsDB();
} /* End of dumpDetectors */

/* ==== Banks database handlers ==== */
int loadBanksDB() {
  int status;

  if ( (status = dbLoadBanks()) != DB_LOAD_OK ) {
    char msg[ 1024 ];
    snprintf( msg,
	      sizeof(msg),
	      "Error during dbLoadBanks:%d \"%s\".\
 System-dependent status ",
	      status,
	      dbDecodeStatus( status ) );
    perror( msg );
    exitStatus = 1;
  }
  return status == DB_LOAD_OK;
} /* End of loadBanksDB */

const char * const getName( int i ) {
  static char result[1024];

  snprintf( SP(result),
	    "%s %s",
	    stripTrailing(dbDecodeRole( dbRolesDb[i].role ) ),
	    dbRolesDb[i].name );
  if ( strcmp( dbRolesDb[i].name, dbRolesDb[i].hostname ) != 0 )
    snprintf( AP(result), " (%s)", dbRolesDb[i].hostname );
  return result;
} /* End of getName */

void dumpBanks() {
  int i;

  printf( "Banks DB:\n" );
  if ( dbBanksDb == NULL ) {
    printf( "  UNLOADED\n" );
    if ( dbSizeBanksDb != 0 ) {
      printf( "  ??? dbSizeBanksDb:%d ???\n", dbSizeBanksDb );
      exitStatus = 1;
    }
    return;
  }
  for ( i = 0; i != dbSizeRolesDb; i++ ) {
    int b;
    
    if ( (b = dbRolesDb[i].bankDescriptor) != -1 ) {
      int j;
      
      printf( " %s: descriptor:%d %d bank(s)\n",
	      getName( i ),
	      b,
	      dbBanksDb[b].numBanks );
      for ( j = 0; j != dbBanksDb[b].numBanks; j++ ) {
	dbBankPatternType pattern;
	int p;
	
	printf( "   %s \"%s\" size:%lld ",
		dbMemTypeNames[dbBanksDb[b].banks[j].support],
		dbBanksDb[b].banks[j].name,
		dbBanksDb[b].banks[j].size );
	if ( (pattern = dbBanksDb[b].banks[j].pattern) == 0 ) {
	  printf( "!EMPTY!" );
	  exitStatus = 1;
	} else {
	  printf( "=>" );
	  for ( p = 0; p != DB_NUM_BANK_TYPES; p++ ) {
	    if ( DB_TEST_BIT( &pattern, p ) ) {
	      printf( " %s", dbBankNames[p] );
	      DB_FLIP_BIT( &pattern, p );
	    }
	  }
	  if ( pattern != 0 ) {
	    printf( " + residual=%08x", pattern );
	    exitStatus = 1;
	  }
	}
	printf( "\n" );
      }
    }
  }
} /* End of dumpBanks */

/* ==== Run control static database handlers ==== */
int loadRolesDB() {
  int status;

  if ( (status = dbLoadRoles()) != DB_LOAD_OK ) {
    char msg[ 1024 ];
    
    snprintf( msg,
	      sizeof(msg),
	      "Error during dbLoadRolesDb:%d \"%s\". System-dependent status ",
	      status,
	      dbDecodeStatus( status ) );
    perror( msg );
    fprintf( stderr, "\tSource line: %s\n", dbGetLastLine() );
    exitStatus = 1;
  }
  return status == DB_LOAD_OK;
} /* End of loadRolesDB */

void printRolesDb() {
  int i, j;

  printf( "Roles DB:\n" );
  
  if ( dbRolesDb == NULL || dbSizeRolesDb == 0 ) {
    printf( "  UNLOADED\n" );
    if ( dbRolesDb != NULL ) {
      printf( "  ??? dbRolesDb:%p ???\n", dbRolesDb );
      exitStatus = 1;
    }
    if ( dbSizeRolesDb != 0 ) {
      printf( "  ??? dbSizeRolesDb:%d ???\n", dbSizeRolesDb );
      exitStatus = 1;
    }
    return;
  }
  
  for ( i = 0; i != dbSizeRolesDb; i++ ) {
    printf( " %4d)", i );
    printf( " id:%4d", dbRolesDb[i].id );
    printf( " %s", dbDecodeRole( dbRolesDb[i].role ) );
    if ( dbRolesDb[i].role == dbRoleLdc ) {
      printf( " hltRole:%s", dbDecodeHltRole( dbRolesDb[i].hltRole ) );
    } else {
      if ( dbRolesDb[i].hltRole != dbHltRoleUndefined )
	printf( " ??? hltRole:%s ???",
		dbDecodeHltRole( dbRolesDb[i].hltRole ) );
    }
    printf( " %s\t", dbRolesDb[i].name );
    printf( " hostname:%s",
	    dbRolesDb[i].hostname == NULL ? "N/A   " : dbRolesDb[i].hostname);
    if ( ( dbGetSuperRole( dbRolesDb[i].role ) == inLdcMask
	|| dbGetSuperRole( dbRolesDb[i].role ) == inGdcMask )
	 && dbRolesDb[i].hostname == NULL ) {
      printf( " ???" );
      exitStatus = 1;
    }
    printf( "\t" );
    printf( " %s", dbRolesDb[i].description );
    printf( " madeOf:%s",
	    stripTrailing(dbDecodeRole( dbRolesDb[i].madeOf )) );
    if ( dbRolesDb[i].topLevel ) printf( " TOP-LEVEL" );
    if ( !dbRolesDb[i].active ) printf( " INACTIVE" );
    if ( dbRolesDb[i].bankDescriptor != -1 )
      printf( " bankDescriptor:%d", dbRolesDb[i].bankDescriptor );
    printf( "\n" );
  }
  for ( i = j = 0; dbSections[i].keyword != NULL; i++ ) {
    if ( dbSections[i].maxId != NULL ) {
      if ( *dbSections[i].maxId != -1 ) {
	printf( "%sMax %s:%d",
		j++ == 0 ? "\t" : ", ",
		stripTrailing(dbDecodeRole( dbSections[i].role )),
		*dbSections[i].maxId );
      }
    }
  }
  if ( j != 0 ) printf( "\n" );
} /* End of printRolesDb */

int sameRole( const dbRoleType role1, const dbRoleType role2 ) {
  if ( dbGetSuperRole( role1 ) == notInMask
    || dbGetSuperRole( role2 ) == notInMask )
    return role1 == role2;
  return dbGetSuperRole( role1 ) == dbGetSuperRole( role2 );
} /* End of sameRole */

/* Routine to check the roles of two entities who share the same name */
int checkRoles( const dbRoleType role1, const dbRoleType role2 ) {
  /* If same role, error */
  if ( role1 == role2 ) return 1;

  /* If different roles, it depends... */

  /* We accepts machines that can be either LDCs or GDCs (as long as they
   * are not both at the same time, it's OK */
  if ( dbGetSuperRole( role1 ) == inLdcMask
    && dbGetSuperRole( role2 ) == inGdcMask ) return 0;
  if ( dbGetSuperRole( role1 ) == inGdcMask
    && dbGetSuperRole( role2 ) == inLdcMask ) return 0;

  /* We accepts EDMs that are also LDCs, GDCs or trigger host */
  if ( (dbGetSuperRole( role1 ) == inLdcMask
     ||	dbGetSuperRole( role1 ) == inGdcMask
     ||	role1 == dbRoleTriggerHost)
    && role2 == dbRoleEdmHost ) return 0;
  if ( (dbGetSuperRole( role2 ) == inLdcMask
     ||	dbGetSuperRole( role2 ) == inGdcMask
     ||	role2 == dbRoleTriggerHost)
    && role1 == dbRoleEdmHost ) return 0;

  /* We accept trigger hosts that are also LDCs or EDMs
  */
  if ( (dbGetSuperRole( role1 ) == inLdcMask || role1 == dbRoleEdmHost) &&
       role2 == dbRoleTriggerHost ) return 2;
  if ( (dbGetSuperRole( role2 ) == inLdcMask || role2 == dbRoleEdmHost) &&
       role1 == dbRoleTriggerHost ) return 3;

  /* We accept GDCs which are trigger hosts (no need to check the id) */
  if ( dbGetSuperRole( role1 ) == inGdcMask
    && role2 == dbRoleTriggerHost ) return 0;
  if ( dbGetSuperRole( role2 ) == inGdcMask
    && role1 == dbRoleTriggerHost ) return 0;

  /* Anything else is not allowed... */
  return 1;
} /* End of checkRoles */

void checkRolesDb() {
  int i;

  if ( dbRolesDb == NULL ) {
    if ( dbSizeRolesDb != 0 ) {
      printf( " ??? dbRoles:%p dbSizeRolesDb:%d ???\n",
	      dbRolesDb, dbSizeRolesDb );
      exitStatus = 1;
    }
    return;
  }
  
  for ( i = 0; i != dbSizeRolesDb; i++ ) {
    int j;

    for ( j = i+1; j != dbSizeRolesDb; j++ ) {
      if ( strcmp( dbRolesDb[i].name, dbRolesDb[j].name ) == 0 ) {
	int checkResult = checkRoles( dbRolesDb[i].role, dbRolesDb[j].role );
	if ( checkResult == 1 ||
	     (checkResult == 2 && dbRolesDb[i].id != dbRolesDb[j].id ) ) {
	  printf( " ???" );
	  printf( " Rule:%d", i );
	  printf( " conflict name/role/ID for \"%s\" with rule:%d",
		  dbRolesDb[i].name, j );
	  printf( " ???\n" );
	  exitStatus = 1;
	}
      }
      if ( dbRolesDb[i].id == dbRolesDb[j].id
	&& sameRole( dbRolesDb[i].role, dbRolesDb[j].role )) {
	printf( " ???" );
	printf( " Rule:%d", i );
	printf( " duplicated ID %d", dbRolesDb[i].id );
	printf( " for \"%s\"", dbRolesDb[i].name );
	printf( " with rule:%d", j );
	printf( " ???\n" );
	exitStatus = 1;
      }
    }
  }
} /* End of checkRolesDb */

void dumpRoles() {
  printRolesDb();
  checkRolesDb();
} /* End of dumpRoles */

/* ===== Check and dump global parameters ===== */

#ifdef NOT_DEFINED
/* Update a current ldcPattern using a given detector id */
int updateLdcPattern( const dbIdType id, dbLdcPatternType ldcPattern ) {
  int numErrors = 0;
  int numMatches = 0;
  int i;

  for ( i = 0; i != dbSizeDetectorsDb; i++ ) {
    if ( dbDetectorsDb[i].id == id ) {
      int j;
      
      numMatches++;
      for ( j = 0; j != DB_WORDS_IN_LDC_MASK; j++ ) {
	ldcPattern[j] |= dbDetectorsDb[i].componentPattern[j];
      }
      if ( numMatches == 2 ) {
	printf( " <DETECTOR MULTIPLY DEFINED>" );
      }
    }
  }
  if ( numMatches == 0 ) {
    printf( " <DETECTOR NOT DEFINED>" );
  }
  if ( numMatches != 1 ) numErrors++;

  return numErrors;
} /* End of updateLdcPattern */
#endif

/* Print the parameters that concern the databases as a unique entity */
void printGlobalParameters() {
#ifdef NOT_DEFINED
  int r;
  int t;
  int numErrors = 0;
  int nMatches = 0;
  int i;

  printf( "Trigger paths:\n" );
  for ( t = 0; t != dbSizeTriggersDb; t++ ) {
    dbLdcPatternType ldcPattern;
    
    printf(   "%4d) ", dbTriggersDb[t].id );
    for ( nMatches = 0, r = 0; r != dbSizeRolesDb; r++ ) {
      if ( dbRolesDb[r].role == dbRoleTriggerMask
	&& dbRolesDb[r].id   == dbTriggersDb[t].id ) {
	if ( nMatches++ != 0 ) printf( "+" );
	printf( "%s", dbRolesDb[r].name );
      }
    }
    if ( nMatches == 0 ) printf( "<UNKNOWN>" );
    if ( nMatches != 1 ) numErrors++;
    printf( "\n" );
    memset( ldcPattern, 0, sizeof( ldcPattern ) );
    printf( "\t" );
    nMatches = 0;
    if ( dbDetectorsDb != NULL ) {
      int d;

      for ( d=0; d <= MAX( dbMaxDetectorId, EVENT_DETECTOR_ID_MAX )+1; d++ ) {
	if ( DB_TEST_BIT( dbTriggersDb[i].detectorPattern, d ) ) {
	  nMatches++;
	  if ( d > dbMaxDetectorId || d > EVENT_DETECTOR_ID_MAX ) {
	    printf( " ???" );
	    printf( " Trigger:%d", dbTriggersDb[t].id );
	    printf( " associated to detector:%d", d );
	    printf( " dbMaxDetectorId:%d EVENT_DETECTOR_ID_MAX:%d",
		    dbMaxDetectorId, EVENT_DETECTOR_ID_MAX );
	    printf( " ???" );
	    exitStatus = 1;
	  }
	}
      }
    }
    if ( nMatches == 0 ) {
      printf( "No associated detector?" );
      numErrors++;
    } else {
      if ( dbDetectorsDb != NULL ) {
	int d;

	
	for ( d=0; d <= MAX( dbMaxDetectorId,EVENT_DETECTOR_ID_MAX )+1; d++ ) {
	  if ( DB_TEST_BIT( dbTriggersDb[t].detectorPattern, d ) ) {
	    int r;

	    if ( d > dbMaxDetectorId || d > EVENT_DETECTOR_ID_MAX ) {
	      printf( "???" );
	      printf( " Trigger:%d", dbTriggersDb[d].id );
	      printf( " associated to detector:%d", d );
	      printf( " dbMaxDetectorId:%d EVENT_DETECTOR_ID_MAX:%d",
		      dbMaxDetectorId, EVENT_DETECTOR_ID_MAX );
	      printf( " ???" );
	      exitStatus = 1;
	    }
	    
	    for ( nMatches = 0, r = 0; r != dbSizeRolesDb; r++ ) {
	      if ( dbRolesDb[r].role == dbRoleDetector &&
		   dbRolesDb[r].id == d ) {
		printf( "%s%s",
			nMatches++ == 0 ? " " : "+",
			dbRolesDb[r].name );
		if ( nMatches != 1 ) numErrors++;
		numErrors += updateLdcPattern( d, ldcPattern );
	      }
	    }
	    if ( nMatches == 0 ) {
	      printf( "UNKNOWN:%d", d );
	      numErrors++;
	    }
	  }
	}
      }
    }
    printf( "\n" );
    if ( dbMaxLdcId != -1 ) {
      int l;
      printf( "\t   " );

      for ( l = 0; l != MAX( dbMaxLdcId, HOST_ID_MAX )+1; l++ ) {
	if ( DB_TEST_BIT( ldcPattern, l ) ) {
	  if ( l > dbMaxLdcId || l > HOST_ID_MAX ) {
	    printf( "??? " );
	    printf( " Trigger:%d", dbTriggersDb[t].id );
	    printf( " associated to LDC:%d", l );
	    printf( " dbMaxLdcId:%d HOST_ID_MAX:%d", dbMaxLdcId, HOST_ID_MAX );
	    printf( " ???" );
	    exitStatus = 1;
	  }
	  for ( nMatches = 0, r = 0; r != dbSizeRolesDb; r++ ) {
	    if ( dbGetSuperRole( dbRolesDb[r].role ) == inLdcMask
	      && dbRolesDb[r].id == i ) {
	      printf( "%s%s", nMatches++ == 0 ? "" : "+", dbRolesDb[r].name );
	    }
	  }
	  if ( nMatches == 0 ) {
	    printf( "??? #%d ???", i );
	    exitStatus = 1;
	  }
	  if ( nMatches != 1 ) numErrors++;
	  printf( " " );
	}
      }
    }
    printf( "\n" );
  }

  SEPARATOR;
  
  if ( numErrors != 0 ) {
    printf( " => %d semantic error%s detected <=\n",
	    numErrors, numErrors == 1 ? "" : "s" );
    SEPARATOR;
    exitStatus = 1;
  }
#endif
} /* End of printGlobalParameters */

static int goodResult = 300;
float doTiming( int searchRoutine( const char * const, const dbRoleType ),
	      char * const name,
	      const dbRoleType role ) {
  int nLoops = 10;
  clock_t start, end;

  do {
    int i;
    nLoops *= 5;
    start = clock();
    for ( i = 0; i != nLoops; i++ )
      (void)searchRoutine( name, role );
    end = clock();
  } while ( ( end - start ) < goodResult);
  return 100 * ( end - start ) / nLoops;
} /* End of doTiming */

/* Repeat a search until a meaningful CPU time is measured */
int measureTime( int searchRoutine( const char * const, const dbRoleType ),
		 char * const name,
		 const dbRoleType role ) {
#define N_SAMPLES 10
  float samples[ N_SAMPLES ];
  int i;
  float avg;
  int accepted;
  float min, max;
  int minId, maxId;

  do {
    /* A first sample to be thrown away (warmup) */
    doTiming( searchRoutine, name, role );
    
    /* Take the requested number of samples */
    for ( i=0; i!=N_SAMPLES; i++ ) {
      samples[ i ] = doTiming( searchRoutine, name, role );
    }

    /* Find the maximum and the minimum values (most likely to be wrong) */
    for ( min=samples[0], max=samples[0], i=1; i != N_SAMPLES; i++ ) {
      if ( samples[i] < min ) min = samples[i];
      else if ( samples[i] > max ) max = samples[i];
    }

    /* Calculate the average excluding (once) the minimum and maximum values */
    for ( minId=-1, maxId=-1, avg=0.0, i=0; i != N_SAMPLES; i++ ) {
      if ( minId == -1 && samples[i] == min ) {
	minId = i;
      } else if ( maxId == -1 && samples[i] == max ) {
	maxId = i;
      } else {
	avg += samples[i];
      }
    }
    avg /= ( N_SAMPLES - 2 );

    /* We accept the measurement only if these do not exceed a given variation
     * threshold */
    accepted = TRUE;
    for ( i = 0; i != N_SAMPLES && accepted; i++ ) {
      if ( i != minId && i != maxId ) {
	double delta = ( samples[i] - avg ) / avg;
	if ( delta < 0.0 ) delta = -delta;
	accepted = delta < 0.20;
      }
    }
    
    /* If we did not made it, sample longer next time */
    if ( !accepted ) goodResult *= 1.5;

    /*
    printf( "\n[ " );
    for ( i = 0; i != N_SAMPLES; i++ ) {
      if ( i == minId ) printf( "min=" );
      if ( i == maxId ) printf( "max=" );
      printf( "%.2f ", samples[i] );
    }
    printf( "] " );
    printf( "avg:%.3f accepted:%s   ", avg, accepted ? "YES" : "NO" );
    */
    
  } while ( !accepted );
  return avg;
} /* End of measureTime */

int mySearch( const char * const name, const dbRoleType role ) {
  int r;

  for ( r = 0; r != dbSizeRolesDb; r++ ) {
    if ( dbRolesDb[r].role == role || role == dbRoleUnknown ) {
      if ( strcmp( name, dbRolesDb[r].name ) == 0 ) {
	return r;
      }
    }
  }
  return -1;
} /* End of mySearch */

void checkHash( int i, char * const check ) {
  char *name;
  dbRoleType role;
  int time1 = -1;
  int time2 = -1;
  double delta;
  
  if ( i == -1 ) {
    name = "xyzyyz";
    role = dbRoleUnknown;
    while ( dbRolesFind( name, role ) != -1 )
      *(int *)name = *(int *)name + 1;
  } else {
    name = dbRolesDb[i].name;
    role = dbRoleUnknown;
  }

  if ( mySearch( name, role ) != dbRolesFind( name, role )) {
    int r;
      
    printf( "Mismatch of results: " );
    printf( "search( name:\"%s\" role:%s )", name, dbDecodeRole( role ) );
    r = mySearch( name, role );
    printf( "(%d id:%d name:\"%s\") ", r, dbRolesDb[r].id, dbRolesDb[r].name );
    r = dbRolesFind( name, role );
    printf( "(%d id:%d name:\"%s\") ", r, dbRolesDb[r].id, dbRolesDb[r].name );
    printf( "\n" );
  } else {
    time1 = measureTime( mySearch, name, role );
    time2 = measureTime( dbRolesFind, name, role );
  }

  if ( FALSE ) printf( "\tindex:%d %s %s", i, name, dbDecodeRole( role ) );

  if ( time1 > time2 )
    delta = 100.0*(double)(time1-time2)/(double)time2;
  else
    delta = 100.0*(double)(time2-time1)/(double)time2;
  printf( "\t%s\tLinear:%5d\tHash:%5d",
	  check,
	  time1,
	  time2 );
  if ( delta != 0 )
    printf( "\tHash is %03.2f%% %s",
	    delta,
	    time1 > time2 ? "faster" : "slower" );
  printf( "\n" );
} /* End of checkHash */

/* Perform extra global checks */
void checkGlobalParameters() {
  if ( doHashTableCheck ) {
    printf( "Search time:\n" );

    checkHash( 0, "start-of-list" );
    checkHash( dbSizeRolesDb / 3, "third-of-list" );
    checkHash( dbSizeRolesDb / 2, "middle-of-list" );
    checkHash( dbSizeRolesDb * 2 / 3, "2/3-of-list" );
    checkHash( dbSizeRolesDb-1, "end-of-list" );
    checkHash( -1, "not-in-list" );

    SEPARATOR;
  }
} /* End of checkGlobalParameters */

void dumpGlobalParameters() {
  printGlobalParameters();
  checkGlobalParameters();
} /* End of dumpGlobalParameters */

/* ------------------------------------------------------------------------- */
/* Handle the command line arguments and switches */
void handleArgs( const int argc, char ** const argv ) {
  int c;
  int error = FALSE;
  int silent = FALSE;
  extern int optind;
  
  while ( (c = getopt( argc, argv, "st" )) != EOF ) {
    switch (c) {
    case 's' :
      silent = TRUE;
      break;
    case 't' :
      doHashTableCheck = TRUE;
      break;
    case '?' :
      error = TRUE;
    }
  }
  if ( error || optind != argc ) {
    fprintf( stderr,
	     "usage: %s [-s][-h]\n\
\t-s: silent (suppress all output)\n\
\t-t: hashTable timing check\n\
",
	     argv[0] );
    exit(2);
  }
  if ( silent )
    freopen( "/dev/null", "w", stdout );
} /* End of handleArgs */

int main( const int argc, char ** const argv ) {
  handleArgs( argc, argv );

  loadRolesDB();
  loadTriggerDB();
  loadDetectorsDB();
  loadBanksDB();
  loadEventBuildingControlDB();
  
  dumpRoles();                SEPARATOR;
  dumpTrigger();              SEPARATOR;
  dumpDetectors();            SEPARATOR;
  dumpBanks();                SEPARATOR;
  dumpEventBuildingControl(); SEPARATOR;
  dumpGlobalParameters();
  exit( exitStatus );
} /* End of main */
