#!/usr/bin/tclsh

set timeloop [lindex $argv 0]
set outfile  [lindex $argv 1]

if { ([string length $timeloop]==0) || ([string length $outfile]==0) } {
  puts "Usage: mysqld_sensor.tcl timeout outfile"
  exit 1
}

puts "Time loop:   $timeloop"
puts "Output file: $outfile"


# Add mysqltcl path
if { [exec arch ] == "i686" } { 
  lappend auto_path $env(DATE_ROOT)/local
}
if { [exec arch ] == "x86_64" } {
  lappend auto_path $env(DATE_ROOT)/local64
}

# define connection parameters
set db_user $env(DATE_DB_MYSQL_USER)
set db_pwd $env(DATE_DB_MYSQL_PWD)
set db_host $env(DATE_DB_MYSQL_HOST)
set db_db $env(DATE_DB_MYSQL_DB)

if {[string length $db_user]==0} {
  puts "Undefined DATE_DB_MYSQL_USER"
  exit -1
}
if {[string length $db_host]==0} {
  puts "Undefined DATE_DB_MYSQL_HOST"
  exit -1
}
if {[string length $db_pwd]==0} {
  puts "Undefined DATE_DB_MYSQL_PWD"
  exit -1
}
if {[string length $db_db]==0} {
  puts "Undefined DATE_DB_MYSQL_DB"
  exit -1
}

puts -nonewline "Connecting infologger database :$db_user@$db_host:$db_db ... "
flush stdout
if [ catch {package require mysqltcl} ] {
  puts "Error - package mysqltcl required"
  exit 1
}

if [ catch {set db [mysqlconnect -host $db_host -user $db_user -password $db_pwd -db $db_db]} ] {
  puts "Error - failed to connect to database"
  exit 1
}
puts " ok"


# open output file
set fd [open $outfile "w"]


#set statusvars {"Connections" "Threads_connected" "Threads_created" "Bytes_received" "Bytes_sent" "Max_used_connections" "Com_insert" "Com_select"}
set statusvars {"Connections" "Threads_connected" "Max_used_connections" "Com_insert" "Com_select"}

foreach s $statusvars {
  set last($s) -1
  set diff($s) 0
}
set last_fout ""
set last_t 0
set last_t_p 0

while {1} {

    set t [clock format [clock seconds] -format "%d/%m/%Y %T"]
    set vars [mysqlsel $db "show status" -list]
    
    foreach v $vars {
      set name  [lindex $v 0]
      set value [lindex $v 1]
      
      foreach s $statusvars {
        if {$name==$s} {
#          puts "$name \t $value"
          
          if {$last($s)!=-1} {
            set diff($s) [expr $value - $last($s)]
          }
          set last($s) $value
          break
        }
      }
    }
    
    set fout "$diff(Connections)\t$last(Threads_connected)\t$last(Max_used_connections)\t$diff(Com_insert)\t$diff(Com_select)"
    
    if {$fout!=$last_fout} {
        if {($last_fout!="")&&($last_t_p!=$last_t)} {
          puts $fd "$last_t\t$last_fout"
        }
        puts $fd "$t\t$fout"
        set last_t_p $t
    }

    set last_fout $fout
    set last_t $t
    
    flush $fd
        
    # sleep until timeout
    set timeout 0
    after $timeloop {set timeout 1}
    vwait timeout
}

# close db
mysqlclose $db

# close output file
close $fd
