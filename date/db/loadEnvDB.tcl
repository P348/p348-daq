#!/usr/bin/tclsh

# read environment variables from MySQL DB if configured
# (do nothing if variable DATE_DB_LOAD=FALSE)
# output options:
# -b : print commands to export values in bash
# -c : print commands to export values in csh
# -s : print list of NAME = VALUE on stdout (default)

proc print_usage {} {
  puts "loadEnvDB \[-b|-c|-s\] \[-C CLASS|-A\]"
  puts " -b : print commands to export values in bash"
  puts " -c : print commands to export values in csh"
  puts " -s : print list of NAME = VALUE on stdout (default)"
  puts " -C : load environment variables of the specified CLASS."
  puts "      This option can be repeated to retrieve several classes."
  puts " -A : load all environment variables."
  
  puts "\nThis utility gets environment variables from database."
  puts "\nIf -A or -C not specified, only default variables are returned."
  puts "\nTo define these variables in the current shell, use:"
  puts "eval `loadEnv -b` (for bash)"
  puts "eval `loadEnv -c` (for bash)"
}

set x 0
set mode "stdout"
set class_mode "default"
set class_name {}

while {[set opt [lindex $argv $x]] != ""} {
  switch -exact -- $opt {
    -b {
      set mode "bash"
    }
    -c {
      set mode "csh"
    }
    -s {
      set mode "stdout"
    }
    -h {
      print_usage
      exit 0
    }
    -A {
      set class_mode "all"
    }
    -C {
      incr x
      set class_mode "select"
      lappend class_name [lindex $argv $x]
    }
    default {
      print_usage
      exit 1    
    }
  }
  incr x
}

set cmd {}

proc err_msg {msg} {
  global mode
  switch -exact -- $mode {
    "stdout" {
    puts "$msg"
    }
    "bash" {
    puts "echo \"$msg\";\n"
    }
    "csh" {
    puts "echo \"$msg\";\n"
    }
  }
}



catch {
  if {[llength [array names env "DATE_DB_MYSQL"]]==0} {
    exit 0
  }
  if {$env(DATE_DB_MYSQL)!="TRUE"} {
    exit 0
  }

  # do nothing if specified
  if {[llength [array names env "DATE_DB_LOAD"]]!=0} {
    if {$env(DATE_DB_LOAD) == "FALSE"} {
      exit 0
    }
  }

  
  # define connection parameters
  set db_user $env(DATE_DB_MYSQL_USER)
  set db_pwd $env(DATE_DB_MYSQL_PWD)
  set db_host $env(DATE_DB_MYSQL_HOST)
  set db_db $env(DATE_DB_MYSQL_DB)

  if {[string length $db_user]==0} {
    return 1
  }
  if {[string length $db_host]==0} {
    return 1
  }
  if {[string length $db_pwd]==0} {
    return 1
  }
  if {[string length $db_db]==0} {
    return 1
  }    

  # Add mysqltcl path
  if { [exec arch ] == "i686" } { 
    lappend auto_path $env(DATE_ROOT)/local
  }
  if { [exec arch ] == "x86_64" } {
    lappend auto_path $env(DATE_ROOT)/local64
  }

  if [ catch {package require mysqltcl 3.0} ] {
    err_msg "Package mysqltcl required"
    return 1
  }

  #puts "mysql -h $db_host -u $db_user -p$db_pwd $db_db"

  if [ catch {set db [mysqlconnect -host $db_host -user $db_user -password $db_pwd -db $db_db]} ] {
    err_msg "Failed to connect to $db_host as user $db_user with pwd $db_pwd for DB $db_db"
    return 2
  }



  # check db version
  set dbversion "v9"
  set query "select VALUE from GLOBALS where NAME='DB Version'"
  foreach row [mysqlsel $db "$query" -list] {
    if { "$row" != "$dbversion" } {
      err_msg "Wrong database version $row, should be $dbversion."
      return 1
    }
  }

  switch -exact -- $class_mode {
    all -
    select {
      set query "select NAME,VALUE from ENV where not (VALUE IS NULL)"
      set x 0
      foreach c $class_name {
        if {$x==0} {
          set query "$query AND ("
        } else {
          set query "$query OR"
        }
        set query "$query CLASS=\"$c\""
        incr x
      }
      if {$x!=0} { set query "$query )" }
    }
    default {
      set query "select NAME,VALUE from ENV where LOAD_BY_DEFAULT=1 and not (VALUE IS NULL)"
    }
  }

  foreach row [mysqlsel $db "$query" -list] {
    set name [lindex $row 0]
    set value [lindex $row 1]
    switch -exact -- $mode {
      "stdout" {
        puts "$name = $value"
      }
      "bash" {
        puts "export $name=\"$value\";"
      }
      "csh" {
        puts "setenv $name \"$value\";"
      }

    }
  }

  ::mysql::close $db
  
} err

if {$err!=""} {
  err_msg "Could not load environment from DB"
  return 1
}

return 0
