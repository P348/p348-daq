/*      sqlMysql.c
 *      =======
 *
 * DATE SQL database routines (cf dbSql.h) implemented for MySQL.
 *
 * Revision history:
 *  1.03  01 Dec 2005  SC      Update column names (Membanks, Triggers)
 *  1.02  02 Aug 2005  SC      Changed definition of membanks table
 *  1.01  23 Sep 2004  SC      Integration to v4.10
 *  1.00  23 Jun 2004  SC      First release
 */

 
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>

#include <mysql.h>

#include "infoLogger.h"

#include "dbSql.h"
#include "dateDb.h"
#include "dbConfig.h"


/* debugging mode - verbose info messages */
static char dbsql_log_debug=0;


/* DB access - default definitions */
#define MYSQL_HOST   ""          /* database server name */
#define MYSQL_DB     ""          /* database name */
#define MYSQL_USERID ""          /* database user id */
#define MYSQL_PASSWD ""          /* userid password */


#define LOG_STREAM "db"

/***************************************************************/
/* Various utilities                                           */
/***************************************************************/

/* strdup which works with NULL */
char * safe_strdup(const char *s){
  if (s==NULL) return NULL;
  return strdup(s);
}




/****************************************************************/
/* DATE SQL DB access library                                   */
/****************************************************************/

/* constants */
#define MAX_QUERY_SIZE 2000      /* maximum size of a SQL query */


/* dbsql global variables */
MYSQL dbsql_mysql;                 /* MYSQL structure for db connection */
int   dbsql_mysql_is_init=0;       /* set to 1 when mysql init done */
int   dbsql_mysql_is_connected=0;  /* set to 1 (or more) when mysql connected */

char v_dbsql_buffer[MAX_QUERY_SIZE];     /* a buffer for SQL queries */
int  v_dbsql_chars;                      /* number of chars in the current query */



/*................................................................
    check if the DATE database is configured
    returns: 1 if configured, 0 if not
................................................................*/
int dbsql_is_configured(){
  char *v;
   
  /* SQL DB activated? */
  v=getenv("DATE_DB_MYSQL");
  if (v==NULL) return 0;
  if (strcmp(v,"TRUE")) return 0;
    
  return 1;
}


/*................................................................
    open DATE database
    returns: 0 on success, -1 on failure
................................................................*/

int dbsql_open(){
  char *userid,*passwd,*database,*host;
  char *debug;
  
  userid=getenv("DATE_DB_MYSQL_USER");
  if (userid==NULL) userid=MYSQL_USERID;
  passwd=getenv("DATE_DB_MYSQL_PWD");
  if (passwd==NULL) passwd=MYSQL_PASSWD;
  database=getenv("DATE_DB_MYSQL_DB");
  if (database==NULL) database=MYSQL_DB;
  host=getenv("DATE_DB_MYSQL_HOST");
  if (host==NULL) host=MYSQL_HOST;

  dbsql_log_debug=0;
  debug=getenv("DATE_DB_MYSQL_DEBUG");
  if (debug!=NULL) {
    if (!strcmp(debug,"TRUE")) {
      dbsql_log_debug=1;
    }
  }

  /* init MySQL library if needed*/
  if (!dbsql_mysql_is_init) {
    mysql_init(&dbsql_mysql);
    dbsql_mysql_is_init=1;
  }

  /* connect database if needed*/
  if (!dbsql_mysql_is_connected) {

    if (dbsql_log_debug) {
      /* Log connection parameters */
      infoLog_f(LOG_STREAM, LOG_INFO, "DATE SQL connection : host '%s', user '%s', database '%s'",host,userid,database);
    }

    if (!mysql_real_connect(&dbsql_mysql,host,userid,passwd,database,0,NULL,0)) {
      infoLog_f(LOG_STREAM, LOG_ERROR, "Failed to connect to database: %s",mysql_error(&dbsql_mysql));
      return -1;
    }
  }

  /* increase number of 'open' calls */
  dbsql_mysql_is_connected++;
    
  return 0;
}


/*................................................................
    close DATE database
    returns: always 0
................................................................*/

int dbsql_close(){

  /* close connection if needed, if nobody else uses the connexion */
  if (dbsql_mysql_is_connected==1) {
    mysql_close(&dbsql_mysql);
  }

  /* decrease number of 'open' calls */
  dbsql_mysql_is_connected--;

  return 0;
}



/* Map open/close routine to API */
int dbOpen(){
  return dbsql_open();
}
int dbClose(){
  return dbsql_close();
}




/*................................................................
    execute a SQL query to DATE database
    accepts strings "a la" printf with additionnal arguments
    returns: 0 on success, -1 on failure
................................................................*/

int dbsql_query(const char *query,...){

  char buffer_query[MAX_QUERY_SIZE];     /* the buffer for the SQL query */
  va_list ap;                            /* list of optionnal args */
  int query_size;                        /* query size */
  int result;                            /* error code */
  
  /* do nothing if not connected */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }
  
  /* format query */
  va_start(ap, query);
  query_size=vsnprintf(buffer_query,MAX_QUERY_SIZE,query,ap);
  va_end(ap);

  /* check that query doe not overrun buffer */
  if ((query_size<=0)||(query_size>MAX_QUERY_SIZE)) {
    /*
    printf("dbsql_query() : ");
    va_start(ap, query);
    vprintf(query,ap);
    va_end(ap);
    printf("\n");
    */
    infoLog_f(LOG_STREAM, LOG_INFO, "sql query '%s' ...", query);
    infoLog_f(LOG_STREAM, LOG_ERROR, "dbsql_query() : bad SQL query size (%d bytes). Maximum is %d bytes.",query_size,MAX_QUERY_SIZE);
    return -1;
  }

  if (dbsql_log_debug) {
    /* log query */
    infoLog_f(LOG_STREAM, LOG_INFO, "dbsql_query(): executing : %s",buffer_query);
  }
  
  /* execute query */
  result=mysql_query(&dbsql_mysql,buffer_query);
  
  /* explain error if any */
  if (result!=0) {
    infoLog_f(LOG_STREAM, LOG_INFO, "dbsql_query(): executing : %s",buffer_query);
    infoLog_f(LOG_STREAM, LOG_ERROR, "dbsql_query(): query failed : %s",mysql_error(&dbsql_mysql));
    return -1;
  }

  return 0;
}


/*................................................................
    create a new query
    returns: always 0
................................................................*/

int dbsql_query_new(){
  /* reinit string */
  v_dbsql_chars=0;
  v_dbsql_buffer[0]=0;
  return 0;
}


/*................................................................
    append a string to the current query
    accepts strings "a la" printf with additionnal arguments
    returns: 0 on success, -1 on failure
................................................................*/

int dbsql_query_append(const char *s,...){
  va_list ap;          /* list of optionnal args */
  int query_size;      /* size of string to be inserted to query */

  /* was the query completely stored in buffer up to now? */
  if (v_dbsql_chars==-1) return -1;
  
  /* format query */
  va_start(ap,s);
  query_size=vsnprintf(&v_dbsql_buffer[v_dbsql_chars],MAX_QUERY_SIZE-v_dbsql_chars,s,ap);
  va_end(ap);
  
  /* check buffer space */
  if ((query_size<=0)||(query_size>MAX_QUERY_SIZE-v_dbsql_chars)) {
    /* disable query, it was truncated */
    v_dbsql_chars=-1;
    return -1;
  }

  /* update query length */  
  v_dbsql_chars+=query_size;
  return 0;    
}


/*................................................................
    add string value between quotes (if not null) or NULL to the current query
    string processing possible with the following options (can be combined with '+')
    returns: 0 on success, -1 on failure
................................................................*/

#define DBSQL_OPT_NONE        0x0   /* no option */
#define DBSQL_OPT_WITH_COMA   0x1   /* option : add a coma in the query before the string */
#define DBSQL_OPT_STRIP_QUOTE 0x2   /* option : leading and trailing quotes (") are removed from string */

int dbsql_query_append_sval(const char *s_in,int options){
  const char *s=NULL;    /* string value to insert */
  char *s_copy=NULL;     /* copy of the string input if needed */
  int rval=-1;           /* return value */
  
  /* if quotes have to be removed, copy string and cut it */
  if ( (s_in!=NULL) && (options & DBSQL_OPT_STRIP_QUOTE) && (s_in[0]=='"') && (s_in[strlen(s_in)-1]=='"') ) {

    s_copy=strdup(&s_in[1]);         /* copy input string and remove starting quote */
    if (s_copy!=NULL) {
      s_copy[strlen(s_copy)-1]=0;   /* remove ending quote */
      s=s_copy;                     /* use the copy as the string to be inserted */
    }

  } else {

    s=s_in;                         /* no need to copy string, insert it directly */

  }

  /* add the string to the query */
  if (options & DBSQL_OPT_WITH_COMA) {
    
    /* with leading coma */
    if (s!=NULL) {
      rval=dbsql_query_append(",'%s'",s);
    } else {
      rval=dbsql_query_append(",NULL");
    }
    
  } else {
    
    /* without leading coma */
    if (s!=NULL) {
      rval=dbsql_query_append("'%s'",s);
    } else {
      rval=dbsql_query_append("NULL");
    }
   
  }
   
  /* free copy if any */
  if (s_copy!=NULL) {
    free(s_copy);
  }

  return rval;
}


/*................................................................
    execute current query
    returns: 0 on success, -1 on failure
................................................................*/

int dbsql_query_exec(){
  int result;                            /* error code */
  
  /* do nothing if not connected */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  /* is there a query to execute? */
  if (v_dbsql_chars==0) return -1;

  /* was the query completely stored in buffer? */
  if (v_dbsql_chars==-1) {
    infoLog_f(LOG_STREAM, LOG_ERROR, "dbsql_query_exec() : can not execute truncated query (%s...)",v_dbsql_buffer);
    return -1;
  }

  if (dbsql_log_debug) {
    /* log query */
    infoLog_f(LOG_STREAM, LOG_INFO, "dbsql_query_exec(): executing : %s",v_dbsql_buffer);
  }
  
  /* execute query */
  result=mysql_query(&dbsql_mysql,v_dbsql_buffer);
  
  /* explain error if any */
  if (result!=0) {
    infoLog_f(LOG_STREAM, LOG_INFO, "dbsql_query_exec(): executing : %s",v_dbsql_buffer);
    infoLog_f(LOG_STREAM, LOG_ERROR, "dbsql_query_exec(): query failed : %s",mysql_error(&dbsql_mysql));
    return -1;
  }

  return 0;
}


/*................................................................
    execute the queries listed in a given file
    returns: 0 on success, -1 on failure
................................................................*/

int dbsql_source(char * file){
  FILE *fp;
  char buffer[MAX_QUERY_SIZE];
  char *ptr;
  int end_query;
  
  fp=fopen(file,"r");
  if (fp==NULL) {
    infoLog_f(LOG_STREAM, LOG_ERROR, "dbsql_source: failed to open %s",file);
    return -1;
  }

  end_query=0;
  dbsql_query_new();
  for(;;){
    /* read line */
    if (fgets(buffer,MAX_QUERY_SIZE,fp)==NULL) break;

    /* skip comments */
    for(ptr=buffer;;ptr++){
      if (*ptr==0) break;
      if (!isspace(*ptr)) break;
    }       
    if (*ptr=='#') continue;
    
    /* is it the end? */
    for(ptr=&buffer[strlen(buffer)-1];ptr>=0;ptr--){
      if (!isspace(*ptr)) break;
    }       
    if (*ptr==';') {
      end_query=1;
      *ptr=0;
    }
        
    /* add to query */
    dbsql_query_append("%s",buffer);
    
    /* execute query if needed */
    if (end_query) {
      end_query=0;
      dbsql_query_exec();
      dbsql_query_new();     
    }      
  }
  
  fclose(fp);
  return 0;
}


/****************************************************************/
/* end of DATE SQL DB library                                   */
/****************************************************************/









/****************************************************************/
/* DATE DB package - interface implementation for SQL database  */
/****************************************************************/




/*................................................................
    Create database tables
    returns: 0 on success, -1 on failure
................................................................*/

int dbsql_create(){
  int i;
  char *dir;
  char path[MAX_QUERY_SIZE];

  /* check database connection status */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  /* disable constraints while creating tables */
  dbsql_query("SET FOREIGN_KEY_CHECKS=0");
 
  /* Roles */
  dbsql_query("CREATE TABLE IF NOT EXISTS ROLES \
    ( \
       NAME CHAR (32) UNIQUE NOT NULL, \
       HOSTNAME CHAR (32), \
       DESCRIPTION CHAR (32), \
       ROLE ENUM('Undefined','Unknown','GDC','LDC','EDM','Detector','Subdetector','Trigger-Host','Trigger-Mask','DDG','FILTER'), \
       ID BIGINT, \
       HLT_ROLE ENUM('Undefined','hltLdc','detectorLdc'), \
       TOPLEVEL BOOL, \
       ACTIVE BOOL, \
       MADEOF ENUM('Undefined','Unknown','GDC','LDC','EDM','Detector','Subdetector','Trigger-Host','Trigger-Mask','DDG','FILTER'), \
       PRIMARY KEY (ROLE,ID), \
       INDEX (ROLE,ID), \
       INDEX (NAME) \
    )  ENGINE = INNODB \
  ");

 
  /* Memory banks */
  dbsql_query_new();
  dbsql_query_append("CREATE TABLE IF NOT EXISTS MEMBANKS \
    ( \
      ROLE_NAME CHAR (32), \
      SUPPORT ENUM('undefined','ipc','bigphys','heap','physmem'), \
      KEY_PATH CHAR (255), \
      SIZE BIGINT, \
      PATTERN SET( \
  ");
  for(i=0;;i++){
    dbsql_query_append("\"%s\"",dbBankNames[i]);
    if (i>=DB_NUM_BANK_TYPES-1) break;
    dbsql_query_append(",");
  }  
  dbsql_query_append("), \
      ID INT NOT NULL AUTO_INCREMENT, \
      INDEX (ROLE_NAME), \
      FOREIGN KEY (ROLE_NAME) REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT, \
      PRIMARY KEY (ID) \
    ) ENGINE = INNODB \
  ");  
  dbsql_query_exec();


  /* Detectors */
  dbsql_query("CREATE TABLE IF NOT EXISTS DETECTORS \
    ( \
      COMPONENT CHAR (32) PRIMARY KEY, \
      PARENT    CHAR (32), \
      INDEX(COMPONENT), \
      INDEX(PARENT), \
      FOREIGN KEY (COMPONENT) REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT, \
      FOREIGN KEY (PARENT)    REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT \
    )  ENGINE = INNODB \
  ");


  /* Triggers */
  dbsql_query("CREATE TABLE IF NOT EXISTS TRIGGERS \
    ( \
      TRIGGER_NAME     CHAR (32), \
      DETECTOR    CHAR (32), \
      INDEX(TRIGGER_NAME), \
      INDEX(DETECTOR), \
      PRIMARY KEY (TRIGGER_NAME,DETECTOR), \
      FOREIGN KEY (TRIGGER_NAME)  REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT, \
      FOREIGN KEY (DETECTOR) REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT \
    )  ENGINE = INNODB \
  ");


  /* Event Building Control */
  dbsql_query("CREATE TABLE IF NOT EXISTS EVENT_BUILDING_RULES \
   ( \
      EVENT_TYPE ENUM('StartOfRun','EndOfRun','StartOfRunFiles','EndOfRunFiles','StartOfBurst','EndOfBurst','Physics','Calibration','StartOfData','EndOfData'), \
      PRIORITY BIGINT, \
      BUILD BOOL, \
      HLT_DECISION BOOL, \
      TYPE ENUM('Full Build','Detector Pattern','Trigger Pattern'), \
      PRIMARY KEY (EVENT_TYPE,PRIORITY), \
      INDEX(EVENT_TYPE), \
      INDEX(PRIORITY) \
    )  ENGINE = INNODB \
  ");
  
  dbsql_query("CREATE TABLE IF NOT EXISTS EVENT_BUILDING_PATTERNS \
    ( \
      EVENT_TYPE   ENUM('StartOfRun','EndOfRun','StartOfRunFiles','EndOfRunFiles','StartOfBurst','EndOfBurst','Physics','Calibration','StartOfData','EndOfData'), \
      PRIORITY     BIGINT, \
      COMPONENT    CHAR (32), \
      PRIMARY KEY  (EVENT_TYPE,PRIORITY,COMPONENT), \
      INDEX        (EVENT_TYPE), \
      INDEX        (PRIORITY), \
      INDEX        (COMPONENT), \
      FOREIGN KEY  (EVENT_TYPE,PRIORITY) REFERENCES EVENT_BUILDING_RULES(EVENT_TYPE,PRIORITY) ON UPDATE CASCADE ON DELETE RESTRICT, \
      FOREIGN KEY  (COMPONENT) REFERENCES ROLES(NAME) ON UPDATE CASCADE ON DELETE RESTRICT \
    )  ENGINE = INNODB \
  ");

  /* source RunControl code */
  dir=getenv("DATE_DB_BIN");
  if (dir==NULL) dir=".";
  snprintf(path,MAX_QUERY_SIZE,"%s//runControl_create.sql",dir);
  dbsql_source(path);

  memset(path,0,sizeof path);

  /* source defaultEquipment code */
  snprintf(path,MAX_QUERY_SIZE,"%s//equipment_create.sql",dir);
  dbsql_source(path);

  /* source db tables code */
  snprintf(path,MAX_QUERY_SIZE,"%s//db_create.sql",dir);
  dbsql_source(path);

  /* source DDL tables code */
  snprintf(path,MAX_QUERY_SIZE,"%s//DDL_create.sql",dir);
  dbsql_source(path);


  /* re-enable constraints */
  dbsql_query("SET FOREIGN_KEY_CHECKS=1");
  
  return 0;  
}



/*................................................................
    Destroy database tables
    returns: 0 on success, -1 on failure
................................................................*/

int dbsql_destroy(){
  char *dir;
  char path[MAX_QUERY_SIZE];

  /* check database connection status */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  /* disable constraints */
  dbsql_query("SET FOREIGN_KEY_CHECKS=0");
 
  dbsql_query("DROP TABLE IF EXISTS ROLES");
  dbsql_query("DROP TABLE IF EXISTS MEMBANKS");
  dbsql_query("DROP TABLE IF EXISTS DETECTORS");
  dbsql_query("DROP TABLE IF EXISTS TRIGGERS");  
  dbsql_query("DROP TABLE IF EXISTS EVENT_BUILDING_RULES");  
  dbsql_query("DROP TABLE IF EXISTS EVENT_BUILDING_PATTERNS");  

  /* source RunControl code */
  dir=getenv("DATE_DB_BIN");
  if (dir==NULL) dir=".";
  snprintf(path,MAX_QUERY_SIZE,"%s//runControl_destroy.sql",dir);
  dbsql_source(path);
      
  memset(path,0,sizeof path);

  /* source defaultEquipment code */
  snprintf(path,MAX_QUERY_SIZE,"%s//equipment_destroy.sql",dir);
  dbsql_source(path);

  /* source DDL tables code */
  snprintf(path,MAX_QUERY_SIZE,"%s//DDL_destroy.sql",dir);
  dbsql_source(path);

  /* source db tables code */
  snprintf(path,MAX_QUERY_SIZE,"%s//db_destroy.sql",dir);
  dbsql_source(path);
  
  /* re-enable constraints */
  dbsql_query("SET FOREIGN_KEY_CHECKS=1");
  
  return 0;
}




/*................................................................
    store ROLES in memory to DATE database
    returns: 0 on success, -1 on error
    warning: to be used on empty database only
................................................................*/

int dbsql_update_roles(){
  int i;
  int exit_status=0;  /* this is if everything goes well */
    
  /* check database connection status */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  printf( "Updating roles DB\n" );

  /* check RolesDb status */
  if ( dbRolesDb == NULL || dbSizeRolesDb == 0 ) {
    printf( "  UNLOADED\n" );
    if ( dbRolesDb != NULL ) {
      printf( "  ??? dbRolesDb:%p ???\n", dbRolesDb );
    }
    if ( dbSizeRolesDb != 0 ) {
      printf( "  ??? dbSizeRolesDb:%d ???\n", dbSizeRolesDb );
    }
    return -1;
  }
  
  /* create entities */
  for ( i = 0; i != dbSizeRolesDb; i++ ) {

    dbsql_query_new();
    dbsql_query_append("INSERT into %s VALUES(","ROLES");
    dbsql_query_append_sval(dbRolesDb[i].name,DBSQL_OPT_NONE);
    dbsql_query_append_sval(dbRolesDb[i].hostname,DBSQL_OPT_WITH_COMA);
    dbsql_query_append_sval(dbRolesDb[i].description,DBSQL_OPT_WITH_COMA + DBSQL_OPT_STRIP_QUOTE);
    dbsql_query_append_sval(dbDecodeRole( dbRolesDb[i].role ),DBSQL_OPT_WITH_COMA);
    dbsql_query_append(",'%d'",dbRolesDb[i].id);
    dbsql_query_append_sval(dbDecodeHltRole( dbRolesDb[i].hltRole ),DBSQL_OPT_WITH_COMA);
    dbsql_query_append(",'%d'",dbRolesDb[i].topLevel);
    dbsql_query_append(",'%d'",dbRolesDb[i].active);
    dbsql_query_append_sval(dbDecodeRole( dbRolesDb[i].madeOf ),DBSQL_OPT_WITH_COMA);
    dbsql_query_append(")");

    /* exec and stop on error */
    if (dbsql_query_exec()) {
      exit_status=-1;
      continue;
    }
   
  }
  
  return exit_status;
}





/*................................................................
    load ROLES in memory from DATE database
    returns: DB_LOAD_OK on success
................................................................*/
int dbsql_load_roles(){
  MYSQL_RES *result;
  MYSQL_ROW row;
  int n,i;
  int val;
  int failure;
  int s;
    
  /* no reload if already in memory */
  if ( dbRolesDb != NULL ) return DB_LOAD_OK;

  /* get ROLES from database */
  dbsql_query("SELECT NAME,HOSTNAME,DESCRIPTION,ID,ROLE,HLT_ROLE,TOPLEVEL,ACTIVE,MADEOF FROM ROLES ORDER BY ROLE,ID");

  /* load result in memory - otherwise can't know how many rows in result */
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) return DB_LOAD_ERROR;
  n=mysql_num_rows(result);

  /* allocate memory for given number of roles */
  dbRolesDb=malloc((n+1) * sizeof(dbRoleDescriptor));
  if (dbRolesDb==NULL) {
    perror("malloc for dbRolesDb failed");
    return DB_LOAD_ERROR;
  }
  dbSizeRolesDb=n;
  
  /* no failure yet */
  failure=0;
  
  /* loop over the roles */
  for (i=0;;i++) {

    /* init role descriptor */
    dbRolesDb[i].name=NULL;
    dbRolesDb[i].hostname=NULL;
    dbRolesDb[i].description=NULL;
    dbRolesDb[i].id=-1;
    dbRolesDb[i].role=dbRoleUnknown;
    dbRolesDb[i].hltRole=dbHltRoleUndefined;
    dbRolesDb[i].topLevel=FALSE;
    dbRolesDb[i].active=TRUE;
    dbRolesDb[i].madeOf=dbRoleUndefined;
    dbRolesDb[i].bankDescriptor=-1;

    /* stop when needed - but after 'empty' role is initialized at the 
       end of the list */
    if (i==n) break;

    /* get next role data */
    row = mysql_fetch_row(result);
    if (row==NULL) {
      failure=1;
      continue;
    }

    /* store role description */
    dbRolesDb[i].name=safe_strdup(row[0]);
    dbRolesDb[i].hostname=safe_strdup(row[1]);
    dbRolesDb[i].description=safe_strdup(row[2]);
    if ( (row[3]!=NULL) && (sscanf(row[3],"%d",&val)==1) ) dbRolesDb[i].id=val;
    dbRolesDb[i].role=dbEncodeRole(row[4]);
    dbRolesDb[i].hltRole=dbEncodeHltRole(row[5]);
    if ( (row[5]!=NULL) && (sscanf(row[6],"%d",&val)==1) ) dbRolesDb[i].topLevel=val;
    if ( (row[6]!=NULL) && (sscanf(row[7],"%d",&val)==1) ) dbRolesDb[i].active=val;
    dbRolesDb[i].madeOf=dbEncodeRole(row[8]);;
    dbRolesDb[i].bankDescriptor=-1;

    /* verify id range and update maxId in dbSection if needed*/
    for ( s = 0; dbSections[s].keyword != NULL; s++ ) {
      if (dbSections[s].role==dbRolesDb[i].role) break;
    }
    if (dbSections[s].keyword == NULL) {
      /* role not found! */
      failure=1;
      continue;
    }
    if ((dbRolesDb[i].id < dbSections[s].minIdValue) || (dbRolesDb[i].id > dbSections[s].maxIdValue)) {
      /* id not allowed */
      printf("%s : id %d out of range (%d <= id <= %d)\n",dbRolesDb[i].name,dbRolesDb[i].id,dbSections[s].minIdValue,dbSections[s].maxIdValue);
      failure=1;
      continue;
    }
    if (dbRolesDb[i].id > *dbSections[s].maxId) {
      *dbSections[s].maxId=dbRolesDb[i].id;
    }
    
  }

  /* free result */
  mysql_free_result(result);

  /* clean on error */
  if (failure) {
    dbUnloadRoles();
    return DB_LOAD_ERROR;
  }

  /* success */  
  return DB_LOAD_OK;
}




/*................................................................
    store MEMBANKS in memory to DATE database
    returns: 0 on success, -1 on error
    warning: to be used on empty database only
................................................................*/

int dbsql_update_membanks(){
  int i;
  int exit_status=0;  /* this is if everything goes well */
  
  /* check database connection status */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  printf( "Updating banks DB\n" );

  if ( dbBanksDb == NULL ) {
    printf( "  UNLOADED\n" );
    if ( dbSizeBanksDb != 0 ) {
      printf( "  ??? dbSizeBanksDb:%d ???\n", dbSizeBanksDb );
    }
    return -1;
  }
  
  for ( i = 0; i != dbSizeRolesDb; i++ ) {
    int b;
    
    if ( (b = dbRolesDb[i].bankDescriptor) != -1 ) {
      int j;
      
    
      for ( j = 0; j != dbBanksDb[b].numBanks; j++ ) {

        dbsql_query_new();
        dbsql_query_append("INSERT into %s VALUES(","MEMBANKS");
        dbsql_query_append("'%s'",dbRolesDb[i].name);
        dbsql_query_append(",NULL");        
        dbsql_query_append_sval(dbMemTypeNames[dbBanksDb[b].banks[j].support],DBSQL_OPT_WITH_COMA);
        dbsql_query_append_sval(dbBanksDb[b].banks[j].name,DBSQL_OPT_WITH_COMA);
        dbsql_query_append(",'%d'",dbBanksDb[b].banks[j].size);
        dbsql_query_append(",'%ld'",dbBanksDb[b].banks[j].pattern); /* does work with a set in mySQL - Warning, 64 bits max */
        dbsql_query_append(")");

        /* exec and stop on error */
        if (dbsql_query_exec()) {
          exit_status=-1;
          continue;
        }

      } 
    }
  }
  
  return exit_status;
}



/*................................................................
    load MEMBANKS in memory from DATE database
    returns: DB_LOAD_OK on success
................................................................*/


int dbsql_load_membanks(){
  MYSQL_RES *result;
  MYSQL_ROW row;
  int i;
  int val;
  int failure;
  char *role_name;
  dbBankDescriptor *newBankList,*bankList;
  dbSingleBankDescriptor *newSingleBank,*theBank;
          
  /* no reload if already in memory */
  if ( dbBanksDb != NULL ) return DB_LOAD_OK;

  /* load ROLES db first */
  if (dbLoadRoles() != DB_LOAD_OK) return DB_LOAD_ERROR;

  /* get MEMBANKS from database */
  dbsql_query("SELECT ROLE_NAME,SUPPORT,KEY_PATH,SIZE,PATTERN+0,ID FROM MEMBANKS");

  /* load result in memory - no need to further server access */
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) return DB_LOAD_ERROR;

  /* Init parsing */
  dbSizeBanksDb=0;
  failure=0;

  /* parse the result */
  while ((row = mysql_fetch_row(result))!=NULL) {

    /* read role name */
    role_name=row[0];
    if (role_name==NULL) continue;
          
    /* look for role/id in ROLES db */
    for (i=0;i<dbSizeRolesDb;i++) {

      /* we found the corresponding item in ROLES db ? */
      if (!strcmp(dbRolesDb[i].name,role_name)) {
        
        /* create a bankDescriptor if none existing yet */
        if (dbRolesDb[i].bankDescriptor==-1) {
          newBankList=realloc(dbBanksDb,(dbSizeBanksDb+1)*sizeof(dbBankDescriptor));
          if (newBankList==NULL) {
            failure=1;
            break;
          }
          newBankList[dbSizeBanksDb].numBanks=0;
          newBankList[dbSizeBanksDb].banks=NULL;
          dbBanksDb=newBankList;
          dbRolesDb[i].bankDescriptor=dbSizeBanksDb;
          dbSizeBanksDb++;
        }

        bankList=&(dbBanksDb[dbRolesDb[i].bankDescriptor]);
        
        /* append a new single bank descriptor to the list */
        newSingleBank=realloc(
          bankList->banks,
          (bankList->numBanks+1)*sizeof(dbSingleBankDescriptor)
        );
        if (newSingleBank==NULL) {
          failure=1;
          break;
        }
        bankList->banks=newSingleBank;
        theBank=&(bankList->banks[bankList->numBanks]);
        bankList->numBanks++;
       
        /* now fill in fields */
        theBank->support=dbEncodeMemType(row[1]);
        theBank->name=NULL;
        if (row[2]!=NULL) {
          if (strlen(row[2])!=0) {
            theBank->name=safe_strdup(row[2]);
          }
        }
        if (theBank->name==NULL) {
          theBank->name=safe_strdup(row[5]);
        }
        if ( (row[3]!=NULL) && (sscanf(row[3],"%d",&val)==1) ) theBank->size=val; else theBank->size=-1;
        if ( (row[4]!=NULL) && (sscanf(row[4],"%d",&val)==1) ) theBank->pattern=val; else theBank->pattern=0;
        
        /* expand bank patterns (readout -> readoutReadyFifo, readoutFirstLevel, ...) */
        int n;
        dbBankPatternType newpattern;
        newpattern=theBank->pattern;
        for (n=0;n<DB_NUM_BANK_TYPES;n++) {
          if (DB_TEST_BIT(&theBank->pattern, n)) {
            newpattern |= dbBankComponents[n];
          }
        }
        theBank->pattern=newpattern;
        
        break;
      }
    }  
  }  

  /* free result */
  mysql_free_result(result);

  /* add empty record at the end */
  newBankList=realloc(dbBanksDb,(dbSizeBanksDb+1)*sizeof(dbBankDescriptor));
  if (newBankList==NULL) {
    failure=1;
  } else {
    newBankList[dbSizeBanksDb].numBanks=0;
    newBankList[dbSizeBanksDb].banks=NULL;
    dbBanksDb=newBankList;
  }
  
  /* add an empty record at the end of each single descriptor list */
  for (i=0; i<=dbSizeBanksDb; i++){
        bankList=&(dbBanksDb[i]);
        newSingleBank=realloc(
          bankList->banks,
          (bankList->numBanks+1)*sizeof(dbSingleBankDescriptor)
        );
        if (newSingleBank==NULL) {
          failure=1;
          break;
        }
        bankList->banks=newSingleBank;
        theBank=&(bankList->banks[bankList->numBanks]);
        theBank->support=dbMemUndefined;
        theBank->name=NULL;
        theBank->size=0;
        theBank->pattern=0;       
  }

  /* clean on error */
  if (failure) {
    dbUnloadBanks();
    return DB_LOAD_ERROR;
  }

  /* success */  
  return DB_LOAD_OK;
}




/*................................................................
    store DETECTORS in memory to DATE database
    returns: 0 on success, -1 on error
    warning: to be used on empty database only
................................................................*/


int dbsql_update_detectors() {
  int i,j,k,l;
  int exit_status=0;  /* this is if everything goes well */
    
  /* check database connection status */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  printf( "Updating detectors DB\n" );

  if ( dbDetectorsDb == NULL ) {
    printf( "  UNLOADED\n" );
    if ( dbSizeDetectorsDb != 0 ) {
      printf( "  ??? dbSizedetectorsDb:%d ???\n", dbSizeDetectorsDb );
    }
    return -1;
  }
  
  if ( dbMaxDetectorId < EVENT_DETECTOR_ID_MIN
    || dbMaxDetectorId > EVENT_DETECTOR_ID_MAX ) {
    printf( "  ???" );
    printf( " dbMaxDetectorId:%d", dbMaxDetectorId );
    printf( " EVENT_DETECTOR_ID_MIN:%d", EVENT_DETECTOR_ID_MIN );
    printf( " EVENT_DETECTOR_ID_MAX:%d", EVENT_DETECTOR_ID_MAX );
    printf( " ???\n" );
    return -1;
  }
  
  /* loop over the detectors */
  for ( i = 0; i != dbSizeDetectorsDb; i++ ) {
     
    /* find the corresponding item in Roles DB */
    for ( j = 0; j != dbSizeRolesDb; j++ ) {
      if ( dbRolesDb[j].role == dbDetectorsDb[i].role
	&& dbRolesDb[j].id == dbDetectorsDb[i].id ) break;        
    }
    /* not found? */
    if ( j == dbSizeRolesDb ) {
      printf( " %s id:%3d ??? UNKNOWN ???\n", dbDecodeRole( dbDetectorsDb[i].role ), dbDetectorsDb[i].id );
      exit_status=-1;
      continue;
    }

    /* check role */
    if ( dbRolesDb[j].role != dbRoleDetector && dbRolesDb[j].role != dbRoleSubdetector ) {
      printf("Illegal role %s assigned to detector %s\n",
        dbDecodeRole( dbDetectorsDb[j].role ),
        dbRolesDb[j].name
      );
      exit_status=-1;        
      continue;
    }

    /* add record for this detector if not defined in a component pattern */
    /* first scan each defined detector pattern and match for id */
    for ( l = 0; l != dbSizeDetectorsDb; l++ ) {
      /* check id */
      if (!DB_TEST_BIT(dbDetectorsDb[l].componentPattern,dbRolesDb[j].id) ) continue;
      /* id not sufficient, check madeOf of parent */
      /* so look for parent in role DB */
      for ( k = 0; k != dbSizeRolesDb; k++ ) {
        if (   dbRolesDb[k].role == dbDetectorsDb[l].role
	    && dbRolesDb[k].id == dbDetectorsDb[l].id
            && dbRolesDb[k].madeOf == dbRolesDb[j].role ) {
            break;
        }
      }
      if (k!=dbSizeRolesDb) break;
    }
    /* this item is defined nowhere in a component pattern => it has no parent, add a record */
    if (l==dbSizeDetectorsDb) {
      dbsql_query_new();
      dbsql_query_append("INSERT into DETECTORS VALUES(");
      dbsql_query_append_sval(dbRolesDb[j].name,DBSQL_OPT_NONE);
      dbsql_query_append_sval(NULL,DBSQL_OPT_WITH_COMA);
      dbsql_query_append(")");
      /* exec and check errors */
      if (dbsql_query_exec()) {
        exit_status=-1;
      }
    }
            
    /* add records for each of this detector components */
    if ((dbRolesDb[j].madeOf==dbRoleLdc)||(dbRolesDb[j].madeOf==dbRoleSubdetector)) {
      for ( k = 0; k != dbSizeRolesDb; k++ ) {
        if (dbRolesDb[k].role != dbRolesDb[j].madeOf) continue;
        if (!DB_TEST_BIT( dbDetectorsDb[i].componentPattern, dbRolesDb[k].id)) continue;

        dbsql_query_new();
        dbsql_query_append("INSERT into DETECTORS VALUES(");
        dbsql_query_append_sval(dbRolesDb[k].name,DBSQL_OPT_NONE);
        dbsql_query_append_sval(dbRolesDb[j].name,DBSQL_OPT_WITH_COMA);
        dbsql_query_append(")");

        /* exec and stop on error */
        if (dbsql_query_exec()) {
          exit_status=-1;
          continue;
        }

      }    
    }    
  }
      
  return exit_status;
}



/*................................................................
    load DETECTORS in memory from DATE database
    returns: DB_LOAD_OK on success
................................................................*/


int dbsql_load_detectors(){
  MYSQL_RES *result;
  MYSQL_ROW row;
  int parent,component;
  int i;
  int failure;
  dbDetectorDescriptor *newList;
  
  /* no reload if already in memory */
  if ( dbDetectorsDb != NULL ) return DB_LOAD_OK;

  /* load ROLES db first */
  if (dbLoadRoles() != DB_LOAD_OK) return DB_LOAD_ERROR;

  /* get DETECTORS from database */
  dbsql_query("SELECT COMPONENT,PARENT FROM DETECTORS");

  /* load result in memory - no need to further server access */
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) return DB_LOAD_ERROR;

  /* Init parsing */
  failure=0;

  /* parse the result */
  while ((row = mysql_fetch_row(result))!=NULL) {
    /* look for component in ROLES db */
    if (row[0]==NULL) continue; /* can not happen as value not null */
    component=dbRolesFindName(row[0]);
    if (component==-1) {
      printf("Detector %s not defined in rolesDB\n",row[0]);
      failure=1;
      continue;
    }

    /* look for parent in ROLES db */
    if (row[1]!=NULL) {
      parent=dbRolesFindName(row[1]);
      if (parent==-1) {
        printf("Detector %s : parent %s not defined in rolesDB\n",row[0],row[1]);
        failure=1;
        continue; 
      }
      /* check role */
      if (dbRolesDb[component].role!=dbRolesDb[parent].madeOf) {
        printf("Detector %s is %s whereas parent %s made of %s\n",
          dbRolesDb[component].name,
          dbDecodeRole(dbRolesDb[component].role),
          dbRolesDb[parent].name,
          dbDecodeRole(dbRolesDb[parent].role)
        );
        failure=1;
        continue;
      }
    } else {
      /* nothing to do for NULL parents */
      continue;
    }
        
    /* extend DB if necessary - to include parent node */
    for (i=0;i<dbSizeDetectorsDb;i++) {
      if ((dbRolesDb[parent].role==dbDetectorsDb[i].role) && (dbRolesDb[parent].id==dbDetectorsDb[i].id)) break;
    }
    if (i==dbSizeDetectorsDb) {
      newList=realloc(dbDetectorsDb,sizeof(dbDetectorDescriptor)*(dbSizeDetectorsDb+1));
      if (newList==NULL) {
        failure=1;
        break;
      }
      dbDetectorsDb=newList;
      dbSizeDetectorsDb++;
      dbDetectorsDb[i].id=dbRolesDb[parent].id;
      dbDetectorsDb[i].role=dbRolesDb[parent].role;
      memset( dbDetectorsDb[i].componentPattern,
	  0,
	  sizeof(dbLdcPatternType) );

    }

    // printf("%s -> %s\n",dbRolesDb[parent].name,dbRolesDb[component].name);

    /* add component to parent 'componentPattern' */
    DB_SET_BIT(dbDetectorsDb[i].componentPattern,dbRolesDb[component].id);        
  }

  /* add empty record at the end */
  newList=realloc(dbDetectorsDb,sizeof(dbDetectorDescriptor)*(dbSizeDetectorsDb+1));
  if (newList==NULL) {
    failure=1;
  } else {
    dbDetectorsDb=newList;
    dbDetectorsDb[dbSizeDetectorsDb].id=-1;
    dbDetectorsDb[dbSizeDetectorsDb].role=dbRoleUndefined;
    memset( dbDetectorsDb[dbSizeDetectorsDb].componentPattern,
	  0,
	  sizeof(dbLdcPatternType) );
  }
 
 
  /* free result */
  mysql_free_result(result);

  /* clean on error */
  if (failure) {
    dbUnloadDetectors();
    return DB_LOAD_ERROR;
  }

  /* success */  
  return DB_LOAD_OK;
}




/*................................................................
    store TRIGGERS in memory to DATE database
    returns: 0 on success, -1 on error
    warning: to be used on empty database only
................................................................*/

int dbsql_update_triggers(){
  int i,j,mask_index;
  int exit_status=0;  /* this is if everything goes well */
    

  /* check database connection status */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  printf( "Updating triggers DB\n" );

  /* check TriggersDb status */
  if ( dbTriggersDb == NULL || dbSizeTriggersDb == 0 ) {
    printf( "  UNLOADED\n" );
    if ( dbTriggersDb != NULL ) {
      printf( "  ??? dbTriggersDb:%p ???\n", dbTriggersDb );
    }
    if ( dbSizeTriggersDb != 0 ) {
      printf( "  ??? dbSizeTriggersDb:%d ???\n", dbSizeTriggersDb );
    }
    return -1;
  }
  
  /* create entities */
  for ( i = 0; i != dbSizeTriggersDb; i++ ) {

    /* look for corresponding entry in roles */
    mask_index=dbRolesFindId(dbTriggersDb[i].id,dbRoleTriggerMask);
    if (mask_index==-1) {
      printf("Triggermask %d not found in rolesDB\n",dbTriggersDb[i].id);
      exit_status=-1;
      continue;
    }

    /* for each trigger mask, insert list of associated detectors */
    for ( j = 0; j != dbSizeRolesDb; j++ ) {
      /* check id of detectors in roles db to see if in pattern */
      if (    (dbRolesDb[j].role!=dbRoleDetector)
           || (!TEST_DETECTOR_IN_PATTERN(dbTriggersDb[i].detectorPattern,dbRolesDb[j].id))
          ) continue;

      /* add record to triggers table */
      dbsql_query_new();
      dbsql_query_append("INSERT into TRIGGERS VALUES(");
      dbsql_query_append_sval(dbRolesDb[mask_index].name,DBSQL_OPT_NONE);
      dbsql_query_append_sval(dbRolesDb[j].name,DBSQL_OPT_WITH_COMA);
      dbsql_query_append(")");

      /* exec and stop on error */
      if (dbsql_query_exec()) {
        exit_status=-1;
        continue;
      }

    }
      
  }
  
  return exit_status;
}




/*................................................................
    load TRIGGERS in memory from DATE database
    returns: DB_LOAD_OK on success
................................................................*/

int dbsql_load_triggers(){
  MYSQL_RES *result;
  MYSQL_ROW row;
  int i;
  int failure;
  dbTriggerDescriptor *newList;
  int role_mask_id;
  int role_detector_id;
 
  

  /* no reload if already in memory */
  if ( dbTriggersDb != NULL ) return DB_LOAD_OK;

  /* load ROLES db first */
  if (dbLoadRoles() != DB_LOAD_OK) return DB_LOAD_ERROR;

  /* get TRIGGERS from database */
  dbsql_query("SELECT TRIGGER_NAME,DETECTOR FROM TRIGGERS");

  /* load result in memory - no need to further server access */
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) return DB_LOAD_ERROR;

  /* Init parsing */
  dbSizeTriggersDb=0;
  failure=0;

  /* parse the result */
  while ((row = mysql_fetch_row(result))!=NULL) {   

    /* look for trigger mask in roles db */
    role_mask_id=dbRolesFind(row[0],dbRoleTriggerMask);
    if (role_mask_id==-1) {
      printf("Trigger Mask %s not defined in ROLES\n",row[0]);
      failure=1;
      continue;
    }
    
    /* look for detector in roles db */
    role_detector_id=dbRolesFind(row[1],dbRoleDetector);
    if (role_detector_id==-1) {
      printf("Trigger Mask %s not defined in ROLES\n",row[1]);
      failure=1;
      continue;
    }

    /* is there an entry for the trigger mask in triggersDb ?*/
    for (i=0; i<dbSizeTriggersDb; i++) {
      if (dbTriggersDb[i].id == dbRolesDb[role_mask_id].id) break;
    }
    /* if not, create one */
    if (i==dbSizeTriggersDb) {
      newList=realloc(dbTriggersDb,sizeof(dbTriggerDescriptor)*(dbSizeTriggersDb+1));
      if (newList==NULL) {
        failure=1;
        continue;
      } else {
        dbTriggersDb=newList;
        dbTriggersDb[dbSizeTriggersDb].id=dbRolesDb[role_mask_id].id;
        memset( dbTriggersDb[dbSizeTriggersDb].detectorPattern,
	      0,
	      sizeof(eventDetectorPatternType) );
        dbSizeTriggersDb++;
      }
    }
    
    /* add the detector to bit mask */
    DB_SET_BIT(dbTriggersDb[i].detectorPattern,dbRolesDb[role_detector_id].id);
  }

  /* free result */
  mysql_free_result(result);

  /* add empty record at the end */
  newList=realloc(dbTriggersDb,sizeof(dbTriggerDescriptor)*(dbSizeTriggersDb+1));
  if (newList==NULL) {
    failure=1;
  } else {
    dbTriggersDb=newList;
    dbTriggersDb[dbSizeTriggersDb].id=-1;
    memset( dbTriggersDb[dbSizeTriggersDb].detectorPattern,
	  0,
	  sizeof(eventDetectorPatternType) );
  }

  /* clean on error */
  if (failure) {
    dbUnloadTriggers();
    return DB_LOAD_ERROR;
  }

  /* success */  
  return DB_LOAD_OK;
}




/*................................................................
    store EVENT_BUILDING_RULES in memory to DATE database
    returns: 0 on success, -1 on error
    warning: to be used on empty database only
................................................................*/

int dbsql_update_eventBuildingControl(){
  int i,j,rule_index;
  char *type;
  int exit_status=0;  /* this is if everything goes well */
    

  /* check database connection status */
  if (!dbsql_mysql_is_connected) {
    return -1;
  }

  printf( "Updating eventBuildingControl DB\n" );

  /* check eventBuildingControlDb status */
  if ( dbEventBuildingControlDb == NULL || dbSizeEventBuildingControlDb == 0 ) {
    printf( "  UNLOADED\n" );
    if ( dbEventBuildingControlDb != NULL ) {
      printf( "  ??? eventBuildingControlDb:%p ???\n", dbEventBuildingControlDb );
    }
    if ( dbSizeEventBuildingControlDb != 0 ) {
      printf( "  ??? dbSizeEventBuildingControlDb:%d ???\n", dbSizeEventBuildingControlDb );
    }
    return -1;
  }
  
  /* create entities */
  for ( i = 0; i != dbSizeEventBuildingControlDb; i++ ) {

    /* find the index of this rule for its type - order is important! */
    rule_index=1;
    for (j=0;j<i;j++) {
      if (dbEventBuildingControlDb[j].eventType == dbEventBuildingControlDb[i].eventType) {
        rule_index++;
      }
    }
    
    /* define rule type */
    if (dbEventBuildingControlDb[i].type == fullBuild) {
      type = "Full Build";
    } else if (dbEventBuildingControlDb[i].type == useDetectorPattern) {
      type = "Detector Pattern";
    } else if (dbEventBuildingControlDb[i].type == useTriggerPattern) {
      type = "Trigger Pattern";
    } else {
      type = NULL;
    }

    /* add record to triggers table */
    dbsql_query_new();
    dbsql_query_append("INSERT into EVENT_BUILDING_RULES VALUES(");
    dbsql_query_append_sval(dbDecodeEventType(dbEventBuildingControlDb[i].eventType),DBSQL_OPT_NONE);
    dbsql_query_append(",%d",rule_index);
    dbsql_query_append(",%d",dbEventBuildingControlDb[i].build);
    dbsql_query_append(",%d",dbEventBuildingControlDb[i].hltDecision);
    dbsql_query_append_sval(type,DBSQL_OPT_WITH_COMA);  
    dbsql_query_append(")");

    /* exec and stop on error */
    if (dbsql_query_exec()) {
      exit_status=-1;
      continue;
    }

    /* add pattern to table if any */
    if (dbEventBuildingControlDb[i].type != fullBuild) {
      for (j=0;j<dbSizeRolesDb;j++){
        if (dbEventBuildingControlDb[i].type == useDetectorPattern) {
          if (dbRolesDb[j].role != dbRoleDetector) continue;
          if (!TEST_DETECTOR_IN_PATTERN(dbEventBuildingControlDb[i].pattern.detectorPattern,dbRolesDb[j].id)) continue;
        }
        if (dbEventBuildingControlDb[i].type == useTriggerPattern) {
          if (dbRolesDb[j].role != dbRoleTriggerMask) continue;
          if (!TEST_TRIGGER_IN_PATTERN(dbEventBuildingControlDb[i].pattern.triggerPattern,dbRolesDb[j].id)) continue;
        }
        dbsql_query_new();
        dbsql_query_append("INSERT into EVENT_BUILDING_PATTERNS VALUES(");
        dbsql_query_append_sval(dbDecodeEventType(dbEventBuildingControlDb[i].eventType),DBSQL_OPT_NONE);
        dbsql_query_append(",%d",rule_index);
        dbsql_query_append_sval(dbRolesDb[j].name,DBSQL_OPT_WITH_COMA);
        dbsql_query_append(")");
        if (dbsql_query_exec()) {
          exit_status=-1;
          continue;
        }
      }
    }
 
  }
  
  return exit_status;
}




/*................................................................
    load EVENT_BUILDING_RULES in memory from DATE database
    returns: DB_LOAD_OK on success
................................................................*/

int dbsql_load_eventBuildingControl(){
  MYSQL_RES *result;
  MYSQL_ROW row;
  int failure;
  dbEventBuildingRule *newRule;
  int val; 
  int i,j;  
  eventTypeType eventType;
  int role_id;
  
    
  /* no reload if already in memory */
  if ( dbEventBuildingControlDb != NULL ) return DB_LOAD_OK;

  /* load ROLES db first */
  if (dbLoadRoles() != DB_LOAD_OK) return DB_LOAD_ERROR;

  /* get EVENT BUILDING RULES from database */
  dbsql_query("SELECT EVENT_TYPE, BUILD, HLT_DECISION, TYPE FROM EVENT_BUILDING_RULES ORDER BY EVENT_TYPE,PRIORITY");

  /* load result in memory - no need to further server access */
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) return DB_LOAD_ERROR;

  /* Init parsing */
  dbSizeEventBuildingControlDb=0;
  failure=0;

  /* parse the result */
  while ((row = mysql_fetch_row(result))!=NULL) {

      newRule=realloc(dbEventBuildingControlDb,sizeof(dbEventBuildingRule)*(dbSizeEventBuildingControlDb+1));
      if (newRule==NULL) {
        failure=1;
        continue;
      } else {
        dbEventBuildingControlDb=newRule;
        dbEventBuildingControlDb[dbSizeEventBuildingControlDb].eventType=dbEncodeEventType(row[0]);
        dbEventBuildingControlDb[dbSizeEventBuildingControlDb].build=FALSE;
        if ( (row[1]!=NULL) && (sscanf(row[1],"%d",&val)==1) ) dbEventBuildingControlDb[dbSizeEventBuildingControlDb].build=val;
        dbEventBuildingControlDb[dbSizeEventBuildingControlDb].hltDecision=FALSE;
        if ( (row[2]!=NULL) && (sscanf(row[2],"%d",&val)==1) ) dbEventBuildingControlDb[dbSizeEventBuildingControlDb].hltDecision=val;        
        if (row[3]==NULL) {
          dbEventBuildingControlDb[dbSizeEventBuildingControlDb].type=fullBuild;
        } else if (!strcmp(row[3],"Full Build")) {
          dbEventBuildingControlDb[dbSizeEventBuildingControlDb].type=fullBuild;
        } else if (!strcmp(row[3],"Detector Pattern")) {
          dbEventBuildingControlDb[dbSizeEventBuildingControlDb].type=useDetectorPattern;
        } else if (!strcmp(row[3],"Trigger Pattern")) {
          dbEventBuildingControlDb[dbSizeEventBuildingControlDb].type=useTriggerPattern;
        } else {
          dbEventBuildingControlDb[dbSizeEventBuildingControlDb].type=fullBuild;
        }
        
        ZERO_DETECTOR_PATTERN( dbEventBuildingControlDb[dbSizeEventBuildingControlDb].pattern.detectorPattern );
        ZERO_TRIGGER_PATTERN( dbEventBuildingControlDb[dbSizeEventBuildingControlDb].pattern.triggerPattern );

        dbSizeEventBuildingControlDb++;
      }
  }
  
      
  /* free result */
  mysql_free_result(result);


  /* add empty record at the end */
  newRule=realloc(dbEventBuildingControlDb,sizeof(dbEventBuildingRule)*(dbSizeEventBuildingControlDb+1));
  if (newRule==NULL) {
    failure=1;
  } else {
    dbEventBuildingControlDb=newRule;
    dbEventBuildingControlDb[dbSizeEventBuildingControlDb].eventType=DB_UNDEFINED_EVENT_TYPE;
    dbEventBuildingControlDb[dbSizeEventBuildingControlDb].build=FALSE;
    dbEventBuildingControlDb[dbSizeEventBuildingControlDb].type=fullBuild;
    ZERO_DETECTOR_PATTERN( dbEventBuildingControlDb[dbSizeEventBuildingControlDb].pattern.detectorPattern );
    ZERO_TRIGGER_PATTERN( dbEventBuildingControlDb[dbSizeEventBuildingControlDb].pattern.triggerPattern );
  }
  

  /* get EVENT BUILDING PATTERNS from database */
  dbsql_query("SELECT EVENT_TYPE, PRIORITY, COMPONENT FROM EVENT_BUILDING_PATTERNS ORDER BY EVENT_TYPE,PRIORITY");

  /* load result in memory - no need to further server access */
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) return DB_LOAD_ERROR;

  /* parse the result */
  while ((row = mysql_fetch_row(result))!=NULL) {
    eventType=dbEncodeEventType(row[0]);
    val=0;
    if (row[1]!=NULL) {
      sscanf(row[1],"%d",&val);
    }
    
    /* look for corresponding Rule */
    j=0;
    for(i=0;i<dbSizeEventBuildingControlDb;i++){
      if (dbEventBuildingControlDb[i].eventType == eventType) {
        j++;
        if (j == val) break;
      }
    }
    
    /* look for component in Roles */
    role_id=-1;
    if (dbEventBuildingControlDb[i].type==useDetectorPattern) role_id=dbRolesFind(row[2],dbRoleDetector);
    if (dbEventBuildingControlDb[i].type==useTriggerPattern) role_id=dbRolesFind(row[2],dbRoleTriggerMask);
    if (role_id==-1) {
      printf("Component %s not defined in ROLES with good type\n",row[2]);
      failure=1;
      continue;
    }    

    /* set bit mask */
    if (dbEventBuildingControlDb[i].type==useDetectorPattern)
      SET_DETECTOR_IN_PATTERN(dbEventBuildingControlDb[i].pattern.detectorPattern,dbRolesDb[role_id].id);
    if (dbEventBuildingControlDb[i].type==useTriggerPattern) 
          SET_TRIGGER_IN_PATTERN(dbEventBuildingControlDb[i].pattern.triggerPattern,dbRolesDb[role_id].id);
   
  }

  /* clean on error */
  if (failure) {
    dbUnloadEventBuildingControl();
    return DB_LOAD_ERROR;
  }

  /* success */  
  return DB_LOAD_OK;
}


int dbGetDDLtoLDCtable(dbLdcPatternType *LDCmask, dbIdType **table, int *size){
  dbIdType *t=NULL;
  int s=0;
  int i;
  int nactive=0;
  MYSQL_RES *result;
  MYSQL_ROW row;
  dbIdType id_ldc;
  int  id_ddl;

  if (table==NULL) return -1;
  if (size==NULL) return -1;
  
  *table=t;
  *size=s;
  
  dbsql_open();

  /* get max id in table */
  dbsql_query("SELECT MAX(id) from DDLin");
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) goto return_error;
  row = mysql_fetch_row(result);
  if (row==NULL) goto return_error;
  if (sscanf(row[0],"%d",&s)!=1) goto return_error;
  s++; /* size of table=maxid+1 */
  mysql_free_result(result);


  /* get list of DDLs */  
  dbsql_query("SELECT T1.DDLin_id, T3.id FROM EQUIP_PARAM_RorcData as T1, DDLin as T2, ROLES as T3 \
               where T1.ACTIVE=1 and T1.DDLin_id is not null and T2.id=T1.DDlin_id and T3.NAME=T1.ldc \
               and T3.ACTIVE=1 and T3.ROLE='LDC'");

  /* load result in memory */
  result = mysql_store_result(&dbsql_mysql);
  if (result==NULL) goto return_error;

  /* init table */
  t=(dbIdType *)malloc(s * sizeof(dbIdType));
  if (t==NULL) goto return_error;;
  for (i=0;i<s;i++) {
    t[i]=LDC_ID_INACTIVE;
  }

  /* loop over the roles */
  for (;;) {

    /* get next role data */
    row = mysql_fetch_row(result);
    if (row==NULL) {break;}

    /* parse data */
    if (sscanf(row[0],"%d",&id_ddl)==0) continue;
    if (sscanf(row[1],"%d",&id_ldc)==0) continue;
    /* skip if ldc not in mask */
    if (!DB_TEST_BIT(*LDCmask,id_ldc)) continue;

    /* set ddl / ldc id  in table */
    /* if ldc id set in mask */
    t[id_ddl]=id_ldc;
    nactive++;
  }

  /* free result */
  mysql_free_result(result);
  dbsql_close();

  *table=t;
  *size=s;
  
  return nactive;
  
  return_error:
    if (result!=NULL) mysql_free_result(result);
    dbsql_close();
    return -1;
}
