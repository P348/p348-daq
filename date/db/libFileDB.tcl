# API to access the files stored in FILES table
#
# This API requires mysqltcl (>=3)
# 
# This file can be included with the following command:
#   source $env(DATE_DB_DIR)/libFileDB.tcl


# Examples:
#
# writeFileDB $db "testfile" {"This is first line" "This is second line"}
# writeFileDB $db "testfile" {"This is first line" "This is second line"} {EDITABLE 1 DESCRIPTION "this is a test file"}
# readFileDB $db "testfile" value
# foreach line $value {
#   puts "$line"
# }
# deleteFileDB $db "testfile"


# Read a file from database
# handle: mysqltcl handle to database
# path:   file name
# value:  variable name to store the file content.
#         The result is a list, each item of the list corresponding to one line of the file.
# Returns 0 in case of success, -1 otherwise.
proc readFileDB {dbhandle path value} {
  # read from table
  set query "SELECT VALUE FROM FILES WHERE PATH='$path' AND HOST=''"
  if { [catch {set result [mysqlsel $dbhandle $query -list]} ] !=0 } {return -1}
  if {[llength $result]!=1} {return -1}
 
  upvar value filecontent
  set filecontent [split [lindex [lindex $result 0] 0] ""]
  
  return 0
}



# Write a file to database
# handle: mysqltcl handle to database
# path:   file name
# value:  file content.
#         The result is a list, each item of the list corresponding to one line of the file.
# attributes: optionnal list of attributes/values
#             EDITABLE to define if file is seen from editDB (by default, set to 0)
#             CLASS to define which family this file belongs to
#             DESCRIPTION to comment on the file content
# Returns 0 in case of success, -1 otherwise.
proc writeFileDB {dbhandle path value {attributes {EDITABLE 0}} } {

  # create BLOB
  set blob [::mysql::escape [join $value "\n"]]
  
  # create attribute settings
  set l_attr {}
  set v_attr {}
  foreach {a_name a_value} $attributes {
    lappend l_attr ",$a_name"
    lappend v_attr ",'[::mysql::escape $a_value]'"
  }
  # insert in table
  set query "REPLACE INTO FILES (VALUE,PATH,HOST [join ${l_attr}]) VALUES('$blob','$path','' [join ${v_attr}])"
  if {[catch {mysqlexec $dbhandle $query}]!=0} {return -1}
  return 0
}



# Delete a file from database
# handle: mysqltcl handle to database
# path:   file name
# Returns 0 in case of success, -1 otherwise.
proc deleteFileDB {dbhandle path} {

  set query "DELETE FROM FILES WHERE PATH='$path' AND HOST=''"
  if {[catch {mysqlexec $dbhandle $query}]!=0} {return -1}
  return 0
}
