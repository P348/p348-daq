/*      dbEventBuilding.c
 *      =================
 *
 * Module to implement DB access to the event building control database.
 *
 * Revision history:
 *  1.00  01 Sep 00  RD  First release
 *  1.01  01 Jul 04  RD  HLT role & HLT decision control added
 *  1.02  17 Aug 05  RD  Vanguard and Rearguard events added
 *  1.03   5 Sep 05  RD  System and Detector software trigger evts added
 *  1.04  14 Sep 05  RD  VAN/REARGUARD events changed into SOD/EOD
 */

#define VID "V 1.04"

#define DESCRIPTION "DATE event building control DB access"
#ifdef AIX
static
#endif
char dbEventBuildingIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
                       """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include "dateDb.h"
#include "dbConfig.h"

#include "dbSql.h"

/* ------------------------------------------------------------------------- */
dbEventBuildingRule *dbEventBuildingControlDb = NULL;
int                  dbSizeEventBuildingControlDb = 0;

/* ------------------------------------------------------------------------- */
static int dbEvbSyntaxError( const int lineNo, const char * const line ) {
  dbSetLastLine( lineNo, line );
  fprintf( stderr,
	   "DB load: syntax error handling DATE event building control\
 configuration\n" );
  return DB_PARSE_ERROR;
} /* End of dbEvbSyntaxError */

/* ------------------------------------------------------------------------- */
static int allocateNextEvb() {
  dbEventBuildingRule *p;
  dbSizeEventBuildingControlDb++;
  
  if ( (dbEventBuildingControlDb =
	  realloc( dbEventBuildingControlDb,
		   sizeof(dbEventBuildingRule)*dbSizeEventBuildingControlDb))
       == NULL ) {
    perror( "DB load: cannot allocate dbEventBuildingControlDb " );
    return DB_INTERNAL_ERROR;
  }

  p = &dbEventBuildingControlDb[dbSizeEventBuildingControlDb-1];
  p->eventType = DB_UNDEFINED_EVENT_TYPE;
  p->build = FALSE;
  p->hltDecision = FALSE;
  p->type = fullBuild;
  ZERO_DETECTOR_PATTERN( p->pattern.detectorPattern );
  ZERO_TRIGGER_PATTERN( p->pattern.triggerPattern );
  
  return DB_LOAD_OK;
} /* End of allocateNextEvb */

/* ------------------------------------------------------------------------- */
/* Parse the "event type" field of an input rule, return appropriate
 * event type (DB_UNDEFINED_EVENT_TYPE for error).
 */
static eventTypeType parseEventType( const char * const inType ) {
  if ( strcasecmp( inType, "SOR" ) == 0
    || strcasecmp( inType, "StartOfRun" ) == 0
    || strcasecmp( inType, "START_OF_RUN" ) == 0 )
    return START_OF_RUN;
  if ( strcasecmp( inType, "EOR" ) == 0
    || strcasecmp( inType, "EndOfRun" ) == 0
    || strcasecmp( inType, "END_OF_RUN" ) == 0 )
    return END_OF_RUN;
  if ( strcasecmp( inType, "SORF" ) == 0
    || strcasecmp( inType, "StartOfRunFiles" ) == 0
    || strcasecmp( inType, "START_OF_RUN_FILES" ) == 0 )
    return START_OF_RUN_FILES;
  if ( strcasecmp( inType, "EORF" ) == 0
    || strcasecmp( inType, "EndOfRunFiles" ) == 0
    || strcasecmp( inType, "END_OF_RUN_FILES" ) == 0 )
    return END_OF_RUN_FILES;
  if ( strcasecmp( inType, "SOB" ) == 0
    || strcasecmp( inType, "StartOfBurst" ) == 0
    || strcasecmp( inType, "START_OF_BURST" ) == 0 )
    return START_OF_BURST;
  if ( strcasecmp( inType, "EOB" ) == 0
    || strcasecmp( inType, "EndOfBurst" ) == 0
    || strcasecmp( inType, "End_OF_BURST" ) == 0 )
    return END_OF_BURST;
  if ( strcasecmp( inType, "PHY" ) == 0
    || strcasecmp( inType, "PHYSICS" ) == 0
    || strcasecmp( inType, "physicsEvent" ) == 0
    || strcasecmp( inType, "PHYSICS_EVENT" ) == 0 )
    return PHYSICS_EVENT;
  if ( strcasecmp( inType, "CAL" ) == 0
    || strcasecmp( inType, "calibration" ) == 0
    || strcasecmp( inType, "calibrationEvent" ) == 0
    || strcasecmp( inType, "CALIBRATION_EVENT" ) == 0 )
    return CALIBRATION_EVENT;
  if ( strcasecmp( inType, "SOD" ) == 0
    || strcasecmp( inType, "startOfData" ) == 0
    || strcasecmp( inType, "START_OF_DATA" ) == 0 )
    return START_OF_DATA;
  if ( strcasecmp( inType, "EOD" ) == 0
    || strcasecmp( inType, "endOfData" ) == 0
    || strcasecmp( inType, "END_OF_DATA" ) == 0 )
    return END_OF_DATA;
  if ( strcasecmp( inType, "SST" ) == 0
    || strcasecmp( inType, "systemSoftware" ) == 0
    || strcasecmp( inType, "systemSoftwareTriggerEvent" ) == 0
    || strcasecmp( inType, "SYSTEM_SOFTWARE_TRIGGER_EVENT" ) == 0 )
    return SYSTEM_SOFTWARE_TRIGGER_EVENT;
  if ( strcasecmp( inType, "DST" ) == 0
    || strcasecmp( inType, "detectorSoftware" ) == 0
    || strcasecmp( inType, "detectorSoftwareTriggerEvent" ) == 0
    || strcasecmp( inType, "DETECTOR_SOFTWARE_TRIGGER_EVENT" ) == 0 )
    return DETECTOR_SOFTWARE_TRIGGER_EVENT;
  return DB_UNDEFINED_EVENT_TYPE;
} /* End of parseEventType */

/* ------------------------------------------------------------------------- */
static int handleEvb( struct dbSectionStruct *s ) {
  int l;
  int status;

  for ( l = 0; l != s->numLines; l++ ) {
    dbEventBuildingRule *n;
    int k;

    if ( (status = allocateNextEvb()) != DB_LOAD_OK ) return status;
    
    n = &dbEventBuildingControlDb[dbSizeEventBuildingControlDb-1];
    
    if ( s->lines[l].tokens[0].keyword != NULL ||
	 s->lines[l].tokens[1].value == NULL )
      return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    if ( (n->eventType = parseEventType( s->lines[l].tokens[0].value )) ==
	 DB_UNDEFINED_EVENT_TYPE ) {
      fprintf( stderr,
	       "DB load: unknown event type \"%s\"\
 in event building configuration\n",
	       s->lines[l].tokens[0].value );
      return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    }

    for ( k = 1; s->lines[l].tokens[k].value != NULL; k++ ) {
      if ( s->lines[l].tokens[k].keyword != NULL ) 
	return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);

      if ( s->lines[l].tokens[k+1].value == NULL ) {

	/* This must be the BUILD/NOBUILD flag */
	if ( strcasecmp( s->lines[l].tokens[k].value, "build" ) == 0 ) {
	  n->build = TRUE;
	} else if ( strcasecmp( s->lines[l].tokens[k].value,
				"nobuild" ) == 0 ) {
	  n->build = FALSE;
	} else {
	  return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}
      } else {
	/* This must be a modifier */
	if ( strncasecmp( "hltDecisionNeeded",
			  s->lines[l].tokens[k].value,
			  strlen(s->lines[l].tokens[k].value)) == 0 ) {
	  n->hltDecision = TRUE;
	} else {
	  int r = dbRolesFind( s->lines[l].tokens[k].value, dbRoleUnknown );

	  if ( r == -1 ) {
	    fprintf( stderr,
		     "DB load: unknown role \"%s\"\
 in event building configuration\n",
		     s->lines[l].tokens[k].value );
	    return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	  }
	  if ( dbRolesDb[r].role == dbRoleTriggerMask ) {

	    /* Build by trigger pattern */
	    if ( n->type != fullBuild && n->type != useTriggerPattern ) {
	      fprintf( stderr,
		       "DB load: unexpected role \"%s\"\
 in event building configuration\n",
		       s->lines[l].tokens[k].value );
	      return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	    }
	    n->type = useTriggerPattern;
	    SET_TRIGGER_IN_PATTERN( n->pattern.triggerPattern, dbRolesDb[r].id );
	  
	  } else if ( dbRolesDb[r].role == dbRoleDetector ) {

	    /* Build by detector pattern */
	    if ( n->type != fullBuild && n->type != useDetectorPattern ) {
	      fprintf( stderr,
		       "DB load: unexpected role \"%s\"\
 in event building configuration\n",
		       s->lines[l].tokens[k].value );
	      return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	    }
	    n->type = useDetectorPattern;
	    SET_DETECTOR_IN_PATTERN( n->pattern.detectorPattern,
				     dbRolesDb[r].id );
	  
	  } else {
	    fprintf( stderr,
		     "DB load: unexpected role \"%s\"\
 in event building configuration\n",
		     s->lines[l].tokens[k].value );
	    return dbEvbSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	  }
	}
      }
    }
  }
  
  if ( (status = allocateNextEvb()) != DB_LOAD_OK ) return status;
  dbSizeEventBuildingControlDb--;

  return DB_LOAD_OK;
} /* End of handleEvb */

/* ------------------------------------------------------------------------- */
static int checkEvb() {
  return DB_LOAD_OK;
} /* End of checkEvb */

/* ------------------------------------------------------------------------- */
/* Load the event builder control database. Return DB_LOAD_OK on success.    */
int dbLoadEventBuildingControl() {
  int status;

  if ( dbEventBuildingControlDb != NULL ) return DB_LOAD_OK;

  /* Switch for SQL database */
  if (dbsql_is_configured()) {
    if ( dbsql_open()!=0 ) return DB_LOAD_ERROR;
    if ( (status = dbsql_load_eventBuildingControl()) != DB_LOAD_OK ) return status;
    dbsql_close();
  } else {

  if ( (status = dbInitInput()) != DB_UNLOAD_OK ) return status;
  if ( (status = dbInput( dbsTable[ DB_TABLE_EVENT_BUILDING_CONTROL ].name ))
       != DB_LOAD_OK )
    return status;
  if ( (status = handleEvb( &dbSections[ DB_SECTION_EVENT_BUILDING_CONTROL ] ))
       != DB_LOAD_OK ) {
    dbUnloadEventBuildingControl();
    return status;
  }
  
  } /* end of Switch for SQL database */  
  
  if ( (status = checkEvb()) != DB_LOAD_OK ) {
    dbUnloadEventBuildingControl();
    return status;
  }
  return status;
} /* End of dbLoadEventBuildingControl */

/* ------------------------------------------------------------------------- */
/* Unload the event building control database */
int dbUnloadEventBuildingControl() {
  free( dbEventBuildingControlDb );
  dbEventBuildingControlDb = NULL;
  dbSizeEventBuildingControlDb = 0;

  return DB_UNLOAD_OK;
} /* End of dbUnloadEventBuildingControl */
