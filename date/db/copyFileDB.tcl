#!/usr/bin/tclsh

# copy a file to/from DATE MySQL DB if configured

proc print_usage {} {
  puts "copyFileDB get|set \[-f filename\] \[-h host\] \[-d dir\]"
  puts ""
  puts "Copy file(s) from/to DATE DB. Options:"
  puts "  get/set: choose one to copy file(s) from/to database."
  puts "  -f filename: specify a file to copy."
  puts "               Relative paths are defined from DATE_SITE_CONFIG or"
  puts "               DATE_SITE/DATE_HOSTNAME depending on -h option."
  puts "               This option can be repeated to get/set a list of files."
  puts "               To set exec flag when getting file, use -fe instead of -f"
  puts "  -h host: specify host name."
  puts "           If not provided, looks for non-specific file(s) in database."
  puts "  -d dir: use this local directory to read/write file."
  puts "  -i : insert new entries in db. Use this flag to allow creating new entries. (set)"
  puts "  -s : skip errors when files do not exist (set)."
}

set x 0
set files {}
set execfiles {}
set host ""
set mode ""
set localdir ""
set insert 0
set noreaderr 0
while {[set opt [lindex $argv $x]] != ""} {
  switch -exact -- $opt {
    -f {
      incr x
      lappend files [lindex $argv $x]
    }
    -fe {
      incr x
      lappend files [lindex $argv $x]
      lappend execfiles [lindex $argv $x]
    }
    -h {
      incr x
      set host [lindex $argv $x]
    }
    -d {
      incr x
      set localdir [lindex $argv $x]
    }
    -i {
      set insert 1
    }
    -s {
      set noreaderr 1
    }
    "get" {
      set mode "get"
    }
    "set" {
      set mode "set"
    }
    default {
      print_usage
      exit 1    
    }
  }
  incr x
}

if {("$mode"=="")||([llength $files]==0)} {
      print_usage
      exit 1    
}

# define path prefix if directory unspecified
if {[string length $host]} {
  set path_prefix "$env(DATE_SITE)/${host}"
} else {
  set path_prefix "$env(DATE_SITE_CONFIG)"
}



catch {
  # Do nothing if not in MySQL mode
  if {[llength [array names env "DATE_DB_MYSQL"]]==0} {
    exit 0
  }
  if {$env(DATE_DB_MYSQL)!="TRUE"} {
    exit 0
  }
  
  # define connection parameters
  set db_user $env(DATE_DB_MYSQL_USER)
  set db_pwd $env(DATE_DB_MYSQL_PWD)
  set db_host $env(DATE_DB_MYSQL_HOST)
  set db_db $env(DATE_DB_MYSQL_DB)

  if {[string length $db_user]==0} {
    return 1
  }
  if {[string length $db_host]==0} {
    return 1
  }
  if {[string length $db_pwd]==0} {
    return 1
  }
  if {[string length $db_db]==0} {
    return 1
  }    

  # Add mysqltcl path
  if { [exec arch ] == "i686" } { 
    lappend auto_path $env(DATE_ROOT)/local
  }
  if { [exec arch ] == "x86_64" } {
    lappend auto_path $env(DATE_ROOT)/local64
  }

  if [ catch {package require mysqltcl 3.0} ] {
    puts "Package mysqltcl required"
    return 1
  }

  #puts "mysql -h $db_host -u $db_user -p$db_pwd $db_db"

  if [ catch {set db [mysqlconnect -host $db_host -user $db_user -password $db_pwd -db $db_db]} ] {
    puts "Failed to connect to database"
    return 2
  }

  if {"$mode"=="get"} {

    # first remove existing destination files, if any
    # this is in case file exist on disk but not in db
    foreach path $files {
      if {"$localdir"!=""} {
        set path "$localdir/[file tail $path]"
      } else {
        if {[string range $path 0 0]!="/"} {
          set path "${path_prefix}/${path}"
        }
      }
      file delete $path
    }

    set query {"SELECT PATH,VALUE FROM FILES WHERE"}
    set x 0
    foreach f $files {
      if {$x!=0} {
        lappend query "OR"
      } else {
        lappend query "("
      }
      lappend query "PATH=\"$f\""
      incr x
    }
    lappend query ")"
    
    if {[string length $host]} {
      lappend query "AND HOST=\"$host\""
    } else {
      lappend query "AND (HOST=\"\" OR HOST IS NULL)"
    }
    
    set query [join $query]
    
    foreach row [mysqlsel $db "$query" -list] {
      set path [lindex $row 0]
      set value [lindex $row 1]     

      set execflag 0
      if {[lsearch $execfiles $path]!=-1} {
        set execflag 1
      }
      
      if {"$localdir"!=""} {
        set path "$localdir/[file tail $path]"
      } else {
        if {[string range $path 0 0]!="/"} {
          set path "${path_prefix}/${path}"
        }
      }

      if {[catch {set fd [open "$path" "w"]}]} {
        puts "Could not open $path for writing"
        continue
      }
      
      puts "Writing $path"
      
      fconfigure $fd -translation binary
      puts -nonewline $fd $value     
      close $fd
      
      if {$execflag} {
        file attributes $path -permission 0755
      }
    }

  } elseif {"$mode"=="set"} {
  
    foreach f $files {
      if {"$localdir"!=""} {
        set path "$localdir/[file tail $f]"
      } else {
        if {[string range $f 0 0]!="/"} {
          set path "${path_prefix}/${f}"
        } else {
          set path "$f"
        }
      }
      
      # Read file from disk
      if {[catch {set fd [open "$path" "r"]}]} {
        if {!$noreaderr} {puts "Could not open $path for reading"}
        continue
      }
      
      puts "Reading $path"
      
      fconfigure $fd -translation binary
      set value [::mysql::escape [read $fd]]
      close $fd
      
      # Insert file in table
      if {!$insert} {
        set query "UPDATE FILES SET VALUE=\"$value\" WHERE PATH=\"$f\" AND HOST=\"$host\""
        mysqlsel $db $query -list
      } else {
        set query "REPLACE INTO FILES (VALUE,PATH,HOST) VALUES('$value','$f','$host')"
        mysqlsel $db $query -list
      }
    }
  
  }

  ::mysql::close $db
  
} err

if {$err!=""} {
  puts "Could not get/set file(s) : $err"
  return 1
}

return 0
