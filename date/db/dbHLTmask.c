#include <stdio.h>
#include "math.h"

#include "dateDb.h"
#include "event.h"


int main() {

  int i,j,maxj,id;
  char *detector;

  /* Generate SQL statements to create entries in DDLin table
     according to specification of DAQ-HLT interface DDL numbering scheme.

    id 0 = bit 0 of word 0
  */
  
  for (i=0;i<=28;i++) {

    switch (i) {
      case 0:  maxj=20; detector="SPD"; break;
      case 1:  maxj=24; detector="SDD"; break;
      case 2:  maxj=16; detector="SSD"; break;
      case 3:  maxj=32; detector="TPC"; break;
      case 4:  maxj=32; detector="TPC"; break;
      case 5:  maxj=32; detector="TPC"; break;
      case 6:  maxj=32; detector="TPC"; break;
      case 7:  maxj=32; detector="TPC"; break;
      case 8:  maxj=32; detector="TPC"; break;
      case 9:  maxj=24; detector="TPC"; break;
      case 10: maxj=0; detector="TPC"; break;
      case 11: maxj=18; detector="TRD"; break;
      case 12: maxj=32; detector="TOF"; break;
      case 13: maxj=32; detector="TOF"; break;
      case 14: maxj=8; detector="TOF"; break;
      case 15: maxj=20; detector="PHOS"; break;
      case 16: maxj=20; detector="HMPID"; break;
      case 17: maxj=20; detector="Muon_TRK"; break;
      case 18: maxj=2; detector="Muon_TRG"; break;
      case 19: maxj=10; detector="CPV"; break;
      case 20: maxj=6; detector="PMD"; break;
      case 21: maxj=1; detector="TRG"; break;
      case 22: maxj=3; detector="FMD"; break;
      case 23: maxj=1; detector="T0"; break;
      case 24: maxj=1; detector="V0"; break;
      case 25: maxj=1; detector="ZDC"; break;
      case 26: maxj=1; detector="ACCORDE"; break;
      case 27: maxj=24; detector="EMCal"; break;
      case 28: maxj=10; detector="HLT"; break;
      default: maxj=0;
    }
  
    #define l 28 
    long32 v[l];
    int k;
    
    for (j=0;j<maxj;j++) {
      id=i*32+j;

      if ( (DB_ID_TO_BIT(id)!=(int)pow(2,(double)j)) || (DB_ID_TO_NUM(id)!=i) ) {
        for (k=0;k<l;k++) {v[k]=0;}
        DB_SET_BIT(v, id);
        printf("id %d (word %d bit %d): mask=",id,i,j);
        for (k=0;k<l;k++) {printf (" %04X",v[k]);}
        printf("\n");
    
        printf ("Error: mismatch id %d - bit %d word %d -> macro returns mask %d in word %d\n",id,j,i,DB_ID_TO_BIT(id),DB_ID_TO_NUM(id));
      } else {
        printf ("insert into DDLin(id,wordIndex,bitIndex,detectorName) values('%d','%d','%d','%s');\n",id,i,j,detector);
      }

    }   
  }

  return 0;
}


