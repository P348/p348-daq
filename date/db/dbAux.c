/*      dbAux.c
 *      =======
 *
 * Auxiliary routines for the DATE database package.
 *
 * Revision history:
 *  1.00  24 Oct 00  RD  First release
 *  1.01  30 Apr 03  RD  Possible problem (?) with dbDecodeBankPattern fixed
 *  1.02   1 Jul 04  RD  HLT role handling added
 *  1.03  17 Aug 05  RD  Handling of Vanguard and Rearguard events added
 *  1.04   5 Sep 05  RD  System and Detector software trigger events added
 *  1.05  14 Sep 05  RD  VAN/REARGUARD changed into SOD/EOD
 */

#define VID "V 1.05"

#define DESCRIPTION "DATE DB access auxiliary routines"
#ifdef AIX
static
#endif
char dbAuxIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
                  """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include "dateDb.h"
#include "dbConfig.h"

/* ------------------------------------------------------------------------- */
/*    Macros for string I/O handling (sized print and sized append)          */
#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

/* ------------------------------------------------------------------------- */
static char * const dbStatusString[ DB_LAST_ERROR+1 ] = {
  "operation completed OK",
  "error opening database (see errno for more details)",
  "parse error",
  "internal error (see errno for more details)",
  "invalid dynamic sizing requested",
  "invalid input parameter",
  "unknown ID"
};

/* ------------------------------------------------------------------------- */
const char * const dbDecodeStatus( const int status ) {
  if ( status <= DB_LAST_ERROR && status >= 0 ) {
    return dbStatusString[ status ];
  } else {
    static char msg[ 1024 ];
    sprintf( msg, "Unknown error code:%d", status );
    return msg;
  }
} /* End of dbDecodeStatus */

/* ------------------------------------------------------------------------- */
/* Get the "SUPER" role from a role */
superRoleType dbGetSuperRole( const dbRoleType role ) {
  switch (role) {
  case dbRoleGdc:
    return inGdcMask;
  case dbRoleLdc:
    return inLdcMask;
  case dbRoleEdmHost:
  case dbRoleDetector:
  case dbRoleSubdetector:
  case dbRoleTriggerHost:
  case dbRoleTriggerMask:
  case dbRoleDdg:
  case dbRoleFilter:
  case dbRoleUnknown:
  case dbRoleUndefined:
    return notInMask;
  }
  return notInMask;
} /* end of dbGetSuperRole */

/* ------------------------------------------------------------------------- */
const char * const dbDecodeRole( const dbRoleType role ) {
  static char msg[ 1024 ];

  switch ( role ) {
  case dbRoleUndefined:   return "Undefined   ";
  case dbRoleUnknown:     return "Unknown     ";
  case dbRoleGdc:         return "GDC         ";
  case dbRoleLdc:         return "LDC         ";
  case dbRoleEdmHost:     return "EDM         ";
  case dbRoleDetector:    return "Detector    ";
  case dbRoleSubdetector: return "Subdetector ";
  case dbRoleTriggerHost: return "Trigger-Host";
  case dbRoleTriggerMask: return "Trigger-Mask";
  case dbRoleDdg:         return "DDG         ";
  case dbRoleFilter:      return "FILTER      ";
  }
  snprintf( msg, sizeof(msg), "Unknown-Role:%d", (int)role );
  return msg;
} /* End of dbDecodeRole */

/* ------------------------------------------------------------------------- */
/* Encode a DB role, return dbRoleType value for the given string            */
/* Reverse operation of dbDecodeRole()                                       */
dbRoleType dbEncodeRole(const char * srole) {
  static char role[20];
  int i;

  /* trim spaces at right */
  snprintf(role,20,"%s",srole);
  for (i=strlen(role)-1;i>=0;i--) {
    if (role[i]!=' ') break;
    role[i]=0;
  }
  
  
  if (!strcmp(role,"Undefined")) return dbRoleUndefined;
  if (!strcmp(role,"Unknown")) return dbRoleUnknown;
  if (!strcmp(role,"GDC")) return dbRoleGdc;
  if (!strcmp(role,"LDC")) return dbRoleLdc;
  if (!strcmp(role,"EDM")) return dbRoleEdmHost;
  if (!strcmp(role,"Detector")) return dbRoleDetector;
  if (!strcmp(role,"Subdetector")) return dbRoleSubdetector;
  if (!strcmp(role,"Trigger-Host")) return dbRoleTriggerHost;
  if (!strcmp(role,"Trigger-Mask")) return dbRoleTriggerMask;  
  if (!strcmp(role,"DDG")) return dbRoleDdg;  
  if (!strcmp(role,"FILTER")) return dbRoleFilter;  

  return dbRoleUnknown;
} /* End of dbEncodeRole */

/* ------------------------------------------------------------------------- */
const char * const dbDecodeHltRole( const dbHltRoleType role ) {
  static char msg[ 1024 ];

  switch ( role ) {
  case dbHltRoleUndefined:   return "Undefined  ";
  case dbHltRoleHltLdc:      return "hltLdc     ";
  case dbHltRoleDetectorLdc: return "detectorLdc";
  }
  snprintf( msg, sizeof(msg), "Unknown-HLT-Role:%d", (int)role );
  return msg;
} /* End of dbDecodeRole */

/* ------------------------------------------------------------------------- */
dbHltRoleType dbEncodeHltRole(const char * srole) {
  static char role[20];
  int i;

  /* trim spaces at right */
  snprintf(role,20,"%s",srole);
  for (i=strlen(role)-1;i>=0;i--) {
    if (role[i]!=' ') break;
    role[i]=0;
  }
   
  if (!strcmp(role,"Undefined")) return dbHltRoleUndefined;
  if (!strcmp(role,"hltLdc")) return dbHltRoleHltLdc;
  if (!strcmp(role,"detectorLdc")) return dbHltRoleDetectorLdc;
  
  return dbHltRoleUndefined;;
} /* End of dbEncodeHltRole */

/* ------------------------------------------------------------------------- */
/* Unload all databases */
int dbUnloadAll() {
  int s;
  int status = DB_UNLOAD_OK;

  for ( s = 0; dbsTable[s].name != NULL; s++ ) {
    int nextStatus = (dbsTable[s].unloader)();

    if ( status == DB_UNLOAD_OK ) status = nextStatus;
  }
  return status;
} /* End of dbUnloadAll */

/* ------------------------------------------------------------------------- */
const char * const dbDecodeBankPattern( const dbBankPatternType pattern ) {
  int i;
  int n;
  static char l[1024];

  for ( l[0]=0, n=0, i=0; i != DB_NUM_BANK_TYPES; i++ ) {
    if ( DB_TEST_BIT( &pattern, i ) ) {
      snprintf( AP(l), "%s%s", n++==0 ? "" : "+", dbBankNames[i] );
    }
  }
  if ( n == 0 ) snprintf( SP(l), "<none>" );

  return l;
} /* End of dbDecodeBankPattern */

/* ------------------------------------------------------------------------- */
/* Returns the dbMemTypeNames index corresponding to a given string          */
dbMemType dbEncodeMemType(const char * memtype) {
  int i;
  
  if (memtype==NULL) return 0;  /* return undefined if not found */
  
  for (i=0;i<DB_NUM_MEM_TYPES;i++) {
    if (!strcmp(memtype,dbMemTypeNames[i])) return i;
  }
  
  return 0; /* return undefined if not found */
} /* End of dbEncodeMemType */

/* ------------------------------------------------------------------------- */
/* Returns the index of the role entry with the given name in the roles      */
/* database, or -1 if none found. Can use rolesFind and hash table instead   */
int dbRolesFindName(const char *name){
  int i;
  
  for (i=0;i<dbSizeRolesDb;i++) {
    if (!strcmp(name,dbRolesDb[i].name)) return i;
  }
  return -1;
}

/* ----------------------------------------------------------------------- */
/* Find given role/id in Roles DB.                                         */
/* Returns the index of corresponding entry, or -1 if not found.           */
int dbRolesFindId(const dbIdType id, const dbRoleType role){
  int i;
  
  for (i=0;i<dbSizeRolesDb;i++){
    if ((dbRolesDb[i].id == id) && (dbRolesDb[i].role == role)) return i;
  } 
  
  return -1;
}

/* ----------------------------------------------------------------------- */
/* Decode event type to string */
const char *dbDecodeEventType( const eventTypeType t ) {
  switch (t) {
  case START_OF_RUN:       return "StartOfRun";
  case END_OF_RUN:         return "EndOfRun";
  case START_OF_RUN_FILES: return "StartOfRunFiles";
  case END_OF_RUN_FILES:   return "EndOfRunFiles";
  case START_OF_BURST:     return "StartOfBurst";
  case END_OF_BURST:       return "EndOfBurst";
  case PHYSICS_EVENT:      return "Physics";
  case CALIBRATION_EVENT:  return "Calibration";
  case START_OF_DATA:      return "StartOfData";
  case END_OF_DATA:        return "EndOfData";
  case SYSTEM_SOFTWARE_TRIGGER_EVENT:
                           return "SystemSwTrig";
  case DETECTOR_SOFTWARE_TRIGGER_EVENT:
                           return "DetectorSwTrig";
  }
  return NULL;
} /* End of dbDecodeEventType */

/* ----------------------------------------------------------------------- */
/* Encode event type from string */
/* Reverse operation of dbDecodeEventType() */
eventTypeType dbEncodeEventType( const char *t ) {
  if (t==NULL) return DB_UNDEFINED_EVENT_TYPE;
  if (!strcmp(t,"StartOfRun")) return START_OF_RUN;
  if (!strcmp(t,"EndOfRun")) return END_OF_RUN;
  if (!strcmp(t,"StartOfRunFiles")) return START_OF_RUN_FILES;
  if (!strcmp(t,"EndOfRunFiles")) return END_OF_RUN_FILES;
  if (!strcmp(t,"StartOfBurst")) return START_OF_BURST;
  if (!strcmp(t,"EndOfBurst")) return END_OF_BURST;
  if (!strcmp(t,"Physics")) return PHYSICS_EVENT;
  if (!strcmp(t,"Calibration")) return CALIBRATION_EVENT;
  if (!strcmp(t,"StartOfData")) return START_OF_DATA;
  if (!strcmp(t,"EndOfData")) return END_OF_DATA;
  if (!strcmp(t,"SystemSoftwareTrigger"))
    return SYSTEM_SOFTWARE_TRIGGER_EVENT;
  if (!strcmp(t,"DetectorSoftwareTrigger"))
    return DETECTOR_SOFTWARE_TRIGGER_EVENT;
  return DB_UNDEFINED_EVENT_TYPE;
} /* End of dbEncodeEventType */

/* ----------------------------------------------------------------------- */
