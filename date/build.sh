#!/bin/bash -ev

cd monitoring
mkdir -p Linux
make ./Linux/libmonitorstdalone.a
cd Linux
ln -s -f libmonitorstdalone.a libmonitor.a
cd ../..
ln -s -f monitoring/Linux lib
ln -s -f monitoring include
