#!/bin/ksh

h="`hostname`"
[ "${DATE_USER_ID}" = "" ] && DATE_USER_ID="nobody"
/bin/ps -efl 2>&1 | 
 egrep "(${DATE_USER_ID}|setiathome)" | 
 egrep -v '(Warning|grep|in.identd|tftpd|httpd|<defunct>|PID)' | {
  while read line; do
   echo $line | {
    read a b c d e
    o="`/bin/ps -p $d 2>&1 | grep -v PID | grep -v Warning`"
    if [ "$o" ]; then
     echo "$h: $o"
     kill $d
     oo="`/bin/ps -p $d 2>&1 | grep -v PID | grep -v Warning`"
     if [ "$o" = "$oo" ]; then
     sleep 1
     kill -9 $d
     sleep 1
     oo="`/bin/ps -p $d 2>&1 | grep -v PID | grep -v Warning`"
     if [ "$o" = "$oo" ]; then
      echo "Host $h may need reboot..."
     fi
    fi
   fi
  }
  done
 }
ipcs -m | grep "${DATE_USER_ID}" | { 
 while read line; do
  echo "$h:$line"
  echo $line | {
   read a b c;
   if [ "`uname`" = "Linux" ]; then
    ipcrm shm $b 2>&1 | grep -v '^resource deleted'
   else
    ipcrm -m $b
   fi
  }
 done
}
