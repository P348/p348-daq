// example client subscriber to DIM logbook update service
//
// gcc getnotify.c /opt/dim/linux/libdim.a -I/opt/dim/dim/ -o getnotify -lpthread

#include <stdio.h>
#include "dic.h"

#define LOGBOOK_UPDATE_SERVICE "/DATE/LOGBOOK/UPDATE"


/* callback routine, called by DIM upon service status change */
/* buffer is of type 'long', and its value means: */
/* -1 : service not available */
/* 0  : no update yet */
/* >0 : time of last update */

void user_routine (long *tag, int *buffer, int *size) {
  printf("Data received : %ld\n",*((long *)buffer));
}


int main(int argc, char **argv){

  unsigned int logbook_update_service_id;
  long invalid=-1;

  logbook_update_service_id=dic_info_service (LOGBOOK_UPDATE_SERVICE, MONITORED, 0, NULL, 0, user_routine, 0, (int *)&invalid, sizeof(invalid));
  sleep(100);
  dic_release_service(logbook_update_service_id);

  return 0;
}
