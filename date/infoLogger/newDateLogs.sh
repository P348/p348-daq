#!/bin/sh

# Clean/create log database
# This script must be run on the host where infoLoggerServer is running in case
# of the flat file database. The new log file is created with current user
# access.

# 07 Apr 2005  SC  Added Run Number log field (number 8).
# 22 Apr 2005  SL  Added new statistics field to logbook.
# 25 Apr 2005  SC  source config file
# 08 Nov 2005  SC  Added new table for logbook. Added indexes.


usage() {
  echo "Usage: $0 [-c|-d|-e]"
  echo "  -c : create table only, no backup"
  echo "  -d : delete messages only, no backup"
  echo "  -e : erase all message archives and create new message table"
  echo "By default, messages are saved in a table with backup time"
}


# the script is to be run on master host
MASTERHOST=`ssh pccodb00 hostname -s`
if [ $? != 0 ]; then
    echo 'ERROR: failed to get master host name'
    exit 1
fi
LOCALHOST=`hostname -s`

if [ "$LOCALHOST" != "$MASTERHOST" ]; then
    # nothing to do, we are on slave host
    exit 0
fi


DELETE=0;
CREATE=0;
ERASE_ALL=0;
while getopts 'hdce' OPTION; do
 case $OPTION in
  h|\?)
   usage
   exit 0 ;;
  d)
   DELETE=1 ;;
  c)
   CREATE=1 ;;
  e)
   DELETE=1
   CREATE=1
   ERASE_ALL=1 ;;
 esac
done

# Setup DATE and infoLogger environment
if [ "${DATE_INFOLOGGER_DIR}" = "" ]; then
  DATE_INFOLOGGER_DIR=/date/infoLogger
fi
. ${DATE_INFOLOGGER_DIR}/infoLoggerConfig.sh --

# current date-time
NOW="__"`date "+%Y_%m_%d__%H_%M_%S"`

# definition of message table
TABLEDESCR='severity char(1), timestamp double(16,6), hostname varchar(20), pid int, username varchar(20), system varchar(20), facility varchar(20), dest varchar(20), run int, message text \
,index ix_severity(severity), index ix_timestamp(timestamp), index ix_hostname(hostname), index ix_facility(facility), index ix_dest(dest), index ix_run(run), index ix_system(system)'

if [ "$DATE_INFOLOGGER_MYSQL" == "TRUE" ]; then

  if [ "$MYSQL_PATH" == "" ]; then
    SQLCLIENT="mysql"
  else
    SQLCLIENT="$MYSQL_PATH/bin/mysql"
  fi

  if [ "$ERASE_ALL" == "1" ]; then
      # drop all messsages tables in db
      QUERY="show tables;"
      MSGTABLES=`$SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY"`
      T2=`echo $MSGTABLES|tr ' ' '\n'|grep messages`
      if [ "$T2" != "" ]; then
	      QUERY2="drop table `echo $T2|tr ' ' ','`"
	      $SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY2"
      fi      
  fi

  if [ "$CREATE" == "1" ]; then
      QUERY="create table if not exists messages($TABLEDESCR);"
  else
    if [ "$DELETE" == "1" ]; then
      QUERY="delete from messages;"
    else
      QUERY="drop table if exists new; create table new ($TABLEDESCR); rename table messages to messages$NOW, new to messages;"
    fi
  fi

  $SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY"
  
  
  # create logbook table as well
  QUERY="create table if not exists logbook (run int, time_start double(16,6), time_end double(16,6), time_update double(16,6), log mediumtext, totalEvents int, totalData double(16,1), \
  averageDataRate double(16,1), averageEventsPerSecond double, averageDuration int, numberOfLDCs int, numberOfGDCs int, numberOfStreams int, primary key (run), \
  index(run), index(time_start), index(time_end), index(time_update) \
  );"
  $SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY"
  
  QUERY="create table if not exists logbook_detectors (run int, detector_name char(128), detector_level smallint, readoutEvents int, readoutData double(16,1),
  index(run), foreign key (run) references logbook(run), index(detector_name), index(detector_level));"
  $SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY"
  
  
else
  # stop server
  NOTRUNNING=`${DATE_ROOT}/infoLogger/infoLoggerServer.sh stop | grep "not running"`
  
  if [ "$ERASE_ALL" == "1" ]; then
      if [ "$DATE_SITE_LOGS" != "" ]; then
        rm -f ${DATE_SITE_LOGS}/*
        mkdir -p $DATE_SITE_LOGS
      fi
  fi
    
  if [ "$CREATE" == "0" ]; then
    if [ "$DELETE" == "1" ]; then
      rm $DATE_SITE_LOGS/dateLogs
    else
      mv $DATE_SITE_LOGS/dateLogs "$DATE_SITE_LOGS/dateLogs"$NOW
    fi
  fi
  touch $DATE_SITE_LOGS/dateLogs

  # restart server if it was running
  if [ "$NOTRUNNING" == "" ]; then
  STARTED=`${DATE_ROOT}/infoLogger/infoLoggerServer.sh start | grep "started"`
    if [ "$STARTED" == "" ]; then
      echo "Failed to restart infoLoggerServer"
      exit 1
    fi
  fi
fi

exit 0
