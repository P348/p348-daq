/* logFromStdin.c */
/*
   This program reads data coming on stdin line by line, and transmit
   them as infoLogger messages (one line = one message).
   
   History:
    - 28 Jan 2005   SC   File created.
*/


#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "infoLogger.h"
#include "utility.h"


void print_usage(){
  printf("Options: \n");
  printf("  -d destination            (default=log)     \n");
  printf("  -f facility               (default=operator)\n");
  printf("  -s [info,error,fatal]     (default=info)    \n");
}


int main(int argc, char **argv){

  char *facility=NULL;
  char *destination="defaultLog";
  char severity=LOG_INFO;
  
  struct lineBuffer *lb;
  char *msg;
  char option;
  int  eof;


  /* read options */
  while((option = getopt(argc, argv, "d:f:s:")) != -1){
    switch(option){
      case 'd': 
        destination=optarg;
        break;

      case 'f': 
        facility=optarg;
        break;

      case 's':
        /* optarg can't be null, getopt checks parameter provided */
        if (!strcmp(optarg,"info")) {severity=LOG_INFO; break;}
        if (!strcmp(optarg,"error")) {severity=LOG_ERROR; break;}
        if (!strcmp(optarg,"fatal")) {severity=LOG_FATAL; break;}                    
        printf("Bad severity\n");
        print_usage();
        return -1;

      default:
        print_usage();
        return -1;
    }
  }

  /* take facility from env. by default */
  if (facility==NULL) {
    facility = getenv( "DATE_FACILITY" );
    if ( facility == NULL || strlen( facility ) == 0 ) {
      facility = "operator";
    }
  }

  /* create a buffer for input lines */
  lb=lineBuffer_new();
  if (lb==NULL) return -1;
  
  /* read lines from stdin until EOF, and send them as log messages*/
  for(;;) {
    eof=lineBuffer_add(lb,fileno(stdin),-1);
    for(;;){
      msg=lineBuffer_getLine(lb);
      if (msg==NULL) break;
      infoLogTo(facility,destination,severity,msg);
      free(msg);
    }
    if (eof) break;     
  }
  
  /* cleanup on exit */
  lineBuffer_destroy(lb);
  return 0;
}
