/** Infologger server
  *
  * This daemon receives log data and stores it in a database.
  *
  * @file     infoLoggerServer.c
  * @author   sylvain.chapeland@cern.ch
  * 
  * History:
  *  - 02/11/2004  File created.
  *  - 03/02/2005  Runtime SQL/flatfile switch
  *  - 07/04/2005  Added Run Number field
  *  - 20/04/2005  Config of ports done in environment
*/

#include "simplelog.h"
#include "utility.h"
#include "transport_server.h"

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

/*
#define TIME_SAMPLE_PROCESSING
*/

/**************************************/
/* timing functions - for development */
/**************************************/
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

static  int olddiff;
static  struct timeval tv1,tv2;
static  struct timezone tz;

void timer_start(){
  olddiff=0;
  gettimeofday(&tv1,&tz);
}

void timer_continue(){
  gettimeofday(&tv1,&tz);
}

int timer_stop(){
  int diff;
  gettimeofday(&tv2,&tz);
  diff=olddiff+(tv2.tv_usec-tv1.tv_usec)+(tv2.tv_sec-tv1.tv_sec)*1000000;
  olddiff=diff;
  return (int)(diff/1000);
}

int timer_getval(){
  return olddiff;
}

void timer_reset(){
  olddiff=0;
}

void timer_stop_print(){
  int diff;
  gettimeofday(&tv2,&tz);
  diff=olddiff+(tv2.tv_usec-tv1.tv_usec)+(tv2.tv_sec-tv1.tv_sec)*1000000;
  olddiff=diff;
  printf("%d microsec\n",diff);
}
/**************************************/
/* end of timing functions            */
/**************************************/





/*********************/
/* Constants         */
/*********************/

#define APP_NAME     "InfoLogger server"    /* The application name, as appears in log messages */
#define APP_VERSION  "1.1"                  /* The application version */



/*********************/
/* Globals           */
/*********************/

int shutdown_request=0;                 /* Set to 1 (by signal handler) for shutdown */

int date_socket_infolog_rx;          /* port to receive data  */
int date_socket_infolog_tx;          /* port to dispatch data */



/*********************/
/* Signal handling   */
/*********************/


/** Common signals handler.
  * SIGTERM triggers a shutdown request.
*/
void signal_handler(int signal_rcvd){
  slog(SLOG_INFO,"[%d] : signal %d received",getpid(),signal_rcvd);
  if ((signal_rcvd == SIGTERM)||(signal_rcvd == SIGQUIT)||(signal_rcvd == SIGINT)) {
    shutdown_request=1;
  }
  signal(signal_rcvd, signal_handler);
}




/********************/
/* Message parsing  */
/********************/

#include "infoLoggerServer_insert.h"

/* create an empty message structure - initializes partially the structure (just enough to be destroyed) */
/* uses checked_malloc() */
infoLog_msg_t *infoLog_msg_create(void){
  infoLog_msg_t *m;
  
  m=checked_malloc(sizeof(infoLog_msg_t));
  m->data=NULL;
  m->next=NULL;

  return m;
}

/* free memory allocated to a infoLog_msg_t */
/* uses checked_free() */
void infoLog_msg_destroy(infoLog_msg_t *m){
  infoLog_msg_t     *new,*old;

  for(new=m;new!=NULL;new=old){
    old=new->next;
    checked_free(new->data);
    checked_free(new);
  }
  
}



/* Decode message from string:

# Command to create tables (check in newDateLogs.sh):
# definition of message table
# TABLEDESCR='severity char(1), timestamp double(16,6), hostname varchar(20), pid int, username varchar(20), system varchar(20), facility varchar(20), dest varchar(20), run int, message text \
# ,index ix_severity(severity), index ix_timestamp(timestamp), index ix_hostname(hostname), index ix_facility(facility), index ix_dest(dest), index ix_run(run), index ix_system(system)'

This function is destructive, file content is altered (blobs content removed) to avoid data copy.
*/
infoLog_msg_t * infoLog_decode(TR_file *f){

  char              *ptr,*start,*end;
  infoLog_msg_t     *new,*old,*first;
  TR_blob           *b;
  int               is_error;

  first=NULL;
  old=NULL;
  is_error=0;
  
  /* parse all blobs */
  for (b=f->first;b!=NULL;b=b->next){
    /* create structure */
    new=infoLog_msg_create();

    /* steal data from blob */
    new->data=b->value;    
    end=b->value+b->size;
    b->value=NULL;
    b->size=0;
    
    /* chain item to current list */
    if (first==NULL) {
      first=new;
    } else {
      old->next=new;
    }
    old=new;
    
    /* parse data */
    /* should be like: #I#1099570259#pcald10#30287#slord#DAQ#testclient#defaultLog#12345#blablabla [NULL terminated] */
    ptr=new->data;

//  slog(SLOG_INFO,"%s",ptr);

    /* check initial marker */
    if (*ptr!='#') {is_error=1; break;}
    ptr++;
    if (ptr>=end) {is_error=1; break;}

    /* read severity */
    new->severity=ptr;
    ptr++;
    if (ptr>=end) {is_error=1; break;}
    if (*ptr!='#') {is_error=1; break;}
    *ptr=0;
    ptr++;
    if (ptr>=end) {is_error=1; break;}
    
    /* read timestamp */
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    if (sscanf(start,"%lf",&new->timestamp)!=1) {is_error=1; break;}

    /* read hostname */
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    new->hostname=start;

    /* read pid */    
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    if (sscanf(start,"%d",&new->pid)!=1) {is_error=1; break;}
    
    /* read username */
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    new->username=start;

    /* read system */
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    new->system=start;
    
    /* read facility */
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    new->facility=start;

    /* read destination */
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    new->dest=start;

    /* read run number */    
    for(start=ptr;(*ptr!='#')&&(ptr<end);ptr++) {}
    if (ptr>=end) {is_error=1; break;}
    *ptr=0;
    ptr++;
    if (sscanf(start,"%d",&new->run)!=1) {is_error=1; break;}
    
    /* read message */
    *end=0;
    new->message=ptr;        
  }
  
  /* we have emptied file from blobs */
  f->size=0;

  if (!is_error) {
    return first;
  }
  
  /* free memory on error */
  infoLog_msg_destroy(first);  
  
  return NULL;
}




/*****************/
/* Insert thread */
/*****************/

/* An insertion thread calls infoLog_insert to store data in the repository.
   The data is fed in using a FIFO (param. queue in struct).
   The FIFO is emptied completely before the thread stops.
*/


/* start an insertion thread */
int insert_th_start(insert_th * t){
  char *db_mode;

  /* create queue */
  t->queue=ptFIFO_new(INSERT_TH_QUEUE_SIZE);
  
  /* init shutdown variable */
  t->shutdown=0;
  pthread_mutex_init(&t->shutdown_mutex,NULL);
  
  /* select MySQL / flat file mode*/
  db_mode=getenv("DATE_INFOLOGGER_MYSQL");
  if (db_mode!=NULL) {
    if (!strcmp(db_mode,"TRUE")) {
      /* launch sql thread (extern function) */
      pthread_create(&t->thread,NULL,(void *) &insert_th_loop,(void *) t);
      return 0;
    }
  }

  /* launch flat-file thread */
  pthread_create(&t->thread,NULL,(void *) &insert_th_loop_nosql,(void *) t);

  return 0;
}


/*close an insertion thread */
int insert_th_stop(insert_th * t){

  /* request shutdown */
  pthread_mutex_lock(&t->shutdown_mutex);
  t->shutdown=1;
  pthread_mutex_unlock(&t->shutdown_mutex);
  
  /* wait */
  slog(SLOG_DEBUG,"waiting insert_thread to stop");
  pthread_join(t->thread,NULL);
  slog(SLOG_DEBUG,"insert_thread stopped");
  
  /* clean */
  ptFIFO_destroy(t->queue);  /* the queue is emptied by the insert thread on shutdown to avoid losing data */
  pthread_mutex_destroy(&t->shutdown_mutex);

  return 0;
}


/**************************/
/* non-SQL implementation */
/* Insert to flat file    */
/**************************/

#define MAX_PATH 1000           /* max size of log */
#define DEFAULT_LOG_DIR "/tmp"  /* default logging directory */
#define MAX_TIME_STR 30         /* max size of timestamp */

/* insertion thread main loop */
int insert_th_loop_nosql(void *arg){
  insert_th       *t;                     /* thread handle */
  infoLog_msg_t   *m;                     /* message received */
  FILE            *fd;                    /* log file */
  char            *logdir;
  char            path[MAX_PATH];
  char            *msg,*nl;
  
  if (arg==NULL) {
    slog(SLOG_ERROR,"insert_th_loop(): bad parameters");
    return -1;
  }

  /* open file */
  logdir=getenv("DATE_SITE_LOGS");
  if (logdir==NULL) logdir=DEFAULT_LOG_DIR;
  snprintf(path,MAX_PATH,"%s/dateLogs",logdir);
  fd=fopen(path,"a");
  if (fd==NULL) {
    slog(SLOG_ERROR,"Can't open log file %s",path);
    return -1;
  }
  slog(SLOG_INFO,"Received messages written to file %s",path);

  
  /* receive and store messages */
  t=(insert_th *)arg;
  for(;;){

    /* read sample from FIFO - timeout 1 sec */
    m=(infoLog_msg_t *)ptFIFO_read(t->queue,1);
    
    /* FIFO empty? */
    if (m!=NULL) {      
      
      /* re-format message with multiple line */
      for(msg=m->message;msg!=NULL;msg=nl) {
        nl=strchr(msg,'\f');
        if (nl!=NULL) {
          *nl=0;
          nl++;
        }
      
        fprintf(fd,"%f\t%c\t%s\t%d\t%s\t%s\t%s\t%s\t%d\t%s\n",
          m->timestamp,
          *m->severity,
          m->hostname,
          m->pid,
          m->username,
          m->system,
          m->facility,
          m->dest,
          m->run,
          msg
        );
      }
      
      /* free message */
      infoLog_msg_destroy(m);

      /* loop until empty queue */
      continue;
    }

    /* flush file */
    fflush(fd);

    /* shutdown requested? */
    pthread_mutex_lock(&t->shutdown_mutex);
    if (t->shutdown) {
      pthread_mutex_unlock(&t->shutdown_mutex);
      break;
    }
    pthread_mutex_unlock(&t->shutdown_mutex);
  }

  /* close log file */
  fclose(fd);
  slog(SLOG_INFO,"Log file %s closed",path);

  return 0;
}



/****************************/
/* Dispatch thread          */
/****************************/
/* structure used to communicate with a dispatch thread */
#define DISPATCH_TH_QUEUE_SIZE 20000
#define DISPATCH_MAX_CLIENTS 5
#define DISPATCH_LOG_HEADER "Dispatch thread - "
#define DISPATCH_BUFFER_SIZE 200

typedef struct _dispatch_th{
  struct ptFIFO *     queue;            /**< The queue of messages to be dispatched (these are NULL terminated strings) */
  pthread_t           thread;           /**< Handle to the thread */  

  int                 shutdown;         /**< set to 1 to stop thread */
  pthread_mutex_t     shutdown_mutex;   /**< lock on shutdown variable */
  
} dispatch_th;


/* dispatch thread main loop */
/* Creates a listening socket. Clients get a copy of what is put in dispatch thread FIFO */
int dispatch_th_loop(void *arg){
  dispatch_th         *t;                                 /* thread handle */
  int                 listen_sock;                        /* listening socket */
  int                 clients[DISPATCH_MAX_CLIENTS];      /* clients */
  char                buffer[DISPATCH_BUFFER_SIZE];       /* generic purpose buffer */
  int                 opts;                               /* socket options */
  struct sockaddr_in  srv_addr;                           /* socket address */
  int                 i;                                  /* counter */

  fd_set select_read;   /* List of sockets to select */
  int highest_sock;
  struct timeval tv;
  int result;
  int new_cl_sock;                  /* socket address for new client */
  struct sockaddr_in new_cl_addr;   /* address of new client */
  int cl_addr_len;                  /* address length */
  int want_delay;                /* set to one if nothing to do */

  char *m;                        /* message */  
  int size_m;                     /* message size */
            
  t=(dispatch_th *)arg;
  slog(SLOG_INFO,DISPATCH_LOG_HEADER "PID %d listening on port %d",getpid(),date_socket_infolog_tx);
  for(i=0;i<DISPATCH_MAX_CLIENTS;i++){
    clients[i]=-1;
  }  


  /* initialize receiving socket */

  /* create a socket */
  if ((listen_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    slog(SLOG_ERROR,DISPATCH_LOG_HEADER "socket - %s",strerror_r(errno,buffer,DISPATCH_BUFFER_SIZE));
    return -1;
  }

  /* make sure re-bind possible without TIME_WAIT problems */
  opts=1;
  setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &opts, sizeof(opts));	

  /* use keep alive messages */
  opts=1;
  setsockopt(listen_sock, SOL_SOCKET, SO_KEEPALIVE, &opts, sizeof(opts));	

  /* define server address / port */
  bzero((char *) &srv_addr, sizeof(srv_addr));
  srv_addr.sin_family = AF_INET;
  srv_addr.sin_addr.s_addr = INADDR_ANY;
  srv_addr.sin_port=htons(date_socket_infolog_tx);

  /* bind socket */
  if (bind(listen_sock, (struct sockaddr *) &srv_addr, sizeof(srv_addr)) < 0) {
    slog(SLOG_ERROR,DISPATCH_LOG_HEADER "bind - %s",strerror_r(errno,buffer,DISPATCH_BUFFER_SIZE));
    close(listen_sock);
    return -1;
  }

  /* queue length for incoming connections */
  if (listen(listen_sock, DISPATCH_MAX_CLIENTS) < 0) {
    slog(SLOG_ERROR,DISPATCH_LOG_HEADER "listen - %s",strerror_r(errno,buffer,DISPATCH_BUFFER_SIZE));
    close(listen_sock);
    return -1;
  }

  for(;;){
    want_delay=1;

    /* create a 'select' list, with listening socket (we don't care what clients send) */
    FD_ZERO(&select_read);
    /* listening socket */
    FD_SET(listen_sock,&select_read);
    highest_sock=listen_sock;
    /* add the connected clients */
    for(i=0;i<DISPATCH_MAX_CLIENTS;i++) {
      if (clients[i]!=-1) {
        FD_SET(clients[i],&select_read);
        if (clients[i] > highest_sock) {
          highest_sock=clients[i];
        }
      }
    }

    /* select returns immediately */
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    result=select(highest_sock+1,&select_read, NULL, NULL, &tv);

    if (result<0) {
      /* an error occured */
      slog(SLOG_ERROR,DISPATCH_LOG_HEADER "select - %s",strerror_r(errno,buffer,DISPATCH_BUFFER_SIZE));
    } else if (result>0) {

      /* some sockets are ready */
      want_delay=0;
      
      /* read from clients */	
      for(i=0;i<DISPATCH_MAX_CLIENTS;i++) {
        if (clients[i]!=-1) {
          if (FD_ISSET(clients[i],&select_read)) {
            result=read(clients[i],buffer,DISPATCH_BUFFER_SIZE);
            if (result>0) {
              /* success */
              /* we don't use the input */
              buffer[result]=0;
              printf("buffer: %s\n",buffer);
            }
            /* error reading socket ? */
            if (result<0) {
              slog(SLOG_ERROR,DISPATCH_LOG_HEADER "read (%d) - %s",errno,strerror_r(errno,buffer,DISPATCH_BUFFER_SIZE));
              result=0;
            }
            /* close connection on EOF / error */
            if (result==0) {
              slog(SLOG_INFO,DISPATCH_LOG_HEADER "Connection %d closed",i+1);
              close(clients[i]);
              clients[i]=-1;
            }	        
          }
        }
      }

      /* new connection ? */
      if (FD_ISSET(listen_sock,&select_read)) {
        cl_addr_len=sizeof(new_cl_addr);
        new_cl_sock = accept(listen_sock, (struct sockaddr *) &new_cl_addr, &cl_addr_len);
        if (new_cl_sock < 0) {
          slog(SLOG_ERROR,DISPATCH_LOG_HEADER "accept - %s",strerror_r(errno,buffer,DISPATCH_BUFFER_SIZE));
        } else {
          slog(SLOG_INFO,DISPATCH_LOG_HEADER "%s connected on port %d",inet_ntoa(new_cl_addr.sin_addr),new_cl_addr.sin_port);        
          for(i=0;i<DISPATCH_MAX_CLIENTS;i++) {
            if (clients[i]==-1) break;
          }
          if (i==DISPATCH_MAX_CLIENTS) {
            slog(SLOG_ERROR,DISPATCH_LOG_HEADER "Too many connections - closing");
            close(new_cl_sock);
          } else {
/* Non blocking */
/*            opts = fcntl(new_cl_sock,F_GETFL);
            if (opts == -1){
              slog(SLOG_ERROR,DISPATCH_LOG_HEADER "fcntl - F_GETFL");
              close(new_cl_sock);
            } else {
              opts = (opts | O_NONBLOCK);
              if (fcntl(new_cl_sock,F_SETFL,opts) == -1){
                slog(SLOG_ERROR,DISPATCH_LOG_HEADER "fcntl - F_SETFL");
                close(new_cl_sock);
              } else {
                slog(SLOG_INFO,DISPATCH_LOG_HEADER "Assigned connection %d",i+1);
                clients[i]=new_cl_sock;
              }
            }
*/
            slog(SLOG_INFO,DISPATCH_LOG_HEADER "Assigned connection %d",i+1);
            clients[i]=new_cl_sock;
          }
        }
      }      
    }

    /* dispatch pending messages */
    for(;;) {
          m=(char *)ptFIFO_read(t->queue,0);
          if (m==NULL) break;
          want_delay=0;
          size_m=strlen(m);
          for(i=0;i<DISPATCH_MAX_CLIENTS;i++){
            if (clients[i]==-1) continue;
            result=write(clients[i],m,size_m);
            if (result>0) {
              result=write(clients[i],"\n\0",2);
            }
            if (result<=0) {
              slog(SLOG_INFO,DISPATCH_LOG_HEADER "Write failed - connection %d closed",i+1);
              close(clients[i]);
              clients[i]=-1;
            }            
          }
          checked_free(m);
    }
  
    /* wait if nothing happened in last iteration */
    if (want_delay) {
      usleep(100000);
    }

    /* shutdown requested? */
    pthread_mutex_lock(&t->shutdown_mutex);
    if (t->shutdown) {
      pthread_mutex_unlock(&t->shutdown_mutex);
      break;
    }
    pthread_mutex_unlock(&t->shutdown_mutex);
  }

  /* close sockets */
  close(listen_sock);
  for(i=0;i<DISPATCH_MAX_CLIENTS;i++){
    close(clients[i]);
  }  

  return 0;
}


/* start dispatch thread */
int dispatch_th_start(dispatch_th * t){

  /* create queue */
  t->queue=ptFIFO_new(DISPATCH_TH_QUEUE_SIZE);
  
  /* init shutdown variable */
  t->shutdown=0;
  pthread_mutex_init(&t->shutdown_mutex,NULL);
  
  /* launch thread */
  pthread_create(&t->thread,NULL,(void *) &dispatch_th_loop,(void *) t);

  return 0;
}


/* close dispatch thread */
int dispatch_th_stop(dispatch_th * t){

  /* request shutdown */
  pthread_mutex_lock(&t->shutdown_mutex);
  t->shutdown=1;
  pthread_mutex_unlock(&t->shutdown_mutex);
  
  /* wait */
  slog(SLOG_DEBUG,"waiting dispatch_thread to stop");
  pthread_join(t->thread,NULL);
  slog(SLOG_DEBUG,"dispatch_thread stopped");
  
  /* clean */
  ptFIFO_destroy(t->queue);  /* the queue is emptied by the dispatch thread on shutdown to avoid losing data */
  pthread_mutex_destroy(&t->shutdown_mutex);

  return 0;
}





/****************************/
/** Print out program usage.*/
/****************************/
static void usage(){

  printf("usage: infoLoggerServer [-o] [-n threads] [-h]\n");
  printf("  -o          : start in the shell, log to stdout instead of log files\n");
  printf("  -n threads  : number of threads connected to MySQL server\n");
  printf("  -t nmsgs    : run in test mode, just insert nmsgs to database");
  printf("  -h          : this help\n");
} /* End of usage */



/****************************/
/* Main loop                */
/****************************/

/* Following def is to output statistics about sample processing time in main loop */
/*
#define TIME_SAMPLE_PROCESSING
*/


int main(int argc, char * const argv[]){


  /* define the servers */
  #define N_SERVERS 1       /* The number of servers (transports) we have */
  int n_servers=N_SERVERS;
  TR_server_configuration   srv_config[N_SERVERS];
  TR_server_handle          srv_h[N_SERVERS];

  /* other locals */
  int i,j;
  TR_file *f;
  TR_blob *b;
  int want_delay;
  infoLog_msg_t *msg;
  char *m;
  char *logdir;
  char logfile[1000];
  int option; 
  int daemonize=1;
  int ntest=0;
      
  /* insert thread pool */
  int n_insert_pool=1;
  insert_th *insert_pool;
  int insert_count;

  /* dispatch thread */
  dispatch_th     dispatch;

  long long received_messages=0;

  #ifdef TIME_SAMPLE_PROCESSING
  int nstat=1;
  #endif


  /* parse command line parameters */
  while((option = getopt(argc, argv, "on:t:h")) != -1){
    switch(option){
    case 'o': 
      daemonize=0;
      break;

    case 'n':
      if (sscanf(optarg,"%d",&n_insert_pool)!=1) {
        printf("-n %s: bad number of threads\n",optarg);
        return -1;
      }
      break;
      
    case 't':
      if (sscanf(optarg,"%d",&ntest)!=1) {
        printf("-n %s: bad number of test messages\n",optarg);
        return -1;
      }
      /* do not start transport in test mode */
      n_servers=0;
      break;

    case 'h':
    case '?':    
      usage();
      return 0;

    default:
      usage();
      return -1;
    }
  }


  if (daemonize) {
    /* log messages to $DATE_SITE_LOGS */
    logdir=getenv("DATE_SITE_LOGS");
    if (logdir!=NULL) {
      snprintf(logfile,1000,"%s/infoLoggerServerLog",logdir);
      // slog(SLOG_INFO,"Going in the background, logs stored in %s",logfile);

      /* start as a daemon */
      daemon(0,0);

      slog_set_file(logfile,SLOG_FILE_OPT_NONE);    
    } else {
      slog(SLOG_INFO,"$DATE_SITE_LOGS undefined");
      return 1;    
    }
  }
  
  /* read environment config */
  if (  (getenv_int("DATE_SOCKET_INFOLOG_RX",&date_socket_infolog_rx))
     || (getenv_int("DATE_SOCKET_INFOLOG_TX",&date_socket_infolog_tx))
     ) {
    slog(SLOG_FATAL,"Bad environment configuration - exiting");
    return 1;
  }  
  

  /* log all messages - for development only */
  // slog_debug_on();


  /* Start message */
  slog(SLOG_INFO,APP_NAME " " APP_VERSION " started - pid %d",getpid());

  /* Initialization of signal handlers */
  if (!daemonize)  signal(SIGINT, signal_handler);  /* CTRL-C */
  signal(SIGQUIT, signal_handler);  /* termination signal */
  signal(SIGTERM, signal_handler);  /* termination signal */
  signal(SIGPIPE, signal_handler);  /* PIPE signal */
  
/* simulate several start/stop to check for memory leaks - for development only */
for(j=0;j<1;j++){
/*
sleep(1);
shutdown_request=0;
*/

  /* create thread pool struct */
  insert_pool=checked_malloc(n_insert_pool*sizeof(insert_th));


  /* Setup transport layers */
  for(i=0;i<n_servers;i++) {

    /* configure */
    switch(i) {
    
      case 0:
        srv_config[i].server_type  = TR_SERVER_TCP;  /* TCP server  */
        srv_config[i].server_port  = date_socket_infolog_rx;          /* server port */
        srv_config[i].max_clients  = 500;            /* max clients */
        srv_config[i].queue_length = 10000;          /* queue size  */
        break;
        
      default:
        slog(SLOG_FATAL,"%s:%d - server %d not configured",__FILE__,__LINE__,i);
      
    }
    
    /* start */
    srv_h[i]=TR_server_start(&srv_config[i]);
    if (srv_h[i]==NULL) {
      slog(SLOG_ERROR,"Server %d can not start",i);
    }
  }

  /* Setup insertion thread(s) */
  if (n_insert_pool) {
    slog(SLOG_INFO,"Preparing %d connections to database",n_insert_pool);
  } else {
    slog(SLOG_WARNING,"Server launched without insert thread, received messages will be lost");
  }
  for (i=0;i<n_insert_pool;i++){
    insert_th_start(&insert_pool[i]);
  }
  insert_count=0;

  /* Setup dispatch thread */
  dispatch_th_start(&dispatch);


  if (ntest) {
    sleep(2); /* sleep to let time to insert threads to init */
    slog(SLOG_INFO,"Test mode, writing %d messages",ntest);
    infoLog_msg_t *tmsg;
    time_t now;  
    now=time(NULL);

    timer_start();    
    for (i=0;i<ntest;i++) {
      tmsg=infoLog_msg_create();
      tmsg->severity="I";
      tmsg->timestamp=now;
      tmsg->hostname="localhost";
      tmsg->pid=0;
      tmsg->username="usertest";
      tmsg->system="test";
      tmsg->facility="test";
      tmsg->dest="test";
      tmsg->run=0;
      tmsg->message="This is a test message";    /* message */
      tmsg->data=NULL;
      
      if (n_insert_pool) {
        ptFIFO_write(insert_pool[insert_count].queue, (void *)tmsg, -1);
        insert_count++;
        if (insert_count==n_insert_pool) {
          insert_count=0;
        }
      } else {
        infoLog_msg_destroy(tmsg);
      }
    }
    for(i=0;i<n_insert_pool;i++) {
      for(;;){
        if (ptFIFO_space_used(insert_pool[i].queue)==0) break;
        usleep(1000);
      }
    }
    timer_stop();
    
    slog(SLOG_INFO,"Done, in %.03fs, %d msg/s",(float)(timer_getval()/1000000.0),(int) (ntest/(timer_getval()/1000000.0)));
    
    
  } else for(;;){
  
    /* should we wait a bit at the end of the loop if nothing to do? */
    want_delay=1;
    
    /* Read and dipatch data from transport sources */
    for(i=0;i<n_servers;i++){
      
      if (srv_h[i]!=NULL) {

        /* read a file from transport */
        /* a file is a collection of messages, with a format depending on the transport used */
        f=TR_server_get_file(srv_h[i],0);

        if (f!=NULL) {

          #ifdef TIME_SAMPLE_PROCESSING
          // print average processing time every N samples received
          if ((nstat%1000)==0) {
            printf("%lld - Average processing time of a sample: %d us\n",received_messages,timer_getval()/1000);
            nstat=1;
            timer_start();
          } else {
            timer_continue();
          }
          #endif


          /* the queue is not empty, we should continue asap */
          want_delay=0;

//          TR_file_dump(f);

          /* dispatch copies of messages */
         for (b=f->first;b!=NULL;b=b->next){
           m=checked_strdup(b->value);
           ptFIFO_write(dispatch.queue,m,1);
         }

          /* decode message */
          msg=infoLog_decode(f);
          if (msg==NULL) {
            slog(SLOG_WARNING,"Failed to decode message");
          } else {

            received_messages++;
          
            /* insert message in queue */
            /* the insert thread is in charge of freeing the sample */
            if (n_insert_pool) {
              ptFIFO_write(insert_pool[insert_count].queue, (void *)msg, -1);
              insert_count++;
              if (insert_count==n_insert_pool) {
                insert_count=0;
              }
            } else {
              infoLog_msg_destroy(msg);
            }
            
          }
          
          /* acknowledge file */
          TR_server_ack_file(srv_h[i],&f->id);

          /* destroy file */          
          TR_file_destroy(f);          

          #ifdef TIME_SAMPLE_PROCESSING
          timer_stop();
          nstat++;
          #endif          
        }
      }      
    }


    /* if nothing to do, wait a bit */
    if (want_delay) {
      usleep(10000);
    }    



    /* simulate load (for development): stale the server for a few seconds */
/*
    if (time(NULL)%10==0) {
      slog(SLOG_INFO,"Sleep");
      sleep(8);
      timer_start();
    }
*/


    /* shutdown? */
    if (shutdown_request) {
      slog(SLOG_INFO, APP_NAME " shutting down");      
      break;
    }
    
  }


  /* Close transport layers */
  for(i=0;i<n_servers;i++) {
    if (srv_h[i]!=NULL) {
      TR_server_stop(srv_h[i]);
    }
  }


  /* Close insertion thread(s) */
  /* request shutdown in parallel, then wait sequentially */
  for (i=0;i<n_insert_pool;i++){
    pthread_mutex_lock(&insert_pool[i].shutdown_mutex);
    insert_pool[i].shutdown=1;
    pthread_mutex_unlock(&insert_pool[i].shutdown_mutex);
  }

  for (i=0;i<n_insert_pool;i++){
    insert_th_stop(&insert_pool[i]);
  }

  /* Close dispatch thread */
  dispatch_th_stop(&dispatch);

  /* Destroy thread pool struct */
  checked_free(insert_pool);

  /* Stop message */
  slog(SLOG_INFO,"%lld messages collected during uptime",received_messages);
  slog(SLOG_INFO,APP_NAME " stopped");


  /* Check no memory left */
  if (checked_memstat()) slog(SLOG_WARNING,"There are still %ld items allocated",checked_memstat());


// end of start/stop loop simulation
}


  return 0;
}
