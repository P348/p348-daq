#! /bin/sh
#
#  Script to setup DATE environment and infoLogger specific variables
#  
#
#  26/07/2005  SC   Script created

usage() {
  echo "Usage: $0 [-f FILE]"
  echo "  -f : dump infoLogger configuration to a file"
  echo "  -h : help"
  echo "This script loads the environment variables for infoLogger."
}

DUMPFILE=""
while getopts 'hf:' OPTION; do
 case $OPTION in
  h|\?)
   usage
   exit 0 ;;
  f)
   DUMPFILE="${OPTARG}" ;;
 esac
done



# Check base variables
if [ "$DATE_ROOT" = "" ]; then
  export DATE_ROOT=/date
fi

if [ "$DATE_SITE" = "" ]; then
  echo "DATE_SITE undefined"
  exit 1
fi


# Load general DATE environment
. $DATE_ROOT/setup.sh


# Load infoLogger specific environment
if [ "$DATE_DB_MYSQL" = "TRUE" ]; then
  eval `$DATE_DB_DIR/loadEnvDB.tcl -b -C infoLogger`
else
  dateSetup $DATE_SITE_CONFIG/infoLogger.config
fi


# Optionnaly dump infoLogger config to a file
if [ "$DUMPFILE" != "" ]; then
  rm -f $DUMPFILE
  echo "# InfoLogger configuration
# `date`
# DATE_SITE = $DATE_SITE
DATE_INFOLOGGER_LOGHOST $DATE_INFOLOGGER_LOGHOST 
DATE_INFOLOGGER_MYSQL $DATE_INFOLOGGER_MYSQL
DATE_INFOLOGGER_MYSQL_HOST $DATE_INFOLOGGER_MYSQL_HOST
DATE_INFOLOGGER_MYSQL_USER $DATE_INFOLOGGER_MYSQL_USER
DATE_INFOLOGGER_MYSQL_PWD $DATE_INFOLOGGER_MYSQL_PWD
DATE_INFOLOGGER_MYSQL_DB $DATE_INFOLOGGER_MYSQL_DB" > $DUMPFILE
fi
