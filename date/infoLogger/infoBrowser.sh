#! /bin/sh

# Setup DATE and infoLogger environment
if [ "${DATE_INFOLOGGER_DIR}" = "" ]; then
  DATE_INFOLOGGER_DIR=/date/infoLogger
fi
. ${DATE_INFOLOGGER_DIR}/infoLoggerConfig.sh --


# try to find tk 8.4
if [ "$DATE_TK_PATH" = "" ]; then
  SHELL=`locate bin/wish8.4 | head -1`
else
  SHELL="$DATE_TK_PATH"
fi
if [ "$SHELL" = "" ]; then
  ${DATE_INFOLOGGER_DIR}/infoBrowser.tcl &
else
  $SHELL -f ${DATE_INFOLOGGER_DIR}/infoBrowser.tcl &
fi


sleep 1
