#!/usr/bin/perl

use IO::Handle;

my $p = $ENV{'DATE_INFOLOGGER_BIN'};
my $f = 'UNKNOWN';
$f = $ARGV[0] if $#ARGV >= 0;
open(OUT_INFO, "|$p/logFromStdin -f $f")
  or die "cannot open info stream: $!";
open(OUT_ERROR, "|$p/logFromStdin -s error -f $f")
  or die "cannot open error stream: $!";
open(OUT_FATAL, "|$p/logFromStdin -s fatal -f $f")
  or die "cannot open fatal stream: $!";

OUT_INFO->autoflush(1);
OUT_ERROR->autoflush(1);
OUT_FATAL->autoflush(1);

while (<STDIN>) {
  if (/FATAL/) {
    print OUT_FATAL;
  } elsif (/(ERROR)|(core dumped)/) {
    print OUT_ERROR;
  } else {
    print OUT_INFO;
  }
}

close OUT_INFO;
close OUT_ERROR;
close OUT_FATAL;
