#!/usr/bin/wish -f
 
#############################################################
# This GUI allows to store log messages to the DAQ logbook


# Add mysqltcl path
lappend auto_path $env(DATE_ROOT)/local
 
# Use infoLogger library - and copy logs to stdout
load $env(DATE_INFOLOGGER_BIN)/libInfo_tcl.so infoLogger
set logFacility "notebook"
set logStream "logbook"

# Widgets management
proc addWidget {name vlabel nlines} {
  label .finput.l_${name} -text "$vlabel" -justify right
  if {$nlines>1} {
    text .finput.e_${name} -width 80 -height $nlines
  } else {
    entry .finput.e_${name} -width 15
  }
  frame .finput.f_${name}
  grid .finput.l_${name} .finput.e_${name} .finput.f_${name} -sticky ew -padx 1
}

# create window
wm title . "noteBook - DATE_SITE = $env(DATE_SITE)"
wm geometry  . 800x400

# create widgets
frame .finput
addWidget "op" "Operator" 1
addWidget "run" "Run Number" 1
addWidget "msg" "Message" 15




# Additionnals buttons

# clear msg
proc clearMsg {} {
  .finput.e_msg delete 0.0 end
  focus .finput.e_msg
}


# get run number
set db ""
proc getRun {} {
  global db
  global env

  # connect database if not done yet
  if {"$db"==""} {
    catch {
      if {$env(DATE_DB_MYSQL)=="TRUE"} {

        # define connection parameters
        set db_user $env(DATE_DB_MYSQL_USER)
        set db_pwd $env(DATE_DB_MYSQL_PWD)
        set db_host $env(DATE_DB_MYSQL_HOST)
        set db_db $env(DATE_DB_MYSQL_DB)

        if [ catch {package require mysqltcl} ] {
          puts "Error - package mysqltcl required"
          return
        }

        if [ catch {set db [mysqlconnect -host $db_host -user $db_user -password $db_pwd -db $db_db]} ] {
          puts "Error - failed to connect to database"
          return
        }
      }
    }
  }
  
  if {"$db"==""} { return }

  catch {
    set msgs [mysqlsel $db "select VALUE from GLOBALS where NAME=\"Run Number\"" -list]
    set run [lindex $msgs 0]
    .finput.e_run delete 0 end
    .finput.e_run insert 0 $run
  }
}


# do log
proc doLog {} {
  global env
  global logFacility
  global logStream
  update
  set env(DATE_RUN_NUMBER) [.finput.e_run get]
  global user
  if {[.finput.e_op get]!=$user} {
      set user [.finput.e_op get]
    infoSetUserName "$user"
  }
  set msg [.finput.e_msg get 0.0 end]
  set msg [string range $msg 0 [expr [string length $msg] - 2 ]]
  infoLogTo $logFacility $logStream "I" $msg
}


# log & next : log, clear msg, get log
button .finput.logandnew -text "Log & new" -command {
  doLog
  getRun
  clearMsg
}


# set layout
button .finput.f_msg.clear -text "Clear" -command {clearMsg}
button .finput.f_msg.log -text "Log" -command {doLog}
button .finput.f_run.get -text "Get" -command {getRun}

pack .finput.f_msg.clear .finput.f_msg.log -fill x
pack .finput.f_run.get -fill x

pack .finput.logandnew -side bottom -fill x
pack .finput -expand 1 -fill both
grid .finput.logandnew -col 1 -pady 10 -sticky ew



# Initialize notebook
set user ""
infoLog $logFacility "I" "Notebook started"
