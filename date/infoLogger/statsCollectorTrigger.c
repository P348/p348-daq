/*
**  statsCollectorTrigger.c
**
**  gcc -g statsCollectorTrigger.c /date/dim/linux/libdim.a -I/date/dim/dim/ -o statsCollectorTrigger -lpthread
**
**  This client should be execve()'d from with runControl at the point
**  where a run has finished/ended. It would be polite to create an
**  argv structure containing the run number for the run just finished
**  at position 1 so we can pass it to the statsCollectorDaemon.c server

eg:

char *myargv[3];
myargv[0] = "/date/infoLogger/statsCollectorTrigger";
myargv[1] = runNumber;
myargv[2] = 0;

if((fork() == 0){
  execv("/date/infoLogger/statsCollectorTrigger",myargv);
}


**
**  This program connects to a dim service and executes a dim command which
**  informs the server (statsCollectorDaemon.c) that the statsCollector.tcl
**  script should be ran as there is new stats data waiting to be collected.
**
**  Creator: Simon Lord
**
** 7/7/2005 - SC  Replaced sleep 5 by callback on execution of command
*/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "dic.h"

#define DIM_COMMAND "DEVICE/RunStoppedTrigger/CMD"
#define LOG_HEADER  "statsCollectorTrigger : "
int              cmd_done=0;                           /* set by callback when command executed       */
                                                       /* 0 when waiting, 1 if success, -1 if failure */
pthread_mutex_t  cmd_mutex=PTHREAD_MUTEX_INITIALIZER;  /* exclusive access to cmd_done variable       */


/* callback routine, called by DIM when command executed */
void cb (long *tag, int *ret_code){
  pthread_mutex_lock(&cmd_mutex);
  if(*ret_code!=1){
    cmd_done=-1;
  } else {
    cmd_done=1;
  }
  pthread_mutex_unlock(&cmd_mutex);
}


int main(int argc, char **argv){
  int runNumber = 0;
  int status = 0;

  /* Exit if we don't have a run number */
  if(argc < 2){
    status = dic_cmnd_callback (DIM_COMMAND, NULL, 0, cb, 0);
  }
  else{
    runNumber = atoi(argv[1]);
    status = dic_cmnd_callback (DIM_COMMAND,&runNumber,sizeof(int), cb, 0);
  }

  if(status!=1){
    // fprintf(stderr,"%s dic_cmnd_callback() failed\n",LOG_HEADER);
    return 1;
  }
  
  /* wait maximum 5 seconds for the command to be acknowledged */
  int i;
  for(i=0;i<25;i++){
    pthread_mutex_lock(&cmd_mutex);
    status=cmd_done;
    pthread_mutex_unlock(&cmd_mutex);
    if (status) break;
    usleep(200000);
  }
  if (status!=1) {
    // fprintf(stderr,"%s statsCollectorDaemon not found\n",LOG_HEADER);
    return 2;
  }

  return 0;
}
