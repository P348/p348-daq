#!/usr/bin/tclsh

####################################
# stats collector
####################################


####################################
# Database initialization
####################################

# Add mysqltcl path
lappend auto_path $env(DATE_ROOT)/local

# Use infoLogger library - and copy logs to stdout
load $env(DATE_INFOLOGGER_BIN)/libInfo_tcl.so infoLogger
set logFacility "statsCollector"
set env(DATE_INFOLOGGER_STDOUT) "TRUE"
if {[llength $argv]} {
  infoLog $logFacility "I" "statsCollector started ($argv)"
} else {
  infoLog $logFacility "I" "statsCollector started"
}

# Check DATE environment
set use_db 0
catch {
  if {$env(DATE_INFOLOGGER_MYSQL)=="TRUE"} {
    set use_db 1
  }
}
if {!$use_db} {
  infoLog $logFacility "E" "statsCollector does not work with flat-file logs"
  exit -1
}

# define connection parameters
set db_user $env(DATE_INFOLOGGER_MYSQL_USER)
set db_pwd $env(DATE_INFOLOGGER_MYSQL_PWD)
set db_host $env(DATE_INFOLOGGER_MYSQL_HOST)
set db_db $env(DATE_INFOLOGGER_MYSQL_DB)

if {[string length $db_user]==0} {
  infoLog $logFacility "E" "Undefined DATE_INFOLOGGER_MYSQL_USER"
  exit -1
}
if {[string length $db_host]==0} {
  infoLog $logFacility "E" "Undefined DATE_INFOLOGGER_MYSQL_HOST"
  exit -1
}
if {[string length $db_pwd]==0} {
  infoLog $logFacility "E" "Undefined DATE_INFOLOGGER_MYSQL_PWD"
  exit -1
}
if {[string length $db_db]==0} {
  infoLog $logFacility "E" "Undefined DATE_INFOLOGGER_MYSQL_DB"
  exit -1
}




#########################################
# Source a script for creating stats info
#########################################
source "$env(DATE_INFOLOGGER_DIR)/statsCollectorStats.tcl"


#########################################
# Get command line options
#########################################

##
## options -r <run number>, -t <table>, -f
##
set myruns {}
set runs {}
set x 0
set tables {}
set tableSet 0
set runsSet 0
set force 0

while {[set opt [lindex $argv $x]] != ""} {
    switch -exact -- $opt {
	-r {
	    lappend myruns [lindex $argv [expr $x + 1]]
	    set runsSet 1
	    
	    incr x 
	}
	-t {
	    if [regexp {^[^-]} [lindex $argv [expr $x + 1]] match] {
		lappend tables [lindex $argv [expr $x + 1]]
		incr x 
	    } else {
		set tables messages
	    }
	    set tableSet 1
	}
	-f {
	    set force 1
	}
	-h {
	    puts "./statsCollector.tcl -r <run> -f -t <table>"
	    puts "\t-r <run>   : Get stats for a specific run"
	    puts "\t-t <table> : Get stats for a specific table"
	    puts "\t-t         : Get stats for current messages table"
	    puts "\t-f         : Force rerunning stats collection"
	    exit 0
	}
    }
    incr x
}

if {$force} {
    puts "Forcing statsCollection for runs in tables: $tables"
}


#########################################
# Connect database
#########################################

#infoLog $logFacility "I" "Connecting infologger database :$db_user@$db_host:$db_db ... "
puts "Connecting infologger database :$db_user@$db_host:$db_db ... "

if [ catch {package require mysqltcl 3.0} ] {
  infoLog $logFacility "E" "Package mysqltcl required"
  exit -1
}

if [ catch {set db [mysqlconnect -host $db_host -user $db_user -password $db_pwd -db $db_db]} ] {
  infoLog $logFacility "E" "Failed to connect to database $db_user@$db_host:$db_db"
  exit -1
}


#########################################
# Retrieve list of runs
#########################################

puts "Get list of available runs from tables:"

# get list of log message tables
if {$tableSet == 0} {
    foreach table [mysqlsel $db "show tables" -list] {
	if {[string match "message*" $table]} {
	    lappend tables $table
	}
    }
}

###########################################
# Get a list of runs already in the logbook
# and subtract from $runs list so we don't
# create the same stats over and over again
###########################################
if {$force == 0} {
    set query "select run from logbook where run is not null"
    set runsInLogbook [mysqlsel $db $query -list]
} else {
    set runsInLogbook {}
}

# get list of runs

foreach table $tables {
    puts "$table"
    flush stdout
    if {$runsSet} {
	set query "select distinct run from $table where "
	foreach run $myruns {
	    set query [concat $query "run = " $run " or "]
	}
	set query [lreplace $query [expr [llength $query] - 1] end]
	set query [concat $query " order by run;"]
    } else {
	set query "select distinct run from $table where run is not null order by run;"
    }
        
    foreach run [mysqlsel $db $query -list] {
	if {[lsearch -exact $runsInLogbook $run] == -1} {
	    if {[lsearch $runs $run]==-1} {
		lappend runs $run
		set run_tables($run) "$table"
	    } else {
		lappend run_tables($run) "$table"
	    }
	} else {
	    puts "Run number $run has already been statsCollected...skipping"
	}
    }
}

if {[llength $runs]} {
  #infoLog $logFacility "I" "Found [llength $runs] runs"
  puts "Found [llength $runs] runs"
  puts "\nRuns found: $runs\n"
} else {
  infoLog $logFacility "I" "No run to process"
  exit 0
}


# get time data for runs 
foreach run $runs {
    set run_start($run) -1
    set run_stop($run) -1
}
foreach table $tables {
    if {$runsSet} {
	set query "select run,timestamp,message from $table where "
	foreach run $runs {
	    set query [concat $query "run = " $run " or "]
	}
	set query [lreplace $query [expr [llength $query] - 1] end]
	set query [concat $query " and dest=\"logbook\" and facility=\"runControl\" and message like \"Run st%\";"]
    } else {
	set query "select run,timestamp,message from $table where run is not null and dest=\"logbook\" and facility=\"runControl\" and message like \"Run st%\";"
    }
    
    set details [mysqlsel $db $query -list]
    
    foreach item $details {
	set v_run  [lindex $item 0]
	set v_time [lindex $item 1]
	set v_msg  [lindex $item 2]
	
	if {[string match "Run starting" $v_msg]} {
	    set run_start($v_run) $v_time
	} elseif {[string match "Run stopped" $v_msg]} {
	    set run_stop($v_run) $v_time
	} else {
	}
    }
}

set nsuccess 0

# build logbook entry for each run
foreach run $runs {
    
    if {($run_start($run)==-1) || ($run_stop($run)==-1)} {
	puts "Incomplete data for run $run"
	continue
    }
    
    puts "Run $run: [clock format [expr int($run_start($run))] -format "%d/%m/%Y %H:%M:%S"] to [clock format [expr int($run_stop($run))] -format "%d/%m/%Y %H:%M:%S"]"
    puts "Reading data from $run_tables($run)"
    
    set n 0
    set query "select message from ("
    foreach table $run_tables($run) {
	if {$n!=0} {
	    set query "$query union all"
	}
	set query "$query select facility,hostname,timestamp,run,message from $table where dest=\"logbook\" and run=\"$run\""
	incr n
    }
    set query "$query order by run,facility, hostname, timestamp) as full_query;"
    
    set log [join [mysqlsel $db $query -flatlist] "\n"]
    set log [::mysql::escape $db $log]
    
    set query "replace into logbook values(\"$run\",\"$run_start($run)\",\"$run_stop($run)\",\"$log\",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);"
    set qu_h [mysqlquery $db $query]
    mysqlendquery $db
    
    regsub -all {\\n} "$log" "\n" log
    
    ## create statistics and add to db
    if [catch {createStats $run $log} msg] {
	puts "Could not create stats for run: $run, $msg"
    } else {
      incr nsuccess
    }
}


infoLog $logFacility "I" "Statistics created - ${nsuccess}/[llength $runs] ok"
if {$nsuccess>0} {
  exit 1
} else {
  exit 0
}
