/** Infologger server
  *
  * This file implements the MySQL insert function.
  *
  * History:
  *  - 07/04/2005  Added Run Number field
*/


#include <stdlib.h>

#include "infoLoggerServer_insert.h"
#include "simplelog.h"

#include <string.h>
#include "utility.h"
#include <stdio.h>
#include <unistd.h>

#include <mysql.h>

#define SQL_INSERT_QUERY "INSERT INTO messages VALUES(?,?,?,?,?,?,?,?,?,?)"
#define SQL_INSERT_NPAR  10      /* number of columns in table */      
#define SQL_RETRY_CONNECT 10    /* base retry time, will be sleeping up to 10x this value */


#define BUFFER_SIZE 1000

/* Command to create tables (check in newDateLogs.sh):
# definition of message table
TABLEDESCR='severity char(1), timestamp double(16,6), hostname varchar(20), pid int, username varchar(20), system varchar(20), facility varchar(20), dest varchar(20), run int, message text \
,index ix_severity(severity), index ix_timestamp(timestamp), index ix_hostname(hostname), index ix_facility(facility), index ix_dest(dest), index ix_run(run), index ix_system(system)'
*/  


/* insertion thread main loop */
int insert_th_loop(void *arg){
  insert_th       *t;                     /* thread handle */
  MYSQL           db;                     /* database handle */
  MYSQL_STMT      *stmt=NULL;             /* prepared insertion query */
  int             param_count;            /* number of parameters to insert */
  MYSQL_BIND      bind[SQL_INSERT_NPAR];  /* parameters bound to variables */
  infoLog_msg_t   *m=NULL;                /* message received */

  char *db_host;    /* MySQL server host */
  char *db_user;    /* MySQL user */
  char *db_pwd;     /* MySQL password */
  char *db_infodb;  /* MySQL infologger db */

  char buffer[BUFFER_SIZE];

  int n_retry=0;    /* number of connection retry */
  int exit_loop=0;  /* to exit insert loop */
  int i;            /* simple counter */  

  char *msg,*nl;    /* variables used to reformat multiple-line messages */
  int reconnect;    /* set to 1 to ask db reconnection */

  my_bool run_isnull; /* boolean telling if run number NULL */

  /* arg = insert thread struct */
  if (arg==NULL) {
    slog(SLOG_ERROR,"insert_th_loop(): bad parameters");
    return -1;
  }
  t=(insert_th *)arg;

  /* take connection parameters from environment */
  /* mysql_connect handles correctly NULL values, no need to check */
  db_host=getenv("DATE_INFOLOGGER_MYSQL_HOST");
  db_user=getenv("DATE_INFOLOGGER_MYSQL_USER");
  db_pwd=getenv("DATE_INFOLOGGER_MYSQL_PWD");
  db_infodb=getenv("DATE_INFOLOGGER_MYSQL_DB");

  /* set default database */
  if (db_infodb==NULL) db_infodb="infologger";

  /* log connection parameters */
  slog(SLOG_INFO,"connecting MySQL: host=%s, user=%s, pwd=%s, db=%s",
    db_host ? db_host : "[localhost]",
    db_user ? db_user : "[default]",
    db_pwd ? "[YES]" : "[NO]",
    db_infodb ? db_infodb : "[default]");

  /* init mysql lib */
  if (mysql_init(&db)==NULL) {
    slog(SLOG_ERROR,"mysql_init() failed");
    return -1;
  } 
  

  /* connection loop */
  for (;;) {
      
    /* connect database (keep trying) */
    while (!mysql_real_connect(&db,db_host,db_user,db_pwd,db_infodb,0,NULL,0)) {
      if (!n_retry) {
        slog(SLOG_ERROR,"Failed to connect to database: %s",mysql_error(&db));
      }
      if (n_retry<10) {
        n_retry++;
      }
      for (i=0;i<n_retry*SQL_RETRY_CONNECT;i++) {
        /* shutdown requested? */
        pthread_mutex_lock(&t->shutdown_mutex);
        if (t->shutdown) {
          pthread_mutex_unlock(&t->shutdown_mutex);
          return 0;
        }
        pthread_mutex_unlock(&t->shutdown_mutex);
        sleep(1);
      }
    }
    n_retry=0;
    slog(SLOG_INFO,"DB connected");
    
    /* create prepared insert statement */
    stmt=mysql_stmt_init(&db);
    if (stmt==NULL) {
      slog(SLOG_ERROR,"mysql_stmt_init() failed: %s",mysql_error(&db));
      mysql_close(&db);
      slog(SLOG_INFO,"DB disconnected");
      return -1;
    }
    if (mysql_stmt_prepare(stmt,SQL_INSERT_QUERY,strlen(SQL_INSERT_QUERY))) {
      slog(SLOG_ERROR,"mysql_stmt_prepare() failed: %s\n",mysql_error(&db));
      mysql_stmt_close(stmt);
      mysql_close(&db);
      slog(SLOG_INFO,"DB disconnected");
      return -1;
    }

    /* bind variables */
    param_count=mysql_stmt_param_count(stmt);
    if (param_count!=SQL_INSERT_NPAR) {
      slog(SLOG_ERROR,"mysql_stmt_param_count(): wrong number of parameters (%d)",param_count);
      mysql_stmt_close(stmt);
      mysql_close(&db);
      slog(SLOG_INFO,"DB disconnected");
      return -1;
    }

    bind[0].buffer_type    = MYSQL_TYPE_STRING;
    bind[0].buffer         = &m->severity;
    bind[0].buffer_length  = 0;
    bind[0].is_null        = 0;
    bind[0].length         = NULL;

    bind[1].buffer_type    = MYSQL_TYPE_DOUBLE;
    bind[1].buffer         = (char *) &m->timestamp;
    bind[1].is_null        = 0;
    bind[1].length         = NULL;

    bind[2].buffer_type    = MYSQL_TYPE_STRING;
    bind[2].buffer         = &m->hostname;
    bind[2].buffer_length  = 0;
    bind[2].is_null        = 0;
    bind[2].length         = NULL;

    bind[3].buffer_type    = MYSQL_TYPE_LONG;
    bind[3].buffer         = (char *) &m->pid;
    bind[3].is_null        = 0;
    bind[3].length         = NULL;

    bind[4].buffer_type    = MYSQL_TYPE_STRING;
    bind[4].buffer         = &m->username;
    bind[4].buffer_length  = 0;
    bind[4].is_null        = 0;
    bind[4].length         = NULL;

    bind[5].buffer_type    = MYSQL_TYPE_STRING;
    bind[5].buffer         = &m->system;
    bind[5].buffer_length  = 0;
    bind[5].is_null        = 0;
    bind[5].length         = NULL;

    bind[6].buffer_type    = MYSQL_TYPE_STRING;
    bind[6].buffer         = &m->facility;
    bind[6].buffer_length  = 0;
    bind[6].is_null        = 0;
    bind[6].length         = NULL;

    bind[7].buffer_type    = MYSQL_TYPE_STRING;
    bind[7].buffer         = &m->dest;
    bind[7].buffer_length  = 0;
    bind[7].is_null        = 0;
    bind[7].length         = NULL;

    bind[8].buffer_type    = MYSQL_TYPE_LONG;
    bind[8].buffer         = (char *) &m->run;
    bind[8].is_null        = &run_isnull;
    bind[8].length         = NULL;

    bind[9].buffer_type    = MYSQL_TYPE_STRING;
    bind[9].buffer         = &m->message;
    bind[9].buffer_length  = 0;
    bind[9].is_null        = 0;
    bind[9].length         = NULL;


    /* receive and store messages */
    for(reconnect=0;;){

      /* read sample from FIFO - timeout 1 sec */
      /* if not already one pending */
      if (m==NULL) {
        m=(infoLog_msg_t *)ptFIFO_read(t->queue,1);
      }

      /* got a message? */
      if (m!=NULL) {   

        /* re-format message with multiple line */
        for(msg=m->message;msg!=NULL;msg=nl) {
          nl=strchr(msg,'\f');
          if (nl!=NULL) {
            *nl=0;
            nl++;
          }

          /* copy data */

          bind[0].buffer         = m->severity;
          bind[1].buffer         = (char *) &m->timestamp;
          bind[2].buffer         = m->hostname;
          bind[3].buffer         = (char *) &m->pid;
          bind[4].buffer         = m->username;
          bind[5].buffer         = m->system;
          bind[6].buffer         = m->facility;
          bind[7].buffer         = m->dest;
          bind[8].buffer         = (char *) &m->run;
          bind[9].buffer         = msg;

          if (m->run==-1) {
            run_isnull = 1;
          } else {
            run_isnull = 0;
          }


          bind[0].buffer_length  = strlen(bind[0].buffer);
          bind[2].buffer_length  = strlen(bind[2].buffer);
          bind[4].buffer_length  = strlen(bind[4].buffer);
          bind[5].buffer_length  = strlen(bind[5].buffer);
          bind[6].buffer_length  = strlen(bind[6].buffer);
          bind[7].buffer_length  = strlen(bind[7].buffer);
          bind[9].buffer_length  = strlen(bind[9].buffer);


          /* update bind variables */
          if (mysql_stmt_bind_param(stmt,bind)) {
            slog(SLOG_ERROR,"mysql_stmt_bind() failed: %s\n",mysql_error(&db));
            mysql_stmt_close(stmt);
            mysql_close(&db);
            slog(SLOG_INFO,"DB disconnected");
            return -1;
          }

          /* Do the insertion */
          if (mysql_stmt_execute(stmt)) {
            slog(SLOG_ERROR,"mysql_stmt_exec() failed: %s\n",mysql_error(&db));
            mysql_stmt_close(stmt);
            mysql_close(&db);
            slog(SLOG_INFO,"DB disconnected");
            /* retry with new connection - usually it means server was down */
            reconnect=1;
            break;
          }

        }
        
        /* reconnect db if needed */
        if (reconnect) break;
               
        /* free message */
        infoLog_msg_destroy(m);
        m=NULL;

        /* loop until empty queue */
        continue;
      }

      /* shutdown requested? */
      pthread_mutex_lock(&t->shutdown_mutex);
      if (t->shutdown) {
        pthread_mutex_unlock(&t->shutdown_mutex);
        /* disconnect database */
        mysql_stmt_close(stmt);
        mysql_close(&db);
        slog(SLOG_INFO,"DB disconnected");
        return 0;
      }
      pthread_mutex_unlock(&t->shutdown_mutex);
    }
  }


  /* log to file not to loose messages */
  emergency_mode:
  slog(SLOG_ERROR,"Switching to emergency mode, logging to file");
  return insert_th_loop_nosql(arg);
}
