#include <stdio.h>
#include <stdlib.h>

#include "infoLoggerServer_insert.h"
#include "simplelog.h"

int insert_th_loop(void *arg) {

  /* Exit program in case built without SQL */
  slog(SLOG_FATAL,"infoLoggerServer was not compiled with DATE_MYSQL=TRUE");
  return -1;
}
