#define LOG_FACILITY "infoLoggerTester"

#include <stdlib.h>
#include "infoLogger.h"

int main( int argc, char **argv ) {
  int runs;
  char buf[]="works or not";

  LOG_SET_DEF( "testDefault" );

  INFO_TO( "testFile", "Test INFO message to TO log stream" );

  infoLog_f(LOG_FACILITY,LOG_INFO,"Test infoLog_f %s",buf);

  for (runs = 3; --runs != 0;) {
    /*
    LOG( LOG_INFO, "Test LOG message" );
    */
    /*
    FATAL_ALL( "infoLogger", "Test LOG message" );
    */
    /*
    LOG_ALL( "infoLogger", LOG_INFO, "Test LOG message" );
    */
    INFO_DEF( "Test INFO message to default stream" );
  }
  exit( 0 );
  return 0;
}
