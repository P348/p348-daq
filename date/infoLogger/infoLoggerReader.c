/*
**
** InfoLoggerReader: Receive messages from client processes and forward to
** InfoLoggerServer.
**
** Creator: Simon Lord
**
** 27/09/04  Created.
** 13/10/04  Added fifo and second thread.
** 28/10/04  Added bail function and cleaned up.
** 29/10/04  Added usage of SC's slog logging functions.
**           Use some env variables for some settings.
** 01/11/04  Fixed bugs and added some command line options.
** 02/11/04  Added transport code to connect to infoLoggerServer.
** 05/11/04  Made a temporary bugfix in getMessage so we don't loose
**           messages anymore.
** 08/11/04  Have properly fixed bug in getMessage()
** 11/11/04  Added cmd line option for local socket eg /tmp/mysock
** 17/11/04  Day of the bug fixes!!
** 22/11/04  Increased MAXCONNECTIONS from 10 to 20.
** 02/12/04  Cleanup.
** 06/01/05  Handle SIGQUIT and SIGTERM.
**           Detach from tty and become daemon if no -o switch.
** 07/01/05  Fixed bug in bail() and added better error handling
**           changed from using DATE_INFOLOGGER_SERVER_HOST to
**           DATE_INFOLOGGER_LOGHOST for backward compatibility.
**           Slog what server host we are connecting to.
** 10/01/05  Fixed error handling for select() call.
** 17/01/05  The unix socket abstract path is now set as DATE_SITE env variable
** 18/01/05  We can now retrieve msgs from the trnsport queue that can get stuck 
**           if the infoLoggerServer is never connected to.
** 25/01/05  Fixed bug causing reader to get stuck trying to empty
**           the transport queue.
**           Fixed bug which caused infoBrowser to not function corrently
**           when reading from a local log file, after the reader had reconnected
**           to the server and told the server it has messages stored locally.
**           Fixed bug causing reader not being able to reconnect to server fully.
** 27/01/05  Tweaked code that decides if the reader has lost contact with server.
** 28/05/05  When we decide we have lost connection to server we dump anything
**           in the transport queue to disk before getting new messages, otherwise
**           some messages would be trapped in the queue until we exit the reader.
** 20/05/05  Removed all 'default' settings for socket name and server port but have
**           kept default server host as localhost. Changed socket name from 
**           $DATE_SITE to 'infoLogger-$DATE_SITE'
** 23/08/05  Removed nearly all code in 2nd thread thanks to SC's new on disk FIFO,
**           now i don't need to care about the details of the transport layer at all!
**
** $Id: infoLoggerReader.c,v 1.11 2005/08/30 15:24:09 slord Exp $
**
** Last edited by: $Author: slord $
**
**
*/




#define LOG_FACILITY "InfoLoggerReader"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <ctype.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <assert.h>
#include <string.h>
#include "utility.h"
#include "simplelog.h"
#include "transport_client.h"
#include "infoLogger.h"

#define _GNU_SOURCE

/* Default values we might need */
#define SERVER_HOST "localhost"


#define FIFO_SIZE 65535          /* Byte size of the ptFIFO between our threads    */
#define MAXCONNECTIONS 20        /* Max number of connected clients at once        */
#define TCP_RCV_BUFSIZE 131072   /* Size to set tcp receive buffer with setsockopt */
#define SERVER_CONNECT_TIMEOUT 5 /* Seconds to try and connect to server           */
#define QUEUE_SIZE 1000         /* Transport queue size                           */ 

/* Global variables */
struct    ptFIFO *fifo;          /* Thread safe fifo            */
float     space = 0.0;           /* Max fifo fill ratio         */
int       messageReceived;       /* Messages received from fifo */
int       messageSent;           /* Messages sent into fifo     */
int       messageLost;           /* Number of messages we have lost */
int       messageLoggedLocally;  /* Number logged locally */
int       shutdown_flag;         /* Thread shutdown flag        */
pthread_t msgConsumer;           /* consumer thread             */
int       serverPort = 0;        /* Server port to connect to   */
char     *serverHost = NULL;     /* Hostname of server to connect to */
char      localLogFile[PATH_MAX]; /* Where msgs go if server is unavailable */
char      logSite[PATH_MAX];      /* Full path to our personal log file     */

/* Client struct to hold the socket and a linebuffer for a connected client */
struct client {
  int fd;
  struct lineBuffer *buf;
};


/* Function Prototypes */
static void * consumerThread(void *);
static int    setNonBlocking(int);
static void   getMessage(struct client *);
static void   sigquit(int);
static void   bail(char *, int);
static void   usage();

/*
** Set the fd passed to non-blocking io
*/
static int setNonBlocking(int sock){

  long opts;
  
  opts = fcntl(sock,F_GETFL);
  if (opts == -1){
    slog(SLOG_ERROR,"socket %d: %s: fcntl(2) - F_GETFL\n",sock,strerror(errno));
    return -1;
  }
  opts = (opts | O_NONBLOCK);
  if (fcntl(sock,F_SETFL,opts) == -1){
    slog(SLOG_ERROR,"socket %d: %s: fcntl(2) - F_SETFL\n",sock,strerror(errno));
    return -1;
  }
  return 0;
}/*End of setNonBlocking() */





/*
** Get everything waiting on socket until empty
*/
static void getMessage(struct client *sock){

  int    status;        /* System call return variable                          */
  char   message[500];  /* Buffer for read() to put socket contents into        */
  char  *msg;           /* A pointer to a line from the linebuffer_getline call */

  for(;;){
    errno = 0;

    /* Read data from socket, if nothing read then close socket and destroy lineBuffer */
    if(((status = read(sock->fd,message,sizeof message)) == 0)
       || (errno == EISDIR)
       || (errno == EBADF)
       || (errno == EINVAL)
       || (errno == EFAULT)){
      close(sock->fd);
      slog(SLOG_DEBUG,"%s:Got EOF from fd %d, closing now.\n",
	strerror(errno),sock->fd);
      sock->fd = 0;
      lineBuffer_destroy(sock->buf);
      errno = 0;
      return;
    }

    /* Check for errors, we ignore EAGAIN, EINTR AND EIO */
    else if(status == -1){
      switch(errno){
      case EAGAIN:
      case EINTR:
      case EIO: errno = 0; return; break;
      default: slog(SLOG_WARNING,"getMessage: %s\n",
		    strerror(errno));return;break;
      }
    }

    /* We must have some data, add it to the lineBuffer */
    else{
      message[status] = 0;
      
      status = lineBuffer_addstring(sock->buf,message);
      if(status == -1){
	/* Can't allocate memory! */
	slog(SLOG_WARNING,"lineBuffer_addstring: Can't allocate more memory\n");
	slog(SLOG_WARNING,"Discarded message\n");
      }
      
      /*
      ** Currently we loop until no messages
      ** are left in lineBuffer.
      */
      for(;(msg = lineBuffer_getLine(sock->buf)) != NULL;){
	/* 
	** Push msg into fifo for second thread to deal with. 
	** should probably also check space available 
	*/
	for(;;){
	  status = ptFIFO_write(fifo, (void *)msg, 0);
	  if(status == -1){
	    slog(SLOG_WARNING,"ptFIFO_write error\n");
	    usleep(300);
	  }
	  else
	    break;
	}
	messageSent++;
      }

    }
  }
}/* End of getMessage() */






/*
** When we get a SIGTERM or SIGQUIT we want to join our second
** thread and then exit. This happens when main sees
** that shutdown_flag is 1.
** Unfortunetly both threads get the sigterm and call this handler
** luckily it doesn't really matter as we are only setting 
** the shutdown_flag to 1.
*/
static void sigterm(int signo){
  /*
  ** slog commented out as it caused problems, it might be doing
  ** things one should not do inside a signal handler 
  
  slog(SLOG_DEBUG,"PID of process calling sigterm():%d\n",getpid());

  */

  shutdown_flag = 1;
  signal(SIGTERM,sigterm);
  signal(SIGQUIT,sigterm);
} /* End of sigterm */






/*
** Main! See top for program description
*/
int main(int argc, char **argv){

  int status;     /* Holds the return value from system calls                      */
  int count;      /* General int used in for loops                                 */
  int totalfd;    /* Holds the current highest file descriptor for use with select */
  int modes;      /* Used to hold the 'modes' reutrned from get/setsockopt         */
  int s;          /* Our server socket file descriptor                             */
  int tmpfd;      /* Temporary socket file descriptor                              */
  int rcvbuf = 0; /* Used for setting the tcp receive buffer size                  */ 
  int option;     /* Used to store return of getopt()                              */
  int needtty = 0;/* If 1 we won't become a deamon                                 */

  char *path = NULL;          /* Pointer to abstract path specified on cmd line        */
  char *siteLogs;             /* Pointer to env variable DATE_SITE_LOGS                */
  char  asciiport[10];        /* Port number on server to connect to                   */
  char  myhost[80];           /* Our hostname -- only used in debugging for log output */
  char  socketName[256]; /* Will hold the final abstract path to use for socket to libInfo */

  socklen_t len;     /* Holds the sixe of clientaddr for use in accept() call */
  socklen_t optlen;  /* Length of modes used for getsockopt                   */

  struct client       c[MAXCONNECTIONS]; /* Array of connected clients           */
  struct client      *client = c;        /* Pointer to connected clients array c */
  struct sockaddr_un  serveraddr;        /* Server socket connection details     */
  struct sockaddr_un  clientaddr;        /* Client connection details            */

  fd_set  read_socks; /* Used in select() call                              */
  pid_t   mypid;      /* Our PID, only used for logfile name when debugging */

  umask(0); /* So that we can create files rw for user, group and other */

  /* Get hostname and PID */
  memset(myhost,0,sizeof myhost);
  status = gethostname(myhost,sizeof myhost);
  if(status == -1){
    bail("gethostname error",errno);
  }
  mypid = getpid();
  if(mypid == (pid_t)-1){
    bail("getpid error",0);
  }

  memset(localLogFile,0,sizeof localLogFile);

  /* DATE_SITE_LOGS defines logfile root directory */
  siteLogs = getenv("DATE_SITE_LOGS");
  if((siteLogs != NULL) && (strlen(siteLogs) > 0)){
    snprintf(logSite,sizeof logSite,"%s/infoLoggerReaderLog@%s",siteLogs,myhost);
    snprintf(localLogFile,sizeof localLogFile,"%s/infoLoggerReader@%s",siteLogs,myhost);
  }
  else {
    slog(SLOG_WARNING,"DATE_SITE_LOGS not defined, logging to /tmp");
    snprintf(logSite,sizeof logSite,"/tmp/infoLoggerReaderLog@%s",myhost);
    snprintf(localLogFile,sizeof localLogFile,"/tmp/infoLoggerReader@%s",myhost);
  }


  /* Set where our personal log messages end up */
  slog_set_file(logSite,0);
  status =  chmod(logSite,S_IROTH|S_IWOTH|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
  if(status == -1){
    slog(SLOG_ERROR,"Error whilst chmodding %s: %s\n",logSite,strerror(errno));
  }

  /* Handle program arguments.
  ** -p <port> specfies what port we use to connect to server
  ** -s <hostname> specifies the hostname where the infoLoggerServer
  **               is running to connect to.
  ** -d turns on debug mode
  ** -l set a specific logfile to use
  ** -o set logfile to stdout
  ** -h prints usage();
  ** -u specifies what abstract path to use for bind()
  ** and anything else prints usage();
  */
  while((option = getopt(argc, argv, "adhop:l:s:u:")) != -1){
    switch(option){
    case 'd': 
      slog_debug_on();
      slog(SLOG_DEBUG,"Debug is on.\n");
      break;

    case 'l': 
      status = slog_set_file(optarg,1);
      memset(logSite,0,sizeof logSite);
      
      strncpy(logSite,optarg,sizeof logSite);
      if(strlen(optarg)>=sizeof logSite){
	logSite[sizeof logSite -1] = '\0';
      }
      
      status =  chmod(optarg,S_IROTH|S_IWOTH|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
      if(status == -1){
	slog(SLOG_ERROR,"Error whilst chmodding %s\n",optarg);
      }
      if(status == -1){
	slog(SLOG_WARNING,"Can't open %s, logging to stdout\n",optarg);
      }
      break;

    case 'o': 
      slog_set_file(NULL,0);
      needtty = 1;
      break;

    case 'p': 
      strncpy(asciiport, strdup(optarg),sizeof asciiport);
      if(asciiport != NULL){
	serverPort = atoi(asciiport);
	slog(SLOG_INFO,"Connecting to server on port %d.\n",serverPort);
      }
      else{
	slog(SLOG_FATAL,"DATE_SOCKET_INFOLOG_RX not defined\n");
	return 1;
      }
      break;

    case 's':
      serverHost = strdup(optarg);
      if(serverHost == NULL){
	slog(SLOG_FATAL,"Error setting server host name, use -s <host>");
      }
      break;

    case 'u':
      path = strdup(optarg);
      if(path == NULL){
	slog(SLOG_FATAL,"Error getting named socket");
	return 1;
      }
      slog(SLOG_INFO,"infologger named socket: %s\n",path);
      break;

    case 'h': 
    case '?': 
      usage();
      break;
    }
  }


  /* Become a daemon ;) But only if '-o' was not specified on cmdline */
  if(!needtty){
    if(daemon(0,0) == -1){
      slog(SLOG_ERROR,"Could not become daemon: %s\n",strerror(errno));
    }
    else{
      printf("infoLoggerReader daemon started\n");
    }
  }






  /*
  ** DATE_SOCKET_INFLOGGER defines the named
  ** socket for the clients to connect to the 
  ** infoLoggerReader
  */
  if((path == NULL) || (strlen(path)<1)){
    path = getenv("DATE_SITE");
    if(path == NULL){
      slog(SLOG_FATAL,"DATE_SITE not defined\n");
      return 1;
    }
    else {
      memset(socketName,0,sizeof socketName);
      snprintf(socketName,sizeof socketName,"infoLogger-%s",path);
      
      path = socketName;
    }
  }
  slog(SLOG_INFO,"infoLoggerReader listening on named unix socket: %s\n",path);

  /* This is for the benefit of the consumerThread */
  if((serverHost == NULL) || (strlen(serverHost)<1)){
    serverHost = getenv("DATE_INFOLOGGER_LOGHOST");
  }
  if((serverHost == NULL) || (strlen(serverHost)<1)){
    bail("DATE_INFOLOGGER_LOGHOST not defined, or use -s <host>",0);
  }

  slog(SLOG_INFO,"Server host set to %s\n",serverHost);




  /* If no server port number was specified on command line get it from environment */
  if(!serverPort){
    /* read environment config */
    if (getenv_int("DATE_SOCKET_INFOLOG_RX",&serverPort)){
      slog(SLOG_FATAL,"Bad configuration - exiting");
      return 1;
    }
  }  


  /* Create our own sighandler so we can pthread_join on SIGTERM or SIGQUIT */
  signal(SIGTERM, sigterm);
  signal(SIGQUIT, sigterm);

  /* Setup our FIFO */
  fifo = ptFIFO_new(FIFO_SIZE);

  slog(SLOG_INFO,"PID of Main():%d\n",getpid());
  
  /* Create a socket to work with */
  s = socket(PF_LOCAL,SOCK_STREAM,0);
  if(s == -1){
    bail("socket(2)",errno);
  }

  /* So that we can re-bind to it without TIME_WAIT problems */  

  modes = fcntl(s,F_GETFL);
  if (modes == -1)
    bail("fcntl(2) - F_GETFL",errno);

  modes = (modes | SO_REUSEADDR);
  if (fcntl(s,F_SETFL,modes) == -1)
    bail("fcntl(2) - F_SETFL for SO_REUSEADDR",errno);

  if(setNonBlocking(s) == -1){
    bail("Could not set server socket to non blocking io",errno);
  }

  /* Make receive buffer 2 times larger */
  rcvbuf = TCP_RCV_BUFSIZE;
  status = setsockopt(s,SOL_SOCKET,SO_RCVBUF,&rcvbuf,sizeof rcvbuf);
  if(status == -1){
    bail("setsockopt(2)",errno);
  }

  /* Get receive buffer size */
  optlen = sizeof rcvbuf;
  status = getsockopt(s,SOL_SOCKET,SO_RCVBUF,&rcvbuf,&optlen);
  if(status == -1){
    bail("getsockopt(2)",errno);
  }
  slog(SLOG_DEBUG,"Current receive buffer size: %d bytes\n",rcvbuf);

  /* Connect to named socket */
  bzero(&serveraddr, sizeof(serveraddr));
  assert(sizeof path < sizeof serveraddr.sun_path);
  serveraddr.sun_family = PF_LOCAL;

  /* +1 so that we use abstract namespace, see man 7 unix! */
  strncpy(serveraddr.sun_path + 1, path, strlen(path) + 1);
  status = bind(s, (struct sockaddr *)&serveraddr, sizeof(serveraddr));  
  if(status == -1){
    bail("bind(2)",errno);
  }

  /* Set s listening with max MAXCONNECTIONS clients */
  status = listen(s,MAXCONNECTIONS);
  if(status == -1){
    bail("listen(2)",errno);
  }

  /* Create our other thread */
  if(pthread_create(&msgConsumer,NULL,consumerThread,NULL)){
    bail("pthread_create(3)",errno);
  }

  /* Initialize select fd sets */
  FD_ZERO(&read_socks);
  FD_SET(s,&read_socks);
  totalfd = s;
  
  /* Initialize connections struct array */
  for(count = 0; count < MAXCONNECTIONS; count++){
    c[count].fd = 0;
    c[count].buf = NULL;
  }

  /* Main server loops */
  for(;;){
    status = select(totalfd+1,&read_socks,NULL,NULL,NULL);
    if(status == -1 && errno == EINTR){
      /* We want to ignore this so do nothing */
    }
    else if(status == -1){
      slog(SLOG_ERROR,"select(2): %s\n",strerror(errno));

      /* Reset fd sets and continue */
      FD_ZERO(&read_socks);
      FD_SET(s,&read_socks);
      totalfd = s;
      for(count = 0; count < MAXCONNECTIONS; count++){
	if(c[count].fd != 0){
	  FD_SET(c[count].fd,&read_socks);
	  if(c[count].fd > totalfd){
	    totalfd = c[count].fd;
	  }
	}
      }
      
      continue;
    }

    /* Check to see if we need to shutdown */
    if(shutdown_flag){
      break;
    }

    if(FD_ISSET(s,&read_socks)){
      /* New connection request */
      len = sizeof clientaddr;
      tmpfd = accept(s,(struct sockaddr *)&clientaddr,&len);
      if(tmpfd == -1){
	slog(SLOG_ERROR,"accept(2) failed on socket fd: %d: %s\n",tmpfd,strerror(errno));	
      }

      if(tmpfd && !setNonBlocking(tmpfd)){
	/* Find an empty space for our new socket */
	for(count = 0; count < MAXCONNECTIONS; count++){
	  if(c[count].fd == 0){
	    c[count].fd = tmpfd;
	    c[count].buf = lineBuffer_new();
	    tmpfd = -1;
	    if(c[count].fd > totalfd){
	      totalfd = c[count].fd;
	    }
	    break;
	  }
	}
      

	if(tmpfd != -1){
	  /* No room for new connection */
	  close(tmpfd);
	  slog(SLOG_DEBUG,"Too many connections - connection closed.\n");
	}
	else{
	  slog(SLOG_DEBUG,"New connection fd: %d\n",c[count].fd);
	}
      }
    }
    
    /* 
    ** If one of the clients is active then we want to get
    ** its messages.
    */
    for(count = 0; count < MAXCONNECTIONS; count++){
      if(FD_ISSET(c[count].fd,&read_socks)){
	getMessage(client);
      }
      client++;
    }
    /* Re-point our pointer to the beginning of connection array */
    client -= MAXCONNECTIONS;

    /* Reset select list */
    FD_ZERO(&read_socks);
    FD_SET(s,&read_socks);
    totalfd = s;
    for(count = 0; count < MAXCONNECTIONS; count++){
      if(c[count].fd != 0){
	FD_SET(c[count].fd,&read_socks);
	if(c[count].fd > totalfd){
	  totalfd = c[count].fd;
	}
      }
    }
  }//End of main server loop - for(;;)

  /* shutdown reader*/
  sleep(5);
  
  if(pthread_join(msgConsumer,NULL)){
    slog(SLOG_ERROR,"%s: pthread_join(3) failed, thread may need to be killed manually\n",
	 strerror(errno));
  }

  for(count = 0; count < MAXCONNECTIONS; count++){
    close(client->fd);
    client++;
  }

  ptFIFO_destroy(fifo);
  slog(SLOG_INFO,"FIFO max fill: %f\n",space);
  slog(SLOG_INFO,"Messages sent into fifo: %d\n",messageSent);
  slog(SLOG_INFO,"Messages received from fifo: %d\n",messageReceived);
  slog(SLOG_INFO,"Messages logged locally: %d\n",messageLoggedLocally);
  slog(SLOG_INFO,"To be freed by check_free(): %ld\n",checked_memstat());
  return 0;

}/* End of main */








/*
** This will be a seperate thread to consume messages from
** the fifo and send them on to the server.
**
** Connect to server, Read messages out of fifo and send to server.
**
** NOTE: we might have so little code here that it may make sense to get rid
**       of this thread completely! HOWEVER: Having the second thread and the
**       FIFO between the two leaves a nice buffer area that we might want to keep.
*/
static void * consumerThread(void *args){

  int i;                /* Counter variable                                  */
  int status      = 0;  /* Return variable for functions calls               */
  int count;            /* General pupose counter                            */

  char *msg;             /* Pointer to the message                           */
  char  trhostname[100]; /* Buffer to hold our hostname                      */
  char  buffer[1000];    /* Buffer to hold our identifier for the transport  */

  TR_client_configuration   config;   /* Configuration for transport         */
  TR_client_handle          h;        /* Handle to transport                 */

  float  newspace = 0.0;  /* For tracking the max space used   */

  messageReceived = 0; /* Number of messages received from ptFIFO */

  slog(SLOG_INFO,"PID of consumerThread():%d\n",getpid());
  
  /* Setup config for connecting to server */
  if(gethostname(trhostname,100) == 0){
    snprintf(buffer,1000,"%s:%d",trhostname,getpid());
  }
  else{
    snprintf(buffer,1000,"%s:%d","unkownhost",getpid());
    slog(SLOG_WARNING,"Couldn't get hostname. gethostname(2):%s\n",
	 strerror(errno));
  }

  snprintf(trhostname,100,"%s",buffer);
  slog(SLOG_INFO,"Identifying ourself as \"%s\" to server.\n",trhostname);
  config.server_name    = serverHost;
  config.server_port    = serverPort;
  config.queue_length   = QUEUE_SIZE;
  config.client_name    = trhostname;
  config.proxy_state    = TR_PROXY_CAN_NOT_BE_PROXY;
  config.msg_queue_path = localLogFile;

  /* Connect to server */
  h = TR_client_start(&config);
  if (h==NULL) {
    slog(SLOG_FATAL,"can't create transport client"); 
  }

  /* Main loop where we receive messages from ptFIFO and add to the transport queue */
  for(count = 0;;count++){
    /* Check shutdown flag */
    if(shutdown_flag){
      break;
    }

    /* Keep track of max fifo fill */
    if(space < (newspace = ptFIFO_space_used(fifo))){
      space = newspace;
    }

    /* Get data from fifo, with wait timeout at 1 second */
    msg = (char *)ptFIFO_read(fifo,1);
			   
    if(msg != NULL){
      messageReceived++;
      
      /* Write message to server */
      TR_client_send_msg(h,msg);
      free(msg);
    }
    else {
      usleep(100);
      continue;
    }
    
  }/* End of main for(count=0;;count++) loop */
  
  

  /* Should be safe to stop now */
  TR_client_stop(h);

  slog(SLOG_DEBUG,"consumerThread exiting now\n");
  slog(SLOG_INFO,"%ld left - memchecked\n",checked_memstat()); 
  return;
} /* End of consumerThread */




/*
** Error message handler that will log messages
** passed to it using SC's slog function
*/
static void bail(char *msg, int err){
  if(err){
    slog(SLOG_FATAL,"(pid %d) %d: %s (%s)\n",
	 getpid(),err,strerror(err),msg);
  }
  else{
    slog(SLOG_FATAL,"(pid %d) %s\n",getpid(),msg);
  }
} /*End of bail */





/*
** Print out program usage.
*/
static void usage(){

  printf("usage: infoLoggerReader [options]\n\n");
  printf("\t-p <port>\tServer port number ");
  printf("to connect to infoLoggerServer.\n");
  printf("\t-s <host>\tServer hostname that you want to connect to\n");
  printf("\t-u <unix socket>\tUnix Socket to use between client and reader\n");
  printf("\t-d\t\tTurn on debug mode.\n");
  printf("\t-l <logfile>\tSet what file to log personal log messages to.\n");
  printf("\t-o\t\tprint personal log messages to stdout.\n");
  printf("\t-h\t\tthis usage message.\n");
  printf("\nDo not use -l and -o together, ");
  printf("otherwise only the last occurence is used.\n");
  printf("\n\n\tMake sure the following environment variables are set:\n\n");
  printf("\tDATE_SOCKET_INFOLOG_RX - port number to connect to infoLoggerServer\n");
  printf("\tDATE_INFOLOGGER_LOGHOST - default: localhost,  eg pcald10.cern.ch\n\n");
  exit(0);
} /* End of usage */
