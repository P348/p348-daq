/*
**	infoLogger.h
**	============
**
** Definitions for the infoLogger facilities. Should be included in all
** modules using the DATE infoLogger package.
**
** Predefined symbols:
**
**	LOG_FACILITY	The description of the facility issuing log messages
**			(optional)
**
**	PACKAGE_NAME	Used as LOG_FACILITY if this is not defined (should
** 			be automatically defined by the DATE GNUmakefiles)
**
**	__FILE__	Used as LOG_FACILITY if nothing else is defined
** 			(should be automatically defined by the C compiler)
**
** Output symbols:
**
**	LOG_DEFAULT_LOG		The default log file
**	LOG_LOGBOOK		The default logBook file
**	LOG_INFO		Informational message tag
**	LOG_ERROR		Error message tag
**	LOG_FATAL		Fatal message tag
**	LOG_MAX_RECORD_SIZE	Max record size for log messages
**	LOG( )			Log message to the default log file
**	LOG_TO( )		Log message to the given log file
**	LOG_ALL( )		Log message to default+given log files
**
**	INFO( )			Log info message
**	INFO_TO( )
**	INFO_ALL( )
**	ERROR( )		Log error message
**	ERROR_TO( )
**	ERROR_ALL( )
**	FATAL( )		Log fatal message
**	FATAL_TO( )
**	FATAL_ALL( )
**
**	LOG_ERROR_TH		Threshold for error log messages
**	LOG_NORMAL_TH		Threshold for normal log messages
**	LOG_DETAILED_TH		Threshold for detailed log messages
**	LOG_DEBUG_TH		Threshold for debug log messages
**
**	LOGBOOK( )		Log message to the logbook
**	DATE_LOGBOOK_MARKER	Add an End-of-record logbook marker
**
** The strategy behind different types of log messages is the following:
**
**   Severities:
**	Info	Messages concerning normal functioning
**	Error	Errors detected during normal functioning, recover possible
**	Fatal	Fatal condition, execution cannot continue
**
**   Thresholds
**	---		No log messages, execution at full speed, total
**			blackup from all sources
**	Error		Error messages only
**	Normal		Normal functioning mode, execution 100% effective,
**			some messages logged
**	Detailed	Some extra information, execution effective but
**			could be slower then usual
**	Debug		Maximum detailed level, execution could be heavily
**			effected
**
**  03/01/2005  S.C.  Added infoLog_f(), same as infolog but printf like: variable arguments ok, string formatted. No need of intermediate buffer.
*/

#ifndef __infoLogger_h__
#define __infoLogger_h__

#ifndef LOG_FACILITY
# ifdef PACKAGE_NAME
#  define LOG_FACILITY PACKAGE_NAME
# else
#  ifdef __FILE__
#   define LOG_FACILITY __FILE__
#  else
#   define LOG_FACILITY "UNKNOWN"
#  endif
# endif
#endif

#define LOG_DEFAULT_LOG		"runLog"
#define LOG_LOGBOOK    		"logBook"

#ifdef LOG_INFO
#undef LOG_INFO
#endif
#define LOG_INFO  'I'
#define LOG_ERROR 'E'
#define LOG_FATAL 'F'

#define LOG_MAX_RECORD_SIZE 4096

#define LOG( severity, message ) \
                   infoLog(    LOG_FACILITY,       severity, message )
#define LOG_TO( file, severity, message ) \
                   infoLogTo(  LOG_FACILITY, file, severity, message )
#define LOG_ALL( file, severity, message ) \
                   infoLogAll( LOG_FACILITY, file, severity, message )
#define LOG_DEF( severity, message ) \
                   infoLogDef( LOG_FACILITY,       severity, message )

#define INFO( message )            LOG(           LOG_INFO,  message )
#define INFO_TO( file, message )   LOG_TO(  file, LOG_INFO,  message )
#define INFO_ALL( file, message )  LOG_ALL( file, LOG_INFO,  message )
#define INFO_DEF( message )	   LOG_DEF(       LOG_INFO,  message )
#define ERROR( message )           LOG(           LOG_ERROR, message )
#define ERROR_TO( file, message )  LOG_TO(  file, LOG_ERROR, message )
#define ERROR_ALL( file, message ) LOG_ALL( file, LOG_ERROR, message )
#define ERROR_DEF( message )       LOG_DEF(       LOG_ERROR, message )
#define FATAL( message )           LOG(           LOG_FATAL, message )
#define FATAL_TO( file, message )  LOG_TO(  file, LOG_FATAL, message )
#define FATAL_ALL( file, message ) LOG_ALL( file, LOG_FATAL, message )
#define FATAL_DEF( message )       LOG_DEF(       LOG_FATAL, message )

#define LOG_ERROR_TH	1	/* Errors only */
#define LOG_NORMAL_TH	10	/* Normal functioning mode */
#define LOG_DETAILED_TH	20	/* Some extra information */
#define LOG_DEBUG_TH	30	/* Debug, execution heavily effected */

#define LOGBOOK( message ) LOG_TO( LOG_LOGBOOK, LOG_INFO, message )
#define LOGBOOK_EOR        "+++++++++++++++++++++++++++++++++++++++"
#define LOGBOOK_MARKER     LOGBOOK( LOGBOOK_EOR )

#define LOG_SET_DEF( file ) infoSetDef( file ) /* Set default file */
#define LOG_DEF_IS_SET      infoDefIsSet()     /* Inquiry if the default
						* file is already set */

extern void infoLog(    const char * const facility,
			const char         severity,
			const char * const message );
extern void infoLogTo(  const char * const facility,
			const char * const fileName,
			const char         severity,
			const char * const message );
extern void infoLogAll( const char * const facility,
			const char * const fileName,
			const char         severity,
			const char * const message );
extern void infoLogDef( const char * const facility,
			const char         severity,
			const char * const message );
extern void infoSetDef( const char * const fileName );
extern int  infoDefIsSet();
extern void infoClose();
extern void infoOpen(); /* optionnal */
extern void infoLog_f(  const char * const facility,
			const char         severity,
			const char * const message,
                        ... ); /* printf-like formatted log function */
extern void infoLogTo_f(  const char * const stream,
                          const char * const facility,
			  const char         severity,
			  const char * const message,
                        ... ); /* printf-like formatted log function - with stream specification*/
                        
extern void infoSetUserName(  const char * username); /* set username - if NULL, take current user */

#endif
