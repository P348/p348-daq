/*
** statsCollectorDaemon.c
**
** Create a dim service/command that runControl processes
** activate each time a run is stopped/finished. This is 
** then used to trigger the running of the statsCollector.tcl
** script.
**
** runControl processes should execve() statsCollectorTrigger.c
** when a run finishes.
**
** Creator: Simon Lord
**
** gcc -g statsCollectorDaemon.c /date/dim/linux/libdim.a -I/date/dim/dim/ -o statsCollectorDaemon -lpthread
**
** 7/9/2005 - SC  Only DATE_ROOT and DATE_SITE needs to be defined when calling statsCollector. Use of strncat, snprintf. Various cleaning.
** 7/7/2005 - SC  Replaced sleep 5 by callback on execution of command
*/

#define LOG_FACILITY "statsCollectorDaemon"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <string.h>
#include "dis.h"
#include "infoLogger.h"

#define TIMEOUT 30  /* Mins before timing out and running statsCollector.tcl */
#define DELAY   5   /* Delay before launching statsCollector - need to make sure everything is in db */

/* Globals */
int   shutdown_now = 0;     /* set to 1 in signal handler on TERM/QUIT request */
struct timeval  tv1;        /* updated every time statsCollector is run */

char path[PATH_MAX];            /* path to statsCollector*/
char str_date_root[PATH_MAX];   /* DATE_ROOT=... */
char str_date_site[PATH_MAX];   /* DATE_SITE=... */



unsigned int logbook_update_service_id=0;     /* DIM service for logbook updates */
long         logbook_update_time=0;           /* time of last logbook update */

    
/* Simple sighandler for a nice shutdown */
static void sig_handle(int signo){
  switch(signo){
  case SIGQUIT:
  case SIGTERM:
    shutdown_now = 1; break;
  }
}



/* 
** This is ran each time a runControl process activates the
** dim command we setup later on. It executes the StatsCollector.tcl
** script.
*/

void runStatsCollector(long *tag, int *address, int *size){
  char   *myargv[5];
  char   *myenv[3];  
  char    run[21];
  int     pid = 0;
  int    *runNumber;
  int     status=0;

  myargv[0] = path;

  /* run number specified ? */
  if(address != NULL && *size > 0){
    runNumber = address;
    snprintf(run,sizeof(run),"%d",*runNumber);
    myargv[1] = "-r";
    myargv[2] = run;
    myargv[3] = "-t";
    myargv[4] = NULL;
    infoLog_f(LOG_FACILITY,LOG_INFO,"statsCollector will be launched in %ds for run %d",DELAY,*runNumber);
  }
  else {
    myargv[1] = NULL;
    infoLog_f(LOG_FACILITY,LOG_INFO,"statsCollector will be launched in %ds",DELAY);
  }

  /* define env */
  myenv[0]=str_date_root;
  myenv[1]=str_date_site;
  myenv[2]=NULL;

  /* keep time and fork */
  gettimeofday(&tv1,NULL);
  pid = fork();

  if (pid == -1){
    /* fork failed */
    infoLog_f(LOG_FACILITY,LOG_ERROR,"Fork() failed: %s",strerror(errno));

  } else if (pid == 0){
    /* Child */  
    sleep(DELAY);    
    execve(path,myargv,myenv);
    infoLog_f(LOG_FACILITY,LOG_ERROR,"statsCollector.tcl exec failed: %s",strerror(errno));
    exit(1);    

  } else {
    /* Parent */
    wait(&status);
  }
  if (status==256) {
    time_t newtime;
    newtime=time(NULL);
    if (newtime<=logbook_update_time) {
      newtime=logbook_update_time+1;
    }
    logbook_update_time=newtime;
    infoLog_f(LOG_FACILITY,LOG_INFO,"statsCollector success - %d clients updated at %ld", dis_update_service(logbook_update_service_id), newtime );
  } else if (status==0) {
    infoLog_f(LOG_FACILITY,LOG_INFO,"statsCollector success - no new runs");
  } else {
    infoLog_f(LOG_FACILITY,LOG_ERROR,"statsCollector exited with code %d",status);
  }
}



int main(int argc, char **argv){
  int                status         = 0;

  char              *tmp_path       = NULL;

  char              *date_root      = NULL;
  char              *date_site      = NULL;
  char              *date_user      = NULL;

  struct timeval     tv2;
  struct passwd     *date_user_info = NULL;

  int   server_id = 0;


  /* Daemonize ourself :) */
  if(daemon(0,0) == -1){
    infoLog_f(LOG_FACILITY,LOG_FATAL,"Error whilst calling daemon() at line %d of %s: %s",
	    __LINE__,__FILE__,strerror(errno));
    return 1;
  }

  /* Redirect SIGQUIT and SIQTERM for a clean shutdown */
  signal(SIGQUIT, sig_handle);
  signal(SIGTERM, sig_handle);
  
  INFO("statsCollectorDaemon started");


  /* Get environment variables required to run statsCollector.tcl */
  if((date_root = getenv("DATE_ROOT")) == NULL || strlen(date_root) < 1){
    infoLog_f(LOG_FACILITY,LOG_FATAL,"Undefined DATE_ROOT");
    return 1;
  }
  if((date_site = getenv("DATE_SITE")) == NULL || strlen(date_site) < 1){
    infoLog_f(LOG_FACILITY,LOG_FATAL,"Undefined DATE_SITE");
    return 1;
  }
  snprintf(str_date_root,sizeof(str_date_root),"DATE_ROOT=%s",date_root);
  snprintf(str_date_site,sizeof(str_date_site),"DATE_SITE=%s",date_site);


  /* Get directory where we should find statsCollector.tcl from env var */
  tmp_path = getenv("DATE_INFOLOGGER_DIR");
  if((tmp_path == NULL)||(strlen(tmp_path) < 1)){
    infoLog_f(LOG_FACILITY,LOG_FATAL,"Please set $DATE_INFOLOGGER_DIR to the directory where statsCollector.tcl can be found.");
    return 1;
  } else {
    path[0]=0;
    strncat(path, tmp_path, sizeof(path));
    strncat(path, "/statsCollector.sh", sizeof(path));
  }


  /* Attempt to change user to $DATE_USER_ID if it fails carry on anyhow */
  date_user = getenv("DATE_USER_ID");
  if(date_user == NULL || strlen(date_user) < 1){
    infoLog_f(LOG_FACILITY,LOG_FATAL,"Please set DATE_USER_ID");
    return 1;
  }
  date_user_info = getpwnam(date_user);
  if(date_user_info == NULL && errno){
    infoLog_f(LOG_FACILITY,LOG_FATAL,"Error whilst calling getpwnam() at line %d of %s: %s",
	      __LINE__,__FILE__,strerror(errno));
    return 1;
  } 
  status = setuid(date_user_info->pw_uid);
  if(status == -1){
    infoLog_f(LOG_FACILITY,LOG_ERROR,"Cannot change to user %s: %s. Will continue running as current user",
	      date_user,strerror(errno));
    errno = 0;
  }


  /* 
  ** Create our command which the client will access to let us know to run
  ** the statsCollector.tcl script
  */
  server_id = dis_add_cmnd("DEVICE/RunStoppedTrigger/CMD", 0, runStatsCollector, 1);
  logbook_update_service_id = dis_add_service("/DATE/LOGBOOK/UPDATE", "L:1", (int *) &logbook_update_time, sizeof(logbook_update_time), NULL, 0);
  
  if(dis_start_serving("statsCollectorDaemon") != 1){
    infoLog_f(LOG_FACILITY,LOG_FATAL,"dis_start_serving() failed");
    return 1;
  }

  gettimeofday(&tv1,NULL);
  /* Do nothing until we get SIGQUIT or SIGTERM */
  while(shutdown_now == 0){
    /* 
    ** Run the statsCollector.tcl script if it hasn't 
    ** been triggered every TIMEOUT minutes 
    */
    gettimeofday(&tv2,NULL);
    if((tv2.tv_sec - tv1.tv_sec) > (TIMEOUT * 60)){
      infoLog_f(LOG_FACILITY,LOG_INFO,"No statsCollection for the last %d minutes, starting now...",TIMEOUT);
      runStatsCollector(NULL,NULL,NULL);
      gettimeofday(&tv1,NULL);
    }
    sleep(1);
  }


  /* Shutdown */
  dis_remove_service(server_id);
  dis_stop_serving();
  INFO("statsCollectorDaemon stopped");
  
  return 0;
}
