#! /bin/sh
# usage: infoLoggerReader.sh [start|stop|status|restart] [DATE_SITE]
# first argument is command: start|stop|status|restart
# second argument can define/override DATE_SITE

# History:
# 20/01/2005  S.Chapeland     File created

if [ "$2" != "" ]; then
  export DATE_SITE=$2
fi

# Setup DATE and infoLogger environment
if [ "${DATE_INFOLOGGER_DIR}" = "" ]; then
  DATE_INFOLOGGER_DIR=/date/infoLogger
fi
. ${DATE_INFOLOGGER_DIR}/infoLoggerConfig.sh --


# Parameters
COMMAND=$1
NAME=$DATE_INFOLOGGER_BIN/infoLoggerReader

# By default, start the daemon
if [ "$COMMAND" = "" ]; then
  COMMAND="start"
fi


${DATE_COMMON_SCRIPTS}/daemonControl.sh $NAME $COMMAND 
