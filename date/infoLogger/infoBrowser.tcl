#!/usr/bin/wish -f

################################################################
# infoBrowser
#
# This GUI allows to browse the log messages of the DATE system.
#
# requires tcl/tk 8.4 (panedwindow widget)
#
#
# 02/12/2004  SC  - File created
# 27/01/2005  SC  - No need of Tk 8.4 (better but works with 8.3)
# 31/01/2005  SC  - Added more filters
#                 - Mousewheel support
# 01/02/2005  SC  - Online/Flatfile filters
#                 - Version detect improved
# 15/02/2005  SC  - Export messages
#                 - Added filter extension .ibf
# 24/02/2005  SC  - Added archive selection
# 31/03/2005  SC  - Reformating of online messages: \f ->\n
# 07/04/2005  SC  - Added Run Number field
# 11/04/2005  SC  - Corrected display problem of online messages
# 12/04/2005  SC  - Selection of time decimals
# 25/04/2005  SC  - Position in message list kept when changing displayed columns
#                 - Unselect of highlighted message
# 22/06/2005  SC  - Colorize log messages depending on severity
# 11/04/2006  SC  - Decrease width of filter fields,
#                   so that infoBrowser fully seen on 1024x768 screens
#                 - Added "Clean view" button to delete messages in window
################################################################

# check Tcl/Tk version, it would be nice to find 8.4 or higher
set version_ok [split [info tclversion] "."]
if {[llength $version_ok]==2} {
  if {([lindex $version_ok 0]==8) && ([lindex $version_ok 1]>=4)} {
    set version_ok 1
  } else {
    set version_ok 0
  }
} else {
  set version_ok 0
}
if {!$version_ok} {
  #puts "You should use Tcl/Tk 8.4 or higher for better display"
}

# Add mysqltcl path
lappend auto_path $env(DATE_ROOT)/local

# Create busy flag to avoid starting different things at the same time
# only one guy (timeformat, query, online) should edit the message lists at a time
set busy 0

##############################
# init window
##############################
# wm title . "infoBrowser"
wm title . "infoBrowser - DATE_SITE = $env(DATE_SITE)"

font create filterfont -family Arial -size 9 -weight bold
font create timefont -family Arial -size 9 -weight bold

##############################
# create filter selection area
##############################
frame .select
frame .select.filter

label .select.filter.lnone    -text "" -font filterfont
label .select.filter.lonly    -text "match" -font filterfont
label .select.filter.lexclude -text "exclude" -font filterfont

set filters {
  {"Level" "severity"}
  {"Hostname" "hostname"}
  {"Username" "username"}
  {"System" "system"}
  {"Facility" "facility"}
  {"Stream" "dest"}
  {"Run" "run"}
  {"Message" "message"}
}

foreach f $filters {
  set l [lindex $f 0]
  set c [lindex $f 1]
  label .select.filter.l_$c -text $l  -font filterfont
  entry .select.filter.vin_$c -width 9  -font filterfont
  entry .select.filter.vex_$c -width 9  -font filterfont
  lappend filter_c $c
}


frame .select.cmdfilters
label .select.cmdfilters.lcmd -text "Filters"

button .select.cmdfilters.clearfilters -text "Clear" -width 5 -command {
  foreach c $filter_c {
    .select.filter.vin_$c delete 0 end
    .select.filter.vex_$c delete 0 end
  }
  .select.time.vstart delete 0 end
  .select.time.vend delete 0 end
}

button .select.cmdfilters.savefilters -text "Save" -width 5 -command {
  set filename [tk_getSaveFile -initialdir $env(DATE_SITE_CONFIG) -title "Save filters to" -defaultextension ".ibf" -filetypes {{{infoBrowser filters} {.ibf}}}]
  if {$filename != ""} {
    set fd [open "$filename" "w"]
    foreach c $filter_c {
      puts $fd ".select.filter.vin_$c [.select.filter.vin_$c get]"
      puts $fd ".select.filter.vex_$c [.select.filter.vex_$c get]"
    }
    puts $fd ".select.time.vstart [.select.time.vstart get]"
    puts $fd ".select.time.vstart [.select.time.vstart get]"
    close $fd
  }
}

button .select.cmdfilters.loadfilters -text "Load" -width 5 -command {
  set filename [tk_getOpenFile -initialdir $env(DATE_SITE_CONFIG) -title "Load filters from"  -filetypes {{{infoBrowser filters} {.ibf}}}]
  if {$filename != ""} {
    set fd [open "$filename" "r"]
    while {1} {
      gets $fd line
      if {[eof $fd]} {break}
      set l [split $line]
      if {[llength $l] < 2} {continue}
      set w [lindex $l 0]
      set v [join [lrange $l 1 end]]
      if {[string first ".select." $w]!=0} {continue}
      eval "$w delete 0 end"
      if {[string length $v]==0} {continue}
      eval "$w insert 0 \"$v\""
    }
    close $fd    
  }
}

# update the statistics on the number of messages
# actions: reset, display
proc update_stats {action} {
  global n_msgs
  global n_msgs_err
  global n_msgs_fat
  
  set m {}
  if {$action=="reset"} {
    set n_msgs 0
    set n_msgs_err 0
    set n_msgs_fat 0
    
    # no message highlighted yet
    global log_selected
    set log_selected -1
    log_highlight
    
    # clear lists
    global log_fields
    foreach item $log_fields {
      global log_val_$item
      set log_val_$item {}
    }
    
  }
  if {$n_msgs} {
    lappend m "$n_msgs messages"
    if {$n_msgs_err} {lappend m "$n_msgs_err errors"}
    if {$n_msgs_fat} {lappend m "$n_msgs_fat fatals"}
  } else {
    lappend m "$n_msgs message"
  }
  
  .stat_result configure -text [join $m ", "]
  update
}

proc select_archive {} {
  if {[winfo exists .logselect]} {return}

  toplevel .logselect
  button .logselect.select -text "Select" -width 5 -command {
    global lpath
    global logfile
    set logfile [lindex $lpath [.logselect.listbox curselection]]
    destroy .logselect
  }
  frame .logselect.f
  listbox   .logselect.listbox -width 40 -height 10 -xscrollcommand ".logselect.scrollx set" -yscrollcommand ".logselect.scrolly set" -selectmode single -selectbackground yellow -selectforeground black -exportselection no 
  scrollbar .logselect.scrollx -orient horizontal -command ".logselect.listbox xview" -width 10
  scrollbar .logselect.scrolly -orient vertical -command ".logselect.listbox yview" -width 10
    
  global use_db db
  global defaultlogfile

  set nl 0
  if {$use_db} {
    set nl [mysqlsel $db "select count(*) from $defaultlogfile" -list]
  } else {
    catch {scan [exec wc -l $defaultlogfile] "%d" nl}
  }
  .logselect.listbox insert end "Latest logs                  (${nl} messages)"
  global lpath
  set lpath {}
  lappend lpath $defaultlogfile

  if {$use_db} {

    set files [lsort -decreasing [mysqlsel $db "show tables" -list]]
    foreach f $files {
      if {$f == $defaultlogfile} {continue}
      set n [file tail $f]
      set s [split $n "_"]
      if {[llength $s] != 9} {continue}
      set d "[lindex $s 4]/[lindex $s 3]/[lindex $s 2] [lindex $s 6]:[lindex $s 7]:[lindex $s 8]"
      set nl [mysqlsel $db "select count(*) from $f" -list]
      .logselect.listbox insert end "$d     (${nl} messages)"
      lappend lpath $f
    }
  
  } else {  

    global env
    set files [lsort -decreasing [glob -directory $env(DATE_SITE_LOGS) "dateLogs__*" -nocomplain]]   
    foreach f $files {
      set n [file tail $f]
      set s [split $n "_"]
      if {[llength $s] != 9} {continue}
      set d "[lindex $s 4]/[lindex $s 3]/[lindex $s 2] [lindex $s 6]:[lindex $s 7]:[lindex $s 8]"
      set nl 0
      catch {scan [exec wc -l $f] "%d" nl}
      .logselect.listbox insert end "$d     (${nl} messages)"
      lappend lpath $f
    }
    
  }
  
  # select the previously selected item
  global logfile
  set index [lsearch $lpath $logfile]
  if {$index<0} {set index 0}
    
  .logselect.listbox selection set $index
  
  set g [split [wm geometry .] "+"]
  set x [expr [lindex $g 1] +100 ]
  set y [expr [lindex $g 2] +100 ]
  pack .logselect.listbox -side left -in .logselect.f -fill both -expand 1
  pack .logselect.scrolly -side left -in .logselect.f -fill y
  pack .logselect.f -fill x -expand 1
  pack .logselect.scrollx -fill x
  pack .logselect.select

  wm geometry  .logselect +$x+$y
  wm title .logselect "Select log"
}



frame .select.archive
label .select.archive.label -text "Archive"
button .select.archive.select -text "Select" -width 5 -command select_archive
button .select.archive.roll -text "Create" -width 5 -state disabled -command {
  if {[winfo exists .boxmsg]} {return}
  toplevel .boxmsg
  label .boxmsg.l -text "Creating archive ..." -width 30
  set g [split [wm geometry .] "+"]
  set x [expr [lindex $g 1] +100 ]
  set y [expr [lindex $g 2] +100 ]
  wm geometry  .boxmsg +$x+$y
  wm title .boxmsg "Archive"
  pack .boxmsg.l -padx 10 -pady 10
  update
  exec $env(DATE_INFOLOGGER_DIR)/newDateLogs.sh
  .boxmsg.l configure -text "Creating archive ... done!"
  after 1000 {set done 1}
  vwait done
  destroy .boxmsg
}
button .select.archive.clear -text "Delete" -width 5 -state disabled -command {
  if {[winfo exists .boxmsg]} {return}
  toplevel .boxmsg
  label .boxmsg.l -text "Are you sure you want to delete \n latest (unarchived) messages ?" -width 50
  set g [split [wm geometry .] "+"]
  set x [expr [lindex $g 1] +100 ]
  set y [expr [lindex $g 2] +100 ]
  wm geometry  .boxmsg +$x+$y
  wm title .boxmsg "Archive"
  button .boxmsg.yes -text "Yes" -width 5 -command {
    destroy .boxmsg.yes
    destroy .boxmsg.no
    .boxmsg.l configure -text "Deleting messages ..." -width 50
    update
    exec $env(DATE_INFOLOGGER_DIR)/newDateLogs.sh -d
    .boxmsg.l configure -text "Deleting messages ... done!"
    after 1000 {set done 1}
    vwait done
    destroy .boxmsg
  }
  button .boxmsg.no -text "No" -width 5 -command {destroy .boxmsg}
  pack .boxmsg.l -padx 10 -pady 10
  pack .boxmsg.yes .boxmsg.no -side left -fill x -expand 1 -padx 10 -pady 10
}


frame .select.time
label .select.time.l -text "Time" -font timefont
label .select.time.lnone -text "" -font timefont
label .select.time.lstart -text "min." -font timefont
entry .select.time.vstart -width 8 -font timefont
label .select.time.lend -text "max." -font timefont
entry .select.time.vend -width 8 -font timefont


##############################
# query window
##############################
frame .cmd
button .cmd.query -text "Query" -width 10 -height 3 -command {doQuery}
button .cmd.clean -text "Clean view" -width 10 -command {doClean}

checkbutton .cmd.online -text "Online" -width 10 -command {doQuery} -anchor w
.cmd.online configure -command {doOnline}
checkbutton .cmd.autoclean -text "Autoclean" -anchor w

frame .stat_action -background darkgrey
frame .stat_query  -background darkgrey

label .stat_action.l -text "Status : " -background darkgrey
label .stat_action.v -text "Idle" -wraplength 800 -justify left -background darkgrey -fg black
label .stat_query.l -text "Query  : "  -background darkgrey
label .stat_query.v -text "" -wraplength 800 -justify left  -background darkgrey
label .stat_result -text "0 message"


# save messages
button .cmd.savemsg -text "Export" -width 10 -command {
  set filename [tk_getSaveFile -initialdir $env(DATE_SITE_LOGS) -title "Save log messages to"]
  if {$filename != ""} {
    set fd [open "$filename" "w"]

    # define print command
    global log_visible_fields
    global n_msgs

    set insert_cmd {}
    lappend insert_cmd "puts \$fd \""
    set i 0
    foreach item $log_visible_fields {
      if {$i!=0} {
        lappend insert_cmd "\\t"
      }
      lappend insert_cmd "\[lindex \$log_val_$item \$index\]"
      global log_val_$item
      incr i
    }
    lappend insert_cmd "\""
    set insert_cmd [join $insert_cmd ""]
   
    for {set index 0} {$index<$n_msgs} {incr index} {
      eval $insert_cmd
    }
    
    close $fd
  }
}



##############################
# message display
##############################
frame .messages -borderwidth 2 -height 300

# display message with configured log fields
set log_fields {Level Date Time Subsecond Host Pid Username System Facility Stream Run Message}
set log_fields_def_size {7 7 8 15 12 6 8 6 9 9 6 30}
set log_visible_fields {Level Time Host Facility Run Message}

# One global for each field contains list of items - create empty
foreach item $log_fields {
  set log_val_$item {}
}


# this global holds the current selected message
set log_selected -1

# highlight selected log, defined in global variable log_selected
set n_msgs 0
set n_msgs_err 0
set n_msgs_fat 0
proc log_highlight {} {
  global log_selected
  global log_visible_fields
  global n_msgs  

  if {($log_selected>=0) && ($log_selected<$n_msgs)} {
    clipboard clear
    set i 0
    foreach item $log_visible_fields {
      .messages.pw.frame_$item.listbox selection clear 0 end
      .messages.pw.frame_$item.listbox selection set $log_selected 
      .messages.pw.frame_$item.listbox see $log_selected
      #clipboard append "[.messages.pw.frame_$item.listbox selection get $log_selected] "
      set v [.messages.pw.frame_$item.listbox get $log_selected]
      if {$i} { set v "\t${v}"}
      clipboard append $v
      incr i
    }
  } else {
    foreach item $log_visible_fields {
      .messages.pw.frame_$item.listbox selection clear 0 end
    }
  }
}

# selection of a field sets the global log_selected value and update display
proc on_log_select {w} {
  global log_selected
  set i [lindex [$w curselection] 0]
  if {[string length $i]>0} {
    if {$i==$log_selected} {
      set log_selected -1
    } else {
      set log_selected $i
    }
    log_highlight
  }
}



# update log level colors
# takes fractionnal range of items to update (between 0.0 and 1.0)
set severity_visible 0
set severity_colorize 1

proc colorize_severity {start_f end_f} {
  global severity_visible
  global severity_colorize

  if {!$severity_visible} {return}
  if {!$severity_colorize} {return}

  global n_msgs
  
  if {($start_f<0)||($end_f<0)} {
    #set r [.messages.sb get]
    set r [.messages.pw.frame_Level.listbox yview]
    if {[llength $r]!=2} {return}
    set start_f [lindex $r 0]
    set end_f [lindex $r 1]
  }
  set min [expr int(floor($n_msgs*$start_f))]
  set max [expr int(ceil($n_msgs*$end_f))]
  
  global log_val_Level
  
  # update listbox attributes
  set i $min
  foreach s [lrange $log_val_Level $min $max] {
    set color gray
    switch $s {
      "ERROR" {set color "#FFFF00"}
      "FATAL" {set color red}
    }
    .messages.pw.frame_Level.listbox itemconfigure $i -background $color

# To colorize all the line:
#    global log_visible_fields
#    foreach item $log_visible_fields {
#      .messages.pw.frame_$item.listbox itemconfigure $i -background $color
#    }

    incr i
  }
}


# create the pannel with messages
# each field is displayed in a different resizable column
proc show_messages {} {
  global log_fields
  global log_visible_fields
  global log_fields_def_size
    
  if {[winfo exists .messages.pw]} {destroy .messages.pw}
  
  global version_ok
  if {!$version_ok} {
    frame .messages.pw
  } else {
    panedwindow .messages.pw  -orient horizontal -sashwidth 5 -showhandle false -sashpad 0
  }

  global severity_visible
  set severity_visible 0

  # fields to be displayed
  foreach item $log_visible_fields {
    frame .messages.pw.frame_$item 
    label .messages.pw.frame_$item.label -text "$item"
    listbox .messages.pw.frame_$item.listbox -listvariable log_val_$item -yscrollcommand ".messages.sb set" -xscrollcommand ".messages.pw.frame_$item.scroll set"  -selectmode single -selectbackground yellow -selectforeground black -exportselection no 
    scrollbar .messages.pw.frame_$item.scroll -orient horizontal -command ".messages.pw.frame_$item.listbox xview" -width 10

    set pwidth "[expr 8*[lindex $log_fields_def_size [lsearch $log_fields $item]]]"
    if {$version_ok} {
      .messages.pw add .messages.pw.frame_$item -width "${pwidth}p"
    } else {
      if {$item=="Message"} {
        pack .messages.pw.frame_$item -side left -fill both -expand 1
      } else {
        .messages.pw.frame_$item.listbox configure -width [expr $pwidth/8]
        pack .messages.pw.frame_$item -side left -fill y -expand 0
      }
    }
    
    if {$item=="Level"} {
      .messages.pw.frame_$item.listbox configure -yscrollcommand "_listbox_severity_update"
      set severity_visible 1
    }
    
    pack .messages.pw.frame_$item.label
    pack .messages.pw.frame_$item.listbox -fill both -expand 1
    pack .messages.pw.frame_$item.scroll -fill x
    bind .messages.pw.frame_$item.listbox <<ListboxSelect>> {on_log_select %W}
    pack .messages.pw -fill both -side left -expand 1
  }

  # highlight selected message if any
  log_highlight
}


# mousewheel bindings
bind all <Button-4> {_msgs_scroll scroll -1 page}
bind all <Button-5> {_msgs_scroll scroll +1 page}

# update in the severity area
proc _listbox_severity_update {a b} {
  # match scrollbar
  .messages.sb set $a $b
  # colorize items
  colorize_severity $a $b
}


# scrollbar and associated procedure
scrollbar .messages.sb -orient vertical -command {_msgs_scroll}
proc _msgs_scroll {args} {
  global log_fields
  global log_visible_fields

  switch [lindex $args 0] {
    scroll {
      if {[llength $args]!=3} return
      
      # realign all items (because of default mousewheel binding)
      # take the median value of the yview 
      foreach item $log_visible_fields {
        lappend a [lindex [.messages.pw.frame_$item.listbox yview] 0]
      }
      set a [lsort $a]
      set a [lindex $a [expr int([llength $a]/2)]]
      foreach item $log_visible_fields {
        .messages.pw.frame_$item.listbox yview moveto $a
      }
              
      foreach item $log_visible_fields {
        .messages.pw.frame_$item.listbox yview scroll [lindex $args 1] [lindex $args 2] 
      }
    }
    moveto {
      if {[llength $args]!=2} return
      foreach item $log_visible_fields {
        .messages.pw.frame_$item.listbox yview moveto [lindex $args 1]
      }
    }
  }
}
show_messages

# options
frame .msg_options

# number of decimals for timestamp
label .msg_options.subsec_l -text " decimals"
set subsecond_decimal 0
set subsecond_decimal_l 0
entry .msg_options.subsec_v -width 1 -textvariable subsecond_decimal -validate all -vcmd {
  if {"%S"!="{}"} {
    if {("%S"<0)|| "%S">6} {
      after idle {set subsecond_decimal 0}
    } else {
      after idle {set subsecond_decimal "%S"}
    }
  }
  if {$subsecond_decimal_l!=$subsecond_decimal} {after idle {update_time}}
  set subsecond_decimal_l $subsecond_decimal
  return 1
}
#if {$version_ok} {
#  spinbox .msg_options.subsec_v -from 0 -to 6 -increment 1 -textvariable subsecond_decimal -width 2 -state readonly
#}

# select column to be displayed
foreach item $log_fields {
  checkbutton .msg_options.check_$item -text $item
  if {[lsearch $log_visible_fields $item]>=0} {.msg_options.check_$item select}
  .msg_options.check_$item configure -command {update_visible_fields}
  if {$item=="Subsecond"} {continue}
  pack .msg_options.check_$item -side left
  if {$item=="Time"} {
    pack .msg_options.subsec_v .msg_options.subsec_l  -side left
  }
}


pack .msg_options
proc update_visible_fields {} {
  global log_fields
  global log_visible_fields
  set log_visible_fields {}

  scan [.messages.sb get] "%f %f" pos_saved_begin pos_saved_end
  foreach item $log_fields {
    global check_$item
    set exp "set v \$check_$item"
    eval $exp
    if {$v} {lappend log_visible_fields $item} 
  }
  show_messages  
  _msgs_scroll moveto $pos_saved_begin
}


###############################
# layout
###############################
pack .messages -fill both -expand 1
pack .messages.sb -side right -fill y

pack .select -ipady 10

pack .select.archive -side left -padx 5
pack .select.archive.label .select.archive.select .select.archive.roll .select.archive.clear -side top -in .select.archive

pack .select.cmdfilters -side left -padx 5
pack .select.cmdfilters.lcmd .select.cmdfilters.clearfilters .select.cmdfilters.savefilters .select.cmdfilters.loadfilters -side top -in .select.cmdfilters

pack .select.time -side left -padx 10
pack .select.filter -side left


# filters 
set l "grid .select.filter.lnone"
foreach c $filter_c {
  lappend l ".select.filter.l_$c"
}
lappend l "-in .select.filter"
eval "[join $l]"

set l "grid .select.filter.lonly"
foreach c $filter_c {
  lappend l ".select.filter.vin_$c"
}
lappend l "-in .select.filter"
eval "[join $l]"

set l "grid .select.filter.lexclude"
foreach c $filter_c {
  lappend l ".select.filter.vex_$c"
}
lappend l "-in .select.filter"
eval "[join $l]"

# time filter
grid .select.time.lnone .select.time.l
grid .select.time.lstart .select.time.vstart
grid .select.time.lend .select.time.vend
#pack .select.time.lstart .select.time.vstart .select.time.lend .select.time.vend

pack .stat_action -fill x
pack .stat_query -fill x 
pack .stat_result -fill x -side left

button .stat_searchb -text "Find" -command {
  global log_selected
  global n_msgs
  global log_val_Message
  
  set sval [.stat_searchv get]
  if {[string length $sval>0]} {
    if {($log_selected>=0) && ($log_selected<$n_msgs)} {
      set j0 [ expr $log_selected + 1 ]
    } else {
      set j0 0
    }
    for {set i 0}  {$i < $n_msgs}  {incr i} {
      set j [expr ($i + $j0 ) % $n_msgs]
    
      if {[string first $sval [lindex $log_val_Message $j]]!=-1} {
        set log_selected $j
        log_highlight
        return
      }
    }
  }
  set log_selected -1
  log_highlight
  puts -nonewline "\a"
  flush stdout
}
entry .stat_searchv -width 15
pack .stat_searchb .stat_searchv -side right


pack .stat_action.l .stat_action.v -in .stat_action -side left
pack .stat_query.l .stat_query.v -in .stat_query -side left


pack .cmd.query .cmd.savemsg .cmd.clean
pack .cmd.online .cmd.autoclean -expand true -fill x
pack .cmd -side right -padx 20 -in .select





##############################
# Database query
##############################


# Command to create tables (check in newDateLogs.sh):
# definition of message table
# TABLEDESCR='severity char(1), timestamp double(16,6), hostname varchar(20), pid int, username varchar(20), system varchar(20), facility varchar(20), dest varchar(20), run int, message text \
# ,index ix_severity(severity), index ix_timestamp(timestamp), index ix_hostname(hostname), index ix_facility(facility), index ix_dest(dest), index ix_run(run), index ix_system(system)'


# connect database

# Do we use a database ?
set use_db 0
catch {
  if {$env(DATE_INFOLOGGER_MYSQL)=="TRUE"} {
    set use_db 1
  }
}

# update window title
#if {$use_db} {
#  wm title . "infoBrowser - using database"
#} else {
#  wm title . "infoBrowser - using log file"
#}


if {$use_db} {

  # define connection parameters
  set db_user $env(DATE_INFOLOGGER_MYSQL_USER)
  set db_pwd $env(DATE_INFOLOGGER_MYSQL_PWD)
  set db_host $env(DATE_INFOLOGGER_MYSQL_HOST)
  set db_db $env(DATE_INFOLOGGER_MYSQL_DB)

  if {[string length $db_user]==0} {
    puts "Undefined DATE_INFOLOGGER_MYSQL_USER"
    exit -1
  }
  if {[string length $db_host]==0} {
    puts "Undefined DATE_INFOLOGGER_MYSQL_HOST"
    exit -1
  }
  if {[string length $db_pwd]==0} {
    puts "Undefined DATE_INFOLOGGER_MYSQL_PWD"
    exit -1
  }
  if {[string length $db_db]==0} {
    puts "Undefined DATE_INFOLOGGER_MYSQL_DB"
    exit -1
  }

  #puts -nonewline "Connecting infologger database :$db_user@$db_host:$db_db ... "
  flush stdout
  if [ catch {package require mysqltcl} ] {
    puts "Error - package mysqltcl required"
    exit 1
  }

  if [ catch {set db [mysqlconnect -host $db_host -user $db_user -password $db_pwd -db $db_db]} ] {
    puts "Error - failed to connect to database"
    exit 1
  }
  #puts " ok"

  # this is the table for current log messages
  set defaultlogfile "messages"

} else {

  # this is the file for current log messages
  set defaultlogfile "$env(DATE_SITE_LOGS)/dateLogs"
  puts "Using data from $env(DATE_SITE_LOGS)"

}

# set log to default (no archive selected)
set logfile $defaultlogfile





#########################################################
# Filter handling
#########################################################

# enable filters
proc filter_on {} {
  global filter_c
  .select.time.l configure -state normal
  .select.time.lstart configure -state normal
  .select.time.vstart configure -state normal
  .select.time.lend configure -state normal
  .select.time.vend configure -state normal

  foreach c $filter_c {
    .select.filter.vin_$c configure -state normal
    .select.filter.vex_$c configure -state normal    
  }

  .select.cmdfilters.clearfilters configure -state normal
  .select.cmdfilters.savefilters configure -state normal  
  .select.cmdfilters.loadfilters configure -state normal
}


# disable filters
proc filter_off {} {
  global filter_c
  .select.time.l configure -state disabled
  .select.time.lstart configure -state disabled
  .select.time.vstart configure -state disabled
  .select.time.lend configure -state disabled
  .select.time.vend configure -state disabled

  # correct state is readonly, but only available in 8.4
  global version_ok
  if {$version_ok} {
    set newstate readonly
  } else {
    set newstate disabled
  }

  foreach c $filter_c {
    .select.filter.vin_$c configure -state $newstate
    .select.filter.vex_$c configure -state $newstate
  }
  
  .select.cmdfilters.clearfilters configure -state disabled
  .select.cmdfilters.savefilters configure -state disabled  
  .select.cmdfilters.loadfilters configure -state disabled
}





#########################################################
# Query processing
#########################################################

# query messages corresponding to the defined filter settings
set querying 0
proc doQuery {} {
  global db
  global querying
  global use_db
  global filter_c


  global busy
  if {$busy} {return}
  set busy 1

    
  # disable query while this one is not processed
  if {$querying} {set busy 0; return}
  set querying 1
  
  # reset query
  set msgs {}
  set bad_query 0
  
  # create query based on filters
  #  set query "SELECT FROM_UNIXTIME(timestamp),timestamp-floor(timestamp),message from messages"
  #  set query "SELECT Severity,from_unixtime(Timestamp),Hostname,Pid,Username,Facility,Dest,Message from messages"

  # use good table (can be an archive)
  global logfile
  set query "SELECT * from $logfile"

  # build SQL filter command    
  set sql_filters {}
  
  foreach field $filter_c {
    set boxval [.select.filter.vin_${field} get]
    if {$boxval != ""} {    
      set filtered_items [eval "list $boxval"]
      set count 0
      set subfilter {}
      foreach i $filtered_items {
          if {[string length $i]<=0} {continue}
          if {$count > 0} {
            lappend subfilter "OR"
          } else {
            lappend subfilter "("
          }
          if {$use_db} {
            set i [mysql::escape $i]
          }
          if {[string first "%" $i]!=-1} {
            lappend subfilter "$field LIKE \"$i\""
          } else {
            lappend subfilter "$field=\"$i\""
          }
          incr count
      }
      lappend subfilter ")"
      lappend sql_filters [join $subfilter]
    }

    set boxval [.select.filter.vex_${field} get]
    if {$boxval != ""} {    
      set filtered_items [eval "list $boxval"]
      set count 0
      set subfilter {}
      foreach i $filtered_items {
          if {[string length $i]<=0} {continue}
          if {$count > 0} {
            lappend subfilter "OR"
          } else {
            lappend subfilter "NOT ("
          }
          if {$use_db} {
            set i [mysql::escape $i]
          }
          if {[string first "%" $i]!=-1} {
            lappend subfilter "$field LIKE \"$i\""
          } else {
            lappend subfilter "$field=\"$i\""
          }
          incr count
      }
      lappend subfilter ")"
      lappend sql_filters [join $subfilter]
    }
  }
  
  # start time
  .select.time.vstart configure -bg [.select.time cget -bg]
  set filter_tmin 0
  if {[.select.time.vstart get] != ""} {
    if {([scan [.select.time.vstart get] "%lf" t]==1)&&($t>1000000000)} {
      lappend sql_filters "timestamp>$t"
      set filter_tmin $t
    } elseif {![catch {set t [clock scan [.select.time.vstart get]]}]} {
      lappend sql_filters "timestamp>$t"
      set filter_tmin $t
    } else {
      .select.time.vstart configure -background red
      set bad_query 1
    }
  }
  
  # end time
  set filter_tmax 0
  .select.time.vend configure -bg [.select.time cget -bg]
  if {[.select.time.vend get] != ""} {
    if {([scan [.select.time.vend get] "%lf" t]==1)&&($t>1000000000)} {
      lappend sql_filters "timestamp<$t"
      set filter_tmax $t
    } elseif {![catch {set t [clock scan [.select.time.vend get]]}]} {
      lappend sql_filters "timestamp<$t"
      set filter_tmax $t
    } else {
      .select.time.vend configure -background red
      set bad_query 1
    }
  }

  # any error?
  if {$bad_query} {
    .stat_query.v configure -text ""
    .stat_action.v configure -text "Bad query" -fg darkred
    update_stats reset
    set querying 0
    set busy 0
    return
  }

  # build the query string based on processed input  
  set i 0
  foreach filter $sql_filters {
    if {$i == 0} {
      lappend query "WHERE"
    } else {
      lappend query "AND"
    }
    lappend query "$filter"
    incr i
  }

  # order result by timestamp
  lappend query "ORDER BY timestamp"
  
  # build query
  set query [join $query]

  if {!$use_db} {
    global logfile
    set query "Records loaded from file $logfile"

    # check log file exists
    if {![file exists $logfile]} {
      .stat_query.v configure -text ""
      .stat_action.v configure -text "File $logfile not found" -fg darkred
      update_stats reset
      set querying 0
      set busy 0
      return
    }
  }

  # update status display
  .stat_query.v configure -text "$query"
  .stat_action.v configure -text "Retrieving data..." -fg black
  .stat_result configure -text ""
  update
  
  if {$use_db} {
    # launch query
    set msgs [mysqlsel $db $query -list]

    # free resources if any
    mysqlendquery $db
  } else {

    create_online_filter
    
    # read from file
    global logfile
    set fd [open $logfile "r"]
    while {1} {
      gets $fd line
      if {[eof $fd]} {break}

      set item [split $line "\t"]
    
      set t [lindex $item 0]
      if { (($filter_tmin)&&($t<$filter_tmin)) || (($filter_tmax)&&($t>$filter_tmax))} {continue}      
    
      set v_severity [lindex $item 1]
      set v_hostname [lindex $item 2]
      set v_pid [lindex $item 3]
      set v_username [lindex $item 4]
      set v_system [lindex $item 5]           
      set v_facility [lindex $item 6]
      set v_dest [lindex $item 7]           
      set v_run [lindex $item 8]
      set v_message [join [lrange $item 9 end]]

      if {[online_filter]==1} {continue}
    
      lappend msgs $item
    }
    close $fd
  }

  # number of messages
  update_stats reset
  global n_msgs
  global n_msgs_err
  global n_msgs_fat
  set n_msgs [llength $msgs]

  # update status display  
  .stat_action.v configure -text "Processing data ..." -fg black

  # update stats on the number of messages
  update_stats display

  # some counters for status display
  set i 0
  set l [llength $msgs]
  set step [expr int($l/10)]
  if {$step==0} {set step 10}

  # variables for fast time formatting
  set lastt_val -1
  set lastt_str_d ""
  set lastt_str_t ""
  global subsecond_decimal
  set subsecond_format "%0[expr ${subsecond_decimal}+3].${subsecond_decimal}f"

  # clear lists
  global log_fields
  foreach item $log_fields {
    set l_$item {}
  }

  # process result msg by msg
  foreach item $msgs {

    # severity
    if {$use_db} {
      set s [lindex $item 0]
    } else {
      set s [lindex $item 1]
    }
    switch $s {
    "I" {set ss "Info"}
    "E" {set ss "ERROR"; incr n_msgs_err}
    "D" {set ss "Debug"}
    "F" {set ss "FATAL"; incr n_msgs_fat}
    default {set ss "? $s"}
    }

    # date/time : format string only if not in same second as previous      
    if {$use_db} {
      set tmicro [lindex $item 1]
    } else {
      set tmicro [lindex $item 0]
    }
    set tval [expr int($tmicro)]
    if {$tval!=$lastt_val} {
      set the_clock_format [split [clock format $tval -format "%d/%m/%y %H:%M:%S %H:%M"]]
      set lastt_str_d [lindex $the_clock_format 0]
      set lastt_str_t [lindex $the_clock_format 1]
      set lastt_str_hm [lindex $the_clock_format 2]
      # expr does not like 1-digit numbers with a zero before: e.g. "09" is an invalid octal number
      set lastt_str_s [expr $tval % 60]
      set lastt_val $tval
    }

    lappend l_Level "$ss"
    lappend l_Date $lastt_str_d
    if {$subsecond_decimal} {
      lappend l_Time [format "${lastt_str_hm}:${subsecond_format}" [expr $tmicro - floor($tmicro) + $lastt_str_s]]
    } else {
      lappend l_Time $lastt_str_t
    }
    lappend l_Subsecond $tmicro

    # other fields
    lappend l_Host [lindex $item 2]
    lappend l_Pid [lindex $item 3]
    lappend l_Username [lindex $item 4]
    lappend l_System [lindex $item 5]
    lappend l_Facility [lindex $item 6]
    lappend l_Stream [lindex $item 7]
    lappend l_Run [lindex $item 8]
    lappend l_Message [lindex $item 9]


    # update status display
    incr i
    if {[expr $i % $step]==0} {
      .stat_action.v configure -text "Processing data ... [expr round($i*100.0/$l)]%" -fg black
      update
    }
  }


  # update status display
  .stat_action.v configure -text "Processing data ... 100%" -fg black

  # update stats on the number of messages
  update_stats display
     
  # assign values
  global log_fields
  foreach item $log_fields {
    global log_val_$item
    set a "set log_val_$item \$l_$item"
    eval $a
  }

  # update status display
  .stat_action.v configure -text "Idle" -fg black
  update

  # scroll to last entry
  _msgs_scroll moveto $n_msgs

  # colorize log levels items
  colorize_severity -1 -1
  
  # re-enable queries
  set querying 0
  
  set busy 0
}




############################################################
# create "online_filter" proc
# this new proc returns 1 if message should be discarded
############################################################
proc create_online_filter {} {

  global filter_c  
  
  set online_filter {}
  lappend online_filter "proc online_filter {} {"

  # match filter
  foreach field $filter_c {
    set boxval [.select.filter.vin_${field} get]
    if {$boxval != ""} {
      lappend online_filter "set reject 1" 
      lappend online_filter "upvar v_$field v_$field"
      # add dummy loop to break if match (OR)
      lappend online_filter "for {set k 0} {\$k==0} {incr k} {"

      set filtered_items [eval "list $boxval"]
      set count 0
      foreach i $filtered_items {
          if {[string length $i]<=0} {continue}
          if {[string first "%" $i]!=-1} {
            set pattern [string map {% *} $i]
            lappend online_filter "if {\[string match \"$pattern\" \$v_$field \]} {set reject 0; break;}"
            incr count
          } else {
            lappend online_filter "if {\$v_$field==\"$i\"} {set reject 0; break;}"
            incr count
          }          
      }

      lappend online_filter "}"
      if {$count} {
        lappend online_filter "if {\$reject==1} {return 1}"
      }
    }
  }

  # exclude filter
  foreach field $filter_c {
    set boxval [.select.filter.vex_${field} get]
    if {$boxval != ""} {
      lappend online_filter "set reject 0" 
      lappend online_filter "upvar v_$field v_$field"
      # add dummy loop to break if match (OR)
      lappend online_filter "for {set k 0} {\$k==0} {incr k} {"

      set filtered_items [eval "list $boxval"]
      set count 0
      foreach i $filtered_items {
          if {[string length $i]<=0} {continue}
          if {[string first "%" $i]!=-1} {
            set pattern [string map {% *} $i]
            lappend online_filter "if {\[string match \"$pattern\" \$v_$field \]} {set reject 1; break;}"
            incr count
          } else {
            lappend online_filter "if {\$v_$field==\"$i\"} {set reject 1; break;}"
            incr count
          }          
      }

      lappend online_filter "}"
      if {$count} {
        lappend online_filter "if {\$reject==1} {return 1}"
      }
    }
  }


  lappend online_filter "return 0"
  lappend online_filter "}"
  set online_filter [join $online_filter "\n"]
  eval $online_filter
}



#################################
# display online data
#################################
set server_fd -1
set last_run 0

# process an event on the socket
proc server_event {} {
  global server_fd
  global n_msgs
  global n_msgs_err
  global n_msgs_fat  

  fileevent $server_fd readable ""
  set n_loop 0
  set n_msgs_start $n_msgs
    
  # init empty fields
  set l_Level {}
  set l_Date {}
  set l_Time {}
  set l_Subsecond {}
  set l_Host {}
  set l_Pid {}
  set l_Username {}
  set l_System {}
  set l_Facility {}
  set l_Stream {}
  set l_Run {}
  set l_Message {}

    
  while {1} {
    
    # connection closed?
    if {[eof $server_fd]} {
      close $server_fd
      set server_fd -1
      .stat_action.v configure -text "Connection closed" -fg darkred
      update
      break
    }

    # read and decode message
    if {[gets $server_fd msg]==-1} {break}   
    set item [lrange [split $msg "#"] 1 end]
    if {[llength $item]<8} {return}

    set v_severity [lindex $item 0]
    set v_hostname [lindex $item 2]
    set v_pid [lindex $item 3]
    set v_username [lindex $item 4]
    set v_system [lindex $item 5]
    set v_facility [lindex $item 6]
    set v_dest [lindex $item 7]
    set v_run [lindex $item 8]
    set v_message [join [lrange $item 9 end] "#"] 

    global last_run autoclean
    if {($v_run != "") && ($v_run > $last_run)} {
        set last_run $v_run
        if {$autoclean} {
            doClean
        }
    }

    if {[online_filter]==1} {continue}
   
    # severity
    switch $v_severity {
    "I" {set ss "Info"}
    "E" {set ss "ERROR"; incr n_msgs_err}
    "D" {set ss "Debug"}
    "F" {set ss "FATAL"; incr n_msgs_fat}
    default {set ss "? $v_severity"}
    }
    
    # run number = -1 means undefined
    if {$v_run==-1} { set v_run "" }

    # date/time : could format string only if not in same second as previous -> TODO
    set tmicro [lindex $item 1]
    set tval [expr int($tmicro)]
  
    global subsecond_decimal
    if {$subsecond_decimal} {
      set subsecond_format "%0[expr ${subsecond_decimal}+3].${subsecond_decimal}f"
      set the_clock_format [split [clock format $tval -format "%d/%m/%y %H:%M:%S %H:%M"]]
      set lastt_str_d [lindex $the_clock_format 0]
      set lastt_str_t [lindex $the_clock_format 1]
      set lastt_str_hm [lindex $the_clock_format 2]
      # expr does not like 1-digit numbers with a zero before: e.g. "09" is an invalid octal number
      set lastt_str_s [expr $tval % 60]     
      set lastt_str_t [format "${lastt_str_hm}:${subsecond_format}" [expr $tmicro - floor($tmicro) + $lastt_str_s]]
    } else {
      set lastt_str_d [clock format $tval -format "%d/%m/%y"]
      set lastt_str_t [clock format $tval -format "%H:%M:%S"]
    }

    # re-format multiple line messages
    foreach m [split $v_message "\f"] {

    lappend l_Level "$ss"
    lappend l_Date $lastt_str_d
    lappend l_Time $lastt_str_t
    lappend l_Subsecond $tmicro

    # other fields
    lappend l_Host $v_hostname
    lappend l_Pid $v_pid
    lappend l_Username $v_username
    lappend l_System $v_system
    lappend l_Facility $v_facility
    lappend l_Stream $v_dest
    lappend l_Run $v_run
    lappend l_Message $m

    incr n_msgs

    }

    incr n_loop
    if {$n_loop==1000} {break}
  }    

  if {$n_loop} {
    # assign values
    global log_fields
    foreach item $log_fields {
      global log_val_$item
      set a "set log_val_$item \[concat \$log_val_$item \$l_$item\]"
      eval $a
    }

    # scroll    
    global log_visible_fields
    foreach item $log_visible_fields {
      .messages.pw.frame_$item.listbox see end
    }
  }
  
  # update stats on the number of messages
  update_stats display

  
  if {$server_fd!=-1} {  
    fileevent $server_fd readable server_event
  }
}


# open socket to data server
proc server_connect {} {
  global server_fd
  global env
    
  # connect only if not connected yet
  if {$server_fd!=-1} {return}

  set loghost "localhost"
  ##set loghost "pccodb01"
  catch {
    set loghost $env(DATE_INFOLOGGER_LOGHOST)
  }

  set logport -1
  catch {
    set logport $env(DATE_SOCKET_INFOLOG_TX)
  }


  .stat_action.v configure -text "Connecting $loghost ..." -fg black
  update

  # open socket
  if {[catch {set server_fd [socket $loghost $logport]} err]} {
    .stat_action.v configure -text "while connecting $loghost:$logport - $err" -fg darkred
    update
    return
  }
  fconfigure $server_fd -blocking false   
  fconfigure $server_fd -buffersize 1000000
  fileevent $server_fd readable server_event
  
  
  .stat_action.v configure -text "Connected" -fg black
  
  update
}


proc doOnline {} {
  global online
  global server_fd

  global busy
    
  if {$online} {

    if {$busy} {return}
    set busy 1

    # Go Online

    # create filter
    filter_off
    create_online_filter

    # update status display
    set t0 [clock format [clock seconds] -format "%d/%m/%Y %H:%M:%S"]
    .stat_query.v configure -text "Online data - from $t0"
    .cmd.query configure -state disabled

    # connect server
    server_connect
    
    update_stats reset
    update   
    
  } else {
    # close server
    if {$server_fd!=-1} {
      close $server_fd
      set server_fd -1
    }

    # Go Offline
    filter_on
    .cmd.query configure -state normal
    .stat_action.v configure -text "Idle" -fg black
    set t0 [clock format [clock seconds] -format "%d/%m/%Y %H:%M:%S"]
    .stat_query.v configure -text "[.stat_query.v cget -text] to $t0"
    
    update
    
    set busy 0
  }
}

proc update_time {} {
  global busy
  if {$busy} {return}
  set busy 1

  global subsecond_decimal
 
  # variables for fast time formatting
  set lastt_val -1
  global subsecond_decimal
  set subsecond_format "%0[expr ${subsecond_decimal}+3].${subsecond_decimal}f"
  
  # update time column
  set new {}
  global log_val_Subsecond

  # some counters for status display
  set i 0
  set l [llength $log_val_Subsecond]
  set step [expr int($l/10)]
  if {$step==0} {set step 10}


  if {$subsecond_decimal} {
    foreach tmicro $log_val_Subsecond {
      set tval [expr int($tmicro)]
      if {$tval!=$lastt_val} {
        set lastt_str_hm [clock format $tval -format "%H:%M"]
        set lastt_str_s  [expr $tval % 60]
      }
      lappend new [format "${lastt_str_hm}:${subsecond_format}" [expr $tmicro - floor($tmicro) + $lastt_str_s]]
      
      # update status display
      incr i
      if {[expr $i % $step]==0} {
        .stat_action.v configure -text "Processing data ... [expr round($i*100.0/$l)]%" -fg black
        update
      }
    }
  } else {
    foreach tmicro $log_val_Subsecond {
      set tval [expr int($tmicro)]
      if {$tval!=$lastt_val} {
        set lastt_str_t [clock format $tval -format "%H:%M:%S"]
      }
      lappend new $lastt_str_t
      # update status display
      incr i
      if {[expr $i % $step]==0} {
        .stat_action.v configure -text "Processing data ... [expr round($i*100.0/$l)]%" -fg black
        update
      }
    }
  }

  # update status display
  .stat_action.v configure -text "Processing data ... 100%" -fg black
  update


  global log_val_Time
  set log_val_Time $new

  # update column width
  global log_fields
  global log_fields_def_size
  set i [lsearch $log_fields "Time"]
  set log_fields_def_size [lreplace $log_fields_def_size $i $i [expr $subsecond_decimal + 8]]
  show_messages

  # update status display
  .stat_action.v configure -text "Idle" -fg black
  update
  
  set busy 0
}

proc doClean {} {
  global online
  global busy
    
  # update status display
  if {!$online} {
    if {$busy} {return}
    set busy 1
    .stat_action.v configure -text "Idle" -fg black
    .stat_query.v configure -text ""
  } else {
    global t0_connect
    set t0_connect [clock seconds]
  }
  update_stats reset
  
  if {!$online} {
    set busy 0
  }
  
  update
}

# RK: parsing of command line options, e.g. for status display
proc settoplevelparameters {} {
  global argv subsecond_decimal online autoclean
  if {[lindex $argv 0] == "errormonitor"} {
    set width [winfo vrootwidth .]
    set height [winfo vrootheight .]
    set x [winfo vrootx .]
    set y [winfo vrooty .]
    wm geometry . ${width}x${height}+$x+$y
    set subsecond_decimal 3
    .select.filter.vex_severity insert 0 I
    set online 1
    set autoclean 1
  }
}
settoplevelparameters

