/* Event dump DATE utility program
 * ===============================
 *
 * Module history:
 *  1.00  20-Aug-98  RD  Created
 *  1.01  15-Sep-98  RD  MP declare added
 *  1.02  27-Oct-98  RD  Silent switch added
 *  1.03  28-Oct-98  RD  Silent cursor made function of time
 *  1.04  15-Jan-99  RD  Bug with Asynch sampling fixed
 *  1.05  18-Jan-99  RD  Re-structured
 *  1.06  19-Feb-99  RD  Swapping added
 *  1.07   1-Mar-99  RD  Data dumped as 32 bits rather then 8 bits
 *  1.08   7-Jul-99  RD  Dump of monitor attributes added
 *  1.09   6-Dec-99  RD  Small problem with "old type" events fixed
 *  1.10   6-Apr-00  RD  Other problem with "old type" events fixed
 *  1.11  12-May-00  LS  Version to write selected events in DATE raw file
 *  1.12  12-Jul-00  RD  Added "print after" switches
 *  1.13   1-Dec-00  RD  Adapted for DATE 4.x
 *  1.14   5-Jul-01  RD  Changed handling of attributes (and-or)
 *  1.15   1-Nov-04  RD  Added dump of PERIOD
 *  1.16   8-Nov-04  RD  Data checks changed for new test equipment
 *  1.17  17-Aug-05  RD  Added Vanguard and Rearguard events
 *  1.18   5-Sep-05  RD  Added SST and DST events
 *  1.19  14-Sep-05  RD  VAN/REARGUARD events changed into SOD/EOD
 *  1.20  23-Jul-06  FN  Extension to eventDumpAll version, running under SLC4/DATE5  
 *  1.21  15-Dec-06  FN  Renamed to "eventDumpAllNew" version keeping eventDump functionality 
 *  1.22  30-Apr-07  FN  New Option: Dump Data content for given spillNo AND evtNoInSpill  
 */
#define VID "1.20"

#pragma GCC diagnostic ignored "-Wwrite-strings"

#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#include "Screen_Dump.h"
#include "event.h"
#include "monitor.h"
#include "attributesHandler.h"
#include "subEvent.h"   

#define DESCRIPTION "DATE V3 event dump"
#ifdef AIX
static
#endif
char mpDaemonMainIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                        """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#ifndef TRUE
# define TRUE ( 0 == 0 )
#endif
#ifndef FALSE
# define FALSE ( 0 == 1 )
#endif

typedef uint64_t VOID_PTR;

/* Macros for log strings handling */
#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

uint8_t *dataBuffer;
char *dataSource;
char filename[128];
int32_t terminate;
int32_t briefOutput;
int32_t noOutput;
int32_t checkData;
int32_t useStatic;
int32_t asynchRead;
int32_t interactive;
int32_t must;
int32_t wfile;
int32_t wfile_id;	// SS added
int32_t fd;
int32_t maxGetEvents;
int32_t dumpAll;                     //FN added
int32_t srcIDspecific;               //FN added
int32_t srcID;                       //FN added
int32_t formatIDspecific;            //SS added
int32_t formatID;                    //SS added
int32_t useTable;
int32_t useAttributes;
int32_t numErrors;
int32_t numChecked;
uint32_t expectedPattern;
uint32_t lastEventType;  //FN added
int32_t numHeaders;
int32_t bytesHeaders;
int32_t numSuperEvents;
int32_t numEventData;
int32_t bytesEventData;
int32_t staticDataBuffer[ 100000 ];
int32_t doOutput;
// int32_t startNbInRun;            // FN added
int32_t startBurstNb;	     // FN added
int32_t startBurstNbLoaded;   // FN added
int32_t startNbInBurst;
int32_t startNbInBurstLoaded;
int32_t startBurstNbNbInBurst;
int32_t startBurstNbNbInBurstLoaded;
int32_t startBunchCrossingLoaded;
int32_t startBunchCrossing;
int32_t startOrbitLoaded;
int32_t startOrbit;
int32_t startEventIdLoaded;
eventIdType startEventId;
int32_t startSerialNbLoaded;
int32_t startSerialNb;
int32_t numEvents;
int32_t gotSuperEvents = FALSE;
int32_t dumpEquipmentHeader;
int32_t dumpCommonDataHeader;


#ifdef UDP_MONITOR
const char *mustTable[3] = { "all", "all", 0 };
#else
char *mustTable[3] = { "all", "all", 0 };
#endif

int32_t writeSubEvent(int32_t evsize, uint32_t *data_buf);

/* FN: decode_equipment taken von FHH to check and loop for S-MUX - FN 01.12.2006 */
/* FN: decodes first 7 or 3 spill buffer words (DATE5 or DATE3)*/
int32_t nerror=0;
int32_t nevents=0;

void decode_equipment(int32_t version, int32_t ldc, int32_t *size_eq, uint32_t * buffer) {

  struct subeventStruct subevent[1];
  int32_t error;
  int32_t head_len;
  int32_t src_id, k, offs, len, smux;

  *size_eq = buffer[0]/4; 
  // FN
  switch (version) {
    case 0x50:  head_len = 3;
    default: head_len =7;
  }
  src_id=(buffer[head_len]>>16)&((1<<10)-1);

  if (version == 0x50) {
    // FN    head_len=3;
    *size_eq=buffer[2]/4+head_len;

    if (srcIDspecific == FALSE || srcID == src_id) {
      printf("\nLDC = %i  S-LINK = %i\n",ldc,(buffer[1]>>16)&0xF);
      printf("\nSLINK & Header: %d Words\n",*size_eq);
      printf("type %i, extlen 0x%x, ID 0x%x, other 0x%x, len %i\n",
	     buffer[0]>>16,buffer[0]&0xFFFF,buffer[1]>>16,buffer[1]&0xFFFF,
	     buffer[2]);
      /* old: len/type, res/rawByteAlign/eqID, dataLen */
    }
  } else {
    if (srcIDspecific == FALSE || srcID == src_id) {
      printf("\nLDC = %i  S-LINK = %i\n",ldc,buffer[2]&0xF);
      printf("\nSLINK & Header: %d Words\n",*size_eq);
      
      printf ("Type 0x%x, ID 0x%x, Attr: 0x%x 0x%x 0x%x, BasicSize 0x%x\n",buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6]);
    }
    /* Size, Type, Id, TypeAttribute[3], BasicElementSize */
    // FN    head_len=7;
  }
  
  // FN  src_id=(buffer[head_len]>>16)&((1<<10)-1);
  smux=0;
  len=*size_eq-head_len;
  offs=head_len;
  if (len<=0) {
    printf("\nERR empty S-Link equipment!" );
    nerror++;
    //FN    printf("Events: %d, errors: %d\n\n",nevents,nerror);
    printf("Events: %d, errors: %d\n\n",numEvents,nerror);
    return;
  }

  if ((src_id>=0x380) & (src_id<0x400)) {
    smux=1;
    if (srcIDspecific == FALSE || srcID == src_id) { // FN: ERR suppression in id mode
      printf("\n  0: %08x  Error: %d, Event Type: 0x%02x, Source ID: %4i, Size: %d\n", 
	     buffer[offs], ( (buffer[offs] >> 31) & ((1 << 1)-1) ),
	     ( (buffer[offs] >> 26) & ((1 << 5) -1) ),
	     src_id,buffer[offs]&0xffff);
      
      /* if (mxlen/4 >= 2) */
      printf("  1: %08x  Status: %d, Spill Number: %d, Event Number: %d\n",
	     buffer[offs+1], ( (buffer[offs+1] >> 31) & ((1 << 1) -1) ),
	     ( (buffer[offs+1] >> 20) & ((1 << 11) -1) ),
	     ( (buffer[offs+1] >>  0) & ((1 << 20) -1) ));
    }
    offs+=2; //HW- Das musste raus aus 'if()'
  }

  /* loop over up to n s-link buffers from smux/tiger data concentrator, if smux data*/
  for (k=0;k<1023;k++) {
    if (smux) len=(buffer[offs] & 0xffff);
    subevent[0].decode_err=-1;
    subEvent_decode( len*4, (int32_t *)&buffer[offs], &subevent[0]); 
    if (srcIDspecific == FALSE || srcID == src_id) { // FN: ERR suppression in id mode
      subEvent_check(1, subevent, &error); 
    }
    if (error) {
      nerror++;
      // FN      printf("Events: %d, errors: %d\n\n",nevents,nerror);
      if (srcIDspecific == FALSE || srcID == src_id) { // FN: ERR suppression in id mode
	printf("Events: %d, errors: %d\n\n",numEvents,nerror);
      }
    }
    // FN: here srcID check
    if (srcIDspecific == TRUE){//FN: check for srcID or S-MUX and then srcID
      if (src_id == srcID || ( smux && ( (buffer[offs] >>16) & ((1<< 10)-1) ) == srcID ) ){ //HW- smux Abfrage kein offs+2, da oben schon geschehen
        // check the formatID if we want only a specific one
        if ( formatIDspecific == FALSE || formatID==((buffer[offs+2] >> 24) & ((1 << 8) -1)) ) {
          if(wfile_id) writeSubEvent(len*4, &buffer[offs]);	// SS write raw event to file (for Treedata)
          else Screen_Dump(len*4, &buffer[offs]);
        }
	else printf("skipping event with format: %02x\n",((buffer[offs+2] >> 24) & ((1 << 8) -1)) );
      }
    }
    else if(srcIDspecific == FALSE) Screen_Dump(len*4, &buffer[offs]); 
    else printf("srcIDspecific STRANGE: %d \n",srcIDspecific);
    if (!smux) break;
    offs+=len;
    if (offs >= *size_eq) break;
  }
}
/* end of decode_equipment()  */




/* FN: begin of decode_header() taken from FHH  */
void decode_header(int32_t size, int32_t *size_header, int32_t *version, int32_t *super, int32_t *ldc, uint32_t *header) {
  /* date 5 */
  char* typetext[14] = {"0                   ","start of run        ",
                        "end of run          ","start of run file   ",
                        "end of run file     ","start of burst      ",
                        "end of burst        ","physics event       ",
                        "calibration event   ","event format error  ",
                        "start of data       ","end of data         ",
                        "system soft trigger ","det. soft trigger   "};
  char* attrtext[32] = {"start     ","stop      ","swapped   ","paged     ",
                        "super ev. ","orbit bc  ","keep pag. ","hlt dec.  ",
                        "          ","          ","          ","          ",
                        "          ","          ","          ","          ",
                        "          ","          ","          ","          ",
                        "          ","          ","          ","          ",
                        "          ","          ","          ","          ",
                        "          ","          ","TRUNCATED ","ev. ERROR "}; 

  /* date 3.7.1 */
#define ID_TO_NUM(bit) (2-((bit)>>5))
#define ID_TO_BIT(bit) (int32_t)(1<<((bit)&0x1f))
#define TEST_BIT(mask, bit) (((mask)[ ID_TO_NUM(bit) ] & ID_TO_BIT(bit))!=0)
#define OLD_SUPER_EVENT_BIT 95
  /* ???? */
#define SUPER_EVENT_BIT 95
#define MAX_DETECTOR 96

  int32_t i;
  /* decode header */

  *size_header = header[1]/4-1;
  *version = header[2];
  *ldc =0;
  if (*version == 0x50) {
    *size_header = header[2]/4-1;
    /* old version DATE 3.7.1 */
    if ( (TEST_BIT( &header[9], SUPER_EVENT_BIT )) ||
	 (TEST_BIT( &header[9], OLD_SUPER_EVENT_BIT ))) {
      *super = 0x10;
    } else {
      *super = 0;
      for (i=0; i<MAX_DETECTOR; i++) {
	if ( (TEST_BIT( &header[9], i))) {
	  if (!( (TEST_BIT( &header[9], SUPER_EVENT_BIT )) ||
		 (TEST_BIT( &header[9], OLD_SUPER_EVENT_BIT ))) ) { /* superevent mask */
	    *ldc = i;
	  }
	}
      }
    }

  if (*super) {
    printf("\nGDC event size: %i\n",size);
  } else { 
    printf("\nLDC event size: %i\n",size);
  }
  printf("\nMagic:     %X\n",header[0]);
  printf("Type:      %i %s\n",header[1],(header[1]<9)?typetext[header[1]]:" ");
  printf("Size:      %i     Date V3.7.1\n",header[2]);
  printf("Run:       %i\n",header[3]);
  printf("Burst:     0x%x\n",header[4]);
  printf("Spill/event: %i/%i\n",header[5]/0x100000,header[5]&0xFFFFF);
  printf("Event:     0x%x\n",header[6]);
  printf("triggerNb: 0x%x\n",header[7]);
  printf("fileSeq:   0x%x\n",header[8]);
  printf("detectorId: 0x%x\n",header[9]);
  printf("detectorId: 0x%x\n",header[10]);
  printf("detectorId: 0x%x\n",header[11]);
  printf("time:       %s",ctime((const time_t*)&header[12]));
  printf("usec:       %i\n",header[13]);
  printf("error:      %i\n",header[14]);
  printf("deadtime:   %i\n",header[15]);
  printf("deadtime us:%i\n",header[16]);
  printf("typeAttr:   0x%x\n",header[17]);
  printf("trigger:    0x%x\n",header[18]);

  } else {
    /* DATE 5 */

  *super = header[12] & 0x10;
  *ldc = header[13];

  if (*super) {
    printf("\nGDC event size: %i\n",size);
  } else { 
    printf("\nLDC event size: %i\n",size);
  }
  printf("\nMagic:     %X\n",header[0]);
  printf("Size:      %i\n",header[1]);
  printf("Version:   0x%x   %s\n",header[2],(header[2]==0x30006)?"DATE V5":"unknown");
  printf("Type:      %i %s\n",header[3],(header[3]<14)?typetext[header[3]]:" ");
  printf("Run:       %i\n",header[4]);
  printf("Spill/event: %i/%i\n",header[5]/0x100000,header[5]&0xFFFFF);
  printf("eventID:   0x%x\n",header[6]);
  printf("trigger:   0x%x\n",header[7]);
  printf("trigger:   0x%x\n",header[8]);
  printf("detectorPatt: %i\n",header[9]);
  printf("userAttr:  0x%x\n",header[10]);
  printf("userAttr:  0x%x\n",header[11]);
  printf("systemAttr: 0x%x  ",header[12]);
  for (i=0;i<32;i++) {
    /*    if ( (TEST_BIT( &header[12], i))) { */
    if (header[12] & (1<<i)) {
      printf("%s",attrtext[i]);
    }
  }
  printf("\n");
  printf("LDC:       %i\n",header[13]);
  printf("GDC:       %i\n",header[14]);
  printf("time:      %s\n",ctime((const time_t*)&header[15]));
  }
  /*      if ((header[3]!=1) && (header[13]!=-1)) */
}
/* FN: end of decode_header()  */


// this function returns TRUE, if the event should be saved
// at the moment it is only checked if a specific formatID is present
int32_t eventToBeSaved( void ) {
  int32_t *buffer = (int32_t *)dataBuffer;
  int32_t size = buffer[0];  // the GDC event size in bytes

  int32_t i, size_header, version, super, head_len;
  int32_t k, size_ldc, size_eq, ldc=0;

  int32_t size_slink, srcID, srcFormat, smux, j;
  int32_t toSave = FALSE;

  // if we do not look for a specific format, just save all events
  if ( !formatIDspecific ) return TRUE;

  if ( !briefOutput ) printf("-------------- \n" );

  i=1; // buffer position in dwords
  
  size_header=buffer[i+1]/4-1;
  version=buffer[i+2];
  super=buffer[i+12] & 0x10;
  //printf("SEBI header: size=%d version=%d super=%d \n", size_header , version , super );
  //decode_header(size, &size_header, &version, &super, &ldc, &buffer[i]);
  i+=size_header;

  switch (version) {
    case 0x50:  head_len = 3;
    default: head_len =7;
  }


  if (super) {
    i++;
    // loop over all LDCs
    while (i*4<size) {
      k=0;
      size_ldc = buffer[i-1];  // in bytes
      k++;
      size_header=buffer[i+1]/4-1;      
      //decode_header(size_ldc, &size_header, &version, &super, &ldc, &buffer[i]);
      k=size_header;

      while(k*4<size_ldc-head_len*4) {
	// loop over S-Links
	j = i+k+head_len;
	size_slink = buffer[i+k]/4;
	srcID = (buffer[j]>>16)&((1<<10)-1);
	if ((srcID>=0x380) & (srcID<0x400)) smux=1; else smux=0;

	if(!smux) {
		srcFormat = (buffer[j+2] >> 24) & ((1 << 8) -1);
        if ( srcFormat == formatID ) toSave = TRUE;
		if ( !briefOutput ) printf("SLINK: size=%d srcID=%d fmt=0x%02x \n", size_slink , srcID , srcFormat );
	} else {
		if ( !briefOutput ) printf("SMUX : size=%d srcID=%d\n", size_slink , srcID );
		j+=2;
		while( j< i+k+size_slink ) {
			size_eq=(buffer[j] & 0xffff);
			srcID = (buffer[j]>>16)&((1<<10)-1);
			srcFormat = (buffer[j+2] >> 24) & ((1 << 8) -1);
            if ( srcFormat == formatID ) toSave = TRUE;
			j+=size_eq;
			if ( !briefOutput ) printf("---->: size=%d srcID=%d fmt=0x%02x \n", size_eq , srcID , srcFormat );
		}
	}
	//decode_equipment(version, ldc, &size_eq, &buffer[i+k]);
	k+=size_slink;
      } // end loop over S-Links
      i+=size_ldc/4;
    }  // end loop over LDCs
  } else {
    // no GDC already printed LDC
      k=0;
      size_ldc = size;
      while(k*4<size_ldc-size_header*4-head_len*4) {
	// loop over S-Links
	j = i+k+head_len;
	size_slink = buffer[i+k]/4;
	srcID = (buffer[j]>>16)&((1<<10)-1);
	if ((srcID>=0x380) & (srcID<0x400)) smux=1; else smux=0;

	if(!smux) {
		srcFormat = (buffer[j+2] >> 24) & ((1 << 8) -1);
        if ( srcFormat == formatID ) toSave = TRUE;
		if ( !briefOutput ) printf("SLINK: size=%d srcID=%d fmt=0x%02x \n", size_slink , srcID , srcFormat );
	} else {
		if ( !briefOutput ) printf("SMUX : size=%d srcID=%d\n", size_slink , srcID );
		j+=2;
		while( j< i+k+size_slink ) {
			size_eq=(buffer[j] & 0xffff);
			srcID = (buffer[j]>>16)&((1<<10)-1);
			srcFormat = (buffer[j+2] >> 24) & ((1 << 8) -1);
            if ( srcFormat == formatID ) toSave = TRUE;
			j+=size_eq;
			if ( !briefOutput ) printf("---->: size=%d srcID=%d fmt=0x%02x \n", size_eq , srcID , srcFormat );
		}
	}
	//decode_equipment(version, ldc, &size_eq, &buffer[i+k]);
	k+=size_slink;
      } // end loop over S-Links
  }
  
  if(toSave) printf(".");
  
  return toSave;
}


/* FN: decode_event() taken from FHH ->  to check for SMUX  !!! Attention: different from decodeEvent !!!) */
void decode_event(int32_t size, uint32_t *buffer) {
  int32_t i, size_header, version, super;
  int32_t k, size_ldc, size_eq, ldc;
  i=0;
  decode_header(size, &size_header, &version, &super, &ldc, &buffer[0]);
  i+=size_header;

  if (super) {
    /* loop over all LDCs */
    while (i<size) {
      k=0;
      size_ldc = buffer[i]/4; i++;k++;
      decode_header(size_ldc, &size_header, &version, &super, &ldc, &buffer[i]);
      i+=size_header; k+=size_header;

      while(k<size_ldc) {
	/* loop over S-Links */
	decode_equipment(version, ldc, &size_eq, &buffer[i]);
	i+=size_eq;k+=size_eq;
      }
    }
  } else {
    /* no GDC already printed LDC */
      k=size_header;
      size_ldc = size;
      while(k<size_ldc) {
	/* loop over S-Links */
	decode_equipment(version, ldc, &size_eq, &buffer[i]);
	i+=size_eq;k+=size_eq;
      }
  }
}
/* End of decode_event */

/* Check the data content against the defined test sequence */
void doCheckData( const uint8_t * const buffer,
		  int32_t from,
		  int32_t to,
		  const int32_t scream ) {
  int32_t i;
  struct equipmentHeaderStruct *eh;
  int32_t ehBase;
  uint32_t expectedPattern;

  if ( lastEventType != PHYSICS_EVENT ) return;

  ehBase = 0;
  eh = (struct equipmentHeaderStruct *)&buffer[ehBase];

  for ( i = from,
	  expectedPattern = ( from -
			      sizeof( struct equipmentHeaderStruct ) -
			      4 ) >> 2;
	i < to;
	i += 4 ) {
    uint32_t gotPattern;

    /* Re-synch with the equipment headers (if necessary) */
    if ( i >= ehBase + eh->equipmentSize ) {
      do {
	ehBase += eh->equipmentSize;
	eh = (struct equipmentHeaderStruct *)&buffer[ehBase];
      } while ( i >= ehBase + eh->equipmentSize );
      expectedPattern = (i -
			 ehBase -
			 sizeof( struct equipmentHeaderStruct ) -
			 4 ) >> 2;
    }

    /* Skip data from the equipment header & first data word */
    if ( i < ehBase + sizeof( struct equipmentHeaderStruct ) + 4 ) {
      expectedPattern = 0;
    } else {
      gotPattern = *(uint32_t *)&buffer[i];
      if ( expectedPattern != gotPattern ) {
	if ( scream )
	  printf( " <<<(E:%08x G:%08x)", expectedPattern, gotPattern );
	numErrors++;
      }
      numChecked++;
      expectedPattern++;
    }
  }
} /* End of checkData */


/* Dump a data buffer in ASCII form (if possible) */
void dumpAscii( uint8_t *buffer,
		int32_t from,
		int32_t to ) {
  int32_t i;

  if ( from == to ) return;

  for ( i = to; ; i++ ) {
    if ( i % 16 == 0 ) break;
    printf( "  " );
    if ( i % 4 == 3 ) printf( " " );
  }
  printf( "| " );
  for ( i = from; i != to; i++ ) {
    if ( isprint( buffer[ i ] ) )
      printf( "%c", buffer [ i ] );
    else
      printf( "." );
    if ( i % 4 == 3 ) printf( " " );
  }
  printf( "|" );
} /* End of dumpAscii */


void doDumpAll( uint8_t *buffer, int32_t size, int32_t check, int32_t ldcID, int32_t size_ldc_bits ) {
  int32_t i, start;
  int32_t j, size_evt, size_evt_bits, size_header, size_header_bits, size_ldc, version;
  int32_t k, size_eq, gdc, ldc;
  
  ldc = ldcID;
  size_ldc = size_ldc_bits/4;  // in words
  j=0;

  /*
   for( l = 0; l < 65; l++){
        if(l%4==0)  printf( "FNdebug: doDumpAll: Chcek for buffer: i = %d  ,buffer[i] =  %08x \n", l,  *(int32_t *)(&buffer[l]));
	}
 */ 
  if ( size <= 0 ) return;
  if ( !briefOutput ) {
   // FN: Try loop over Slinks etc.
   // FN  char *ptr = dataBuffer;
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;

  size_header_bits = ev -> eventHeadSize;
  size_header      = (size_header_bits/4 -1);     //in words
  size_evt_bits    = ev -> eventSize;
  size_evt         = (size_evt_bits/4 - 1);       //in words 
  gdc = ev->eventGdcId;
  version = ev->eventVersion;

  if (TEST_SYSTEM_ATTRIBUTE( ev->eventTypeAttribute, ATTR_SUPER_EVENT)){ 
        k=0;
	k+=17;      //FN: skip LDC header
        while(k < size_ldc) {  
	 /* loop over S-Links */
       	  decode_equipment(version, ldc, &size_eq,(uint32_t *) buffer+j);
	  j+=size_eq; k+=size_eq;
        }
   } else { 
     /* no GDC already printed LDC */
       k=size_header;
       while(k < size_ldc-3) { 
	/* loop over S-Links */
	  decode_equipment(version, ldc, &size_eq, (uint32_t *) buffer+j);
	j+=size_eq;k+=size_eq;
      }
    }
  /* FN: end of loop over Smux */
  }
   
} /* End of doDumpAll */

void doDump( uint8_t *buffer, int32_t size, int32_t check ) {
  int32_t i, start;

  if ( size <= 0 ) return;
  if ( !briefOutput ) {

    for ( start = 0, i = 0; i != size; ) {
      if ( i % 16 == 0 ) {
	dumpAscii( buffer, start, i );
	if ( check && checkData )
	  doCheckData( buffer, start, i, TRUE );
	start = i;
	if ( i != 0 ) printf( "\n" );
	printf( "%4d) ", i );
      }
      if ( ( size - i ) >= 4 ) {
	printf( "%08x ", *(int32_t *)(&buffer[i]));
  	i += 4;
      } else {
	printf( "%02x", buffer[ i ] );
	if ( i % 4 == 3 ) printf( " " );
	i++;
      }	
    }
    dumpAscii( buffer, start, i );
    if ( check && checkData )
      doCheckData( buffer, start, i, TRUE );
    printf( "\n" );
  } else {
    if ( check && checkData )
      doCheckData( buffer, 0, size, FALSE );
  }
} /* End of doDump */

/* Dump a buffer in HEX and ASCII format. Eventually check the content */
void dumpData( struct eventHeaderStruct *event ) {
  int32_t size_ldc = event->eventSize; 
  int32_t ldcID = event->eventLdcId;

  lastEventType = event->eventType;
  if (dumpAll==TRUE)
    doDumpAll( (uint8_t*)((VOID_PTR)event + event->eventHeadSize),
	  event->eventSize - event->eventHeadSize,
	  TRUE, ldcID, size_ldc ); //FN: Inheritated from doDump -> added: ldcID, size_ldc
  else   
    doDump( (uint8_t*)((VOID_PTR)event + event->eventHeadSize),
	  event->eventSize - event->eventHeadSize,
	  TRUE); //FN: Inheritated from doDump -> added: ldcID, size_ldc

} /* End of dumpData */


/* Swap an event header. We assume that the first LW of the event
 * contains the header length.
 */
void swapHeader( void *inHeader ) {
  int32_t *ptr = (int32_t *)inHeader;
  int32_t  i = 0;
  int32_t  size = -1;

  i = 0;
  ptr = (int32_t *)inHeader;
  do {
    int32_t word = *ptr;
    *ptr = ((word << 24) & 0xff000000) |
           ((word <<  8) & 0x00ff0000) |
           ((word >>  8) & 0x0000ff00) |
           ((word >> 24) & 0x000000ff);
    if ( i == 0 ) size = *ptr;
    i += 4;
    ptr++;
  } while ( i < size );
} /* End of swapHeader */

/* Dump a Common Data Header block attributes field */
void printCdhBlockAttributes( uint32_t ba ) {
  int32_t i, n;

  for ( n = 0, i = 0; i != 7; i++ ) {
    if ( ((1 << i) & ba) != 0 ) {
      if ( n == 0 ) printf( "=" );
      else          printf( "+" );
      n++;
      printf( "%d", i );
      ba ^= (1 << i);
    }
  }
  if ( ba != 0 ) {
    if ( n == 0 ) printf( "=" );
    else          printf( "+" );
    n++;
    printf( "RESIDUE:%08x!", ba );
  }
} /* End of printCdhBlockAttributes */

void testCdhBit( uint32_t *se,
		 int32_t bit,
		 char *text,
		 int32_t *n ) {
  int32_t mask = 1 << bit;

  if ( (*se & mask) != 0 ) {
    if ( (*n)++ != 0 ) printf( "+" );
    printf( text );
    *se ^= mask;
  }
} /* End of testCdhBit */
/* Dump a Common Data Header Status & Error bits field */
void printCdhStatusErrorBits( uint32_t se ) {
  int32_t n = 0;

  testCdhBit( &se,
	      CDH_TRIGGER_OVERLAP_ERROR_BIT,
	      "triggerOverlapError",
	      &n );
  testCdhBit( &se,
	      CDH_TRIGGER_MISSING_ERROR_BIT,
	      "triggerMissingError",
	      &n );
  testCdhBit( &se,
	      CDH_DATA_PARITY_ERROR_BIT,
	      "dataParityError",
	      &n );
  testCdhBit( &se,
	      CDH_CONTROL_PARITY_ERROR_BIT,
	      "controlParityError",
	      &n );
  testCdhBit( &se,
	      CDH_FEE_ERROR_BIT,
	      "feeError",
	      &n );
	      
  testCdhBit( &se,
	      CDH_TRIGGER_INFORMATION_UNAVAILABLE_BIT,
	      "noTriggerInfo",
	      &n );

  if ( se != 0 ) {
    if ( n++ != 0 ) printf( "+" );
    printf( "RESIDUE:x%x!", se );
  }
} /* End of printCdhStatusErrorBits */

/* Dump a Event header (if possible). Returns TRUE if "good" event header */
int32_t dumpHeader( struct eventHeaderStruct *header,
		int32_t scream ) {
  int32_t swapped = FALSE;

  if ( header->eventMagic == EVENT_MAGIC_NUMBER_SWAPPED ) {
    swapped = TRUE;
    swapHeader( header );
  }
  
  if ( header->eventMagic != EVENT_MAGIC_NUMBER ) {
    if ( scream ) {
      printf( "!!! WARNING !!! MAGIC: %08x (expected: %08x)\n",
	      header->eventMagic,
	      EVENT_MAGIC_NUMBER );
    } else {
      return( FALSE );
    }
  }
  if (srcIDspecific == FALSE){ // FN suppress headers in id mode 
    printf( "Size:%d (header:%d)", header->eventSize, header->eventHeadSize );
    printf( " " );
    printf( "Version:0x%08x", header->eventVersion );
    printf( " " );
    printf( "Type:" );
    switch ( header->eventType ) {
    case START_OF_RUN :       printf( "StartOfRun" ); break;
    case END_OF_RUN :         printf( "EndOfRun" ); break;
    case START_OF_RUN_FILES : printf( "StartOfRunFiles" ); break;
    case END_OF_RUN_FILES :   printf( "EndOfRunFiles" ); break;
    case START_OF_BURST :     printf( "StartOfBurst" ); break;
    case END_OF_BURST :       printf( "EndOfBurst" ); break;
    case PHYSICS_EVENT :      printf( "PhysicsEvent" ); break;
    case CALIBRATION_EVENT :  printf( "CalibrationEvent" ); break;
    case EVENT_FORMAT_ERROR : printf( "EventFormatError" ); break;
    case START_OF_DATA :      printf( "StartOfData" ); break;
    case END_OF_DATA :        printf( "EndOfData" ); break;
    case SYSTEM_SOFTWARE_TRIGGER_EVENT :
      printf( "SystemSoftwareTriggerEvent" ); break;
    case DETECTOR_SOFTWARE_TRIGGER_EVENT :
      printf( "DetectorSoftwareTriggerEvent" ); break;
    default :                 printf( "UNKNOWN TYPE %d 0x%08x",
				      header->eventType, header->eventType );
    }
    if ( swapped )
      printf( " SWAPPED" );

    printf( "\n" );

    printf( "RunNb:%d", header->eventRunNb );
    printf( " " );

    if ( TEST_SYSTEM_ATTRIBUTE( header->eventTypeAttribute,
				ATTR_ORBIT_BC ) ) {
      printf( "Period:%d Orbit:%d BunchCrossing:%d",
	      EVENT_ID_GET_PERIOD( header->eventId ),
	      EVENT_ID_GET_ORBIT( header->eventId ),
	      EVENT_ID_GET_BUNCH_CROSSING( header->eventId ) );
    } else {
      printf( "nbInRun:%d burstNb:%d nbInBurst:%d",
	      EVENT_ID_GET_NB_IN_RUN( header->eventId ),
	      EVENT_ID_GET_BURST_NB( header->eventId ),
	      EVENT_ID_GET_NB_IN_BURST( header->eventId ) );
    }
    printf( " " );
  
    printf( "ldcId:" );
    if ( header->eventLdcId == VOID_ID ) printf( "VOID" );
    else printf( "%d", header->eventLdcId );
    printf( " " );

    printf( "gdcId:" );
    if ( header->eventGdcId == VOID_ID ) printf( "VOID" );
    else printf( "%d", header->eventGdcId );
    printf( " " );

    {
      time_t t = header->eventTimestamp;
    
      printf( "time:%s", ctime( &t ) );
    } /* Note that ctime will add a "\n" !!! */
  
    printf( "Attributes:" );
    monitorPrintAttributes( header, stdout );
    if ( !SYSTEM_ATTRIBUTES_OK( header->eventTypeAttribute ) )
      printf( "{NOT OK}" );
    printf( "\n" );
  
    printf( "triggerPattern:%08x-%08x%s%s",
	    header->eventTriggerPattern[0],
	    header->eventTriggerPattern[1],
	    TRIGGER_PATTERN_VALID(header->eventTriggerPattern) ? "":"[invalid]",
	    TRIGGER_PATTERN_OK(header->eventTriggerPattern) ? "" : "{NOT OK}" );
    printf( " " );

    printf( "detectorPattern:%08x%s%s",
	    header->eventDetectorPattern[0],
	    DETECTOR_PATTERN_VALID(header->eventDetectorPattern) ?
	    "" : "[invalid]",
	    DETECTOR_PATTERN_OK(header->eventDetectorPattern) ?
	    "" : "{NOT OK}" );
    printf( "\n" );

    if ( header->eventHeadSize > EVENT_HEAD_BASE_SIZE ) {
      if ( !briefOutput )
	printf( "Header extension (%d byte(s)):\n",
		header->eventHeadSize - EVENT_HEAD_BASE_SIZE );
      doDump( ((uint8_t*)header) + EVENT_HEAD_BASE_SIZE,
	      header->eventHeadSize - EVENT_HEAD_BASE_SIZE,
	      FALSE );
      if ( !briefOutput )
	printf( "Payload:\n" );
    }
  }// FN END suppress headers in id mode

  if ( dumpEquipmentHeader ) {
    if ( (header->eventType == PHYSICS_EVENT
       || header->eventType == CALIBRATION_EVENT)
      && !TEST_SYSTEM_ATTRIBUTE( header->eventTypeAttribute,
				 ATTR_SUPER_EVENT) ){
      uint8_t *p;

      for ( p = (uint8_t *)header + header->eventHeadSize;
	    p < (uint8_t *)header + header->eventSize; ) {
	struct equipmentHeaderStruct *eh = (struct equipmentHeaderStruct *)p;

	printf( " - Equipment: size:%d type:%d id:%d basicElementSize:%d attributes:",
		eh->equipmentSize,
		eh->equipmentType,
		eh->equipmentId,
		eh->equipmentBasicElementSize );
	monitorPrintAttributesMask( &eh->equipmentTypeAttribute, stdout );
	printf( "\n" );
	if ( dumpCommonDataHeader ) {
	  struct commonDataHeaderStruct *h =
	    (struct commonDataHeaderStruct *)(p + sizeof( struct equipmentHeaderStruct ));
	  
	  printf( "   > Length:%d", h->cdhBlockLength );
	  if ( h->cdhBlockLength == 0xffffffff ) {
	    printf( "=<VOID>" );
	  } else {
	    if ( eh->equipmentSize - sizeof( struct equipmentHeaderStruct ) !=
		 h->cdhBlockLength ) {
	      printf( "!=equipmentSize-sizeof(struct equipmentHeaderStruct):%d",
		      eh->equipmentSize - (int32_t)sizeof( struct equipmentHeaderStruct ) );
	    }
	  }
	  printf( " version:%d", h->cdhVersion );
	  if ( CDH_VERSION != h->cdhVersion
	    && h->cdhVersion != 0 ) {
	    printf( "(!=CDH_VERSION:%d!)", CDH_VERSION );
	  }
	  /* Skip the other fields if version numbers differ. Exception:
	     version 0 (development phase) */
	  if ( 0           == h->cdhVersion
	    || CDH_VERSION == h->cdhVersion ) {
	    printf( " l1TriggerMsg:%x", h->cdhL1TriggerMessage );
	    printf( " blockAttr:x%x", h->cdhBlockAttributes );
	    if ( h->cdhBlockAttributes != 0 )
	      printCdhBlockAttributes( h->cdhBlockAttributes );

	    printf( "\n    " );
	    
	    printf( " eventId1(bunch crossing):%d", h->cdhEventId1 );
	    printf( " eventId2(orbit counter):%d", h->cdhEventId2 );	    
	    printf( " miniEventId(bunch crossing):%d", h->cdhMiniEventId );

	    printf( "\n    " );
	    
	    printf( " participatingSubDetectors:x%x", h->cdhParticipatingSubDetectors );
	    printf( " Status/Errors:" );
	    if ( h->cdhStatusErrorBits == 0 ) {
	      printf( "<NONE>" );
	    } else {
	      printCdhStatusErrorBits( h->cdhStatusErrorBits );
	    }
    
	    printf( "\n    " );
	    
	    printf( " triggerClassesHigh/Low:x%x-x%x",
		    h->cdhTriggerClassesHigh,
		    h->cdhTriggerClassesLow );
	    printf( " ROIhigh/low:x%x-x%x",
		    h->cdhRoiHigh,
		    h->cdhRoiLow );

	    if ( h->cdhMBZ0 != 0
	      || h->cdhMBZ1 != 0
	      || h->cdhMBZ2 != 0
	      || h->cdhMBZ3 != 0 ) {
	      printf( "\n    " );
	      printf( " MBZ0:x%x!", h->cdhMBZ0 );
	      printf( " MBZ1:x%x!", h->cdhMBZ1 );
	      printf( " MBZ2:x%x!", h->cdhMBZ2 );
	      printf( " MBZ3:x%x!", h->cdhMBZ3 );
	    }
	  }
	  printf( "\n" );
	}
	p += eh->equipmentSize;
	if ( eh->equipmentSize < sizeof( struct equipmentHeaderStruct ) ) {
	  printf( " >>> Equipment size too small (%d < %d): skipping\n",
		  eh->equipmentSize,
		  (int32_t)sizeof( struct equipmentHeaderStruct ) );
	  p = (uint8_t *)header + header->eventSize;
	}
      }
    }
  }
  
  return( header->eventMagic == EVENT_MAGIC_NUMBER );
} /* End of dumpHeader */


/* Decode and store a monitoring policy table */
#define MAX_ENTRIES 15
char *monitorTable[ MAX_ENTRIES ] = { 0 };
int32_t decodeTable( char *declaration ) {
  char *p = declaration;
  int32_t nEntries = 0;
  while ( *p != 0 ) {
    while ( *p == ' ' ) p++;
    if ( *p != 0 ) {
      char *s = p;
      char *r;
      while ( *p != 0 && *p != ' ' ) p++;
      if ( ( r = (char*)malloc( p-s+1 ) ) == NULL ) {
	perror( "malloc failed " );
	return 1;
      }
      strncpy( r, s, p-s );
      r[ p-s ] = 0;
      if ( nEntries == MAX_ENTRIES-1 ) {
	fprintf( stderr, "Table too complex, cannot store...\n" );
	return( 1 );
      }
      monitorTable[ nEntries++ ] = r;
      /* This part not needed any more (change in syntax of attributes')
      for (; *r != 0; r++ )
	if ( *r == '+' ) *r = ' ';
      */
    }
  }
  return 0;
} /* End of decodeTable */

int32_t decodeTableWithAttributes( char *declaration ) {
  useAttributes = TRUE;
  return decodeTable( declaration );
} /* End of decodeTableWithAttributes */


/* Print the tool's usage instructions */
void usage( char *ourName ) {
  fprintf( stderr,
	   "Usage: %s [-notAll][-id][-fmt][-b][-c][-s][-a][-i][-f \"filename\"][-n number][-t \"table\"][-T \"table\"][-# [s|t|b|t|n|e]number or: -# [z]number number] dataSource\n\
\t-notAll: calls eventDump  \n\
\t-id srcID: dumps data only from given srcID\n\
\t-fmt formatID: dumps data only for given formatID (hex)\n\
\t-b: brief output (skip long events)\n\
\t-S: silent\n\
\t-c: check event data\n\
\t-s: use static data buffer\n\
\t-a: use asynchronous reads\n\
\t-i: interactive\n\
\t-f: write selected events to raw file\n\
\t-F: write subevents from selected srcID to raw file (FIFO format)\n\
\t-n: maximum number of events to process\n\
\t-t: monitoring table to be used (e.g. -t \"ALL yes SOB no\")\n\
\t-T: as \"-t\" but the table has monitoring attributes included\n\
\t\t(e.g. -T \"ALL yes 1 PHY y 1|2 SOB NO 1&5\")\n\
\t-e: dump content of equipment header\n\
\t-D: dump content of common data header (implies \"-e\")\n\
\t-#: wait for given event\n\
\t\t(s:spillNo t:evtNoInSpill z:spillNo evtNoInSpill   b:bunchCrossing o:orbit e:orbit-bunchCrossing <nothing>:serial number)\n",
	   ourName );
} /* End of usage */


/* Check the current configuration */
void checkConfig() {
  if ( sizeof( int32_t ) != 4 )
    fprintf( stderr, "ERROR; sizeof(int32_t):%d\n", (int32_t)sizeof(int32_t) );
  if ( sizeof( int64_t ) != 8 )
    fprintf( stderr, "ERROR; sizeof(int64_t):%d\n", (int32_t)sizeof(int64_t) );
  if ( sizeof( datePointer ) != sizeof( char* ) ||
       sizeof( datePointer ) != sizeof( void* ) )
    fprintf( stderr,
	     "ERROR: \
sizeof(datePointer):%d \
sizeof(char*):%d \
sizeof(void*):%d\n",
	     (int32_t)sizeof( datePointer ),
	     (int32_t)sizeof( char* ),
	     (int32_t)sizeof( void* ) );
} /* End of checkConfig */


/* Initialise our variables */
void initVars() {
  dataSource = NULL;
  briefOutput = FALSE;
  dumpAll = TRUE;          // FN added
  srcIDspecific = FALSE;   // FN added
  formatIDspecific = FALSE;
  //  startNbInRun = FALSE;    // FN added
  startBurstNb = FALSE;	   // FN added
  startNbInBurst = FALSE;
  startBurstNbNbInBurst = FALSE; //FN added
  noOutput = FALSE;
  checkData = FALSE;
  useStatic = FALSE;
  asynchRead = FALSE;
  interactive = FALSE;
  useTable = FALSE;
  useAttributes = FALSE;
  must = FALSE;
  wfile = FALSE;
  wfile_id = FALSE;
  fd = 0;
  maxGetEvents = 0;
  srcID = 0;
  formatID = 0;
  numErrors = 0;
  numChecked = 0;
  numHeaders = 0;
  bytesHeaders = 0;
  numSuperEvents = 0;
  numEventData = 0;
  bytesEventData = 0;
  doOutput = TRUE;
  //  startNbInRun = -1;  // FN added
  startBurstNb = -1;      // FN added
  startNbInBurst = -1;    // FN added
  startBurstNbNbInBurst = -1; // FN added
  dumpEquipmentHeader = FALSE;
  dumpCommonDataHeader = FALSE;
  startBunchCrossingLoaded = FALSE;
  startOrbitLoaded = FALSE;
  startEventIdLoaded = FALSE;
  startSerialNbLoaded = FALSE;
  numEvents = 0;
} /* End of initVars */


/* Handle all command line arguments */
void handleArgs( int32_t argc, char **argv ) {
  int32_t   optNum;
  
  for ( optNum = 1; optNum != argc; ) {
    if ( strcmp( argv[optNum], "-?" ) == 0 ) {
      usage( argv[0] );
      exit( 0 );
    }
    if ( strcmp( argv[optNum], "-b" ) == 0 ) {
      briefOutput = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-S" ) == 0 ) {
      noOutput = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-notAll" ) == 0 ) {
      dumpAll = FALSE; optNum++;
    } else if ( strcmp( argv[optNum], "-id" ) == 0 ) {
      srcIDspecific = TRUE; 
      optNum++;
      sscanf( argv[optNum], "%d", &srcID );
      optNum++;
      printf("Dump data only for srcID:%d \n",srcID);
    } else if ( strcmp( argv[optNum], "-fmt" ) == 0 ) {
      formatIDspecific = TRUE; 
      optNum++;
      sscanf( argv[optNum], "%x", &formatID );
      optNum++;
      printf("Dump data only for formatID: 0x%02x \n",formatID);
    } else if ( strcmp( argv[optNum], "-c" ) == 0 ) {
      checkData = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-s" ) == 0 ) {
      useStatic = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-a" ) == 0 ) {
      asynchRead = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-i" ) == 0 ) {
      interactive = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-M" ) == 0 ) {
      must = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-e" ) == 0 ) {
      dumpEquipmentHeader = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-D" ) == 0 ) {
      dumpEquipmentHeader = TRUE; dumpCommonDataHeader = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-f" ) == 0 ) {
      wfile = TRUE; optNum++;
      snprintf( SP(filename), "%s", argv[optNum]); optNum++;
      if (wfile && wfile_id) {
	fprintf( stderr, "%s: only one \"-f\" and/or \"-F\"\n", argv[0] );
	exit( 1 );
      }
    } else if ( strcmp( argv[optNum], "-F" ) == 0 ) {	// save to file only selected src_id (FIFO format)
      wfile_id = TRUE; optNum++;
      snprintf( SP(filename), "%s", argv[optNum]); optNum++;
      if (wfile && wfile_id) {
	fprintf( stderr, "%s: only one \"-f\" and/or \"-F\"\n", argv[0] );
	exit( 1 );
      }
    } else if ( strcmp( argv[optNum], "-n" ) == 0 ) {
      optNum++;
      sscanf( argv[optNum], "%d", &maxGetEvents );
      optNum++;
      printf("Taking max. %d events\n",maxGetEvents);
    } else if ( strcmp( argv[optNum], "-t" ) == 0 ) {
      if ( !useTable ) {
	useTable = TRUE; optNum++;
	if ( decodeTable( argv[optNum++] ) != 0 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
      } else {
	fprintf( stderr, "%s: only one \"-t\" and/or \"-T\"\n", argv[0] );
	exit( 1 );
      }
    } else if ( strcmp( argv[optNum], "-T" ) == 0 ) {
      if ( !useTable ) {
	useTable = TRUE; optNum++;
	if ( decodeTableWithAttributes( argv[optNum++] ) != 0 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
      } else {
	fprintf( stderr, "%s: only one \"-t\" and/or \"-T\"\n", argv[0] );
	exit( 1 );
      }
    } else if ( strcmp( argv[optNum], "-#" ) == 0 ) {
      char *s;
      char* s2;  // FN added
      void *i;
      void* j;   // FN added
 
      optNum++;
      switch ( argv[optNum][0] ) {
      case 's' :  // FN added
	s = &argv[optNum][1];
	i = &startBurstNb;  
	sscanf(s,"%d",i);
	startBurstNbLoaded = TRUE;
	printf("Dump data only for spillNo:%d \n",startBurstNb);
	break;
      case 't' :  // FN added
	s = &argv[optNum][1];
	i = &startNbInBurst;  
	sscanf(s,"%d",i);
	startNbInBurstLoaded = TRUE;
	printf("Dump data only for evtNoInSpill:%d \n",startNbInBurst);
	break;
      case 'z' :  // FN added
       	s = &argv[optNum][1];
	optNum++;
	s2 = argv[optNum];
	i = &startBurstNb;
	sscanf(s,"%d",i);
	j= &startNbInBurst;
	sscanf(s2, "%d",j);
	// FNdebug:	printf("startBurstNb is %d, startNbInBurst is %d",startBurstNb,startNbInBurst);
     	startBurstNbNbInBurstLoaded = TRUE;
	printf("Dump data only for spillNo:%d AND evtNoInSpill: %d\n",startBurstNb, startNbInBurst);
	break;    
      case 'o' :
	s = &argv[optNum][1];
	i = &startOrbit;
	startOrbitLoaded = TRUE;
	break;
      case 'e' :
	s = &argv[optNum][1];
	i = &startEventId;
	startEventIdLoaded = TRUE;
	break;
      case 'b' :
	s = &argv[optNum][1];
	i = &startBunchCrossing;
	startBunchCrossingLoaded = TRUE;
	break;
      default :
	s = &argv[optNum][0];
	i = &startSerialNb;
	startSerialNbLoaded = TRUE;
	break;
      }
      if ( !isdigit( (int)*s ) ) {
	usage( argv[0] );
	exit( 1 );
      }

      if ( i == &startEventId ) {
	int32_t orbit;
	int32_t bunchCrossing;
	char *separator;

	if ( (separator = strchr( s, '-' )) == NULL ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	if ( strrchr( s, '-' ) != separator ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	*separator = 0;
	if ( sscanf( s, "%d", &orbit ) != 1 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	if ( sscanf( separator+1, "%d", &bunchCrossing ) != 1 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	LOAD_EVENT_ID( startEventId, 0, orbit, bunchCrossing );
      } else {
	if ( sscanf( s, "%d", (int32_t *)i ) != 1 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
      }
      doOutput = FALSE;
      optNum++;
    } else if ( dataSource == NULL && argv[optNum][0] != '-' ) {
      dataSource = argv[optNum++];
    } else {
      usage( argv[0] );
      exit( 1 );
    }
  }

  if ( briefOutput && noOutput ) {
    fprintf( stderr, "%s: -i and -S switches incompatible\n", argv[0] );
    exit( 1 );
  }
  if ( briefOutput && noOutput ) {
    fprintf( stderr, "%s: -b and -S switches incompatible\n", argv[0] );
    exit( 1 );
  }
  if ( checkData && noOutput ) {
    fprintf( stderr, "%s: -c and -S switches incompatible\n", argv[0] );
    exit( 1 );
  }
  if ( must && useTable ) {
    fprintf( stderr, "%s: -t and -M switches incompatible\n", argv[0] );
    exit( 1 );
  }
} /* End of handleArgs */


/* Setup monitoring source, parameters and characteristics */
void monitorSetup( char *ourName ) {
  int32_t status;
  
  if ( ( status = monitorDeclareMp( DESCRIPTION """ V""" VID ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot declare MP, status: %s\n",
	     monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( dataSource == NULL ) {
    usage( ourName );		/* We must have a data source! */
    exit( 1 );
  }

  if ( ( status = monitorSetDataSource( dataSource ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot attach to monitor scheme, status: %s\n",
	     monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( ( status = monitorSetSwap( FALSE, FALSE ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot set swpping mode, status: %d (0x%08x), %s",
	     status, status, monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( asynchRead ) {
    if ( ( status = monitorSetNowait() ) != 0 ) {
      fprintf( stderr,
	       "Cannot set Asynchronous mode, status: %s\n",
	       monitorDecodeError( status ) );
      exit( 1 );
    }
  }

  //sleep(1);
  //monitorFlushEvents();
  if ( must ) {
    if ( ( status = monitorDeclareTable( mustTable ) ) != 0 ) {
      fprintf( stderr,
	       "Cannot set must-monitor mode, status: %s\n",
	       monitorDecodeError( status ) );
      exit( 1 );
    }
    printf( "Entering must-monitor mode. \
WARNING: may stop data acquisition!\n" );
  }

  if ( useTable ) {
#ifdef UDP_MONITOR
    if ( useAttributes )
      status = monitorDeclareTableWithAttributes( (const char**)monitorTable );
    else
      status = monitorDeclareTable( (const char**)monitorTable );
#else
    if ( useAttributes )
      status = monitorDeclareTableWithAttributes( monitorTable );
    else
      status = monitorDeclareTable( monitorTable );
#endif
    if ( status != 0 ) {
      fprintf( stderr,
	       "Error during monitoring table declaration%s, status: %s\n",
	       useAttributes ? " with attributes" : "",
	       monitorDecodeError( status ) );
      exit( 1 );
    }
  }

  if ( useStatic ) dataBuffer = (uint8_t *)staticDataBuffer;
} /* End of monitorSetup */


/* Get the next event (if possible) */
#define DOTS_AT 5000
int32_t getNextEvent() {
  int32_t status;
  int32_t again;
  int32_t tries = 0;
  do {
    again = FALSE;
    if ( useStatic )
      status = monitorGetEvent( staticDataBuffer, sizeof(staticDataBuffer) );
    else
      status = monitorGetEventDynamic( (void**)&dataBuffer );
    if ( asynchRead && status == 0 ) {
      if ( ( useStatic && *(int32_t *)staticDataBuffer == 0 ) ||
	   ( !useStatic && dataBuffer == NULL ) ) {
	if ( ((++tries) % DOTS_AT ) == 0 ) {
	  printf( "*" );
	  fflush( stdout );
	}
	again = TRUE;
      }
    }
  } while ( again );
  if ( asynchRead && tries >= DOTS_AT ) printf( "\n" );
  return status;
} /* End of getNextEvent */


/* Prompt for user action */
void promptUser() {
  int32_t status;
  int32_t action = '\n';
  do {
    if ( numErrors != 0 )
      printf( "%d ERROR%s: ",
	      numErrors,
	      numErrors != 1 ? "S" : "" );
    printf( "n[ext event], q[uit], f[lush], l[ogout]: " );
    fflush( stdout );
    if ( (action = getchar() ) != '\n' ) {
      int32_t c;
      do { c = getchar(); } while ( c != '\n' && c != EOF);
    } else {
      action = 'n';
    }
    if ( action == ' ' ) action = 'n';
    if ( action == EOF ) action = 'q';
    if ( action == 'q' ) {
      printf( "Quitting\n" );
      terminate = TRUE;
    } else if ( action == 'f' ) {
      if ( (status = monitorFlushEvents()) != 0 ) {
	printf( "Error flushing: %d (0x%08x): %s\n",
		status, status,
		monitorDecodeError( status ) );
      }
    } else if ( action == 'l' ) {
      if ( (status = monitorLogout()) != 0 ) {
	printf( "Error logging out: %d (0x%08x): %s\n",
		status, status,
		monitorDecodeError( status ) );
      }
    } else if ( action != 'n' ) {
      printf( "Unrecognized command \'%c\'\n", action );
    }
  } while ( action != 'n' && !terminate );
} /* End of promptUser */

/* Write the event we recieved to a raw file */
int32_t writeEvent() {
  int32_t status;
  static int32_t spill = 0;

  // should we write to file?
  if (eventToBeSaved()==FALSE) return(0);

  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;
  if ( spill != (ev->eventId[0])>>20 ) {
    spill = (ev->eventId[0])>>20;
    printf("\n----- spill %d -----\n", spill );
  }
  int32_t dataSize = ev->eventSize;
  if ( fd == 0 ) {
    printf("Opening file %s\n",filename);
    fd=open( filename, O_CREAT|O_WRONLY|O_TRUNC, 0x001a4 );
    if(fd<=0) {
      printf("Error opening file %s\n",filename);
      terminate=TRUE;
      return(-1);
    }
  }
  status=write( fd, dataBuffer, dataSize );
  if ( !briefOutput )
    printf("Wrote %d/%d bytes to %s\n",status,dataSize,filename);
  if(status==dataSize) {
    return(0);
  } else {
    return(status);
  }
} /* End of writeEvent */

/* Write the subevent from the selected sourceID to a raw file */
int32_t writeSubEvent(int32_t evsize, uint32_t *data_buf) {
  int32_t status;
  int32_t headword = 0;

  if ( fd == 0 ) {
    printf("Opening file %s\n",filename);
    fd=open( filename, O_CREAT|O_WRONLY|O_TRUNC, 0x001a4 );
    if(fd<=0) {
      printf("Error opening file %s\n",filename);
      terminate=TRUE;
      return(-1);
    }
  }
  write( fd, &headword, 4 ); // SLINK start word
  status=write( fd, data_buf, evsize );
  headword = 0xcfed1200;
  write( fd, &headword, 4 ); // SLINK end word
  if ( !briefOutput )
    printf("Wrote %d/%d bytes to %s\n",status,evsize,filename);
  if(status==evsize) {
    return(0);
  } else {
    return(status);
  }
} /* End of writeSubEvent */

/* Decode the event we have received */
void decodeEvent() {
  uint8_t *ptr = dataBuffer;
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;

  if ( TEST_SYSTEM_ATTRIBUTE( ev->eventTypeAttribute, ATTR_SUPER_EVENT ) ) {
    /* Superevent */
    int32_t totSize = ev->eventHeadSize;
    int32_t dataSize = ev->eventSize;
 
    gotSuperEvents = TRUE;
    printf( "===============================================================================\n" );
    dumpHeader( ev, TRUE );
    
    while ( totSize != dataSize ) {
      ptr = &dataBuffer[ totSize ];
      ev = (struct eventHeaderStruct *)ptr;
      if (srcIDspecific == FALSE) {
	printf( "...............................................................................\n" );
      }
      if ( dumpHeader( ev, TRUE ) ) dumpData( ev );
      else return;
      
      totSize += ev->eventSize;
    }
  } else {
    /* Normal event */
    if ( gotSuperEvents )
      {
	printf( "===============================================================================\n" );
      }
    else
      {
	if (srcIDspecific == FALSE) {
	  printf( "...............................................................................\n" );
	}
      }
    if ( dumpHeader( ev, TRUE ) )  dumpData( ev );
  }

} /* End of decodeEvent */


/* Check the various numbers to see if it is time to start the output */
int32_t checkNumbers() {
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;

  if ( startSerialNbLoaded )
    if ( numEvents == startSerialNb )
      return TRUE;
  // FNdebug:   printf("\nCheckNumbers: BurstNb = %i \n",EVENT_ID_GET_BURST_NB( ev ->eventId ));
  // FNdebug:  printf("\nCheckNumbers: NbInBurst = %i \n",EVENT_ID_GET_NB_IN_BURST( ev ->eventId ));
  if ( startBurstNbLoaded ) // FN added
    if ( EVENT_ID_GET_BURST_NB( ev ->eventId ) >= startBurstNb ) // FN changed from == to >=
      return TRUE;
  if ( startNbInBurstLoaded ) // FN addedchanged from == to >=
    if ( EVENT_ID_GET_NB_IN_BURST( ev ->eventId ) >= startNbInBurst ) // FN changed from == to >=
      return TRUE;
  if ( startBurstNbNbInBurstLoaded  )  // FN added
    if (EVENT_ID_GET_BURST_NB( ev ->eventId ) >= startBurstNb && EVENT_ID_GET_NB_IN_BURST( ev ->eventId ) >= startNbInBurst )
    return TRUE; 
  if ( startBunchCrossingLoaded )
    if ( EVENT_ID_GET_BUNCH_CROSSING( ev->eventId ) == startBunchCrossing )
      return TRUE;

  if ( startOrbitLoaded )
    if ( EVENT_ID_GET_ORBIT( ev->eventId ) == startOrbit )
      return TRUE;

  if ( startEventIdLoaded )
    if ( EQ_EVENT_ID( ev->eventId, startEventId ) )
	 return TRUE;
	 
  return FALSE;
} /* End of checkNumbers */


/* The main monitor loop */
void monitoringLoop() {
  int32_t  status;
  int32_t  timeOfLastPrint = -1;
  int32_t  outCtr = 0;
  char outFlg[] = { '|', '/', '-', '\\', 0 };
  printf( "\n" );
  
  terminate = FALSE;
  do {
    status = getNextEvent();
    numEvents++;
    if ( status == MON_ERR_EOF ) {
      /* End-of-file: terminate the monitoring loop */
      terminate = TRUE;
      
    } else if ( status != 0 ) {
      /* Error: print the corresponding message */
      fprintf( stderr,
	       "Error getting next event: %s\n",
	       monitorDecodeError( status ) );
      
    } else {
      /* Event received OK */
      if ( !doOutput ) doOutput = checkNumbers();
      if ( doOutput ) {
	if ( noOutput ) {
	  /* No output: print a "progress" cursor */
	  int32_t now = (int32_t)time( NULL );
	  if ( timeOfLastPrint == -1 ) timeOfLastPrint = now;
	  if ( timeOfLastPrint != -1 ) {
	    if ( now - timeOfLastPrint >= 1 ) {
	      timeOfLastPrint = now;
	      printf( "\r%c", outFlg[ outCtr++ ] );
	      if ( outFlg[ outCtr ] == 0 ) outCtr = 0;
	      fflush( stdout );
	    }
	  }
	} else if ( wfile ) {
	  if ( writeEvent() ) {
	    terminate = TRUE;
	    break;
	  }
	} else {
	  /* Normal output: print the content of the event */
	  decodeEvent();
	  if ( interactive ) promptUser();
	}
      }
      
      /* If Dynamic mode, free the data buffer and invalidate the pointer */
      if ( !useStatic ) {
	free( dataBuffer );
	dataBuffer = NULL;
      }
    }
    
    if( maxGetEvents > 0 && numEvents >= maxGetEvents )
      terminate = TRUE;

    if ( !interactive )	/* Terminate on error (unless interactive) */
      terminate = terminate | ( numErrors != 0 );
    
  } while ( status == 0 && !terminate );
  if ( noOutput ) printf( "\n" );
  if ( (wfile || wfile_id) && fd != 0 ) {	// SS
    status=close(fd);
  }
} /* End of monitoringLoop */


/* Our main */
int main( int argc, char **argv ) {
  char PID[32];
  sprintf(PID,"pid: %u",getpid());
  setenv("DATE_SITE",PID,1);
  checkConfig();
  initVars();
  handleArgs( argc, argv );
  monitorSetup( argv[0] );
  monitoringLoop();
  if ( checkData && numErrors != 0 )
    fprintf( stderr,
	     "ERROR%s: %d\n", numErrors != 1 ? "s" : "", numErrors );
  fprintf(stdout,"End of file\n");
  return 0;
} /* End of main */

