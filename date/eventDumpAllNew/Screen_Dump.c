/*
  void Screen_Dump( int32_t, int32_t *); 

  Version: 4.5.2001 FHH
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
/*
  #include <unistd.h>
  #include <signal.h>
  #include <fcntl.h>
  #include <sys/types.h>
  #include <sys/time.h>
  #include <time.h>
  #include <sys/mman.h>
  #include <sys/ioctl.h>
*/
#pragma GCC diagnostic ignored "-Wwrite-strings"


#define min(x,y) ((x)>(y)?(y):(x))
#define max(x,y) ((x)>(y)?(x):(y))

#ifndef ABS
#define ABS(a) (((a)<0)?(-(a)):(a))
#endif

static uint32_t COMPUTE(uint32_t crc, uint8_t ch)
{
  static const uint32_t crctab[] = {
    0x0,
    0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
    0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6,
    0x2b4bcb61, 0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd,
    0x4c11db70, 0x48d0c6c7, 0x4593e01e, 0x4152fda9, 0x5f15adac,
    0x5bd4b01b, 0x569796c2, 0x52568b75, 0x6a1936c8, 0x6ed82b7f,
    0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3, 0x709f7b7a,
    0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
    0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58,
    0xbaea46ef, 0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033,
    0xa4ad16ea, 0xa06c0b5d, 0xd4326d90, 0xd0f37027, 0xddb056fe,
    0xd9714b49, 0xc7361b4c, 0xc3f706fb, 0xceb42022, 0xca753d95,
    0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1, 0xe13ef6f4,
    0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
    0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5,
    0x2ac12072, 0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16,
    0x018aeb13, 0x054bf6a4, 0x0808d07d, 0x0cc9cdca, 0x7897ab07,
    0x7c56b6b0, 0x71159069, 0x75d48dde, 0x6b93dddb, 0x6f52c06c,
    0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08, 0x571d7dd1,
    0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
    0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b,
    0xbb60adfc, 0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698,
    0x832f1041, 0x87ee0df6, 0x99a95df3, 0x9d684044, 0x902b669d,
    0x94ea7b2a, 0xe0b41de7, 0xe4750050, 0xe9362689, 0xedf73b3e,
    0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2, 0xc6bcf05f,
    0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
    0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80,
    0x644fc637, 0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb,
    0x4f040d56, 0x4bc510e1, 0x46863638, 0x42472b8f, 0x5c007b8a,
    0x58c1663d, 0x558240e4, 0x51435d53, 0x251d3b9e, 0x21dc2629,
    0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5, 0x3f9b762c,
    0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
    0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e,
    0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65,
    0xeba91bbc, 0xef68060b, 0xd727bbb6, 0xd3e6a601, 0xdea580d8,
    0xda649d6f, 0xc423cd6a, 0xc0e2d0dd, 0xcda1f604, 0xc960ebb3,
    0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7, 0xae3afba2,
    0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
    0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74,
    0x857130c3, 0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640,
    0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b, 0x43cdc09c, 0x7b827d21,
    0x7f436096, 0x7200464f, 0x76c15bf8, 0x68860bfd, 0x6c47164a,
    0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e, 0x18197087,
    0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
    0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d,
    0x2056cd3a, 0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce,
    0xcc2b1d17, 0xc8ea00a0, 0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb,
    0xdbee767c, 0xe3a1cbc1, 0xe760d676, 0xea23f0af, 0xeee2ed18,
    0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4, 0x89b8fd09,
    0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
    0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf,
    0xa2f33668, 0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
  };
  crc = (crc << 8) ^ crctab[((crc>>24) ^ ch) & 0xff];
  return crc;
}

static uint32_t CheckSum(uint32_t init_val, uint32_t data)
{
  uint8_t* p = (uint8_t*)(&data);
  uint32_t crc32 = init_val;
  for(int i = 3; i >= 0; i--)
    {
      crc32 = COMPUTE(crc32,p[i]);
    }
  return crc32;
}
  
int32_t nrh=0;
int32_t nrd=0;

void Screen_Dump(int32_t evsize, uint32_t *data_buf) {
  int32_t dsize;
  int32_t ctrl_beg,ctrl_end;
  int32_t err;
  int32_t dblock=1;
  int32_t frame_sz;
  int32_t G_baseline;
  int32_t G_maxAmp;
  int32_t frame_cnt=0;
  int32_t ev_type;
  int32_t src_id;
  int32_t ev_size;
  int32_t stat;
  int32_t spill_nr;
  int32_t event_nr;
  int32_t format_nr, nr_err, tcs_err, status_nr;
  int32_t bit[7];
  int32_t trg_time,setup,chip_adr,chn_adr,mes_time,geo_id;
  int32_t j,i,k;
  int32_t head, data;
  int32_t undecode_data;
  int32_t scount=0;
  int32_t tdc_error=0;
  int32_t adc_size, apv_size, apv_format;
  int32_t filter_size,filter_i,filter_type;
  int32_t ram_info, ram_size;
  double sum;
  double dtime,stime;
  time_t t; int32_t n_fiadc;
  char* filtertext[7] = {"No Filter ?   ",
                         "GenTOC        ","GenTriggerTime",
                         "BMS filter    ","SciFi filter  ",
			 "Silicon filter","undefined     "};
  char* filtert_text[7] = {"undefined ","decisions ","calibrat. ",
			   "versions  ","configurat","miss. evts",
			   "undefined "};
  char* filter_evt_text[8] = {"undefined ","missing   ","discarded ",
			      "not writt.","no physics","too big   ",
			      "rejected  ","undefined "};
  char* etext[4] = {"         ","TBO      ","FIFO Full","Hotl.Err."};
  char* etextg[16] = 
    {"ID wrong or         ","data/setup:no header","timeout (no data)   ",
     "event# low          ","no CMC connected    ","data overfl/no trail",
     "FIFO full (port off)","event# higher       ","undefined error 8   ",
     "undefined error 9   ","hotlink error       ","undefined error 11  ",
     "data skipped (trail)","error 13            ","error 14            ",
     "error 15            "};


  
  /* Dump data to the screen */
  dsize=0;
  i=0;
  head=0;
  data=0;
  filter_type=-1;
  filter_size=-1;
  filter_i=0;
  ram_info=0; /* first event of run contains data from RAM */
  printf("\n");
  printf("SLINK Header: %d Words\n",evsize/4);
  /* check for s-link multiplexer */
  src_id = ((data_buf[0] >>16) & ((1<< 10)-1));
  //      printf("FNdebug: Screen_Dump:  srcID: %d \n", src_id  );
  //FN     if ((src_id >= 0x380) & (src_id < 1024)) {
  if ((src_id >= 0x380) && (src_id < 1024)) {
    //      printf("FNdebug: Screen_Dump:  s-link multiplexer. YES\n" );
    err = ((data_buf[0] >>31) & ((1<< 1)-1));
    ev_type = ((data_buf[0] >>26) & ((1<< 5)-1));
    src_id = ((data_buf[0] >>16) & ((1<< 10)-1));
    ev_size = ((data_buf[0] >>0) & ((1<< 16)-1));
    stat = ((data_buf[1] >>31) & ((1<< 1)-1));
    spill_nr = ((data_buf[1] >>20) & ((1<< 11)-1));
    event_nr = ((data_buf[1] >>0) & ((1<< 20)-1));
    if (evsize/4 >= 1)
      printf("\n");
    printf("S-Link multiplexer: Warning data not yet correctly printed!\n");
    printf("  0: %08x  Error: %d, Event Type: 0x%02x, Source ID: %4i, Size: %d\n",data_buf[0],err,ev_type,src_id,ev_size);
    if (evsize/4 >= 2)
      printf("  1: %08x  Status: %d, Spill Number: %d, Event Number: %d\n",data_buf[1],stat,spill_nr,event_nr);
    i += 2;dsize+=8;
  }


  err =     ((data_buf[i] >> 31)   & ((1 << 1) -1));
  ev_type = ((data_buf[i] >> 26)   & ((1 << 5) -1));
  src_id =  ((data_buf[i] >> 16)   & ((1 << 10)-1));
  ev_size = ((data_buf[i] >> 0)    & ((1 << 16)-1));
  stat =    ((data_buf[i+1] >> 31) & ((1 << 1) -1));
  spill_nr =((data_buf[i+1] >> 20) & ((1 << 11)-1));
  event_nr =((data_buf[i+1] >> 0)  & ((1 << 20)-1));
  format_nr=((data_buf[i+2] >> 24) & ((1 << 8) -1));
  nr_err   =((data_buf[i+2] >> 16) & ((1 << 8) -1));
  tcs_err  =((data_buf[i+2] >> 8)  & ((1 << 8) -1));
  status_nr=((data_buf[i+2] >> 0)  & ((1 << 8)-1));
  if (evsize/4 >= i) {
    printf("\n");
    printf("%3d: %08x  Error: %d, Event Type: 0x%02x, Source ID: %4i, Size: %d\n",i,data_buf[i],err,ev_type,src_id,ev_size); 
  }
  i++;dsize+=4;
  if (evsize/4 >= i) {
    printf("%3d: %08x  Status: %d, Spill Number: %d, Event Number: %d\n",i,data_buf[i],stat,spill_nr,event_nr);
  }
  i++;dsize+=4;
  if (evsize/4 >= i) {
    printf("%3d: %08x  Format: 0x%02x, #Err: %3d, TCS-Err: 0x%02x, Status: 0x%02x",i,data_buf[i],format_nr,nr_err,tcs_err,status_nr);
  }
  i++;dsize+=4;
  printf("\n");

  // NA64: check for "FARO" ADC
  const bool isNewAdc = (src_id >= 2 && src_id <= 5);
  
  if (isNewAdc || ((format_nr != 0x60) && ((((format_nr>>4) & 7) != 0)||((format_nr&1) == 1)))) { /* non APV format */
    if (format_nr & 0x80) {
      printf("%3d: %08x  CATCH S/N=%8d #CMC=%02i",i,data_buf[i],data_buf[i]>>16,(data_buf[i]>>12)&0xF);
      ram_info=data_buf[i]&0x1;
      printf(" %s\n",ram_info?"RAM data in this event":" ");
      i++;dsize+=4;
      printf("%3d: %08x  FPGA: M=%3i, F=%3i, S=%3i, T=%3i\n",i,data_buf[i],data_buf[i]>>24,(data_buf[i]>>16)&0xFF,(data_buf[i]>>8)&0xFF,data_buf[i]&0xFF);
      i++;dsize+=4;
      printf("%3d: %08x  CMC 1 S/N %04i  CMC 2 S/N %04i\n",i, data_buf[i], (data_buf[i]>>16)&0xFFFF, data_buf[i]&0xFFFF);
      i++;dsize+=4;
      printf("%3d: %08x  CMC 3 S/N %04i  CMC 4 S/N %04i\n",i, data_buf[i], (data_buf[i]>>16)&0xFFFF, data_buf[i]&0xFFFF);
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      printf("\n");
    }

    printf("SLINK Data:\n\n");

    scount = 0; /* scaler counter */

    if (ram_info==1) { /* this first of run event contains data from RAM 
			  e.g. Pedestals, TDC registers,... */
      ram_size=((data_buf[i]&0xFFFF)+i)*4;
      printf("%3d: %08x  RAM version=%6i RAM words=%6i\n",i, data_buf[i],(data_buf[i]>>16)&0xFFFF,(data_buf[i]&0xFFFF));
      i++;dsize+=4;
      if ((format_nr &0x1) ==1) { /******* TDC type FEOR ************/
	while ((dsize<min(evsize,ram_size))&& (((data_buf[i]>>16)&0xFFFF)!=0xFFFF)) {
	  printf("%3d: %08x  all=%1i port=%2i TDC=%1i all=%1i register= %2i value= 0x%04x\n", i, data_buf[i],
		 ((data_buf[i] >> 28) & ((1 << 1) -1)),
		 ((data_buf[i] >> 24) & ((1 << 4) -1)),
		 ((data_buf[i] >> 21) & ((1 << 3) -1)),
		 ((data_buf[i] >> 20) & ((1 << 1) -1)),
		 ((data_buf[i] >> 16) & ((1 << 4) -1)),
		 ((data_buf[i] >>  0) & ((1 <<16) -1)));
	  i++;dsize+=4;
	}
      } else { /**** ADC type FEOR *************/
	t = data_buf[i];
	printf("%3d: %08x  Pedestals taken: %s\n", i, data_buf[i],ctime(&t));
	i++;dsize+=4;
	n_fiadc=data_buf[i];
	printf("%3d: %08x  Number of FIADC modules = %12i\n", i, data_buf[i],data_buf[i]);
	i++;dsize+=4;
	while ((n_fiadc>0) &&(dsize<min(evsize,ram_size))&& (((data_buf[i]>>16)&0xFFFF)!=0xFFFF)) {
	  printf("%3d: %08x  delta=%7i  FIADC=%7i\n", i, data_buf[i],
		 ((data_buf[i] >> 16) & ((1<<16)-1)),
		 ((data_buf[i] >>  0) & ((1<<16)-1)));
	  i++;dsize+=4;
	  k=1;
	  while ((k<64) &&(dsize<min(evsize,ram_size))&& (((data_buf[i]>>16)&0xFFFF)!=0xFFFF)) {
	    printf("%3d: %08x  pedestal[%2i]=%7i  pedestal[%2i]=%7i\n", i, data_buf[i],
		   k, ((data_buf[i] >> 16) & ((1<<16)-1)),
		   k+1,((data_buf[i] >> 0) & ((1<<16)-1)));
	    i++;dsize+=4;
	    k+=2;
	  }
	  
	  n_fiadc--;
	}
      }      
      if (((data_buf[i]>>16)&0xFFFF)==0xFFFF){
	printf("%3d: %08x  End RAM data,  error=0x%04x\n",i, data_buf[i],(data_buf[i]&0xFFFF));
	i++;dsize+=4;
      }
    }

    /**** decoding of RICH first events *********/

    if ((format_nr ==0xd0)||((format_nr==0x50)&&(event_nr==1))) { /******* RICH type FEOR ************/
      k=0;
      while(dsize<evsize) 
	{
	  printf("%3d: %08x ",i, data_buf[i]);
	  bit[6]=((data_buf[i] >> 31) & ((1 << 1) -1));
	  bit[5]=((data_buf[i] >> 30) & ((1 << 1) -1));
	  if (!bit[6]) {
	    if ( ((data_buf[i] >> 20) & ((1 << 12) -1) ) == 0x7FF) 
	      { /* error word */
		printf(" Error: %s, %s geo.id=%03x        port=%2i",
		       etextg[ (data_buf[i] >> 0) & ((1 << 4) -1) ],
		       etext[ (data_buf[i] >> 4) & ((1<<2)-1) ],
		       (data_buf[i] >> 10) & ((1<<10)-1),
		       (data_buf[i] >>  6) & ((1<<4)-1));
	      }
	    else
	      {
		head+=1;
		if (!bit[5]) {
		  printf(" Header ");
		  k=0;
		}else {
		  printf(" Trailer");
		  k=0;
		}
		printf(" geographic id= %d, event= %d",(data_buf[i]>>20)&0x3FF,data_buf[i]&0xFFFFF);
	      }
	  }
	  else {
	    data+=1;
	    if (k<=4) {
	      printf(" Voltage[%1i]        =%4i",k,(data_buf[i]>>0)&0xFF);
	      printf("  approx.  %7.1f V",((data_buf[i]>>0)&0xFF)/256.*3.3);
	    } else if (k<=8) {
	      printf(" Temperature[%1i]    =%4i C",k-5,(data_buf[i]>>0)&0xFF);
	    } else if (k<=9) {
	      printf(" Burst counter=%9i",(data_buf[i]>>0)&0xFFFFFF);
	    } else if (k<=10) {
	      printf(" Event counter=%9i",(data_buf[i]>>0)&0xFFFFFF);
	    } else if (k<(11+144)) {
	      printf(" Tresh[%3i]=%5i  ",(k-11)*4+2,(data_buf[i]>>0)&((1<<10)-1));
	      printf(" Tresh[%3i]=%5i  ",(k-11)*4+1,(data_buf[i]>>10)&((1<<10)-1));
	      printf(" Tresh[%3i]=%5i",(k-11)*4+0,(data_buf[i]>>20)&((1<<10)-1));
	    } else if (k<11+144+864) {
	      if ((k-11-144+1)%2) {
		sum=(data_buf[i]>>0)&((1<<21)-1);
		printf(" Channel= %4i  sum =   %8i  pedestal = %10.2f",(data_buf[i]>>21)&((1<<10)-1),(data_buf[i]>>0)&((1<<21)-1),((data_buf[i]>>0)&((1<<21)-1))/2048.);
	      } else {
		//printf("              sum^2 =%11i  noise =    %10.2f",(data_buf[i]>>0)&(0x7FFFFFFF),sqrt(fabs(((data_buf[i]&0x7FFFFFFF)-(sum/2048.)*sum)/2047.)) );
	      }

	    }
	    k++;
	  }
	  dsize+=4;
	  i+=1;
	  printf("\n");
	}
    }  /* END RICH first event of run */

    /******** end of decoding first event of run ********************/

    while(dsize<evsize) 
      {
	
	printf("%3d: %08x ",i, data_buf[i]);
	bit[6]=((data_buf[i] >>31) & ((1<< 1)-1));
	bit[5]=((data_buf[i] >>30) & ((1<< 1)-1));
	/*      switch (format_nr & 0x7F) { */
	switch (format_nr & 0x1) {
	case 0:
	  /* 32 bit data */

	  switch ((format_nr>>4) & 7) {
	  case 1: /*GANDALF GTA DATA (FH)*/

			switch (format_nr){

			case 24: /*normal data mode*/
			if (dblock == 1){
			printf("ch: %d, 	baseline: %d,		integral: %d\n", 
					(data_buf[i] >> 27) & ((1 << 4) -1),
					(data_buf[i] >> 16) & ((1 << 11) -1),
					(data_buf[i] >> 0) & ((1 << 16) -1));
			}
			if (dblock ==2){
			 printf("c_t MSB: %d, 	max Amp: %d\n",
                                        (data_buf[i] >> 14) & ((1 << 17) -1),
                                        (data_buf[i] >> 0) & ((1 << 14) -1));
			}			
			if (dblock ==3){
			dblock=0;
                         printf("c_t LSB: %d, 	hr_t: %d\n",
                                        (data_buf[i] >> 10) & ((1 << 21) -1),
                                        (data_buf[i] >> 0) & ((1 << 10) -1));
			}
			dblock++;
			break;


			case 28: /*debug data mode*/
			if (!bit[6]) {/*Header Trailer*/
				if (!bit[5]) {
                                printf("Header  ");
				frame_sz=(data_buf[i] >> 4) & ((1 << 11) -1);
				printf("evt_no: %d, ch: %d, SysMon: %d, Framesize: %d, RDM: %x\n",
					(data_buf[i] >> 24) & ((1 << 6) -1),
					(data_buf[i] >> 20) & ((1 << 4) -1),
					(data_buf[i] >> 15) & ((1 << 5) -1),
                                        frame_sz,
                                        (data_buf[i] >> 0) & ((1 << 4 ) -1));
					dblock=frame_sz+4;
                                }
                                else {
				frame_cnt=0;
                                printf("Trailer ");
                                 printf("evt_no: %d, ch: %d, SysMon: %d, Framesize: %d, RDM: %x\n",
                                        (data_buf[i] >> 24) & ((1 << 6) -1),
                                        (data_buf[i] >> 20) & ((1 << 4) -1),
                                        (data_buf[i] >> 15) & ((1 << 5) -1),
                                        frame_sz,
                                        (data_buf[i] >> 0) & ((1 << 4 ) -1));
				}

			}
			else { /*data*/
				 if (dblock > 4){
					printf("Data ");
					printf("ADC%d: %d,   ADC%d: %d\n",
					2*frame_cnt,
                                        (data_buf[i] >> 16) & ((1 << 14) -1),
					2*frame_cnt+1,
                                        (data_buf[i] >> 0) & ((1 << 14) -1) );
					frame_cnt++;					
				}
				
				if (dblock == 4){
				//frame_cnt=0;
				printf("Data ");
				G_baseline=(data_buf[i] >> 16) & ((1 << 11) -1);
	                        printf("ch: %d,		baseline: %d,	integral: %d\n",
                                        (data_buf[i] >> 27) & ((1 << 4) -1),
                                       	G_baseline,
                                        (data_buf[i] >> 0) & ((1 << 16) -1) );
				}
                        	if (dblock ==3){
                         	printf("Data ");
				G_maxAmp=(data_buf[i] >> 0) & ((1 << 14) -1);
				printf("c_t MSB: %d,   max Amp: %d, +Baseline: %d\n",
                                        (data_buf[i] >> 14) & ((1 << 17) -1),
                                        G_maxAmp,
					G_maxAmp + G_baseline);
                        	}
                        	if (dblock ==2){
				printf("Data ");
                         	printf("c_t LSB: %d,   hr_t: %d\n",
                                        (data_buf[i] >> 10) & ((1 << 21) -1),
                                        (data_buf[i] >> 0) & ((1 << 10) -1));
                        	}
				 if (dblock ==1){
                                printf("Data ");
				dblock=0;
                                printf("threshold: %d, fraction: %d, delay: %d, frame_t: %d\n",
					(data_buf[i] >> 23) & ((1 << 8) -1),
					(data_buf[i] >> 17) & ((1 << 6) -1),
                                        (data_buf[i] >> 12) & ((1 << 5) -1),
                                        (data_buf[i] >> 0) & ((1 << 12) -1));
                                }
                        	dblock--;
			}
			break;

			default:
			  if(src_id == 432) // RW data
			    {
			      if((data_buf[i]&0x80000000)==0)
				{
				  printf("RW %s",(data_buf[i]&0x40000000)==0?"header: ":"trailer:");
				  printf(" geo: %u, event %u\n", (data_buf[i]&0x00F00000)>>20,(data_buf[i]&0x000FFFFF));
				}
			      else
				{
				  printf("RW    data: port: %u, dflag: %u, zero: %u, channel: %02u, data: %04x",
					 (data_buf[i]&0x07000000)>>24, (data_buf[i]&0x00800000)>>23, (data_buf[i]&0x00400000)>>22,
					 (data_buf[i]&0x003F0000)>>16,(data_buf[i]&0x0000FFFF));
				  if((data_buf[i]&0x00800000)!=0 && (data_buf[i]&0x00400000)==0)
				    printf("\n");
				  else
				    printf(" <-- F1 data error!\n");
				}
			    }
			  else
			    printf("GANDALF ADC data, unkown data format\n");
			break;
			}
	   break;

	   case 2: /* online trigger */
	    /* check for begin of filter block */
	    if (filter_i>filter_size) {
	      /* new block: decode header */
	      filter_size = ((data_buf[i] >>  0) & ((1 << 16) -1)/4);
	      filter_type = ((data_buf[i] >> 16) & ((1 <<  8) -1));
	      filter_i=1;
	      printf(" Mon=%01x acc=%01x reserved=%02x type=%02x=%s size=%6i bytes\n",
		     ((data_buf[i] >> 31) & ((1 << 1) -1)),
		     ((data_buf[i] >> 30) & ((1 << 1) -1)),
		     ((data_buf[i] >> 24) & ((1 << 6) -1)),
		     ((data_buf[i] >> 16) & ((1 << 8) -1)),
		     filtert_text[min(filter_type,6)],
		     ((data_buf[i] >>  0) & ((1 << 16) -1)));
	      filter_i++;
	    } else {
	      switch (filter_type) {
	      case 1:
		if ( ((data_buf[i] >> 0) & ((1<< 8)-1)) > 5 ) {
		  printf("   Acc=%01x dec=%01x time=%6i us reserved=%02x filter id=%02x",
			 ((data_buf[i] >> 31) & ((1 << 1) -1)),
			 ((data_buf[i] >> 30) & ((1 << 1) -1)),
			 ((data_buf[i] >> 16) & ((1 <<14) -1)) *8,
			 ((data_buf[i] >>  8) & ((1 << 8) -1)),
			 ((data_buf[i] >>  0) & ((1 << 8) -1))); 
		} else {
		  printf("   Acc=%01x dec=%01x time=%6i us reserved=%02x %s",
			 ((data_buf[i] >> 31) & ((1 << 1) -1)),
			 ((data_buf[i] >> 30) & ((1 << 1) -1)),
			 ((data_buf[i] >> 16) & ((1 <<14) -1)) *8,
			 ((data_buf[i] >>  8) & ((1 << 8) -1)),
			 filtertext[ ((data_buf[i] >> 0) & ((1<< 8)-1))]); 
		};
		break;
	      case 2: /* t0 calibration */ break; 
	      case 3: /* filter version */ break;
	      case 4: /* filter configuration */ break;
	      case 5: /* missing events */ 
		printf("   %i events %s",data_buf[i],
		       filter_evt_text[min(filter_i-1,7)]);
		break;
	      default: break;
	      }
	      printf("\n");
	      filter_i++;
	    }
	    break;


	  case 3: /* scaler */
	    if (!bit[6]) {
	      if ( ((data_buf[i] >> 20) & ((1<<12)-1)) == 0x7FF ) 
		{ /* error word */
		  printf(" Error: %s, %s geo.id=%03x        port=%2i",
			 etextg[(data_buf[i] >>0) & ((1<<4)-1)],
			 etext[(data_buf[i] >>4) & ((1<<2)-1)],
			 (data_buf[i] >>10) & ((1<<10)-1),
			 (data_buf[i] >>6) & ((1<<4)-1));
		}
	      else
		{
		  head+=1;
		  scount=0;
		  if (!bit[5]) {
		    printf(" Header ");
		  }else {
		    printf(" Trailer");
		  }
		  printf(" geographic id= %d, event= %d",(data_buf[i]>>20)&0x3FF,data_buf[i]&0xFFFFF);
		}
	    }
	    else {
	      data+=1;
	      if (scount<=31) 
		printf(" Scaler %3i = %10i",scount,data_buf[i]&0x7FFFFFFF);
	      if (scount==32) {
		printf(" Scaler LSB time = %5i, pattern = 0x%04X",(data_buf[i]>>16)&0x7FFF,data_buf[i]&0xFFFF);
		stime = (data_buf[i]>>16)&0x7FFF;
	      }
	      if (scount==33) {
		dtime = stime + (((data_buf[i]>>16)&0x7FFF)<<15);
		dtime = dtime /38.88/1000;
		printf(" Scaler MSB time = %5i, pattern = 0x%04X time =%13.6f ms ",(data_buf[i]>>16)&0x7FFF,data_buf[i]&0xFFFF, dtime);
	      }
	      if (scount>33) 
		printf(" Scaler (error) = %10i",data_buf[i]&0x7FFFFFFF);
	      scount++;
	    }
	    printf("\n");
	    break;
	  case 4: /* FI-ADC */
	    if (!bit[6]) {
	      if ( ((data_buf[i] >> 20) & ((1<<12)-1)) == 0x7FF) 
		{ /* error word */
		  printf(" Error: %s, %s geo.id=%03x        port=%2i",
			 etextg[(data_buf[i] >>0) & ((1<<4)-1)],
			 etext[(data_buf[i] >>4) & ((1<<2)-1)],
			 (data_buf[i] >>10) & ((1<<10)-1),
			 (data_buf[i] >>6) & ((1<<4)-1));
		}
	      else
		{
		  head+=1;
		  if (!bit[5]) {
		    printf(" Header ");
		  }else {
		    printf(" Trailer");
		  }
		  printf(" geographic id= %d, event= %d",(data_buf[i]>>20)&0x3FF,data_buf[i]&0xFFFFF);
		}
	    }
	    else {
	      data+=1;
	      printf(" ADC ");
	      printf("Pedsub=%1i, chan=%2i, ofl=%1i, otr=%1i, val=%4i",bit[5],(data_buf[i]>>14)&0x3F,(data_buf[i]>>13)&1,(data_buf[i]>>12)&1,data_buf[i]&0xFFF);
	    }
	    printf("\n");
	    break;
	  case 5: /* RICH */
	    if (!bit[6]) {
	      if ( ((data_buf[i] >> 20) & ((1 << 12) -1)) == 0x7FF) 
		{ /* error word */
		  printf(" Error: %s, %s geo.id=%03x        port=%2i",
			 etextg[(data_buf[i] >>0) & ((1<<4)-1)],
			 etext[(data_buf[i] >>4) & ((1<<2)-1)],
			 (data_buf[i] >>10) & ((1<<10)-1),
			 (data_buf[i] >>6) & ((1<<4)-1));
		}
	      else
		{
		  head+=1;
		  if (!bit[5]) {
		    printf(" Header ");
		  }else {
		    printf(" Trailer");
		  }
		  printf(" geographic id= %d, event= %d",(data_buf[i]>>20)&0x3FF,data_buf[i]&0xFFFFF);
		}
	    }
	    else {
	      data+=1;
	      printf(" RICH  spare=%1i chamber=%1i bora=%2i channel=%4i ADC=%4i",(data_buf[i]>>28)&7,(data_buf[i]>>25)&7,(data_buf[i]>>20)&0x1F,(data_buf[i]>>10)&0x3FF,(data_buf[i])&0x3FF);
	    }
	    printf("\n");
	    break;
		
	   case 6: /* GANDALF ADC */
	    break;

	  default:
	    printf("\n");
	    break;
	  }
	  break;
      
	case 1:
	  /******************** 24 bit TDC data ******************/

	  if ((((format_nr>>1) & 1) == 0) || (tdc_error==1)) {

	    tdc_error=0; /* if tdc_error in sparsified mode, next word is in debug mode */
	    bit[6] = ((data_buf[i] >> 31) & ((1<< 1)-1));
	    bit[5] = ((data_buf[i] >> 30) & ((1<< 1)-1));
	    if (!bit[6]) 
	      { /* header word */
		head+=1;
		event_nr = ((data_buf[i] >> 24) & ((1 << 6) -1));
		trg_time = ((data_buf[i] >> 15) & ((1 << 9) -1));
		setup =    ((data_buf[i] >> 14) & ((1 << 1) -1));
		chip_adr = ((data_buf[i] >> 11) & ((1 << 3) -1));
		chn_adr =  ((data_buf[i] >>  8) & ((1 << 3) -1));
		for (j=0;j<4;j++) 
		  {
		    bit[j] = ((data_buf[i] >> j) & ((1<< 1)-1));
		  }
		printf(" Header");
		printf(" event# %2d tbo=%1d time= %3d",event_nr,bit[5],trg_time);
		printf(" xor=%1d chip %x / %x ",setup,chip_adr,chn_adr); 

		if (((format_nr>>6)&1)==0) { /* TDC-CMC */
		  if (chip_adr<4 & bit[chip_adr]==1) 
		    printf("locked");
		  else
		    printf("      ");
		} else {
		  printf("      ");
		}
		printf(" port=%2i", (data_buf[i] >> 4) & ((1<< 4)-1));
		printf("\n");
	      } 
	    else if ( ((data_buf[i] >> 20) & ((1 << 12) -1)) == 0xFFF) 
	      { /* error word */
		/*	  printf(" Error: %06x\n",data_buf[i] >>0 & (1<<20)-1); */

		printf(" Error: %s, %s geo.id=%03x        port=%2i\n",
		       etextg[(data_buf[i] >>0) & ((1<<4)-1)],
		       etext[(data_buf[i] >>4) & ((1<<2)-1)],
		       (data_buf[i] >>10) & ((1<<10)-1),
		       (data_buf[i] >>6) & ((1<<4)-1));
	      }
	    else 
	      { /* data word */
		data+=1;
		chip_adr = ((data_buf[i] >> 27) & ((1 << 3) -1));
		chn_adr  = ((data_buf[i] >> 24) & ((1 << 3) -1));
		mes_time = ((data_buf[i] >>  8) & ((1 <<16) -1));
		for (j=0;j<4;j++) 
		  {
		    bit[j] = ((data_buf[i] >> j) & ((1<< 1)-1));
		  }
		printf(" Data ");
		if ((format_nr>>5) & 1) { /* lead & trail */
		  printf("%s",(mes_time&1)?"leading ":"trailing");
		}else{
		  printf("        ");
		}
		printf("    time= %5d ",mes_time);
		if ((format_nr>>3) & 1) { /* latch mode */
		  printf("wire %1s%1s%1s%1s",(mes_time>>3)&1?"3":" ",(mes_time>>2)&1?"2":" ",(mes_time>>1)&1?"1":" ",(mes_time>>0)&1?"0":" ");
		}else{
		  printf("         ");
		}
	      
		printf(" chip %x / %x ",chip_adr,chn_adr);
		if (((format_nr>>6)&1)==0) { /* TDC-CMC */
		  if (chip_adr<4 & bit[chip_adr]==1) 
		    printf("locked");
		  else
		    printf("      ");
		} else {
		  printf("      ");
		}
		printf(" port=%2i", (data_buf[i] >> 4) & ((1 << 4) -1));
		printf("\n");
	      }
	  }


	  else { /* sparsified TDC data */
	    tdc_error=0;
	    if ( ((data_buf[i] >> 20) & ((1<<12)-1)) == 0xFFF) 
	      { /* error word */
		printf(" Error: %s, %s geo.id=%03x        port=%2i\n",
		       etextg[(data_buf[i] >>0) & ((1<<4)-1)],
		       etext[(data_buf[i] >>4) & ((1<<2)-1)],
		       (data_buf[i] >>10) & ((1<<10)-1),
		       (data_buf[i] >>6) & ((1<<4)-1));
		tdc_error=1;
	      }
	    else 
	      { /* data word */
		data+=1;
		geo_id  = ( (data_buf[i] >> 22) & ((1 << 10) -1) );
		chip_adr= ( (data_buf[i] >> 19) & ((1 <<  3) -1) );
		chn_adr = ( (data_buf[i] >> 16) & ((1 <<  3) -1) );
		mes_time= ( (data_buf[i] >>  0) & ((1 << 16) -1) );
		printf(" Data  ");
		printf("           time= %5d       ",mes_time);
		printf("    chip %x / %x ",chip_adr,chn_adr);
		printf(" id=%4i",geo_id);
		printf("\n");
	      }

	  }
	  break;
	default:
	  printf("\n");
	}

	dsize+=4;
	i+=1;

      }
    printf("\n %d Header words, %d Data words\n",head, data);

  } else if (src_id==1) { /* online trigger */
    while (dsize<evsize) {
      printf("%3d: %08x ",i, data_buf[i]);
      printf(" Acc=%01x dec=%01x time=%04x reserved=%02x filter id=%02x\n",
	     ( (data_buf[i] >> 31) & ((1 << 1) -1) ),
	     ( (data_buf[i] >> 30) & ((1 << 1) -1) ),
	     ( (data_buf[i] >> 16) & ((1 <<14) -1) ),
	     ( (data_buf[i] >>  8) & ((1 << 8) -1) ),
	     ( (data_buf[i] >>  9) & ((1 << 8) -1) ));
      dsize+=4;
      i++;
    }
  }
  else if(src_id==630) // FEM SADC module
    {
      while(dsize<evsize)
	{
	  unsigned t = data_buf[i];
	  printf("%3d: %08x ",i, t);
	  unsigned h_sadc   = (t&0xC0000000)>>30; // if it is ADC header then h_adc == 11b
	  unsigned hg_port  = (t&0x38000000)>>27; // number of HG port
	  unsigned zr_id    = (t&0x04000000)>>26; // is_trailer field, must be 0 for header and 1 for the trailer
	  unsigned h_ovl    = (t&0x02000000)>>25; // overflow flag
	  unsigned one_id   = (t&0x01000000)>>24; // must be 1
	  unsigned blk_size = (t&0x00FFF000)>>12; // 12 bits of block size
	  unsigned ev_nr    = (t&0x00000FFF);     // 12 LSB of event number
	  unsigned ADC_ID   = (hg_port<<1)|zr_id;
	  printf("byte header: %u, ADC_ID %2u, overflow: %u, one: %u, blk size: %4u, ev. nr: %4u\n", 
		 h_sadc, ADC_ID, h_ovl, one_id, blk_size, ev_nr);
	  dsize+=4;
	  i++;
	  blk_size--;
	  int k = 0;
	  while(dsize<evsize && blk_size>0)
	    {
	      unsigned t = data_buf[i];
	      printf("%3d: %08x ",k, t);
	      unsigned h_sadc       = (t&0xC0000000)>>30; // 2 MSB must be 01b
	      unsigned ch_ADC_ch    = (t&0x3C000000)>>26; // ADC channel number
	      unsigned ch_zero      = (t&0x02000000)>>25; // zero
	      unsigned ch_nmb_sampl = (t&0x01FF0000)>>16; // number of samples
	      unsigned ch_sum       = (t&0x0000FFFF);     // sum
	      printf("byte header: %u, ADCch: %2u, zero: %u, n.sampl: %4u, SUM: %5u\n",
		     h_sadc, ch_ADC_ch, ch_zero, ch_nmb_sampl, ch_sum);
	      dsize+=4;
	      i++;
	      k++;
	      blk_size--;
	      unsigned n_words = (ch_nmb_sampl+2)/3;
	      while(dsize<evsize && blk_size>0 && n_words>0)
		{
		  unsigned t = data_buf[i];
		  int j = 0;
		  printf("%3d: %08x ",k, t);
		  unsigned h_sadc       = (t&0xC0000000)>>30; // 2MSB must be 01b for first word and 10b for others
		  unsigned smpl_data2   = (t&0x3FF00000)>>20; // data2 of the sample
		  unsigned smpl_data1   = (t&0x000FFC00)>>10; // data1 of the sample
		  unsigned smpl_data0   = (t&0x000003FF);     // data0 of the sample
		  printf("byte header: %u, data2: %4u, data1: %4u, data0: %4u\n",h_sadc,smpl_data2,smpl_data1,smpl_data0);
		  dsize+=4;
		  i++;
		  k++;
		  blk_size--;
		  n_words--;
		}
	    }
	}
    }
  else if((src_id >= 760 && src_id <= 761) /* new TDC for Straw */
     || format_nr == 0x60)
    {
      while(dsize<evsize)
	{
	  printf("%3d: %08x ",i, data_buf[i]);
	  int port = (data_buf[i]&0x000F0000)>>16;
	  int port_size = (data_buf[i]&0xFFFF);
	  printf("/        port = %2d, port_data_size = %d bytes\n", port, port_size*4);
	  dsize+=4;
	  i++;
	  int n = 1;
	  int step = 1;
	  uint64_t coarse_time = 0;
	  uint32_t CRC32  = 0;
	  while(n < port_size && dsize<evsize)
	    {
	      if(n == port_size-1)
		step = 4;
	      printf("%3d: %08x ",i, data_buf[i]);
	      int is_data = (data_buf[i]&0x80000000)>>31;
	      if(!is_data)
		{
		  printf("| ERROR (Data Flag is 0)\n");
		}
	      else if(step == 1)
		{
		  CRC32 = CheckSum(0,data_buf[i]);
		  int event_num = data_buf[i]&0x7FFFFFFF;
		  printf("|    event_nr = %d", event_num);
		  if((event_num&0xFFFFF) != (event_nr&0xFFFFF))
		    printf(" ERROR (Event number mismatch)");
		  printf("\n");
		  step = 2;
		}
	      else if(step == 2)
		{
		  CRC32 = CheckSum(CRC32,data_buf[i]);
		  coarse_time = data_buf[i]&0x7FFFFFFF;
		  coarse_time <<= 4;
		  printf("| coarse_time = 0x%08lx (%ld)\n", coarse_time, coarse_time);
		  step = 3;
		}
	      else if(step == 3)
		{
		  CRC32 = CheckSum(CRC32,data_buf[i]);
		  int channel = (data_buf[i]&0x7F000000)>>24;
		  uint64_t tdc_time = (data_buf[i]&0xFFFFFF);
		  uint64_t time = (coarse_time&0xFFF000000)|tdc_time;
		  if(ABS(coarse_time-time) > 0x7FFF)
		    time = 0x1000000000UL|(coarse_time&0xFFF000000)|tdc_time;
		  printf("|     channel = %2d, tdc_time = 0x%06lx (coarse_time+tdc_time = 0x%09lx (%ld))\n", channel, tdc_time, time, time);
		}
	      else if(step == 4)
		{
		  int crc32 = data_buf[i]&0x7FFFFFFF;
		  printf("\\       CRC32 = 0x%08x (0x%08x)", crc32, CRC32);
		  if(crc32 != (CRC32&0x7fffffff))
		    printf(" ERROR checksum");
		  printf("\n");
		}
	      dsize+=4;
	      i++;
	      n++;
	    }
	}
    }
  else if(src_id >= 380 && src_id <= 384) ///* MM APV chip decode *///
    {
      while(dsize<evsize)
	{
	  printf("%3d: %08x ", i, data_buf[i]);
	  adc_size = (data_buf[i]&0xFFFF);
	  uint32_t adc_id = (data_buf[i]&0xFF0000)>>16;
	  uint32_t data_format = (data_buf[i]&0xFF000000)>>24;
	  printf(" Format = 0x%02x,  ADC ID: %3u, Size = %5u (0x%04x)\n", data_format, adc_id, adc_size, adc_size);
	  dsize+=4;
	  i++;
	  printf("%3d: %08x ",i, data_buf[i]);
	  uint32_t time_tag = data_buf[i]&0xFFFFFF;
	  uint32_t format_new = (data_buf[i]&0xFF000000)>>24;
	  printf(" Format_new = 0x%02x,  time_tag = %10u (0x%06x)\n", format_new, time_tag, time_tag);
	  bool isSingleFrameReadout = ((format_new&0x01)==0);
	  bool isTripleFrameReadout = ((format_new&0x01)!=0);
	  bool isLatchAll           = ((format_new&0x02)==0);
	  bool isZeroSuppression    = ((format_new&0x02)!=0);
	  bool isNoPedestalSubstr   = ((format_new&0x04)==0);
	  bool isPedestalSubstr     = ((format_new&0x04)!=0);
	  bool isNoCommonMode       = ((format_new&0x08)==0);
	  bool isCommonMode         = ((format_new&0x08)!=0);
	  bool isReoderingOff       = ((format_new&0x10)==0);
	  bool isReoderingOn        = ((format_new&0x10)!=0);
	  bool isAllDataReadout     = ((format_new&0x20)==0);
	  bool isHeaderOnly         = ((format_new&0x20)!=0);
	  bool isAdcVer1_0          = ((format_new&0x40)==0);
	  bool isAdcVer2_0          = ((format_new&0x40)!=0);
	  dsize+=4;
	  i++;
	  adc_size-=2;
	  while(dsize<evsize && adc_size>0)
	    {
	      printf("%3d: %08x ",i, data_buf[i]);
	      apv_size = data_buf[i]&0xFFFF;
	      uint32_t event_num = (data_buf[i]&0xFFF0000)>>16;
	      uint32_t chip_id   = (data_buf[i]&0xF0000000)>>28;
	      printf("    Chip ID: %2u, event_num = %4u (0x%03x), data_len = %5u (0x%04x)\n", chip_id,event_num,event_num, apv_size, apv_size);
	      dsize+=4;
	      i++;
	      printf("%3d: %08x ",i, data_buf[i]);
	      bool isFrame1_good    = ((data_buf[i]&0x00000001)!=0);
	      bool isFrame2_good    = ((data_buf[i]&0x00000200)!=0);
	      bool isFrame3_good    = ((data_buf[i]&0x00040000)!=0);
	      uint32_t reserved0    = ((data_buf[i]&0x18000000)>>27);
	      bool isAPVSync        = ((data_buf[i]&0x20000000)!=0);
	      bool isGlobalSync     = ((data_buf[i]&0x40000000)!=0);
	      uint32_t reserved1    = ((data_buf[i]&0x80000000)>>31);
	      uint32_t frame1_gaddr = ((data_buf[i]&0x000001fe)>>1);
	      uint32_t frame2_gaddr = ((data_buf[i]&0x0003fc00)>>10);
	      uint32_t frame3_gaddr = ((data_buf[i]&0x07f80000)>>19);
	      printf("        one: %u, global sync: %c, APV sync: %c, zero: 0x%02x, gaddr3: 0x%02x (%s), gaddr2: 0x%02x (%s), gaddr3: 0x%02x (%s)\n",
		     reserved1, isGlobalSync?'Y':'N', isAPVSync?'Y':'N', reserved0, frame3_gaddr, isFrame3_good?"good":"bad",
		     frame2_gaddr, isFrame2_good?"good":"bad", frame1_gaddr, isFrame1_good?"good":"bad");
	      dsize+=4;
	      i++;
	      adc_size-=2;
	      apv_size-=2;
	      while(dsize<evsize && apv_size>0)
		{
		  printf("%3d: %08x ",i, data_buf[i]);
		  if(isZeroSuppression)
		    {
		      printf("       Sparse  chan=%02x  ADC3= %03x ADC2= %03x ADC1= %03x\n",
			     (data_buf[i]&0xFE000000)>>25,
			     (data_buf[i]&0x00FF0000)>>16,
			     (data_buf[i]&0x0000FF00)>>8,
			     (data_buf[i]&0x000000FF)
			     );
		    }
		  else
		    {
		      printf("       LatchAll chan=%02x  ADC3= %03x ADC2= %03x ADC1= %03x\n",
			     (data_buf[i]&0xC0000000)>>30,
			     (data_buf[i]&0x3FF00000)>>20,
			     (data_buf[i]&0x000FFC00)>>10,
			     (data_buf[i]&0x000003FF)
			     );
		    }
		  dsize+=4;
		  i++;
		  apv_size--;
		  adc_size--;
		}
	    }
	}
    }
  else { /******** APV chip decode ******/
      
    while(dsize<evsize) {
      adc_size = ( (data_buf[i] >> 0) & ((1<<16)-1) );
      uint32_t apv_format = ( (data_buf[i] >> 24) & ((1<< 8)-1) );
      printf("%3d: %08x ",i, data_buf[i]);
      printf(" Format=%02x      ADC id=%3i         Size=%7i\n",( (data_buf[i] >> 24) & ((1<< 8)-1) ), ( (data_buf[i] >> 16) & ((1<< 8)-1) ),adc_size);
      dsize+=4;
      i++;

      printf("%3d: %08x ",i, data_buf[i]);
      printf(" Dummy = %02x         time tag = %9i\n",( (data_buf[i] >> 24) & ((1<< 8)-1) ),( (data_buf[i] >> 0) & ((1<<24)-1)) );
      dsize+=4;
      i++;
      adc_size-=2;

      while(dsize<evsize && adc_size>0) {
	  
	apv_size = ( (data_buf[i] >> 0) & ((1<<16)-1) );
	printf("%3d: %08x ",i, data_buf[i]);
	printf("    APV id= %01x       event=%5i    size=%7i\n",( (data_buf[i] >> 28) & ((1 << 4) -1) ),( (data_buf[i] >> 16) & ((1 << 12) -1) ),apv_size);
	dsize+=4;
	i++;

	printf("%3d: %08x ",i, data_buf[i]);
	printf("    Dummy=%02x            APV3= %03x APV2= %03x APV1= %03x\n",( (data_buf[i] >> 27) & ((1 << 5) -1) ),( (data_buf[i] >> 18) & ((1 << 9) -1) ),( (data_buf[i] >> 9) & ((1 << 9) -1) ),( (data_buf[i] >> 0) & ((1 << 9) -1) ));
	dsize+=4;
	i++;
	apv_size-=2;
	adc_size-=2;

	while(dsize<evsize && apv_size>0) {
	  printf("%3d: %08x ",i, data_buf[i]);
	  
	  // TODO: the isLatchAll check in the code above (line 943) is reverse logic, why?
	  //       but the below code seems to produce correct output, checked with run 8395
	  if ((apv_format & 2)==2) {
	    /* Latch ALL */
	    printf("       LatchAll chan=%02x  ADC3= %03x ADC2= %03x ADC1= %03x\n",( (data_buf[i] >> 30) & ((1 << 2) -1) ),( (data_buf[i] >> 20) & ((1 << 10) -1) ),( (data_buf[i] >> 10) & ((1 << 10) -1) ),( (data_buf[i] >> 0) & ((1<<10)-1) ));
	  } else {
	    /* sparse data */
	    printf("       Sparse  chan=%02x  ADC3= %03x ADC2= %03x ADC1= %03x\n",( (data_buf[i] >> 25) & ((1 << 7) -1) ),( (data_buf[i] >> 16) & ((1 << 9) -1) ),( (data_buf[i] >> 8) & ((1 << 8) -1)),( (data_buf[i] >> 0) & ((1 << 8) -1) ));
	  }
	    
	  dsize+=4;
	  i++;
	  apv_size--;
	  adc_size--;
	}

      }


    }

  }
} 

