#ifndef __subEvent_h__
#define __subEvent_h__
#define MAX_TDC_DATA 2048
#define MAX_TDC_HEAD 512
struct subeventStruct {
  int32_t decode_err;
  int32_t err;
  int32_t type;
  int32_t source;
  int32_t size;
  int32_t event;
  int32_t spill;
  int32_t stat;
  int32_t format;
  int32_t errwords;
  int32_t tcserr;
  int32_t status;       
  int32_t tdc_error;
  int32_t tdc_nr_header;
  int32_t tdc_header_time[MAX_TDC_HEAD];
  int32_t tdc_header_event[MAX_TDC_HEAD];
  int32_t tdc_tbo;
  int32_t tdc_locked;
  int32_t tdc_nr_data;
  int32_t tdc_chan[MAX_TDC_DATA]; /* 6 bit chip * 8 + channel */
  int32_t tdc_time[MAX_TDC_DATA]; /* 16 bit */
  int32_t tdc_port[MAX_TDC_DATA]; /* 4 bit port number */
};

void subEvent_decode(int32_t, int32_t *, struct subeventStruct * );
//FN void Event_check(int32_t, struct subeventStruct *, int32_t *);
void subEvent_check(int32_t, struct subeventStruct *, int32_t *);

#define min(x,y) ((x)>(y)?(y):(x))
#define max(x,y) ((x)>(y)?(x):(y))


#endif
