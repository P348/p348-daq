/*
  void subEvent_decode(int32_t,int32_t *, struct subeventStruct *) 
*/

#include <stdlib.h>
#include <stdint.h>

#include "subEvent.h"

void subEvent_decode(int32_t evsize,int32_t *data_buf, struct subeventStruct *subevent) {
  int32_t size, i, k, i0;
  subevent->decode_err = 0;
  size = evsize/4;
  if (size < 3) {
    subevent->decode_err = 1;
    return;
      } else {
  /* Decode the S-Link header  */
	subevent->err =       (data_buf[0] >>31 & (1<< 1)-1);           
	subevent->type =      (data_buf[0] >>26 & (1<< 5)-1);  
	subevent->source =    (data_buf[0] >>16 & (1<<10)-1); 
	subevent->size =      (data_buf[0] >> 0 & (1<<16)-1);  
	subevent->stat =      (data_buf[1] >>31 & (1<< 1)-1);  
	subevent->spill =     (data_buf[1] >>20 & (1<<11)-1);  
	subevent->event =     (data_buf[1] >> 0 & (1<<20)-1);
	subevent->format =    (data_buf[2] >>24 & (1<< 8)-1);  
	subevent->errwords =  (data_buf[2] >>16 & (1<< 8)-1);
	subevent->tcserr =    (data_buf[2] >> 8 & (1<< 8)-1);
	subevent->status =    (data_buf[2] >> 0 & (1<< 8)-1);

	/* checks */
	if ((subevent->format & 0x1) !=1) { subevent->decode_err = -2; }
	if (subevent->size !=size) { subevent->decode_err = 3; }

	/* decode the TDC data */
	subevent->tdc_nr_header = 0;
	subevent->tdc_nr_data = 0;
	subevent->tdc_tbo = 0;
	i0=3;
	if (subevent->format & 0x80) i0=22;
	for (i=i0;i<size;i++) {
	  subevent->tdc_locked = data_buf[i] >> 0 & (1<< 4)-1;
	  if ((data_buf[i] >>31 & (1<< 1)-1) == 0) {
	    /* tdc header */
	    k = subevent->tdc_nr_header;
	    if (k<MAX_TDC_HEAD) {
              subevent->tdc_tbo = subevent->tdc_tbo | (data_buf[i] >>30 & (1<< 1)-1);
	      subevent->tdc_header_time[k]=(data_buf[i] >>15 & (1<< 9)-1);
	      subevent->tdc_header_event[k]=(data_buf[i] >>24 & (1<< 6)-1);
	      subevent->tdc_nr_header++;
	    }
	  /* Save header information */
	    /*
	      chip_adr_h=(data_buf[i] >>11 & (1<< 3)-1);
	      chn_adr_h=(data_buf[i] >>8 & (1<< 3)-1);
	    */
	  } else if ((data_buf[i] >>22 & (1<< 11)-1) != (1<< 11)-1 ) {
	    /* tdc_data */
	    k = subevent->tdc_nr_data;
	    if (k<MAX_TDC_DATA) {
	      subevent->tdc_chan[k]=(data_buf[i] >>24 & (1<< 6)-1);
	      subevent->tdc_time[k]=(data_buf[i] >>8 & (1<< 16)-1);
	      subevent->tdc_port[k]=(data_buf[i] >>4 & (1<< 4)-1);
	      subevent->tdc_nr_data++;
	    }
	    
	  }

	}
      }
}
