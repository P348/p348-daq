#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "event.h"
#include "monitor.h"
#include "testLib.h"

char  **config1,
      **config2;
char  **events;
pid_t   pid;
int     status;
int     terminate = FALSE;

#define MAX_EXPECTED 100
int     nextExpected[ MAX_EXPECTED ];
int     expected = -1;
int     offset = 0;
int     expectedNum = -1;
int     checked = 0;
int     lastEvent = -1;

int     buffer[ 10000 ];
int    *eventData = (int *)((long)buffer + EVENT_HEAD_BASE_SIZE);

void sigHandler( int sig ) {
  if ( terminate ) {
    printf( "%08lx   *** Monitor generic consumer aborting ***\n",
	    (long int)pid );
  }
  terminate = TRUE;
}

int configError( char **argv, int where ) {
  fprintf( stderr,
	   "%s: configuration file \"%s\" invalid format, test aborted (%d)\n",
	   argv[0], argv[1], where );
  return 2;
}

void dumpVars() {
  if ( debug ) {
    int i;
    
    printf( "Data source: \"%s\"\n", dataSource );
    printf( "Configuration 1:\n" );
    for ( i = 0; config1[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, config1[i] );
    printf( "Configuration 2:\n" );
    for ( i = 0; config2[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, config2[i] );
    printf( "\t----------\n" ); 
    printf( "Events:\n" );
    for ( i=0; events[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, events[i] );
    printf( "\t----------\n" );
  }
}

void dumpConfig( char **config, char *d ) {
  if ( debug ) {
    int i = 0;
    printf( "Going to configuration # %s\n", d );
    while ( config[i] != 0 ) {
      printf( "\t%s \t=> %s\n", config[i], config[i+1] );
      i += 2;
    }
  }
}

void validateEvent( int *buffer ) {
  struct eventHeaderStruct *event = (struct eventHeaderStruct *)buffer;
  int status;

  if ( ++checked % 1000 == 0 ) {
    printf( "." ); fflush( stdout );
  }

  if ( event->eventSize == 0 ) {
    printf( "Received empty event\n" );
    terminate = TRUE;
    return;
  }
  
  if ( ( status = checkEvent( buffer ) ) != 0 ) {
    printf( "Event does not validate... Status: %d\n", status );
    dumpEvent( buffer );
    terminate = TRUE;
    return;
  }

  if ( lastEvent != -1 &&
       lastEvent >= event->eventTriggerPattern[0] &&
       event->eventTriggerPattern != 0 ) {
    printf( "*** WARNING *** Invalid events sequence (%d, %d)\n",
	    lastEvent, event->eventTriggerPattern[0] );
  }
  lastEvent = event->eventTriggerPattern[0];
}

int main( int argc, char **argv ) {
  pid = getpid();

  printf( "%08lx   *** Monitor changing consumer starting ***\n",
	  (long int)pid );

  if ( argc < 2 ) return usage( argv );

  /* Parse the flags might be used later for the config */
  if ( monParseFlags( argc, argv, FALSE ) != 0 ) return usage( argv );

  if ( consumerName == NULL ) {
    fprintf( stderr,
	     "%s: this is the changing consumer. A configuration must be given via the \"-c\" flag\n",
	     argv[ 0 ] );
    return 1;
  }
  if ( consumerName2 == NULL ) {
    fprintf( stderr,
	     "%s: this is the changing consumer. A configuration must be given via the \"-n\" flag\n",
	     argv[ 0 ] );
    return 1;
  }

  if ( signal( SIGHUP, sigHandler ) == SIG_ERR ) {
    perror( "Cannot declare signal handler... System-dependent status " );
    return 1;
  }
  
  /* First parse the configuration file */
  if ( (config1 = monParseConfig( consumerName, argv[1] ) ) == NULL )
    return configError( argv, 1 );
  if ( (config2 = monParseConfig( consumerName2, argv[1] ) ) == NULL )
    return configError( argv, 1 );
  if ( (config1 = parse( config1 )) == NULL)
    return configError( argv, 2 );
  if ( (config2 = parse( config2 )) == NULL)
    return configError( argv, 2 );
  if ( (events = monParseConfig( "Events", argv[1] ) ) == NULL )
    return configError( argv, 3 );

  /* Parse the flags who might override some of the parameters set by
   * the configuration file */
  if ( monParseFlags( argc, argv, FALSE ) != 0 ) return usage( argv );

  dumpVars();
  initVars();
  
  if ( ( status = monitorSetDataSource( dataSource ) ) != 0 ) {
    printf( "%08lx: monitorSetDataSource failed, status: %d\n",
	    (long int)pid, status );
    return 1;
  }
  if ( debug )
    printf( "%08lx: monitorSetDataSource called OK\n", (long int)pid );

  if ( ( status = monitorDeclareMp( "DATE changing consumer" ) ) != 0 ) {
    printf(  "%08lx: monitorDeclareMp failed, %s",
	    (long int)pid, monitorDecodeError( status ) );
    return 1;
  }

  if ( config1[0] != 0 ) {
    config1 = createConfigTable( config1 );
    if ( (status = monitorDeclareTable( config1 )) != 0 ) {
      printf( "%08lx: monitorDeclareTable status:0x%08x errno:%d (%s)\n",
	      (long int)pid, status, errno, strerror(errno) );
      return 1;
    }
    if ( debug )
      printf( "%08lx: monitor table declared OK\n", (long int)pid );
  }
  if ( config2[0] != 0 ) {
    config2 = createConfigTable( config2 );
  }

  do {
    doWait();
    if ( (status = monitorGetEvent( buffer, sizeof( buffer ))) != 0 ) {
      printf( "Error getting next event: %d (0x%08x): %s\n",
	      status, status,
	      monitorDecodeError( status ) );
    } else {
      if ( debug ) printf( "Event got OK\n" );
    }
    validateEvent( buffer );
    if ( ( checked % 100 ) == 0 ) {
      dumpConfig( config1, "1" );
      if ( ( status = monitorDeclareTable( config1 ) ) != 0 ) {
	printf( "%08lx: problems declaring table # 1 \
status:%08x errno:%d (%s)\n",
		(long int)pid, status, errno, strerror(errno) );
	return 1;
      }
      if ( debug )
	printf( "%08lx: table # 1 declared OK\n", (long int)pid );
    }
    if ( ( checked % 100 ) == 50 ) {
      dumpConfig( config2, "2" );
      if ( ( status = monitorDeclareTable( config2 ) ) != 0 ) {
	printf( "%08lx: problems declaring table # 2 \
status:%08x errno:%d (%s)\n",
		(long int)pid, status, errno, strerror(errno) );
	return 1;
      }
      if ( debug )
	printf( "%08lx: table # 2 declared OK\n", (long int)pid );
    }
  } while ( !terminate );

  printf( "%08lx   *** Monitor generic consumer ending ***\n", (long int)pid );

  return 0;
}
