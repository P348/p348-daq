/* Monitor V 3.xx monitor parameters module
 * ========================================
 *
 * This module is reponsible for storing & handling of the parameters of
 * the monitor scheme which are:
 *
 *   1) internal to the monitor package
 *   2) static to each actor involved in the monitor scheme
 *
 * Module history:
 *  1.00   3-Jul-98 RD   Created
 *  1.01   9-Oct-98 RD   Log level reduction disallowed
 */
#define VID "1.00"

#define IN_MONITOR_PARAMS

#include <stdio.h>
#include <stdlib.h>
#include "monitorInternals.h"

#define DESCRIPTION "DATE V3 monitor parameters handler"
#ifdef AIX
static
#endif
char monitorParamsIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                            """ """ VID """ """ \
                            """ compiled """ __DATE__ """ """ __TIME__;

/* ====================================================================== */
/* These are the global parameters for the monitoring scheme:
 */
int monitoringLogLevel = DEFAULT_LOG_LEVEL; 	/* The log level           */
int monitoringLogLevelFromEnvironment = FALSE;  /* TRUE if log level set via
						 * environment variable    */
int maxClients      = DEFAULT_MAX_CLIENTS; 	/* The maximum # of clients
						 * (consumers only)        */
int maxEvents       = DEFAULT_MAX_EVENTS; 	/* The maximum number of
						 * events                  */
int eventSize       = DEFAULT_EVENT_SIZE; 	/* The average event size  */
int eventBufferSize = DEFAULT_EVENT_BUFFER_SIZE;/* The total buffer space
						 * available for events    */
int monitorMaxAge   = DEFAULT_MAX_AGE;          /* Maximum age of an event */

/* ====================================================================== */

/* Return a version descriptive string */
char *monitorParamsGetVid() {
  return VID;
} /* End of monitorParamsGetVid */


/* Set/change the log level */
void setLogLevel( int newLevel ) {
  if ( newLevel <= monitoringLogLevel ) {
    LOG_DEVELOPER {
      char msg[ 1000 ];
      
      snprintf( SP(msg),
	       "setLogLevel Request to change log level %d=>%d ignored",
		monitoringLogLevel,
		newLevel );
      INFO_TO( MON_LOG, msg );
    }
  } else {
    char msg[ 1000 ];
    int done = FALSE;
    snprintf( SP(msg),
	      "setLogLevel Log level changed from %d to %d",
	      monitoringLogLevel, newLevel );
    LOG_DETAILED {
      INFO_TO( MON_LOG, msg );
      done = TRUE;
    }
    monitoringLogLevel = newLevel;
    LOG_DETAILED {
      if ( !done ) {
	INFO_TO( MON_LOG, msg );
      }
    }
  }
} /* End of setLogLevel */


/* Initialise the log level using the appropriate environment variable */
void initLogLevel() {
  char msg[ 1000 ];
  char *envVar = getenv( MONITOR_LOG_ENV_VAR );
  if ( envVar == NULL ) {
    LOG_DEVELOPER {
      snprintf( SP(msg), 
		"initLogLevel Environment variable \"%s\" not defined",
		MONITOR_LOG_ENV_VAR );
      INFO_TO( MON_LOG, msg );
    }
  } else {
    int newLevel;
    
    LOG_DEVELOPER {
      snprintf( SP(msg),
		"initLogLevel Environment variable \"%s\"=\"%s\"",
		MONITOR_LOG_ENV_VAR, envVar );
      INFO_TO( MON_LOG, msg );
    }
    if ( sscanf( envVar, "%d", &newLevel ) != 1 ) {
      LOG_NORMAL {
	snprintf( SP(msg),
		 "initLogLevel Warning: log level control environment \
variable \"%s\" improperly set to \"%s\"\nLog level NOT changed \
currentValue:%d",
		  MONITOR_LOG_ENV_VAR, envVar, monitoringLogLevel );
	INFO_TO( MON_LOG, msg );
      }
    } else {
      monitoringLogLevelFromEnvironment = TRUE;
      setLogLevel( newLevel );
      LOG_DETAILED {
	snprintf( SP(msg),
		  "initLogLevel Log level set via environment variable \"%s\"",
		  MONITOR_LOG_ENV_VAR );
	INFO_TO( MON_LOG, msg );
      }
    }
  }
} /* End of initLogLevel */
