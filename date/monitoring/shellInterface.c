/* Monitor V 3.xx shell interface module
 * =====================================
 *
 * This is the module that contains the interface with the system shell.
 * calls.
 *
 * Module history:
 *  1.00  20-Jul-98 RD   Created
 *  1.01  24-Aug-98 RD   First public release
 *  1.02  25-Jan-99 RD   Mods for pipes under ROOT added
 *  1.03  11-Oct-99 RD   Fix for "nslookup" answering in French added
 */
#define VID "1.03"

#include <stdio.h>
#include <string.h>

#include "monitorInternals.h"

#define DESCRIPTION "DATE V3 monitor shell interface"
#ifdef AIX
static
#endif
char monitorShellInterfaceIdent[] = \
             "@(#)""" __FILE__ """: """ DESCRIPTION \
             """ """ VID """ compiled """ __DATE__ """ """ __TIME__;


/* Get the result from a pipe. Fiddle with EOF to fix a "feature" of
 * pipes opened within a ROOT environment.
 *
 * Parameters:
 *   pipe     The input pipe
 *   result   Where the result is stored
 *   length   Length of result string in bytes
 */
void getResult( FILE *pipe, char *result, int length ) {
  int i, retries;
  
  for ( i = 0; i != length; ) {
    int nextC;
    retries = 0;
    do {
      nextC = getc( pipe );
    } while ( ( nextC == '\n' && i == 0 ) ||        /* Skip empty lines */
	      ( nextC == EOF && ++retries != 100 ));/* Wait for forked process
						     * to be ready */
    if ( nextC == EOF || nextC == '\n' ) break;
    result[i++] = nextC;
  }
  if ( i != length ) result[i] = 0;
  else result[ length-1 ] = 0;
} /* End of getResult */


/* Translate a string using shell-defined logical symbols.
 *
 * Params:
 *   string	String containing the source and used to store the
 *		translated string
 *   length	Size in characters of the string buffer
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorTranslate( char *string, int length ) {
  FILE *pipe;
  char  cmd[ 1024 ];
  int   status;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorTranslate(\"%s\",%d)", string, length );
    INFO_TO( MON_LOG, msg );
  }
  
  snprintf( SP(cmd), "echo %s", string );
  pipe = popen( cmd, "r" );
  getResult( pipe, string, length );
  if ( ( status = pclose( pipe ) ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "monitorTranslate command \"%s\" returns:%d translation:\"%s\"",
		cmd, status, string );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_SYS_ERROR;
  } else status = 0;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorTranslate return:(%d x%08x) result:\"%s\"",
	      status, status, string );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorTranslate */


/* Find out our hostname. WARNING: the result might be an alias and
 * most likely will not have the domain name attached. A translation
 * on the result is most likely needed.
 *
 * Parameters:
 *   string	Buffer used to store the result
 *   length	Size in characters of the string buffer
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorGetHostname( char *string, int length ) {
  FILE *pipe;
  int   status;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorGetHostname(%p, %d)", string, length );
    INFO_TO( MON_LOG, msg );
  }

  pipe = popen( "hostname", "r" );
  getResult( pipe, string, length );
  if ( ( status = pclose( pipe )) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "monitorGetHostname command \"hostname\" returns \
status:%d output:\"%s\"",
		status, string );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_SYS_ERROR;
  } else status = 0;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorGetHostname return:(%d x%08x) result:\"%s\"",
	      status, status, string );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorGetHostname */


/* Try to match a given line with a given template, looking for the
 * host name.
 *
 * Parameters:
 *   template	template prefix for the host name
 *   line	text line that may contain the host name
 *   name	output host name
 */
void tryToDecode( char *template, char *line, char *name ) 
{
  if ( strncmp( template, line, strlen( template ) ) != 0 )
   {
    return;
   }
   {
    int i, j;
    for ( i = strlen( template ); line[i] != 0 && line[i] == ' '; i++ );
    for ( j = i; line[j] != 0 && line[j] != ' ' && line[j] != '\n'; j++ );
    if ( i != j ) {
      strncpy( name, &line[i], j-i );
    }
    name[j-i] = 0;
    line[0] = 0;
    printf("<<<<<<<<<<<<< name = %s\n", name);
  }
} /* End of tryToDecode */


/* Translate a given hostname into a complete string, hopefully with
 * all hosts aliases resolved.
 *
 * Parameters:
 *   name    resulting hostname
 *   length  length of name
 *   in      source hostname to translate
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorTranslateAddress( char *name, int length, char *in ) {
#if 1
  strcpy(name,in);
  return 0;
}
#else
  FILE *pipe;
  char  cmd[ 1024 ];
  char  line[ 1024 ];
  int   status;

  LOG_DEVELOPER {
    char msg[ 2048 ];
    snprintf( SP(msg),
	      "monitorTranslateAddress(%p, %d, \"%s\")", name, length, in );
    INFO_TO( MON_LOG, msg );
  }
  
  name[0] = 0;
  snprintf( SP(cmd),
	    "export LANG; LANG=\"C\"; nslookup %s 2>&1", in );
  pipe = popen( cmd, "r" );
  do {
    getResult( pipe, line, sizeof( line ) );
    if ( strlen( line ) != 0 ) {
      /* Try some of the most used languages... */
      tryToDecode( "Name:",  line, name );
      if ( strlen( name ) == 0 ) tryToDecode( "Nom:",   line, name );
      if ( strlen( name ) == 0 ) tryToDecode( "Nome:",  line, name );
      if ( strlen( name ) == 0 ) tryToDecode( "Namen:", line, name );
    }
  } while ( strlen( name ) == 0 && strlen( line ) != 0 );
  if ( ( status = pclose( pipe ) ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorTranslateAddress command \"%s\" returns:%d",
		cmd, status );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_SYS_ERROR;
  } else status = 0;

  if ( strlen( name ) == 0 ) {
    /* Cannot convert the host name: revert to the original value */
    LOG_DETAILED {
      char msg[ 1024 ];
      snprintf( SP(msg), 
		"Cannot nslookup name \"%s\" command:\"%s\"", in, cmd );
      ERROR_TO( MON_LOG, msg );
    }
    strcpy( name, in );
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorTranslateAddress return:(%d x%08x) \"%s\"",
	      status, status, name );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  //printf("monitorTranslateAddress status = 0x%08X\n", status);
  
  return status;
} /* End of monitorTranslateAddress */
#endif
