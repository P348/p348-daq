#ifndef __test_lib_h__
#define __test_lib_h__

#ifndef TRUE
#define TRUE (0==0)
#endif
#ifndef FALSE
#define FALSE (0==1)
#endif

extern int   timerMin,
             timerMax,
             seed,
             debug,
             numLoops,
             mustConsume,
             useIOV;
extern char *dataSource,
            *consumerName,
            *consumerName2;

char **monParseConfig( char *whoAmI, char *configFile );
char **parse( char **table );
char **createConfigTable( char **table );
int    monParseFlags( int argc, char **argv, int client );
int    usage( char **argv );
int    initVars();
int    doWait();
void  *createNextEvent( char **eventsList );
int    checkEvent( void *rawData );
int    dumpEvent( void *rawData );
#endif
