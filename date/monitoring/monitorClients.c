/* monitorClients.c
 * ================
 *
 * Utility to list the clients registered with the local online
 * monitoring scheme.
 *
 */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include "monitorInternals.h"

key_t  key;
extern int monitorSharedMemoryId;
char   keyFile[ 1024 ] = { 0 };
volatile int error = FALSE;
int showTop = FALSE;
int timeBetweenChecks = 5;

void sigHandler( int sigNum ) {
  error = TRUE;
  signal( SIGSEGV, sigHandler );
  fflush( stdout );
  fflush( stderr );
  printf( "\nSEGV catched: aborting\n" );
  exit( 1 );
} /* End of sigHandler */


int usage( int argc, char **argv ) {
  fprintf( stderr, "Usage: %s [-t] [keyFile]\n", argv[0] );
  exit( 1 );
} /* End of usage */


void initVars( int argc, char **argv ) {
  int i;

  for ( i = 1; i != argc; i++ ) {
    if ( argv[i][0] == '-' ) {
      if ( argv[i][1] == 't' ) {
	showTop = TRUE;
      } else {
	usage( argc, argv );
      }
    } else {
      strcpy( keyFile, argv[1] );
    }
  }

  if ( keyFile[0] == 0 ) {
    strcpy( keyFile, getenv( "DATE_SITE" ) );
    strcpy( &keyFile[ strlen( keyFile ) ], "/" );
    strcpy( &keyFile[ strlen( keyFile ) ], getenv( "DATE_HOSTNAME" ) );
    strcpy( &keyFile[ strlen( keyFile ) ], "/monitoring.config" );
  }

  initLogLevel();

  signal( SIGSEGV, sigHandler );
}

#define PROTECTION_PUBLIC \
   ( S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH )

extern void monitorInitMonitorBufferPointers();
extern int  monitorInitSemaphores( key_t );

void initPtrs( char *keyFile ) {
  int status;
  
  if ( (key = ftok( keyFile, 1 )) == (key_t)(-1) ) {
    perror( "Cannot get key to key file " );
    fprintf( stderr, "Key file: \"%s\"\n", keyFile );
    exit( 1 );
  }
  if ( (monitorSharedMemoryId = shmget( key, 0, PROTECTION_PUBLIC )) == -1 ) {
    perror( "Cannot get shared memory segment " );
    exit( 1 );
  }
  if ( (monitorBuffer = shmat( monitorSharedMemoryId, 0, SHM_R|SHM_W )) ==
       (void *)(-1) ) {
    perror( "Cannot attach to shared memory segment " );
    exit( 1 );
  }
  if ( ( status = monitorParseConfigFile( keyFile ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot parse monitoring configuration file. Status: %d\n",
	     status );
    exit( 1 );
  }
  monitorInitMonitorBufferPointers();
  if ( monitorInitSemaphores( key ) != 0 ) {
    perror( "Cannot initialise control semaphores " );
    exit( 1 );
  }
}

void separator() {
  printf( "--------------------------------------------------------------------------------\n" );
}

char *indent() { return "                                 "; }

extern int monitorGetMonitorBufferDescriptor();
extern struct shmid_ds monitorBufferDescriptor;


char *dumpEventType( eventTypeType eventType ) {
  static char msg[ 1024 ];
  
  switch ( eventType ) {
    case START_OF_RUN :       return " SOR";
    case END_OF_RUN :         return " EOR";
    case START_OF_RUN_FILES : return "SORF";
    case END_OF_RUN_FILES :   return "EORF";
    case START_OF_BURST :     return " SOB";
    case END_OF_BURST :       return " EOB";
    case PHYSICS_EVENT :      return "PHYS";
    case CALIBRATION_EVENT :  return " CAL";
    case START_OF_DATA :      return " SOD";
    case END_OF_DATA :        return " EOD";
    case SYSTEM_SOFTWARE_TRIGGER_EVENT :
                              return "SST";
    case DETECTOR_SOFTWARE_TRIGGER_EVENT :
                              return "DST";
    case EVENT_FORMAT_ERROR : return "FERR";
  }
  snprintf( SP(msg), "UNKNOWN:x%08x", eventType );
  return msg;
}


char *dumpMonitorType( monitorType type ) {
  switch( type ) {
  case yes: return " yes";
  case all: return " ALL";
  case no:  return "  no";
  default:  return "--- WARNING --- UNKNOWN --- WARNING ---";
  }
}


void dumpClientsDatabase() {
  int ix;
  int headerDone;

  printf( "%d clients max, %d client%s declared, %ld process%s attached\n",
	  *mbMaxClients,
	  *mbNumClients,
	  ( *mbNumClients == 1 ? "" : "s" ),
	  (long int)monitorBufferDescriptor.shm_nattch,
	  ( monitorBufferDescriptor.shm_nattch == 1 ? "" : "es" ) );

  ix = 0;
  headerDone = FALSE;
  do {
    if ( CLIENT( ix )->inUse ) {
      int ev;

      if ( !headerDone ) {
	printf( " PID    " );
	for ( ev = EVENT_TYPE_MIN; ev <= EVENT_TYPE_MAX; ev++ ) {
	  printf( "%s  ", dumpEventType( ev ) );
	}
	printf( "  Monitoring program:\n\n" );
	headerDone = TRUE;
      }
      
      printf( "%-8ld", (long int)CLIENT( ix )->pid );
      for ( ev = 0; ev <= EVENT_TYPE_MAX-EVENT_TYPE_MIN; ev++ ) {
	printf( "%s  ", dumpMonitorType( CLIENT( ix )->monitorPolicy[ ev ] ) );
      }
      printf( "  %s", CLIENT( ix )->mpId );
      printf( "\n" );
    }
    ix++;
  } while ( ix != *mbMaxClients );
}


unsigned long64 *descriptors;
int    *counters;
void gatherData() {
  int i;
  int ix;
  int first;

  descriptors = malloc( (*mbMaxClients) * sizeof( unsigned long64 ) );
  counters = malloc( (*mbMaxClients) * sizeof( int ) );
  if ( descriptors == NULL || counters == NULL ) {
    perror( "Error during malloc " );
    exit( 1 );
  }
  if ( monitorLockBuffer() != 0 ) {
    printf( "Cannot lock monitor buffer...\n" );
    exit( 1 );
  }
  for ( i = 0; i != *mbMaxClients; i++ ) {
    if ( CLIENT( i )->inUse ) {
      descriptors[ i ] =
	CLIENT( i )->bytesRead[0] * MONITOR_TOP_COUNTER +
	CLIENT( i )->bytesRead[1];
      counters[ i ] = CLIENT( i )->eventsRead;
    } else {
      descriptors[ i ] = 0;
      counters[ i ] = 0;
    }
  }
  if ( monitorUnlockBuffer() != 0 ) {
    printf( "Cannot lock monitor buffer...\n" );
    exit( 1 );
  }

  sleep( timeBetweenChecks );

  if ( monitorLockBuffer() != 0 ) {
    printf( "Cannot lock monitor buffer...\n" );
    exit( 1 );
  }
  for ( i = 0; i != *mbMaxClients; i++ ) {
    if ( CLIENT( i )->inUse && descriptors[ i ] != 0 ) {
      descriptors[i] = ( CLIENT( i )->bytesRead[0] * MONITOR_TOP_COUNTER +
			 CLIENT( i )->bytesRead[1] ) - descriptors[i];
      counters[ i ]  = ( CLIENT( i )->eventsRead ) - counters[ i ];
    } else {
      descriptors[i] = 0;
      counters[ i ] = 0;
    }
  }
  if ( monitorUnlockBuffer() != 0 ) {
    printf( "Cannot lock monitor buffer...\n" );
    exit( 1 );
  }

  first = TRUE;
  system( "clear" );
  do {
    for ( ix = -1, i = 0; i != *mbMaxClients; i++ ) {
      if ( descriptors[ i ] != 0 ) {
	if ( ix == -1 ) ix = i;
	else if ( descriptors[ ix ] < descriptors[ i ] ) ix = i;
      }
    }
    if ( ix != -1 ) {
      if ( first ) {
	first = FALSE;
	printf( "PID         Bytes/s Mp\n" );
      }
      printf( "%-8ld ", (long int)CLIENT( ix )->pid );
      printf( "%10lld ", (unsigned long64)(descriptors[ ix ] / timeBetweenChecks) + 1 );
      printf( "%s", CLIENT( ix )->mpId );
      printf( "\n" );

      descriptors[ ix ] = 0;
    }
  } while ( ix != -1 );
  free( descriptors );

  first = TRUE;
  do {
    for ( ix = -1, i = 0; i != *mbMaxClients; i++ ) {
      if ( counters[ i ] != 0 ) {
	if ( ix == -1 ) ix = i;
	else if ( counters[ ix ] < counters[ i ] ) ix = i;
      }
    }
    if ( ix != -1 ) {
      if ( first ) {
	first = FALSE;
	printf( "\nPID        Events/s Mp\n" );
      }
      printf( "%-8ld ", (long int)CLIENT( ix )->pid );
      printf( "%10d ", (counters[ ix ] / timeBetweenChecks) + 1 );
      printf( "%s", CLIENT( ix )->mpId );
      printf( "\n" );

      counters[ ix ] = 0;
    }
  } while ( ix != -1 );
  free( counters);
  
} /* End of gatherData */
  


void dumpTopClients() {
  printf( "Displaying top clients: ^C to stop\n" );
  for (;;) {
    gatherData();
  }
} /* End of dumpTopClients */


void dumpBuffer() {
  if ( showTop ) {
    dumpTopClients();
  } else {
#ifdef LOCK_BUFFER
    if ( monitorLockBuffer() != 0 ) {
      printf( "Cannot lock monitor buffer...\n" );
      return;
    }
#endif
    if ( monitorGetMonitorBufferDescriptor() != 0 ) {
      printf( "Cannot get buffer descriptor...\n" );
    }
    dumpClientsDatabase();
#ifdef LOCK_BUFFER
    if ( monitorUnlockBuffer() ) {
      printf( "Cannot unlock monitor buffer...\n" );
    }
#endif
  }
} /* End of dumpBuffer */


int main( int argc, char **argv ) {
  if ( argc != 1 && argc != 2 && argc != 3 ) return usage( argc, argv );

  initVars( argc, argv );
  initPtrs( keyFile );
  dumpBuffer();
  return 0;
} /* End of main */
