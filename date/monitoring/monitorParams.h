/* Monitor parameters definitions
 * ==============================
 */
#ifndef __monitor_params_h__
#define __monitor_params_h__

/* ==================== The library entries ==================== */

char *monitorParamsGetVid();	/* Get the version ID of the module       */
void  setLogLevel(int);		/* Change the log level to a given value  */
void  initLogLevel();		/* Set the log level using the appropriate
				 * environment variable (if declared)     */

#endif
