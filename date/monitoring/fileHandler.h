/* Monitor file handler definitions
 * ================================
 */
#ifndef __file_handler_h__
#define __file_handler_h__


/* All the entries return 0 for OK, else for error */
extern int monitorFileOpenChannel( char *monitorBufferName );
extern int monitorFileDeclareTable( monitorRecordPolicy *monitorPolicyTable );
extern int monitorFileNextEvent( void *buffer, int sizeOfBuffer );
extern int monitorFileDisconnect();
#endif
