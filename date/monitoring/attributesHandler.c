/* Events attributes handler module
 * ================================
 *
 * This module contains some utilities to handler the attributes field
 * of the events headers
 *
 * Module history:
 *  1.00  6-Jul-99 RD   Created as part of the monitoring package
 *  1.01 27-Jul-99 RD	New attributes handling macros used
 *  1.02  5-Jul-01 RD   Mods for "&" and "|" of attributes
 *  1.03 12-Oct-04 RD   Added printing of HLT attributes
 */
#define VID "1.03"

#include <stdio.h>
#include <strings.h>
#include <string.h>

#include "monitorInternals.h"

#define DESCRIPTION "Events attributes handler"
#ifdef AIX
static
#endif
char attributesHandlerIdent[] = \
   "@(#)""" __FILE__ """: """ DESCRIPTION \
   """ """ VID """ compiled """ __DATE__ """ """ __TIME__;


/* Shortcut to force a pointer to an event header structure pointer */
#define HEADER( ev ) ((struct eventHeaderStruct *)ev)


/* Validate an attribute value.
 *
 * Parameter:
 *	num	The attribute number
 *
 * Returns:
 *	TRUE	Valid attribute
 *	FALSE	Invalid attribute
 */
int monitorValidateAttribute( int num ) {
  return ( num >= 0 && num < ALL_ATTRIBUTE_BITS );
} /* End of monitorValidateAttribute */


/* Assert all the attributes of an event.
 *
 * Parameters:
 *	ev	The event whose attributes field needs to be asserted
 */
void monitorAssertAttributes( void *ev ) {
  monitorAssertAttributesMask( &(HEADER(ev)->eventTypeAttribute) );
} /* End of monitorAssertAttributes */


/* Assert all the attributes of a mask.
 *
 * Parameters:
 *	attr	The event attributes to Assert
 */
void monitorAssertAttributesMask( eventTypeAttributeType *attr ) {
  int i;
  for ( i = 0; i != ALL_ATTRIBUTE_WORDS; i++ ) (*attr)[i] = 0xffffffff;
} /* End of monitorAssertAttributesMask */


/* Check if an event is selected by the given attribute mask.
 *
 * Parameters:
 *	ev	The event
 *	attr	The attribute mask
 *      byAnd   Select the event by ANDing the attributes
 *
 * Returns:
 *	TRUE	Event is selected by the given mask
 *	FALSE	otherwise
 */
int monitorSelectedByAttributes( void *ev,
				 eventTypeAttributeType *attr,
				 int byAnd ) {
  int i;

  /*
  printf( "Selected test, evAttr:[ " );
  for ( i=0; i != ALL_ATTRIBUTE_BITS; i++)
    if ( TEST_ANY_ATTRIBUTE( HEADER( ev )->eventTypeAttribute, i ) )
      printf( "%d ", i );
  printf( "] reqAttr:[ " );
  for ( i=0; i != ALL_ATTRIBUTE_BITS; i++)
    if ( TEST_ANY_ATTRIBUTE( *attr, i ) )
      printf( "%d ", i );
  printf( "] byAnd:%s\n", byAnd ? "TRUE" : "FALSE" );
  */
  
  if ( byAnd ) {
    /* The event must have all attributes from the mask set */
    for ( i=0; i != ALL_ATTRIBUTE_BITS; i++ )
      if ( TEST_ANY_ATTRIBUTE( *attr,i ) &&
	   (!TEST_ANY_ATTRIBUTE( HEADER( ev )->eventTypeAttribute, i )) )
	break;
    if ( i == ALL_ATTRIBUTE_BITS ) return TRUE;
  } else {
    /* If the event has at least one attribute from the mask set, select it */
    for ( i=0; i != ALL_ATTRIBUTE_BITS; i++ )
      if ( TEST_ANY_ATTRIBUTE( HEADER( ev )->eventTypeAttribute, i ) &&
	   TEST_ANY_ATTRIBUTE( *attr, i ) )
	return TRUE;
  }

  /* If the client has not selected attributes, select the event */
  for ( i=0; i != ALL_ATTRIBUTE_WORDS; i++ )
    if ( (*attr)[i] != 0xffffffff )
      return FALSE;
  
  /* Select the event */
  return TRUE;
} /* End of monitorSelectedByAttributes */


/* Print the attributes of an event to the given stream.
 *
 * Parameters:
 *	ev	The event whose attributes have to be print
 *	stream	The stream to print the attributes into
 */
void monitorPrintAttributes( void *ev, FILE *stream ) {
  monitorPrintAttributesMask( &(HEADER( ev )->eventTypeAttribute), stream );
} /* End of monitorPrintAttributes */


/* Print an attributes mask to a given stream.
 *
 * Parameters:
 *	attr	The mask
 *	stream	The stream to print the attributes into
 */
void monitorPrintAttributesMask( eventTypeAttributeType *attr, FILE *stream ) {
  char line[ 2000 ];
  monitorDumpAttributesMask( attr, line, sizeof( line ) );
  fprintf( stream, line );
} /* End of monitorPrintAttributes */


/* Utility routine to "safe append" a string to a string.
 *
 * Parameters:
 *	str	Pointer to the string to append to
 *	len	Length (in chars) of str
 *	tail	String to append
 */
void monitorAppendString( char *str, int len, char *tail ) {
  int l = strlen( str );

  if ( l == len - 1 ) {
    str[ len - 2 ] = '>';
    str[ len - 1 ] = 0;
  } else {
    if ( l + strlen( tail ) < len - 1 ) {
      strcpy( &str[ l ], tail );
    } else {
      int left = len - l - 1;
      if ( left >= 0 )
	strncpy( &str[ l ], tail, left );
      str[ len-2 ] = '>';
      str[ len-1 ] = 0;
    }
  }
} /* End of monitorAppendString */


/* Dump attributes into a string buffer.
 *
 * Parameters:
 *	attr	The attributes to dump
 *	str	The string buffer
 *	len	Size of the string buffer (in chars)
 */
void monitorDumpAttributes( void *ev, char *str, int len ) {
  if ( len <= 0 ) return;
  str[0] = 0;
  monitorDumpAttributesMask( &(HEADER( ev )->eventTypeAttribute), str, len );
} /* End of monitorDumpAttributes */


/* Dump attributes into a string buffer.
 *
 * Parameters:
 *	attr	The attributes to dump
 *	str	The string buffer
 *	len	Size of the string buffer (in chars)
 */
void monitorDumpAttributesMask( eventTypeAttributeType *attr,
				char *str,
				int len ) {
  int word;
  int all;
    
  if ( len <= 0 ) return;
  str[0] = 0;

  for ( all = TRUE, word = 0; all && word != ALL_ATTRIBUTE_WORDS; word++ )
    all = ((*attr)[ word ] == 0xffffffff);
  if ( all ) {
    monitorAppendString( str, len, "-=-" );
  } else {
    int  bit;
    char buffer[ 100 ];
    int  first = TRUE;
    int  i;

    for ( bit = 0; bit != ALL_ATTRIBUTE_BITS; bit++ ) {
      if ( TEST_ANY_ATTRIBUTE( *attr, bit ) ) {
	if ( !first ) monitorAppendString( str, len, "+" );
	first = FALSE;
	if ( bit < FIRST_SYSTEM_ATTRIBUTE ) {
	  /* User attribute: print the number */
	  snprintf( SP(buffer), "%d", bit );
	} else {
	  /* System attribute: try to decode it */
	  switch ( bit ) {
	  case ATTR_P_START:
	    snprintf( SP(buffer), "PhaseStart" );
	    break;
	  case ATTR_P_END :
	    snprintf( SP(buffer), "PhaseEnd" );
	    break;
	  case ATTR_EVENT_SWAPPED :
	    snprintf( SP(buffer), "Swapped" );
	    break;
	  case ATTR_EVENT_PAGED :
	    snprintf( SP(buffer), "Paged" );
	    break;
	  case ATTR_SUPER_EVENT :
	    snprintf( SP(buffer), "Super" );
	    break;
	  case ATTR_ORBIT_BC :
	    snprintf( SP(buffer), "Orbit/BC" );
	    break;
	  case ATTR_KEEP_PAGES :
	    snprintf( SP(buffer), "Keep" );
	    break;
	  case ATTR_HLT_DECISION :
	    snprintf( SP(buffer), "HLT" );
	    break;
	  case ATTR_EVENT_DATA_TRUNCATED :
	    snprintf( SP(buffer), "Truncated" );
	    break;
	  case ATTR_EVENT_ERROR :
	    snprintf( SP(buffer), "Error" );
	    break;
	  default :
	    snprintf( SP(buffer), "??%d??", bit );
	  }
	}
	monitorAppendString( str, len, buffer );
      }
    }
    if ( first ) monitorAppendString( str, len, "noAttr" );
    snprintf( SP(buffer), " (" );
    for ( i = 0; i != ALL_ATTRIBUTE_WORDS; i++ ) {
      snprintf( AP(buffer),
		"%08x", (*attr)[i] );
      if ( i != ALL_ATTRIBUTE_WORDS-1 )
	snprintf( AP(buffer), "." );
    }
    snprintf( AP(buffer), ")" );
    monitorAppendString( str, len, buffer );
  }
} /* End of monitorDumpAttributes */
