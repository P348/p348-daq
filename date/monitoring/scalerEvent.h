#define MAX_SCALER_DATA 32
#define MAX_SCALER_PORT 4
struct scalerStruct {
  int decode_err;
  int err;
  int type;
  int source;
  int size;
  int event;
  int spill;
  int stat;
  int format;
  int errwords;
  int tcserr;
  int status;

  int sc_port;
  int sc_port_error[MAX_SCALER_PORT];
  int sc_geoid[MAX_SCALER_PORT];
  int sc_data[MAX_SCALER_DATA*MAX_SCALER_PORT];
  int sc_time[MAX_SCALER_PORT];
  int sc_flags[MAX_SCALER_PORT];
};

void scaler_decode(int, int *, struct scalerStruct * );

#define min(x,y) ((x)>(y)?(y):(x))
#define max(x,y) ((x)>(y)?(x):(y))


