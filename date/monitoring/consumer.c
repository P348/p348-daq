#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "event.h"
#include "monitor.h"
#include "testLib.h"

char  **config;
char  **events;
pid_t   pid;
int     status;
int     terminate = FALSE;

#define MAX_EXPECTED 100
int     nextExpected[ MAX_EXPECTED ];
int     expected = -1;
int     offset = 0;
int     expectedNum = -1;
int     checked = 0;
int     lastEvent = -1;

int     buffer[ 10000 ];
int    *eventData = (int *)((long)buffer + EVENT_HEAD_BASE_SIZE);

void sigHandler( int sig ) {
  if ( terminate ) {
    printf( "%08lx   *** Monitor generic consumer aborting ***\n",
	    (long int)pid );
  }
  terminate = TRUE;
}

int configError( char **argv, int where ) {
  fprintf( stderr,
	   "%s: configuration file \"%s\" invalid format, test aborted (%d)\n",
	   argv[0], argv[1], where );
  return 2;
}

void dumpVars() {
  if ( debug ) {
    int i;
    
    printf( "Data source: \"%s\"\n", dataSource );
    printf( "Configuration:\n" );
    for ( i = 0; config[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, config[i] );
    printf( "\t----------\n" ); 
    printf( "Events:\n" );
    for ( i=0; events[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, events[i] );
    printf( "\t----------\n" );
    if ( nextExpected[ 0 ] == -1 )
      printf( "No \"MUST\" monitoring requested\n" );
    else {
      printf( "MUST events: " );
      for ( i = 0; nextExpected[ i ] != -1; i++ )
	printf( "%d ", nextExpected[ i ] );
      printf( "\n" );
    }
  }
}

void initExpected() {
  int i;
  int evNum = 0;

  for ( i = 0; i != MAX_EXPECTED; i++ ) nextExpected[ i ] = -1;
  for ( i = 0; events[ i ] != 0; i++ ) {
    int nLine;
    char *thisEventSize, *thisEvent;
    char *theEvent = strdup( events[ i ] );
    
    thisEventSize = strtok( theEvent, " " );
    thisEvent = strtok( NULL, " " );

    for ( nLine = 0; config[ nLine ] != 0; nLine++ ) {
      char *evType, *monType;
      char *thisLine = strdup( config[ nLine ] );
    
      evType = strtok( thisLine, " " );
      monType = strtok( NULL, " " );
      if ( strcasecmp( evType, thisEvent ) == 0 ) {
	if ( evNum == MAX_EXPECTED - 1 ) {
	  fprintf( stderr, "Too many expected events...\n" );
	} else {
	  nextExpected[ evNum++ ] = i;
	  expected = 0;
	}
      }
    }
  }
}

void validateEvent( int *buffer ) {
  struct eventHeaderStruct *event = (struct eventHeaderStruct *)buffer;
  int status;

  if ( ++checked % 1000 == 0 ) {
    printf( "." ); fflush( stdout );
  }

  if ( event->eventSize == 0 ) {
    printf( "Received empty event\n" );
    terminate = TRUE;
    return;
  }
  
  if ( ( status = checkEvent( buffer ) ) != 0 ) {
    printf( "Event does not validate... Status: %d\n", status );
    dumpEvent( buffer );
    terminate = TRUE;
    return;
  }

  if ( lastEvent != -1 &&
       lastEvent >= event->eventTriggerPattern[0] &&
       event->eventTriggerPattern != 0 ) {
    printf( "*** WARNING *** Invalid events sequence (%d, %d)\n",
	    lastEvent, event->eventTriggerPattern[0] );
  }
  lastEvent = event->eventTriggerPattern[0];
  
  if ( event->eventType == START_OF_RUN ) {
    expected = 0;
    expectedNum = nextExpected[ 0 ];
    offset = event->eventTriggerPattern[0];
  }
  if ( event->eventSize > EVENT_HEAD_BASE_SIZE && expectedNum != -1 ) {
    if ( event->eventTriggerPattern[0] == (expectedNum + offset)) {
      expectedNum = nextExpected[ ++expected ];
    } else if ( event->eventTriggerPattern[0] > (expectedNum +offset)) {
      printf( "Missed event: expected = %d, got = %d!\n",
	      expectedNum + offset, event->eventTriggerPattern[0] );
      dumpEvent( buffer );
      terminate = TRUE;
      return;
    }
  }
}

int main( int argc, char **argv ) {
  pid = getpid();

  printf( "%08lx   *** Monitor generic consumer starting ***\n",
	  (long int)pid );

  if ( argc < 2 ) return usage( argv );

  /* Parse the flags might be used later for the config */
  if ( monParseFlags( argc, argv, FALSE ) != 0 ) return usage( argv );

  if ( consumerName == NULL ) {
    fprintf( stderr,
	     "%s: this is a generic consumer. A configuration must be given via the \"-c\" flag\n",
	     argv[ 0 ] );
    return 1;
  }

  if ( signal( SIGHUP, sigHandler ) == SIG_ERR ) {
    perror( "Cannot declare signal handler... System-dependent status " );
    return 1;
  }
  
  /* First parse the configuration file */
  if ( (config = monParseConfig( consumerName, argv[1] ) ) == NULL )
    return configError( argv, 1 );
  if ( (config = parse( config )) == NULL)
    return configError( argv, 2 );
  if ( (events = monParseConfig( "Events", argv[1] ) ) == NULL )
    return configError( argv, 3 );

  /* Parse the flags who might override some of the parameters set by
   * the configuration file */
  if ( monParseFlags( argc, argv, FALSE ) != 0 ) return usage( argv );

  initExpected();
  dumpVars();
  initVars();
  
  if ( ( status = monitorDeclareMp( "DATE test consumer" ) ) != 0 ) {
    printf( "%08lx: monitorDeclareMp failed, %s",
	    (long int)pid, monitorDecodeError( status ) );
    return 1;
  }
  if ( ( status = monitorSetDataSource( dataSource ) ) != 0 ) {
    printf( "%08lx: monitorSetDataSource failed, status: %d\n",
	    (long int)pid, status );
    return 1;
  }
  if ( debug )
    printf( "%08lx: monitorSetDataSource called OK\n", (long int)pid );

  if ( config[0] != 0 ) {
    config = createConfigTable( config );
    if ( (status = monitorDeclareTableWithAttributes( config )) != 0 ) {
      printf( "%08lx: monitorDeclareTableWithAttributes \
status:0x%08x errno:%d (%s)\n",
	      (long int)pid, status, errno, strerror(errno) );
      return 1;
    }
    if ( debug )
      printf( "%08lx: monitor table declared OK\n", (long int)pid );
  }

  do {
    doWait();
    if ( (status = monitorGetEvent( buffer, sizeof( buffer ))) != 0 ) {
      printf( "Error getting next event: %d (0x%08x): %s\n",
	      status, status,
	      monitorDecodeError( status ) );
    } else {
      if ( debug ) {
	printf( "Event got OK\n" );
	dumpEvent( buffer );
      }
    }
    validateEvent( buffer );
  } while ( !terminate );

  printf( "%08lx   *** Monitor generic consumer ending ***\n",
	  (long int)pid );

  return 0;
}
