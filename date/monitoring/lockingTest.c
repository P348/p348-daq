#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "monitorInternals.h"

void usage( char **argv, int where ) {
  fprintf( stderr,
	   "%d Usage: %s l[ock]|w[aitFreeSpace]|s[ignalFreeSpace]|W[aitEvents]N[ewEvents] [keyFile]\n",
	   where, argv[0] );
  exit( 1 );
}

int main( int argc, char **argv ) {
  int  status;
  char mode;
  int  pid;

  if ( argc != 2 && argc != 3 ) usage( argv, 1 );
  if ( sscanf( argv[1], "%c", &mode ) != 1 ) usage( argv, 2 );
  if ( mode != 'l' &&
       mode != 'w' &&
       mode != 's' &&
       mode != 'W' &&
       mode != 'N' )
    usage( argv, 3 );

  pid = getpid();

  initLogLevel();
  
  status = monitorAttachToBuffer( argc == 2 ? "/tmp/keyFile" : argv[2] );
  if ( status != 0 ) {
    printf( "%08x: monitorAttachToBuffer returns %d: %s\n",
	    pid, status, monitorDecodeError(status) );
    fflush( stdout );
  }

  if ( mode == 'l' ) {
    printf( "%08x: locking monitor buffer\n", pid );
    fflush( stdout );

    status = monitorLockBuffer();
    printf( "%08x: monitorLockBuffer() returns %d\n",
	    pid, status );
    fflush( stdout );

    printf( "Press RETURN to continue " ); fflush( stdout );
    mode = getchar();
    
    printf( "%08x: unlocking monitor buffer\n", pid );
    fflush( stdout );

    status = monitorUnlockBuffer();
    printf( "%08x: monitorUnlockBuffer() returns %d\n",
	    pid, status );
    fflush( stdout );
    
  } else if ( mode == 'w' ) {
    printf( "%08x: locking monitor buffer\n", pid );
    fflush( stdout );

    status = monitorLockBuffer();
    printf( "%08x: monitorLockBuffer() returns %d\n",
	    pid, status );
    fflush( stdout );
    
    printf( "%08x: waiting for free space\n", pid );
    fflush( stdout );

    status = monitorWaitForFreeSpace();
    printf( "%08x: monitorWaitForFreeSpace() returns %d\n",
	    pid, status );
    fflush( stdout );

  } else if ( mode == 's' ) {
    printf( "%08x: locking monitor buffer\n", pid );
    fflush( stdout );

    status = monitorLockBuffer();
    printf( "%08x: monitorLockBuffer() returns %d\n",
	    pid, status );
    fflush( stdout );
    
    printf( "%08x: signalling free space\n", pid );
    fflush( stdout );

    status = monitorSignalFreeSpace();
    printf( "%08x: monitorSignalFreeSpace() returns %d\n",
	    pid, status );
    fflush( stdout );

    printf( "%08x: unlocking monitor buffer\n", pid );
    fflush( stdout );

    status = monitorUnlockBuffer();
    printf( "%08x: monitorUnlockBuffer() returns %d\n",
	    pid, status );
    fflush( stdout );

  } else if ( mode == 'W' ) {

    status = monitorLockBuffer();
    if ( status != 0 ) {
      printf( "%08x: monitorLockBuffer() returns %d\n",
	      pid, status );
      fflush( stdout );
    }
    
    printf( "%08x: waiting for new events\n", pid );
    fflush( stdout );

    status = monitorWaitForNewEvents();
    printf( "%08x: monitorWaitForNewEvents() returns %d\n",
	    pid, status );
    fflush( stdout );

    status = monitorUnlockBuffer();
    if ( status != 0 ) {
      printf( "%08x: monitorUnlockBuffer() returns %d\n",
	      pid, status );
    }
    
  } else if ( mode == 'N' ) {
    printf( "%08x: signalling new event\n", pid );
    fflush( stdout );

    status = monitorSignalNewEvent();
    if ( status != 0 ) {
      printf( "%08x: monitorSignalNewEvent() returns %d: %s\n",
	      pid, status, monitorDecodeError( status ) );
      fflush( stdout );
    }
    
  }
  
  exit( 0 );
}
