/* The mpDaemon main program
 *
 * Module history:
 *  1.00  18-Aug-98 RD   Created
 *  1.01  24-Aug-98 RD   First public release
 *  1.02  31-Aug-98 RD   Bug fixes, cleanup
 *  1.03  15-Sep-98 RD   Fixed daemon hanging for online + no producer
 *  1.04   7-Oct-98 RD   Bug fixed for parsing errors of configration file(s)
 *  1.05   8-Oct-98 RD   mpDaemon configuration file parsing added
 *  1.06  12-Oct-98 RD   First reply added to select endianness
 *  1.07   8-Jul-99 RD   Event attributes added
 */
#define VID "1.07"

#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#ifdef SunOS
#include <sys/filio.h>		/* For FIONREAD */
#else
#include <sys/ioctl.h>		/* For FIONREAD */
#endif

#include "monitorInternals.h"


#define DESCRIPTION "DATE V3 mpDaemon main"
#ifdef AIX
static
#endif
char mpDaemonMainIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                        """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

extern char *remoteHostname;
extern int monitorSilentParsing;

int terminate = FALSE;
int online = FALSE;
int inFile = -1;
int outFile = -1;
int errFile = -1;
int command = 0;
int inputIsBlocking = TRUE;
int fromMemory = FALSE;


/* Set the channel in blocking mode (if necessary).
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int setBlocking() {
  if ( !inputIsBlocking ) {
    int zero = 0;
    if ( ioctl( inFile, FIONBIO, &zero ) < 0 ) {
      LOG_NORMAL ERROR_TO( MON_LOG, "mpDaemon: cannot ioctl (clear)" );
      return -1;
    }
    inputIsBlocking = TRUE;
  }
  return 0;;
} /* End of setBlocking */


/* Set the channel in non-blocking mode (if necessary).
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int setNonBlocking() {
  if ( inputIsBlocking ) {
    int one = 1;
    if ( ioctl( inFile, FIONBIO, &one ) < 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "mpDaemon: cannot ioctl (set non-blocking)" );
      return -1;
    }
    inputIsBlocking = FALSE;
  }
  return 0;
} /* End of set NonBlocking */


/* Our timeout handler, used either to start or to restart our
 * periodic timer, used to interrupt the wait for online data
 */
void timeoutHandler( int sigNum ) {
  signal( SIGALRM, timeoutHandler );
  alarm( TIME_BETWEEN_CHECKS );
  if ( sigNum != 0 )
    LOG_DEVELOPER INFO_TO( MON_LOG, "mpDaemon: timeout" );
} /* End of timeoutHandler */


/* Get the next event from the monitor buffer and - if possible - send it
 * over the link.
 *
 * Returns:
 *   0    => error
 *   else => OK
 */
int getAndSendNextEvent() {
  void  *nextEvent;
  int    status;
  long32 size;

  LOG_DEVELOPER
    INFO_TO( MON_LOG, "mpDaemon: getting next event" );
  
  if ( fromMemory ) timeoutHandler( 0 );
  status = monitorGetEventDynamic( &nextEvent );
  if ( fromMemory ) alarm( 0 );
  
  if ( status == MON_ERR_INTERRUPTED ) {
    LOG_DEVELOPER
      INFO_TO( MON_LOG,
	       "mpDaemon: wait for next event interrupted" );
    return 0;
  }
    
  if ( status != 0 ) {
    long32 p[2];
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon Error getting next event:(%d x%08x)",
		status, status );
      ERROR_TO( MON_LOG, msg );
    }
    p[0] = 0;
    p[1] = status;
    send( outFile, (void *)p, 2*sizeof( long32 ), 0 );
    return 1;
  }

  size = *(long32 *)nextEvent;
  if ( size == 0 ) {
    LOG_DEVELOPER INFO_TO( MON_LOG, "mpDaemon: empty event received" );
    return 0;
  }
  if ( setBlocking() != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"mpDaemon: failed to set blocking (getAndSendNextEvent)" );
    return 2;
  }
  status = send( outFile, nextEvent, size, 0 );
  free( nextEvent );
  if ( status != size ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon Problems sending next event \
size:%d status:%d errno:%d (%s)",
		size, status, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return 3;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "mpDaemon Event sent size:%d", size );
    INFO_TO( MON_LOG, msg );
  }
  return 0;
} /* End of getAndSendNextEvent */


/* Send a reply over the monitor channel
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int sendReply( int toSend ) {
  int status;
  int outBuffer[2];

  outBuffer[0] = REPLY_FLAG;
  outBuffer[1] = toSend;
  if ( ( status = send( outFile, (void *)outBuffer, 8, 0 ) ) != 8 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "mpDaemon Error sending reply status:%d", status );
      ERROR_TO( MON_LOG, msg );
    }
    return 1;
  }
  
  if ( fflush( stdout ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "mpDaemon Error flushing output channel errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return 2;
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "mpDaemon Reply (%d x%08x) sent OK", toSend, toSend );
    INFO_TO( MON_LOG, msg );
  }
  return 0;
} /* End of sendReply */


/* Get a variable-length string from the input channel.
 *
 * Returns:
 *   0    => OK
 *   else => error
 *
 * WARNING:
 *   the returned string (if any) MUST be disposed
 */
int getString( char **theString ) {
  int   currSiz = 0;
  int   currLen = 0;
  char *currStr = NULL;
  
  do {
    if ( currSiz == currLen ) {
      char *newStr;
      
      currSiz += 80;
      if ( ( newStr = (char *)malloc( (size_t)currSiz ) ) == NULL ) return 1;
      if ( currLen != 0 ) {
	strncpy( newStr, currStr, currLen );
	free( currStr );
      }
      currStr = newStr;
    }
    if ( ( currStr[ currLen ] = getchar() ) == EOF ) return 2;
  } while ( currStr[ currLen++ ] != 0 );

  LOG_DEVELOPER {
    char msg[ 2048 ];
    snprintf( SP(msg), "mpDaemon Got string:\"%s\"", currStr );
    INFO_TO( MON_LOG, msg );
  }
  *theString = currStr;
  return 0;
} /* End of getString */


/* Declare a default ID */
int declareDefaultId() {
  int status = 0;
  struct sockaddr_in name;
  unsigned int namelen = sizeof( name );
  char *hostName;
  struct hostent *peer = NULL;
  char id[ MP_MAX_CHARS ];
 
  if ( getpeername( 0, (struct sockaddr *) &name, &namelen ) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon getpeername failed errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_SYS_ERROR;
  }
  if ( status == 0 ) {
    hostName = inet_ntoa( name.sin_addr );
    peer = gethostbyaddr( (unsigned char *)&name.sin_addr,
			  sizeof( long32 ),
			  AF_INET );
    if ( peer == NULL ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "mpDaemon gethostbyaddr failed \
hostname:\"%s\" errno:%d (%s)",
		  hostName, h_errno, strerror(h_errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_SYS_ERROR;
    }
  }

  if ( status == 0 ) {
    remoteHostname = strdup( peer->h_name );
    strncpy( &id[ 1 ], peer->h_name, MP_MAX_CHARS-1 );
    id[ 0 ] = '@';
    id[ MP_MAX_CHARS-1 ] = 0;
    if ( ( status = monitorDeclareMp( id ) ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "mpDaemon monitorDeclareMp(%s) failed status:(%d x%08x)",
		  id, status, status );
	ERROR_TO( MON_LOG, msg );
      }
    } else {
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg), "mpDaemon monitorDeclareMp(%s) OK", id );
	INFO_TO( MON_LOG, msg );
      }
    }
  }
  return status;
} /* End of declareDefaultId */


/* Open the link with the monitor buffer. Returns to the client the
 * appropriate status, according to the monitor library response.
 *
 * Returns:
 *   0    => OK
 *   else => fatal error
 */
int openTheLink() {
  char *bufferName;
  int status;

  LOG_DETAILED INFO_TO( MON_LOG, "mpDaemon: opening the link" );

  if ( getString( &bufferName ) != 0 ) return 1;
  if ( ( status = monitorSetDataSource( bufferName )) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "mpDaemon Cannot setDataSource(%s) status:(%d x%08x)",
		bufferName, status, status );
      ERROR_TO( MON_LOG, msg );
    }
  }
  status = sendReply( status );
  free( bufferName );

  fromMemory = monitorFromMemory();

  if ( status == 0 ) {
    if ( ( status = declareDefaultId() ) != 0 ) {
      LOG_NORMAL ERROR_TO( MON_LOG, "mpDaemon: failed to declare default ID" );
    }
  }
  
  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf(SP(msg), "mpDaemon link open returns:(%d x%08x)", status, status);
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of openTheLink */


/* Serve a "flush" command */
int flushEvents() {
  int status, status1;

  if ( ( status = monitorFlushEvents() ) != 0 )
    LOG_DEVELOPER ERROR_TO( MON_LOG, "mpDaemon: monitorFlushEvents failed" );
  
  if ( ( status1 = sendReply( status ) ) != 0 )
    LOG_DEVELOPER
      ERROR_TO( MON_LOG, "mpDaemon: flushEvents/sendReply failed" );

  return status1;
} /* End of flush events */


/* Receive and declare a monitor policy table.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int declareTable() {
#define MAX_ELEMENTS 150
  int   curr;
  int   status;
  int   status1;
  int   goOn = TRUE;
  char *table[ MAX_ELEMENTS+1 ];

  LOG_DEVELOPER INFO_TO( MON_LOG, "mpDaemon: declaring table" );

  for ( curr = 0; curr != MAX_ELEMENTS; curr++ )
    table[ curr ] = NULL;
  
  curr = 0;
  do {
    if ( ( status = getString( &table[ curr ] ) ) == 0 ) {
      if ( ( goOn = strlen( table[ curr ] ) != 0 ) ) {
	if ( curr == MAX_ELEMENTS ) {
	  LOG_NORMAL ERROR_TO( MON_LOG, "mpDaemon: table too big" );
	  status = MON_ERR_TABLE_TOO_BIG;
	} else curr++;
      }
    }
  } while ( status == 0 && goOn );

  if ( status == 0 )
    status = monitorDeclareTableWithAttributes( table );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "mpDaemon/declare table replying:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  status1 = sendReply( status );
  for ( curr = 0;
	curr != MAX_ELEMENTS && strlen( table[ curr ] ) != 0 ;
	curr++ )
    free( table[ curr ] );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "mpDaemon/declareTable Return:(%d x%08x)",
	      status1, status1 );
    if ( status1 == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status1;
} /* End of declareTable */


int declareId() {
  char *id;
  int status = getString( &id );

  if ( status != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "mpDaemon/declareId: failed to fetch ID" );
  } else {
    if ( ( status = monitorDeclareMp( id ) ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		 "mpDaemon/declareId monitorDeclareMp(%s)\
 failed status:(%d x%08x)",
		 id, status, status );
	INFO_TO( MON_LOG, msg );
      }
    } else {
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg), "mpDaemon/declareId monitorDeclareMp(%s) OK", id );
	INFO_TO( MON_LOG, msg );
      }
    }
    free( id );
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf(SP(msg), "mpDaemon/declareId return:(%d x%08x)", status, status);
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of declareId */


#ifdef UNUSED_CODE
/* This code is unused in the final version of the mpDaemonMain, but it is
 * left in as a trace in case the porting to other architecture(s) needs
 * some hints on how to peek at the input channel. These are all things we
 * have tried out on AIX without success, as the procedure failed to detect
 * abnormal link termination when no producer was present */
#include <poll.h>
#include <stropts.h>

int fdsInitd = FALSE;
struct pollfd fds[3];

/* Peek at the command channel, waiting for input.
 *
 * Returns:
 *    0   => nothing available
 *    > 0 => command available
 *    < 0 => error
 */
int peekCommandChannel() {
  int inLen;
  int so_error;
  int len;

  /* A quick check if the channel is still open */
  if ( feof( stdin ) || feof( stdout ) ) return -1;

  /* A more detailed check if the channel is still open */
  if ( !fdsInitd ) {
    int one;
    
    fds[0].fd = inFile;
    fds[1].fd = outFile;
    fds[2].fd = errFile;
    fds[0].events = POLLIN;
    fds[1].events = POLLOUT;
    fds[2].events = POLLOUT;
    fds[0].revents = 0;
    fds[1].revents = 0;
    fds[2].revents = 0;
    
    one = 1;
    if ( setsockopt( 0,
		     SOL_SOCKET,
		     SO_KEEPALIVE,
		     (char *)&one,
		     sizeof( one ) ) < 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "mpDaemon Failed to setsockopt errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return -1;
    }

    fdsInitd = TRUE;
  }
  if ( poll( fds, 3, 0 ) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon Cannot poll errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  if ( ( fds[0].revents & ( POLLHUP | POLLERR | POLLNVAL ) ) != 0 ||
       ( fds[1].revents & ( POLLHUP | POLLERR | POLLNVAL ) ) != 0 ||
       ( fds[2].revents & ( POLLHUP | POLLERR | POLLNVAL ) ) != 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "mpDaemon: channel closed (poll)" );
    return -1;
  }

  /* Another in-deep check to see if the channel is still open */
  so_error = 0;
  len = sizeof( int );
  if ( getsockopt( 0,
		   SOL_SOCKET,
		   SO_ERROR,
		   (char *)&so_error,
		   &len ) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon Failed to getsockopt errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  if ( so_error != 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "mpDaemon: channel closed (getsockopt)" );
    return -1;
  }
    
  /* Now we see how much data is available on the input channel */
  if ( ioctl( inFile, FIONREAD, &inLen ) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon Cannot FIONREAD errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  if ( inLen < 4 )
    /* Not enough data or no data at all */
    return 0;

  /* Enough data: command is present */
  return 1;
} /* End of peekCommandChannel */
#endif


/* Get the next input command.
 *
 * Returns:
 *    0    => OK
 *    else => error
 *
 * Sets:
 *    command global variable to the command on input, zero for no command
 */
int getCommand() {
  int   len;
  char *ptr;

  command = 0;
  ptr = (char *)&command;

  /* This could be a good place to use the peekCommandChannel, if it only
   * would work... */
  
  /* Have a peek at the input channel in non-blocking mode,
   * to avoid blocking in case no command is available */
  if ( setNonBlocking() != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "mpDaemon: failed to set non-blocking (getCommand)" );
    return -1;
  }

  /* Peek at the input channel. This returns either "would block" (no input
   * present), "interrupted" (our timer fired) or OK with length zero
   * (strange but true, this - at least on our architectures - means that
   * the channel has been closed) */
  if ( ( len = recv( inFile, (void *)ptr, 4, MSG_PEEK ) ) < 0 ) {
    if ( errno == EINTR || errno == EWOULDBLOCK ) return 0;
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon Input channel closed (peek) errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  if ( len == 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "mpDaemon: input channel closed (peek - 0)" );
    return -1;
  }

  /* We have a command waiting: put in blocking mode and read the command */
  if ( setBlocking() != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "mpDaemon: failed to set blocking (getCommand)" );
    return -1;
  }
  command = -1;
  len = 0;
  do {
    int l = recv( inFile, (void *)ptr, 4 - len, 0 );
    if ( l < 0 ) {
      LOG_NORMAL ERROR_TO( MON_LOG, "mpDaemon: input channel closed (recv)" );
      return -1;
    } else {
      len += l;
      ptr += l;
    }
  } while ( len != 4 );
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "mpDaemon Received command (%d x%08x)",
	      command, command );
    INFO_TO( MON_LOG, msg );
  }

  return 0;
} /* End of getCommand */


/* Our signal handler, to intercept I/O signals generated by the socket */
void sigHandler( int sigNum ) {
  LOG_DEVELOPER
    ERROR_TO( MON_LOG, "mpDaemon: I/O signal intercepted, closing link" );
  terminate = TRUE;
  monitorLogout();
} /* End of sigHandler */


/* Initialise our internal variables.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int initVars() {

  inFile = fileno( stdin );
  outFile = fileno( stdout );
  errFile = fileno( stderr );
  
  if ( signal( SIGPIPE, sigHandler ) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg), 
		"mpDaemon Cannot install signal handler errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  return 0;
} /* End of initVars */


/* Init the link, send the first reply to select endianness.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int initLink() {
  int status;
  int firstReply = FIRST_REPLY;
  
  if ( ( status = send( outFile, (void *)&firstReply, 4, 0 )) != 4 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"mpDaemon Failed to send first reply \
status:%d errno:%d (%s)",
		status, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return 1;
  }
  return 0;
} /* End of initLink */


/* The mpDaemon main program */
int main( int argc, char **argv ) {
  initLogLevel();
  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf( SP(msg), "mpDaemon version %s starting", VID );
    INFO_TO( MON_LOG, msg );
  }

  monitorSilentParsing = TRUE;

  monitorParseConfigFile( MONITOR_MPDAEMON_CONFIGURATION );

  if ( initVars() != 0 ) return 1;

  if ( initLink() != 0 ) return 1;

  do {
    do {
      if ( getCommand() < 0 )
      {
	terminate = TRUE;
      }
      else if ( command != 0 )
      {
	if ( command == OPEN_LINK ) {
	  terminate = openTheLink() != 0;
	} else if ( command == DECLARE_TABLE ) {
	  terminate = declareTable() != 0;
	  online = TRUE;
	} else if ( command == FLUSH_EVENTS ) {
	  terminate = flushEvents() != 0;
	} else if ( command == DECLARE_ID ) {
	  terminate = declareId() != 0;
	} else {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "mpDaemon Received unknown command (%d x%08x)",
		      command, command );
	    ERROR_TO( MON_LOG, msg );
	  }
	  terminate = TRUE;
	}
      }
      if ( !online && !terminate ) sleep( 1 );
    } while ( !online && !terminate );

    if ( !terminate )
      terminate = ( getAndSendNextEvent() != 0 );
    
  } while (!terminate);

  LOG_DEVELOPER
    INFO_TO( MON_LOG, "mpDaemon: ending" );
  
  monitorLogout();
  
  return 0;
} /* End of main */
