#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C function monitorSetDataSource;
#pragma link C function monitorDeclareTable;
#pragma link C function monitorDeclareTableWithAttributes;
#pragma link C function monitorDeclareMp;
#pragma link C function monitorDecodeError;
#pragma link C function monitorGetEvent;
#pragma link C function monitorGetEventDynamic;
#pragma link C function monitorSetWait;
#pragma link C function monitorSetNowait;
#pragma link C function monitorControlWait;
#pragma link C function monitorSetSwap;
#pragma link C function monitorFlushEvents;
#pragma link C function monitorLogout;

#endif
