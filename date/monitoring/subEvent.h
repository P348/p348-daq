#define MAX_TDC_DATA 2048
#define MAX_TDC_HEAD 512
struct subeventStruct {
  int decode_err;
  int err;
  int type;
  int source;
  int size;
  int event;
  int spill;
  int stat;
  int format;
  int errwords;
  int tcserr;
  int status;       
  int tdc_error;
  int tdc_nr_header;
  int tdc_header_time[MAX_TDC_HEAD];
  int tdc_header_event[MAX_TDC_HEAD];
  int tdc_tbo;
  int tdc_locked;
  int tdc_nr_data;
  int tdc_chan[MAX_TDC_DATA]; /* 6 bit chip * 8 + channel */
  int tdc_time[MAX_TDC_DATA]; /* 16 bit */
  int tdc_port[MAX_TDC_DATA]; /* 4 bit port number */
};

void subEvent_decode(int, int *, struct subeventStruct * );
void Event_check(int, struct subeventStruct *, int *);

//#define min(x,y) ((x)>(y)?(y):(x))
//#define max(x,y) ((x)>(y)?(x):(y))


