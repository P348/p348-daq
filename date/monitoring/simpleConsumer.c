#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "monitor.h"
#include "testLib.h"

int buffer[ 10000 ];

char  **config;
char  **events;
pid_t   pid;
int     status;

int configError( char **argv, int where ) {
  fprintf( stderr,
	   "%s: configuration file \"%s\" invalid format, test aborted (%d)\n",
	   argv[0], argv[1], where );
  return 2;
}

void dumpVars() {
  if ( debug ) {
    int i;
    
    printf( "Data source: \"%s\"\n", dataSource );
    if ( mustConsume )
      printf( "Must consumer selected\n" );
    printf( "Configuration:\n" );
    for ( i=0; config[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, config[i] );
    printf( "\t----------\n" ); 
    printf( "Events:\n" );
    for ( i=0; events[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, events[i] );
    printf( "\t----------\n" );
  }
}

int main( int argc, char **argv ) {
  int action;
  
  pid = getpid();

  printf( "%08lx   *** Monitor simple consumer starting ***\n",
	  (long int)pid );

  if ( argc < 2 ) return usage( argv );

  /* Parse the flags might be used later for the config */
  if ( monParseFlags( argc, argv, TRUE ) != 0 ) return usage( argv );

  /* First parse the configuration file */
  if ( (config = monParseConfig( consumerName, argv[1] ) ) == NULL )
    return configError( argv, 1 );
  if ( (config = parse( config )) == NULL)
    return configError( argv, 2 );
  if ( (events = monParseConfig( "Events", argv[1] ) ) == NULL )
    return configError( argv, 3 );

  /* Parse the flags who might override some of the parameters set by
   * the configuration file */
  if ( monParseFlags( argc, argv, TRUE ) != 0 ) return usage( argv );

  dumpVars();
  initVars();

  if ( ( status = monitorDeclareMp( "DATE simple consumer" ) ) != 0 ) {
    printf( "%08lx: monitorDeclareMp failed, %s",
	    (long int)pid, monitorDecodeError( status ) );
    return 1;
  }

  if ( ( status = monitorSetDataSource( dataSource ) ) != 0 ) {
    printf( "%08lx: monitorSetDataSource(%s) failed, status: %d\n",
	    (long int)pid, dataSource, status );
    return 1;
  }
  if ( debug )
    printf( "%08lx: monitorSetDataSource called OK\n", (long int)pid );
   

  if ( config[0] != 0 ) {
    config = createConfigTable( config );
    if ( (status = monitorDeclareTable( config )) != 0 ) {
      printf( "%08lx: monitorDeclareTable status:0x%08x errno:%d (%s)\n",
	      (long int)pid, status, errno, strerror(errno) );
      return 1;
    }
    if ( debug )
      printf( "%08lx: monitor table declared OK\n", (long int)pid );
  }

  action = '\n';
  do {
    printf( "q[uit]|n[ext event]: " ); fflush( stdout );
    if ( (action = getchar() ) != '\n' ) {
      int c;
      do { c = getchar(); } while ( c != '\n' && c != EOF);
    } else {
      action = 'n';
    }

    if ( action == EOF ) action = 'q';
    if ( action == 'q' ) {
      printf( "Quitting\n" );
    } else if ( action == 'n' ) {
      if ( (status = monitorGetEvent( buffer, sizeof( buffer ))) != 0 ) {
	printf( "Error getting next event: %d (0x%08x): %s\n",
		status, status,
		monitorDecodeError( status ) );
	if ( status == MON_ERR_EVENT_TRUNCATED ) dumpEvent( buffer );
      } else {
	if ( debug ) printf( "Event got OK\n" );
	if ( buffer[ 0 ] == 0 ) {
	  printf( "No event(s) available\n" );
	} else {
	  dumpEvent( buffer );
	}
      }
    } else {
      printf( "Unrecognized command \'%c\'\n", action );
    }
  } while ( action != 'q' );

  return 0;
}
