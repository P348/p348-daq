/* Monitor mpDaemon routines
 * =========================
 *
 * This is the module that contains all the routines related to the
 * access to the DATE monitoring scheme via network sockets.
 *
 * Module history:
 *  1.00  17-Aug-98 RD   Created
 *  1.01  24-Aug-98 RD   First public release
 *  1.02  12-Oct-98 RD   Mods for endianness selection added
 *                       Bug with swallow event fixed (missing free)
 *  1.03  14-Apr-99 RD   Event swapped bit handling added
 *  1.04  24-Jun-99 RD	 Swapping needed flag reset added
 *  1.05   5-Jul-99 RD   Exchange of monitor attributes added
 *  1.06  30-Nov-00 RD   Updated to DATE 4.x
 *  1.07  17-Jan-01 RD   Exchange of DATE_SITE via separate msg by socket
 */
#define VID "1.07"

#include <stdio.h>
#include <ctype.h>
#include <errno.h> 
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#ifdef SunOS
#include <sys/filio.h>		/* For FIONREAD */
#else
#include <sys/ioctl.h>		/* For FIONREAD */
#endif

#include "event.h"
#include "monitorInternals.h"


#define DESCRIPTION "DATE V3 mpDaemon library"
#ifdef AIX
static
#endif
char mpDaemonIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                       """ """ VID """ compiled """ __DATE__ """ """ __TIME__;


int channel;   			/* The channel used for socket I/O */
int monitorCurrentStatus = 0;	/* The current command/reply status */

int monitorSwappingNeeded;
int monitorByteSwappingRequested = TRUE;
int monitorWordSwappingRequested = FALSE;

/* Entry to swap the bytes of an 32-bit word, to adapt to
 * a different endianness
 *
 * Parameter:
 *   inWord  The word whose bytes need to be swapped
 *
 * Returns:
 *   The word with bytes swapped
 */
int monitorSwapBytes( long32 inWord ) {
  return ( inWord >> 24 & 0x000000ff ) |
         ( inWord >>  8 & 0x0000ff00 ) |
         ( inWord <<  8 & 0x00ff0000 ) |
         ( inWord << 24 & 0xff000000 );
} /* End of monitorSwapBytes */


/* Entry to swap the 16-bit words of an 32-bit word, to adapt to
 * a different endianness
 *
 * Parameter:
 *   inWord  The word whose 16-bit words need to be swapped
 *
 * Returns:
 *   The word with 16-bit words swapped
 */
int monitorSwapWords( long32 inWord ) {
  return ( inWord >> 16 & 0x0000ffff ) | ( inWord << 16 & 0xffff0000 );
} /* End of monitorSwapWords */


/* Apply the byte-swapping algorithm to a complete buffer.
 *
 * Parameters:
 *    buffer     The buffer to swap
 *    size       The size of the buffer in bytes
 *    swapBytes  Swap the bytes within each word
 *    swapWords  Swap the 16-bit words within each word
 */
void monitorSwapBuffer( void *buffer,
			int   size,
			int   swapBytes,
			int   swapWords ) {
  if ( !monitorSwappingNeeded ) return;	      /* No swapping needed */
  if ( (!swapBytes) && (!swapWords) ) return; /* No swapping needed */
  if ( size > 0 ) {
    int i;
    long32 *intPtr = (long32 *)buffer;
    int remainder = size % 4;
    
    /* First swap the integer integers */
    for ( i = ( size - remainder ) >> 2; i != 0; i-- ) {
      if ( swapBytes )
	*intPtr = monitorSwapBytes( *intPtr );
      if ( swapWords )
	*intPtr = monitorSwapWords( *intPtr );
      intPtr++;
    }
    /* Now, what do we do with the tail? */
  }
} /* End of monitorSwapBuffer */


/* Read a block of binary data, 32 bit words.
 *
 * Parameters:
 *   buffer          pointer to the buffer
 *   requestedSize   how much data is expected
 *   doByteSwap      apply byte swapping (if necessary)
 *   doWordSwap      apply 16-bit word swapping (if necessary)
 *
 * Returns:
 *   Number of bytes actually read
 */
int monitorReadBlock( void *buffer,
		      int requestedSize,
		      int doByteSwap,
		      int doWordSwap ) {
  int   remainingSize = requestedSize;
  int   totalSize = 0;
  int   nBytes;
  char *ptr = buffer;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorReadBlock(%p,%d,%s,%s)",
	      buffer,
	      requestedSize,
	      doByteSwap ? "TRUE" : "FALSE",
	      doWordSwap ? "TRUE" : "FALSE" );
    INFO_TO( MON_LOG, msg );
  }
  
  do {
    if ( ( nBytes = recv( channel, ptr, remainingSize, 0 ) ) != EOF ) {
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorReadBlock recv returns %d byte%s",
		  nBytes, nBytes != 1 ? "s" : "" );
	INFO_TO( MON_LOG, msg );
      }
      ptr += nBytes;
      remainingSize -= nBytes;
      totalSize += nBytes;
    }
  } while (( remainingSize != 0 ) && ( nBytes != 0 ) && ( nBytes != EOF ));
  monitorSwapBuffer( buffer, totalSize, doByteSwap, doWordSwap );
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorReadBlock read %d byte%s",
	      totalSize, totalSize == 1 ? "" : "s" );
    INFO_TO( MON_LOG, msg );
  }
  return totalSize;
} /* End of monitorReadBlock */


/* Send the given 32-bits entity as binary.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorSendIntBin( long32 theInt ) {
  int status;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorSendIntBin((%d x%08x))", theInt, theInt );
    INFO_TO( MON_LOG, msg );
  }

  if ( monitorSwappingNeeded )
    theInt = monitorSwapBytes( theInt );
  
  if ( ( status = send( channel, (void *)&theInt, 4, 0 ) ) != 4 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorSendIntBin Failed to send (%d x%08x)\
 status:%d errno:%d (%s)",
		theInt, theInt, status, errno, strerror(errno) );
    }
    return MON_ERR_SYS_ERROR;
  }
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSendIntBin: normal completion" );
  return 0;
} /* End of monitorSendIntBin */


/* Send the given string.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorSendString( char *theString ) {
  int status;
  int len = strlen( theString ) + 1;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorSendString(%s)", theString );
    INFO_TO( MON_LOG, msg );
  }

  if ( theString[ len-2 ] == '\n' )
    len--; 			/* Skip termination for plain ASCII text */
  if ( ( status = send( channel, theString, len, 0 ) ) != len ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorSendString Failed to send \"%s\" \
status:%d (expected:%d) errno:%d (%s)",
		theString, status, len, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }
   LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSendString: normal completion" );
  return 0;
} /* End of monitorSendString */


/* Send the given table.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorSendTable( monitorRecordPolicy *theTable ) {
  int status;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSendTable: starting" );

  do {
    status = monitorSendString( theTable->eventType );
    if ( status == 0 )
      status = monitorSendString( theTable->monitorType );
    if ( status == 0 )
      status = monitorSendString( theTable->monitorAttributes[0] == 0 ?
				  "*" : theTable->monitorAttributes );
    theTable = theTable->next;
  } while ( status == 0 && theTable != NULL );

  if ( status == 0 )
    status = monitorSendString( "" );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorSendTable Return:(%d x%08x)", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorSendTable */


/* Receive an 32-bits value in binary form.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorReceiveIntBin( long32 *theWord ) {
  int  status;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorReceiveIntBin(%p)", theWord );
    INFO_TO( MON_LOG, msg );
  }
  *theWord = -1;
  if ( ( status = monitorReadBlock( (void *)theWord,
				    4,
				    TRUE,
				    FALSE) ) != 4 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "monitorReceiveIntBin Failed to receive\
 status:%d errno:%d (%s)",
		status, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorReceiveIntBin Normal completion received:(%d x%08x)",
	      *theWord, *theWord );
    INFO_TO( MON_LOG, msg );
  }
  
  return 0;
} /* End of monitorReceiveIntBin */


/* Receive a string.
 *
 * Returns:
 *   NULL => error
 *   else => pointer to string receibed
 *
 * WARNING: the pointed value should be freed when done!
 */
char *monitorReceiveString() {
  int   currentSize   = 1000;
  int   currentLength = 0;
  char *currentString = (char *)malloc( (size_t)currentSize );

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorReceiveString: starting" );
  
  if ( currentString == NULL ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorReceiveString: failed to malloc (1)" );
    return NULL;
  }
  
  do {
    int status;

    if ( currentLength == currentSize ) {
      char *newString;
      currentSize *= 2;
      if ( ( newString = (char *)malloc( (size_t)currentSize ) ) == NULL ) {
	LOG_NORMAL
	  ERROR_TO( MON_LOG, "monitorReceiveString: failed to malloc (2)" );
	free( currentString );
	return NULL;
      }
      strncpy( newString, currentString, currentSize );
      free( currentString );
      currentString = newString;
    }
    if ( (status = recv(channel,&currentString[ currentLength ],1,0)) != 1 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorReceiveString Failed to receive\
 status:%d errno:%d (%s)",
		  status, errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return NULL;
    }
  } while( currentString[ currentLength++ ] != 0 );

  LOG_DEVELOPER {
    char msg[ 2048 ];
    snprintf( SP(msg), "monitorReceiveString Return:\"%s\"", currentString );
    INFO_TO( MON_LOG, msg );
  }

  return currentString;
} /* End of monitorReceiveString */


/* Swallow an event.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorSwallowEvent( int eventLen ) {
  char *dummy;
  int   status;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorSwallowEvent(%d)", eventLen );
    INFO_TO( MON_LOG, msg );
  }
  if ( ( dummy = (char *)malloc( (size_t)(eventLen-4) ) ) == NULL ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorSwallowEvent Malloc for %d bytes failed \
errno:%d (%s)",
		eventLen-4, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_MALLOC;
  }
  status = monitorReadBlock( dummy, eventLen-4, FALSE, FALSE );
  free( dummy );
  if ( status != ( eventLen-4 ) ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorSwallowEvent Failed to receive \
status:%d (expected:%d) errno:%d (%s)",
		status, eventLen-4, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSwallowEvent: normal completion" );
  return 0;
} /* End of monitorSwallowEvent */


/* Send the given command and wait for the reply
 *
 * Parameters:
 *   command	The command to be sent
 *   parameter  Optional parameter (actual value depends on command)
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorSendCommand( int command, void *parameter ) {
  int status = 0;
  int waitForReply = TRUE;
  char line[1000];

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSendCommand((%d x%08x)): starting ",
	      command, command );
    INFO_TO( MON_LOG, msg );
  }
  
  if ( channel < 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorSendCommand: channel is closed" );
    status = 1;
  }

  if ( status == 0 ) {
    if ( command != SEND_SITE ) {
      if ( ( status = monitorSendIntBin( command ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG, "monitorSendCommand: monitorSendIntBin failed" );
    }
  }
  
  if ( status == 0 ) {

    switch ( command ) {
    case OPEN_LINK :
      if ( ( status = monitorSendString( parameter ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorSendCommand: failed to send buffer name \
for OPEN_LINK command" );
      break;
      
    case DECLARE_TABLE :
      if ( ( status = monitorSendTable( parameter ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorSendCommand: failed to send monitor table \
for DECLARE_TABLE command" );
      break;

    case DECLARE_ID :
      if ( ( status = monitorSendString( parameter ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorSendCommand: failed to send mpId for\
 DECLARE_ID command" );

    case SEND_SITE :
      snprintf( SP(line), "%s\n", (char *)parameter );
      if ( ( status = monitorSendString( line ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorSendCommand: failed to send DATE_SITE for\
 SEND_SITE command" );
    }
  }

  if ( command != DECLARE_ID && command != SEND_SITE ) {
    monitorCurrentStatus = -1;
    while ( status == 0 && waitForReply ) {
      int reply;
      
      if ( ( status = monitorReceiveIntBin( &reply ) ) != 0 ) {
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorSendCommand: failed to receive reply" );
      } else {
	if ( reply == REPLY_FLAG ) {
	  waitForReply = FALSE;
	  if ( ( status=monitorReceiveIntBin( &monitorCurrentStatus ) ) != 0 )
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorSendCommand: failed to receive status" );
	} else {
	  if ( ( status = monitorSwallowEvent( reply ) ) != 0 )
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorSendCommand: failed to swallow event" );
	}
      }
    }
  } else {
    monitorCurrentStatus = 0;
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSendCommand Command %d returned status:(%d x%08x) \
monitorSendCommand status:(%d x%08x)",
	      command, monitorCurrentStatus, monitorCurrentStatus,
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorSendCommand */


/* Find the socket number to connect to.
 *
 * Returns:
 *   the socket number
 */
int monitorFindSocketNumber() {
  char *environmentPort;
  int   port;
  
  if ( (environmentPort = getenv( MONITOR_SOCKET_ENVIRONMENT )) != NULL ) {
    if ( sscanf( environmentPort, "%d", &port ) == 1 ) {
      LOG_DETAILED {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorFindSocketNumber Port number extracted from\
 environment variable \"%s\":\"%s\"=>%d",
		  MONITOR_SOCKET_ENVIRONMENT, environmentPort, port );
	INFO_TO( MON_LOG, msg );
      }
    } else {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		 "monitorFindSocketNumber Failed to extract port number\
 from environment variable \"%s\":\"%s\"",
		  MONITOR_SOCKET_ENVIRONMENT, environmentPort );
	ERROR_TO( MON_LOG, msg );
      }
      port = MONITOR_DEFAULT_SOCKET;
    }
  } else {
    LOG_DETAILED
      INFO_TO( MON_LOG,
	       "monitorFindSocketNumber: socket enviroment variable \
not defined, using default value" );
    port = MONITOR_DEFAULT_SOCKET;
  }

  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorFindSocketNumber Socket port number:%d", port );
    INFO_TO( MON_LOG, msg );
  }
  return port;
} /* End of monitorFindSocketNumber */


/* Open the monitor channel to the given monitor host
 *
 * Parameter:
 *   hostName	The monitor host
 *
 * Returns:
 *   < 0  => error
 *   else => monitor channel number
 */
int monitorOpenSocket( char *hostName ) {
  int                 portNo;
  struct hostent     *hp;
  struct sockaddr_in  sin;
  int                 theChannel;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorOpenSocket(%s)", hostName );
    INFO_TO( MON_LOG, msg );
  }
  
  portNo = monitorFindSocketNumber();
  
  if ( (hp = gethostbyname( hostName )) == NULL ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorOpenSocket gethostbyname(%s) failed errno:%d (%s)",
		hostName, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  if ( (theChannel = socket( AF_INET, SOCK_STREAM, 0 )) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorOpenSocket socket creation failed errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  sin.sin_family = hp->h_addrtype;
  memcpy( &sin.sin_addr, hp->h_addr, hp->h_length ); 
  sin.sin_port = htons( portNo );
  
  if (connect( theChannel,
#ifdef AIX
               (const struct sockaddr*)
#endif
#ifdef OSF1
               (const struct sockaddr*)
#endif
#ifdef SunOS
               (struct sockaddr*)
#endif
#ifdef Linux
               (const struct sockaddr*)
#endif
               &sin, (int)sizeof( sin ) ) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorOpenSocket failed to connect errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return -1;
  }
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorOpenSocket: normal completion" );
  return theChannel;
} /* End of monitorOpenSocket */


/* Close the monitor channel
 *
 * Returns:
 *   0    =>  OK
 *   < 0  =>  error
 */
int monitorCloseChannel() {
  if ( channel > 0 ) {
    if ( close( channel ) < 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorCloseChannel failed to close channel errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_SYS_ERROR;
    }
  }
  channel = -1;
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorCloseChannel: channel closed OK" );
  return 0;
} /* End of monitorCloseChannel */


/* Open the monitor link
 *
 * Parameter:
 *   bufferName		The remote monitor buffer name
 *
 * Returns:
 *   0 => OK
 *   else => error
 */
int monitorOpenLink( char *monitorBufferName ) {
  int status;
  int firstReply;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorOpenLink(%s)", monitorBufferName );
    INFO_TO( MON_LOG, msg );
  }

  /* Get the first reply from the daemon. This will confirm that the link
   * is properly set and will also help to figure out if we have the same
   * endianness as the remote daemon */
  if ( ( status = monitorReceiveIntBin( &firstReply ) ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "monitorOpenLink receive int for first reply failed\
 status:(%d x%08x)",
	       status,
	       status );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_NOCONNECT;
  }
  
  if ( firstReply == FIRST_REPLY ) {
    LOG_DEVELOPER
      INFO_TO( MON_LOG,
	       "monitorNetConnectTo: first reply OK, same endianness" );
  } else if ( firstReply == monitorSwapBytes( FIRST_REPLY ) ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorNetConnectTo: first reply OK, other endianness" );
      monitorSwappingNeeded = TRUE;
  } else {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorNetConnectTo unexpected first reply\
 expected:x%08x got:x%08x",
		FIRST_REPLY,
		firstReply );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_NOCONNECT;
  }

  /* Now we can send the "open link" command. The associated parameter is
   * the name of the remote monitoring buffer */
  if ( ( status = monitorSendCommand( OPEN_LINK, monitorBufferName ) ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorOpenLink: failed to send OPEN_LINK command" );
    return status;
  }
  if ( monitorCurrentStatus != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorOpenLink error with OPEN_LINK:(%d x%08x)",
		monitorCurrentStatus, monitorCurrentStatus );
      ERROR_TO( MON_LOG, msg );
    }
    return monitorCurrentStatus;
  }
  LOG_DETAILED
    INFO_TO( MON_LOG, "monitorOpenLink: normal completion" );
  return 0;
} /* End of monitorOpenLink */


/* Close the monitor link
 *
 * Returns:
 *   0 => OK
 *   else => error
 */
int monitorCloseLink() {
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorCloseLink: link closed OK" );
  return 0;
} /* End of monitorCloseLink */


/* Disconnects from the remote server
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorNetDisconnect() {
  int status;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorNetDisconnect: starting" );
  
  if ( ( status = monitorCloseLink() ) == 0 ) {
    status = monitorCloseChannel();
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorNetDisconnect return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorNetDisconnect */


/* Connect to the given host using the given monitor buffer
 *
 * Parameters:
 *    hostName		The target host
 *    bufferName	The monitor buffer name
 *
 * Returns:
 *    0 =>    OK
 *    else => error
 */
int monitorNetConnectTo( char *hostName,
			 char *bufferName,
			 monitorRecordPolicy *monitorPolicyTable ) {
  int status = 0;
  int closeLink = FALSE;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorNetConnectTo(\"%s\",\"%s\",<table>)",
	      hostName, bufferName );
    INFO_TO( MON_LOG, msg );
  }

  /* Do not swap, unless required */
  monitorSwappingNeeded = FALSE;
  
  if ( ( channel = monitorOpenSocket( hostName ) ) < 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorNetConnectTo: failed to open monitor channel" );
    status = MON_ERR_NOCONNECT;
  } else {
    if ( (monitorSendCommand( SEND_SITE, getenv( "DATE_SITE" ))) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorOpenSocket: failed to SEND_SITE command" );
      closeLink = TRUE;
    } else {
      LOG_DETAILED
	INFO_TO( MON_LOG,
		 "monitorNetConnectTo: monitor channel opened OK" );
    }
  }

  if ( status == 0 ) {
    if ( monitorOpenLink( bufferName ) != 0 ) {
      LOG_NORMAL {
	ERROR_TO( MON_LOG,
		  "monitorNetConnectTo: failed to create monitor link" );
      }
      closeLink = TRUE;
    }
  }

  if ( closeLink ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorNetConnectTo: closing link due to previous error(s)" );
    monitorCloseLink();
    monitorCloseChannel();
    status = MON_ERR_NOCONNECT;
  }
  
  if ( status == 0 ) {
    LOG_DETAILED {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorNetConnectTo connected to monitor host \"%s\"\
 buffer:\"%s\"",
	       hostName, bufferName );
      INFO_TO( MON_LOG, msg );
    }
  } else {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorNetConnectTo failed to connect to monitor\
 host \"%s\" buffer:\"%s\" status:(%d x%08x)",
		hostName, bufferName, status, status );
      ERROR_TO( MON_LOG, msg );
    }
  }

  if ( status == 0 ) {
    if ( ( status = monitorSendPolicyTable( monitorPolicyTable ) ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorNetConnectTo monitorSendPolicyTable failed\
 status:(%d x%08x)",
		  status, status );
	ERROR_TO( MON_LOG, msg );
      }
    }
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf(SP(msg), "monitorNetConnectTo Return:(%d x%08x)", status, status);
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorNetConnectTo */


/* Send the monitor policy table to the remote side of the link.
 *
 * Parameter:
 *   monitorPolicyTable		The monitor policy table
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorSendPolicyTable( monitorRecordPolicy *theTable ) {
  int status;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSendPolicyTable: starting" );
  
  if ( ( status = monitorSendCommand( DECLARE_TABLE, theTable ) ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorSendPolicyTable Error sending \
command DECLARE_TABLE status:%d",
		status );
      ERROR_TO( MON_LOG, msg );
    }
    return status;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSendPolicyTable return:(%d x%08x)",
	      monitorCurrentStatus, monitorCurrentStatus );
    INFO_TO( MON_LOG, msg );
  }
  return monitorCurrentStatus;
} /* End of monitorSendPolicyTable */


/* Execute a flush on the remote site
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorCleanReservationsNetwork() {
  int status;

  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorCleanReservationsNetwork: starting" );
  
  if ( ( status = monitorSendCommand( FLUSH_EVENTS, NULL ) ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorCleanReservationsNetwork Failed to send\
 FLUSH_EVENTS command status:(%d x%08x)",
		status, status );
      ERROR_TO( MON_LOG, msg );
    }
    return status;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCleanReservationsNetwork Return:(%d x%08x)",
	      monitorCurrentStatus, monitorCurrentStatus );
    INFO_TO( MON_LOG, msg );
  }
  return monitorCurrentStatus;
} /* End of monitorCleanReservationsNetwork  */


/* Get the next available event.
 *
 * Parameters:
 *   buffer         Pointer to user buffer (or pointer to pointer for dynamic)
 *   sizeOfBuffer   Size of user buffer (or zero for dynamic buffer)
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int fetchNextEvent( void *buffer, int sizeOfBuffer ) {
  int size;
  int status;
  int toAccept = 0;
  int transferred = 0;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "fetchNextEvent(%p,%d)", buffer, sizeOfBuffer );
    INFO_TO( MON_LOG, msg );
  }

  if ( ( status = monitorReceiveIntBin( &size ) ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"fetchNextEvent Receive for event size failed status:%d",
		status );
      ERROR_TO( MON_LOG, msg );
    }
  } else {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"fetchNextEvent Incoming event size:(%d x%08x)",
		size, size );
      INFO_TO( MON_LOG, msg );
    }
    if ( size == 0 ) {
      int retStat;
      if ( ( status = monitorReceiveIntBin( &retStat ) ) != 0 ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "fetchNextEvent size:0 read for status failed \
status:%d errno:%d (%s)",
		    status, errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
	return status;
      }
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "fetchNextEvent size:0 status:(%d x%08x)",
		  retStat, retStat );
	ERROR_TO( MON_LOG, msg );
      }
      return retStat;
    }
    if ( size < EVENT_HEAD_BASE_SIZE ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "fetchNextEvent event too small size:%d \
expected:%d sizeof(int):%d sizeof(long):%d sizeof(long32):%d \
sizeof(long64):%d sizeof(pointer):%d",
		  size,
		  (int)EVENT_HEAD_BASE_SIZE,
		  (int)sizeof(int),
		  (int)sizeof(long),
		  (int)sizeof(long32),
		  (int)sizeof(long64),
		  (int)sizeof(datePointer));
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_INTERNAL;
    }
  }
  if ( sizeOfBuffer == 0 ) {
    void *ptr = malloc( (size_t)size );

    if ( ptr == NULL ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "fetchNextEvent malloc for %d bytes failed errno:%d (%s)",
		  size, errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_MALLOC;
    } else {
      char **ptr1 = (char **)buffer;
      *ptr1 = ptr;
      sizeOfBuffer = size;
      buffer = ptr;
    }
    
  }
  if ( status == 0 ) {
    toAccept = ( size < EVENT_HEAD_BASE_SIZE?size:EVENT_HEAD_BASE_SIZE ) - 4;
    *(long32 *)buffer = size;
    if ( ( status = monitorReadBlock( buffer+4,
				      toAccept,
				      TRUE,
				      FALSE ) ) != toAccept ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "fetchNextEvent monitorReadBlock/header failed status:%d\
 expected:%d",
		  status, toAccept );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_INTERNAL;
    } else {
      status = 0;
      transferred = 4 + toAccept;
      if ( monitorSwappingNeeded ) {
	/* We had do swap the header: toggle the "event swapped" attribute */
	struct eventHeaderStruct *head = (struct eventHeaderStruct *)buffer;
	FLIP_SYSTEM_ATTRIBUTE( head->eventTypeAttribute, ATTR_EVENT_SWAPPED );
      }
    }
  }
    
  if ( status == 0 &&
       size > EVENT_HEAD_BASE_SIZE &&
       sizeOfBuffer > EVENT_HEAD_BASE_SIZE ) {
    toAccept = ( size > sizeOfBuffer?sizeOfBuffer:size )-EVENT_HEAD_BASE_SIZE;
    status = monitorReadBlock( buffer+EVENT_HEAD_BASE_SIZE,
			       toAccept,
			       monitorByteSwappingRequested,
			       monitorWordSwappingRequested );
    if ( status != toAccept ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "fetchNextEvent monitorReadBlock/data failed status:%d\
 expected:%d",
		  status, toAccept );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_INTERNAL;
    } else {
      status = 0;
      transferred += toAccept;
    }
  }
  if ( status == 0 ) {
    if ( transferred != size ) {
      void *dummy;
      int left;

      LOG_DEVELOPER {
	char msg[1024];
	snprintf( SP(msg),
		  "fetchNextEvent leftover transferred:%d got:%d",
		  transferred, size );
	INFO_TO( MON_LOG, msg );
      }
      status = MON_ERR_EVENT_TRUNCATED;
      left = size - transferred;
      if ( ( dummy = malloc( (size_t)left ) ) == NULL ) {
	LOG_NORMAL
	  ERROR_TO( MON_LOG, "fetchNextEvent: malloc for leftovers failed" );
	status = status == 0 ? MON_ERR_MALLOC : status;
      } else {
	int status1 = monitorReadBlock( dummy, left, FALSE, FALSE );
	free( dummy );
	if ( ( status1 ) != left ) {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "fetchNextEvent monitorReadBlock for leftover failed\
 status:%d expected:%d",
		      status1, left );
	  }
	  if ( status == 0 ) status = MON_ERR_INTERNAL;
	}
      }
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "fetchNextEvent eventLength:%d bufferSize:%d status:(%d x%08x)",
	      size, sizeOfBuffer, status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of fetchNextEvent */


/* Peek the input channel checking for available event(s)
 *
 * Returns:
 *  < 0   => error
 *    0   => no events
 *  > 0   => size of next available event
 */
int peekNextEvent() {
  int inLen;
  
  if ( ioctl( channel, FIONREAD, &inLen ) < 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"peekNextEvent FIONREAD failed errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_INTERNAL;
  }
  if ( inLen < 4 ) {
    /*
    LOG_DEVELOPER
      INFO_TO( MON_LOG, "peekNextEvent: no event on input channel" );
    */
    return 0;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "peekNextEvent event available length:(%d x%08x)",
	      inLen, inLen );
    INFO_TO( MON_LOG, msg );
  }
  return inLen;
} /* End of peekNextEvent */


/* Get the next event available on the TCP/IP channel.
 *
 * Parameters:
 *   mustWait      Boolean flag, TRUE if we can block
 *   buffer        Pointer to user buffer (or to pointer for dynamic memory)
 *   sizeOfBuffer  Size of user buffer (or 0 for dynamic memory)
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorNetNextEvent( int mustWait, void *buffer, int sizeOfBuffer ) {
  int status;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorNetNextEvent(%s,%p,%d)",
	      mustWait ? "TRUE" : "FALSE",
	      buffer,
	      sizeOfBuffer );
    INFO_TO( MON_LOG, msg );
  }
  
  if ( !mustWait ) {
    if ( ( status = peekNextEvent() ) < 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorNetNextEvent: peekNextEvent failed" );
      return status;
    }
    if ( status == 0 ) {
      /* No events available */
      if ( sizeOfBuffer == 0 ) *(void**)buffer = NULL;
      else *(long32 *)buffer = 0;
      return 0;
    }
  }
  status = fetchNextEvent( buffer, sizeOfBuffer );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorNetNextEvent return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }

  return status;
} /* End of monitorNetNextEvent */


/* Transmit our monitoring program ID to the remote side of the link.
 *
 * Parameter:
 *   mpId   The ID of the monitoring program
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorXmitMp( char *mpId ) {
  int status;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorXmitMp(%s)", mpId );
    INFO_TO( MON_LOG, msg );
  }
  
  status = monitorSendCommand( DECLARE_ID, mpId );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorXmitMp Return:(%d x%08x)", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorXmitMp */
