/* Monitor V 3.xx queue support module
 * ===================================
 *
 * Defines:
 *   FULL_CHECKS	Run extra checks at some run-time penalties
 *
 * Module history:
 *  1.00    1-Jul-98 RD   Created
 *  1.01   29-Jul-98 RD	  Entry insertAndGather added
 *  1.02   24-Aug-98 RD   First public release
 *  1.03   25-Aug-98 RD   Absolute pointers changed into relative
 */
#define VID "1.03"

#define NO_LOGGING

#include <stdlib.h>
#include <stdio.h>

#include "monitorInternals.h"

#define DESCRIPTION "DATE V3 monitor queues support"
#ifdef HANDLE_SIZE
#define SIZE_SUPPORT "(with size support)"
#else
#define SIZE_SUPPORT "(without size support)"
#endif
#ifdef AIX
static
#endif
char monitorQueuesSupportIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                                   """ """ VID """ """ SIZE_SUPPORT \
                                   """ compiled """ __DATE__ \
                                   """ """ __TIME__;

/* Return a version descriptive string */
char *queueSupportGetVid() {
  return VID """ """ SIZE_SUPPORT;
} /* End of queueSupportGetVid */


/* Initialise the given queue
 *
 * Returns: 0    -> OK
 *          else -> ERROR
 */
int initQueue( struct queuePtr *theQueue ) {
#ifdef HANDLE_SIZE
  theQueue->size = 0;
#endif
  theQueue->next = 0;
  theQueue->prev = 0;
  return 0;
} /* End of initQueue */


/* Check the physical and logical structure of a queue
 *
 * Returns: 0    -> OK
 *          else -> error
 *
 * On error: set the static variable errDescriptor to a printable string
 */
int checkQueue( struct queuePtr *theQueue ) {
  struct queuePtr *ptr;
#ifdef HANDLE_SIZE
  int size;
#endif
  int numElements;
  int ctr;

  /* Step 1: validate the forward pointers. If the input parameter
   * is not a valid queue or if one of the forward pointers is gone
   * wild, this entry will cause a signal (and probably a core dump).
   * While we run this check we count the number of elements and we
   * compute the total size of the data blocks.
   */
#ifdef HANDLE_SIZE
  size = 0;
#endif
  for ( numElements = 0, ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue;
	ptr = ADD(ptr, ptr->next) ) {
#ifdef HANDLE_SIZE
    size += ptr->size;
#endif
    numElements++;
  }

  /* Step 2: validate the backward pointers. This loop can cause a
   * signal if the input parameter is not a good list descriptor or if
   * the back chain is not correct. While we do the check, we also
   * validate the elements count. As before, we check for NULL pointers.
   */
  for ( ctr = 0, ptr = ADD(theQueue, theQueue->prev);
	ptr != theQueue;
	ptr = ADD(ptr, ptr->prev) ) {
    ctr++;
  }
  if ( ctr != numElements ) return 3;
#ifdef HANDLE_SIZE
  if ( theQueue->size != size ) return 4;
#endif

  /* Step 3: validate sizes and pointers */
  for ( ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue;
	ptr = ADD(ptr, ptr->next) ) {
    struct queuePtr *prev, *next, *curr;
#ifdef HANDLE_SIZE
    if ( ptr->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return 5;
#endif
    prev = ADD(ptr, ptr->prev);
    curr = ADD(prev, prev->next);
    if ( curr != ptr ) return 6;

    next = ADD(ptr, ptr->next);
    curr = ADD(next, next->prev);
    if ( curr != ptr ) return 7;
  }
  
  return 0;
} /* End of checkQueue */


/* Get the number of fragments in a given queue.
 *
 * Return: the number of fragments
 */
int getNumOfFragments( struct queuePtr *theQueue ) {
  int n;
  struct queuePtr *ptr;

  for ( n = 0, ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue;
	ptr = ADD(ptr, ptr->next) ) {
    n++;
  }
  return n;
} /* End of getNumOfFragments */


/* Get the average fragment size of a given queue.
 *
 * Return: the average fragment size
 */
float getAverageFragmentSize( struct queuePtr *theQueue ) {
#ifdef HANDLE_SIZE
  int n;
  struct queuePtr *ptr;
  float totalSize;

  for ( totalSize = 0.0, n = 0, ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue;
	ptr = ADD(ptr, ptr->next) ) {
    totalSize += ptr->size ;
    n++;
  }

  if ( n == 0 ) return 0.0;
  return totalSize / n;
#else
  return -1;
#endif
} /* End of getAverageFragmentSize */

  
/* Insert the given chunk as first element of the given queue.
 *
 * Return: 0    -> OK
 *         else -> error
 */
int insertFirst( struct queuePtr *theQueue, void *chunk ) {
  struct queuePtr *theChunk = chunk;
  struct queuePtr *next;

#ifdef HANDLE_SIZE
  if ( theChunk->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return 1;
  theQueue->size += theChunk->size;
#endif

  next = ADD(theQueue, theQueue->next);
  theChunk->next = SUBTRACT(next, theChunk);
  theChunk->prev = SUBTRACT(theQueue, theChunk);
  theQueue->next = SUBTRACT(theChunk, theQueue);
  next->prev = SUBTRACT(theChunk, next);

  return 0;
} /* End of insertFirst */


/* Insert the given chunk as last element of the given queue.
 *
 * Return: 0    -> OK
 *         else -> error
 */
int insertLast( struct queuePtr *theQueue, void *chunk ) {
  struct queuePtr *theChunk = chunk;
  struct queuePtr *prev;

#ifdef HANDLE_SIZE
  if ( theChunk->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return 1;
  theQueue->size += theChunk->size;
#endif

  prev = ADD(theQueue, theQueue->prev);
  theChunk->prev = SUBTRACT(prev, theChunk);
  theChunk->next = SUBTRACT(theQueue, theChunk);
  theQueue->prev = SUBTRACT(theChunk, theQueue);
  prev->next = SUBTRACT(theChunk, prev);

  return 0;
} /* End of insertLast */


/* Insert the given chunk after the given chunk of the given queue.
 *
 * Return: 0    -> OK
 *         else -> error
 */
int insertAfter( struct queuePtr *theQueue, void *afterThis, void *chunk ) {
  struct queuePtr *theAfter = afterThis;
  struct queuePtr *theChunk = chunk;
  struct queuePtr *ptr;
  struct queuePtr *next;

#ifdef FULL_CHECKS
  for ( ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue && ptr != theAfter;
	ptr = ADD(ptr, ptr->next) );
  if ( ptr != theAfter ) return 1;
#else
  ptr = theAfter;
#endif

#ifdef HANDLE_SIZE
  if ( theChunk->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return 2;
  theQueue->size += theChunk->size;
#endif

  next = ADD(ptr, ptr->next);
  theChunk->prev = SUBTRACT(ptr, theChunk);
  theChunk->next = SUBTRACT(next, theChunk);
  ptr->next = SUBTRACT(theChunk, ptr);
  next->prev = SUBTRACT(theChunk, next);

  return 0;
} /* End of insertAfter */


/* Insert the given chunk before the given chunk of the given queue.
 *
 * Return: 0    -> OK
 *         else -> error
 */
int insertBefore( struct queuePtr *theQueue, void *beforeThis, void *chunk ) {
  struct queuePtr *theBefore = beforeThis;
  struct queuePtr *theChunk = chunk;
  struct queuePtr *ptr;
  struct queuePtr *prev;

#ifdef FULL_CHECKS
  for ( ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue && ptr != theBefore;
	ptr = ADD(ptr, ptr->next) );
  if ( ptr != theBefore ) return 1;
#else
  ptr = theBefore;
#endif

#ifdef HANDLE_SIZE
  if ( theChunk->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return 2;
  theQueue->size += theChunk->size;
#endif

  prev = ADD( ptr, ptr->prev );
  theChunk->prev = SUBTRACT(prev, theChunk);
  theChunk->next = SUBTRACT(ptr, theChunk);
  ptr->prev = SUBTRACT(theChunk, ptr);
  prev->next = SUBTRACT(theChunk, prev);

  return 0;
} /* End of insertBefore */


/* Insert the given chunk in memory order within the given list.
 *
 * Return: 0    -> OK
 *         else -> error
 */
int insertOrdered( struct queuePtr *theQueue, void *chunk ) {
  struct queuePtr *ptr;

  for ( ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue && (unsigned long)ptr < (unsigned long)chunk;
	ptr = ADD(ptr, ptr->next) );

  if ( ptr == theQueue ) return insertLast( theQueue, chunk );
  return insertBefore( theQueue, ptr, chunk );
} /* End of insertOrdered */


/* Insert the given chunk in memory order within the given list and try to
 * gather any super-chunk that this makes possible.
 *
 * Return: 0    -> OK
 *         else -> error
 */
int insertAndGather( struct queuePtr *theQueue, void *chunk ) {
#ifndef HANDLE_SIZE
  return 1;
#else
  struct queuePtr *ptr, *prev, *next, *nextNext;

  for ( ptr = chunk, next = ADD(theQueue, theQueue->next);
	next != theQueue && (unsigned long)next < (unsigned long)ptr;
	next = ADD( next, next->next ) );
  if ( ptr == next ) return 1;
  prev = ADD( next, next->prev );
  prev->next = SUBTRACT( ptr, prev );
  next->prev = SUBTRACT( ptr, next );
  ptr->next  = SUBTRACT( next, ptr );
  ptr->prev  = SUBTRACT( prev, ptr );
  theQueue->size += ptr->size;

  if ( prev != theQueue ) {
    if ( ADD(prev, prev->size + QUEUE_HEAD_LEN) == ptr ) {
      prev->size += ptr->size + QUEUE_HEAD_LEN;
      theQueue->size += QUEUE_HEAD_LEN;
      ptr = prev;
      ptr->next = SUBTRACT(next, ptr);
      next->prev = SUBTRACT(ptr, next);
    }
  }
  if ( next != theQueue ) {
    if ( ADD(ptr, ptr->size + QUEUE_HEAD_LEN) == next ) {
      nextNext = ADD(next, next->next);
      ptr->size += next->size + QUEUE_HEAD_LEN;
      theQueue->size += QUEUE_HEAD_LEN;
      ptr->next = SUBTRACT(nextNext, ptr);
      nextNext->prev = SUBTRACT(ptr, nextNext);
    }
  }
  return 0;
#endif
} /* End of insertAndGather */


/* Extract the first element from the given queue.
 *
 * Return: NULL -> empty list
 *         else -> pointer to the element
 */
void *extractFirst( struct queuePtr *theQueue ) {
  struct queuePtr *theChunk, *next;
  
  if ( theQueue->next == 0 ) return NULL;

  theChunk = ADD(theQueue, theQueue->next);
#ifdef HANDLE_SIZE
  if ( theChunk->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return NULL;
  theQueue->size -= theChunk->size;
#endif

  next = ADD(theChunk, theChunk->next);
  theQueue->next = SUBTRACT(next, theQueue);
  next->prev = SUBTRACT(theQueue, next);

#ifdef FULL_CHECKS
  theChunk->next = 0;	/* This is just for sanity, in case     */
  theChunk->prev = 0;	/* someone tries to re-use the pointers */
#endif

  return theChunk;
} /* End of extractFirst */


/* Extract the last element from the given queue.
 *
 * Return: NULL -> empty list
 *         else -> pointer to the element
 */
void *extractLast( struct queuePtr *theQueue ) {
  struct queuePtr *theChunk, *prev;
  
  if ( theQueue->prev == 0 ) return NULL;

  theChunk = ADD(theQueue, theQueue->prev);

#ifdef HANDLE_SIZE
  if ( theChunk->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return NULL;
  theQueue->size -= theChunk->size;
#endif

  prev = ADD(theChunk, theChunk->prev);
  theQueue->prev = SUBTRACT(prev, theQueue);
  prev->next = SUBTRACT(theQueue, prev);

#ifdef FULL_CHECKS
  theChunk->next = 0;	/* This is just for sanity, in case     */
  theChunk->prev = 0;	/* someone tries to re-use the pointers */
#endif

  return theChunk;
} /* End of extractLast */


/* Extract a given element from the given queue.
 *
 * Return: 0    -> OK
 *         else -> ERROR
 */
int extractElement( struct queuePtr *theQueue, void *chunk ) {
  struct queuePtr *theChunk = chunk;
  struct queuePtr *prev, *next;

#ifdef FULL_CHECKS
  struct queuePtr *ptr;
  
  for ( ptr = ADD(theQueue, theQueue->next);
	ptr != theQueue && ptr != theChunk;
	ptr = ADD(ptr, ptr->next) );
  if ( ptr != theChunk ) return 1; /* Chunk not found in the list... */
#endif

#ifdef HANDLE_SIZE
  if ( theChunk->size < MINIMUM_QUEUE_ELEMENT_SIZE ) return 2;
  theQueue->size -= theChunk->size;
#endif

  prev = ADD(theChunk, theChunk->prev);
  next = ADD(theChunk, theChunk->next);
  prev->next = SUBTRACT(next, prev);
  next->prev = SUBTRACT(prev, next);

#ifdef FULL_CHECKS
  theChunk->next = 0;	/* This is just for sanity, in case     */
  theChunk->prev = 0;	/* someone tries to re-use the pointers */
#endif

  return 0;
} /* End of extractLast */


/* Get the pointer to the element following the given one
 */
void *getNext( struct queuePtr *theElement ) {
  return ADD(theElement, theElement->next);
} /* End of next */


/* Get the pointer to the element preceeding the given one
 */
void *getPrev( struct queuePtr *theElement ) {
  return ADD(theElement, theElement->prev);
} /* End of next */


/* Gather all chunks of an ordered queue into as many super-chunks as
 * possible
 */
void gatherOrderedQueue( struct queuePtr *theQueue ) {
#ifndef HANDLE_SIZE
  return 1;
#else
  struct queuePtr *ptr = ADD(theQueue, theQueue->next);
  struct queuePtr *next;

  while ( (next = ADD(ptr, ptr->next)) != theQueue ) {
    if ( ADD(ptr, QUEUE_HEAD_LEN + ptr->size) == next ) {
      struct queuePtr *nextNext = ADD(next, next->next);
      ptr->next = SUBTRACT(nextNext, ptr);
      nextNext->prev = SUBTRACT(ptr, nextNext);
      ptr->size += next->size + QUEUE_HEAD_LEN;
    } else {
      ptr = next;
    }
  }
#endif
} /* End of gatherOrderedQueue */


/* Gather all chunks of an unordered queue into as many super-chunks as
 * possible
 */
void gatherQueue( struct queuePtr *theQueue ) {
#ifndef HANDLE_SIZE
  return 1;
#else
  struct queuePtr *ptr = ADD(theQueue, theQueue->next);
  int again;

  do {
    do {
      struct queuePtr *ptr1 = ADD(theQueue, theQueue->next);
      again = FALSE;
      while ( ptr1 != theQueue ) {
	struct queuePtr *next = ADD(ptr1, ptr1->next);
	if ( ptr != ptr1 ) {
	  if ( ADD(ptr, QUEUE_HEAD_LEN + ptr->size) == ptr1 ) {
	    extractElement( theQueue, ptr1 );
	    ptr->size += ptr1->size + QUEUE_HEAD_LEN;
	    again = TRUE;
	  }
	}
	ptr1 = next;
      }
    } while ( again );
    ptr = ADD(ptr, ptr->next);
  } while ( ptr != theQueue );
#endif
} /* End of gatherQueue */
