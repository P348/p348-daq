/*
  void scaler_decode(int,int *, struct scalerStruct *) 
*/

#include <stdlib.h>

#include "scalerEvent.h"

void scaler_decode(int evsize,int *data_buf, struct scalerStruct *subevent) {


  int size, i, k, i0;
  subevent->decode_err = 0;
  size = evsize/4;
  if (size < 3) {
    subevent->decode_err = 1;
    return;
      } else {
  /* Decode the S-Link header  */
	subevent->err =       (data_buf[0] >>31 & (1<< 1)-1);           
	subevent->type =      (data_buf[0] >>26 & (1<< 5)-1);  
	subevent->source =    (data_buf[0] >>16 & (1<<10)-1); 
	subevent->size =      (data_buf[0] >> 0 & (1<<16)-1);  
	subevent->stat =      (data_buf[1] >>31 & (1<< 1)-1);  
	subevent->spill =     (data_buf[1] >>20 & (1<<11)-1);  
	subevent->event =     (data_buf[1] >> 0 & (1<<20)-1);
	subevent->format =    (data_buf[2] >>24 & (1<< 8)-1);  
	subevent->errwords =  (data_buf[2] >>16 & (1<< 8)-1);
	subevent->tcserr =    (data_buf[2] >> 8 & (1<< 8)-1);
	subevent->status =    (data_buf[2] >> 0 & (1<< 8)-1);

	/* checks */
	if ((subevent->format & 0x7F) !=30) 
	  { 
	    subevent->decode_err = -2; 
	    return;
	  }
	if (subevent->size !=size) { subevent->decode_err = 3; return;}

	/* decode the scaler data */
	subevent->sc_port = 0;
	for (i=0;i<MAX_SCALER_PORT;i++) subevent->sc_port_error[i0]=0;
	if (subevent->format & 0x80) {
	  subevent->decode_err = -3;
	  return;
	}
	i0=3;
	for (i=i0;i<size;i++) {
	  if ((data_buf[i] >>30 & (1<< 2)-1) == 0) {
	    /* scaler header */
	    subevent->sc_port++;
	    subevent->sc_geoid[subevent->sc_port-1]=(data_buf[i] >>20 & (1<<10)-1);
	    k = 0;
	  } else if ((data_buf[i] >>20 & (1<<12)-1) == 0x7FF) {
	    /* error word - ignore*/
	    i++;
	  } else if ((data_buf[i] >>30 & (1<< 2)-1) == 1) {
	    /* scaler trailer */
	  } else {
	    /* tdc_data */
	    if (k<MAX_SCALER_DATA) {
	      subevent->sc_data[(subevent->sc_port-1)*32+k] = 
		(data_buf[i] >>0 & 0x7FFFFFFF);
	    } else if (k<MAX_SCALER_DATA+2) {
	      if (k=MAX_SCALER_DATA) {
		subevent->sc_time[subevent->sc_port-1] =(data_buf[i] >>16 & (1<<15)-1);
		subevent->sc_flags[subevent->sc_port-1]=(data_buf[i] >> 0 & (1<<16)-1);
	      } else {
		subevent->sc_time[subevent->sc_port-1] +=
		  (data_buf[i] >>16 & (1<<15)-1)<<15;
		subevent->sc_flags[subevent->sc_port-1]+=
		  (data_buf[i] >> 0 & (1<<16)-1)<<16;
	      }
	    }
	    k++;
	  }
	  
	}
      }
}
