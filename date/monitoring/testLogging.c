/* Test the monitoring logging facilities */
#include <stdio.h>
#include <stdlib.h>

#define NO_LOGGING
#include "monitorInternals.h"

int main() {
  setLogLevel( 0 );
  setLogLevel( 10 );
  setLogLevel( 20 );
  setLogLevel( 30 );
  setLogLevel( 40 );
  setLogLevel( 0 );

  setLogLevel( 40 );
  initLogLevel();
  
  exit( 0 );
}
