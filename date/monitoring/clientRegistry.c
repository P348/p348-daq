/* Monitor V 3.xx clients registry module
 * ======================================
 *
 * Module history:
 *  1.00 23-Jul-98 RD   Created
 *  1.01 24-Aug-98 RD   First public release
 *  1.02 31-Aug-98 RD   Cleanup of obsolete events added to periodic check
 *  1.03 19-Nov-98 RD   Counter for events & bytes added
 *  1.04 16-Jul-99 RD   Bad increment in SignIn fixed
 */
#define VID "1.04"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>

#include "monitorInternals.h"
#include "event.h"

#define DESCRIPTION "DATE V3 monitor clients registry handler"
#ifdef AIX
static
#endif
char monitorClientRegistryIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                                    """ """ VID """ """ \
                                    """ compiled """ __DATE__ """ """ __TIME__;


int monitorOurId = -1;			/* Our client ID */
int monitorOurRegistration = -1;	/* Our registration number */

static int monitorExitHandlerRegistered = FALSE;

static time_t timeOfLastCheck = 0;


/* Our default exit handler */
void monitorExitHandler() {
  monitorSignOut();
} /* End of monitorExitHandler */


/* Sign-in. This procedure is called to register with the monitor database.
 *
 * WARNING: we assume that the monitor buffer had been locked.
 *
 * Return:
 *   0    => OK
 *   else => Error
 *
 * Initialise:
 *   monitorOurId	Out client identification
 */
int monitorSignIn() {
  int   i;
  int   n;
  pid_t ourPid;
  monitorClientDescriptor *us;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSignIn: starting" );
  
  /* Run a preliminary check on the coherency between the clients' database
   * content and the various counters
   */
  for ( i = 0, n = 0; i != (*mbMaxClients); i++ )
    if ( CLIENT( i )->inUse )
      n++;
  if ( n != (*mbNumClients) ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorSignIn Pre-check failed, *mbNumClients:%d found:%d",
		*mbNumClients,
		n );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_INTERNAL;
  }

  /* Get our PID */
  ourPid = getpid();
  if ( ourPid == (pid_t)(-1) || ourPid == (pid_t)0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorSignIn Cannot get our PID errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }

  /* Check if someone else was using the same PID and is still registered */
  for ( i = 0; i != (*mbMaxClients); i++ ) {
    if ( CLIENT( i )->inUse && CLIENT( i )->pid == ourPid ) {
      int status;
      
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorSignIn PID %d already in use and still registered",
		  (int)ourPid );
	INFO_TO( MON_LOG, msg );
      }
      (*mbForcedExits)++;
      if ( ( status = monitorForceSignOut( i ) ) != 0 ) {
	LOG_NORMAL
	  ERROR_TO( MON_LOG, "monitorSignIn: monitorForceSignOut failed" );
	return status;
      }
    }
  }

  /* Do some cleanup (if necessary...) */
  monitorValidateRegistrations();

  /* Check if there is free space in the clients' database */
  if ( *mbNumClients == *mbMaxClients ) {
    LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSignIn: too many clients" );
    (*mbTooManyClients)++;
    return MON_ERR_TOO_MANY_CLIENTS;
  }

  /* Find the first unused entry in the clients' database */
  i = -1;
  do {
    if ( ++i == (*mbMaxClients) ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorSignIn: no free space in monitor clients table?" );
      return MON_ERR_INTERNAL;
    }
  } while ( CLIENT( i )->inUse );

  /* Update counters, registration numbers and our client entry */
  (*mbNumSignIn)++;
  (*mbNumClients)++;
  monitorOurId = i;
  us = CLIENT( monitorOurId );
  monitorOurRegistration = ++(us->registrationNumber);
  us->lastEventNumber = *mbCurrentEventNumber;
  us->pid = ourPid;
  us->waitingForNewEvents = FALSE;
  us->mpId[0] = 0;
  us->inUse = TRUE;
  us->eventsRead = 0;
  us->bytesRead[0] = 0;
  us->bytesRead[1] = 0;

  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSignIn Signed in as client:%d reg:(%d %d) numClients:%d \
PID:(%d %d)",
	      monitorOurId,
	      monitorOurRegistration,
	      us->registrationNumber,
	      *mbNumClients,
	      (int)ourPid,
	      (int)us->pid);
    INFO_TO( MON_LOG, msg );
  }

  /* Register some sort of exit handler/signal handler */
  if ( !monitorExitHandlerRegistered ) {
    if ( atexit( monitorExitHandler ) == 0 ) {
      monitorExitHandlerRegistered = TRUE;
      LOG_DETAILED
	INFO_TO( MON_LOG, "monitorSignIn: exit handler registered OK" );
    } else {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorSignIn: cannot register exit handler" );
    }
  }
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSignIn: normal completion" );
  return 0;
} /* End of monitorSignIn */


/* Register an MP in the client's descriptor.
 *
 * Parameter:
 *   mpName   Name (descriptor) of the monitoring program
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorRecordMpInMemory( char *mpName ) {
  int status;
  int mustUnlock;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorRecordMpInMemory(%s)", mpName );
    INFO_TO( MON_LOG, msg );
  }
  
  if ( ( mustUnlock = ( status = monitorLockBuffer() ) == 0 ) ) {
    status = monitorCheckRegistration();
  } else {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorRecordMpInMemory: failed to lock buffer" );
  }

  if ( status == 0 ) {
    monitorClientDescriptor *us = CLIENT( monitorOurId );
    strncpy( us->mpId, mpName, MP_MAX_CHARS );
    us->mpId[ MP_MAX_CHARS-1 ] = 0;
  }

  if ( mustUnlock ) {
    int status1 = monitorUnlockBuffer();

    if ( status1 != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorRecordMpInMemory: failed to unlock buffer" );
      if ( status == 0 ) status = status1;
    }
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorRecordMpInMemory return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorRecordMpInMemory */

/* Sign ourselves out, release all resources that are eventually
 * locked for our use.
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorSignOut() {
  int status = 0;
  
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSignOut: starting" );

  if ( monitorOurId != (-1) ) {
    if ( ( status = monitorCheckRegistration() ) == 0 ) {
      if ( ( status = monitorForceSignOut( monitorOurId ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG, "monitorSignOut: monitorForceSignOut failed" );
      monitorOurId = -1;
      monitorValidateRegistrations();
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorSignOut Return:(%d x%08x)", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorSignOut */


/* Sign someone out, release all resources that are eventually
 * locked for his use.
 *
 * Return:
 *   0    => OK
 *   else => error (bid-coded value)
 */
int monitorForceSignOut( int index ) {
  int status;
  int mustUnlock;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorForceSignOut(%d)", index );
    INFO_TO( MON_LOG, msg );
  }

  if ( ( mustUnlock = ( status = monitorLockBuffer() ) == 0 ) ) {
    if ( CLIENT( index )->inUse ) {
      if ( ( status = monitorCleanReservations( index ) ) == 0 ) {
      CLIENT( index )->inUse = FALSE;
      (*mbNumSignOut)++;
      (*mbNumClients)--;
      } else {
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorForceSignOut: error in monitorCleanReservations" );
      }
    }
    monitorValidateRegistrations();
  } else {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorForceSignOut: failed to lock buffer" );
  }
  if ( mustUnlock ) {
    int status1 = monitorUnlockBuffer();
    if ( status1 != 0 )
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorForceSignOut: failed to unlock buffer" );
    status = status == 0 ? status1 : status;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorForceSignOut Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorForceSignOut */


/* Check our registration ticket. This should avoid problems in case
 * some other process has re-used our registration entry.
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorCheckRegistration() {
  int status;
  int mustUnlock;

  if ( ( mustUnlock = ( ( status = monitorLockBuffer() ) == 0 ) ) ) {
    /* Do we have a valid ID? */
    if ( monitorOurId < 0 || monitorOurId >= (*mbMaxClients) ) {
      status = MON_ERR_INTERNAL;
    } else {
      /* The client record must be in use and the registration number must be
       * the same we found when wesigned in */
      monitorClientDescriptor *d = CLIENT( monitorOurId );
    
      if (( !d->inUse ) ||
	  ( d->inUse && (d->registrationNumber != monitorOurRegistration ) )) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorCheckRegistration Client:%d inUse:%s \
originalregistrationNum:%d currentRegistrationNum:%d",
		    monitorOurId, d->inUse ? "yes" : "NO", 
		    monitorOurRegistration, d->registrationNumber );
	  ERROR_TO( MON_LOG, msg );
	}
	status = MON_ERR_SIGNED_OUT;
      }
    }
  } else {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorCheckRegistration: failed to lock buffer" );
  }
  if ( mustUnlock ) {
    int status1 = monitorUnlockBuffer();
    if ( status1 != 0 )
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorCheckRegistration: failed to unlock buffer" );
    status = status == 0 ? status1 : status;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCheckRegistration Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorCheckRegistration */


/* Check and validate all active registrations. This entry should be called
 * either periodically or on request (e.g. search for free space) to gather
 * all closed connections.
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorValidateRegistrations() {
  int status = 0;
  int mustUnlock;
  int forced = 0;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorValidateRegistrations: starting" );
  
  if ( ( timeOfLastCheck = time( NULL ) ) == (time_t)(-1) ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg), 
		"monitorValidateRegistrations Failed to get time \
errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }
  if ( ( mustUnlock = ( status = monitorLockBuffer() ) == 0 ) ) {
    int i;
    for ( i = 0; status == 0 && i != (*mbMaxClients); i++ ) {
      monitorClientDescriptor *c = CLIENT( i );
      if ( c->inUse ) {
	/* We try to kill the process, signal 0 => NOP. The result can be:
	 *   0     => fine, the process is there and we can kill it
	 *   EPERM => we don't have the permission, the process is there
	 *   else  => the process is dead
	 */
	int killStat = kill( c->pid, 0 );
	if ( !( killStat == 0 ||
		( killStat == (-1) && errno == EPERM ) ) ) {
	  /* Oops: the process is no more there */
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorValidateRegistrations client:%d PID:%d \
registrationNum:%d not found -> removing (status:%d errno:%d (%s))",
		      i, (int)c->pid, c->registrationNumber,
		      killStat, errno, strerror(errno) );
	    INFO_TO( MON_LOG, msg );
	  }
	  (*mbForcedExits)++;
	  forced++;
	  status = monitorForceSignOut( i );
	}
      }
    }
  } else {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorValidateRegistrations: failed to lock buffer" );
  }
  if ( status == 0 )
    status = monitorCheckObsoleteEvents();
  
  if ( mustUnlock ) {
    int status1 = monitorUnlockBuffer();
    if ( status1 != 0 )
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorValidateRegistrations: failed to unlock buffer" );
    status = status == 0 ? status1 : status;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorValidateRegistrations Status:(%d x%08x) signoff(s):%d",
	      status, status, forced );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorValidateRegistrations */


/* Entry to perform a periodic check on the registered clients. The frequency
 * of the check is function of the event type and the time of the last check */
int monitorPeriodicClientsCheck( eventTypeType evType ) {
  int status = 0;
  time_t now = time( NULL );

  if ( now == (time_t)(-1) ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorPeriodicClientsCheck Failed to get time errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_SYS_ERROR;
  }
  if ( status == 0 ) {
    if ( now - timeOfLastCheck >= TIME_BETWEEN_CHECKS ||
	 evType == END_OF_RUN ||
	 evType == START_OF_RUN ) {
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorPeriodicClientsCheck time:%ld \
timeOfLastCheck:%ld TIME_BETWEEN_CHECKS:%d eventType:%s",
		  (long)now,
		  (long)timeOfLastCheck,
		  TIME_BETWEEN_CHECKS,
		  monitorDumpEventType( evType ) );
	INFO_TO( MON_LOG, msg );
      }
      status = monitorValidateRegistrations();
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorPeriodicClientsCheck Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorPeriodicClientsCheck */
