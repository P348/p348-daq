/* Monitor reservation handler definitions
 * =======================================
 */
#ifndef __reservation_handler_h__
#define __reservation_handler_h__

/* All the entries below return 0 for OK, else for error */

int monitorCleanReservations( int );
int monitorCleanReservation( int, int );
int monitorBuildReservation( monitorEventDescriptor*, const void* const );
int monitorFreeReservations();
int monitorCheckReservations( int );
int monitorInsertReservation( monitorEventDescriptor* );
int monitorFreeOne( int );
int monitorFindNextEvent( int, int, void*, int );
int monitorCheckObsoleteEvents();
int monitorCheckEventsAge( time_t now );

/* This entry returns 0 if space has been freed, else if no space has
 * been found
 */
int monitorMakeSpace( const struct eventHeaderStruct * const );
#endif
