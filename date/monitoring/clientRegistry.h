/* DATE monitor V3.xx client regsitry definitions
 * ==============================================
 *
 * This module contains the declarations for the entries and the statics
 * used for monitor clients registration & handling (sign-in, sign-out,
 * checking etc).
 */

#ifndef __client_registry_h__
#define __client_registry_h__

/* Our client ID, used to select the proper client descriptor record */
extern int monitorOurId;

/* All entries return 0 for OK, else for error */

/* Entry to "sign-in" with the monitoring scheme: get a client ID, initialise
 * the client descriptor tables, register the exit handler                   */
extern int monitorSignIn();

/* Entry to declare the MP descriptor string */
extern int monitorRecordMpInMemory( char * );

/* Entry to "sign-out" with the monitoring scheme: release the allocated
 * resources, the locked events etc...                                       */
extern int monitorSignOut();

/* Entry to force the "sign-out" of an arbitrary client                      */
extern int monitorForceSignOut( int );

/* Entry to check the validity of our client registration record. On error,
 * it means that some other client "took over" and we must re-register to
 * get access to the monitor buffer                                          */
extern int monitorCheckRegistration();

/* Entry to call at any time to check all the registered clients             */
extern int monitorValidateRegistrations();

/* Entry to perform a periodic check on all the registered clients           */
extern int monitorPeriodicClientsCheck( eventTypeType );
#endif
