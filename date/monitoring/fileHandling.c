/* Monitor V 3.xx file handling module
 * ===================================
 *
 * This is the module responsible for parsing and checking of configuration
 * and key files used to control the monitoring scheme.
 *
 * Module history:
 *  1.00    3-Jul-98 RD   Created
 *  1.01   24-Aug-98 RD   First public release
 *  1.02    7-Oct-98 RD   Monitoring hosts & must monitoring hosts lists added
 *			  Silent parsing option added
 */
#define VID "1.02"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "monitorInternals.h"
#include "infoLogger.h"

#define DESCRIPTION "DATE V3 file handling"
#ifdef AIX
static
#endif
char fileHandlingIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                           """ """ VID """ """ \
                           """ compiled """ __DATE__ """ """ __TIME__;

int monitorSilentParsing = FALSE;

extern int maxClients;		/* Maximum number of clients */
extern int maxEvents;		/* Maximum number of events  */
extern int eventSize;		/* Average event size        */
extern int eventBufferSize;	/* Maximum event buffer size */
extern int monitorMaxAge;	/* Maximum events' age       */

extern int monitoringLogLevelFromEnvironment; /* TRUE if logging level
					       * declared via environment
					       * variable (priority over
					       * file declaration) */

/* The lists of monitoring hosts and must monitoring hosts */
extern monitoringHostDescriptor *monitoringHostsList;
extern monitoringHostDescriptor *mustMonitoringHostsList;

/* Return a version descriptive string */
char *monitorFileHandlingGetVid() {
  return VID;
} /* End of fileHandlingGetVid */


/* Test if a given file exists and is readable by us */
int monitorTestFileReadable( char *fileName ) {
  FILE *in;
  int   status;

  if ( ( in = fopen( fileName, "r" ) ) != NULL )
    fclose( in );
  status = ( in != NULL );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorTestFileReadable(%s) Return:%s errno:%d (%s)",
	      fileName, status ? "TRUE" : "FALSE",
	      status ? 0 : errno, strerror(errno) );
    if ( status ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }    
  return status;
} /* End of monitorTestFileReadable */


/* Expand a list of hosts.
 *
 * Parameters:
 *   list   The source list (from the user)
 *   result The output of the parsing
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorExpandList( char *list, monitoringHostDescriptor **result ) {
  /* Free any list eventually preloaded in this list */
  while ( *result != NULL ) {
    void *curr = *result;
    *result = (*result)->next;
    free( curr );
  }

  /* Load this list with the new values */
  if ( *list != 0 ) {
    char *start = list;
    list--;
    do {
      list++;
      if ( *list == 0 || *list == ',' ) {
	monitoringHostDescriptor *newDescriptor;
	
	if ( list == start ) return 2;
	if ( ( newDescriptor =
	       (monitoringHostDescriptor *)
	         malloc( sizeof( monitoringHostDescriptor ) ) ) == NULL )
	  return 3;
	if ( ( newDescriptor->hostname =
	       (char *)malloc(list-start+1) ) == NULL ) {
	  free( newDescriptor );
	  return 4;
	}
	strncpy( newDescriptor->hostname, start, list-start );
	newDescriptor->hostname[list-start] = 0;
	newDescriptor->next = *result;
	*result = newDescriptor;
	start = list+1;
      }
    } while ( *list != 0 );
  }
  return 0;
} /* End of monitorExpandList */


/* Parse a configuration file, return 0 if OK, else for error */
int monitorParseConfigFile( char *fileName ) {
  FILE *in;
  char  msg[ 1000 ];
  int   nLine;

  LOG_DETAILED {
    snprintf( SP(msg), "monitorParseConfigFile(%s)", fileName );
    INFO_TO( MON_LOG, msg );
  }

  if ( !monitorTestFileReadable( fileName ) ) {
    LOG_DETAILED
      ERROR_TO( MON_LOG, "monitorParseConfigFile file unreadable" );
    return MON_ERR_SYS_ERROR;
  }
  
  if ((in = fopen( fileName, "r")) == NULL ) {
    int e = errno;
    snprintf( SP(msg),
	      "Cannot open configuration file \"%s\" \
System-dependent status ",
	      fileName );
    if ( !monitorSilentParsing ) perror( msg );
    LOG_NORMAL {
      snprintf( SP(msg),
		"monitorParseConfigFile Fopen failed \
file:\"%s\" errno:%d (%s)",
		fileName, e, strerror(e) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }

  for ( nLine = 1; ; nLine++ ) {
    int   i;
#define MAX_ARGS 15		/* Maximum number of arguments */
    int   argc;
    char *argv[ MAX_ARGS ];
    int   syntaxError;
#define MAX_LINE 1000		/* Maximum number of chars (plus NULL) */
    char  line[ MAX_LINE + 1 ];
    char  inputLine[ sizeof( line ) ];

    /* Get the next input line (if any), terminate it and create the
     * working copy for us to play. While we scan the line to remove
     * the newline, we will change TABs into SPACEs: this will simplify
     * the scanning later on.
     */
    if ( fgets( inputLine, MAX_LINE, in ) == NULL ) break;
    inputLine[ MAX_LINE ] = 0;
    for ( i = 0; inputLine[ i ] != 0 && inputLine[ i ] != '\n'; i++ )
      if ( inputLine[i] == '\t' ) inputLine[i] = ' ';
    inputLine[ i ] = 0;
    strcpy( line, inputLine );

    LOG_DEVELOPER {
      snprintf( SP(msg), "%5d), \"%s\"", nLine, line );
      INFO_TO( MON_LOG, msg );
    }

    /* Get rid of any comment */
    for ( i=0; line[i] != 0 && line[i] != '#'; i++ );
    line[i]=0;

    /* Get all the arguments  out of the line */
    for ( argc=0, i=0; line[i]!=0; ) {
      for ( ; line[i] != 0 && ( line[i] == ' ' ); i++ );
      if ( line[i] != 0 ) {
	if ( argc == MAX_ARGS ) {
	  if ( !monitorSilentParsing )
	    fprintf( stderr, 
		     "Configuration file \"%s\": too many arguments\
 in line %d: \"%s\"\n",
		   fileName, nLine, inputLine );
	  fclose( in );
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorParseConfigFile: too many arguments" );
	  return MON_ERR_LINE_TOO_COMPLEX;
	}
	argv[ argc++ ] = &line[i];
	for ( ; line[i] != 0 && line[i] != ' '; i++ );
	if ( line[ i ] != 0 ) line[ i++ ] = 0;
      }
    }
    if ( argc != 0 ) {

      /* Try to understand what the parameters are */
      syntaxError = FALSE;
      if ( strcasecmp( "LOGLEVEL", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  int newLogLevel;

	  if ( sscanf( argv[1], "%d", &newLogLevel ) != 1 ) {
	    syntaxError = TRUE;
	  } else {
	    /* Here we have the two log levels: the one from the
	       configuration file and (eventually) the one from the
	       environment variable. We will give priority to the
	       environment variable...
	     */
	    if ( monitoringLogLevelFromEnvironment ) {
	      LOG_DETAILED {
		snprintf( SP(msg),
			  "monitorParseConfigFile Log level set from \
environment variable: cannot override it. \
Current value:%d requested value:%d",
			  monitoringLogLevel, newLogLevel );
		INFO_TO( MON_LOG, msg );
	      }
	    } else {
	      setLogLevel( newLogLevel );
	    }
	  }
	}
      } else if ( strcasecmp( "MAX_CLIENTS", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  int newMax;

	  if ( sscanf( argv[1], "%d", &newMax ) != 1 ) {
	    syntaxError = TRUE;
	  } else {
	    LOG_DETAILED {
	      snprintf( SP(msg),
			"monitorParseConfigFile \
Setting max number of clients to %d",
			newMax );
	      INFO_TO( MON_LOG, msg );
	    }
	    maxClients = newMax;
	  }
	}
      } else if ( strcasecmp( "MAX_EVENTS", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  int newMax;

	  if ( sscanf( argv[1], "%d", &newMax ) != 1 ) {
	    syntaxError = TRUE;
	  } else {
	    LOG_DETAILED {
	      snprintf( SP(msg),
			"monitorParseConfigFile \
Setting max number of events to %d",
			newMax );
	      INFO_TO( MON_LOG, msg );
	    }
	    maxEvents = newMax;
	  }
	}
      } else if ( strcasecmp( "EVENT_SIZE", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  int newVal;

	  if ( sscanf( argv[1], "%d", &newVal ) != 1 ) {
	    syntaxError = TRUE;
	  } else {
	    LOG_DETAILED {
	      snprintf( SP(msg),
			"monitorParseConfigFile \
Setting average event size to %d",
			newVal );
	      INFO_TO( MON_LOG, msg );
	    }
	    eventSize = newVal;
	  }
	}
      } else if ( strcasecmp( "EVENT_BUFFER_SIZE", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  int newVal;

	  if ( sscanf( argv[1], "%d", &newVal ) != 1 ) {
	    syntaxError = TRUE;
	  } else {
	    LOG_DETAILED {
	      snprintf( SP(msg),
			"monitorParseConfigFile \
Setting event buffer size to %d",
			newVal );
	      INFO_TO( MON_LOG, msg );
	    }
	    eventBufferSize = newVal;
	  }
	}
      } else if ( strcasecmp( "EVENTS_MAX_AGE", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  int newVal;

	  if ( sscanf( argv[1], "%d", &newVal ) != 1 ) {
	    syntaxError = TRUE;
	  } else {
	    LOG_DETAILED {
	      snprintf( SP(msg),
			"monitorParseConfigFile Setting events max age to %d",
			newVal );
	      INFO_TO( MON_LOG, msg );
	    }
	    monitorMaxAge = newVal;
	  }
	}
      } else if ( strcasecmp( "MONITORING_HOSTS", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  if ( monitorExpandList( argv[1], &monitoringHostsList ) != 0 )
	    syntaxError = TRUE;
	}
      } else if ( strcasecmp( "MUST_MONITORING_HOSTS", argv[0] ) == 0 ) {
	if ( argc != 2 ) syntaxError = TRUE;
	else {
	  if ( monitorExpandList( argv[1], &mustMonitoringHostsList ) != 0 )
	    syntaxError = TRUE;
	}
      } else {
	if ( !monitorSilentParsing )
	  fprintf( stderr,
		   "Configuration file \"%s\":\
 parse error on line %d: \"%s\"\n",
		   fileName, nLine, inputLine );
	fclose( in );
	LOG_NORMAL {
	  snprintf( SP(msg),
		    "monitorParseConfigFile Unknown keyword \"%s\"",
		    argv[0] );
	  ERROR_TO( MON_LOG, msg );
	}
	return MON_ERR_CONFIG_ERROR;
      }
      if ( syntaxError ) {
	if ( !monitorSilentParsing )
	  fprintf( stderr,
		   "monitorParseConfigFile: configuration file \"%s\": \
syntax error on line %d: \"%s\"\n",
		   fileName, nLine, inputLine );
	fclose( in );
	LOG_NORMAL {
	  snprintf( SP(msg),
		    "monitorParseConfigFile \
Syntax error in configuration file:\"%s\" lineNumber:%d line:\"%s\"",
		    fileName, nLine, inputLine );
	  ERROR_TO( MON_LOG, msg );
	}
	return MON_ERR_CONFIG_ERROR;
      }
    }
  }
  LOG_DETAILED
    INFO_TO( MON_LOG, "monitorParseConfigFile: normal completion" );
  fclose( in );
  return 0;
} /* End of parseConfigFile */
