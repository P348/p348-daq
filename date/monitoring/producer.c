#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <sys/uio.h>

#include "monitorInternals.h"
#include "testLib.h"

char  **config;
char  **events;
pid_t   pid;
int     status;

extern int monitorInjectEvent( void * );
extern int monitorInjectEventVector( const struct iovec * );

struct iovec iov[ 10 ];

void buildIov( void *data ) {
  int left = *(long32 *)data;
  int i = 0;

  while ( left != 0 ) {
    iov[i].iov_base = data;
    iov[i].iov_len  = i == 9 ? left : 1000;
    data = ADD( data, iov[i].iov_len );
    left -= iov[i].iov_len;
    i++;
  }
}

int createKeyFile() {
  char cmd[ 1000 ];
  char dataFile[ 1000 ];

  if ( dataSource[0] == 0 || dataSource[0] == ':' ) return 0;
  strcpy( dataFile, dataSource );
  if ( dataFile[ strlen( dataFile ) - 1 ] == ':' )
    dataFile[ strlen( dataFile ) - 1 ] = 0;

  snprintf( cmd, sizeof(cmd),
	    "/usr/bin/touch %s; chmod 0777 %s",
	    dataFile, dataFile );
  if ( system( cmd ) != 0 ) {
    fprintf( stderr,
	     "Problem(s) creating stamp file \"%s\", command: \"%s\".\n",
	     dataFile, cmd );
    return 1;
  }
  if ( debug ) {
    printf( "Configuration/key file \"%s\" created/updated ok\n", dataFile );
    fflush( stdout );
  }
  return 0;
}

int configError( char **argv ) {
  fprintf( stderr,
	   "%s: configuration file \"%s\" invalid format, test aborted\n",
	   argv[0], argv[1] );
  return 2;
}

void dumpVars() {
  if ( debug ) {
    int i;
    
    printf( "Test runs for %d loop%s, ",
	    numLoops, (numLoops != 1 ? "s" : "") );
    printf( "Interval timers: %d, %d\n", timerMin, timerMax );
    printf( "Random key: %d\n", seed );
    printf( "Data source: \"%s\"\n", dataSource );
    printf( "Configuration:\n" );
    for ( i=0; config[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, config[i] );
    printf( "\t----------\n" ); 
    printf( "Events:\n" );
    for ( i=0; events[i] != 0; i++ )
      printf( "\t%2d) \"%s\"\n", i, events[i] );
    printf( "\t----------\n" );
  }
}

int main( int argc, char **argv ) {
  int loop;

  pid = getpid();

  printf( "%08lx   *** Monitor producer validation starting ***\n",
	  (long int)pid );

  if ( argc < 2 ) return usage( argv );

  /* First parse the configuration file */
  if ( (config = monParseConfig( "Producer", argv[1] ) ) == NULL )
    return configError( argv );
  if ( (config = parse( config )) == NULL) return configError( argv );
  if ( (events = monParseConfig( "Events", argv[1] ) ) == NULL )
    return configError( argv );

  /* Parse the flags who might override some of the parameters set by
   * the configuration file */
  if ( monParseFlags( argc, argv, FALSE ) != 0 ) return usage( argv );

  dumpVars();
  initVars();
  
  if ( createKeyFile() != 0 ) return 3;

  if ( ( status = monitorSetDataSource( dataSource ) ) != 0 ) {
    printf( "%08lx: monitorSetDataSource failed, status: %d\n",
	    (long int)pid, status );
    return 1;
  }
  if ( debug )
    printf( "%08lx: monitorSetDataSource called OK\n", (long int)pid );
  
  for ( loop = numLoops; loop != 0; loop-- ) {
    void *rawData;
    int   status;
    
    doWait();
    rawData = createNextEvent( events );
    if ( debug ) {
      printf( "New event:\n" );
      if ( ( status = dumpEvent( rawData ) ) != 0 ) {
	printf( "ERROR: dumpEvent failed, status = 0x%08x\n", status );
	return 1;
      }
      printf( "\n" );
    }

    if ( useIOV ) {
      buildIov( rawData );
      status = monitorInjectEventVector( iov );
    } else {
      status = monitorInjectEvent( rawData );
    }
    if ( status != 0 ) {
      printf( "%08lx: monitorInjectEvent failed errno:%d (%s) status:%d %s\n",
	      (long int)pid,
	      errno, strerror(errno),
	      status, monitorDecodeError( status ) );
    } else if ( debug ) {
      printf( "%08lx: monitorInjectEvent called OK\n", (long int)pid );
    }
  }

  printf( "%08lx   *** Monitor producer validation ending ***\n",
	  (long int)pid );

  return 0;
}
