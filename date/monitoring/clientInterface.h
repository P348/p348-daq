/* Monitor client interface definitions
 * ====================================
 */
#ifndef __client_interface__h__
#define __client_interface__h__


/* TRUE if the channel to the monitor buffer has been opened */
extern int monitorChannelIsOpen;

/* TRUE if local monitor buffer */
extern int monitorBufferIsLocal;

/* TRUE if networked monitor buffer */
extern int monitorBufferIsNetwork;

/* The event type for a given monitor policy record (-1 => all events) */
extern int thisEventType;

/* The size of the event to be injected */
extern int thisEventSize;
extern int thisEventPages;

/* The monitor type/attributes for a given monitor policy record */
extern monitorType thisEventMonitorType;
extern int thisEventMonitorAnd;
extern eventTypeAttributeType thisEventMonitorAttributes;

extern char *monitorDumpEventType( unsigned long32 );
extern int   monitorParsePolicyEntry( char *, char *, char * );
extern int   monitorFromMemory();
#endif

