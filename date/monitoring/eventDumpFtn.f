C =============================================================
      character ourname*100
      character datasource*100
      integer   iargc
      pointer   ( ptr, buffer )
      integer   buffer ( 1 )

      if ( iargc() .ne. 1 ) then
         call getarg( 0, ourname )
         write (*, *) 'Usage: ',
     1                ourname(:lastc( ourname )),
     1                ' bufferName'
         stop
      end if
      call getarg( 1, datasource )
      call check_status( 
     1        monitor_set_data_source(datasource(:lastc(datasource))),
     1        'set_data_source' )
      call check_status( 
     1        monitor_declare_mp( 'Simple FORTRAN test mp' ),
     1        'declare_mp' )
      call check_status(
     1        monitor_declare_table( 'SOR yes EOR yes PHY yes sob no' ),
     1        'declare_table' )

 10   call check_status ( 
     1        monitor_get_event_dynamic( ptr ),
     1        'get_event_dynamic' )
      call dump_event( buffer )
      call monitor_free_event( ptr )
      goto 10
      end
C =============================================================
      subroutine check_status( status, where )
      integer   status
      character where*(*)
      character v*100
      character ourname*100
      external monitor_decode_error
      
      if ( status .ne. 0 ) then
         call getarg( 0, ourname )
         call monitor_decode_error( status, v )
         write (*, *) ourname(:lastc(ourname)),
     1                ': error during ', where, ' = ',
     1                v(:lastc(v))
         stop
      endif
      end
C =============================================================
      integer function lastc( string )
      character string(*)
      integer i

      lastc = 1
      i = 1
 10   if ( i .eq. 100 ) return
      if ( string( i ) .ne. ' ' ) lastc = i
      i = i + 1
      goto 10
      end
C =============================================================
      subroutine dump_event( buffer )
      integer buffer(*)

      write (*, *) 'Event: size: ', buffer(1)
      return
      end
C =============================================================
