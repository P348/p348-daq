#define NO_LOGGING
#include <stdio.h>
#include "monitorInternals.h"

int main( int argc, char **argv ) {
  int status;
  
  if ( argc == 1 )
    argv[1] = "testParams.txt";

  initLogLevel();
  
  status = monitorParseConfigFile( argv[1] );
  printf( "Parsing of \"%s\" returns %d\n",
	  argv[1],
	  status );
  
  return 0;
}
