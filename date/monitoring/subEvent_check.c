/*
  void subEvent_check( int * ,struct subeventStruct *, int *) 
*/

#include <stdlib.h>
#include <stdio.h>

#include "subEvent.h"

void subEvent_check( int nsub, struct subeventStruct *subevent, int *error) {
  int i, k, tmin,tmax,ev,n0,n;

  *error=0;
  for (i=0;i<nsub;i++) {
    if (subevent[i].decode_err > 0) *error=subevent[i].decode_err;
  }
  if (*error) {
    printf("ERR Decode error: ");
    for (i=0;i<nsub;i++) printf(" %i",subevent[i].decode_err);
    printf("\n");
    return;
  }
  
  n0=-1;
  for (i=0;i<nsub;i++) {
    if (subevent[i].decode_err==0) {
      if (n0<0) n0= subevent[i].spill;
      if (n0 != subevent[i].spill) {
	*error=2;
	printf("ERR Spill numbers: %i %i\n",n0,subevent[i].spill);
	return;
      }
    }
  }


  n0=-1;
  for (i=0;i<nsub;i++) {
    if (subevent[i].decode_err==0) {
      if (n0<0) n0= subevent[i].event;
      if (n0 != subevent[i].event) {
	*error=3;
	printf("ERR Event numbers: %i %i\n",n0,subevent[i].event);
	return;
      }
    }
  }

  /*  for (i=0;i<nsub;i++) {
    if (subevent[i].decode_err==0) {
      if (subevent[i].tdc_nr_header!=8) { 
	*error=4;
	printf("ERR Nr of TDC headers: %i\n",subevent[i].tdc_nr_header);
	return;
      }
    }
  }
  */

  for (i=0;i<nsub;i++) {
    if (subevent[i].decode_err==0) {
      if (subevent[i].tdc_tbo == 1) {
	*error=7;
	printf("ERR event: tbo \n");
	return;
      }
    }
  }

  for (i=0;i<nsub;i++) {
    tmin=512; tmax=-1;
    ev = -1;
    if (subevent[i].decode_err==0) {
      for (k=0;k<subevent[i].tdc_nr_header;k++) {
	/* ignore timing from empty Hotlink inputs */
	if (subevent[i].tdc_header_time[k]!=0) {
	  tmin = min(tmin,subevent[i].tdc_header_time[k]);
	  tmax = max(tmin,subevent[i].tdc_header_time[k]);
	}
	if (ev<0) ev = subevent[i].tdc_header_event[k];
	if (ev != subevent[i].tdc_header_event[k]) *error=10;
      }
    }
  
    if (*error==10) {
      printf("ERR event: tdc event nr missmatch\n");
      return;
    } else {
      if (tmax-tmin > 3) {
	printf("ERR event: TDC not in sync tmin/tmax %i %i\n",tmin,tmax);
	*error=11;
	return;
      }
    }
  }
  for (i=0;i<nsub;i++) {
    if (subevent[i].decode_err==0) {
      if (subevent[i].tdc_header_event[0] != subevent[i].event % 64) {
	*error=6;
	printf("ERR event: tdc/header %i %i\n",subevent[i].tdc_header_event[0], subevent[i].event % 64);
	return;
      }
    }
  }
  
}

