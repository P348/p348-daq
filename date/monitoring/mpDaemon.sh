#! /bin/ksh
#
# Shell wrapper for the monitoring daemon.
#
# WARNING: the use of ksh builtin read may lead to CPU-bound endless loop for
# the "read DATE_SITE" statement (ksh + Solaris 2.6). We tried to avoid this
# by reading with "sh" (where available).

cd /tmp
LD_LIBRARY_PATH=/usr/lib:/usr/local/lib; export LD_LIBRARY_PATH
ulimit -c >/dev/null

if [ -x /bin/sh ]; then
  DATE_SITE=`/bin/sh -c 'read D; echo $D'`
else
  read DATE_SITE
fi
[ -d "$DATE_SITE" ] || exit 1
export DATE_SITE
. /date/setup.sh

${DATE_MONITOR_BIN}/mpDaemon
