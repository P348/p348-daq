/* Monitor V 3.xx reservation handler module
 * =========================================
 *
 * This is the module responsible for the handling of events reservation and
 * de-reservation, due to injection, monitoring and sign-out events.
 *
 * Module history:
 *  1.00 24-Jul-98 RD   Created
 *  1.01 24-Aug-98 RD   First public release
 *  1.02 31-Aug-98 RD   Cleanup of obsolete events implemented
 *  1.03  8-Sep-98 RD   Bug with CheckReservations fixed
 *  1.04 28-Oct-98 RD   monitorFreeReservations added
 *  1.05 19-Nov-98 RD   Counters for events & bytes added
 *  1.06  2-Jun-99 RD   Make space feature added
 *  1.07  7-Jul-99 RD   Monitor attributes handling added
 *  1.08  1-Dec-00 RD   Mods for DATE 4.x
 *  1.09  5-Jul-01 RD   Mods for "&" and "|" of attributes
 */
#define VID "1.09"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "monitorInternals.h"

#define DESCRIPTION "DATE V3 monitor reservation handler"
#ifdef AIX
static
#endif
char monitorReservationHandIdent[]= "@(#)""" __FILE__ """: """ DESCRIPTION \
                                    """ """ VID """ """ \
                                    """ compiled """ __DATE__ """ """ __TIME__;


/* Macro to increment an event index counter (wrap around) */
#define NEXT_EVENT( index ) ((index)+1 == (*mbMaxEvents) ? 0 : (index)+1)

/* Macro to decrement an event index counter (wrap around) */
#define PREV_EVENT( index ) ((index) == 0 ? (*mbMaxEvents)-1 : (index)-1)

/* Macros to check a given event against a given client:
 *    SHOULD_MONITOR returns TRUE if the client should monitor the event
 *    MUST_MONITOR   returns TRUE of the client must monitor the event
 */
#define SHOULD_MONITOR( eI, cI ) monitorCheckMonitor( eI, cI, yes )
#define MUST_MONITOR( eI, cI ) monitorCheckMonitor( eI, cI, all )


/* Number of the last inserted event, used for post-insertion checking */
int monitorLastInsertedEvent;


/* Maximum allowed age for events stored in the monitor buffer */
extern int monitorMaxAge;


/* Check if a given event is of interest to a given client for a given policy.
 *
 * Parameters:
 *	eI	Event index
 *	cI	Client index
 *	w	Monitor policy to use for the check
 *
 * Returns:
 *	TRUE	The given event is of interest for the given client and for
 *		the given policy
 *	FALSE	otherwise
 */
int monitorCheckMonitor( int eI, int cI, monitorType w ) {
  monitorClientDescriptor *cl = CLIENT( cI );
  monitorEventDescriptor  *ev = EVENT( eI );
  int ix = ev->eventType - EVENT_TYPE_MIN;
  
  return cl->monitorPolicy[ ix ] == w &&
         monitorSelectedByAttributes( ADD(ADD( mbFreeEvents,
					       ev->eventData),QUEUE_HEAD_LEN ),
				      &(cl->monitorAttributes[ ix ]),
				      cl->monitorAttributesAnd[ ix ]);
} /* End of monitorCheckMonitor */


/* Shift events one position left from the given point.
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorShiftLeft( int startFromHere ) {
  int current, next;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorShiftLeft(%d) *mbOldestEvent:%d *mbFirstFreeEvent: %d",
	      startFromHere, *mbOldestEvent, *mbFirstFreeEvent );
    INFO_TO( MON_LOG, msg );
  }
  current = startFromHere;
  if ( ( next = NEXT_EVENT( current ) ) == *mbFirstFreeEvent ) {
    if ( *mbOldestEvent == startFromHere ) {
      *mbOldestEvent = -1;
      *mbFirstFreeEvent = -1;
      
    } else {
      *mbFirstFreeEvent = current;
    }
    
  } else {
    do {
      memcpy( EVENT( current ),
	      EVENT( next ),
	      sizeof( monitorEventDescriptor ));
      current = next;
      next = NEXT_EVENT( current );
    } while ( next != (*mbFirstFreeEvent) );
    *mbFirstFreeEvent = current;
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorShiftLeft OK *mbOldestEvent:%d *mbFirstFreeEvent:%d",
	      *mbOldestEvent, *mbFirstFreeEvent );
    INFO_TO( MON_LOG, msg );
  }
  
  return 0;
} /* End of monitorShiftLeft */


/* Check the reservation status of a given event, eventually return it to
 * the free events list
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorCheckReservations( int eI ) {
  int status = 0;
  int status1;
  monitorEventDescriptor *event = EVENT( eI );
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCheckReservations(%d) numYes:%d numMust:%d",
	      eI, event->numYes, event->numMust );
    if ( event->numYes < 0 || event->numMust < 0 ) ERROR_TO( MON_LOG, msg );
    else INFO_TO( MON_LOG, msg );
  }

  /* This is a fix for an error condition that should not happen */
  if ( event->numMust < 0 ) event->numMust = 0;
  if ( event->numYes  < 0 ) event->numYes  = 0;

  /* If nobody wants this event, we can drop it */
  if ( event->numMust == 0 && event->numYes == 0 ) {
    
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorCheckReservations Removing event:%d number:%d",
		eI, event->eventNumber );
      INFO_TO( MON_LOG, msg );
    }
    
    if ( ( status = monitorDropEvent( event->eventData ) ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorCheckReservations: monitorDropEvent failed" );
    } else if ( ( status = monitorShiftLeft( eI ) ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorCheckReservations: \
monitorShiftLeft failed" );
    }
    
    if ( ( status1 = monitorSignalFreeSpace() ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorCheckReservations: monitorSignalFreeSpace failed" );
      status = status == 0 ? status1 : status;
    }
    
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorCheckReservations Removed event index:%d \
number:%d status:(%d x%08x)",
		eI, event->eventNumber, status, status );
      if ( status == 0 ) INFO_TO( MON_LOG, msg );
      else ERROR_TO( MON_LOG, msg );
    }
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCheckReservations Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorCheckReservations */


/* Find the first unmonitored event from the given client
 *
 * Return:
 *   >= 0	The event id of the first unmonitored event
 *   < 0	No unmonitored events available
 */
int monitorFindFirstUnmonitored( int clientId ) {
  monitorClientDescriptor *theClient = CLIENT( clientId );
  int status;
  int result = -1;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorFindFirstUnmonitored(%d) last monitored:%d",
	      clientId, theClient->lastEventNumber );
    INFO_TO( MON_LOG, msg );
  }

  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorFindFirstUnmonitored: lock failed" );
  } else {
    int ev = *mbOldestEvent;
    if ( ev >= 0  &&
	 *mbCurrentEventNumber != theClient->lastEventNumber ) {
      do {
	if ( EVENT( ev )->eventNumber > theClient->lastEventNumber ) {
	  result = ev;
	} else {
	  ev = NEXT_EVENT( ev );
	}
      } while ( result == -1 && ev != *mbFirstFreeEvent );
    }
    if ( ( status = monitorUnlockBuffer() ) != 0 )
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorFindFirstUnmonitored: unlock failed" );
  }
  status = status == 0 ? result : -1;
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorFindFirstUnmonitored Return:(%d x%08x)",
	      status, status );
    INFO_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorFindFirstUnmonitored */


/* Reset all the reservations eventually taken for a given client.
 *
 * Parameter:
 *   id: the id of the client to cleanup
 *
 * Return:
 *   0    => OK
 *   else => error (bit coded)
 */
int monitorCleanReservations( int id ) {
  int status;
  int status1;
  monitorClientDescriptor *client = CLIENT( id );

  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorCleanReservations(%d)", id );
    INFO_TO( MON_LOG, msg );
  }

  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorCleanReservations: lock failed" );
  } else {
    int ev;

    while ( (ev = monitorFindFirstUnmonitored( id )) >= 0 ) {
      monitorEventDescriptor *event = EVENT( ev );

      client->lastEventNumber = event->eventNumber;
      if ( SHOULD_MONITOR( ev, id ) ) {
	(event->numYes)--;
	if ( ( status = monitorCheckReservations( ev ) ) != 0 )
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorCleanReservations: \
monitorCheckReservations (1) failed" );
	
      } else if ( MUST_MONITOR( ev, id ) ) {
	int doSignal = --(event->numMust) == 0;
	if ( ( status1 = monitorCheckReservations( ev ) ) != 0 )
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorCleanReservations: \
monitorCheckReservations (2) failed" );
	if ( doSignal ) {
	  if ( (status = monitorSignalFreeSpace() ) != 0 ) {
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorCleanReservations: \
monitorSignalFreeSpace failed" );
	    status = status == 0 ? status1 : status;
	  }
	}
      }
    }
    if ( ( status1 = monitorUnlockBuffer() ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorCleanReservations: monitorUnlockBuffer failed" );
      status = status == 0 ? status1 : status;
    }
  }
  
  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCleanReservations Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorCleanReservations */


/* Clean a reservation previously imposed on an event, eventually dispose
 * the event if no more clients need it.
 *
 * Parameter:
 *   clientId	The ID of the client who wants to clean his reservation
 *   eventId    The ID of the event to clean the reservation of
 *
 * Return:
 *   0    => OK
 *   else => error (bit coded)
 */
int monitorCleanReservation( int clientId, int eventId ) {
  int status;
  int status1;

  LOG_DEVELOPER {
    char msg[1024 ];
    snprintf( SP(msg), "monitorCleanReservation(%d,%d)", clientId, eventId );
    INFO_TO( MON_LOG, msg );
  }
  
  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorCleanReservation: monitorLockBuffer failed" );
  } else {
    monitorEventDescriptor *event = EVENT( eventId );
    if ( SHOULD_MONITOR( eventId, clientId ) ) {
      (event->numYes)--;
      if ( ( status = monitorCheckReservations( eventId ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorCleanReservation: \
monitorCheckReservations (1) failed" );
    } else if ( MUST_MONITOR( eventId, clientId ) ) {
      int doSignal = --(event->numMust) == 0;
      if ( ( status = monitorCheckReservations( eventId ) ) != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorCleanReservation: \
monitorCheckReservations (2) failed" );
      if ( doSignal ) {
	if ( ( status1 = monitorSignalFreeSpace() ) != 0 ) {
	  LOG_NORMAL ERROR_TO( MON_LOG, "monitorCleanReservation: \
monitorSignalFreeSpace failed" );
	  status = status == 0 ? status1 : status;
	}
      }
    } else {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorCleanReservation(%d,%d): eventNumber:%d\
 NOT RESERVED ERROR",
		  clientId,
		  eventId,
		  event->eventNumber );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_INTERNAL;
    }
    if ( ( status1 = monitorUnlockBuffer() ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorCleanReservation: monitorUnlockBuffer failed" );
      status = status == 0 ? status1 : status;
    }
  }

  LOG_DEVELOPER {
    char msg[1024 ];
    snprintf( SP(msg),
	      "monitorCleanReservation return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorCleanReservation */


/* Check if a free event descriptor is available to store a new event.
 *
 * Return:
 *   True if the event can be stored
 */
int monitorFreeReservations() {
  int status = FALSE;
  if ( *mbOldestEvent == -1 ) status = TRUE;
  else if ( *mbOldestEvent != *mbFirstFreeEvent ) status = TRUE;
  LOG_DEVELOPER {
    if ( status ) {
      INFO_TO( MON_LOG,
	       "monitorFreeReservations: event can be stored" );
    } else {
      INFO_TO( MON_LOG,
	       "monitorFreeReservations: no free descriptors for event" );
    }
  }
  return status;
} /* End of monitorFreeReservations */


/* Build a reservation event for a given event.
 *
 * Parameter:
 *   monitorEvent	The descriptor to build
 *   rawData		The raw data of the event
 *
 * Initialise:
 *   monitorEvent	All field excepted the eventData pointer
 *
 * Return:
 *   0    => OK
 *   else => error (bit coded)
 */
int monitorBuildReservation( monitorEventDescriptor *monitorEvent,
			     const void * const rawData ) {
  int status = 0;
  struct eventHeaderStruct *event = (struct eventHeaderStruct *)rawData;
  time_t now;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorBuildReservation \
event#:%d-%d type:%s size:%d data:%p",
	      EVENT_ID_GET_ORBIT(event->eventId),
	      EVENT_ID_GET_BUNCH_CROSSING(event->eventId),
	      monitorDumpEventType( event->eventType ),
	      event->eventSize,
	      rawData );
    INFO_TO( MON_LOG, msg );
  }

  if ( ( now = time( NULL ) ) == (time_t)(-1) ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorBuildReservation Failed to get time errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_SYS_ERROR;
  }

  if ( status == 0 ) {
    int mustUnlock;
    
    monitorEvent->eventType = event->eventType;
    monitorEvent->numYes = 0;
    monitorEvent->numMust = 0;
    monitorEvent->birthTime = now;
    monitorEvent->monitored = FALSE;
    if ( ( mustUnlock = ( ( status = monitorLockBuffer() ) == 0 ) ) ) {
      int client;
      int ix = monitorEvent->eventType - EVENT_TYPE_MIN;
      
      for ( client = 0; client != *mbMaxClients; client++ ) {
	monitorClientDescriptor *theClient = CLIENT( client );
	if ( theClient->inUse ) {
	  if ( monitorSelectedByAttributes(
		     event,
		     &(theClient->monitorAttributes[ix]),
		     theClient->monitorAttributesAnd[ix] )) {
	    if ( theClient->monitorPolicy[ix] == yes )
	      monitorEvent->numYes++;
	    if ( theClient->monitorPolicy[ix] == all )
	      monitorEvent->numMust++;
	  }
	}
      }
      LOG_DEVELOPER {
	char msg[ 2048 ];
	snprintf( SP(msg),
		  "monitorBuildReservation Record built \
eventType:x%08x numYes:%d numMust:%d birthTime:%ld",
		  monitorEvent->eventType,
		  monitorEvent->numYes,
		  monitorEvent->numMust,
		  monitorEvent->birthTime );
	INFO_TO( MON_LOG, msg );
      }
    } else {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorBuildReservation: failed to lock buffer" );
    }
    if ( mustUnlock ) {
      int status1 = monitorUnlockBuffer();
      if ( status1 != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorBuildReservation: failed to unlock buffer" );
      status = status == 0 ? status1 : status;
    }
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorBuildReservation Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }

  return status;
} /* End of monitorBuildReservation */


/* Insert a previously built Event descriptor record.
 *
 * Parameter:
 *   monitorEvent	The descriptor, fully initialised
 *
 * Assumes:
 *   Monitor buffer already locked
 *   
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorInsertReservation( monitorEventDescriptor *monitorEvent ) {
  int status = 0;
  int again;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorInsertReservation Event#:%d",
	      monitorEvent->eventNumber );
    INFO_TO( MON_LOG, msg );
  }

  /* Find a free event descriptor (if any) */
  do {
    again = FALSE;
    if ( *mbOldestEvent == -1 ) {

      /* All descriptors are free: use the first one */
      *mbOldestEvent = 0;
      *mbFirstFreeEvent = 1;
      memcpy( EVENT( 0 ), monitorEvent, sizeof( monitorEventDescriptor ) );
      monitorLastInsertedEvent = 0;
      
    } else {
      
      if ( *mbOldestEvent != *mbFirstFreeEvent ) {

	/* Use the first available descriptor */
	memcpy( EVENT( *mbFirstFreeEvent ),
		monitorEvent,
		sizeof( monitorEventDescriptor ) );
	monitorLastInsertedEvent = *mbFirstFreeEvent;
	*mbFirstFreeEvent = NEXT_EVENT( *mbFirstFreeEvent );
	
      } else {

	/* No descriptor available: can we make some space? */
	status = monitorFreeOne( monitorEvent->numMust != 0 );
	if ( status != 0 && status != -1 ) {
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorInsertReservation: monitorFreeOneFailed" );
	} else if ( status == 0 ) {
	  again = TRUE;
	} else {
	  status = 0;
	  if ( monitorEvent->numMust == 0 ) {
	    /* We can drop this event */
	    if ((status = monitorDropEvent( monitorEvent->eventData )) != 0 ) {
	      LOG_NORMAL
		ERROR_TO( MON_LOG,
			  "monitorInsertReservation: \
monitorDropEvent failed" );
	    }
	  } else {
	    /* We must monitor this event */
	    status = monitorWaitForFreeSpace();
	    again = TRUE;
	  }
	}
      }
    }
  } while( again && status == 0 );
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorInsertReservation return:(%d %d)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorInsertReservation */


/* Try to make space for this event (if possible). We see if this is the
 * only event of this type in the monitoring buffer. If so, we try to
 * eliminate an event whose type is "duplicated" in the monitoring buffer.
 *
 * Parameter:
 *   event	Pointer to the event we must make space for
 *
 * Assumes:
 *   Monitor buffer already locked
 *
 * Return:
 *   0    => OK, space found
 *   else => No space found
 */
int monitorMakeSpace( const struct eventHeaderStruct * const event ) {
  int i = *mbOldestEvent;

  if ( i == -1 ) return 1;	/* No events in buffer??? This should never
				 * happen! */
  /* First step: check if this type of event is already in the buffer */
  do {
    if ( EVENT( i )->eventType == event->eventType ) return 1;
  } while ( ( i = NEXT_EVENT( i ) ) != *mbFirstFreeEvent );

  /* OK: check if there is a "duplicated" event to discard */
  i = *mbOldestEvent;
  do {
    int j = NEXT_EVENT( i );
    while ( j != *mbFirstFreeEvent ) {
      if ( EVENT( i )->eventType == EVENT( j )->eventType ) {
	if ( EVENT( i )->numMust == 0 ) {
	  EVENT( i )->numYes = 0;
	  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorMakeSpace: freed" );
	  monitorCheckReservations( i );
	  return 0;
	}
      }
      j = NEXT_EVENT( j );
    }
  } while ( ( i = NEXT_EVENT( i ) ) != *mbFirstFreeEvent );
  return 1;
} /* End of monitorMakeSpace */


/* Try to free one event from the buffer.
 *
 * Parameter:
 *   mustDrop   Boolean TRUE if events must be dropped to make space for a
 *              "must monitor" event
 *   
 * Return:
 *   0    => OK, one event freed
 *   -1   => OK, no space available
 *   else => error
 */
int monitorFreeOne( int mustDrop ) {
  int i = *mbOldestEvent;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorFreeOne(%s) oldest event index:%d",
	      mustDrop ? "TRUE" : "FALSE", i );
    INFO_TO( MON_LOG, msg );
  }
  
  if ( i == -1 ) return 0;

  /* Pass one: free events that have been monitor at least once */
  do {
    if ( EVENT( i )->numMust == 0 && EVENT( i )->monitored ) {
      EVENT( i )->numYes = 0;
      LOG_DEVELOPER INFO_TO( MON_LOG, "monitorFreeOne: freed (pass one)" );
      return monitorCheckReservations( i );
    }
  } while ( ( i = NEXT_EVENT( i ) ) != *mbFirstFreeEvent );
  if ( !mustDrop ) return -1;

  /* Must drop: re-scan all the events, this time drop the first "non-must" */
  i = *mbOldestEvent;
  do {
    if ( EVENT( i )->numMust == 0 ) {
      EVENT( i )->numYes = 0;
      LOG_DEVELOPER INFO_TO( MON_LOG, "monitorFreeOne: freed (pass two)" );
      return monitorCheckReservations( i );
    }
  } while ( ( i = NEXT_EVENT( i ) ) != *mbFirstFreeEvent );

  /* No space... */
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorFreeOne: no space" );
  return -1;
} /* End of monitorFreeOne */


/* Find and copy next event to monitor.
 *
 * Parameters:
 *   id           client ID
 *   mustWait     TRUE if client wants to wait if no events are available now
 *   buffer       pointer to user buffer (size != 0) or to pointer (size == 0)
 *   sizeOfBuffer size of user buffer (if != 0)
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorFindNextEvent( int   id,
			  int   mustWait,
			  void *buffer,
			  int   sizeOfBuffer ) {
  int status = 0;
  int status1;
  int done = FALSE;
  
  do {
    int ev;

    /* Is there one event to monitor for this client? */
    if ( (ev = monitorFindFirstUnmonitored( id )) >= 0 ) {
      int doSignal = FALSE;

      /* Yes: adjust stamps, counters and flags */
      monitorEventDescriptor *theEvent = EVENT( ev );
      CLIENT( id )->lastEventNumber = theEvent->eventNumber;

      /* Has this client requested to monitor this event type and how? */
      if ( SHOULD_MONITOR( ev, id ) ) {
	done = TRUE;
	(theEvent->numYes)--;
	  
      } else if ( MUST_MONITOR( ev, id ) ) {
	done = TRUE;
	doSignal = --(theEvent->numMust) == 0;
      }

      if ( done ) {
	/* This client requested this event for monitoring. Mark the
	 * event monitored. This will make it eligible for dropping
	 * if space is needed */
	theEvent->monitored = TRUE;

	/* Allocate dynamic buffer if requested */
	if ( sizeOfBuffer == 0 ) {
	  char   **result = (char **)buffer;
	  long32   evLen  = *(long32 *)
	                      (ADD(ADD(mbFreeEvents, theEvent->eventData),
				   QUEUE_HEAD_LEN));
	  LOG_DEVELOPER {
	    char msg[ 1024 ];
	    snprintf( SP(msg), 
		      "monitorFindNextEvent Allocating %d bytes", evLen );
	    INFO_TO( MON_LOG, msg );
	  }
	  if ( ( buffer = (void *)malloc( (size_t)evLen ) ) == NULL ) {
	    LOG_DEVELOPER {
	      char msg[ 1024 ];
	      snprintf( SP(msg),
		       "monitorFindNextEvent Failed to allocate %d bytes",
			evLen );
	      ERROR_TO( MON_LOG, msg );
	    }
	    status = MON_ERR_MALLOC;
	  } else {
	    *result = (char*)buffer;
	    sizeOfBuffer = evLen;
	  }
	}

	if ( status == 0 ) {
	  /* Copy the event into the user buffer */
	  if ( ( status = monitorCopyEvent( buffer,
					    theEvent->eventData,
					    sizeOfBuffer ) ) != 0 ) {
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorFindNextEvent: monitorCopyEvent failed" );
	  } else {
	    (CLIENT( id )->eventsRead)++;
	    monitorAddToCounter( *(long32*)(buffer),
				 (long32*)(&(CLIENT(id)->bytesRead)) );
	  }
	}
	if ( ( status1 = monitorCheckReservations( ev ) ) != 0 ) {
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorFindNextEvent: \
monitorCheckReservations failed" );
	  status = status == 0 ? status1 : status;
	}
	if ( doSignal ) {
	  if ( ( status1 = monitorSignalFreeSpace() ) != 0 ) {
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorFindNextEvent: \
monitorSignalFreeSpace failed" );
	    status = status == 0 ? status1 : status;
	  }
	}
      }
    }

    /* If event if not found and the client wants to wait, wait */
    if ( !done && mustWait && status == 0 ) {
      if ( ( status = monitorWaitForNewEvents() ) != 0 ) {
	if ( status == MON_ERR_INTERRUPTED ) {
	  LOG_DEVELOPER
	    INFO_TO( MON_LOG,
		     "monitorFindNextEvent: WaitForNewEvents interrupted" );
	} else {
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorFindNextEvent: WaitForNewEvents failed" );
	}
      }
    }
    
  } while ( status == 0 && !done && mustWait );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorFindNextEvent return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorFindNextEvent */


/* Check for obsolete events, events that do not need to be monitored by any
 * of the clients currently registered (if any).
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorCheckObsoleteEvents() {
  int status, status1;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorCheckObsoleteEvents: starting" );

  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_DEVELOPER
      INFO_TO( MON_LOG, "monitorCheckObsoleteEvents: failed to lock buffer" );
  } else {
    int i;
    int ev = *mbOldestEvent;
    int lastMonitored = -1;

    if ( ev >= 0 ) {
      for ( i = 0; status == 0 && i != (*mbMaxClients); i++ ) {
	monitorClientDescriptor *c = CLIENT( i );
	if ( c->inUse ) {
	  if ( c->lastEventNumber < lastMonitored || lastMonitored == -1 ) {
	    lastMonitored = c->lastEventNumber;
	  }
	}
      }
      do {
	monitorEventDescriptor *event = EVENT( ev );
	if ( event->eventNumber <= lastMonitored ) {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorCheckObsoleteEvents \
Discarding event:#%d index:%d",
		      (int)event->eventNumber, ev );
	    INFO_TO( MON_LOG, msg );
	  }
	  event->numMust = 0;
	  event->numYes = 0;
	  if ( ( status = monitorCheckReservations( ev ) ) != 0 )
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorCheckObsoleteEvents: \
monitorCheckReservations failed" );
	} else {
	  ev = NEXT_EVENT( ev );
	}
      } while ( ev < *mbFirstFreeEvent && status == 0 );
    }
    if ( ( status1 = monitorUnlockBuffer() ) != 0 ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorCheckObsoleteEvents: failed to unlock buffer" );
      status = status == 0 ? status1 : status;
    }
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	     "monitorCheckObsoleteEvents Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorCheckObsoleteEvents */


/* Check the events' (if any) age and expire old events.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorCheckEventsAge( time_t now ) {
  int status, status1;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorCheckEventsAge: starting" );

  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_DEVELOPER
      INFO_TO( MON_LOG, "monitorCheckEventsAge: failed to lock buffer" );
  } else {
    int ev = *mbOldestEvent;

    if ( ev >= 0 ) {
      do {
	monitorEventDescriptor *event = EVENT( ev );
	if ( ( now - event->birthTime ) >= monitorMaxAge && 
	     event->numMust == 0 ) {
	  event->numYes = 0;
	  status = monitorCheckReservations( ev );
	} else {
	  ev = NEXT_EVENT( ev );
	}
      } while ( ev < *mbFirstFreeEvent && status == 0 );
    }
    if ( ( status1 = monitorUnlockBuffer() ) != 0 ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorCheckEventsAge: failed to unlock buffer" );
      status = status == 0 ? status1 : status;
    }
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCheckEventsAge Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }

  return status;
} /* End of monitorCheckEventsAge */
