#include <stdio.h>
#include <stdlib.h>
#include "monitor.h"

extern void printDataSource();

int main( int argc, char **argv ) {
  int status;

  if ( argc != 2 ) {
    fprintf( stderr, "Usage: %s dataSource\n", argv[0] );
    exit( 1 );
  }

  status = monitorSetDataSource( argv[1] );
  printf( "monitorSetDataSource( \"%s\" ) returns %d\n",
	  argv[1], status );

  printDataSource();

  exit( 0 );
}
