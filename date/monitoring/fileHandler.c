/* Monitor V 3.xx file handler module
 * ==================================
 *
 * This is the module responsible for the handling of monitoring
 * targeted towards files.
 *
 * Compilation switches:
 *    NO_RFIO   Define if RFIO is not needed
 *
 * Module history:
 *  1.00  1-Sep-98 RD  Created
 *  1.01  3-May-99 RD  Mods for input from pipes added
 *  1.02 23-Jun-99 RD  Swapping header/buffer added
 *  1.03  5-Jul-99 RD  Event attributes handling added
 *  1.04 27-Oct-00 RD  Bug with zero-length payloads fixed
 *  1.05  1-Dec-00 RD  Updated for DATE 4.x
 *  1.06  5-Jul-01 RD  Handling of "&" and "|" of attributes
 *  1.07 14-Nov-01 RD  Fixup for short events
 *  1.08 21-Nov-01 RD  Fixup for header extensions
 */
#define VID "1.08"

#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#ifndef NO_RFIO
#include <shift.h>
#endif

#include "event.h"
#include "monitorInternals.h"

#define DESCRIPTION "DATE V3 monitor file handler"
#ifdef NO_RFIO
#define EXTRA " (without RFIO)"
#else
#define EXTRA " (with RFIO)"
#endif
#ifdef AIX
static
#endif
char fileHandlerIdent[]= "@(#)""" __FILE__ """: """ DESCRIPTION EXTRA \
                         """ """ VID """ """ \
                         """ compiled """ __DATE__ """ """ __TIME__;


static int policyDeclared = FALSE;
static int policy[ EVENT_TYPE_RANGE ];
static int attributesAnd[ EVENT_TYPE_RANGE ];
static eventTypeAttributeType attributes[ EVENT_TYPE_RANGE ];
static char dummy[ 512 ];

static int in = -1;

extern int monitorSwappingNeeded;
extern int monitorByteSwappingRequested;
extern int monitorWordSwappingRequested;

/* Open the given monitor channel.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorFileOpenChannel( char *monitorBufferName ) {
  int status = 0;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorFileOpenChannel(%s)", monitorBufferName );
    INFO_TO( MON_LOG, msg );
  }

  if ( (in = open( monitorBufferName, O_RDONLY, 0x666 )) < 0 ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorFileOpenChannel Cannot open file errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    if ( errno == ENOENT )
      status = MON_ERR_NO_SUCH_FILE;
    else
      status = MON_ERR_SYS_ERROR;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorFileOpenChannel Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorFileOpenChannel */


/* Disconnect from the monitor channel.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorFileDisconnect() {
  int status = 0;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorFileDisconnect: starting" );
  
  if ( in != -1 ) {
    if ( close( in ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorFileDisconnect Close failed errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_SYS_ERROR;
    }
    in = -1;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorFileDisconnect Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorFileDisconnect */


/* Declare a monitor policy table to be used in subsequent I/Os.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorFileDeclareTable( monitorRecordPolicy *table ) {
  int i;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorFileDeclareTable: starting" );
  
  policyDeclared = FALSE;
  for ( i = 0; i != EVENT_TYPE_RANGE; i++ ) {
    policy[ i ] = DEFAULT_MONITOR_POLICY;
    RESET_ATTRIBUTES( attributes[i] );
  }
  while ( table != NULL ) {
    if ( monitorParsePolicyEntry( table->eventType,
				  table->monitorType,
				  table->monitorAttributes ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorFileDeclareTable Parse failed on record \
\"%s\" \"%s\"",
		  table->eventType, table->monitorType );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_INTERNAL;
    }
    if ( thisEventType == -1 ) {
      for ( i = 0; i != EVENT_TYPE_RANGE; i++ ) {
	policy[ i ] = thisEventMonitorType != no;
	COPY_ALL_ATTRIBUTES( thisEventMonitorAttributes, attributes[i] );
	attributesAnd[i] = thisEventMonitorAnd;
      }
    } else {
      policy[ thisEventType ] = thisEventMonitorType != no;
      COPY_ALL_ATTRIBUTES( thisEventMonitorAttributes,
			   attributes[ thisEventType ] );
      attributesAnd[ thisEventType ] = thisEventMonitorAnd;
    }
    table = table->next;
  }
  policyDeclared = TRUE;
  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorFileDeclareTable: returning 0" );
  return 0;
} /* End of monitorFileDeclareTable */


/* myRead: read the input file, eventually breaking the read into pieces.
 *
 * Parameters:
 *   handle	file handler
 *   buffer	destination for the read
 *   size	size of the read
 *
 * Returns:
 *   status of the read, eventually integrated by errno.
 */
int myRead( int handle, void *buffer, int size ) {
  int status;
  int done = 0;
  int gErrno = 0;

  if ( size == 0 ) return 0;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "myRead(%d,%p,%d)", handle, buffer, size );
    INFO_TO( MON_LOG, msg );
  }
  do {
    errno = 0;
    status = read( handle, buffer, size );
    if ( status > 0 ) {
      size -= status;
      done += status;
      buffer = (void *)((char *)buffer + status );
    } else {
      gErrno = errno;
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "myRead Error during read:%d errno:%d (%s) buffer:%p \
read:%d left:%d",
		  status,
		  gErrno, strerror(gErrno),
		  buffer,
		  done,
		  size );
	ERROR_TO( MON_LOG, msg );
      }
    }
  } while ( status > 0 && size > 0 );
  if ( done > 0 ) status = done;
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "myRead Return:%d errno:%d (%s)",
	      status, gErrno, strerror(gErrno) );
    INFO_TO( MON_LOG, msg );
  }
  errno = gErrno;
  return status;
} /* End of myRead */


/* mySeek: seek the input file, handle special cases such as pipes.
 *
 * Parameters:
 *   toDo	Number of bytes to seek
 *
 * Returns:
 *   status of seek, usually integrated with errno.
 */
int mySeek( int toDo ) {
  int status;
  
  if ( toDo == 0 ) return 0;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "mySeek(%d)", toDo );
    INFO_TO( MON_LOG, msg );
  }

  errno = 0;
  if ( ( status = lseek( in, toDo, SEEK_CUR ) ) == (-1) ) {
    if ( errno == ESPIPE ) {
      do {
	status = myRead( in,
		       dummy,
		       MIN( sizeof( dummy ), toDo ) );
	if ( status >= 0 ) toDo -= status;
      } while ( status > 0 && toDo != 0 );
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    int gErrno = errno;
    snprintf( SP(msg),
	      "mySeek Return:%d errno:%d (%s)",
	      status, gErrno, strerror(gErrno) );
    INFO_TO( MON_LOG, msg );
    errno = gErrno;
  }
  return status;
} /* End of mySeek */


/* Get next event from file.
 *
 * Parameters:
 *   buffer         pointer to the buffer (if sizeOfBuffer != 0)
 *                  address of a pointer (if sizeOfBuffer == 0)
 *   sizeOfBuffer   size of buffer (if > HEADER_LENGTH)
 *                  0 => use dynamic memory allocation
 *
 * Returns:
 *   0       => OK
 *   else    => error
 *   *buffer == 0 (or NULL) with status == 0 means no event available
 *
 * This procedure has a BIG problem. It may read events written with format
 * different from the current one. For all format-dependent points, look for
 * the label [FORMAT].
 */
int monitorFileNextEvent( void *buffer, int sizeOfBuffer ) {
  static struct eventHeaderStruct *header;
  static int    headerSize = 0;
  int    status;
  int    again;
  int    toRead;
  void  *ptr;
  int    payloadSize;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorFileNextEvent(%p,%d)", buffer, sizeOfBuffer );
    INFO_TO( MON_LOG, msg );
  }
  
  if ( !policyDeclared ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorFileNextEvent: policy table not yet declared" );
    return MON_ERR_INTERNAL;
  }

  if ( sizeOfBuffer != 0 && sizeOfBuffer < EVENT_HEAD_BASE_SIZE ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorFileNextEvent Invalid buffer size:%d",
		sizeOfBuffer );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_BAD_INPUT;
  }

  if ( headerSize == 0 ) {
    headerSize = EVENT_HEAD_BASE_SIZE;
    if ( (header = malloc( headerSize )) == NULL ) {
      LOG_NORMAL {
	char msg[1024];
	snprintf( SP(msg),
		  "monitorFileNextEvent Failed to allocate %d bytes for header\
 errno:%d (%s)",
		  headerSize, errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_MALLOC;
    }
  }

  do {
    /* First read: get only the first three long32s:
         eventSize
	 eventMagic
	 eventHeadSize
    */
#define PREAMBLE (3 * (int)sizeof(long32))
    if ( (status = myRead( in, header, PREAMBLE )) != PREAMBLE ) {
      LOG_NORMAL {
	char msg[1024];
	snprintf( SP(msg),
		  "monitorFileNextEvent Header preamble read error\
 status:%d PREAMBLE:%d errno:%d (%s)",
		  status, PREAMBLE, errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return status == 0 ? MON_ERR_EOF : MON_ERR_SYS_ERROR;
    }
    /* Check if the preamble looks OK (eventually swap bytes), if the result
       is not correct abort the procedure */
    if ( header->eventMagic == EVENT_MAGIC_NUMBER_SWAPPED ) {
      monitorSwappingNeeded = TRUE;
      monitorSwapBuffer( header, PREAMBLE, TRUE, FALSE );
    }
    if ( header->eventMagic    != EVENT_MAGIC_NUMBER
      || header->eventSize     <= PREAMBLE
      || header->eventHeadSize <= PREAMBLE
      || header->eventSize     <  header->eventHeadSize ) {
      LOG_NORMAL {
	char msg[1024];
	snprintf( SP(msg),
		  "monitorFileNextEvent Header preamble corrupted\
 magic:x%08x eventSize:%d eventHeadSize:%d swapped:%s",
		  header->eventMagic,
		  header->eventSize,
		  header->eventHeadSize,
		  monitorSwappingNeeded ? "TRUE" : "false" );

	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_BAD_EVENT;
    }
    /* If the space currently allocated for the header is not enough, get more
       space */
    if ( header->eventHeadSize > headerSize ) {
      headerSize = header->eventHeadSize;
      if ( (header = realloc( header, headerSize )) == NULL ) {
	LOG_NORMAL {
	  char msg[1024];
	  snprintf( SP(msg),
		    "monitorFileNextEvent\
 Failed to reallocate %d bytes for header errno:%d (%s)",
		    headerSize, errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
	return MON_ERR_MALLOC;
      }
    }
    /* Read in the rest of the header using the header size written in the
       event header */
    ptr = (void *)((datePointer)header + PREAMBLE);
    toRead = header->eventHeadSize - PREAMBLE;
    if ( (status = myRead( in, ptr, toRead )) != toRead ) {
      LOG_DETAILED {
	char msg[1024];
	snprintf( SP(msg),
		  "monitorFileNextEvent Header first read error\
 status:%d toRead:%d PREAMBLE:%d eventHeadSize:%d errno:%d (%s)",
		  status,
		  toRead,
		  PREAMBLE,
		  header->eventHeadSize,
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_SYS_ERROR;
    }
    /* If needed, swap the rest of the header [FORMAT] */
    if ( monitorSwappingNeeded ) {
      monitorSwapBuffer( ptr, toRead, TRUE, FALSE );
      FLIP_SYSTEM_ATTRIBUTE( header->eventTypeAttribute, ATTR_EVENT_SWAPPED );
    }

    /* Check the other fields of the header [FORMAT] */
    if ( ! EVENT_TYPE_OK( header->eventType )
      || ! TRIGGER_PATTERN_OK( header->eventTriggerPattern )
      || ! DETECTOR_PATTERN_OK( header->eventDetectorPattern )
      || ! SYSTEM_ATTRIBUTES_OK( header->eventTypeAttribute ) ) {
      LOG_DETAILED {
	char msg[1024];
	snprintf( SP(msg),
		  "monitorFileNextEvent Header first read error\
 eventSize:%d magic:x%08x headSize:%d\
 triggerPattern:%08x-%08x%s\
 eventType:%d%s\
 attributes System:%08x%s User:%08x-%08x%s",
		  header->eventSize,
		  header->eventMagic,
		  header->eventHeadSize,
		  header->eventTriggerPattern[0],
		  header->eventTriggerPattern[1],
		  TRIGGER_PATTERN_OK(header->eventTriggerPattern) ?
		    "" : " >CHECK<",
		  header->eventType,
		  EVENT_TYPE_OK(header->eventType) ? "" : " >CHECK<",
		  header->eventTypeAttribute[0],
		  SYSTEM_ATTRIBUTES_OK( header->eventTypeAttribute ) ?
		    "" : " >CHECK<",
		  header->eventTypeAttribute[1],
		  header->eventTypeAttribute[2],
		  DETECTOR_PATTERN_OK( header->eventDetectorPattern ) ?
		    "" : " >CHECK DETECTOR PATTERN<" );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_BAD_EVENT;
    }

    /* Calculate payload size and che if this event is selected by the
       current tables [FORMAT] */
    payloadSize = header->eventSize - header->eventHeadSize;
    int sel=monitorSelectedByAttributes(
		     &header,
		     &attributes[header->eventType],
		     attributesAnd[header->eventType]);
 
    again =  !( policy[ header->eventType ] && sel);
    //    again=0;
    if (again) {
      if ( ( status = mySeek( payloadSize ) ) == (-1) ) {
	LOG_DEVELOPER {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorFileNextEvent Seek failed\
 payloadSize:%d errno:%d (%s)",
		    payloadSize, errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
	return MON_ERR_SYS_ERROR;
      }
    }
  } while ( again );

  /* Allocate a buffer from the pool if requested */
  if ( sizeOfBuffer == 0 ) {
    void *p;
    
    if ( (p = malloc( (size_t)header->eventSize )) == NULL ) {
      LOG_DEVELOPER
	ERROR_TO( MON_LOG,
		  "monitorFileNextEvent: malloc for event data failed" );
      //printf("BD MONITOR MALLOC");
      return MON_ERR_MALLOC;
    }
    *(void**)buffer = p;
    buffer = p;
    sizeOfBuffer = header->eventSize;
  }


  /* Copy the header and read in the data. Note that the user buffer may
     be too small for the event. In this case we read whatever we can and
     skip the rest.
   */
  memcpy( (void *)buffer, header, header->eventHeadSize );
  ptr = (void *)((datePointer)buffer + header->eventHeadSize);
  toRead = MIN( header->eventSize, sizeOfBuffer );
  toRead -= header->eventHeadSize;
  if ( ( status = myRead( in, ptr, toRead )) != toRead ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorFileNextEvent Read for event data failed\
 status:%d expected:%d errno:%d (%s)",
		status, toRead, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_BAD_EVENT;
  }
  if ( ( status = mySeek( payloadSize - toRead ) ) == (-1) ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorFileNextEvent Seek for leftover failed\
 payloadSize:%d toRead:%d errno:%d (%s)",
		payloadSize, toRead, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }

  /* If needed, swap the payload using the requested policies */
  if ( monitorSwappingNeeded ) {
    monitorSwapBuffer( ptr, toRead,
		       monitorByteSwappingRequested,
		       monitorWordSwappingRequested );
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    struct eventHeaderStruct *result = (struct eventHeaderStruct *)buffer;
    snprintf( SP(msg),
	      "monitorFileNextEvent Read completed \
size:(%d %d) magic:(x%08x x%08x) type:(%d %d)",
	      header->eventSize,  result->eventSize,
	      header->eventMagic, result->eventMagic,
	      header->eventType,  result->eventType );
    INFO_TO( MON_LOG, msg );
  }
	     
  return 0;
} /* End of monitorFileNextEvent */
