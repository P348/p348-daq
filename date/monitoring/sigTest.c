#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "event.h"
#include "monitor.h"
#include "testLib.h"

void sigHandler( int sigNum ) {
  if ( signal( SIGHUP, sigHandler ) == SIG_ERR ) {
    perror( "Cannot declare signal handler... System-dependent status " );
  }
}

int main( int argc, char **argv ) {
  int status;
  int buffer[ 1000 ];
  
  if ( signal( SIGHUP, sigHandler ) == SIG_ERR ) {
    perror( "Cannot declare signal handler... System-dependent status " );
    return 1;
  }

  if ( ( status = monitorSetDataSource( "/tmp/keyFile:" ) ) != 0 ) {
    printf( "Error calling monitorSetDataSource, status: %d: %s\n",
	    status, monitorDecodeError( status ) );
  }
  
  printf( "Signal handler declared. Calling monitorGetEvent. Shoot at will!\n" );
  for (;;)
    if ( (status = monitorGetEvent( buffer, sizeof( buffer ))) != 0 ) {
      printf( "Error getting next event: %d (0x%08x): %s\n",
	      status, status,
	      monitorDecodeError( status ) );
    } else {
      if ( debug ) printf( "Event got OK\n" );
    }

  return 0;
}
