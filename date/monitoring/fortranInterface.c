/* Monitor V 3.xx FORTRAN client interface module
 * ==============================================
 *
 * This is the module that defines the FORTRAN API to the DATE monitoring
 *
 * Compilation switches:
 *  PRODUCER   Include only code for DAQ producers
 *
 * FORTRAN => C calling conventions:
 *
 *   1) FORTRAN subroutines calls are converted to C calls as follows:
 *        CALL xxx    =>  void xxx OR void xxx_ OR void xxx__
 *
 *   2) FORTRAN TRUE and FALSE are defined by FORTRAN_TRUE and FORTRAN_FALSE
 *
 *   3) FORTRAN strings are passed by reference with one extra parameter
 *      giving the string length (type of this parameter defined by STRLEN)
 *
 *   4) FORTRAN parameters are all passed by address
 *
 * Module history:
 *  1.00 11-Nov-98 RD   Created, extracted code from clientInterface.c
 */
#define VID "1.00"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "monitorInternals.h"

#define DESCRIPTION "DATE V3 monitor FORTRAN interface"
#ifdef PRODUCER
#define ROLE " PRODUCER"
#else
#define ROLE " CONSUMER"
#endif
#ifdef AIX
static
#endif
char monitorFortranInterfaceIdent[] = \
   "@(#)""" __FILE__ """: """ DESCRIPTION ROLE \
   """ """ VID """ compiled """ __DATE__ """ """ __TIME__;


/* TRUE and FALSE for FORTRAN code */
#define FORTRAN_TRUE( x )  ((x) != 0)
#define FORTRAN_FALSE( x ) (! FORTRAN_TRUE(x))


/* The type of the string length parameters passed from FORTRAN to C */
#define STRLEN long int


#ifndef PRODUCER

/* Copy a C string into a Fortran string
 *
 * Parameters:
 *   in    input (C) string
 *   out   output (FORTRAN) string
 *   len   Length of output string
 */
static void monitorCopyStringToFortran( char *in, char *out, STRLEN len ) {
  int   outLen = 0;
  while ( *in != 0 && outLen != len ) {
    *out++ = *in++;
    outLen++;
  }
  while ( outLen++ != len ) *out++ = ' ';
} /* End of monitorCopyStringToFortran */


/* Duplicate a FORTRAN string into a C string.
 *
 * Parameters:
 *   inStr   Input (FORTRAN) string
 *   size    Length of input string
 *
 * Returns:
 *   pointer to newly created string
 *
 * NOTE: the returned string must be disposed when done.
 */
static char *monitorCopyFortranString( char *inStr, STRLEN size ) {
  char *p, *p1;
  int   i;
  char *result = strdup( inStr );
  if ( result != NULL ) {
    for ( i = 0, p = result, p1 = result;
	  i++ != size && *p != 0;
	  p++ )
      if ( *p != ' ' ) p1 = p;
    *++p1 = 0;
  }
  return result;
} /* End of monitorCopyFortranString */
  

/* MONITOR_SET_DATA_SOURCE entry
 */
int monitor_set_data_source( char *source, STRLEN size ) {
  int status;
  char *ourSource = monitorCopyFortranString( source, size );
  if ( ourSource == NULL ) return MON_ERR_MALLOC;
  status = monitorSetDataSource( ourSource );
  free( ourSource );
  return status;
}
int monitor_set_data_source_( char *source, STRLEN size ) {
  return monitor_set_data_source( source, size );
}
int monitor_set_data_source__( char *source, STRLEN size ) {
  return monitor_set_data_source( source, size );
}
  

/* MONITOR_DECLARE_MP entry
 */
int monitor_declare_mp( char *mp, STRLEN size ) {
  int status;
  char *ourMp = monitorCopyFortranString( mp, size );
  if ( ourMp == NULL ) return MON_ERR_MALLOC;
  status = monitorDeclareMp( ourMp );
  free( ourMp );
  return status;
}
int monitor_declare_mp_( char *mp, STRLEN size ) {
  return monitor_declare_mp( mp, size );
}
int monitor_declare_mp__( char *mp, STRLEN size ) {
  return monitor_declare_mp( mp, size );
}


/* MONITOR_DECLARE_TABLE entry
 */
int monitor_declare_table( char *inTable, STRLEN size ) {
#define MAX_TABLE 50
  char *table[ MAX_TABLE ];
  int   currEntry = -1;
  int   status = 0;
  char *in = monitorCopyFortranString( inTable, size );

  LOG_DETAILED {
    char msg[ 2048 ];
    snprintf( SP(msg), "monitor_delare_table(\"%s\", %ld )", in, size );
    INFO_TO( MON_LOG, msg );
  }

  if ( in == NULL ) return  MON_ERR_MALLOC;

  do {
    if ( ++currEntry == MAX_TABLE ) {
      status = MON_ERR_PARSE_ERROR;
    } else {
      if ( currEntry == 0 )
	table[ currEntry ] = strtok( in, " " );
      else
	table[ currEntry ] = strtok( NULL, " " );
    }
  } while ( status == 0 && table[ currEntry ] != NULL );

  LOG_DETAILED {
    char msg[ 100 + ( MAX_TABLE * 100 ) ];
    int i;
    
    snprintf( SP(msg), "\tResulting table:\n" );
    for ( i = 0; table[ i ] != NULL; i++ )
      snprintf( AP(msg), 
		"\"%s\"%c",
		table[ i ],
		(i % 2) == 0 ? ' ' : '\n' );
    INFO_TO( MON_LOG, msg );
  }

  if ( status == 0 )
    status = monitorDeclareTable( table );
  free( in );
  return status;
}
int monitor_declare_table_( char *inTable, STRLEN size ) {
  return monitor_declare_table( inTable, size );
}
int monitor_declare_table__( char *inTable, STRLEN size ) {
  return monitor_declare_table( inTable, size );
}


/* MONITOR_DECLARE_TABLE_WITH_ATTRIBUTES entry
 */
int monitor_declare_table_with_attributes( char *inTable, STRLEN size ) {
#define MAX_TABLE 50
  char *table[ MAX_TABLE ];
  int   currEntry = -1;
  int   status = 0;
  char *in = monitorCopyFortranString( inTable, size );

  LOG_DETAILED {
    char msg[ 2048 ];
    snprintf( SP(msg),
	      "monitor_delare_table_with_attributes(\"%s\",%ld)",
	      in, size );
    INFO_TO( MON_LOG, msg );
  }

  if ( in == NULL ) return  MON_ERR_MALLOC;

  do {
    if ( ++currEntry == MAX_TABLE ) {
      status = MON_ERR_PARSE_ERROR;
    } else {
      if ( currEntry == 0 )
	table[ currEntry ] = strtok( in, " " );
      else
	table[ currEntry ] = strtok( NULL, " " );
    }
  } while ( status == 0 && table[ currEntry ] != NULL );

  LOG_DETAILED {
    char msg[ 100 + ( MAX_TABLE * 100 ) ];
    int i;
    
    snprintf( SP(msg), "\tResulting table:\n" );
    for ( i = 0; table[ i ] != NULL; i++ )
      snprintf( AP(msg),
		"\"%s\"%c",
		table[ i ],
		(i % 2) == 0 ? ' ' : '\n' );
    INFO_TO( MON_LOG, msg );
  }

  if ( status == 0 )
    status = monitorDeclareTableWithAttributes( table );
  free( in );
  return status;
}
int monitor_declare_table_with_attributes_( char *inTable, STRLEN size ) {
  return monitor_declare_table_with_attributes( inTable, size );
}
int monitor_declare_table_with_attributes__( char *inTable, STRLEN size ) {
  return monitor_declare_table_with_attributes( inTable, size );
}


/* MONITOR_GET_EVENT entry
 */
int monitor_get_event( void *data, int *size ) {
  return monitorGetEvent( data, *size );
}
int monitor_get_event_( void *data, int *size ) {
  return monitor_get_event( data, size );
}
int monitor_get_event__( void *data, int *size ) {
  return monitor_get_event( data, size );
}


/* MONITOR_GET_EVENT_DYNAMIC entry
 */
int monitor_get_event_dynamic( void **ptr ) {
  return monitorGetEventDynamic( ptr );
}
int monitor_get_event_dynamic_( void **ptr ) {
  return monitor_get_event_dynamic( ptr );
}
int monitor_get_event_dynamic__( void **ptr ) {
  return monitor_get_event_dynamic( ptr );
}


/* MONITOR_FREE_EVENT entry
 */
void monitor_free_event( void **ptr ) {
  free( *ptr );
}
void monitor_free_event_( void **ptr ) {
  monitor_free_event( ptr );
}
void monitor_free_event__( void **ptr ) {
  monitor_free_event( ptr );
}


/* MONITOR_FLUSH_EVENTS entry
 */
int monitor_flush_events() {
  return monitorFlushEvents();
}
int monitor_flush_events_() {
  return monitor_flush_events();
}
int monitor_flush_events__() {
  return monitor_flush_events();
}


/* MONITOR_SET_WAIT entry
 */
int monitor_set_wait() {
  return monitorSetWait();
}
int monitor_set_wait_() {
  return monitor_set_wait();
}
int monitor_set_wait__() {
  return monitor_set_wait();
}


/* MONITOR_SET_NOWAIT entry
 */
int monitor_set_nowait() {
  return monitorSetNowait();
}
int monitor_set_nowait_() {
  return monitor_set_nowait();
}
int monitor_set_nowait__() {
  return monitor_set_nowait();
}


/* MONITOR_CONTROL_WAIT entry
 */
int monitor_control_wait( int *flag ) {
  return monitorControlWait( FORTRAN_TRUE( *flag ) );
}
int monitor_control_wait_( int *flag ) {
  return monitor_control_wait( flag );
}
int monitor_control_wait__( int *flag ) {
  return monitor_control_wait( flag );
}


/* MONITOR_SET_SWAP entry
 */
int monitor_set_swap( int *flag1, int *flag2 ) {
  return monitorSetSwap( FORTRAN_TRUE( *flag1 ), FORTRAN_TRUE( *flag2 ));
}
int monitor_set_swap_( int *flag1, int *flag2 ) {
  return monitor_set_swap( flag1, flag2 );
}
int monitor_set_swap__( int *flag1, int *flag2 ) {
  return monitor_set_swap( flag1, flag2 );
}


/* MONITOR_LOGUT entry
 */
int monitor_logout() {
  return monitorLogout();
}
int monitor_logout_() {
  return monitorLogout();
}
int monitor_logout__() {
  return monitorLogout();
}


/* MONITOR_DECODE_ERROR entry
 */
void monitor_decode_error( int *error, char *result, STRLEN len ) {
  monitorCopyStringToFortran( monitorDecodeError( *error ), result, len );
}
void monitor_decode_error_( int *error, char *result, STRLEN len ) {
  monitor_decode_error( error, result, len );
}
void monitor_decode_error__( int *error, char *result, STRLEN len ) {
  monitor_decode_error( error, result, len );
}

/************************** FORTRAN OLD interface ***************************
 *
 * WARNING: this section is given "as-is". No changes are planned and
 * recommended
 */
int get_event__( char *data, int* length ) {
  return getevent( data, *length);
}
int get_event_( char *data, int* length ) {
  return getevent( data, *length);
}
int get_event( char *data, int* length) {
  return getevent( data, *length );
}

int flush_event__() {
  FlushGetEventBuffer();
  return 0;
}
int flush_event_() {
  FlushGetEventBuffer();
  return 0;
}
int flush_event() {
  FlushGetEventBuffer();
  return 0;
}

int set_data_source__( char* nodeName ) {
  return SetDataSource( nodeName );
}
int set_data_source_( char* nodeName ) {
  return SetDataSource( nodeName );
}
int set_data_source( char* nodeName) {
  return SetDataSource( nodeName );
}

int set_get_event_interactive__() {
  SetGetEventInteractive();
  return 0;
}
int set_get_event_interactive_() {
  SetGetEventInteractive();
  return 0;
}
int set_get_event_interactive() {
  SetGetEventInteractive();
  return 0;
}

int set_get_event_batch__() {
  SetGetEventBatch();
  return 0;
}
int set_get_event_batch_() {
  SetGetEventBatch();
  return 0;
}
int set_get_event_batch() {
  SetGetEventBatch();
  return 0;
}
#endif
