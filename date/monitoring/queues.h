/* DATEV3 queues handler definition
 * ================================
 *
 * Compilation symbols:
 *	HANDLE_SIZE	Size of data chunks handled by queue handlers
 */

#ifndef __queues_h__
#define __queues_h__

#define HANDLE_SIZE

/* The queue descriptor: */
struct queuePtr {
  datePointer next;
  datePointer prev;
#ifdef HANDLE_SIZE
  int     size;
#endif
};

/* The data chunk descriptor: basically identical to the queue descriptor,
 * it provides a data[] pointer for easy access to the data area */
struct chunkStruct {
  datePointer next;
  datePointer prev;
#ifdef HANDLE_SIZE
  int     size;
#endif
  int     data[1];
};


#define QUEUE_HEAD_LEN sizeof( struct queuePtr )


/* Queues allow initialisation, check, statistics, insertion (first,
 * last, after a given element, before a given element or in memory
 * address order - useful for gathering of chunks into super-chunks),
 * extraction (extract the first element, the last or a given element)
 * and scanning (get next/previous element from the list)
*/
char  *queueSupportGetVid();
int    initQueue(struct queuePtr*);	        /* -> 0: OK, else: ERROR */
int    checkQueue(struct queuePtr*);            /* -> 0: OK, else: ERROR */
int    getNumOfFragments(struct queuePtr*);
float  getAverageFragmentSize(struct queuePtr*);
int    insertFirst(struct queuePtr*, void*);    /* -> 0: OK, else: ERROR */
int    insertLast(struct queuePtr *, void*);    /* -> 0: OK, else: ERROR */
int    insertAfter(struct queuePtr*,
		    void*,	/* Insert after this                     */
		    void*);	/* Insert this                           */
                                                /* -> 0: OK, else: ERROR */
int    insertBefore(struct queuePtr*,
		    void*,	/* Insert before this                    */
		    void* );	/* Insert this                           */
				                /* -> 0: OK, else: ERROR */
int    insertOrdered(struct queuePtr*, void*);  /* -> 0: OK, else: ERROR */
int    insertAndGather( struct queuePtr *,
			void *);                /* -> 0: OK, else: ERROR */
void  *extractFirst(struct queuePtr*);/* -> NULL: empty or error,        */
				      /*    else: first from queue       */
void  *extractLast(struct queuePtr*); /* -> NULL: empty or error,        */
				      /*    else: last from queue        */
int    extractElement(struct queuePtr*, void*); /* -> 0: OK, else: ERROR */
void  *getNext(struct queuePtr*);
void  *getPrev(struct queuePtr*);
void   gatherOrderedQueue(struct queuePtr*);
void   gatherQueue(struct queuePtr*);
#endif
