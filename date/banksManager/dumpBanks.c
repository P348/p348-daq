#include <stdio.h>
#include <stdlib.h>

#include "banksManager.h"
#include "rcShm.h"
#include "dateHlt.h"

struct {
  const char *roleName;
  dbRoleType  role;
} roles[] = {
  { "ldc", dbRoleLdc },
  { "gdc", dbRoleGdc },
  { "edm", dbRoleEdmHost },
  { "triggerHost", dbRoleTriggerHost },
  { "ddg", dbRoleDdg },
  { "filter", dbRoleFilter },
  { "", dbRoleUndefined }
};

void decodeFifo( void * volatile p ) {
  if ( p == readoutReady ) {
    printf( "readoutReadyFifo" );
  } else if ( p == edmReady ) {
    printf( "edmReadyFifo" );
  } else if ( p == hltReady ) {
    printf( "hltReadyFifo" );
  } else {
    printf( "UNKNOWN" );
  }
  printf( " == " );
  if ( p == edmInput ) {
    printf( "edmInputFifo" );
  } else if ( p == hltInput ) {
    printf( "hltInputFifo" );
  } else if ( p == recorderInput ) {
    printf( "recorderInputFifo" );
  } else {
    printf( "UNKNOWN" );
  }
} /* End of decodeFifo */

int main( int argc, char **argv ) {
  dbRoleType myRole = dbRoleUndefined;

  if ( argc == 2 ) {
    int i;
    
    for ( i = 0; roles[i].role != dbRoleUndefined; i++ ) {
      if ( strcmp( roles[i].roleName, argv[1] ) == 0 ) {
	myRole = roles[i].role;
	break;
      }
    }
  }
  if ( myRole == dbRoleUndefined ) {
    int i;
    
    printf( "Usage: %s (", argv[0] );
    for ( i = 0; roles[i].role != dbRoleUndefined; i++ ) {
      printf( "%s%c",
	      roles[i].roleName,
	      roles[i+1].role != dbRoleUndefined ? '|' : ')' );
    }
    printf( "\n" );
    exit( 1 );
  }
  
  if ( !DATE_MAP_BANKS( myRole ) ) {
    printf( "Failed to map to DATE banks\n" );
    exit( 1 );
  }

  printf( "rcShm:\t\t\t" );
  if ( rcShm == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d",
	    rcShm, rcShmO, rcShmSize, rcShmBank );
  }
  printf( "\n" );
  
  printf( "readoutReady:\t\t" );
  if ( readoutReady == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d\tdepth:%lld",
	    readoutReady,
	    readoutReadyO,
	    readoutReadySize,
	    readoutReadyBank,
	    readoutReadySize / (int)sizeof(struct eventLocationDescriptorStruct) );
  }
  printf( "\n" );

  printf( "readoutFirstLevel:\t" );
  if ( readoutFirstLevel == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d\tdepth:%lld,%lld,%lld...",
	    readoutFirstLevel,
	    readoutFirstLevelO,
	    readoutFirstLevelSize,
	    readoutFirstLevelBank,
	    readoutFirstLevelSize/((int)sizeof(struct eventHeaderStruct)+
				   (int)sizeof(struct vectorPayloadDescriptorStruct)+
				   (int)sizeof(struct equipmentDescriptorStruct)),
	    readoutFirstLevelSize/((int)sizeof(struct eventHeaderStruct)+
				   (int)sizeof(struct vectorPayloadDescriptorStruct)+
				   2*(int)sizeof(struct equipmentDescriptorStruct)),
	    readoutFirstLevelSize/((int)sizeof(struct eventHeaderStruct)+
				   (int)sizeof(struct vectorPayloadDescriptorStruct)+
				   3*(int)sizeof(struct equipmentDescriptorStruct)));
  }
  printf( "\n" );

  printf( "readoutSecondLevel:\t" );
  if ( readoutSecondLevel == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d",
	    readoutSecondLevel,
	    readoutSecondLevelO,
	    readoutSecondLevelSize,
	    readoutSecondLevelBank );
  }
  printf( "\n" );

  printf( "readoutData:\t\t" );
  if ( readoutData == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d",
	    readoutData,
	    readoutDataO,
	    readoutDataSize,
	    readoutDataBank );
  }
  printf( "\n" );
  
  printf( "edmReady:\t\t" );
  if ( edmReady == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d\tdepth:%lld",
	    edmReady,
	    edmReadyO,
	    edmReadySize,
	    edmReadyBank,
	    edmReadySize / (int)sizeof(struct eventLocationDescriptorStruct) );
  }
  printf( "\n" );
  
  printf( "hltReady:\t\t" );
  if ( hltReady == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d",
	    hltReady, hltReadyO, hltReadySize, hltReadyBank );
  }
  printf( "\n" );
  
  printf( "hltDataPages:\t\t" );
  if ( hltDataPages == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d depth:%lld",
	    hltDataPages,
	    hltDataPagesO,
	    hltDataPagesSize,
	    hltDataPagesBank,
	    hltDataPagesSize / (int)sizeof( struct hltDecisionStruct ) );
  }
  printf( "\n" );
  
  printf( "eventBuilderReady:\t" );
  if ( eventBuilderReady == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d",
	    eventBuilderReady,
	    eventBuilderReadyO,
	    eventBuilderReadySize,
	    eventBuilderReadyBank );
  }
  printf( "\n" );

  printf( "eventBuilderData:\t" );
  if ( eventBuilderData == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\toffset:%lld\tsize:%lld\tbank:%d",
	    eventBuilderData,
	    eventBuilderDataO,
	    eventBuilderDataSize,
	    eventBuilderDataBank );
  }
  printf( "\n" );

  printf( "physmem:\t\t" );
  if ( physmemAddr == NULL ) {
    printf( "NOT AVAILABLE" );
  } else {
    printf( "@%p\tbank:%d", physmemAddr, physmemBank );
  }
  printf( "\n" );

  if ( readoutReady != NULL || edmReady != NULL || hltReady != NULL ||
       edmInput != NULL || hltInput != NULL || recorderInput != NULL ) {
    void * volatile p = readoutReady;
    
    printf( "FIFOs:\t\t\t" );
    do {
      decodeFifo( p );
      if ( p == edmInput ) {
	p = edmReady;
	printf( "\n\t\t\t => " );
      } else if ( p == hltInput ) {
	p = hltReady;
	printf( "\n\t\t\t => " );
      } else if ( p == recorderInput ) {
	p = NULL;
      } else {
	printf( " ???" );
	p = NULL;
      }
    } while ( p != NULL );
    printf( "\n" );
  }

  if ( rcShm != NULL ) {
    printf( "edmAgent: %s (%d) ",
	    rcShm->edmEnabled ? "enabled" : "disabled",
	    rcShm->edmEnabled );
    printf( "hltAgent: %s (%d) ",
	    rcShm->hltEnabled ? "enabled" : "disabled",
	    rcShm->hltEnabled );
    printf( "\n" );
  }
  
  exit( 0 );
} /* End of main */
