#include <stdio.h>
#include <stdlib.h>

#include "banksManager.h"
#include "rcShm.h"

struct {
  const char *roleName;
  dbRoleType  role;
} roles[] = {
  { "ldc", dbRoleLdc },
  { "gdc", dbRoleGdc },
  { "edm", dbRoleEdmHost },
  { "triggerHost", dbRoleTriggerHost },
  { "ddg", dbRoleDdg },
  { "filter", dbRoleFilter },
  { "", dbRoleUndefined }
};

void print_pattern(const char*name, volatile unsigned int p[], int size)
{
  int i, j;
  unsigned int mask;

  printf("  %25s:", name);
  j = 0;
  for (i = 0; i < size; ++i) {
    for (mask = 1; mask; mask <<= 1, ++j) {
      if (p[i] & mask) {
	printf(" %d", j);
      }
    }
  }
  printf("\n");
}

void print_string(const char*name, volatile char p[], int size)
{
  static char *buffer = NULL;
  buffer = (char *)realloc(buffer, size + 1);
  memcpy(buffer, (void*)p, size);
  buffer[size] = 0;
  printf("  %25s: \"%s\"\n", name, buffer);
}

void print_event_array(const char*name, volatile eventIdType p[], int size)
{
  int i;
  char buffer[25];

  for (i = 0; i < size; ++i) {
    snprintf(buffer, 25, "%s[%02d]", name, i);
    printf("  %25s: 0x%08x.0x%08x\n", buffer, (p[i])[0], (p[i])[1]);
  }
}

#define PRINT_V32(x) \
  if(!strcmp(#x,argv[2])) {rcShm->x=strtoul(argv[3],NULL,0);}
#define PRINT_V64(x) \
  if(!strcmp(#x,argv[2])) {rcShm->x=strtoull(argv[3],NULL,0);}
#define PRINT_PAT(x)
#define PRINT_STR(x) \
  if(!strcmp(#x,argv[2])) {strncpy((char*)rcShm->x,argv[3],sizeof(rcShm->x)-1);}
#define PRINT_EVT(x)
#define PRINT_EVA(x)

int print_role(dbRoleType role, char**argv)
{
  if (!DATE_MAP_BANKS(role)) {
    return 0;
  }
  PRINT_V64(maxFileSize);
  PRINT_V64(maxBytes);
  PRINT_V64(bytesRecorded);
  PRINT_V64(bytesInjected);
  PRINT_V32(fullShmSize);
  PRINT_V32(eventHeaderId);
  PRINT_V32(rcShmId);
  PRINT_V32(controlShmSize);
  PRINT_V32(edmFifoSize);
  PRINT_V32(masterPid);
  PRINT_V32(roleFlag);
  PRINT_V32(thisMachineId);
  PRINT_V32(nbOfLdcConnected);
  PRINT_V32(nbOfGdcConnected);
  PRINT_PAT(ldcPattern);
  PRINT_PAT(gdcPattern);
  PRINT_STR(edmHost);
  PRINT_V32(runNumber);
  PRINT_V32(logLevel);
  PRINT_V32(maxEvents);
  PRINT_V32(maxEventSize);
  PRINT_V32(maxBursts);
  PRINT_V32(ldcSocketSize);
  PRINT_V32(phaseTimeoutLimit);
  PRINT_V32(recorderSleepTime);
  PRINT_V32(checkCompletionSleepTime);
  PRINT_V32(ebNearlyEmpty);
  PRINT_V32(ebNearlyFull);
  PRINT_V32(recMaskSleepTime);
  PRINT_V32(recMaskSleepCntLimit);
  PRINT_V32(colliderModeFlag);
  PRINT_V32(cdhPresentFlag);
  PRINT_V32(burstPresentFlag);
  PRINT_V32(monitorEnableFlag);
  PRINT_V32(edmEnabled);
  PRINT_V32(hltEnabled);
  PRINT_STR(recordingDevice);
  PRINT_V32(simBurstLength);
  PRINT_V32(pagedDataFlag);
  PRINT_V32(edmPollTimeOut);
  PRINT_V32(edmQuorumPercent);
  PRINT_EVT(edmInterval);
  PRINT_EVT(edmDelta);
  PRINT_V32(sodEodEnabled);
  PRINT_V32(startOfDataTimeout);
  PRINT_V32(endOfDataTimeout);
  PRINT_V32(runStatus);
  PRINT_V32(nbOfDataGenEquipments);
  PRINT_V32(triggerCount);
  PRINT_V32(eventCount);
  PRINT_V32(burstCount);
  PRINT_V32(eventsInBurstCount);
  PRINT_V32(fileCount);
  PRINT_V32(ebFullCnt);
  PRINT_V32(ebEmptyCnt);
  PRINT_V32(edmFifoFullCnt);
  PRINT_V32(recMaskSleepLoopCnt);
  PRINT_V32(recMaskSleepCnt);
  PRINT_V32(eventsRecorded);
  PRINT_V32(fifoFullCnt);
  PRINT_V32(inBurstFlag);
  PRINT_V32(recorderPid);
  PRINT_V32(eventBuilderPid);
  PRINT_V32(wakeUpRequestFlag);
  PRINT_EVT(wakeUpId);
  PRINT_EVT(maxWakeUpId);
  PRINT_EVT(lastThresholdSent);
  PRINT_EVT(lastUpperBoundSent);
  PRINT_EVT(edmMask.firstEventId);
  PRINT_EVT(edmMask.lastEventId);
  PRINT_PAT(edmMask.mask);
  PRINT_STR(ebStatus);
  PRINT_EVA(lastEventId);
  PRINT_V32(readoutFlag);
  PRINT_V32(recordingFlag);
  PRINT_V32(eventBuilderFlag);
  PRINT_V32(edmFlag);
  PRINT_V32(edmAgentFlag);
  PRINT_V32(edmClientFlag);
  PRINT_V32(hltAgentFlag);
  PRINT_V32(startOfRunFlag);
  PRINT_V32(endOfRunFlag);
  PRINT_V32(runEndCondition);
  PRINT_V32(spare1);
  PRINT_V32(spare2);
  PRINT_V32(spare3);
  PRINT_V32(spare4);
  PRINT_V32(spare5);
  PRINT_V32(spare6);
  PRINT_V32(spare7);
  PRINT_V32(spare8);
  PRINT_V32(spare9);
  PRINT_V32(spare10);
  PRINT_STR(spareString1);
  PRINT_STR(spareString2);
  PRINT_STR(spareString3);
  PRINT_STR(spareString4);
  PRINT_STR(spareString5);
  PRINT_V32(siteSpec01);
  PRINT_V32(siteSpec02);
  PRINT_V32(siteSpec03);
  PRINT_V32(siteSpec04);
  PRINT_V32(siteSpec05);
  PRINT_STR(siteSpecString1);
  PRINT_STR(siteSpecString2);
  PRINT_V32(triggerRateSD);
  PRINT_V32(eventRateSD);
  PRINT_V32(eventsRecordedRateSD);
  PRINT_STR(bytesInjectedRateSD);
  PRINT_STR(bytesRecordedRateSD);
  PRINT_STR(bytesInBufferSD);
  PRINT_STR(edmMaskValidityRangeSD);
  PRINT_STR(edmMaskSD);
  PRINT_STR(lastEventIdSD);
  PRINT_STR(unavailableGdcsSD);
  PRINT_STR(wakeUpIdSD);
  PRINT_STR(maxWakeUpIdSD);
  PRINT_STR(lastThresholdSentSD);
  PRINT_STR(lastUpperBoundSentSD);
  return 1;
}

int main(int argc, char**argv)
{
  dbRoleType myRole = dbRoleUndefined;

  if ( argc == 4 ) {
    int i;
    
    for ( i = 0; roles[i].role != dbRoleUndefined; i++ ) {
      if ( strcmp( roles[i].roleName, argv[1] ) == 0 ) {
	myRole = roles[i].role;
	break;
      }
    }
  }
  if ( myRole == dbRoleUndefined ) {
    int i;
    
    printf( "Usage: %s (", argv[0] );
    for ( i = 0; roles[i].role != dbRoleUndefined; i++ ) {
      printf( "%s%c",
	      roles[i].roleName,
	      roles[i+1].role != dbRoleUndefined ? '|' : ')' );
    }
    printf( "\n" );
    exit( 1 );
  }
  return !print_role(myRole, argv);
}
