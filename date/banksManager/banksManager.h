/*                       banksManager.h
                         ==============

   DATE banks manager to be used for loading, sizing, mapping and
   initialisation of the memory banks (and the entities included
   therein) associate to a running DATE system.

   Revision history:
     1.0   5 Dec 2001  RD       First release, based on rcShm.h 3.1
     1.1  15 Nov 2004  RD       dateUnmapBanks added

   All DATE actors running on the same DATE role will map to the same
   memory bank(s). They will all see the same entities mapped onto
   their private virtual address space. These entities will most likely
   have different base addresses. What will not change is the size of
   each entity and the offset between the entity base address and the
   base address of the bank they belong to. Every time a DATE actor
   wish to transfer some information to another DATE actor, it should
   do so via offset.

   This module defines the API to map to the banks and to initialise
   the entities therein. We also define the entities - as available
   on a running DATE system - in terms of virtual address, offsets and
   sizes.
*/

#ifndef __banks_manager_h__
#define __banks_manager_h__

#ifndef TRUE
# define TRUE (0 == 0)
#endif
#ifndef FALSE
# define FALSE (0 == 1)
#endif

#include "event.h"
#include "dateDb.h"  

/* ------------------------------------------------------------------------ */
/* Create and initialise the DATE banks system. Create and initialise the
   control bank. Do not create the other banks.

   Return value:
     TRUE   OK
     FALSE  Error
*/
int dateInitControlRegion( const dbRoleType hostRole,
			   const long32     eventId,
			   const long32     rcShmId );

#define DATE_INIT_CONTROL_REGION( hostRole ) \
  dateInitControlRegion( hostRole, EVENT_CURRENT_VERSION, RCSHM_H_ID )

/* ------------------------------------------------------------------------ */
/* Create and map all the banks defined and needed for the given role.
   
   If requested, create and initialise all entities within the
   banks. Load the base addresses and sizes of all the entities within
   the banks.

   This entry assumes that dateCreateBanks() has been already called, by
   this or by another process...
   
   Return value:
     TRUE   OK
     FALSE  Error
*/
int dateMapBanks( const dbRoleType hostRole,
		  const long32     eventId,
		  const long32     rcShmId,
		  const int        initTheBanks );

/* Access macros */
#define DATE_MAP_BANKS( hostRole ) \
  dateMapBanks( hostRole, EVENT_CURRENT_VERSION, RCSHM_H_ID, FALSE )
#define DATE_MAP_AND_INIT_BANKS( hostRole ) \
  dateMapBanks( hostRole, EVENT_CURRENT_VERSION, RCSHM_H_ID, TRUE )

/* Old (deprecated) macros */
#ifndef MAPCONTROLANDBUFFERREGION
#define MAPCONTROLANDBUFFERREGION( hostRole ) \
  dateMapBanks( hostRole, EVENT_CURRENT_VERSION, RCSHM_H_ID, FALSE )
#define MAPCONTROLREGION( hostRole ) MAPCONTROLANDBUFFERREGION( hostRole )
#endif

/* ------------------------------------------------------------------------ */
/* Unmap all banks                                                          */
int dateUnmapBanks( void );

/* ------------------------------------------------------------------------ */
/* Refresh the setup of memory banks                                        */
int dateRefreshBanks( void );

/* ------------------------------------------------------------------------ */
/* Macros used to manipulate virtual addresses, banks and offsets           */
#define BO2V(bankId,offset)  (void * volatile)(dateBanks[bankId]+offset)
#define BV2O(bankId,address) (int)(((long)(address))-((long)(dateBanks[bankId])))
#define V2O(address)         bankVirtualToOffset(address)
#define V2B(address)         bankVirtualToBank(address)

/* ------------------------------------------------------------------------ */
/* The bank(s) base addresses */
#ifndef DATE_IN_BANKS_MODULE
extern int     numDateBanks;
extern void ** dateBanks;
#endif

/* ------------------------------------------------------------------------ */
/* The structures to be created and initialised by the DATE banks manager   */
#ifndef DATE_IN_BANKS_MODULE
extern struct daqControlStruct * volatile rcShm;
extern long64 rcShmO;
extern long64 rcShmSize;
extern int rcShmBank;

extern void * volatile readoutReady;
extern long64 readoutReadyO;
extern long64 readoutReadySize;
extern int readoutReadyBank;

extern void * volatile readoutFirstLevel;
extern long64 readoutFirstLevelO;
extern long64 readoutFirstLevelSize;
extern int readoutFirstLevelBank;

extern void * volatile readoutSecondLevel;
extern long64 readoutSecondLevelO;
extern long64 readoutSecondLevelSize;
extern int readoutSecondLevelBank;

extern void * volatile readoutData;
extern long64 readoutDataO;
extern long64 readoutDataSize;
extern int readoutDataBank;

extern void * volatile edmReady;
extern long64 edmReadyO;
extern long64 edmReadySize;
extern int    edmReadyBank;
extern void * volatile edmInput;

extern void * volatile hltReady;
extern long64 hltReadyO;
extern long64 hltReadySize;
extern int    hltReadyBank;
extern void * volatile hltInput;

extern void * volatile hltDataPages;
extern long64 hltDataPagesO;
extern long64 hltDataPagesSize;
extern int    hltDataPagesBank;

extern void * volatile recorderInput;

extern void * volatile eventBuilderReady;
extern long64 eventBuilderReadyO;
extern long64 eventBuilderReadySize;
extern int    eventBuilderReadyBank;

extern void * volatile eventBuilderData;
extern long64 eventBuilderDataO;
extern long64 eventBuilderDataSize;
extern int    eventBuilderDataBank;

extern void *physmemAddr;
extern int   physmemBank;
#endif

/* ------------------------------------------------------------------------ */
/* Macros used to reference some of the commonly used entities              */
#define DAQCONTROL	rcShm
#define RECORDINGBUFFER readoutReady

#endif
