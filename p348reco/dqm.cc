// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include <TH1F.h>
#include <TH2F.h>
#include <TProfile.h>
#include <TFile.h>
#include "TStyle.h"

// c++
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

// P348 reco
#include "p348reco.h"

int main(int argc, char* argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./dqm.exe file1.dat [file2.dat ...]" << endl;
    return 1;
  }
  
  // setup input data files
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  // output file for histograms
  TFile* rootFile = new TFile("dqm.root", "RECREATE");
  
  // declaration of histogram for each channel
  // X axis: channel name / Y axis: amplitude
  TH2I* histoSADC = new TH2I("histoSADC", "amplitude;channel;amplitude, ADC counts", 1, 0, 1, 4096, 0, 4096);
  
  // option "s" for standard deviation (RMS) as a bin error
  TProfile* prof = new TProfile("prof", ";channel;value", 1, 0, 1, "s");
  
  // read all events
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    RecoEvent e = RunP348Reco(manager);
    
    TString etype;
    if (e.isPhysics) etype = "PHYSICS:";
    else if (e.isCalibration) etype = "CALIBRATION:";
    else etype = "UNKNOWN:";
    
    const vector<const Cell*> list = listCells(e);
    
    for (size_t i = 0; i < list.size(); ++i) {
      const Cell& c = *list[i];
      if (!c.hasDigit) continue;
      histoSADC->Fill(etype + c.calib->name + ".amplitude", c.amplitude, 1.);
    }
    
    const int nMM = 12;
    const Micromega* listMM[nMM] = {&e.MM1, &e.MM2, &e.MM3, &e.MM4, &e.MM5, &e.MM6, &e.MM7, &e.MM8, &e.MM9, &e.MM10, &e.MM11, &e.MM12};
    
    for (int i = 0; i < nMM; ++i) {
      const Micromega& mm = *listMM[i];
      const TString prefix = etype + mm.calib->name;
      
      // plane number of clusters
      prof->Fill(prefix + "X.Nclusters", mm.xplane.clusters.size(), 1.);
      prof->Fill(prefix + "Y.Nclusters", mm.yplane.clusters.size(), 1.);
      
      // plane best cluster size
      if (mm.xplane.hasHit()) {
        const int idx = mm.xplane.bestClusterIndex();
        prof->Fill(prefix + "X.bestClusterSize", mm.xplane.clusters[idx].size(), 1.);
      }
      
      if (mm.yplane.hasHit()) {
        const int idx = mm.yplane.bestClusterIndex();
        prof->Fill(prefix + "Y.bestClusterSize", mm.yplane.clusters[idx].size(), 1.);
      }
      
      // station efficiency
      prof->Fill(prefix + ".efficiency", mm.hasHit(), 1.);
      
      // station beam spot
      if (mm.hasHit()) {
        prof->Fill(prefix + ".Xpos", mm.xpos, 1.);
        prof->Fill(prefix + ".Ypos", mm.ypos, 1.);
      }
    }
    
    
    const int nGEM = 4;
    const gem::GEM* listGM[nGEM] = {e.GM01, e.GM02, e.GM03, e.GM04};
    
    for (int i = 0; i < nGEM; ++i) {
      const gem::GEM& gm = *listGM[i];
      const TString prefix = etype + gm.getname();
      
      // plane number of clusters
      prof->Fill(prefix + "X.Nclusters", gm.xplane.size(), 1.);
      prof->Fill(prefix + "Y.Nclusters", gm.yplane.size(), 1.);
      
      // plane best cluster size
      if (gm.bestHit.x) {
        prof->Fill(prefix + "X.bestClusterSize", gm.bestHit.x.orig->GetSize(), 1.);
      }
      
      if (gm.bestHit.y) {
        prof->Fill(prefix + "Y.bestClusterSize", gm.bestHit.y.orig->GetSize(), 1.);
      }
      
      // station efficiency
      prof->Fill(prefix + ".efficiency", gm.bestHit, 1.);
      
      // station beam spot
      if (gm.bestHit) {
        const TVector3 pos = gm.bestHit.rel_pos();
        prof->Fill(prefix + ".Xpos", pos.X(), 1.);
        prof->Fill(prefix + ".Ypos", pos.Y(), 1.);
      }
    }
  }
  
  histoSADC->LabelsDeflate("X");
  histoSADC->LabelsOption("a v", "X");
  prof->LabelsDeflate("X");
  prof->LabelsOption("a v", "X");
  
  // write channel mean energy, RMS and number of entries to .txt file
  ofstream txtFile("dqm.txt");
  
  // work-around for even empty histogram has at least one bin
  const int nhisto = (histoSADC->GetEntries() > 0) ? histoSADC->GetNbinsX() : 0;
  
  for (int k = 1; k <= nhisto; ++k) {
    const TString name = histoSADC->GetXaxis()->GetBinLabel(k);
    //cout << name << endl;
    
    TH1D* h = histoSADC->ProjectionY(name, k, k, "");
    
    txtFile << h->GetName() << " "
            << h->GetMean() << " "
            << h->GetStdDev() << " "
            << h->GetEntries()
            << endl;
  }
  
  const int nprof = (prof->GetEntries() > 0) ? prof->GetNbinsX() : 0;
  
  for (int k = 1; k <= nprof; ++k) {
    const TString name = prof->GetXaxis()->GetBinLabel(k);
    //cout << name << endl;
    
    txtFile << name << " "
            << prof->GetBinContent(k) << " "
            << prof->GetBinError(k) << " "
            << prof->GetBinEntries(k)
            << endl;
  }
  
  // save histograms to .root file
  histoSADC->Write();
  prof->Write();
  rootFile->Close();
  
  return 0;
}
