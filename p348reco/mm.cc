#include "mm.h"

#include <cmath>

namespace {
// Returns linear interpolation between a and b for parameter t
double lerp(double a, double b, double t) {
  return (a * (1.0 - t)) + (b * t);
}
}

void MMStrip::SetToZero() {
  charge  = 0;
  t02     = nan("");
  t12     = nan("");
  sigma02 = nan("");
  sigma12 = nan("");
}

// hit time computed with t02 and t12
double MMStrip::time() {
  double st_t = nan("");
  double t_num = 0;
  double t_den = 0;

  if (MM_time_ratio_sigma) {
    t_num = t02/pow(sigma02,2) + t12/pow(sigma12,2);
    t_den = 1./pow(sigma02,2) + 1./pow(sigma12,2);
  } else {
    t_num = t02 + t12;
    t_den = 2.;
  }

  if (t_den != 0) st_t = t_num/t_den;
  return st_t;
}


// Function to obtain a new strip as an average of two other strips
// --> Used in FixMMBadStrips
MMStrip avgStrip(const CS::uint16 bin, const MMStrip &s1, const MMStrip &s2) {
  MMStrip avg;
  avg.bin = bin;
  if (s1.charge == 0 && s2.charge == 0) return avg;

  // CHARGE: Interpolate charge in strip
  double t = (s1.bin != s2.bin)? ((double)(bin - s1.bin))/(s2.bin - s1.bin) : 0.;
  avg.charge = lerp(s1.charge, s2.charge, t);
  // T02 & SIGMA02: Weighted average
  if (s1.sigma02 != 0 && s2.sigma02 != 0) {
    double t_den  = 1./pow(s1.sigma02,2) + 1./pow(s2.sigma02,2);
    avg.t02       = (s1.t02/pow(s1.sigma02,2) + s2.t02/pow(s2.sigma02,2))/(t_den);
    avg.sigma02   = 1./sqrt(t_den);
  }
  // T12 & SIGMA12: Weighted average
  if (s1.sigma12 != 0 && s2.sigma12 != 0) {
    double t_den  = 1./pow(s1.sigma12,2) + 1./pow(s2.sigma12,2);
    avg.t12       = (s1.t12/pow(s1.sigma12,2) + s2.t12/pow(s2.sigma12,2))/(t_den);
    avg.sigma12   = 1./sqrt(t_den);
  }

  return avg;
}


// A cluster is a collection of fired strips

  void MMCluster::add_strip(const MMStrip& new_strip)
  {
    // Fill in contents of cluster
    strips.push_back(new_strip);
    charge_total += new_strip.charge;
  }

  void MMCluster::add_strip(const CS::uint16 bin_, const array<CS::uint32, 3> raw_, const CS::uint32 charge_, const double t02_, const double t12_, const double sigma02_, const double sigma12_)
  {
    // Fill in contents of cluster
    strips.emplace_back(bin_, raw_, charge_, t02_, t12_, sigma02_, sigma12_);
    charge_total += charge_;
  }

  void MMCluster::edit_strip_charge(const CS::uint16 bin_, const CS::uint32 charge_)
  {
    // Don't modify anything if strip is not found in MMCluster
    auto it = find(strips.begin(), strips.end(), MMStrip(bin_));
    if (it == strips.end()) return;
    size_t idx = distance(strips.begin(), it);
    // Subtract previous
    charge_total -= strips[idx].charge;
    // Replace contents of existing strip
    strips[idx].charge = charge_;
    charge_total += charge_;
  }

  bool MMCluster::has_strip(const CS::uint16 bin_) const
  {
    // Find MMStrip with same strip number
    auto it = find(strips.begin(), strips.end(), MMStrip(bin_));
    return (it != strips.end());
  }

  // return cluster center-of-mass
  double MMCluster::position() const
  {
    double num = 0;
    double den = 0;

    for (const auto& strip : strips) {
      num += strip.bin * strip.charge;
      den += strip.charge;
    }

    return (den != 0) ? num/den : 0;
  }

  double MMCluster::cluster_time() const
  {
    double cl_t =0;
    double t_num=0;
    double t_den=0;

    if(MM_time_ratio_sigma)
    {
      for(const auto& strip : strips) {
        if(!std::isnan(strip.t02) && !std::isnan(strip.t12))
        {
          t_num+=(strip.t02/pow(strip.sigma02,2))+(strip.t12/pow(strip.sigma12,2));
          t_den+=(1/pow(strip.sigma02,2))+(1/pow(strip.sigma12,2));
        }
      }

    } else {
      for(const auto& strip : strips) {
        if(!std::isnan(strip.t02) && !std::isnan(strip.t12))
        {
          t_num += ((strip.t02+strip.t12)/2) * strip.charge;
          t_den += strip.charge;
        }
      }
    }

    cl_t = (t_den > 0)? t_num/t_den : nan("");
    return (cl_t);
  }

  double MMCluster::cluster_time_error() const
  {
    double t_error =0;
    double t_den=0;

    for(const auto& strip : strips) {
      if(strip.sigma02!=0 && strip.sigma12!=0) {
        t_den += ((1/pow(strip.sigma02,2))+(1/pow(strip.sigma12,2)));
      }
    }

    t_error = (t_den > 0)? 1/sqrt(t_den) : nan("");
    return t_error;
  }

//-----------------------------------------------------------------------------
//------------ SORT FUNCTIONS for vector<MMCluster>, best is index=0 ------------
namespace {
bool cmpByTotalCharge(const MMCluster& a, const MMCluster& b)
{
  // highest charge is the best
  return (a.charge_total > b.charge_total);
}

bool cmpBySize(const MMCluster& a, const MMCluster& b)
{
  // largest size is the best
  return (a.size() > b.size());
}
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

namespace {
bool isClusterDeleted(const MMCluster& a)
{
  return (a.size() == 0);
}
}

//Hit struct

  void Hit::CalculatePos()
  {
    double strip_x = xcluster->position();
    double strip_y = ycluster->position();

    // translate into center of MM surface
    const double xcenter = calib->xcalib.MM_Nstrips/2.0 - 0.5;
    const double ycenter = calib->ycalib.MM_Nstrips/2.0 - 0.5;
    strip_x -= xcenter;
    strip_y -= ycenter;

    // scale to real size
    strip_x *= calib->xcalib.MM_strip_step;
    strip_y *= calib->ycalib.MM_strip_step;

    // rotate by 45 degree
    const double angle = geom->Angle;
    const double Kx = cos(angle);
    const double Ky = sin(angle);
    xpos = +Kx*strip_x - Ky*strip_y;
    ypos = +Ky*strip_x + Kx*strip_y;
    if(geom->invertAxis)
      xpos = -xpos;
  }

  // Return sum of squared difference to reference time for both planes
  double Hit::timediff() const
  {
    double result = nan("");
    if (calib->hasTime()) {
      result = pow((xcluster->cluster_time() - calib->xcalib.time), 2) + pow((ycluster->cluster_time() - calib->ycalib.time), 2);
      result = sqrt(result);
    }
    return result;
  }

//-------------------------------------------------------------------------
//------------ SORT FUNCTIONS for vector<Hit>, best is index=0 ------------
namespace {
bool hitByTotalCharge(const Hit& a, const Hit& b)
{
  const double charge_a = a.xcluster->charge_total + a.ycluster->charge_total;
  const double charge_b = b.xcluster->charge_total + b.ycluster->charge_total;
  // highest charge is the best
  return (charge_a > charge_b);
}

bool hitBySize(const Hit& a, const Hit& b)
{
  const double size_a = a.xcluster->size() + a.ycluster->size();
  const double size_b = b.xcluster->size() + b.ycluster->size();
  // largest size is the best
  return (size_a > size_b);
}

bool hitByTime(const Hit& a, const Hit& b)
{
  // minimum interval to t0 is the best
  return (a.timediff() < b.timediff());
}
}
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

  // custom copy for proper cluster reference
  // TODO: the only use of the Hit::xcluster,::ycluster is the hits sort (disabled by default) at reco step Micromega::DoMMReco()
  //       revise/remove Hit::xcluster,::ycluster pointers and so the custom copy constructor?
  Micromega::Micromega(const Micromega & orig): xplane(orig.xplane), yplane(orig.yplane),
  hits(orig.hits), xpos(orig.xpos), ypos(orig.ypos), calib(orig.calib),
  geom(orig.geom) {
    // fix broken cluster references in hits
    for( vector<Hit>::iterator it = hits.begin(); it != hits.end(); ++it ) {
      for( size_t i = 0; i < xplane.clusters.size(); ++i ) {
        if( &(orig.xplane.clusters[i]) != it->xcluster ) continue;
        it->xcluster = &(xplane.clusters[i]);
        break;
      }
      for( size_t i = 0; i < yplane.clusters.size(); ++i ) {
        if( &(orig.yplane.clusters[i]) != it->ycluster ) continue;
        it->ycluster = &(yplane.clusters[i]);
        break;
      }
    }
  }

  void Micromega::Init(const MM_calib& _calib, const DetGeo& _geom)
  {
    calib = &_calib;
    geom  = &_geom;

    //initialize single planes
    xplane.Init(calib->xcalib);
    yplane.Init(calib->ycalib);
  }

void DoHitAccumulation(const CS::uint16 chan, const CS::uint32* raw_q, MicromegaPlane& plane)
{
  const MM_plane_calib& calib = *plane.calib;

  CS::uint32 max_q = 0;

  // Find max ADC value
  for (int i = 0; i < MM_apv_time_samples; i++) {
    const CS::uint32 q = raw_q[i];
    if (max_q < q) max_q = q;
  }

  const double A0   = raw_q[0];
  const double A1   = raw_q[1];
  const double A2   = raw_q[2];
  const array<CS::uint32,3> raw_Q = {raw_q[0], raw_q[1], raw_q[2]};

  // Calculate timings with ADC ratios using time calibrations
  double t02        = calib.timing.a02.time(A0,A2);
  double t12        = calib.timing.a12.time(A1,A2);
  double sigma_t02  = calib.timing.a02.error(A0,A2,calib.sigma[chan]);
  double sigma_t12  = calib.timing.a12.error(A1,A2,calib.sigma[chan]);

  // create an entry for each strip connected to electronics channel 'chan'
  for(CS::uint16 bin : calib.MM_map.at(chan)) {
    MMStrip hit_strip(bin, raw_Q, max_q, t02, t12, sigma_t02, sigma_t12);
    plane.strips[bin] = hit_strip;
  }
}

void MicromegaPlane::DoMMClusClear()
{
  // loop over each pair of clusters
  for (auto& c1 : clusters) {
    if (c1.size() == 0) continue; // skip deleted cluster

    for (auto& c2 : clusters) {
      if (c2.size() == 0) continue;

      // check if clusters c1 and c2 share common channel
      bool haveIntersection = false;

      if(c1.charge_total<c2.charge_total/2) c1.clear();
      else if(c2.charge_total<c1.charge_total/2) c2.clear();

      // Check for intersection of two clusters (sharing readout channels)
      for (const auto& s1 : c1.strips) {
        for (const auto& s2 : c2.strips) {
          if (calib->MM_reverse_map.at(s1.bin) == calib->MM_reverse_map.at(s2.bin)) {
            haveIntersection = true;
            goto endcheck;
          }
        }
      }
      endcheck: ;

      if (haveIntersection) {
        // mark weakest cluster for deletion (cluster.clear())
        if (c1.charge_total > c2.charge_total)
          c2.clear();
        else if (c1.charge_total < c2.charge_total)
          c1.clear();
      }
    }
  }

  // drop deleted clusters:
  //   remove_if() moves deleted clusters to the tail of array
  //   erase() truncates tail with deleted clusters
  clusters.erase(remove_if(clusters.begin(), clusters.end(), isClusterDeleted), clusters.end());
}

void MicromegaPlane::DoMMClusSort()
{
  if (MM_cluster_sort == kSort_BySize) {
    // Sort by size of plane cluster
    sort(clusters.begin(),clusters.end(), cmpBySize);
    return;
  }
  else if (MM_cluster_sort == kSort_ByCharge) {
    // Sort by total charge of plane cluster
    sort(clusters.begin(),clusters.end(), cmpByTotalCharge);
    return;
  }
  else if (MM_cluster_sort == kSort_ByTiming) {
    // Sort by time if possible
    if (calib->hasTime()) {
      double ctime = calib->time;
      sort(clusters.begin(),clusters.end(), [ctime](const MMCluster &a , const MMCluster &b) {return (abs(a.cluster_time() - ctime) < (abs(b.cluster_time() - ctime)));});
      return;
    // Fallback case
    } else {
      // Sort by total charge of plane cluster
      sort(clusters.begin(),clusters.end(), cmpByTotalCharge);
      return;
    }
  }
  else if (MM_cluster_sort == kSort_No) {
    // Do not sort clusters
    return;
  }

  // Throw exception if method does not exist
  throw std::runtime_error("ERROR: MM_cluster_sort method does not exist!");

}

void MicromegaPlane::DoMMClustering()
{
  FixMMBadStrips();

  const unsigned int MM_size = calib->MM_Nstrips;

  clusters.clear();

  // scan through all strips
  unsigned int bin = 0;

  while (true) {
    // skip 'empty' strips
    while (bin < MM_size && strips[bin].charge == 0) bin++;

    // end check
    if (bin >= MM_size) break;

    // accumulate cluster strips
    MMCluster c;

    while (bin < MM_size && strips[bin].charge != 0) {
      c.add_strip(strips[bin]);
      bin++;
    }

    // skip small clusters
    if (c.size() < calib->MM_min_cluster_size) continue;

    // record cluster
    clusters.push_back(c);

    // end check
    if (bin >= MM_size) break;
  }

  FixMMClusMultiplex();

  if (MM_clear_clusters) DoMMClusClear();

  DoMMClusSort();
  /*
     cout << "Nclusters = " << clusters.size() << endl;
     for (size_t i = 0; i < clusters.size(); ++i)
     cout << i << " : size = " << clusters[i].size()
     << ", total charge = " << clusters[i].charge_total
     << ", position = " << clusters[i].position()
     << endl;

     cout << "Max cluster index = " << bestClusterIndex()
     << ", position = " << bestHitStrip()
     << endl;
     */
}

void MicromegaPlane::FixMMClusMultiplex()
{
  for (auto& clus : clusters) {
    map<int, int>  cluster_wires;
    for (const auto& strip : clus.strips) {
      // Check if wire has already been added to cluster
      auto ret = cluster_wires.insert(pair<int, int> (calib->MM_reverse_map.at(strip.bin), 0));
      ret.first->second++;
    }
    // Check for strips connected to the same wire in cluster
    for (auto iwire = cluster_wires.begin(); iwire != cluster_wires.end(); ++iwire) {
      // More than one strip connected to the same wire in the cluster
      if(iwire->second > 1) {
        // Set strips to 0 to calculate the center of mass
        for(const auto& istrip : calib->MM_map.at(iwire->first)) {
          if(clus.has_strip(istrip)) {
            clus.edit_strip_charge(istrip, 0);
          }
        }
      }
    }
    for (auto iwire = cluster_wires.begin(); iwire != cluster_wires.end(); ++iwire) {
      // More than one strip connected to the same wire in the cluster
      if(iwire->second > 1) {
        double ds[iwire->second];
        double den = 0.;
        size_t idx = 0;
        // Calculate weights based on distance to center of mass
        for(const auto& istrip : calib->MM_map.at(iwire->first)) {
          if(clus.has_strip(istrip)) {
            ds[idx] = fabs(istrip - clus.position());
            den += fabs(istrip - clus.position());
            idx++;
          }
        }
        idx = 0;
        // Add strips again but with corresponding weight
        for(const auto& istrip : calib->MM_map.at(iwire->first)) {
          if(clus.has_strip(istrip)) {
            const double f = (den > 0) ? (1-ds[idx]/den) : 1;
            clus.edit_strip_charge(istrip, f*strips[istrip].charge);
            idx++;
          }
        }
      } // if more than one occurence
    } // last wire loop
  } // cluster loop
}


void MicromegaPlane::FixMMBadStrips()
{
  const unsigned int MM_size = calib->MM_Nstrips;

  // Replace bad strips by the average of neighboring strips
  for(const auto& badBin : calib->badStrips) {
    // Check if neighbours are still inside MM_size
    const bool isInside = (badBin > 0) && (badBin+1 < MM_size);
    // "Zero" strip if it is on the border
    if (!isInside) {
      strips[badBin].SetToZero();
      continue;
    }

    // Check if neighboring strips are not bad as well
    if (calib->badStrips.count(badBin-1) > 0) continue;
    if (calib->badStrips.count(badBin+1) > 0) continue;

    // Correct bad strip as average of neighbouring strips
    strips[badBin] = avgStrip(badBin, strips[badBin-1], strips[badBin+1]);
  }
}

void Micromega::MMCalculatePos()
{
  //best hit is saved as first entry
  Hit besthit = hits[0];
  xpos = besthit.xpos;
  ypos = besthit.ypos;
}

void Micromega::DoMMHitSort()
{
  // Skip if no hits
  if (hits.empty()) return;

  //TODO: sort hit on some variable that exploit information on both X and Y
  //TODO: make it independent from single plane configuration
  if (MM_hit_sort == kSort_BySize) {
    // Sort by size of X and Y clusters
    sort(hits.begin(),hits.end(), hitBySize);
    return;
  }
  else if (MM_hit_sort == kSort_ByCharge) {
    // Sort by total charge of X and Y clusters
    sort(hits.begin(),hits.end(), hitByTotalCharge);
    return;
  }
  else if (MM_hit_sort == kSort_ByTiming) {
    // Sort by time if possible
    if (calib->hasTime()) {
      sort(hits.begin(),hits.end(), hitByTime);
      return;
    // Fallback case
    } else {
      // Sort by total charge of X and Y clusters
      sort(hits.begin(),hits.end(), hitByTotalCharge);
      return;
    }
  }
  else if (MM_hit_sort == kSort_No) {
    // Do not sort hits
    return;
  }

  // Throw exception if method does not exist
  throw std::runtime_error("ERROR: MM_hit_sort method does not exist!");

}

void Micromega::DoMMReco()
{
  xplane.DoMMClustering();
  yplane.DoMMClustering();
  if (hasHit())
  {
    vector<MMCluster>& xclusters = xplane.clusters;
    vector<MMCluster>& yclusters = yplane.clusters;
    //calculate possible track candidate (put more trust on xplane)
    for(unsigned int i(0);i < xclusters.size();++i)
      for(unsigned int j(0);j < yclusters.size();++j)
      {
        Hit newhit;
        newhit.xcluster = &xclusters[i];
        newhit.ycluster = &yclusters[j];
        newhit.geom  = geom;
        newhit.calib = calib;
        newhit.CalculatePos();
        hits.push_back(newhit);
      }

    // Sort MM hits with given method MM_hit_sort
    DoMMHitSort();

    MMCalculatePos();
  }
}
