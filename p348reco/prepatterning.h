#pragma once

/*! \file prepatterning.h 
    \author Henri Hugo Sieber <henri.hugo.sieber@cern.ch>

    \brief Interface to Renat Dusaev's Cellular Automaton based track
    finding implementation
*/

// STD Library
#include <array>
#include <map>
#include <vector>
#include <set>
#include <iomanip>

// CATSC
#include <catsc/cats.h>
#include <catsc/cats.hh>

// ROOT
#include <TVector3.h>

// Tracking Tools
#include <FittedLine3D.hh>
#include <PCALinearReg.hh>
#include <ConfigFileParser.hh>

// P348reco
#include <p348reco.h>

//helper to make available make_unique for c++11
//source: https://herbsutter.com/gotw/_102/
namespace std11patch{
  template<typename T, typename ...Args>
  std::unique_ptr<T> make_unique( Args&& ...args )
  {
      return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
  }
}

// helper to apply angular corrections to hits with respect to the aligned fixpoint (undeflected beamspot)
// also used in tracking_new.h
void applyAngularCorrection(TVector3& correctedLocalPos, ConfigFileParser& parser, const string& trackerName, bool inv = false){
  // second entry in json entry is sigma of beam distribution
  const double beamx = parser.GetConfigValueWithATVector2( trackerName+"x_beam_loc" ).X();
  const double beamy = parser.GetConfigValueWithATVector2( trackerName+"y_beam_loc" ).X();

  TVector3 p_beam(beamx, beamy, 0.); 
  // NOTE: the alignment has been optimised for the beamcore, as such the angular corrections ideally are applied with the beamcore as fixpoint
  // Mathematically: p_corr = R*(p_loc-p_beam)+p_beam, where p_beam refers to the beamcore
  
  // angle from cross-correlations
  // derived from slope of v/du plot (x-pos vs y-residual plot)
  // linear fit: y_res=p0+p1*x_pos
  // slope <=> p1
  // angle <=> arctan(p1)
  double dphi = parser.GetConfigValueWithADouble( trackerName+"_angCorr" ); 
  if (inv) dphi*=-1; // for MC reco, where we need to first introduce the rotation, so it can then be "undone" upon reconstruction

  correctedLocalPos-=p_beam;       // p_loc-p_beam
  correctedLocalPos.RotateZ(dphi); // R*(p_loc-p_beam)
  correctedLocalPos+=p_beam;       // R*(p_loc-p_beam)+p_beam
}

/*! \brief Globals variables. 
    \note The placements and corrections should be accordingly filled
    by the user.
*/
namespace PPTRN{
  /*! \brief The placements file. */
  ConfigFileParser PLACEMENTS;
  /*! \brief The corrections file. */
  //ConfigFileParser CORR;
} // end namespace PPTRN

/*! \struct GenericRecoHit
    \brief Abstract generic structure for reconstructed hits to interface
    hits to CATS.
*/
struct GenericRecoHit{

  /*! \brief The constructor. 
      \param _name    The name of the detector associated with this hit.
      \param _layerID The unique layer identity associated with this hit.
  */
  GenericRecoHit(const std::string& _name, const unsigned int _layerID )
    : name(_name), layerID(_layerID), resolution(PPTRN::PLACEMENTS.GetConfigValueFromListWithADouble(_name,"resolution")) {;}

  /*! \brief The destructor. */
  virtual ~GenericRecoHit() {;}

  /*! \brief Get the global position of the hit. */
  virtual TVector3 GetGlobalPos() const = 0;

  /*! \brief The name of the detector's hit (MM, GEM, ST). */
  std::string name;
  /*! \brief The layer identity associated with this hit. */
  unsigned int layerID;
  /*! \brief The hit resolution (assumed equal in both direction) .*/
  double resolution;
};

/*! \struct MCRecoHit
    \brief Structure to interface MC hits to CATS
*/
struct MCRecoHit : public GenericRecoHit {
  
  /*! \brief The constructor.
      \param _hit     The MChit instance.
      \param _name    The name of the detector associated to the MChit.
      \param _layerID The unique layer identity associated with this hit.
  */
  MCRecoHit( const MChit& _hit
           , const std::string& _name
           , const unsigned int _layerID)
    : GenericRecoHit( _name, _layerID ), hit(_hit) {}

  /*! \brief The destructor. */
  ~MCRecoHit() override {;}

  /*! \brief Get the global hit position associated with the MChit. 
      \note The MChit are given in global coordinate system.
  */
  TVector3 GetGlobalPos() const override { 
    TVector3 globPos(hit.pos.X(), hit.pos.Y(), PPTRN::PLACEMENTS.GetConfigValueFromListWithATVector3( name, "origin" ).Z());
    return globPos; 
  }

  /*! \brief The MChit detector hit. */
  MChit hit;
};

/*! \struct MMRecoHit
    \brief Structure to interface Micromegas hits to CATS
*/
struct MMRecoHit : public GenericRecoHit {
  

  /*! \brief The constructor. 
      \param _hit     The Micromega hit instance.
      \param _name    The name of the detector associated to the Micromegas.
      \param _layerID The unique layer identity associated with this hit.

      \note The parameter \c _name must correspond to the key of the JSON file
      associated with the ConfigFileParser instance.
  */
  MMRecoHit( const Hit& _hit
           , const std::string& _name
           , const unsigned int _layerID )
    : GenericRecoHit( _name, _layerID ), hit(_hit) {}

  /*! \brief The destructor. */
  ~MMRecoHit() override {;}

  /*! \brief Get the global hit position associated with the Micromegas hit. */
  TVector3 GetGlobalPos() const override {
    
    const TVector3 globalPos =
      PPTRN::PLACEMENTS.GetConfigValueFromListWithATVector3( name, "origin" );
    
    const TVector3 localPos = hit.pos();
    
    TVector3 corrLocPos(localPos);
    applyAngularCorrection(corrLocPos, PPTRN::PLACEMENTS, name);

    return corrLocPos+globalPos;
  }

  /*! \brief The Micromegas detector hit.*/
  Hit hit;
};

/*! \struct GEMRecoHit
    \brief Structure to interface GEMs hits to CATS
*/
struct GEMRecoHit : public GenericRecoHit {
  

  /*! \brief The constructor. 
      \param _gem     The GEM instance.
      \param _name    The name of the detector associated to the GEM.
      \param _layerID The unique layer identity associated with this hit.

      \note The parameter \c _name must correspond to the key of the JSON file
      associated with the ConfigFileParser instance.
  */
  GEMRecoHit( const gem::GEM* _gem
            , const std::string& _name
            , const unsigned int _layerID )
    : GenericRecoHit( _name, _layerID ), gem(_gem) {}

  /*! \brief The destructor. */
  ~GEMRecoHit() override {;}

  /*! \brief Get the global hit position associated with the GEM hit. */
  TVector3 GetGlobalPos() const override {
    const TVector3 globalPos =
      PPTRN::PLACEMENTS.GetConfigValueFromListWithATVector3( name, "origin" );
    const TVector3 localPos = gem->bestHit.rel_pos();
    
    TVector3 corrLocPos(localPos);
    applyAngularCorrection(corrLocPos, PPTRN::PLACEMENTS, name);

    return corrLocPos+globalPos;
  }

  /*! \brief The GEM detector instance.*/
  const gem::GEM* gem;
};

/*! \struct STRecoHit
    \brief Structure to interface Micromegas hits to CATS
*/
struct STRecoHit : public GenericRecoHit {
  

  /*! \brief The constructor. 
      \param _hit     The straw hit instance.
      \param _name    The name of the detector associated to the straw.
      \param _layerID The unique layer identity associated with this hit.

      \note The parameter \c _name must correspond to the key of the JSON file
      associated with the ConfigFileParser instance.
  */
  STRecoHit( const StHit& _hit
           , const std::string& _name
           , const unsigned int _layerID )
    : GenericRecoHit( _name, _layerID ), hit(_hit) {}

  /*! \brief The destructor. */
  ~STRecoHit() override {;}

  /*! \brief Get the global hit position associated with the straw hit. */
  TVector3 GetGlobalPos() const override {
    const TVector3 globalPos =
      PPTRN::PLACEMENTS.GetConfigValueFromListWithATVector3( name, "origin" );
    const TVector3 localPos = hit.pos();
    TVector3 corrLocPos(localPos);
    applyAngularCorrection(corrLocPos, PPTRN::PLACEMENTS, name);

    return corrLocPos+globalPos;
  }

  /*! \brief The straw detector hit.*/
  StHit hit;
};

/*! \brief typedef for user-defined hits type in CATS */
typedef catsc::TrackFinder<GenericRecoHit*> CATSTrackFinder;

/*! \struct ByAngleFilter
    \brief Angular-based filter structure for pre-patterning
*/
struct ByAngleFilter : public CATSTrackFinder::iTripletFilter {
  /*! \brief The angular threshold. */
  double threshold;

  /*! \brief The default constructor. */
  ByAngleFilter() : iTripletFilter(), threshold(0) {;}
  /*! \brief The constructor
      \param _threshold The threshold
  */
  ByAngleFilter(double _threshold) : iTripletFilter(), threshold(_threshold) {}

  /*! \brief Set the threshold. 
      \param _threshold The threshold
  */
  void SetThreshold(double _threshold) {threshold = _threshold;}
  /*! \brief Check whether the hit triplet can form a tracklet.
      \param a The first hit candidate
      \param b The second hit candidate
      \param c The third hit candidate
  */
  bool matches( const GenericRecoHit& a
              , const GenericRecoHit& b
              , const GenericRecoHit& c ) const override {
    TVector3 p[3] = { a.GetGlobalPos(), b.GetGlobalPos(), c.GetGlobalPos() };
    p[0] -= p[1];
    p[2] -= p[1];
    const double angle = p[0].Angle(p[2]);
    return (angle > threshold);
  }

};

/*! \struct DoubletFilter
    \brief trivial filter structure for pre-patterning of 2-hit tracklets
*/
struct DoubletFilter : public CATSTrackFinder::iDoubletFilter {

  /*! \brief The default constructor. */
  DoubletFilter() : iDoubletFilter() {;}

  /*! \brief Set the threshold. 
      \param _threshold The threshold
  */

  bool matches( const GenericRecoHit& a
              , const GenericRecoHit& b ) const override {
    return true;
  }

};

/*! \brief typedef for tracklets with user-defined hits */
typedef std::vector<const GenericRecoHit*> Tracklet_t;

/*! \class MyCollector
    \brief Collector class for the CATS results
*/
class MyCollector : public CATSTrackFinder::iTrackCandidateCollector {

  public:
    /*! \brief The default constructor. */
    MyCollector() {}
    /*! \brief The destructor. */
    ~MyCollector() {}

    /*! \brief Collect the tracklets. */
    void collect(const cats_HitData_t* hits_, size_t N) override {
      Tracklet_t candidates;
      for( size_t i = 0; i < N; i++ ) {
        auto hitPtr = reinterpret_cast<const GenericRecoHit*>( hits_[i] );
        candidates.push_back(hitPtr);
      }
      fTracklets.push_back(candidates);
    }

    /*! \brief Erase the tracklet candidates. */
    void reset() {
      for( unsigned int k = 0; k < fTracklets.size(); k++ ) {
        fTracklets[k].erase( fTracklets[k].begin(), fTracklets[k].end() );
      }
      fTracklets.clear();
    }

    /*! \brief Return the tracklets. */
    std::vector<Tracklet_t> getTracklets() {
      return fTracklets;
    }

    /*! \brief Return the best tracklet.
        
        The best tracklet is obtained as the straight line with minimum
        residuals as computed from a principal component analysis fit of the
        hit position and minimisation of the parametric line equation distance
        to each point (computation of residual sum of squares).
    */
    Tracklet_t getBestTracklet() {
      std::vector<double> residuals;
      std::vector<Eigen::RowVector3d> points;
      for( const auto& tracklet : fTracklets ) {
        points.clear();
        if( tracklet.empty() ) continue;
        for( unsigned int i = 0; i < tracklet.size(); i++ ) {
          points.push_back(Eigen::RowVector3d( tracklet.at(i)->GetGlobalPos().X()
                                             , tracklet.at(i)->GetGlobalPos().Y()
                                             , tracklet.at(i)->GetGlobalPos().Z() ) );
        }

        PCALinearReg reg( points, 0.0);
        reg.Fit();
        Eigen::Vector3d seed = reg.GetClusterSeed();
        Eigen::Vector3d unit = reg.GetUnitVector();
        FittedLine3D recoLine( seed, unit );

        double rss = 0.0;
        // compute the sum of residuals (residual sum of squares - RSS)
        for( const auto& point : points ) {
          Eigen::Vector3d _reco = recoLine.GetExtrapolation(point(2));
          Eigen::Vector3d _true(point(0),point(1),point(2));

          rss += pow((_reco-_true).norm(), 2);
        }
        residuals.push_back(rss);
      }

      // find index of smallest rss
      auto it = std::min_element(std::begin(residuals), std::end(residuals));
      size_t idx = std::distance(std::begin(residuals), it);

      if( idx >= fTracklets.size() ) return Tracklet_t(); //{nullptr};


      return fTracklets.at(idx);

    }

    /*! \brief Return the sorted tracklets.
        
        The best tracklet is obtained as the straight line with minimum
        residuals as computed from a principal component analysis fit of the
        hit position and minimisation of the parametric line equation distance
        to each point (computation of residual sum of squares).

        \note Temporary implementation.
    */
    std::vector<Tracklet_t> getSortedTracklets() {

      std::vector<Eigen::RowVector3d> points;

      std::vector< std::pair<double, Tracklet_t> > temp;

      for( auto& tracklet : fTracklets ) {
        points.clear();
        if( tracklet.empty() ) continue;
        for( unsigned int i = 0; i < tracklet.size(); i++ ) {
          points.push_back(Eigen::RowVector3d( tracklet.at(i)->GetGlobalPos().X()
                                             , tracklet.at(i)->GetGlobalPos().Y()
                                             , tracklet.at(i)->GetGlobalPos().Z() ) );
        }

        PCALinearReg reg( points, 0.0);
        reg.Fit();
        Eigen::Vector3d seed = reg.GetClusterSeed();
        Eigen::Vector3d unit = reg.GetUnitVector();
        FittedLine3D recoLine( seed, unit );

        double rss = 0.0;
        // compute the sum of residuals (residual sum of squares - RSS)
        for(const auto& point : points ) {
          Eigen::Vector3d _reco = recoLine.GetExtrapolation(point(2));
          Eigen::Vector3d _true(point(0),point(1),point(2));

          rss += pow((_reco-_true).norm(), 2);
        }
        temp.push_back( std::make_pair( rss, tracklet ) );
      }

      // sort
      std::sort( temp.begin(), temp.end(), []( const std::pair<double, Tracklet_t>& p1
                                             , const std::pair<double, Tracklet_t>& p2 )
                                             { return p1.first < p2.first; } );     

      std::vector<Tracklet_t> results;
      for( auto it = temp.begin(); it != temp.end(); it++ ) results.push_back( it->second );

      return results;

    }

  private:

    /*! \brief The container of tracklet candidates. */
    std::vector<Tracklet_t> fTracklets;
};

/*! \class MyTrackFinder
    \brief Interface class to CATS TrackFinder
*/
class MyTrackFinder{
  
  public:

    /*! \brief The constructor.
      
        \note See https://github.com/CrankOne/catsc/tree/main.
    */
    MyTrackFinder( unsigned int       nLayers
                 , size_t             cells
                 , size_t             hits
                 , size_t             refs
                 , const std::string& method
                 , unsigned int       nMissingLayers
                 , unsigned int       minLength
                 , double             angleThreshold
                 , const std::map<std::string, unsigned int>& labels ) :
          fMethod(method)
        , fnMissingLayers(nMissingLayers)
        , fminLength(minLength)
        , fLabels(labels)
        , fCats(new CATSTrackFinder( nLayers, cells, hits, refs ))
        , fFilter(ByAngleFilter(angleThreshold))
        , fDbltFilter(0)
    {}
    MyTrackFinder( const MyTrackFinder& mtf ) :
          fMethod(mtf.fMethod)
        , fnMissingLayers(mtf.fnMissingLayers)
        , fminLength(mtf.fminLength)
        , fLabels(mtf.fLabels)
        , fCats(new CATSTrackFinder(*(mtf.fCats)))
        , fFilter(ByAngleFilter(mtf.fFilter.threshold))
        , fDbltFilter(mtf.fDbltFilter)
    {}
    MyTrackFinder& operator=(const MyTrackFinder&) = delete;

    ~MyTrackFinder() {;}

    void Reset() {
      fCollector.reset();
      fCats->reset();
    }

    void HardReset() {
      Reset();
      delete fCats;
      fCats = nullptr;
    }

    std::map<std::string, unsigned int>
    GetLabels() const {
      return fLabels;
    }

    std::vector<Tracklet_t> Collect( std::vector<std::unique_ptr<GenericRecoHit>>& candidates
                                   , bool useBest = false
                                   , bool verbose = false) {
      std::vector<Tracklet_t> res;

      // add the candidates
      for( unsigned int k = 0; k < candidates.size(); k++ ) {
        fCats->add( candidates[k]->layerID, candidates[k].get() );
      }
#if 0
      for( cats_LayerNo_t nLayer = 0; nLayer < fCats->n_layers(); ++nLayer ) {
        std::cout << "  #" << (int) nLayer << ": "
          << fCats->n_points(nLayer) << std::endl;
      }
#endif
      fCats->evaluate(fFilter, fnMissingLayers/*, fDbltFilter*/);
      if( fMethod == "excessive" )     fCats->collect_excessive(fCollector, fminLength);
      else if( fMethod == "moderate" ) fCats->collect(fCollector, fminLength);
      else if( fMethod == "strict" )   fCats->collect_strict(fCollector, fminLength);
      else if( fMethod == "longest")   fCats->collect_longest(fCollector, fminLength);
      else if( fMethod == "winning")   fCats->collect_winning(fCollector, fminLength);
      else {
        std::cerr << "Error in <MyTrackFinder::Collect>: "
          << "unknown method \"" << fMethod << "\"\n";
        exit(EXIT_FAILURE);
      }

      // TODO: change this to order the tracklets according to the RSS
      res = useBest ?  std::vector<Tracklet_t>{fCollector.getBestTracklet()} 
                     // : fCollector.getTracklets();
                     : fCollector.getSortedTracklets();

      if( verbose ) {
        std::cout << "Info in <MyTrackFinder::Collect>: printing tracklets...\n";
        for( unsigned int i = 0; i < res.size(); i++ ) {
          std::cout << "Tracklets #" << i+1 << ":\n";
          for( auto it = res.at(i).begin(); it != res.at(i).end(); it++ ) {
            auto hitPtr = reinterpret_cast<const GenericRecoHit*>( *it );
            std::cout << hitPtr->name << ": "  << hitPtr->GetGlobalPos().X() << ", "
                                               << hitPtr->GetGlobalPos().Y() << ", "
                                               << hitPtr->GetGlobalPos().Z() << "\n";
          }
        }
      }

      return res;
    }

  private:

    std::string  fMethod;
    unsigned int fnMissingLayers;
    unsigned int fminLength;

    std::map<std::string, unsigned int> fLabels;

    CATSTrackFinder* fCats;
    MyCollector      fCollector;
    ByAngleFilter    fFilter;
    DoubletFilter*   fDbltFilter;
};

/*! \brief Usefull function for filling hits 
 
    \param candidates The container for the different candidates to the tracklet finding.
    \param labels     The name - layer identity map.
    \param e          The RecoEvent instance.
    
    \note The nomenclature of the labels follow the JSON nomenclature of the
    ConfigFileParser.
*/
bool MyTrackFinderFillingHelper( std::vector<std::unique_ptr<GenericRecoHit>>& candidates
                               , map<string, unsigned int>& labels
                               , const RecoEvent& e
                               , const string& placements_path )
{
  // TODO: automatise this, in development stage (for now, only available for muon run 2023)
  //if( !e.mc ) {
  PPTRN::PLACEMENTS.SetConfigFile( conddbpath() + placements_path );
  //}

  for( auto iter = labels.begin(); iter != labels.end(); iter++ ) {
    std::string label(iter->first);

    if( !e.mc ) {

      if( label.find("MM") != std::string::npos ) { // Micromegas

        int trID = stoi(label.substr(2,2));
        std::vector<Hit> hits = e.getMM(trID)->hits;
        if( hits.empty() ) continue;

        for( unsigned int k = 0; k < hits.size(); k++ ) {
          candidates.push_back( std11patch::make_unique<MMRecoHit>( hits.at(k)     // the hit
                                                                  , label          // the name
                                                                  , iter->second   // the layer identity
                                                                  ) );
        }

      }
      else if( label.find("GM") != std::string::npos ) { // GEMs

        int trID = stoi(label.substr(2,2));
        const gem::GEM* gem = e.getGEM(trID);
        if( !(gem->bestHit) ) continue;

        candidates.push_back( std11patch::make_unique<GEMRecoHit>( gem            // the GEM
                                                                 , label          // the name
                                                                 , iter->second   // the layer identity
                                                                 ) );

      }
      else if( label.find("ST") != std::string::npos ) { // Straw

        int trID = stoi(label.substr(2,2));
        std::vector<StHit> hits = e.getST(trID)->Hits;
        if( hits.empty() ) continue;

        // TODO: check the 0.7 condition
        for( unsigned int k = 0; k < hits.size(); k++ ) {
          if( hits.at(k).prob() <= 0.7 ) continue;
          candidates.push_back( std11patch::make_unique<STRecoHit>( hits.at(k)     // the hit
                                                                  , label          // the name
                                                                  , iter->second   // the layer identity
                                                                  ) );
        }
      }
    } // end check for data events
    else {
      if(label.find("MM")!= string::npos){
        int trID = stoi(label.substr(2,2));
        std::vector<Hit> hits = e.getMM(trID)->hits;
        if( hits.empty() ) continue;

        for( unsigned int k = 0; k < hits.size(); k++ ) {
          candidates.push_back( std11patch::make_unique<MMRecoHit>( hits.at(k)     // the hit
                                                                  , label          // the name
                                                                  , iter->second   // the layer identity
                                                                  ) );
        }
      }
      //else if ( label.find("ST0") != string::npos) {
      //  int trID = stoi(label.substr(2,2));
      //  std::vector<StHit> hits = e.getST(trID)->Hits;
      //  
      //  if( hits.empty() ) continue;
      //  // TODO: check the 0.7 condition
      //  for( unsigned int k = 0; k < hits.size(); k++ ) {
      //    if( hits.at(k).prob() <= 0.7 ) continue;
      //    candidates.push_back( std11patch::make_unique<STRecoHit>( hits.at(k)     // the hit
      //                                                            , label          // the name
      //                                                            , iter->second   // the layer identity
      //                                                            ) );
      //  }
      //}
      else {
        vector<MChit> hits = e.mc->getMCTruth(label);

        if( hits.empty() ) continue;
        for( unsigned int k = 0; k < hits.size(); k++ ) {
          candidates.push_back( std11patch::make_unique<MCRecoHit>( hits.at(k)   // the hit
                                                                  , label        // the name
                                                                  , iter->second // the layer identity
                                                                  ) );
        }
      }
    } // end check for MC events

  } // end of loop over labels

  // verify whether there is at least three different layers (layer identity corresponds to each inserted hits)
  std::set<unsigned int> uniqueLayers;
  for (auto & candidate : candidates) uniqueLayers.insert( candidate->layerID ); 
  

  return uniqueLayers.size() >= 3 ? true : false;

}
