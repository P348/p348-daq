#pragma once

#include "TVector3.h"
#include "TMath.h"

// 3-point track momentum calculation
// (the method make use of exact magnet position)
// p1, p2 - coordinates of two hit points before magnet
// p3 - coordinates of hit point after magnet
// magb_z, mage_z - Z coordinates of begin and end of magnet
// mag_field - magnetic field, Tesla
// length and position units: mm

double simple_tracking3(
  const TVector3 p1,
  const TVector3 p2,
  const TVector3 p3,
  const double magb_z,
  const double mage_z,
  const double mag_field)
{
  const TVector3 del_12 = p2 - p1;
  const TVector3 del_23 = p3 - p2;
  
  // extrapolate p1 -> p2 line on p3 surface
  const double t1 = del_23.Z() / del_12.Z();
  const TVector3 P3 = p2 + t1 * del_12;
  
  const TVector3 del_23_new = p3 - P3;
  
  double mag_len = fabs(mage_z - magb_z);

  TVector3 del_23_correct = del_23_new * ((mage_z - magb_z)/2.0 / (p3.Z() - mage_z + (mage_z - magb_z)/2.0 ) );
  double rad = 0.5 * mag_len * sqrt(1 + pow(mag_len / del_23_correct.Perp(), 2));

  rad *= 0.001;

  const double q = 1.6E-19; // particle charge, C
  const double SI_GeV = 5.34E-19; // SI to GeV conversion = 1e9 * e * 1V / c , e - electron charge, c - speed of light
  const double C = mag_field * q / SI_GeV;
  
  const double mom = C * rad * cos(atan(del_12.Y()/del_12.Z()));

  return mom;
}

// simple track reconstruction
// p1, p2 - coordinates of two hit points before magnet
// p3, p4 - coordinates of two hit points after magnet
// mag_len - length of magnetic field region along Z
// mag_field - magnetic field, Tesla
// length and position units: mm

double simple_tracking(
  const TVector3 p1,
  const TVector3 p2,
  const TVector3 p3,
  const TVector3 p4,
  const double mag_len,
  const double mag_field)
{
  const TVector3 del_12 = p2 - p1;
  const TVector3 del_23 = p3 - p2;
  const TVector3 del_34 = p4 - p3;
  const TVector3 del_24 = p4 - p2;
  
  // extrapolate p1 -> p2 line on p3 surface
  const double t1 = del_23.Z() / del_12.Z();
  const TVector3 P3 = p2 + t1 * del_12;
  
  // extrapolate p1 -> p2 line on p4 surface
  const double t2 = del_24.Z() / del_12.Z();
  const TVector3 P4 = p2 + t2 * del_12;
  
  const TVector3 del_23_new = p3 - P3;
  const TVector3 del_24_new = p4 - P4;
  
  const double def_1 = sqrt(pow(del_12.Y(),2));
  const double def_2 = del_23_new.Perp();
  const double def_3 = del_24_new.Perp();
  
  const double q = 1.6E-19; // particle charge, C
  const double SI_GeV = 5.34E-19; // SI to GeV conversion = 1e9 * e * 1V / c , e - electron charge, c - speed of light
  const double C = mag_field * q / SI_GeV;
  
  const double rad = mag_len*1.E-3 * sqrt(1 + pow(del_34.Z() / (def_2-def_3), 2));
  const double mom = C * rad * cos(atan(def_1/del_12.Z()));
  
  return mom;
}

// straight segment of track
struct TrackSegment {
  bool isSet;
  TVector3 p1;
  TVector3 p2;
  
  TrackSegment(): isSet(false) {}
  
  void set(const TVector3& p1_, const TVector3& p2_)
  {
    isSet = true;
    p1 = p1_;
    p2 = p2_;
  }
  
  operator bool() const { return isSet; }
  TVector3 v() const { return (p2 - p1); }
  double theta() const { return v().Theta(); }
  double theta_x() const { return TMath::ATan2(v().x(), v().z()); }
  double theta_y() const { return TMath::ATan2(v().y(), v().z()); }
};


struct SimpleTrack {
  double momentum;
  TrackSegment in;
  TrackSegment out;
  
  SimpleTrack(): momentum(0) {}
  
  operator bool() const { return in && out; }
  
  double in_out_angle() const { return in.v().Angle(out.v()); }
  
  double in_out_distance() const
  {
    // distance between skew lines
    // https://en.wikipedia.org/wiki/Skew_lines#Distance
    const TVector3 n = in.v().Cross(out.v()).Unit();
    return fabs(n.Dot(out.p1 - in.p2));
  }
};

ostream& operator<<(ostream& os, const TVector3& v)
{
  os << "[" << v.X() << ", " << v.Y() << ", " << v.Z() << "]";
  return os;
}

ostream& operator<<(ostream& os, const SimpleTrack& t)
{
  os << "SimpleTrack:\n"
     << "  isDefined = " << (bool)t << "\n";
  
  if (!t) return os;
  
  const double mrad = 1e-3; // radians to milli-radians conversion
  
  os << "  momentum = " << t.momentum << " GeV/c\n"
     
     << "  in.theta = " << t.in.theta()/mrad << " mrad\n"
     << "  in.theta_x = " << t.in.theta_x()/mrad << " mrad\n"
     << "  in.theta_y = " << t.in.theta_y()/mrad << " mrad\n"
     
     << "  out.theta = " << t.out.theta()/mrad << " mrad\n"
     << "  out.theta_x = " << t.out.theta_x()/mrad << " mrad\n"
     << "  out.theta_y = " << t.out.theta_y()/mrad << " mrad\n"
     
     << "  in_out_angle = " << t.in_out_angle()/mrad << " mrad\n"
     << "  in_out_distance = " << t.in_out_distance() << " mm\n";
  
  return os;
}

// convenience function assuming hits in four MM chambers
SimpleTrack simple_tracking_4MM_2016(const RecoEvent& e)
{
  SimpleTrack t;
  
  // upstream track
  const bool hasUpStream = (e.MM1.hasHit() && e.MM2.hasHit());
  if (hasUpStream) {
    const TVector3 p1 = e.MM1.abspos();
    const TVector3 p2 = e.MM2.abspos();
    t.in.set(p1, p2);
  }
  
  // downstream track
  const bool hasDownStream = (e.MM3.hasHit() && e.MM4.hasHit());
  if (hasDownStream) {
    const TVector3 p3 = e.MM3.abspos();
    const TVector3 p4 = e.MM4.abspos();
    t.out.set(p3, p4);
  }
  
  if (t) {
    // momentum
    const double mag_len = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
    t.momentum = simple_tracking(t.in.p1, t.in.p2, t.out.p1, t.out.p2, mag_len, MAG_field);
  }
  
  return t;
}

// convenience function assuming hits in four MM chambers
SimpleTrack simple_tracking_4MM_2017(const RecoEvent& e)
{
  SimpleTrack t;
  
  // upstream track
  const bool hasUpStream =
    (e.MM1.hasHit() || e.MM2.hasHit()) && (e.MM3.hasHit() || e.MM4.hasHit());
  if (hasUpStream) {
    const TVector3 p1 = e.MM1.hasHit() ? e.MM1.abspos() : e.MM2.abspos();
    const TVector3 p2 = e.MM3.hasHit() ? e.MM3.abspos() : e.MM4.abspos();
    t.in.set(p1, p2);
  }
  
  // downstream track
  const bool hasDownStream = (e.MM5.hasHit() && e.MM7.hasHit());
  if (hasDownStream) {
    const TVector3 p3 = e.MM5.abspos();
    const TVector3 p4 = e.MM7.abspos();
    t.out.set(p3, p4);
  }
  
  if (t) {
    // momentum
    const double mag_len = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
    t.momentum = simple_tracking(t.in.p1, t.in.p2, t.out.p1, t.out.p2, mag_len, MAG_field);
  }
  
  return t;
}

SimpleTrack simple_tracking_4MM_2018(const RecoEvent& e)
{
  SimpleTrack t;
  
  // upstream track
  const bool hasUpStream = (e.MM1.hasHit() && e.MM2.hasHit());
  if (hasUpStream) {
    const TVector3 p1 = e.MM1.abspos();
    const TVector3 p2 = e.MM2.abspos();
    t.in.set(p1, p2);
  }
  
  // downstream track
  const bool hasDownStream =
    (e.MM3.hasHit() || e.MM4.hasHit()) && (e.MM5.hasHit() || e.MM6.hasHit());
  if (hasDownStream) {
    const TVector3 p3 = e.MM3.hasHit() ? e.MM3.abspos() : e.MM4.abspos();
    const TVector3 p4 = e.MM5.hasHit() ? e.MM5.abspos() : e.MM6.abspos();
    t.out.set(p3, p4);
  }
  
  if (t) {
    // momentum
    const double mag_len = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
    t.momentum = simple_tracking(t.in.p1, t.in.p2, t.out.p1, t.out.p2, mag_len, MAG_field);
  }
  
  return t;
}

SimpleTrack simple_tracking_4MM_2018_vis150(const RecoEvent& e)
{
  SimpleTrack t;
  
  // upstream track
  const bool hasUpStream = (e.MM3.hasHit() && e.MM4.hasHit());
  if (hasUpStream) {
    const TVector3 p1 = e.MM3.abspos();
    const TVector3 p2 = e.MM4.abspos();
    t.in.set(p1, p2);
  }
  
  // downstream track
  const bool hasDownStream =
    (e.MM5.hasHit() && e.MM6.hasHit());
  if (hasDownStream) {
    const TVector3 p3 = e.MM5.abspos();
    const TVector3 p4 = e.MM6.abspos();
    t.out.set(p3, p4);
  }
  
  if (t) {
    // momentum
    const double mag_len = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
    t.momentum = simple_tracking(t.in.p1, t.in.p2, t.out.p1, t.out.p2, mag_len, MAG_field);
  }
  
  return t;
}

// convenience function assuming hits in four MM chambers
SimpleTrack simple_tracking_4MM(const RecoEvent& e)
{
  // 2016B
  if (1776 <= e.run && e.run <= 2551) return simple_tracking_4MM_2016(e);
  
  // 2017
  if (2817 <= e.run && e.run <= 3573) return simple_tracking_4MM_2017(e);
  
  // 2018
  if (3574 <= e.run && e.run <= 4207) return simple_tracking_4MM_2018(e);
  if (4224 <= e.run && e.run <= 4310) return simple_tracking_4MM_2018_vis150(e);
  
  // 2021
  // same configuration as 2018: upstream MM1+2, downstream MM3+4, MM5+6
  if (4642 <= e.run && e.run <= 5187) return simple_tracking_4MM_2018(e);
  
  // 2021mu: make us of 2016 - upstream MM1+2, downstream MM3+4
  if (5188 <= e.run && e.run <= 5572) return simple_tracking_4MM_2016(e);
  
  // 2022 (electron) and 2023 (electron)
  // same configuration as 2016: upstream MM1+2, downstream MM3+4
  if (6035 <= e.run && e.run <= 999999) return simple_tracking_4MM_2016(e);
  
  return SimpleTrack();
}

// extrapolate the line defined by two points p1 and p2
// and find the cross-section point with the surface defined by surfZ coordinate

//const TVector3 ecal_hit = extrapolate_line(e.MM3.abspos(), e.MM4.abspos(), ECAL_pos.pos.Z())

TVector3 extrapolate_line(
  const TVector3 p1,
  const TVector3 p2,
  const double surfZ)
{
  const TVector3 diff = p2 - p1;
  const double t = (surfZ - p2.Z()) / diff.Z();
  const TVector3 crossp = p2 + t * diff;
  return crossp;
}

TVector3 extrapolate_line(
  const TrackSegment ts,
  const double surfZ)
{
  return extrapolate_line(ts.p1, ts.p2, surfZ);
}
