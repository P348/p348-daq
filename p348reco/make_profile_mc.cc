// ROOT
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TProfile2D.h>

// c++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"
#include "simu.h"

int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "Usage: ./makeprofile_mc.exe <MC file name> " << std::endl;
    return 1;
  }
  
  std::ifstream inFile(argv[1]);
  
  if (! inFile) {
    std::cerr << "ERROR: can't open file " << argv[1] << std::endl;
    return 1;
  }
  
  const int Nevents = 1e8;

    
  // cuts for selecting profile
  const double srd_cut[] = {1.,70.}; //MeV
  const double HCAL_cut = 1.; //GeV


  TFile* file_profile = new TFile("shower_profileMC.root","RECREATE");

  //VARIABLE RELEVANT TO SHOWER PROFILE
  //size of the spot where the profiles are selected
  const double spot_radius = 20; //mm
  //size of the bin of the profile, should not go under 0.3 mm to not exceed MM precision in extrapolate the line
  const int Nbin_profile = 3*spot_radius;
  // Percentage of energy fraction in cell
  const double norm = 100.;
  

  /*      define TProfile2D to fill, The profile have their center in the same center of the beam spot on the ECAL surface and they express
          the percentage of the total energy recorded by the ECAL deposited in each cell   */
  ////  chi-square distribution due to the large fluctuation of low energy deposit in the cells ////
  TString name;
  TString title;
  TProfile2D* Shower_Profiles[6][6];
  for(int x=0;x < 6;++x)
    for(int y=0;y < 6;++y)
      {
        name.Form("ECAL1-%i-%i",x,y);
        title.Form("ECAL energy fraction deposited in cell Nr.%iX%i",x,y);
        Shower_Profiles[x][y] = new TProfile2D(name,title,Nbin_profile,-spot_radius,spot_radius,Nbin_profile,-spot_radius,spot_radius,0,100);
      } 
  
  // event loop, read event by event
  for (int iev = 0; iev < Nevents; iev++) {

    const RecoEvent e = RunP348RecoMC(inFile);
    if (!e.mc) break;

    const double ecal1 = e.ecalTotalEnergy(1);

    // print progress
    if (iev % 10000 == 1)
      std::cout << "===> Event #" << iev << std::endl;


    const TVector3 P_ecal (e.mc->ECALEntryX,e.mc->ECALEntryY,0);
    //shift to center of beam spot
    const TVector3 showerpos = P_ecal - ECAL0BEAMSPOT_pos;


    //prepare booleans of the cuts
    const bool SRDcut = e.SRD[0].energy / MeV > srd_cut[0] && e.SRD[1].energy / MeV > srd_cut[0] && e.SRD[2].energy / MeV > srd_cut[0] && e.SRD[0].energy / MeV < srd_cut[1] && e.SRD[1].energy / MeV < srd_cut[1] && e.SRD[2].energy / MeV < srd_cut[1];

    const bool HCALcut = (e.hcalTotalEnergy(0) + e.hcalTotalEnergy(1) + e.hcalTotalEnergy(2)) < HCAL_cut;

    const bool cutAll = SRDcut&&HCALcut;
   
    if (!cutAll) continue;

    // fill the shower profiles
    for (int x = 0; x < 6; x++)
      for (int y = 0; y < 6; y++)
        Shower_Profiles[x][y]->Fill(showerpos.X(), showerpos.Y(), norm*e.ECAL[1][x][y].energy/ecal1);

  }

  // Determine central cell by looking at the cell with most energy
  double totalenergy = -1;
  int xmax, ymax;
  for (int x = 0; x < 6; x++) {
    for (int y = 0; y < 6; y++) {
      double tot = Shower_Profiles[x][y]->Integral();
      if (tot > totalenergy) {
        totalenergy = tot;
        xmax = x; ymax = y;
      }
    }
  }

  // Add list of parameters
  file_profile->cd();
  TList* parlist = new TList();
  // Not too sure about showerpos.X() and showerpos.Y()
  parlist->Add(new TParameter<int>("pcenterX", xmax, 'f'));
  parlist->Add(new TParameter<int>("pcenterY", ymax, 'f'));
  // These parameters are read by LoadMCGeometryFromMetaData() in simu.h
  parlist->Add(new TParameter<int>("dimX", ECAL_pos.Nx, 'f'));
  parlist->Add(new TParameter<int>("dimY", ECAL_pos.Ny, 'f'));
  parlist->Add(new TParameter<double>("norm", norm, 'f'));
  parlist->Add(new TParameter<double>("NominalBeamEnergy", NominalBeamEnergy, 'f'));
  parlist->Write("parameters", TList::kSingleKey);

  file_profile->Write();
  file_profile->Close();

  return 0;
}
