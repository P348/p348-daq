#pragma once

#include <string>

#include <Chip.h>

#include "conddb.h"

using namespace std;

// Micromegas Strip containing all relevant charge and time information
struct MMStrip {
  CS::uint16           bin;        // Strip number
  array<CS::uint32, 3> raw;        // charge in ADC units, A0, A1, A2
  CS::uint32           charge;     // charge in ADC units, maximum of A0, A1, A2
  double               t02;        // time calculated with timing calibrations from A0/A2 ratio
  double               t12;        // time calculated with timing calibrations from A1/A2 ratio
  double               sigma02;    // sigma of time from A0/A2 ratio
  double               sigma12;    // sigma of time from A1/A2 ratio

  // Default Constructor
  MMStrip() : bin(0)
  { SetToZero(); }
  // Constructor with only Strip number
  MMStrip(CS::uint16 bin_)
    : bin(bin_)
  { SetToZero(); }
  // Constructor with full information
  MMStrip(CS::uint16 bin_, array<CS::uint32, 3> raw_, CS::uint32 charge_,
      double t02_, double t12_,
      double sigma02_, double sigma12_)
    : bin(bin_), raw(raw_), charge(charge_),
    t02(t02_), t12(t12_),
    sigma02(sigma02_), sigma12(sigma12_)
  {}

  // Identify MMStrips based on their readout channel
  bool operator==(const MMStrip &other) const {
    return (bin == other.bin);
  }

  void SetToZero();

  // hit time computed with t02 and t12
  double time();
};

MMStrip avgStrip(const CS::uint16 bin, const MMStrip &s1, const MMStrip &s2);

// A cluster is a collection of fired strips
struct MMCluster {
  vector<MMStrip> strips; // All strips of the Micromegas plane are added, but only the first #<size> contain info
  double charge_total;            // total charge

  MMCluster() : charge_total(0) {}

  size_t size() const { return strips.size(); }

  void clear() { strips.clear(); charge_total = 0; }

  void add_strip(const MMStrip& new_strip);

  void add_strip(const CS::uint16 bin_, const array<CS::uint32, 3> raw_, const CS::uint32 charge_, const double t02_, const double t12_, const double sigma02_, const double sigma12_);

  void edit_strip_charge(const CS::uint16 bin_, const CS::uint32 charge_);

  bool has_strip(const CS::uint16 bin_) const;

  // return cluster center-of-mass
  double position() const;

  double cluster_time() const;

  double cluster_time_error() const;

};

struct MicromegaPlane {
  vector<MMStrip> strips;
  vector<MMCluster> clusters;
  const MM_plane_calib* calib;

  MicromegaPlane(): calib(0) {}

  bool hasHit() const { return (clusters.size() > 0); }

  bool hasCalibration() const {return calib;}

  size_t bestClusterIndex() const { return 0; } // DoMMClusSort puts the best clusters at the beginning
  double bestHitStrip() const { return hasHit() ? clusters[bestClusterIndex()].position() : -1; }
  double bestHitTime() const { return hasHit() ? clusters[bestClusterIndex()].cluster_time() : nan("");}
  double totalCharge() const { double total=0; for(const auto &cluster : clusters) { total+=cluster.charge_total; } return total;}

  void Init(const MM_plane_calib& _calib)
  {
    calib = &_calib;
    const int MM_size = calib->MM_Nstrips;
    strips.resize(MM_size);
    for (auto istrip(0); istrip < MM_size; istrip++) strips.at(istrip).bin = istrip;
  }

  void DoMMClusClear();
  void DoMMClusSort();
  void DoMMClustering();
  void FixMMBadStrips();
  void FixMMClusMultiplex();
};

//Hit struct

struct Hit {
  const MMCluster* xcluster;
  const MMCluster* ycluster;
  const DetGeo* geom;
  const MM_calib* calib;

  double xpos;
  double ypos;

  Hit(): xcluster(0), ycluster(0), geom(0), calib(0), xpos(0), ypos(0) {}

  void CalculatePos();

  // Return sum of squared difference to reference time for both planes
  double timediff() const;

  TVector3 pos() const { return TVector3(xpos, ypos, 0); }
  TVector3 abspos() const {return (pos() + geom->pos); }
};

struct Micromega {
  MicromegaPlane xplane;
  MicromegaPlane yplane;
  vector<Hit> hits;
  double xpos;
  double ypos;
  const MM_calib* calib;
  const DetGeo* geom;

  Micromega(): xpos(0), ypos(0), calib(0), geom(0) {}
  Micromega& operator=(const Micromega&) = delete;

  // custom copy for proper cluster reference
  // TODO: the only use of the Hit::xcluster,::ycluster is the hits sort (disabled by default) at reco step Micromega::DoMMReco()
  //       revise/remove Hit::xcluster,::ycluster pointers and so the custom copy constructor?
  Micromega(const Micromega & orig);

  bool hasHit() const { return (xplane.hasHit() && yplane.hasHit()); }

  bool hasCalibration() const { return ( geom && calib ); }

  TVector3 pos() const { return TVector3(xpos, ypos, 0); }

  TVector3 abspos() const {return (pos() + geom->pos); }

  void Init(const MM_calib& _calib, const DetGeo& _geom);

  void MMCalculatePos();
  void DoMMHitSort();
  void DoMMReco();
};

void DoHitAccumulation(const CS::uint16 chan, const CS::uint32* raw_q, MicromegaPlane& plane);

