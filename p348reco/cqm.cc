// c++
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <cmath>
using namespace std;

struct channel {
  string name;
  double mean;
  double stddev;
  int num_entries;
  
  channel(): mean(0.), stddev(0.), num_entries(0) {}
  
  double mean_error() const { return stddev / sqrt(num_entries); }
  
  bool hasData() const { return (num_entries != 0); }
  operator bool() const { return hasData(); }
  
  // return event type
  string etype() const { return name.substr(0, name.find(':')); }
  
  string mean_string() const
  {
    if (!hasData()) return "missing";
    
    ostringstream oss;
    oss << mean << "@" << mean_error();
    return oss.str();
  }
};

vector<channel> read_file(string fname)
{
  ifstream inpfile(fname);
  vector <channel> v;
  string line;
  
  while(getline(inpfile,line)){
    istringstream ss(line);
    channel c;
    ss >> c.name >> c.mean >> c.stddev >> c.num_entries;
    
    v.push_back(c);
  }
  
  return v;
}

int main(int argc, char* argv[])
{
  if (argc != 3) {
    cout << "ERROR: incorrect parameters (two arguments expected)" << endl;
    return 1;
  }
  
  const vector<channel> vc1 = read_file(argv[1]);
  const vector<channel> vc2 = read_file(argv[2]);
  
  if (vc1.empty()) {
    cout << "ERROR: failed to read file " << argv[1] << endl;
    return 1;
  }
  
  if (vc2.empty()) {
    cout << "ERROR: failed to read file " << argv[2] << endl;
    return 1;
  }
  
  
  map<string, pair<channel, channel> > data;
  
  for (size_t i = 0; i < vc1.size(); i++) data[vc1[i].name].first = vc1[i];
  for (size_t i = 0; i < vc2.size(); i++) data[vc2[i].name].second = vc2[i];
  
  // output header
  cout << left;
  cout << setw(35+1) << "Channel "
       << setw(20+1) << "File1"
       << setw(20+1) << "File2"
       << "Diff"
       << endl;
  cout << "-------------------------------------------"
       << "-------------------------------------------"
       << endl;
  
  // compare channels between file1 and file2
  for (auto i = data.begin(); i != data.end(); ++i) {
    const string& name = i->first;
    const channel& c1 = i->second.first;  // file1
    const channel& c2 = i->second.second; // file2
    const string etype = c1 ? c1.etype() : c2.etype();
    const bool isComplete = c1 && c2;
    
    // skip unknown event type channels
    if (etype == "UNKNOWN") continue;
    
    const double chi = fabs(c1.mean - c2.mean) / sqrt(pow(c1.mean_error(),2) + pow(c2.mean_error(),2));
    const bool isCompatible = isComplete && (chi < 4);
    
    // skip compatible channels
    if (isCompatible) continue;
    
    cout << setw(35) << name << " "
         << setw(20) << c1.mean_string() << " "
         << setw(20) << c2.mean_string() << " "
         << chi
         << endl;
  }
  
  return 0;
}
