#pragma once

#include "p348reco.h"
#include "TF1.h"


/*
 * This purely virtual structure contains the base code for pile-up parameterization
 */

struct PileupEcalCell {

public:
  int plane, x, y;
  double ApeakMIN;

  //I declare this as protected, since it is not expected to be used outside this struct and its derived structs
protected:
  TF1 *fCell = 0;
  const int kNpar=10; //not very elegant, but I can't call "getNpars()" in the constructor. Still ok, since I am not using this for fits.

public:
  PileupEcalCell(int pplane, int xx, int yy) :
      plane(pplane), x(xx), y(yy),ApeakMIN(0) {
    fCell = new TF1(Form("fPileupEcalCell_%i_%i_%i", plane, x, y), this, 0, 32,kNpar);
    fCell->SetNpx(1000);
  }

  //members and methods defined in the derived structs
  //To set the parameters of the TF1

  //For the TF1
  virtual double operator()(double *x, double *p) =0;
  virtual int getNpars()=0;
  virtual void setFunParameters(const Cell &c, int iPeak)=0;

  TF1* getTF1(const Cell &c, int iPeak) {
    setFunParameters(c, iPeak);
    return fCell;
  }

  virtual ~PileupEcalCell() {
    if (fCell) {
      delete fCell;
      fCell = 0;
    }
  }

  static constexpr unsigned int switch_ecalID(int xx,int yy){
    return xx*6+yy;
  }
};

/*
 * This structure contains the specific code for 2022B (mostly the positron run)
 * See: https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/365
 *      https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/365
 *
 */

struct PileupEcalCell_2022B: public PileupEcalCell {

private:

public:

  //creator
  PileupEcalCell_2022B(int pplane, int xx, int yy) :
      PileupEcalCell(pplane, xx, yy) {

    ApeakMIN = 20;
  }

  /*Define the function itself:

   f(t)=2*A*exp(-t'/tauD)*[1+exp(-t'/tauR)]^-1
   where t'=t-t0

   Parameters are:
   p0: A
   p1: t0
   p2: tauD
   p3: tauR
   *
   */

  double operator()(double *x, double *par) {
    double A = par[0];
    double t0 = par[1];
    double tauD = par[2];
    double tauR = par[3];

    double t = x[0];
    t = t - t0;

    return 2 * A * exp(-t / tauD) / (1 + exp(-t / tauR));
  }

  int getNpars() {
    return 4;
  }

  double getTauR(double Tpeak, double Apeak) {
    double tauR = 0;
    if (plane == 0) {
      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(1, 3)):
        tauR = 0.205 + 3.0907E-5 * Apeak;
        break;

      case (switch_ecalID(2, 3)):
        if (Apeak > 1551.7) {
          tauR = (Apeak - 1551.7) * 7.1405E-5 + 0.3107;
        } else {
          tauR = 0.3107 + (Apeak - 1551.7) * 1.6E-5;
        }
        break;

      case (switch_ecalID(3, 3)):
        tauR = 0.3483 * pow(1 - exp(-Apeak / 292.8), 0.4638);
        break;

      default:
            tauR = 0.366;
            break;

      }
    }
    if (plane == 1) {
      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(1, 3)):
        tauR = 0.32;
        break;

      case (switch_ecalID(2, 2)):
        // tauR = 0.055;
        tauR = 0.23 * pow(1 - exp(-Apeak / 46.42), 3.38);
        if (tauR < 0.055)
          tauR = 0.055;
        break;

      case (switch_ecalID(2, 3)):
        tauR = 0.366;
        break;

      case (switch_ecalID(3, 3)):
        tauR = 0.3672;
        break;

      case (switch_ecalID(2, 4)):
//      tauR = 0.36;
        tauR = 0.32;
        break;

      default:
        tauR = 0.366;
        break;
      }
    }
    return tauR;
  }

  double getTauD(double Tpeak, double Apeak) {
    double tauD = 0;
    if (plane == 0) {
      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(1, 3)):
        tauD = (1.79824 + Apeak * 7.64692E-5) * pow(1 - exp(-Apeak / 40.58), 1.599);
        break;

      case (switch_ecalID(2, 3)):
        tauD = 2.04 * pow(1 - exp(-Apeak / 74.7), 0.34187);
        break;

      case (switch_ecalID(3, 3)):
        tauD = (2.1 + Apeak * 7.424E-5) * pow(1 - exp(-Apeak / 42.52), 0.9362);
        break;
      default:
        tauD = 2.23;
        break;

      }
    }

    if (plane == 1) {
      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(2, 2)):
        tauD = 1.9 * pow(1 - exp(-Apeak / 65.39), 0.95);
        /*if (Apeak < 50) {
         tauD = 0.2 + Apeak / 50 * 1.4;
         } else if (Apeak < 80) {
         tauD = 1.6 + (Apeak - 50) / 30 * 0.3;
         } else if (Apeak < 150) {
         tauD = 1.9 + (Apeak - 80) / 70 * 0.1;
         } else {
         tauD = 2.0;
         }*/
        break;

      case (switch_ecalID(1, 3)):
        tauD = 1.661 * pow(1 - exp(-Apeak / 150.4), 0.1969);
        break;

      case (switch_ecalID(2, 3)):
        tauD = 2.227 * pow(1 - exp(-Apeak / 161.4), 0.09965);
        break;

      case (switch_ecalID(3, 3)):
        tauD = (1.993 + 3.158E-4 * Apeak) * pow(1 - exp(-Apeak / 29.74), 0.2493);
        break;

      case (switch_ecalID(2, 4)):
        tauD = 1.9 * pow(1 - exp(-Apeak / 142.5), 0.8899);
        /* if (Apeak < 100) {
         tauD = 0.2 + Apeak / 100 * 1.5;
         } else if (Apeak < 200) {
         tauD = 1.7 + (Apeak - 100) / 100 * 0.3;
         } else {
         tauD = 2.0;
         }
         if (tauD < 1)
         tauD = 1.; //cut-off
         */
        break;

      default:
        tauD = 2.23;
        break;
      }
    }
    return tauD;
  }

  double getA(double Tpeak, double Apeak, double Apre, double Apost, double tauR, double tauD) {
    double A = 0;
    if (plane == 0) {
      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(1, 3)): //different
        A = 1.0868 * (0.8 * Apeak + 0.3 * (Apre + Apost)) + 1.092;
        break;

      case (switch_ecalID(2, 3)):
        A = 1.00847 * (0.7 * Apeak + 0.3 * (Apre + Apost)) + 3.04;
        break;

      case (switch_ecalID(3, 3)): //different
        A = 0.9146 * (0.8 * Apeak + 0.3 * (Apre + Apost)) + 9.65597;
        break;

      default:
        A = 0; //default: set amplitude to zero, i.e. no waveform. Very important.
        break;
      }
    } else if (plane == 1) {
      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(1, 3)):
        A = 1.02 * (0.7 * Apeak + 0.3 * (Apre + Apost)) + 1.342;
        break;

      case (switch_ecalID(2, 2)):
        //A = 0.8796 * (0.7 * Apeak + 0.3 * (Apre + Apost)) + 1.5;
        A = 1.07 * (0.7 * Apeak + 0.3 * (Apre + Apost)) + 2.5;
        break;

      case (switch_ecalID(2, 3)):
        A = 0.943 * (0.7 * Apeak + 0.3 * (Apre + Apost)) + 3.662;
        break;

      case (switch_ecalID(2, 4)):
        // A = 1.055 * (0.7 * Apeak + 0.3 * (Apre + Apost)) - 5.38;
        A = 0.961 * (0.7 * Apeak + 0.3 * (Apre + Apost)) + 12.6;
        break;

      case (switch_ecalID(3, 3)):
        A = 0.96736 * (0.7 * Apeak + 0.3 * (Apre + Apost)) - 1.034;
        break;

      default:
        A = 0; //default: set amplitude to zero, i.e. no waveform. Very important.
        break;
      }
    }
    A = (A / 2) / (1 - tauR / tauD) / pow(tauD / tauR - 1, -tauR / tauD);
    return A;
  }

  double getT0(double Tpeak, double Apeak, double Apre, double Apost, double tauR, double tauD) {
    double t0 = Tpeak;
    if (plane == 0) {
      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(1, 3)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        t0 = t0 * 1.0166 - 0.39;
        break;

      case (switch_ecalID(2, 3)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        t0 = t0 * 1.0093 - 0.26;
        break;

      case (switch_ecalID(3, 3)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        t0 = t0 * 0.9718 + 0.106;
        break;
      }
    }
    if (plane == 1) {

      switch (switch_ecalID(x, y)) {

      case (switch_ecalID(1, 3)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        t0 = t0 * .978 + 0.189;
        break;

      case (switch_ecalID(2, 2)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        //t0 = t0 * 0.9763 + 0.1234;
        t0 = 11. + (t0 - 11.4) / (2.8) * 3;
        break;

      case (switch_ecalID(2, 3)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        t0 = t0 * .9963 - 0.1;
        break;

      case (switch_ecalID(2, 4)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        //t0 = t0 * 1.019 - 0.37;
        break;

      case (switch_ecalID(3, 3)):
        t0 += (pow(Apost, 4) - pow(Apre, 4)) / (pow(Apost, 4) + pow(Apre, 4) + pow(Apeak, 4));
        t0 = t0 * .99089;
        break;

      default:
        break;
      }
    }
    t0 -= tauR * log(tauD / tauR - 1);

    return t0;
  }

  void setFunParameters(const Cell &c, int iPeak) {

    double Tpeak = c.peaks[iPeak];
    double Apeak = c.wave(c.peaks[iPeak]);
    double Apre = 0;
    double Apost = 0;

    double A = 0;
    double tauR = 0;
    double tauD = 0;
    double t0 = 0;

    if ((Tpeak > 0) && (Tpeak < (c.raw.size() - 1))) {
      Apre = c.wave(c.peaks[iPeak] - 1);
      Apost = c.wave(c.peaks[iPeak] + 1);
    }

    /*Set tauR. This parameters is, probably, the "less critical" one, since it parameterizes the rising edge,
     * but for pile-up correction the falling edge is more critical
     */
    tauR = getTauR(Tpeak, Apeak);

    /*Set tauD*/
    tauD = getTauD(Tpeak, Apeak);

    if (tauR > tauD) {  //patological case, fix to not fail
      tauD = tauR * 1.01;
    }
    /*Set the amplitude of the waveform.
     * The waveform maximum is:
     *
     * Am=2*A*(1-tauR/tauD)*[tauD/tauR-1]^(-tauR/tauD)
     *
     * Compute A only if the peak amplitude is "large", otherwise this peak is not relevant, set A to 0
     */
    if (Apeak > ApeakMIN) {
      A = getA(Tpeak, Apeak, Apre, Apost, tauR, tauD);
      if (A < 0)
        A = 0; //fix
    }
    /*
     *Set the time t0.
     *--> The maximum of the function is tM=t0+tauR*log(tauD/tauR-1)
     *--> I saw a good correlation between tM and the waveform max time, evaluated as the weighted average of the tPeak,tPeak-1, and tPeak+1 with A^4
     *--> I can thus set t0
     */
    t0 = getT0(Tpeak, Apeak, Apre, Apost, tauR, tauD);

    fCell->SetParameter(0, A);
    fCell->SetParameter(1, t0);
    fCell->SetParameter(2, tauD);
    fCell->SetParameter(3, tauR);

    //  cout << "PARAMS: " << x << " " << y << " " << Apeak << " " << Tpeak << " --> " << A << " " << t0 << " " << tauD << " " << tauR << endl;

  }

};




struct PileupCells {
  PileupEcalCell *ECAL[2][12][6] = { { { 0 } } };
};

//Global var to be used
PileupCells pileup;

void initPileup(const RecoEvent &e) {
  static bool isFirst = true;
  if (!isFirst) {
    return;
  }
  const bool is2022B = (6035 <= e.run && e.run <= 8458);
  if (is2022B) {
    cout << "initPileup - 2022B " << endl;
    fflush(stdout);
    //Init ECAL1 central 3x3 matrix
    for (int ix = 1; ix <= 3; ix++) {
      for (int iy = 2; iy <= 4; iy++) {
        pileup.ECAL[1][ix][iy] = new PileupEcalCell_2022B(1, ix, iy);
      }
    }
    //Init ECAL0 (only central cell, left neighborhood, right neighborhood
    pileup.ECAL[0][1][3] = new PileupEcalCell_2022B(0, 1, 3);
    pileup.ECAL[0][2][3] = new PileupEcalCell_2022B(0, 2, 3);
    pileup.ECAL[0][3][3] = new PileupEcalCell_2022B(0, 3, 3);
  } else {
    //Do nothing for other setups
    void(0);
  }
  isFirst = false;
}

void correctEcalEneCellPileUp(Cell &c, PileupEcalCell *p) {
  if (!c.hasDigit)
    return;

  if (c.amplitude_peak_index <= 0)
    return;

  // recovery calibration factor
  // TODO: for now is not possible to use calibration from .calib structure as
  //       LED corrections are not included into .calib->K
  const double K = (c.amplitude != 0) ? c.energy / c.amplitude : 0.;

  double sumtail = 0.;
  int ipeak = c.amplitude_peak_index - 1;
  for (int ipeak = c.amplitude_peak_index - 1; ipeak >= 0; ipeak--) {
    auto f = p->getTF1(c,ipeak);
    if (f) {
      sumtail = f->Eval(c.amplitude_sample);
      if (sumtail) {
        break;
      }
    }

  }
  // TODO: each peakamplitude(i) should be corrected for pileup from former peaks

  // convert amplitude -> energy units
  sumtail *= K;

  c.energy -= sumtail;
  return;

}

/*This function corrects ECAL cells for pile-up.
 * It should be the only function called in the code
 *
 * */
void correctEcalEnePileUp(RecoEvent &e) {

  initPileup(e);

  const bool is2022B = (6035 <= e.run && e.run <= 8458);
  if (is2022B) { //centermost 3x3 matrix, ECAL0 and ECAL1
    for (int ip = 0; ip < 2; ip++) {
      for (int ix = 1; ix <= 3; ix++) {
        for (int iy = 2; iy <= 4; iy++) {
          if (pileup.ECAL[ip][ix][iy]) {
            correctEcalEneCellPileUp(e.ECAL[ip][ix][iy], pileup.ECAL[ip][ix][iy]);
          }
        }
      }
    }
  } else {
    //Do nothing for other runs
    void(0);
  }
}

