Tracking Tools usage
--------------------

0. Build Tracking Tools (only once)

  $ cd p348reco
  $ make genfit-install
  $ make tracking-tools-install

1. Environment (for each new terminal session)

  $ export GENFIT=1 TRACKINGTOOLS=1

2. Compile example (with tracking-tools)

  $ make example.exe

3. Run

  $ ./example.exe /eos/experiment/na64/data/cdr/cdr01001-005495.dat

4. Output is example.pdf and example.root with momentum distribution histograms

    momentum_up     - based on BMS zone (upstream) tracking detectors
    momentum_down   - based on ECAL/HCAL zone (downstream) tracking detectors


GenFit usage
------------

1. Build GenFit (only once)

  $ cd p348reco
  $ make genfit-install

  (One could need to `unset CC`, otherwise cmake does not properly find compiler)
  
  For installation on local machine additional packages are necessary:

  $ yum install git cmake boost-devel root-montecarlo-eg root-smatrix root-graf3d-eve root-gdml

2. Environment (for each new terminal session)

  $ export GENFIT=1

3. Compile examples

  $ make


GenFit sources
--------------

Web site:
  https://github.com/GenFit/GenFit

$ git clone https://github.com/GenFit/GenFit.git
$ cd GenFit
$ git checkout 2a8d6baf43b298cc4c27f7b35c2763bd462c537c

Old site:
  https://sourceforge.net/projects/genfit/
  https://sourceforge.net/p/genfit/code/1883/log/

  SVN revision 1883 is the latest available at sourceforge.net
  and corresponds to commit 2a8d6baf43b298cc4c27f7b35c2763bd462c537c at github.com.


DQM usage
---------

Harvest data:

  $ ./dqm.exe /eos/experiment/na64/data/cdr/cdr01001-002449.dat cdr01001-002449.txt
  $ ./dqm.exe /eos/experiment/na64/data/cdr/cdr01002-002449.dat cdr01002-002449.txt
  
Compare:

  $ ./cqm.exe cdr01001-002449.txt cdr01002-002449.txt
