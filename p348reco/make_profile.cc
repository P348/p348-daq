// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "TString.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"
#include "timecut.h"
#include "badburst.h"
#ifdef TRACKINGTOOLS
#include "tracking_new.h"
#endif


bool inRange(const double x, const double min, const double max) {
  return (min <= x) && (x <= max);
}


 //cut on straw multiplicity to clean data
    //Straw multiplicity: from A.Toropin
bool CheckStrawHits2022B(const CS::DaqEventsManager& manager){
  
  int mpStraw4X = 0;
  int mpStraw4Y = 0;
  int mpStraw3X = 0;
  int mpStraw3Y = 0;
  
  for (auto &d_it : manager.GetEventDigits()) {
    const Chip::Digit *digit = d_it.second;
    const ChipNA64TDC::Digit *tdc = dynamic_cast<const ChipNA64TDC::Digit*>(digit);
    //if (tdc) std::cout << " tdc: " << tdc << std::endl;
    if (tdc) {
      if (tdc->GetDetID().GetName() == "ST04X" && tdc->GetTime() < 920 && tdc->GetTime() > 840 && tdc->GetWire() != 64) {
        mpStraw4X++;
      }
      if (tdc->GetDetID().GetName() == "ST04Y" && tdc->GetTime() < 920 && tdc->GetTime() > 840 && tdc->GetWire() != 64) {
        mpStraw4Y++;
      }
      if (tdc->GetDetID().GetName() == "ST03X" && tdc->GetTime() < 920 && tdc->GetTime() > 840 && tdc->GetWire() != 64) {
        mpStraw3X++;
      }
      if (tdc->GetDetID().GetName() == "ST03Y" && tdc->GetTime() < 920 && tdc->GetTime() > 840 && tdc->GetWire() != 64) {
        mpStraw3Y++;
      }
    }
  }
  
  bool ret  =true;
  if ((mpStraw3X <= 0) || (mpStraw3X > 5) || (mpStraw3Y <= 0) || (mpStraw3Y > 5)) ret = false;
  return ret;
}


int main(int argc, char *argv[]) {
  if (argc < 2) {
    cerr << "Usage: ./make_profile.exe [optDiMuFlag] file1 [file2 ...]" << endl;
    cerr << "optDiMuFlag can be 0 or 1. If it is not set, then it is considered =0 " << endl;
    return 1;
  }

  // cuts for selecting profile
  const unsigned int nclu_cut = 1; //number of hits recorded in micromegas used for the extrapolation
  const double time_sigma = 3.;
  const double bad_ECAL_cut = 5.;  

  string isDiMuFlag = argv[1];
  int flag_dimuons = 0;
  int ifirst = 1;
  if (isDiMuFlag.find(".dat") == std::string::npos) {
    flag_dimuons = atoi(argv[1]);
    ifirst = 2;
  }
  
  cout << "INFO: mode flag_dimuons = " << flag_dimuons << endl;
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = ifirst; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();

  string output_file_name;
  if (flag_dimuons == 0) {
    output_file_name = "profile_calib.root";
  } else if (flag_dimuons == 1) {
    output_file_name = "profile_dimuons.root";
  }
  TFile *file_profile = new TFile(output_file_name.c_str(), "RECREATE");

  //VARIABLE RELEVANT TO SHOWER PROFILE
  //size of the spot where the profiles are selected
  const double spot_size = 20.; //mm
  //size of the bin of the profile, should not go under 0.3 mm to not exceed MM precision in extrapolate the line
  const double binwidth = 0.666666666666;
  const int Nbin_profile = 2. * spot_size / binwidth;

  // Percentage of energy fraction in cell
  const double norm = 100.;

  //A.C. parameters for the energy part
  //const int Nbin_profile_vsE = 2*spot_size;
  const int nEnergy = 6;
  const double binErange[7] = { 0., 20., 30., 40., 50., 60., 100. };

  const int nRadius = 20;
  double *binRrange = new double[nRadius + 1];
  for (int i = 0; i < (nRadius + 1); i++) {
    binRrange[i] = -spot_size + i * 2 * spot_size / nRadius;
  }
  
  /*      define TProfile2D to fill, The profile have their center in the same center of the beam spot on the ECAL surface and they express
  the percentage of the total energy recorded by the ECAL deposited in each cell   */
  ////  chi-square distribution due to the large fluctuation of low energy deposit in the cells ////  
  //// A.C. also add the TProfile3D, where the "Z" axis is the total energy in ECAL1
  TProfile2D* Shower_Profiles[6][6];
  TProfile2D *Shower_Profiles_Tor[6][6];
  TProfile3D *Shower_Profiles_vsEnergy[6][6];
  
  TH1D *hEneNorm = new TH1D("hEneNorm","hEneNorm",200,0,120); //Histogram to check the energy of the events used to fill the profile
  
  for (int x = 0; x < 6; ++x)
    for (int y = 0; y < 6; ++y) {
      TString name;
      TString title;
      name.Form("ECAL1-%i-%i", x, y);
      title.Form("ECAL1 energy fraction deposited in cell ECAL1-%i-%i;x,mm;y,mm;fraction", x, y);
      Shower_Profiles[x][y] = new TProfile2D(name, title, Nbin_profile, -spot_size, spot_size, Nbin_profile, -spot_size, spot_size, 0, 100);

 name.Form("ECAL1-%i-%i-full", x, y);
      title.Form("ECAL1 energy deposited in cell ECAL1-%i-%i;x,mm;y,mm;energy,GeV", x, y);
      Shower_Profiles_Tor[x][y] = new TProfile2D(name, title, Nbin_profile, -spot_size, spot_size, Nbin_profile, -spot_size, spot_size, 0, 100);
      
      name.Form("ECAL1-%i-%i-vsEnergy", x, y);
      title.Form("ECAL1 energy fraction deposited in cell ECAL1-%i-%i;x,mm;y,mm;ECAL1 energy,GeV;fraction", x, y);
      Shower_Profiles_vsEnergy[x][y] = new TProfile3D(name, title, nRadius, binRrange, nRadius, binRrange, nEnergy, binErange);
      Shower_Profiles_vsEnergy[x][y]->BuildOptions(0, 100, ""); //set the range for "T", the variable that is accumulated
    
    }

  // event loop, read event by event
  int NAccepted = 0;
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();

    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;

    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();

    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }

    // run reconstruction
    RecoEvent e = RunP348Reco(manager);

    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics) continue;
    
    // process beam only if >2022B
    if(e.run>=8663){
      if (!e.isTriggerBeamOnly()) continue;
    }
    
    //bad spill check
    if (IsBadBurst(e.run, e.spill)) continue;

    // get ECAL incoming track line
    TVector3 p1, p2;
    bool Clustercut(true);
    TVector3 P_ecal;
  
    if (e.run < 4642) {
      p1 = e.MM3.abspos();
      p2 = e.MM4.abspos();
      Clustercut = e.MM3.xplane.clusters.size() == nclu_cut && e.MM3.yplane.clusters.size() == nclu_cut &&
                   e.MM4.xplane.clusters.size() == nclu_cut && e.MM4.yplane.clusters.size() == nclu_cut;
      // extrapolate track line on the ECAL surface
      P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    } else if (4642 <= e.run && e.run <= 5187) {
      p1 = e.MM5.abspos();
      p2 = e.MM6.abspos();
      Clustercut = e.MM5.xplane.clusters.size() == nclu_cut && e.MM5.yplane.clusters.size() == nclu_cut &&
                   e.MM6.xplane.clusters.size() == nclu_cut && e.MM6.yplane.clusters.size() == nclu_cut;
      // extrapolate track line on the ECAL surface
      P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    } else if (6035 <= e.run && e.run <= 8458) {
      p1 = e.MM3.abspos();
      p2 = e.MM4.abspos();
      Clustercut = e.MM3.xplane.clusters.size() == nclu_cut && e.MM3.yplane.clusters.size() == nclu_cut &&
                   e.MM4.xplane.clusters.size() == nclu_cut && e.MM4.yplane.clusters.size() == nclu_cut;
      // extrapolate track line on the ECAL surface
      P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    }
    else if(8663 <= e.run && e.run <= 9543){
      // get position of hit on ecal with genfit (2023)
#ifdef TRACKINGTOOLS
      const std::vector<Track_t> upstream = GetReconstructedTracks(e, kRegion::upstream);
      if (upstream.empty()) continue;
      
      //take the first track (LARGER P_VALUE)
      P_ecal = upstream[0].getECALHit();
#else
    throw std::runtime_error("ERROR: SP_GENFIT method required but no TRACKINGTOOLS defined!");     
#endif
    }
    const TVector3 showerpos = P_ecal - ECAL0BEAMSPOT_pos;

    const RecoEvent e0 = e;
    // zero energy of out-of-time cells
    timecut(e, time_sigma);
  
    //prepare booleans of the cuts

    //check on bad_ene VS in_time: if out-of-time energy > bad_ECAL_cut, reject the event
    const bool badECALcut = (e0.ecalTotalEnergy(1) - e.ecalTotalEnergy(1))<bad_ECAL_cut;
    
    double srd = 0;
    for (int isrd = 0; isrd < 3; isrd++) {
      srd += (e.SRD[isrd].energy) / MeV;
    }
    const bool SRDcut = inRange(srd, 10., 200.);

    //"Old" SRD CUTS
    /*
     const double srd_cut[] = {1.*MeV, 70.*MeV};
     const bool SRDcut = e.SRD[0].energy > srd_cut[0] && e.SRD[0].energy < srd_cut[1] &&
     e.SRD[1].energy > srd_cut[0] && e.SRD[1].energy < srd_cut[1] &&
     e.SRD[2].energy > srd_cut[0] && e.SRD[2].energy < srd_cut[1];
     */
   
    const bool Strawcut = CheckStrawHits2022B(manager);

    //Pileup cut
    const Cell &ecentral = e.ECAL[1][ECAL0BEAMCELL.ix][ECAL0BEAMCELL.iy];
    const bool hasPileup = ecentral.pileup() < 5.;

    // dimuons selection:
    const CellXY emcid = e.ecalMaxCell();
    const double ecal0 = e.ecalTotalEnergy(0);
    const double ecal1 = e.ecalTotalEnergy(1);
    const double hcal0 = e.hcalTotalEnergy(0);
    const double hcal1 = e.hcalTotalEnergy(1);
    const double ecal1limit = binErange[nEnergy - 1]; // 60 GeV
  
    const bool isDimuon = (emcid == ECAL0BEAMCELL) &&    // beam is in ECAL central cell
        (ecal0 > 0.5) && (ecal1 < ecal1limit) && inRange(hcal0, 2., 8.25) &&   // double-MIP in HCAL0
        inRange(hcal1, 2., 8.25);     // double-MIP in HCAL1

    const bool isEle100 = (ecal1 > ecal1limit);

    //check if all condition are satisfied
    const bool cutAll = Clustercut && SRDcut && hasPileup && Strawcut && badECALcut;
    if (!cutAll) continue;

    NAccepted++;
    hEneNorm->Fill(ecal1); //fill histogram for energy check
    
    // fill the shower profiles
    for (int x = 0; x < 6; x++) {
      for (int y = 0; y < 6; y++) {
        if (isDimuon && flag_dimuons) {
          Shower_Profiles_vsEnergy[x][y]->Fill(showerpos.X(), showerpos.Y(), ecal1, norm * e.ECAL[1][x][y].energy / ecal1);
        } else if (!flag_dimuons) {
          Shower_Profiles[x][y]->Fill(showerpos.X(), showerpos.Y(), norm * e.ECAL[1][x][y].energy / ecal1);
          Shower_Profiles_Tor[x][y]->Fill(showerpos.X(), showerpos.Y(), e.ECAL[1][x][y].energy);
          if(isEle100) Shower_Profiles_vsEnergy[x][y]->Fill(showerpos.X(), showerpos.Y(), ecal1, norm * e.ECAL[1][x][y].energy / ecal1);
        }
      }
    }
  }  //end event manager looping

  std::cout << "Number of accepted events = " << NAccepted << std::endl;

 if (!flag_dimuons) {

    // Determine central cell by looking at the cell with most energy
    double totalenergy = -1;
    CellXY maxcell = {-1, -1};
    for (int x = 0; x < 6; x++) {
      for (int y = 0; y < 6; y++) {
        double tot = Shower_Profiles[x][y]->Integral();
        if (tot > totalenergy) {
          totalenergy = tot;
          maxcell = {x, y};
        }
      }
    }

    // sanity check - the central cell is in expected position
    if (!(maxcell == ECAL0BEAMCELL)) {
      std::cerr << "ERROR: Max cell of shower profile:"
                << " expected = " << ECAL0BEAMCELL.ix << " " << ECAL0BEAMCELL.iy
                << " observed = " << maxcell.ix << " " << maxcell.iy
                << std::endl;
      throw std::runtime_error("ERROR: max cell position mismatch");
    }
  }

  int NBadCentral = CheckProfileStatistics(Shower_Profiles, 16.);
  //std::cout << "Nbins with poor statistics central part = " << NBadCentral << std::endl;

  DumpProfile(Shower_Profiles);

  TVector3 ProfileCenter = GetProfileCenter(Shower_Profiles);
  cout << "ecal calocoord x = " << ProfileCenter.X() << " y = " << ProfileCenter.Y() << endl;

  // Add list of parameters
  file_profile->cd();
  TList *parlist = new TList();
  parlist->Add(new TParameter<int>("pcenterX", ECAL0BEAMCELL.ix, 'f'));
  parlist->Add(new TParameter<int>("pcenterY", ECAL0BEAMCELL.iy, 'f'));
  parlist->Add(new TParameter<int>("dimX", ECAL_pos.Nx, 'f'));
  parlist->Add(new TParameter<int>("dimY", ECAL_pos.Ny, 'f'));
  parlist->Add(new TParameter<double>("norm", norm, 'f'));
  parlist->Add(new TParameter<double>("NominalBeamEnergy", NominalBeamEnergy, 'f'));
  parlist->Add(new TParameter<double>("ECAL0BEAMSPOT_posX", ECAL0BEAMSPOT_pos.X() ,'f'));
  parlist->Add(new TParameter<double>("ECAL0BEAMSPOT_posY", ECAL0BEAMSPOT_pos.Y() ,'f'));
  
  parlist->Write("parameters", TList::kSingleKey);
  
  // save all histograms to .root file
  file_profile->Write();
  file_profile->Close();

  return 0;
}
