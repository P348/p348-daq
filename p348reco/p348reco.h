#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <memory>
#include <stdexcept>
#include <cmath>

#include "DaqEventsManager.h"
#include "DaqEvent.h"
#include "Chip.h"
#include "ChipSADC.h"
#include "ChipAPV.h"
#include "ChipNA64TDC.h"
#include "ChipNA64WaveBoard.h"
//using namespace CS;
//root
#include <TLorentzVector.h>

#include "conddb.h"
#include "GEMReco.h"
#include "mm.h"
#include "straw.h"

#include "na64/reco/sadc.hh"
#include "na64/reco/time-clustering.hh"

// check the value is inside window  [min, max]
double coerce(const double& x, const double& min, const double& max)
{
  if (x < min) return min;
  if (x > max) return max;
  return x;
}

// check two numbers are equal within the error
bool fuzzyEquals(const double x, const double y, const double eps = 1e-3)
{
  return (fabs(x - y) < eps);
}

struct WavePeak {
  int index;
  double t0;
};

struct Cell {
  Cell()
  : amplitude(0), amplitude_sample(0), amplitude_peak_index(0),
    energy(0), t0(0),tTW(0),
    pedestal0(0), pedestal1(0),
    hasDigit(false),
    calib(0)
  {};
  
  double amplitude;
  int amplitude_sample;
  int amplitude_peak_index;
  double energy;
  double t0,tTW;
  double pedestal0;
  double pedestal1;

  std::vector<na64util::iMSADCReco::PeakInfo> fittedPeaksInfo;
  
  double pedestal() const { return 0.5 * (pedestal0 + pedestal1); }
  
  double t0ns() const {
    return calib->adc.sampling_interval * t0;
  }
  
  double texpected(const float master) const {
    double retT=calib->texpected(master);

    //tTW is the eventual contribution of the time-walk correction. If time-walk correction not used, it is zero.
    retT=retT+tTW;

    return retT;
  }

  bool hasDigit;
  
  const CaloCalibData* calib;
  
  std::vector<CS::uint16> raw;
  
  std::vector<int> peaks;
  
  // check for overflow in waveform
  bool hasOverflow() const
  {
    for (size_t i = 0; i < raw.size(); ++i)
      if (raw[i] == calib->adc.maxADC) return true;
    
    return false;
  }
  
  // return pedestal-compensated waveform
  double wave(const size_t i) const
  {
    const double& ped = (i % 2 == 0) ? pedestal0 : pedestal1;
    const CS::uint16& A = raw[i];

    if (!calib->adc.isWB){//SADC case: the digitized signal for an analogue signal with negative polarity is positive (SADC inverts the signal)
      return (A > ped) ? (A - ped) : 0;
    }
    else{ //WB case: the signal for an analogue signal with negative polarity is negative (WB does not invert the signal)
      return (ped-A); //A.C. check if set to zero
    }
  }
  
  void calcPedestals()
  {
      if (calib->adc.isWB){
        this->calcPedestalsWB();
        return;
      }
      // calculate pedestals
      // odd and even time samples are captured by two distinct ADC chips to achive double sampling rate
      // in result two pedestals should be calculated
      const int pedlen = calib->adc.pedestal_length;
      double ped0 = 0;
      double ped1 = 0;
      
      for (int i = 0; i < pedlen; ++i) {
        if (i % 2 == 0) ped0 += raw[i];
        else ped1 += raw[i];
      }
      
      ped0 /= pedlen/2;
      ped1 /= pedlen/2;
      
      // check min/max in pedestal area
      CS::uint16 ped0min = calib->adc.maxADC;
      CS::uint16 ped1min = calib->adc.maxADC;
      CS::uint16 ped0max = 0;
      CS::uint16 ped1max = 0;
      
      for (int i = 0; i < pedlen; ++i) {
        const CS::uint16& A = raw[i];
        CS::uint16& min = (i % 2 == 0) ? ped0min : ped1min;
        CS::uint16& max = (i % 2 == 0) ? ped0max : ped1max;
        
        if (A < min) min = A;
        if (A > max) max = A;
      }
      
      // check pile-up condition and correct pedestal level
      const CS::uint16 pileup_level = 20;
      const bool ped0pileup = (ped0max - ped0min > pileup_level);
      const bool ped1pileup = (ped1max - ped1min > pileup_level);
      
      if (ped0pileup || ped1pileup) {
        // use minimum sample of raw waveform as estimation of pedestal level
        for (size_t i = pedlen; i < raw.size(); ++i) {
          const CS::uint16& A = raw[i];
          CS::uint16& min = (i % 2 == 0) ? ped0min : ped1min;
          
          if (A < min) min = A;
        }
        
        if (ped0pileup) ped0 = ped0min;
        if (ped1pileup) ped1 = ped1min;
      }
      
      pedestal0 = ped0;
      pedestal1 = ped1;
  }
  

  //WB code - only one pedestal
  void calcPedestalsWB(){
    const int pedlen = calib->adc.pedestal_length;
    double ped = 0;


    for (int i = 0; i < pedlen; ++i) {
       ped += raw[i];
    }
    ped /= pedlen;

    // check min/max in pedestal area
    CS::uint16 pedmin = calib->adc.maxADC;
    CS::uint16 pedmax = 0;

    for (int i = 0; i < pedlen; ++i) {
      const CS::uint16& A = raw[i];
      CS::uint16& min =  pedmin;
      CS::uint16& max =  pedmax;

      if (A < min) min = A;
      if (A > max) max = A;
    }

    // check pile-up condition and correct pedestal level
    const CS::uint16 pileup_level = 40;
    const bool pedpileup = (pedmax - pedmin > pileup_level);

    if (pedpileup) {
      // use maximum sample of raw waveform as estimation of pedestal level (WB raw data does not invert pulse polarity)
      for (size_t i = pedlen; i < raw.size(); ++i) {
        const CS::uint16& A = raw[i];
        CS::uint16& max = pedmax;
        if (A > max) max = A;
      }
      ped = pedmax;
    }

    //A.C. irrelevant, but set both so that I do not have to change the Pedestal() method
    pedestal0 = ped;
    pedestal1 = ped;

  }


  // detect all peaks in waveform
  void searchPeaks()
  {
    peaks.clear();
    
    const size_t nwave = raw.size();
    
    for (size_t i = 0; i < nwave; ++i) {
      const double A0 = (i > 0) ? wave(i-1) : wave(1);
      const double A1 = wave(i);
      const double A2 = (i+1 < nwave) ? wave(i+1) : wave(nwave-2);
      
      const bool riseOnLeft = (A1 >= A0);
      const bool fallOnRight = (A1 >= A2);
      const bool isPlateau = (A1 == A0 && A1 == A2);
      const bool isPeak = !isPlateau && riseOnLeft && fallOnRight;
      
      if (isPeak) peaks.push_back(i);
    }
  }
  
  void selectPeak(const int method, const double master)
  {
    const int i_peak = peak_Best(method, master);

    // energy
    amplitude = peakamplitude(i_peak);
    energy = amplitude * calib->K;

    // timing
    t0 = peaktime(i_peak);
    tTW = 0; //by default, no time-walk correction
    amplitude_sample = peaksample(i_peak);
    amplitude_peak_index = i_peak;

    //In case the fit is enabled, check the fitted peak that has the best timing.
    //If the fit result does not exists, reset the energy and time to 0
    if (calib->doWaveformFit){
      amplitude = 0;
      energy = 0;
      t0 = 0;
      amplitude_sample = 0;
      const na64util::iMSADCReco::PeakInfo * pi = peak_Best_fitted(method,master);
      if (pi) {
          amplitude = pi->rawAmp;
          energy = amplitude * calib->K;
          t0 = pi->time/calib->adc.sampling_interval; //the cell t0 has to be in unit of samples
          amplitude_sample = pi->sampleNo;
       }
    }
  }
  
  int peak_Best(const int method, const double master) const
  {
    if (method == kPeak_BestTiming) {
      const int n = peak_BestTiming(master);
      if (n >= 0) return n;
    }
    
    /*
    if (method == kPeak_MaxAmplitude) {
      return peak_MaxAmplitude();
    }
    */
    
    // fallback
    return peak_MaxAmplitude();
  }

  // search for peak with maximum amplitude
  int peak_MaxAmplitude() const
  {
      double A_max = 0;
      int i_max = -1;
      
      const double raise_thr = calib->minPeakRaisingSlopeSpeed;
      const bool raiseCheck = (raise_thr > 0);

      for (size_t i = 0; i < peaks.size(); ++i) {
        const size_t pos = peaks[i];

        const bool inRange = (calib->adc.signal_range_begin <= pos && pos < calib->adc.signal_range_end);
        if (!inRange) continue;

        // check peak rising edge
        if (raiseCheck && (pos > 0) && (wave(pos-1) < raise_thr*wave(pos))) continue;

        const double A = wave(pos);
        
        if (A > A_max) { A_max = A; i_max = i; }
      }
      
      return i_max;
  }
  
  // search for peak with timing closest to expected
  int peak_BestTiming(const double master) const
  {
    // skip if expected timing is unknown
    if (!calib->hasTcalib()) return -1;
    
    // skip if master time is not defined
    if (calib->isTmeanRelative && (master == 0)) return -1;
    
    double dt_min = 1e6;
    int i_best = -1;
    
    const double texpected = this->texpected(master);
    
    const double raise_thr = calib->minPeakRaisingSlopeSpeed;
    const bool raiseCheck = (raise_thr > 0);
    
    for (size_t i = 0; i < peaks.size(); ++i) {
      const size_t pos = peaks[i];
      
      // check the peak is situated inside signal zone
      const bool inRange = (calib->adc.signal_range_begin <= pos && pos < calib->adc.signal_range_end);
      if (!inRange) continue;

      // check peak rising edge
      if (raiseCheck && (pos > 0) && (wave(pos-1) < raise_thr*wave(pos))) continue;
      
      const double peakt0ns = calib->adc.sampling_interval * peaktime(i);
      const double dt = fabs(peakt0ns - texpected);
      
      if (dt < dt_min) { dt_min = dt; i_best = i; }
    }

    return i_best;
  }

  const na64util::iMSADCReco::PeakInfo * peak_Best_fitted(const int method, const double master) const
   {
     if (method == kPeak_BestTiming)
       return peak_BestTiming_fitted(master);
     else
       return peak_MaxAmplitude_fitted();
   }

  // search for peak with maximum amplitude
  const na64util::iMSADCReco::PeakInfo * peak_MaxAmplitude_fitted() const
  {
    double A_max = 0;

    const na64util::iMSADCReco::PeakInfo * bestFittedPeak = nullptr;
    for(const auto & fittedPeak : fittedPeaksInfo) {
        if(!fittedPeak.are_amp_and_time_valid()) continue;
        if (!fittedPeak.fitFunctionUserdata) continue;
        double A = fittedPeak.rawAmp;
        if (A<A_max) continue;
        A_max = A;
        bestFittedPeak = &fittedPeak;
    }

    return bestFittedPeak;
  }


  // search for peak with timing closest to expected
  const na64util::iMSADCReco::PeakInfo * peak_BestTiming_fitted(const double master) const
  {
    // skip if expected timing is unknown
    if (!calib->hasTcalib()) return nullptr;
    
    // skip if master time is not defined
    if (calib->isTmeanRelative && (master == 0)) return nullptr;
    
    const double texpected = this->texpected(master);
    
    double tMin = std::numeric_limits<double>::infinity();
    const na64util::iMSADCReco::PeakInfo * bestFittedPeak = nullptr;
    for(const auto & fittedPeak : fittedPeaksInfo) {
        if(!fittedPeak.are_amp_and_time_valid()) continue;
        if (!fittedPeak.fitFunctionUserdata) continue;
        double tDelta = fabs(texpected - fittedPeak.time);
        if(tDelta > tMin) continue;
        tMin = tDelta;
        bestFittedPeak = &fittedPeak;
    }
    
    return bestFittedPeak;
  }
  
  // calculate peak timing
  double peaktime(const int idx) const
  {
    if (idx < 0) return 0;
    
    // peak position
    const int sample = peaks[idx];
    
    if (SADC_t0_method == kT0_MaxSample) return sample;
    
    if (SADC_t0_method == kT0_CenterOfMass){
      return t0_CenterOfMass(calib->adc.signal_range_begin,calib->adc.signal_range_end);
    }
    
    if (SADC_t0_method == kT0_RisingEdge) return t0_RisingEdge(sample);
    
    if (SADC_t0_method == kT0_RisingEdgeHH) return t0_RisingEdge(sample);
    
    return 0;
  }
  
  double peakamplitude(const int idx) const
  {
    if (idx < 0) return 0;
    const int sample = peaks[idx];
    return wave(sample);
  }
  
  int peaksample(const int idx) const
  {
    if (idx < 0) return 0;
    return peaks[idx];
  }
  
  int t0_MaxSample(int sample_min, int sample_max) const
  {
    int imax = 0;
    double Amax = 0;
    sample_min = max(sample_min, (int)calib->adc.pedestal_length);
    sample_max = min(sample_max, (int)raw.size());
    
    for (int i = sample_min; i < sample_max; ++i) {
      const double A = wave(i);
      if (A > Amax) { Amax = A;  imax = i; }
    }
    
    return imax;
  }
  
  double A_MaxSample(int sample_min, int sample_max) const
  {
    double Amax = 0;
    sample_min = max(sample_min, (int)calib->adc.pedestal_length);
    sample_max = min(sample_max, (int)raw.size());
    
    for (int i = sample_min; i < sample_max; ++i) {
      const double A = wave(i);
      if (A > Amax) Amax = A;
    }
    
    return Amax;
  }
  
  // compute waveform center-of-mass in the sample range [min, max)
  double t0_CenterOfMass(const int sample_min, const int sample_max) const
  {
    double Sa = 0;
    double Sax = 0;
    
    for (int i = sample_min; i < sample_max; ++i) {
      const double A = wave(i);
      Sa  += A;
      Sax += A * i;
    }
    
    return (Sa != 0.) ? Sax / Sa : 0;
  }
  
  double t0_RisingEdge(const int i_max) const
  {
      // TODO: slope_len should be limited by the length of rising edge
      
      // search for rising edge
      //   https://indico.cern.ch/event/569549/contributions/2302932/
      // run the search in the backward direction from `i_max` to ensure the
      // t0 edge corresponds to `A_max` sample
      const double A_max = wave(i_max);
      const double A_half = 0.5*A_max;
      double A_min = A_max;
      const double noise = 1.;
      
      for (int i = i_max-1; i > 0; --i) {
        const double wi = wave(i);
        
        // check the rising edge is end
        if (wi > A_min + noise) break;
        if (A_min < wi) A_min = wi;
        
        // check the 0.5*A_max cross condition
        const bool cross05 = (wi <= A_half);
        if (!cross05) continue;
        
        // check the segment is rising
        const double wi1 = wave(i+1);
        const bool rising = (wi < wi1);
        if (!rising) continue;
        
        // calculate intersection of the rising edge with time axis to get origin time
        const double t0_rise = (i+1) - wi1/(wi1 - wi);
        
        if (SADC_t0_method == kT0_RisingEdgeHH) {
          const double t0_hh = t0_rise + A_half/(wi1 - wi);
          return t0_hh;
        }
        
        return t0_rise;
      }
      
      return 0.;
  }
  
  /*
  double t0_PolFit() const
  {
      // fit max amplitude region with parabola to obtain precise time and amp
      // y=a*x**2 + b*x +c ;
      // x_max=-b/(2*a);
      // y_max=(4*a*c-b*b)/(4*a) ;
      
      double a_par,b_par,c_par;
      double x1,x2,x3;
      double y1,y2,y3;
      int i1,i2,i3;
      
      i1=i_max-1; i2=i_max;  i3=i_max   + 2;
      
      x1=i1;       x2=i2;       x3=i3;
      y1=wave[i1]; y2=wave[i2]; y3=wave[i3];
      
      a_par=(y3-(x3*(y2-y1)+x2*y1-x1*y2)/(x2-x1))/(x3*(x3-x1-x2)+x1*x2);
      b_par=a_par*(x1+x2)-(y2-y1)/(x2-x1);
      c_par=(x1*y2-x2*y1)/(x1-x2) + a_par*x2*x1 ;
      
      double time_mean0 = 0;
      double y_max = 0;
      
      if(a_par != 0) {
        time_mean0 = b_par/(2.*a_par);
        y_max = (4.0*a_par*c_par - b_par*b_par)/(4.0*a_par);
      }
  }
  */
  
  // return which part of waveform situated inside the window [t0mean - t0sigma, t0mean + t0sigma]
  // initial time samples (pedestal) are not taken into account
  double inTimeWindowRatio(const double& t0mean, const double& t0sigma) const
  {
    if (!hasDigit) return 0.;
    
    double Stotal = 0;
    double Swindow = 0;
    const double Wmin = t0mean - t0sigma;
    const double Wmax = t0mean + t0sigma;

    for (size_t i = calib->adc.pedestal_length; i < raw.size(); ++i) {
      const double& ped = (i % 2 == 0) ? pedestal0 : pedestal1; //ok also for WB
      const double A = raw[i] - ped;
      
      Stotal += A;
      
      // use straight line interpolation to sum partial samples (on the edges of [Wmin, Wmax])
      const double left = coerce(i - 0.5, Wmin, Wmax);
      const double right = coerce(i + 0.5, Wmin, Wmax);
      
      Swindow += (right - left) * A;
      
      /*
      std::cout << "inTimeWindowRatio() "
                << std::setw(2) << i
                << " " << std::setw(7) << A
                << " " << left << " " << right
                << " " << (right - left) << endl;
      */
    }
    
    return (Swindow / Stotal);
  }
  
  // return best peak time offset to the master in units of channel time stddev.
  double timediff(const double master) const
  {
    if (!hasDigit) return 9999.;
    
    return (t0ns() - this->texpected(master)) / calib->tsigma;
  }
  
  // calculate contribution of pile-up tails to the best peak energy
  double pileup() const
  {
    if (!hasDigit) return 0.;
    
    double sumtail = 0.;
    
    // loop over all pileup peaks
    for (int i = 0; i < amplitude_peak_index; i++) {
      const double distance = peaksample(i) - amplitude_sample; // distance is negative
      const double tail = peakamplitude(i) * exp(distance/calib->adc.pulse_shape_decay);
      sumtail += tail;
      
      // TODO: each peakamplitude(i) should be corrected for pileup from former peaks
    }
    
    // recovery calibration factor
    const double K = (amplitude != 0) ? energy / amplitude : 0.;
    
    // TODO: for now is not possible to use calibration from .calib structure as
    //       LED corrections are not included into .calib->K
    
    // TODO: better K estimation for cases of no signal peak (i.e. energy=0, amplitude=0),
    //       althrough pileup is not clearly defined in this case
    
    // convert amplitude -> energy units
    sumtail *= K;
    
    return sumtail;
  }

  double energy_nopileup() const
  {
    const double nopileup = energy - pileup();
    return (nopileup > 0.) ? nopileup : 0.;
  }

// calculate contribution of pile-up in terms of amplitude ratio
// Note: adjustments originally implemented in branch https://gitlab.cern.ch/P348/p348-daq/-/blob/eth-SRDlyso-reco/p348reco/p348reco.h?ref_type=heads#L492
  double pileup_ratio( double threshold = 50. ) const 
  {
    if (!hasDigit) return 1.;

    double ratio = 0.;
    double sum_amplitudes = 0.;
    double best_amplitude = (amplitude > threshold) ? amplitude : 0.;

    // loop over all pileup peaks and add amplitudes
    for (size_t i = 0; i < peaks.size(); ++i) {
      if(peakamplitude(i) > threshold) sum_amplitudes += peakamplitude(i);
    }

    // calculate ratio of best peak amplitude vs all amplitudes
    ratio = (sum_amplitudes > 0) ? best_amplitude / sum_amplitudes : 1.;

    return ratio;
  }

  // calculate time difference between best time and max amplitude peaks in case of pile-up
  double pileup_timediff_ns(const double master) const
  {
    double timediff = std::nan("");

    if (!hasDigit) return timediff;

    // get time difference between max amplitude and best timing peaks
    const int i_bestTime = peak_BestTiming(master);
    const int i_maxAmp  = peak_MaxAmplitude();
    const double amp_diff = peakamplitude(i_bestTime) - peakamplitude(i_maxAmp);

    // skip if peak search fails, an index of -1 is returned
    if (i_bestTime < 0 || i_maxAmp < 0) return timediff;
    // return exact 0 if maxAmp and bestTime are the same peak
    if (i_bestTime == i_maxAmp) return 0.;
    // skip if any of the peak's amplitude is unphysical
    if (peakamplitude(i_bestTime) <= 0 || peakamplitude(i_maxAmp) <= 0) return timediff;

    // compute time difference
    timediff = peaktime(i_bestTime) - peaktime(i_maxAmp);
    timediff *= calib->adc.sampling_interval;
    /*
    std::cout << "Time diff: " << std::endl
              << "i_bestTime=" << i_bestTime
              << " -- i_maxAmp=" << i_maxAmp
              << std::endl
              << "time_bestTime=" << peaktime(i_bestTime)
              << "time_maxAmp=" << peaktime(i_maxAmp)
              << std::endl
              << "amp_bestTime=" << peakamplitdue(i_bestTime)
              << "amp_maxAmp=" << peakamplitude(i_maxAmp)
              << std::endl;
    */

    return timediff;
  }
};

struct Hodo {
  Cell xplane[15];
  Cell yplane[15];
  
  std::vector<int> xhits;
  std::vector<int> yhits;
};

struct Hodo2 {
  Cell xplane[16];
  Cell yplane[16];
  
  std::vector<int> xhits;
  std::vector<int> yhits;
};

//FUNCTION FOR MONTECARLO
struct MChit {
  TVector3 pos;
  double deposit;  // Energy deposition, eV
  int particle;    // PDG ID
  int id;          // G4 track ID
  double energy;   // Particle kinetic energy, GeV
  double time;
  int IProj;       // Straw: projection measured by this plane
  double DistWire; // Straw: minimal distance from wire
  double CWire;    // Straw: wire coordinate in the projection measured
  int IWire;    // Straw: wire number (0-63)
  int IPlane;    // Straw: plane number (0-3)
  
// TODO: remove in favore of MChit(TVector3, ...)
MChit(double xpos_,double ypos_,double zpos_,double deposit_,int particle_,int id_,double energy_,double time_ = 0.,int IProj_=0,double DistWire_=0,double CWire_=0,int IWire_=0,int IPlane_=0):
 pos(TVector3(xpos_,ypos_,zpos_)),deposit(deposit_),particle(particle_),id(id_),energy(energy_),time(time_),IProj(IProj_),DistWire(DistWire_),CWire(CWire_),IWire(IWire_),IPlane(IPlane_)
{}

MChit(TVector3 pos_,double deposit_,int particle_,int id_,double energy_,double time_ = 0.,int IProj_=0,double DistWire_=0,double CWire_=0,int IWire_=0,int IPlane_=0):
 pos(pos_),deposit(deposit_),particle(particle_),id(id_),energy(energy_),time(time_),IProj(IProj_),DistWire(DistWire_),CWire(CWire_),IWire(IWire_),IPlane(IPlane_)
{}

MChit():
 pos(TVector3(0,0,0)),deposit(0),particle(0),id(0),energy(0),time(0),IProj(0),DistWire(0),CWire(0),IWire(0),IPlane(0)
{}

double strawStationWire() const { return 2*IWire+(IPlane+1)%2; }

};

void operator<<(ostream& stream,const MChit& mm)
{
 stream << "hit was caused by a particle with ID " << mm.particle << " with energy of: " << mm.energy << " and left an energy deposit of: " << mm.deposit << " hitting the Micromega in point: " <<
  "(" << mm.pos.x() << "," << mm.pos.y() << "," << mm.pos.z() << ") mm" << " WITH Geant4 ID: " <<mm.id <<  endl;
}

bool MChitByID(const MChit& a,const MChit &b)
{
 return (a.id < b.id);
}
bool MChitByDeposit(const MChit& a,const MChit &b)
{
 return (a.deposit > b.deposit);
}

struct MCTruth {
  double Weight;
  double BeamX0;
  double BeamY0;
  double BeamZ0;
  int BeamPDG;
  double BeamTotalEnergy;
  double BeamPx;
  double BeamPy;
  double BeamPz;
  double ECALEntryX;
  double ECALEntryY;

  //Dark photon official MC truth
  double DME0;          // Energy of electron before DM interaction  
  int DMParentID;      //PDG id of production particle
  double DME;          //Energy of DM
  TVector3 DMP;        //Position of DM production
  TVector3 DMD;        //Position of DM decay
  TLorentzVector DMPE; //Lorentz vector of electron in DM decay
  TLorentzVector DMPP; //Lorentz vector of positron in DM decay
  int DMTRID1;
  int DMTRID2;

  // convenience method to check DM simulation
  bool isDM() const { return (DME != -1 && DME0 != -1 && DMParentID != 0); };

  //truth information for MM
  vector<MChit>  MM1truth;
  vector<MChit>  MM2truth;
  vector<MChit>  MM3truth;
  vector<MChit>  MM4truth;
  vector<MChit>  MM5truth;
  vector<MChit>  MM6truth;
  vector<MChit>  MM7truth;
  vector<MChit>  MM8truth;
  vector<MChit>  MM9truth;
  vector<MChit>  MM10truth;
  vector<MChit>  MM11truth;
  vector<MChit>  MM12truth;
  vector<MChit>  MM13truth;
  //truth information for gems
  vector<MChit>  GEM1truth;
  vector<MChit>  GEM2truth;
  vector<MChit>  GEM3truth;
  vector<MChit>  GEM4truth; 
  //pixel information
  vector<MChit>  PX1truth; 
  vector<MChit>  PX2truth; 
  //Truth information of straw tubes
  vector<MChit>  ST1truth;
  vector<MChit>  ST2truth;
  vector<MChit>  ST3truth;
  vector<MChit>  ST4truth;
  vector<MChit>  ST5truth;
  vector<MChit>  ST6truth;

  vector<MChit>  ST11truth;
  vector<MChit>  ST12truth;
  vector<MChit>  ST13truth;
  //Random seeds
  vector<long> seeds;
  string seedsDumpFLUKA;

  //User-based output (see: https://gitlab.cern.ch/P348/p348-daq/-/issues/138)
  vector<string> userbank;

  vector<MChit> getMCTruth(const string& label) const {
    if( label == "MM01" ) return MM1truth;
    if( label == "MM02" ) return MM2truth;
    if( label == "MM03" ) return MM3truth;
    if( label == "MM04" ) return MM4truth;
    if( label == "MM05" ) return MM5truth;
    if( label == "MM06" ) return MM6truth;
    if( label == "MM07" ) return MM7truth;
    if( label == "MM08" ) return MM8truth;
    if( label == "MM09" ) return MM9truth;
    if( label == "MM10" ) return MM10truth;
    if( label == "MM11" ) return MM11truth;
    if( label == "MM12" ) return MM12truth;
    if( label == "MM13" ) return MM13truth;
    // GEMs
    if( label == "GM01" ) return GEM1truth;
    if( label == "GM02" ) return GEM2truth;
    if( label == "GM03" ) return GEM3truth;
    if( label == "GM04" ) return GEM4truth;
    // Straws
    if( label == "ST01" ) return ST1truth;
    if( label == "ST02" ) return ST2truth;
    if( label == "ST03" ) return ST3truth;
    if( label == "ST04" ) return ST4truth;
    if( label == "ST05" ) return ST5truth;
    if( label == "ST06" ) return ST6truth;
    if( label == "ST11" ) return ST11truth;
    if( label == "ST12" ) return ST12truth;
    if( label == "ST13" ) return ST13truth;
    throw std::runtime_error("RecoEvent::getMCTruth() incorrect label ID\n");
  }

};

struct RecoEvent {
  // event id
  CS::uint32 run;
  CS::uint32 runevent;
  CS::uint32 spill;
  CS::uint32 spillevent;
  
  // event id string "run-spill-spillevent"
  std::string id() const
  {
    std::ostringstream oss;
    oss << run << "-" << spill << "-" << spillevent;
    return oss.str();
  }
  
  // event type:
  bool isCalibration;    // event source is the LED calibration system
  bool isPhysics;        // event source is the detector trigger system
  
  bool isOnSpill;        // event runs during the burst interval
  
  // trigger type of `isPhysics` events:
  bool isTriggerPhysics;        // physics event definition: for example +S0+S1+(ECAL<70)
  bool isTriggerRandom;         // random definition, triggered by the noise generator
  bool isTriggerBeam;           // beam telescope definition: for example +S0+S1
  // MUON MODE TRIGGERS
  bool isTriggerECAL;           // ECAL trigger: beam trigger + missing energy in ECAL
  bool isTriggerMuonInv;        // Muon to invisible: Beam + BK and S4 in anti-coincidence
  // the convenience to suppress the events subset of "isTriggerPhysics" type
  bool isTriggerBeamOnly() const { return isTriggerBeam && (!isTriggerPhysics) && (!isTriggerMuonInv) && (!isTriggerECAL); }
  bool isTriggerMuonInvOnly() const { return isTriggerMuonInv && (!isTriggerPhysics) && (!isTriggerECAL); }
  
  // timing, interval between event trigger and TCS clock,
  // useful for phase shift compensation in SADC
  double masterTime;
  float tdct0_phase;
  double WB_phase; //time reference for WB when used with local 125 MHz clock from DHMux
  
  // event contents
  Cell ECAL[2][12][6];
  Cell HCAL[4][6][3];
  Cell BGO[8];
  Cell VETO[6];
  Cell MUON[4];
  Cell ECALSUM[6];
  Cell ATARG[5][5];
  Cell SRD[4];
  Cell LYSO[100];
  Cell S0;
  Cell S1;
  Cell S2;
  Cell T2;
  Cell S3;
  Cell S4;
  Cell V1;
  Cell V2;
  Cell WBTRIG;
  // NA64mu scintillators
  Cell Smu;
  Cell BeamK;

  Cell ZC;
  Cell WCAL[3];
  Cell WCAT;
  Cell VTEC;
  Cell VTWC;
  Cell DM[2];
  Cell ZDCAL[2];
  Cell VHCAL[2][4][4];
  
  Cell LTG[6];   // "live target"
  Cell CHCNT[2]; // "cherenkov counter"
  
  Cell PKRCAL0[6][12];
  Cell PKRCAL1[12][12];

  Hodo HOD0;
  Hodo HOD1;
  Hodo2 HOD2;

  Straw ST01;
  Straw ST02;
  Straw ST03;
  Straw ST04;
  Straw ST05;
  Straw ST06;
  Straw ST11;
  Straw ST12;
  Straw ST13;
  
  Micromega MM1;
  Micromega MM2;
  Micromega MM3;
  Micromega MM4;
  Micromega MM5;
  Micromega MM6;
  Micromega MM7;
  Micromega MM8;
  Micromega MM9;
  Micromega MM10;
  Micromega MM11;
  Micromega MM12;
  Micromega MM13;

  // GEM's
  gem::GEM * GM01;
  gem::GEM * GM02;
  gem::GEM * GM03;
  gem::GEM * GM04;

  std::shared_ptr<MCTruth> mc;

  RecoEvent():
    run(0), runevent(0), spill(0), spillevent(0),
    isCalibration(false), isPhysics(false), isOnSpill(false),
    isTriggerPhysics(false), isTriggerRandom(false), isTriggerBeam(false),
    isTriggerECAL(false),isTriggerMuonInv(false),
    masterTime(0), tdct0_phase(0),WB_phase(0),
    GM01(gem::rawgem[0]), GM02(gem::rawgem[1]),
    GM03(gem::rawgem[2]), GM04(gem::rawgem[3])
  {}
  
  void Init_Micromegas_Calibration()
  {
    MM1.Init(MM1_calib, MM1_pos);
    MM2.Init(MM2_calib, MM2_pos);
    MM3.Init(MM3_calib, MM3_pos);
    MM4.Init(MM4_calib, MM4_pos);
    MM5.Init(MM5_calib, MM5_pos);
    MM6.Init(MM6_calib, MM6_pos);
    MM7.Init(MM7_calib, MM7_pos);
    MM8.Init(MM8_calib, MM8_pos);
    MM9.Init(MM9_calib, MM9_pos);
    MM10.Init(MM10_calib, MM10_pos);
    MM11.Init(MM11_calib, MM11_pos);
    MM12.Init(MM12_calib, MM12_pos);
    MM13.Init(MM13_calib, MM13_pos);
  }

  void Init_StrawTube_Calibration()
  {
    // Initialize calibration and geometry
    ST01.Init(&ST01_calib, &ST01Geometry);
    ST02.Init(&ST02_calib, &ST02Geometry);
    ST03.Init(&ST03_calib, &ST03Geometry);
    ST04.Init(&ST04_calib, &ST04Geometry);
    ST05.Init(&ST05_calib, &ST05Geometry);
    ST06.Init(&ST06_calib, &ST06Geometry);
    ST11.Init(&ST11_calib, &ST11Geometry);
    ST12.Init(&ST12_calib, &ST12Geometry);
    ST13.Init(&ST13_calib, &ST13Geometry);
  }

  const gem::GEM* getGEM(const unsigned int i) const {
    switch(i)
    {
      case 1: return GM01;
      case 2: return GM02;
      case 3: return GM03;
      case 4: return GM04;
    }
    throw std::runtime_error("RecoEvent::getGEM() incorrect GEM index\n");
  }

  const Micromega* getMM(const unsigned int i) const {
    switch(i)
    {
      case  1: return &MM1;
      case  2: return &MM2;
      case  3: return &MM3;
      case  4: return &MM4;
      case  5: return &MM5;
      case  6: return &MM6;
      case  7: return &MM7;
      case  8: return &MM8;
      case  9: return &MM9;
      case 10: return &MM10;
      case 11: return &MM11;
      case 12: return &MM12;
      case 13: return &MM13;
    }
    throw std::runtime_error("RecoEvent::getMM() incorrect Micromega index\n");
  }

  const Straw* getST(const unsigned int i) const {
    switch(i)
    {
      case  1: return &ST01;
      case  2: return &ST02;
      case  3: return &ST03;
      case  4: return &ST04;
      case  5: return &ST05;
      case  6: return &ST06;
      case 11: return &ST11;
      case 12: return &ST12;
      case 13: return &ST13;
    }
    throw std::runtime_error("RecoEvent::getST() incorrect Straw index\n");
  }

  vector<const gem::GEM*>  getGEMs() const { return { GM01, GM02, GM03, GM04 }; }
  vector<const Micromega*> getMMs()  const { return { &MM1, &MM2, &MM3, &MM4, &MM5, &MM6, &MM7, &MM8, &MM9, &MM10, &MM11, &MM12, &MM13 }; }
  vector<const Straw*>     getSTs()  const { return { &ST01, &ST02, &ST03, &ST04, &ST05, &ST06, &ST11, &ST12, &ST13 }; }

  bool hasMasterTime() const { return (masterTime != 0); }
  
  double ecalTotalEnergy() const
  {
    double total = 0;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 12; ++x)
        for (int y = 0; y < 6; ++y)
          total += ECAL[d][x][y].energy;
    
    return total;
  }
  
  double ecalTotalEnergy(const int d) const
  {
    double total = 0;
    for (int x = 0; x < 12; ++x)
      for (int y = 0; y < 6; ++y)
        total += ECAL[d][x][y].energy;
    
    return total;
  }
  
  // the index (ix,iy) of ECAL cell with the maximum energy
  CellXY ecalMaxCell() const
  {
    CellXY pos = {-1, -1};
    double emax = 0.;
    
    for (int x = 0; x < 12; ++x)
      for (int y = 0; y < 6; ++y) {
        const double e = ECAL[0][x][y].energy + ECAL[1][x][y].energy;
        if (e > emax) {
          pos.ix = x;
          pos.iy = y;
          emax = e;
        }
      }
    
    return pos;
  }

  double hcalTotalEnergy() const
  {
    double total = 0;
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 3; ++y)
          total += HCAL[d][x][y].energy;
    
    return total;
  }
  
  double hcalTotalEnergy(const int d) const
  {
    double total = 0;
    for (int x = 0; x < 6; ++x)
      for (int y = 0; y < 3; ++y)
        total += HCAL[d][x][y].energy;
    
    return total;
  }
  
  double vhcalTotalEnergy(const int d) const
  {
    double total = 0;
    for (int x = 0; x < 4; ++x)
      for (int y = 0; y < 4; ++y)
        total += VHCAL[d][x][y].energy;
    
    return total;
  }
  
  double pkrcalTotalEnergy() const {
         double total = 0;
         for (int d = 0; d < 6; ++d)
             for (int x = 0; x < 12; ++x)
                 total += PKRCAL0[d][x].energy;

         for (int x = 0; x < 12; ++x)
             for (int y = 0; y < 12; ++y)
                 total += PKRCAL1[x][y].energy;

         return total;
     }

     double pkrcalTotalEnergyMatrix() const {
         double total = 0;
         for (int x = 0; x < 12; ++x)
             for (int y = 0; y < 12; ++y)
                 total += PKRCAL1[x][y].energy;
         return total;
     }

     double pkrcalTotalEnergyPre(int il) const {
            double total = 0;
            if ((il>=6)||(il<0)) return 0;
            for (int x = 0; x < 12; ++x)
                total += PKRCAL0[il][x].energy;

            return total;
        }

     // the index (ix,iy) of PKRCAL matrix cell with the maximum energy
     CellXY pkrcalMaxCell() const {
         CellXY pos = { -1, -1 };
         double emax = 0.;
         for (int x = 0; x < 12; ++x) {
             for (int y = 0; y < 12; ++y) {
                 const double e = PKRCAL1[x][y].energy;
                 if (e > emax) {
                     pos.ix = x;
                     pos.iy = y;
                     emax = e;
                 }
             }
         }
         return pos;
     }


  // VETO subdetector has three physical cells
  // each cell is readout by two photomultipliers / SADC channels
  // calculate energy deposit in physical cell
  double vetoEnergy(const int n) const
  {
    // VETO has three cells
    if (n < 0) return 0;
    if (n > 2) return 0;
    
    // energy recorded by two p.m. of the single cell
    const double A1 = VETO[2*n].energy;
    const double A2 = VETO[2*n+1].energy;
    
    // work-around for pile-up
    if (fuzzyEquals(A1, 0.)) return A2;
    if (fuzzyEquals(A2, 0.)) return A1;
    
    return sqrt(A1 * A2);
  }
};

na64util::p348reco::TimeClusters RunTimeClustering(RecoEvent & e) {
    na64util::p348reco::TimeClusters timeClusters;
    // when time clustering method instantiated with given dt
    // (ns) and timing populates topology
    na64util::p348reco::TimeClusteringSingleton::init_if_need(
              6.25 // 12.5 // common (SimpleTimeClustering's) threshold, ns
            , 0    // delay between cells, ns
            , 2.5  // uncertainty, ns
            , 100  // max number of items permitted for a cluster
            );
    // fill timed entities list
    na64util::SimpleTimeClustering::TimingTopologyFilter_t::MaximaInfo timedHits;
    // currently, only ECAL is the subject of time clustering
    for(int zIdx = 0; zIdx < 2; ++zIdx) {
        for(int yIdx = 0; yIdx < 6; ++yIdx) {
            for(int xIdx = 0; xIdx < 5; ++xIdx) {
                // encode detector ID (to be decoded after)
                na64util::DetID_t detID = na64util::p348reco::encode_ECAL_det_id(xIdx, yIdx, zIdx);
                // ^^^ NOTE: must correspond to encoding scheme used in
                //     na64util::p348reco::TimeClustering::init_if_need()
                auto & c = e.ECAL[zIdx][xIdx][yIdx];
                // Use fitted peaks info to populate timed item entry
                auto & peaks = c.fittedPeaksInfo;
                na64util::TimePulseNo_t nPeak = 0;
                for(na64util::iMSADCReco::PeakInfo & peakInfo: peaks) {
                    // add pulse info to collection
                    auto ir = timedHits.emplace(
                              std::pair<na64util::DetID_t, na64util::TimePulseNo_t>{detID, nPeak}
                            , na64util::HitTimeInfo(peakInfo)
                            );
                    // correct "measured time" for ECAL cells by subtracting
                    // mean time. If c.isTmeanRelative the master time is
                    // accounted
                    ir.first->second.hitTime -= c.calib->tmean;
                    ++nPeak;
                }  // pulses loop end
            }  // xIdx loop end
        }  // yIdx loop end
    } // zIdx loop end

    // clusterize
    if(!timedHits.empty()) {
        if(na64util::p348reco::TimeClusteringSingleton::self().simple_clusterize(timedHits)) {
            // decode, assign cluster labels
            for(auto & timedHitEntry: timedHits) {
                // assign time cluster label
                reinterpret_cast<na64util::iMSADCReco::PeakInfo*>(timedHitEntry.second.userdata)
                    ->timeClusterLabel = timedHitEntry.second.timeClusterLabel;
            }
            // ECAL-specific stuff
            for(int zIdx = 0; zIdx < 2; ++zIdx) {
                for(int yIdx = 0; yIdx < 6; ++yIdx) {
                    for(int xIdx = 0; xIdx < 5; ++xIdx) {
                        const auto & c = e.ECAL[zIdx][xIdx][yIdx];
                        for(const auto & pi : c.fittedPeaksInfo) {
                            auto & timeCluster = timeClusters.get_cluster_by_id(pi.timeClusterLabel);
                            timeCluster.consider(&c, pi, c.calib->tmean);
                            const double energyGeV = pi.rawAmp * c.calib->K;
                            timeCluster.energySum += energyGeV;
                            if(zIdx) timeCluster.energyEcal0 += energyGeV;
                            else     timeCluster.energyEcal1 += energyGeV;
                            //std::cout << "        " << xIdx << "x" << yIdx << "x" << zIdx
                            //    << ": t=" << pi.time
                            //    << ", E=" << pi.rawAmp * c.calib->K
                            //    << std::endl;
                        }
                    }
                }
            }
        }
    }
    return timeClusters;
}

vector<const Cell*> listCells(const RecoEvent& e)
{
  vector<const Cell*> list;
  
  list.push_back(&e.S0);
  list.push_back(&e.S1);
  list.push_back(&e.S2);
  list.push_back(&e.S3);
  list.push_back(&e.S4);
  list.push_back(&e.T2);
  list.push_back(&e.V1);
  list.push_back(&e.V2);
  list.push_back(&e.WBTRIG);
  list.push_back(&e.Smu);
  list.push_back(&e.BeamK);
  
  for (int i = 0; i < 4; ++i) {
    list.push_back(&e.SRD[i]);
  }
  
  for (int i = 0; i < 6; ++i) {
    list.push_back(&e.VETO[i]);
  }
  
  for (int d = 0; d < 2; ++d)
    for (int x = 0; x < 12; ++x)
      for (int y = 0; y < 6; ++y) {
        list.push_back(&e.ECAL[d][x][y]);
      }
  
  for (int d = 0; d < 4; ++d)
    for (int x = 0; x < 6; ++x)
      for (int y = 0; y < 3; ++y) {
        list.push_back(&e.HCAL[d][x][y]);
      }

  for (int d = 0; d < 2; ++d)
    for (int x = 0; x < 4; ++x)
      for (int y = 0; y < 4; ++y) {
        list.push_back(&e.VHCAL[d][x][y]);
      }

  for (int i = 0; i < 4; ++i) {
    list.push_back(&e.MUON[i]);
  }
  
  for (int i = 0; i < 6; ++i) {
    list.push_back(&e.ECALSUM[i]);
  }
  
  for (int i = 0; i < 3; ++i)
    list.push_back(&e.WCAL[i]);
  
  list.push_back(&e.WCAT);
  
  list.push_back(&e.VTEC);
  
  list.push_back(&e.VTWC);
  
  for (int i = 0; i < 15; ++i) {
    list.push_back(&e.HOD0.xplane[i]);
    list.push_back(&e.HOD0.yplane[i]);
    list.push_back(&e.HOD1.xplane[i]);
    list.push_back(&e.HOD1.yplane[i]);
  }
  
  for (int i = 0; i < 16; ++i) {
    list.push_back(&e.HOD2.xplane[i]);
    list.push_back(&e.HOD2.yplane[i]);
  }
  
  for (int d = 0; d < 6; ++d)
         for (int x = 0; x < 12; ++x)
             list.push_back(&e.PKRCAL0[d][x]);

   for (int x = 0; x < 12; ++x)
         for (int y = 0; y < 12; ++y)
             list.push_back(&e.PKRCAL1[x][y]);

  for (int x = 0; x < 100; ++x)
     list.push_back(&e.LYSO[x]);


  return list;
}


ostream& operator<<(ostream& os, const RecoEvent& e)
{
  // preserve formatting
  const streamsize precision0 = os.precision();
  const streamsize width0 = os.width();
  const std::ios_base::fmtflags flags0 = os.flags();
  
  // event id
  os << "===> Event: run = " << e.run
     << " / spill = " << e.spill
     << " / spillevent = " << e.spillevent << "\n";
  
  // event type
  os << "Event type ="
     << (e.isPhysics ? " physics" : "")
     << (e.isCalibration ? " calibration" : "")
     << (e.isOnSpill ? " on-spill" : " off-spill")
     << "\n";
  
  // trigger type
  os << "Trigger type ="
     << (e.isTriggerPhysics ? " physics" : "")
     << (e.isTriggerRandom ? " random" : "")
     << (e.isTriggerBeam ? " beam" : "")
     << (e.isTriggerECAL ? " ECAL (muon mode)" : "")
     << (e.isTriggerMuonInv ? " muon inv. (muon mode)" : "")
     << "\n";
  
  os << "ECAL0 = " << e.ecalTotalEnergy(0) << " GeV\n"
     << "ECAL1 = " << e.ecalTotalEnergy(1) << " GeV\n"
     << "ECAL total = " << e.ecalTotalEnergy() << " GeV\n"
     << "HCAL0 = " << e.hcalTotalEnergy(0) << " GeV\n"
     << "HCAL1 = " << e.hcalTotalEnergy(1) << " GeV\n"
     << "HCAL2 = " << e.hcalTotalEnergy(2) << " GeV\n"
     << "HCAL3 = " << e.hcalTotalEnergy(3) << " GeV\n"
     << "HCAL total = " << e.hcalTotalEnergy() << " GeV\n";
  
  for (int ii=0;ii<6;ii++){
         os<<"PKRCAL-PreLayer"<<ii<<" "<<e.pkrcalTotalEnergyPre(ii) << " GeV\n";
     }
  os << "PKRCAL-Matrix = " <<e.pkrcalTotalEnergyMatrix() << " GeV\n";
  os << "PKRCAL total = " <<e.pkrcalTotalEnergy() << " GeV\n";



  // ECAL
  for (int n = 0; n < 2; n++) {
    os << "ECAL" << n << ":\n";
    os << "  Y\\X";
    for (int x = 0; x < 12; x++)
      os << std::setw(8) << x;
    os << "\n";
    
    for (int y = 0; y < 6; y++) {
      os << std::setw(3) << y << ": ";
      for (int x = 0; x < 12; x++) {
        os << std::setw(8) << std::setprecision(3);
        const Cell& c = e.ECAL[n][x][y];
        
        if (!c.hasDigit) {
          os << ".";
          continue;
        }
        
        const double en = c.energy;
        if (en > 0.0005)
          os << std::fixed << en;
        else
          os << 0;
      }
      os << "\n";
    }
  }
  
  // HCAL
  for (int n = 0; n < 4; n++) {
    os << "HCAL" << n << ":\n";
    os << "  Y\\X";
    for (int x = 0; x < 6; x++)
      os << std::setw(8) << x;
    os << "\n";
    
    for (int y = 0; y < 3; y++) {
      os << std::setw(3) << y << ": ";
      for (int x = 0; x < 6; x++) {
        os << std::setw(8) << std::setprecision(3);
        const Cell& c = e.HCAL[n][x][y];
        
        if (!c.hasDigit) {
          os << ".";
          continue;
        }
        
        const double en = c.energy;
        if (en > 0.0005)
          os << std::fixed << en;
        else
          os << 0;
      }
      os << "\n";
    }
  }
  
  // PKRCAL
     for (int n = 0; n < 6; n++) {
         os << "PKRCAL-PRE" << n << ":\n";
         os << "  Y";
         for (int x = 0; x < 12; x++)
             os << std::setw(8) << x;
         os << "\n";

         for (int x = 0; x < 12; x++) {
             os << std::setw(8) << std::setprecision(3);
             const Cell &c = e.PKRCAL0[n][x];

             if (!c.hasDigit) {
                 os << ".";
                 continue;
             }

             const double en = c.energy;
             if (en > 0.0005) os << std::fixed << en;
             else
                 os << 0;
         }
         os << "\n";
     }

     os << "PKRCAL" << ":\n";
     os << "  Y\\X";
     for (int x = 0; x < 12; x++)
         os << std::setw(8) << x;
     os << "\n";

     for (int y = 0; y < 12; y++) {
         os << std::setw(3) << y << ": ";
         for (int x = 0; x < 12; x++) {
             os << std::setw(8) << std::setprecision(3);
             const Cell &c = e.PKRCAL1[x][y];

             if (!c.hasDigit) {
                 os << ".";
                 continue;
             }

             const double en = c.energy;
             if (en > 0.0005) os << std::fixed << en;
             else
                 os << 0;
         }
         os << "\n";
     }



  // BGO
  os << "BGO (MeV):\n";
  for (int x = 0; x < 8; x++)
    os << std::setw(8) << x;
  os << "\n";
  
  for (int x = 0; x < 8; x++) {
    const Cell& c = e.BGO[x];
    
    os << std::setw(8) << std::setprecision(3);
    
    if (!c.hasDigit) {
      os << ".";
      continue;
    }
    
    const double en = c.energy / MeV;
    if (en > 0.0005)
      os << std::fixed << en;
    else
      os << 0;
  }
  os << "\n";
  
  // SRD
  os << "SRD (MeV):\n";
  for (int x = 0; x < 4; x++)
    os << std::setw(8) << x;
  os << "\n";
  
  for (int x = 0; x < 4; x++) {
    const Cell& c = e.SRD[x];
    
    os << std::setw(8) << std::setprecision(3);
    
    if (!c.hasDigit) {
      os << ".";
      continue;
    }
    
    const double en = c.energy / MeV;
    if (en > 0.0005)
      os << std::fixed << en;
    else
      os << 0;
  }
  os << "\n";
  
  // VETO
  os.unsetf(std::ios::fixed);
  os << "VETO (MeV):\n";
  for (int x = 0; x < 6; x++)
    os << std::setw(8) << x;
  os << "\n";
  
  for (int x = 0; x < 6; x++) {
    const Cell& c = e.VETO[x];
    
    os << std::setw(8) << std::setprecision(3);
    
    if (!c.hasDigit) {
      os << ".";
      continue;
    }
    
    const double en = c.energy / MeV;
    if (en > 0.0005)
      os << std::fixed << en;
    else
      os << 0;
  }
  os << "\n";
  
  // TODO: add VHCAL0 print
  
  // MM
  for (unsigned int i = 0; i < 2*8; ++i) {
    const unsigned int idx = 1 + i/2;
    const bool isX = (i%2 == 0);
    os << "MM" << idx << (isX ? "X" : "Y") << ":\n";
    
    const Micromega* mm = e.getMM(idx);
    
    const MicromegaPlane& p = isX ? mm->xplane : mm->yplane;
    
    const size_t best = p.bestClusterIndex();
    
    os << "Nclusters = " << p.clusters.size()
       << ", expected time = " << p.calib->time
       << " (error = " << p.calib->time_err << ")"
       << endl;
    
    for (size_t j = 0; j < p.clusters.size(); ++j) {
      const MMCluster& c = p.clusters[j];
      os << j << (j == best ? "*" : " ") << " : size = " << c.size()
           << ", total charge = " << c.charge_total
           << ", time = " << c.cluster_time()
           << ", position = " << c.position()
           << "\n";
    }
  }
  
  // TODO: add GEM
  
  // restore formatting
  os.precision(precision0);
  os.width(width0);
  os.flags(flags0);
  
  return os;
}

ostream& operator<<(ostream& os, const Cell& c)
{
  os << "SADC cell:\n";
  
  if (c.calib)
    os << "  calibration: " << *c.calib << "\n";
  
  os << "  waveform:";
  for (size_t i = 0; i < c.raw.size(); ++i)
    os << " " << c.raw[i];
  os << "\n";
  
  os << "  hasDigit = " << c.hasDigit << "\n"
     << "  hasOverflow() = " << c.hasOverflow() << "\n"
     << "  pedestal0 = " << c.pedestal0 << "\n"
     << "  pedestal1 = " << c.pedestal1 << "\n";
  
  os << "  peaks:";
  for (size_t i = 0; i < c.peaks.size(); ++i) {
    const bool isSignalPeak = ((int)i == c.amplitude_peak_index);
    os << " "
       << (isSignalPeak ? "A=" : "")  // indicate signal peak
       << "("
       << "sample=" << c.peaksample(i)
       << " amplitude=" << c.peakamplitude(i)
       << " t0=" << c.peaktime(i)
       << ")";
  }
  os << "\n";
  
  os << "  amplitude = " << c.amplitude << "\n"
     << "  energy = " << c.energy << " GeV\n"
     << "  pileup() = " << c.pileup() << " GeV\n"
     << "  energy_nopileup() = " << c.energy_nopileup() << " GeV\n"
     << "  t0 = " << c.t0 << "\n"
     << "  t0ns() = " << c.t0ns() << " ns\n"
     << "  amplitude_sample = " << c.amplitude_sample << "\n";
  
  return os;
}

Cell RunWaveReco1(const std::vector<CS::uint16>& raw, const CaloCalibData& calibration = CALO_nocalibration)
{
  Cell c;
  c.hasDigit = true;
  c.calib = &calibration;
  c.raw = raw;
  c.calcPedestals();

  if(c.calib->doWaveformFit) {
    c.fittedPeaksInfo = na64util::get_moyal01_wf_reconstruction(raw.size())
            ->reconstruct(raw, c.pedestal0, c.pedestal1);
  }

  c.searchPeaks();
  
  // NOTE, the final reconstruction is done in RunWaveReco2()
  
  return c;
}

void RunCellReco(std::vector<Cell*>& list, Cell& c, const std::vector<CS::uint16>& raw, const CaloCalibData& calibration = CALO_nocalibration)
{
  c = RunWaveReco1(raw, calibration);
  list.push_back(&c);
}

void RunWaveReco2(const std::vector<Cell*>& list, const int method, const double master,const double WB_phase)
{
  // set cell energy and timing
  for (size_t i = 0; i < list.size(); ++i){
    const double refTime = (list[i]->calib->adc.isWB ? WB_phase : master);
    list[i]->selectPeak(method,refTime);
  }
}


//A.C. this code is used to correct for each spill and for each run the ECAL0-2-2 and ECAL1-2-2 signals using beam-only data
//Since corrections were obtained using the signal amplitude before LED corrections, we revert back to these
void RunECALCentralCellBeamOnlyCorrection(RecoEvent& e){
  Set_BeamOnlyData_spill(e.run,e.spill);

  const int iX=ECAL0BEAMCELL.ix;
  const int iY=ECAL0BEAMCELL.iy;

  //apply correction
  e.ECAL[0][iX][iY].energy = e.ECAL[0][iX][iY].amplitude * e.ECAL[0][iX][iY].calib->K;
  e.ECAL[0][iX][iY].energy *= BeamOnlyref.ecal[0][iX][iY]/BeamOnlycurr.ecal[0][iX][iY];

  e.ECAL[1][iX][iY].energy = e.ECAL[1][iX][iY].amplitude * e.ECAL[1][iX][iY].calib->K;
  e.ECAL[1][iX][iY].energy *= BeamOnlyref.ecal[1][iX][iY]/BeamOnlycurr.ecal[1][iX][iY];

}


void RunECALExtraFactorCorrection2023A(RecoEvent &e) {
  static const map<int, double> runfactor = LoadRunFactor2023A(conddbpath() + "/runfactors_2023A");
  const bool haveData = (runfactor.find(e.run) != runfactor.end());
  if (!haveData)
    throw std::runtime_error("Error to RunECALExtraFactorCorrection2023A data file");

  const double f = runfactor.at(e.run);
  for (int imodule = 0; imodule < 2; imodule++) {
    for (int iX = 0; iX < 6; iX++) {
      for (int iY = 0; iY < 6; iY++) {
        e.ECAL[imodule][iX][iY].energy *= f;
      }
    }
  }
}


//This function is used for 2024 e- beam session to correct for VETO time-walk effects
//Documentation: https://gitlab.cern.ch/P348/p348-daq/-/issues/146
void RunVETOTimeWalkCorrection2024A(RecoEvent &e){

  //The mean value of the time distribution (relative to master time) T of each VETO cell, with respect to the amplitude is given by
  //f(A)=T_0+(A-A_0)\cdot m \cdot (0.5+0.5\tanh((A-A_0)\cdot\alpha)).
  //A_0: MIP-like amplitude (below this value, the function goes to the constant T_0, in veto.txt
  //m, #alpha: correction parameters (m: slope of the linear part; alpha: controls the transiction)

  //A_0,m,alpha
  struct TWCorrParams {
    double A0;
    double m;
    double alpha;

    double calc(double A) const {
      return m * (A - A0) * 0.5 * (1 + tanh((A - A0) * alpha));
    }
  };

  static const TWCorrParams tw[6]={
       {9.477839787e+02,2.352717469e-03,2.141494551e-03},
      {9.159162533e+02,2.492059399e-03,1.644455478e-03},
      {2.198681195e+02,3.342868312e-03,4.523498969e-04},
      {3.961863927e+02,3.271134704e-03,3.349368339e-04},
      {1.010799799e+03,3.087629027e-03,4.919111772e-04},
      {9.744951137e+02,3.024762739e-03,8.704541120e-04}
  };

  for (int iv=0;iv<6;iv++){
    const double A=e.VETO[iv].amplitude;
    e.VETO[iv].tTW=tw[iv].calc(A);
  }

}


void RunLedCorrection(RecoEvent& e)
{
  // prepare corrections data
  Set_LED_spill(e.run, e.spill);

  // apply correction
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < ECAL_pos.Nx; ++x)
      for (int y = 0; y < ECAL_pos.Ny; ++y) {
        const double ratio = ledref.ecal[d][x][y] / ledcurr.ecal[d][x][y];
        e.ECAL[d][x][y].energy *= ratio;
        /*
        cout << "ledcorrection():"
             << " ECAL" << d << "-" << x << "-" << y
             << " ref=" << ledref.ecal[d][x][y]
             << " curr=" << ledcurr.ecal[d][x][y]
             << " ratio=" <<  ratio
             << endl;
        */
      }

  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; ++x)
      for (int y = 0; y < 3; ++y) {
        const double ratio = ledref.hcal[d][x][y] / ledcurr.hcal[d][x][y];
        e.HCAL[d][x][y].energy *= ratio;
      }


  // VHCAL led data only in 2023
  const bool is2023A = (8459 <= e.run && e.run <= 9717);
  if (is2023A)
    for (int d = 0; d < 1; d++)
      for (int x = 0; x < 4; ++x)
        for (int y = 0; y < 4; ++y) {
          const double ratio = ledref.vhcal[d][x][y] / ledcurr.vhcal[d][x][y];
          e.VHCAL[d][x][y].energy *= ratio;
        }

  // WCAL led data only in 2018
  const bool is2018  = (3574 <= e.run && e.run <= 4310);
  if (is2018)
    for (int d = 0; d < 3; d++) {
      const double ratio = ledref.wcal[d] / ledcurr.wcal[d];
      e.WCAL[d].energy *= ratio;
    }
}

std::vector<int> RunHodoResolve(const Cell row[15])
{
  const size_t n = 15;
  const double cut = HODO_threshold;
  std::vector<int> hit;
  
  /*
  std::cout << "resolveHodo " << n << " : ";
  for (size_t i = 0; i < n; ++i) {
    std::cout << row[i].amplitude << " " ;
    if (row[i].amplitude > 0) {
      std::cout << "(";
      for (size_t j = 0; j < row[i].raw.size(); ++j)
        std::cout << row[i].raw[j] << " ";
      std::cout << ") ";
    }
  }
  std::cout << std::endl;
  */
  
  for (size_t i = 0; i < n; ++i) {
    // hits in prev, curr, next cells of hodoscope
    const bool prev = (i > 0 && row[i-1].amplitude > cut);
    const bool curr = (row[i].amplitude > cut);
    const bool next = (i + 1 < n && row[i+1].amplitude > cut);
    
    if (curr && next) hit.push_back(2*(i + 1));
    if (curr && !prev && !next) hit.push_back(2*i + 1);
  }
  
  return hit;
}

// Resolve for HOD_2 different size cells,cell number and Threshold HOD2_threshold defined in condb.h
std::vector<int> RunHodoResolve2(const Cell row[16])
{
  const size_t n = 16; // Channel X=15 and Y=15 broken
  const double cut = HODO2_threshold;
  std::vector<int> hit;
  
  for (size_t i = 0; i < n-1; ++i) {
    // hits in prev, curr, next cells of hodoscope
    const bool prev = (i > 0 && row[i-1].amplitude > cut);
    const bool curr = (row[i].amplitude > cut);
    const bool next = (i + 1 < n && row[i+1].amplitude > cut);
   // position on HOD2 from 1-29 mm  
    if (curr && next) hit.push_back(2*i + 1);
    if (curr && !prev && !next) hit.push_back(2*i);
  }
  
  return hit;
}

void DoHodoReco(Hodo& h)
{
  h.xhits = RunHodoResolve(h.xplane);
  h.yhits = RunHodoResolve(h.yplane);
}

void DoHodoReco2(Hodo2& h)
{
  h.xhits = RunHodoResolve2(h.xplane);
  h.yhits = RunHodoResolve2(h.yplane);
}

RecoEvent RunP348Reco(const CS::DaqEventsManager& manager)
{
  static int last_run = -1;
  const int current_run = manager.GetEvent().GetRunNumber();
  if (current_run != last_run) {
    Init_Reconstruction(manager);
    gem::Init_gem_reconstruction(manager);
    
    last_run = current_run;

    // init waveform fitting
    auto p = na64util::gDefaultMoyal01Parameters;
    // one may set waveform reconstruction parameters here, for instance:
    //    p.dbgLogStream = &std::cerr;
    p.debugLog = false;
    p.errLogStream = 0;
    p.dbgLogStream = 0;
    na64util::set_default_moyal01_wf_reconstruction_parameters(p);
  }
  
  RecoEvent e;
  
  e.run = manager.GetEvent().GetRunNumber();
  e.runevent = manager.GetEvent().GetEventNumberInRun();
  e.spill = manager.GetEvent().GetBurstNumber();
  e.spillevent = manager.GetEvent().GetEventNumberInBurst();
  
  bool trigLED = false;
  bool trigRAND = false;
  bool trigBEAM = false;
  bool trigPHYS = false;
  bool trigMUON = false;
  bool trigECAL = false;
  
  gem::Init_gem_event();
  
  //Micromegas
  e.Init_Micromegas_Calibration();

  //Straw Tube
  e.Init_StrawTube_Calibration();

  // list of SADC cells in the current event
  std::vector<Cell*> cellsList;
  
  // T0 TDC
  map<uint8_t, float> t0tdc;
 
  // get digits
  typedef CS::Chip::Digits::const_iterator Digi_it;
  for (Digi_it i = manager.GetEventDigits().begin(); i != manager.GetEventDigits().end(); ++i ) {
    const CS::Chip::Digit* digit = i->second;
    
    // process APV digits
    const CS::ChipAPV::Digit* apv = dynamic_cast<const CS::ChipAPV::Digit*>(digit);
    if (apv) {
      //apv->Print();
      
      // APV readout have 3 samples
      const string& name = apv->GetDetID().GetName();
      //const CS::uint16 channel = apv->GetChipChannel();
      const CS::uint16 wire = apv->GetChannel();
      const CS::uint32* raw_q = apv->GetAmplitude();
      
           if (name == "MM01X") DoHitAccumulation(wire, raw_q, e.MM1.xplane);
      else if (name == "MM02X") DoHitAccumulation(wire, raw_q, e.MM2.xplane);
      else if (name == "MM03X") DoHitAccumulation(wire, raw_q, e.MM3.xplane);
      else if (name == "MM04X") DoHitAccumulation(wire, raw_q, e.MM4.xplane);
      else if (name == "MM05X") DoHitAccumulation(wire, raw_q, e.MM5.xplane);
      else if (name == "MM06X") DoHitAccumulation(wire, raw_q, e.MM6.xplane);
      else if (name == "MM07X") DoHitAccumulation(wire, raw_q, e.MM7.xplane);
      else if (name == "MM08X") DoHitAccumulation(wire, raw_q, e.MM8.xplane);
      else if (name == "MM09X") DoHitAccumulation(wire, raw_q, e.MM9.xplane);
      else if (name == "MM10X") DoHitAccumulation(wire, raw_q, e.MM10.xplane);
      else if (name == "MM11X") DoHitAccumulation(wire, raw_q, e.MM11.xplane);
      else if (name == "MM12X") DoHitAccumulation(wire, raw_q, e.MM12.xplane);
      else if (name == "MM13X") DoHitAccumulation(wire, raw_q, e.MM13.xplane);
      else if (name == "MM01Y") DoHitAccumulation(wire, raw_q, e.MM1.yplane);
      else if (name == "MM02Y") DoHitAccumulation(wire, raw_q, e.MM2.yplane);
      else if (name == "MM03Y") DoHitAccumulation(wire, raw_q, e.MM3.yplane);
      else if (name == "MM04Y") DoHitAccumulation(wire, raw_q, e.MM4.yplane);
      else if (name == "MM05Y") DoHitAccumulation(wire, raw_q, e.MM5.yplane);
      else if (name == "MM06Y") DoHitAccumulation(wire, raw_q, e.MM6.yplane);
      else if (name == "MM07Y") DoHitAccumulation(wire, raw_q, e.MM7.yplane);
      else if (name == "MM08Y") DoHitAccumulation(wire, raw_q, e.MM8.yplane);
      else if (name == "MM09Y") DoHitAccumulation(wire, raw_q, e.MM9.yplane);
      else if (name == "MM10Y") DoHitAccumulation(wire, raw_q, e.MM10.yplane);
      else if (name == "MM11Y") DoHitAccumulation(wire, raw_q, e.MM11.yplane);
      else if (name == "MM12Y") DoHitAccumulation(wire, raw_q, e.MM12.yplane);
      else if (name == "MM13Y") DoHitAccumulation(wire, raw_q, e.MM13.yplane);
      
      else if (name == "GM01X1__")
          e.GM01->xPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);
      else if (name == "GM01Y1__")
          e.GM01->yPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);
      else if (name == "GM02X1__")
          e.GM02->xPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);
      else if (name == "GM02Y1__")
          e.GM02->yPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);
      else if (name == "GM03X1__")
          e.GM03->xPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);
      else if (name == "GM03Y1__")
          e.GM03->yPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);
      else if (name == "GM04X1__")
          e.GM04->xPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);
      else if (name == "GM04Y1__")
          e.GM04->yPlane.AddHit(wire, /*_hem = */ 0, raw_q[0], raw_q[1], raw_q[2]);

      // unknown
      else {
        std::cout << "RunP348Reco(): ChipAPV, unknown detector name = " << name << std::endl;
      }
      
      continue;
    }

    // process TDC digits    
    const CS::ChipNA64TDC::Digit* tdc = dynamic_cast<const CS::ChipNA64TDC::Digit*>(digit);
    if (tdc) {
      // tdc->Print();

      const std::string name = tdc->GetDetID().GetName();
      const int z = cast<int>(name.substr(2, 2));
      const bool isXplane = ((name.substr(4, 1) == "X") || (name.substr(4, 1) == "U"));
      const bool isUVstation = ((name.substr(4, 1) == "U") || (name.substr(4, 1) == "V"));
      const int wire = tdc->GetWire();
      const int time = tdc->GetTime();

      // TODO: handle channel #64 (straw tdc T0 ?)
      //if (wire == 64) cout << "ChipNA64TDC: name=" << name << " wire=" << wire << " time=" << time << endl;

      // hits processing
      if (time < 2000)
      if ((!isUVstation && wire < 64) || (isUVstation && wire < 64*6)) {
        const StWire wt = {wire, time};

             if (name == "ST01X") e.ST01.XChannels.push_back(wt);
        else if (name == "ST01Y") e.ST01.YChannels.push_back(wt);
        else if (name == "ST02X") e.ST02.XChannels.push_back(wt);
        else if (name == "ST02Y") e.ST02.YChannels.push_back(wt);
        else if (name == "ST03X") e.ST03.XChannels.push_back(wt);
        else if (name == "ST03Y") e.ST03.YChannels.push_back(wt);
        else if (name == "ST04X") e.ST04.XChannels.push_back(wt);
        else if (name == "ST04Y") e.ST04.YChannels.push_back(wt);
        else if (name == "ST05X") e.ST05.XChannels.push_back(wt);
        else if (name == "ST05Y") e.ST05.YChannels.push_back(wt);
        else if (name == "ST06X") e.ST06.XChannels.push_back(wt);
        else if (name == "ST06Y") e.ST06.YChannels.push_back(wt);
        else if (name == "ST11U") e.ST11.XChannels.push_back(wt);
        else if (name == "ST11V") e.ST11.YChannels.push_back(wt);
        else if (name == "ST12U") e.ST12.XChannels.push_back(wt);
        else if (name == "ST12V") e.ST12.YChannels.push_back(wt);
        else if (name == "ST13U") e.ST13.XChannels.push_back(wt);
        else if (name == "ST13V") e.ST13.YChannels.push_back(wt);
        /// TODO: UXV straw treatment
        else if (name == "ST13X") {} 
        else if (name == "STT0" || name == "STT0XY") {
          // trigger timing channel, actual processing is below
        }
        else if (name == "STT0UV" || name == "STT0UV2") {
          // trigger timing channel of large straw, no processing
        }
        else {
          std::cout << "RunP348Reco(): ChipNA64TDC, unknown detector name = " << name << std::endl;
        }
      }
      
      // timing processing
      // 2021B = STT0
      // 2022A,B 2023A,B = STT0XY
      const bool isT0TDC = (name == "STT0") || (name == "STT0XY");
      if (isT0TDC) {
        const int tdcChannel = tdc->GetChannel();
        const float tdcTimeF = (float)(CS::int16)tdc->GetTime();
        
        // normally it should be only one digit per channel,
        // but sometimes there are several digits for the same channel
        // TODO: study more for better action in the case of multiple digits
        //
        if (e.run >= 8459) {
          // starting from 2023A - use the latest digit
          t0tdc[tdcChannel] = tdcTimeF;
        }
        else {
          // before 2023A - use the very first digit
          if (!t0tdc.count(tdcChannel))
            t0tdc[tdcChannel] = tdcTimeF;
        }
      }
    }
    
    // process SADC digits
    // A.C. this code should also process WB data (documentation: https://gitlab.cern.ch/P348/p348-daq/-/issues/118 and https://twiki.cern.ch/twiki/bin/view/P348/WB
    const CS::ChipSADC::Digit* sadc = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
    const CS::ChipNA64WaveBoard::Digit* wb = dynamic_cast<const CS::ChipNA64WaveBoard::Digit*>(digit);
    if (!sadc && !wb) continue;

    const std::string name = (sadc ? sadc->GetDetID().GetName() : wb->GetDetID().GetName());
    const int x = (sadc ? sadc->GetX() : wb->GetX());
    const int y = (sadc ? sadc->GetY() : wb->GetY());
    const std::vector<CS::uint16>& wave = (sadc ? sadc->GetSamples() : wb->GetSamples());
    


    // trigger
    if (name == "TRIG") {
      bool flag = false;
      
      for (size_t j = 0; j < wave.size(); ++j) {
        flag = (wave[j] > TRIG_raw_threshold);
        if (flag) break;
      }
      
      if (x == 0) trigLED = flag;
      if (x == 1) trigRAND = flag;
    }
    
    //FLT signal to WB
    else if (name == "WBTRIG"){
      RunCellReco(cellsList, e.WBTRIG, wave,WBTRIG_calibration);
    }

    // ecal
    else if (name == "ECAL0" || name == "ECAL1") {
      // process waveform and apply calibration
      const int d = cast<int>(name.substr(4, 1));
      RunCellReco(cellsList, e.ECAL[d][x][y], wave, ECAL_calibration[d][x][y]);
    }
    
    // hcal
    else if (name == "HCAL0" || name == "HCAL1" || name == "HCAL2" || name == "HCAL3") {
      const int d = cast<int>(name.substr(4, 1));
      RunCellReco(cellsList, e.HCAL[d][x][y], wave, HCAL_calibration[d][x][y]);
    }
    
    // VHCAL
    else if (name == "VHCAL0") {
      RunCellReco(cellsList, e.VHCAL[0][x][y], wave, VHCAL_calibration[0][x][y]);
    }

    // VHCAL1
    else if (name == "VHCAL1") {
      RunCellReco(cellsList, e.VHCAL[1][x][y], wave, VHCAL_calibration[1][x][y]);
    }

     // PKRCAL0
    else if (name == "PKRCAL0") {
      RunCellReco(cellsList, e.PKRCAL0[x][y], wave, PKRCAL0_calibration[x][y]);
    }

     // PKRCAL1
    else if (name == "PKRCAL1") {
      RunCellReco(cellsList, e.PKRCAL1[x][y], wave, PKRCAL1_calibration[x][y]);
    }
    
    // muon
    else if (name == "MUON0" || name == "MUON1" || name == "MUON2" || name == "MUON3") {
      const int z = cast<int>(name.substr(4, 1));
      RunCellReco(cellsList, e.MUON[z], wave, MUON_calibration[z]);
    }
    
    // hodoscope
    else if (name == "HOD0X" || name == "HOD1X" || name == "HOD0Y" || name == "HOD1Y" || name == "HOD2X" || name == "HOD2Y") {
      const int z = cast<int>(name.substr(3, 1));
      const bool isXplane = (name.substr(4, 1) == "X");
      const int p = isXplane ? 0 : 1;
      
      // cell id number is caried by 'y' variable, the 'x' is always zero
      //std::cout << name << " z = " << z << " isX = " << isXplane << " x = " << x << " y = " << y << "\n";
      
      if (z == 0) {
        Cell& c = isXplane ? e.HOD0.xplane[y] : e.HOD0.yplane[y];
        RunCellReco(cellsList, c, wave, HOD_calibration[z][p][y]);
      }
      else if (z == 1) {
        Cell& c = isXplane ? e.HOD1.xplane[y] : e.HOD1.yplane[y];
        RunCellReco(cellsList, c, wave, HOD_calibration[z][p][y]);
      }
      else if (z == 2) {
        // HOD2 is a new Hod2 type with 16 cells (by Chile team)
        Cell& c = isXplane ? e.HOD2.xplane[y] : e.HOD2.yplane[y];
        RunCellReco(cellsList, c, wave, HOD_calibration[z][p][y]);
      }
    }
    
    // veto
    else if (name == "VETO") { RunCellReco(cellsList, e.VETO[x], wave, VETO_calibration[x]); }
    
    // ecalsum
    else if (name == "ECALSUM") { RunCellReco(cellsList, e.ECALSUM[x], wave, ECALSUM_calibration[x]); }
    
    // active target
    else if (name == "ATARG") { RunCellReco(cellsList, e.ATARG[x][y], wave); }
    
    // synchrotron radiation detectors
    else if (name == "SRD") { RunCellReco(cellsList, e.SRD[x], wave, SRD_calibration[x]); }
    else if (name == "BGO") { RunCellReco(cellsList, e.BGO[x], wave, BGO_calibration[x]); }
    else if (name == "LYSO") { RunCellReco(cellsList, e.LYSO[x], wave, LYSO_calibration[x]); }
    
    // beam counters
    else if (name == "S0") { RunCellReco(cellsList, e.S0, wave, S0_calibration); }
    else if (name == "S1") { RunCellReco(cellsList, e.S1, wave, S1_calibration); }
    else if (name == "S2") { RunCellReco(cellsList, e.S2, wave, S2_calibration); }
    else if (name == "T2") { RunCellReco(cellsList, e.T2, wave, T2_calibration); }
    else if (name == "S3") { RunCellReco(cellsList, e.S3, wave, S3_calibration); }
    else if (name == "S4") { RunCellReco(cellsList, e.S4, wave, S4_calibration); }
    else if (name == "V1") { RunCellReco(cellsList, e.V1, wave, V1_calibration); }
    else if (name == "V2") { RunCellReco(cellsList, e.V2, wave, V2_calibration); }
    else if (name == "ZC") { RunCellReco(cellsList, e.ZC, wave); }
    else if (name == "Smu") { RunCellReco(cellsList, e.Smu, wave, Smu_calibration); }
    else if (name == "BeamK") { RunCellReco(cellsList, e.BeamK, wave, BeamK_calibration); }
    
    // 2023A, hadron runs counters
    else if (name == "LTG") { RunCellReco(cellsList, e.LTG[x], wave, LTG_calibration[x]); }
    else if (name == "CHCNT") { RunCellReco(cellsList, e.CHCNT[x], wave, CHCNT_calibration[x]); }
    
    // visible mode detectors
    else if (name == "WCAL") { RunCellReco(cellsList, e.WCAL[x], wave, WCAL_calibration[x]); }
    else if (name == "WCAT") { RunCellReco(cellsList, e.WCAT, wave, WCAT_calibration); }
    else if (name == "VTEC") { RunCellReco(cellsList, e.VTEC, wave, VTEC_calibration); }
    else if (name == "VTWC") { RunCellReco(cellsList, e.VTWC, wave, VTWC_calibration); }
    else if (name == "DM") { RunCellReco(cellsList, e.DM[x], wave); }
    
    // unknown
    else {
      std::cout << "RunP348Reco(): ChipSADC, unknown detector name = " << name << std::endl;
    }
  }
  
  // complete trigger type information
  const CS::DaqEvent::EventType etype = manager.GetEvent().GetType();
  
  // runs 2015 trigger type definition
  if (330 <= e.run && e.run <= 629) {
    // during data taking 2015 the only DDD event type is PHYSICS_EVENT
    // actual event trigger type was defined by two SADC channels
    
    if (etype == CS::DaqEvent::PHYSICS_EVENT) {
      e.isCalibration = trigLED;
      e.isTriggerRandom = trigRAND;
      e.isPhysics = ! (trigLED || trigRAND);
      e.isTriggerPhysics = e.isPhysics;
    }
  }
  
  // runs starting from 2016A trigger type
  if (e.run >= 938 && e.run <= 8458) {
    e.isPhysics = (etype == CS::DaqEvent::PHYSICS_EVENT);
    e.isCalibration = (etype == CS::DaqEvent::CALIBRATION_EVENT);
    e.isTriggerRandom = trigRAND;
    e.isTriggerPhysics = e.isPhysics;
  }

  // runs starting from 2023A have prescaler information
  if (e.run >= 8459 && e.run <= 10315) {
    e.isPhysics = (etype == CS::DaqEvent::PHYSICS_EVENT);
    e.isCalibration = (etype == CS::DaqEvent::CALIBRATION_EVENT);
    
    trigPHYS = t0tdc.count(33); // ch33 = physical trigger
    trigBEAM = t0tdc.count(41); // ch41 = beam trigger
    trigRAND = t0tdc.count(37); // ch37 = random trigger
    
    e.isTriggerPhysics = trigPHYS;
    e.isTriggerBeam    = trigBEAM;
    e.isTriggerRandom  = trigRAND;
  }
  if (10316 <= e.run) {
    e.isPhysics = (etype == CS::DaqEvent::PHYSICS_EVENT);
    e.isCalibration = (etype == CS::DaqEvent::CALIBRATION_EVENT);

    // before prescaler
    trigPHYS = t0tdc.count(28); // ch28 = physical trigger
    trigBEAM = t0tdc.count(26); // ch26 = beam trigger
    trigRAND = t0tdc.count(27); // ch27 = random trigger
    trigMUON = t0tdc.count(20); // ch20 = Muon to invisible trigger (muon mode)
    trigECAL = t0tdc.count(21); // ch21 = ECAL missing energy trigger (muon mode)

    e.isTriggerPhysics = trigPHYS;
    e.isTriggerBeam = trigBEAM;
    e.isTriggerRandom = trigRAND;
    e.isTriggerECAL = trigECAL;
    e.isTriggerMuonInv = trigMUON;
  }
  {
    // The event data block lacks the 'is-on-spill' flag,
    // so let's calculate the flag, the assumptions:
    //  - any physics event is of 'on-spill' type
    //  - the calibration event considered 'on-spill' if surrounded by events of type physics
    // 
    // Typical event sequence (C=calibration event, P=physics event):
    //   CCPPPPPCPPPPPCPPPPPPCCCCCCCCCCCCCCCCCC
    //     \_on-spill events_/
    
    static uint32_t prevrun = 0;
    static uint32_t prevspill = 0;
    // the counter of consecutive calibration events
    static int nCalib = 0;
    
    // reset the state on new spill
    const bool isSameSpill = (e.run == prevrun) && (e.spill == prevspill);
    if (!isSameSpill) {
      prevrun = e.run;
      prevspill = e.spill;
      nCalib = 0;
    }
    
    if (e.isPhysics) nCalib = 0;
    if (e.isCalibration) nCalib += 1;
    
    e.isOnSpill = (nCalib < 2);
  }
  
  // set master time
  if (1776 <= e.run && e.run <= 2551) {
    if (e.S1.hasDigit) {
      // 1) explicitly use center-of-mass timing for S1 master time
      // this expected to give better results vs. "rising time" method as
      // S1 was connected to ADC through pulse shaper with output rise time
      // which is much less than 12.5 ns ADC sampling
      // 2) narrow the window (15, 32) -> (15, 20) to mitigate pile-up
      // +7ns to compensate the narrowing
      e.masterTime = e.S1.calib->adc.sampling_interval * e.S1.t0_CenterOfMass(15, 20) + 7.;
    }
  }
  
  // master time 2017
  // master time is based on S2 counter
  if (2817 <= e.run && e.run <= 3190) {
    if (e.S2.hasDigit && e.S2.calib->hasTcalib()) {
      // the S2.t0ns() is not yet reconstructed, run the reco manualy
      e.S2.selectPeak(SADC_peak_method, 0);
      e.masterTime = e.S2.t0ns();
      
      // add average shift between (T2 vs. S2) to use the same timing calibration tables of T2-based master time
      e.masterTime += (T2_calibration.tmean - e.S2.calib->tmean);
    }
  }
  
  // starting from run 3191 the S2 counter signal is recorded without integration circuit by T2 ADC channel
  // which expected to resolve some problematic behaviour observed for S2
  if (3191 <= e.run && e.run <= 3573) {
    if (e.T2.hasDigit) {
      // the T2.t0ns() is not yet reconstructed, run the reco manualy
      e.T2.selectPeak(SADC_peak_method, 0);
      e.masterTime = e.T2.t0ns();
    }
  }
  
  // master time 2018, 2021A
  if (3574 <= e.run && e.run <= 5187) {
    if (e.T2.hasDigit) {
      // the T2.t0ns() is not yet reconstructed, run the reco manualy
      e.T2.selectPeak(SADC_peak_method, 0);
      e.masterTime = e.T2.t0ns();
    }
  }
  
  // master time 2021B, 2022A, 2022B
  if (5188 <= e.run && e.run <= 8458) {
    // T0 TDC channel is "32"
    const bool tdct0_ok = t0tdc.count(32);
    
    if (tdct0_ok) {
      e.tdct0_phase = t0tdc[32];

      // shift +190. is to put the t_master mean to ~100. ns
      // note: 2021A/Donskov shift is +93.5 to set mean = ~0. ns
      e.masterTime = e.tdct0_phase  + 190.;
      
      // 2022B: compensate timing shift due to the adjustment of trigger signal before run 6726
      //   ELOG  http://pcdmfs01.cern.ch:8080/Main/454
      // the "-14.5 ns" shift is extracted by timing.cc comparing run 6616 vs 6726 (by A.Karneyeu)
      if (6726 <= e.run && e.run <= 8458) e.masterTime += -14.5;
    }
  }

  // master time 2023A
  // Starting from this session the "prescaler" hardware is in use
  // and the timing extraction became trigger-type dependent
  //
  // Known issues:
  // - runs before 8706 do not have "trigger Accepted" T0 set,
  //     and the T0 is taken from "beam trigger Primitive" channel
  // - runs between 8708 and 8721 have some events with T0 not set (the majority of events has T0 set)
  // - runs between 8722 and 8743 have T0 set for all events, but suffer from the "dual band issue"
  // - runs >=8744 are ok
  // - more details: https://gitlab.cern.ch/P348/p348-daq/-/issues/79
  //
  if (8459 <= e.run && e.run <= 9717) {
    int ch=0;
    if (t0tdc.count(41)) ch=41;      // put it here so that it works as a "fall-back"
    if (e.run>=8706){
      if (t0tdc.count(53)) ch = 53;  // physical trigger Accepted
      if (t0tdc.count(57)) ch = 57;  // random trigger Accepted
      if (t0tdc.count(61)) ch = 61;  // beam trigger Accepted
    }
    const bool tdct0_ok = (ch != 0);
    if (tdct0_ok) {
      // the tdct0_phase is inverted due to the swap of start-stop channels compared to setups before 2023A,B
      e.tdct0_phase = -t0tdc[ch];
      // For 2023A, masterTime should be inverted by sign
      // shift +123.4 is to put the t_master mean to ~100. ns (by A. Celentano, B. Banto using run 9077)
      e.masterTime = e.tdct0_phase + 123.4;
    }
  }


  // master time 2023B
  if (9718 <= e.run && e.run <= 10315) {
    // extract the timing from the trigger "Accepted" (after prescaler) channels
    // normally, only one of these channels should be high
    // more details: maps/2023mu.xml/STRAW.xml
    int ch = 0;
    if (t0tdc.count(53)) ch = 53;  // physical trigger Accepted
    if (t0tdc.count(57)) ch = 57;  // random trigger Accepted
    if (t0tdc.count(61)) ch = 61;  // beam trigger Accepted
    const bool tdct0_ok = (ch != 0);
    
    if (tdct0_ok) {
      // the tdct0_phase is inverted due to the swap of start-stop channels compared to setups before 2023A,B
      e.tdct0_phase = -t0tdc[ch];

      // shift +130. is to put the t_master mean to ~100. ns (by A.Karneyeu using run 9905)
      e.masterTime = e.tdct0_phase + 130.;
    }
  }

  // master time 2024A
  if (10316 <= e.run && e.run <= 11467) {
    // extract the timing from the trigger "Accepted" (after prescaler) channels
    // normally, only one of these channels should be high
    // more details: maps/2024.xml/STRAW.xml
    int ch = 0;
    if (t0tdc.count(31)) ch = 31;  // physical trigger Accepted
    if (t0tdc.count(30)) ch = 30;  // random trigger Accepted
    if (t0tdc.count(29)) ch = 29;  // beam trigger Accepted
    const bool tdct0_ok = (ch != 0);

    if (tdct0_ok) {
      // the tdct0_phase is inverted due to the swap of start-stop channels compared to setups before 2023A,B
      e.tdct0_phase = -t0tdc[ch];

      // shift +325. is to put the t_master mean to ~100. ns (by B.Banto using run 10602)
      e.masterTime = e.tdct0_phase + 325.;
    }

    //first part of the session, ECAL readout from WB (runs <= 10409)
    // all WB clocks were syncronous with local clock from DHMUX, but not with main TCS clock
    // The FLT was sent to WB 110 ch 6 (port 9 ch 6 of DHMUX, mapped to "WBTRIG")
    if (e.run <= 10409) {
      // the WBTRIG.t0ns() is not yet reconstructed, run the reco manualy
      if (e.WBTRIG.hasDigit) {
        e.WBTRIG.selectPeak(kPeak_MaxAmplitude, 0);
        e.WB_phase = e.WBTRIG.t0ns();
      }
    }

    //A.C. observed a systematic difference for master time between physics events and beam-only events (https://gitlab.cern.ch/P348/p348-daq/-/issues/146)
    //This difference is such that T_Master(PhysicsTrigger) > T_Master(BeamOnly)
    //This difference is run-dependent.
    double delta = 0;
    if (e.run <= 10747)
      delta = 4.7;
    else if (e.run <= 10749)
      delta = 3.3;
    else if (e.run <= 10763)
      delta = 3.8;
    else if (e.run <= 11390)
      delta = 0.9;

    if (ch == 31) {
      e.masterTime -= delta;
    }

    //Also, there is a run-dependent shift of master time. The code below is used to fix it, aligning master time to the value for the beginning of the session.
    //Time calibrations were obtained AFTER implementing this fix
    //Documentation: https://gitlab.cern.ch/P348/p348-daq/-/issues/146
    delta = 0;
    if (e.run <= 10584)
      delta = 0;
    else if (e.run <= 10594)
      delta = +25.6;
    else if (e.run <= 10597)
      delta = +4.4;
    else if (e.run <= 10606)
      delta = +2.8;
    else if (e.run <= 10749)
      delta = +36.7;
    else if (e.run <= 11390)
      delta = +34.2;
    e.masterTime += delta;

    //A.C. TODO: positrons 2024
  }



  // master time 2024B
  if (11468 <= e.run && e.run <= 12263) {
    // extract the timing from the trigger "Accepted" (after prescaler) channels
    // normally, only one of these channels should be high
    // more details: maps/2024mu.xml/STRAW.xml
    int ch = 0;
    if (t0tdc.count(31)) ch = 31;  // physical trigger Accepted
    if (t0tdc.count(30)) ch = 30;  // random trigger Accepted
    if (t0tdc.count(29)) ch = 29;  // beam trigger Accepted
    if (t0tdc.count(18)) ch = 18;  // Muon to invisible trigger Accepted
    if (t0tdc.count(19)) ch = 19;  // ECAL trigger Accepted
    const bool tdct0_ok = (ch != 0);

    if (tdct0_ok) {
      // the tdct0_phase is inverted due to the swap of start-stop channels compared to setups before 2023A,B
      e.tdct0_phase = -t0tdc[ch];

      // shift +358. is to put the t_master mean to ~100. ns (by M.Tuzi using run 12071)
      e.masterTime = e.tdct0_phase + 358.;
    }
  }

  // complete SADC reconstruction (best timing and energy)
  // always use kPeak_MaxAmplitude for calibration events
  const int method = e.isCalibration ? kPeak_MaxAmplitude : SADC_peak_method;
  RunWaveReco2(cellsList, method, e.masterTime,e.WB_phase);

  const bool is2018  = (3574 <= e.run && e.run <= 4310);
  const bool is2022B = (6035 <= e.run && e.run <= 8458);
  const bool is2023A = (8459 <= e.run && e.run <= 9717);
  const bool is2024A_nonWB = (10410 <= e.run && e.run <= 11467);
  //const bool is2024A = (10316 <= e.run && e.run <= 19999);
  
  if (is2018 || is2022B || is2023A || is2024A_nonWB) {
    if (SADC_correction_method == kAmplitude_LED) {
      RunLedCorrection(e);
    }
  }

  // A.C. for 2023A session (production runs) we correct ECAL0-2-2 and ECAL1-2-2 spill-by-spill using beam-only events.
  // We also apply on top of this a run-by-run common scaling factor to all ECAL cells
  if (is2023A){
    const bool isPeriod1= (8663 <= e.run) && (e.run <= 8746);
    const bool isPeriod2= (8747 <= e.run) && (e.run <= 8939);
    const bool isPeriod3= (8940 <= e.run) && (e.run <= 9161);
    const bool isPeriod4= (9252 <= e.run) && (e.run <= 9427);
    const bool isPeriod5= (9431 <= e.run) && (e.run <= 9556);
    const bool isPeriodPosi= (9557 <= e.run) && (e.run <= 9588) && (e.run != 9572); //A.C. remove run 9572, it is an hadron run for which we do not have corrections available


    if (isPeriod1||isPeriod2||isPeriod3||isPeriod4||isPeriod5||isPeriodPosi){
      RunECALCentralCellBeamOnlyCorrection(e);
      RunECALExtraFactorCorrection2023A(e);
    }
  }

  //For 2024A, non-WB runs, we have VETO time-walk correction
  if (is2024A_nonWB){
    RunVETOTimeWalkCorrection2024A(e);
  }

  // resolve hodoscopes hits
  DoHodoReco(e.HOD0);
  DoHodoReco(e.HOD1);
  
  DoHodoReco2(e.HOD2);
  /*
  std::cout << "HOD0.xhits =";
  for (size_t i = 0; i < e.HOD0.xhits.size(); ++i)
    std::cout << " " << e.HOD0.xhits[i];
  std::cout << "\n";
  */
 
  // GEM clusterization
  e.GM01->clusterize();
  e.GM02->clusterize();
  e.GM03->clusterize();
  e.GM04->clusterize();
  
  // resolve MM hits
  e.MM1.DoMMReco();
  e.MM2.DoMMReco();
  e.MM3.DoMMReco();
  e.MM4.DoMMReco();
  e.MM5.DoMMReco();
  e.MM6.DoMMReco();
  e.MM7.DoMMReco();
  e.MM8.DoMMReco();
  e.MM9.DoMMReco();
  e.MM10.DoMMReco();
  e.MM11.DoMMReco();
  e.MM12.DoMMReco();
  e.MM13.DoMMReco();

  DoStrawReco(e.ST01, e.tdct0_phase);//third argument: bool verbose -> set to true to get additional info
  DoStrawReco(e.ST02, e.tdct0_phase);
  DoStrawReco(e.ST03, e.tdct0_phase);
  DoStrawReco(e.ST04, e.tdct0_phase);
  DoStrawReco(e.ST05, e.tdct0_phase);
  DoStrawReco(e.ST06, e.tdct0_phase);
  DoStrawReco(e.ST11, e.tdct0_phase);
  DoStrawReco(e.ST12, e.tdct0_phase);
  DoStrawReco(e.ST13, e.tdct0_phase);
  
  return e;
}

//READING OF THE MC DATA

// MC file metadata
// Usage of MCRunInfo:
//   const double M_A = cast<double>(MCRunInfo["aprime_mass"]);
//   const bool hasSetupNumber = (MCRunInfo.find("SetupNumber") != MCRunInfo.end());
//   const int SetupNumber = mcrunoption<int>("SetupNumber", -1);

int MCFileFormatVersion = 0;
map<string,string> MCRunInfo;

// extract value of parameter "name" or return default_value is parameter is absent
template<typename T>
T mcrunoption(const std::string name, const T default_value)
{
  if (MCRunInfo.find(name) != MCRunInfo.end())
    return cast<T>(MCRunInfo[name]);
  
  return default_value;
}

// extract value of parameter "name", rise exception if parameter is absent
template<typename T>
T mcrunoption(const std::string name)
{
  if (MCRunInfo.find(name) != MCRunInfo.end())
    return cast<T>(MCRunInfo[name]);

  // Exception
  throw std::runtime_error(Form("Parameter %s not found in MC file", name.c_str()));
}

bool mcrunHasOption(const std::string name)
{
  return (MCRunInfo.find(name) != MCRunInfo.end());
}

// function throws exception in case of file IO problem or format error
// use `!e.mc` to check for the "end of file" condition
RecoEvent RunP348ReadMC(std::ifstream& inFile)
{
  RecoEvent e;
  e.mc = std::shared_ptr<MCTruth>(new MCTruth);
  
  //A.C. initialize e.mc->DME, e.mc->DME0 and e.mc->DMParentID to default values to tag events with no DM production
  e.mc->DME=-1;
  e.mc->DME0=-1;
  e.mc->DMParentID=0;

  string mytag;
  while (getline(inFile, mytag)) {

    if(mytag == "VERSION") {

      inFile >> MCFileFormatVersion;
      if (MCFileFormatVersion < 3 || MCFileFormatVersion > 9) {
        std::cout << "ERROR: RunP348ReadMC() unsupported input file format version = " << MCFileFormatVersion << std::endl;
        throw std::runtime_error("unsupported MC file format version");
        //break;
      }

    } else if(mytag == "RUN") {

      // read file line by line
      string line;
      while (getline(inFile, line)) {
        if(line == "ENDRUN") break;
        // section properties
        const size_t eqpos = line.find("=");
        if (eqpos != string::npos) {
          // if where is '=' sign in line, this is a property string: key=value
          const string key = line.substr(0, eqpos);
          const string value = line.substr(eqpos + 1);
          std::cout << "In run info found key " << key << ", value = " << value << std::endl;
          MCRunInfo[key] = value;
        }
      }
      
      continue;

    } else if(mytag == "EVENT") {

      int IEvent, ITrig;
      inFile >> IEvent >> ITrig >> e.mc->Weight;
      if(e.mc->Weight == 0.) e.mc->Weight = 1.;


    } else if (mytag == "EVENTSEEDS") {
      int nSeeds;
      inFile >> nSeeds;
      for (int ii = 0; ii < nSeeds; ii++) {
        long seed;
        inFile >> seed;
        e.mc->seeds.push_back(seed);
      }
    } else if (mytag == "FLUKASEEDS") {
      int nSeeds;
      long seedL;
      string seedS,l0;
      e.mc->seedsDumpFLUKA="";
      inFile >> nSeeds;
      inFile.ignore(1000, '\n'); //to be compatible with the use of getline below
      for (int ii = 0; ii < nSeeds; ii++) {
        getline(inFile, seedS);
        if (ii==0) l0=seedS;
        if (ii!=(nSeeds-1)) seedS+="\n";
        e.mc->seedsDumpFLUKA+=seedS;
      }
      //The code for FLUKASEEDS only filled the seedsDumpFLUKA
      //I also extract an integer seed from this
      std::istringstream strm(l0);
      strm>>std::hex>>seedL;
      e.mc->seeds.push_back(seedL);
      continue; //to avoid the ignore instruction at the end of the "mytag" parsing

    } else if (mytag == "USERBANK") {
      int nL;
      string ss;
      inFile>>nL;
      inFile.ignore(1000,'\n'); //to be compatible with the use of getline
      for (int ii=0;ii<nL;ii++){
        getline(inFile,ss);
        e.mc->userbank.push_back(ss);
      }
      continue; //to avoid the ignore instruction at the end of the "mytag" parsing

    } else if(mytag == "MCTRUTH") {

      inFile >> e.mc->ECALEntryX >> e.mc->ECALEntryY;
      // the ECALEntryX,Y is the hit coordinate on the surface of the virtual "MC Truth plane"
      // the surface is 4 mm upstream of ECALStart
      // with the assumption of the track inclination angle of 22 mrad
      // the shift to the front face of ECAL surface:
      e.mc->ECALEntryX -= 0.088;
      
      if(MCFileFormatVersion >= 4) {
        inFile >> e.mc->BeamX0 >> e.mc->BeamY0 >> e.mc->BeamZ0
               >> e.mc->BeamPDG >> e.mc->BeamTotalEnergy
               >> e.mc->BeamPx >> e.mc->BeamPy >> e.mc->BeamPz;
      }

    } else if(mytag == "DMTRUTH") {

     if(MCFileFormatVersion < 8)
      {
       double ea, adx, ady, adz, aa;
       inFile >> ea >> adx >> ady >> adz >> aa;
      }
     else
      {
      double dmxp,dmyp,dmzp,dmxd,dmyd,dmzd,dmpex,dmpey,dmpez,dmppx,dmppy,dmppz;
      inFile >> e.mc->DME0 >> e.mc->DMParentID >> e.mc->DME >> dmxp >> dmyp >> dmzp >> dmxd >> dmyd >> dmzd >> dmpex >> dmpey >> dmpez >> dmppx >> dmppy >> dmppz >> e.mc->DMTRID1 >> e.mc->DMTRID2;
      //assign it
      e.mc->DMP.SetXYZ(dmxp,dmyp,dmzp);
      e.mc->DMD.SetXYZ(dmxd,dmyd,dmzd);
      e.mc->DMPE.SetXYZM(dmpex,dmpey,dmpez,emass);
      e.mc->DMPP.SetXYZM(dmppx,dmppy,dmppz,emass);
      }
     
    }else if (mytag == "TRIGS"){
      inFile >> e.S0.energy >> e.S1.energy>>e.S2.energy>>e.S3.energy>>e.S4.energy;
    }else if (mytag == "TRIGV"){
      inFile >> e.V1.energy >> e.V2.energy;
    }
    else if(mytag == "ECAL") {

      int NCellsX = mcrunoption<int>("ECAL:NCellsECALX", 6);
      ECAL_pos.Nx = NCellsX;
      ECAL_pos.Ny = 6;

      for(int imod=0; imod < 2; imod++) { // Preshower (imod=0), Main ECAL (imod=1)
        for(int iy=0; iy < 6; iy++) {
          for(int ix=0; ix < NCellsX; ix++) {
            inFile >> e.ECAL[imod][ix][iy].energy;
          }
        }
      }

    } else if(mytag == "HCAL") {

      const int nmod = mcrunoption<int>("HCAL:NModules", 4);
      const int NCellsX = mcrunoption<int>("HCAL:NCellsHCALX", 3);
      const int NCellsY = mcrunoption<int>("HCAL:NCellsHCALY", 3);
      HCAL_pos.Nx = NCellsX;
      HCAL_pos.Ny = NCellsY;

      for(int imod=0; imod < nmod; imod++) { // HCAL
        for(int iy=0; iy < NCellsY; iy++) {
          for(int ix=0; ix < NCellsX; ix++) {
            inFile >> e.HCAL[imod][ix][iy].energy;
          }
        }
      }

    } else if(mytag == "LYSO") {

      const int NlysoX = mcrunoption<int>("LYSO:NX", 14);
      const int NlysoY = mcrunoption<int>("LYSO:NY", 5);
      const int NlysoZ = mcrunoption<int>("LYSO:NZ", 1);

      int ilyso=0;
      for(int iz=0; iz < NlysoZ; iz++) { // LYSO
        for(int iy=0; iy < NlysoY; iy++) {
          for(int ix=0; ix < NlysoX; ix++) {
            inFile >> e.LYSO[ilyso++].energy;
          }
        }
      }
    } else if(mytag == "PKRCAL") {

        int NPKRCAL0_N = mcrunoption<int>("PKRCAL:PKRCAL_LAYER_N", 4);
        int NPKRCAL0_CrsN = mcrunoption<int>("PKRCAL:PKRCAL_CRSPRE_N", 10);
        int NPKRCAL1_NCellsX = mcrunoption<int>("PKRCAL:PKRCAL_MATRIX_NX", 9);
        int NPKRCAL1_NCellsY = mcrunoption<int>("PKRCAL:PKRCAL_MATRIX_NY", 9);

        //first, read pre-shower
        for (int ix = 0; ix < NPKRCAL0_N; ix++) {
            for (int iy = 0; iy < NPKRCAL0_CrsN; iy++) {
                inFile >> e.PKRCAL0[ix][iy].energy;
            }
        }

        //second, read matrix
        for (int ix = 0; ix < NPKRCAL1_NCellsX; ix++) {
            for (int iy = 0; iy < NPKRCAL1_NCellsY; iy++) {
                inFile >> e.PKRCAL1[ix][iy].energy;
            }
        }

    } else if(mytag == "SRD") {

      const int nmod = mcrunoption<int>("SRD:NModules", 3);
      
      for(int i = 0; i < nmod; ++i) {
        inFile >> e.SRD[i].energy;
      }

    } else if(mytag == "VETO") {

      inFile >> e.VETO[0].energy >> e.VETO[2].energy >> e.VETO[4].energy;
      e.VETO[1].energy = e.VETO[0].energy;
      e.VETO[3].energy = e.VETO[2].energy;
      e.VETO[5].energy = e.VETO[4].energy;
      // Units in MC are now GeV

    } else if(mytag == "LTG") {

      const int NCells = mcrunoption<int>("LTG:LTGNCells");
      for (int im = 0; im < NCells; im++) {
        inFile >> e.LTG[im].energy;
      }

    } else if(mytag == "MM") {
      vector<int> nhits(8, 0);
      if(MCFileFormatVersion < 8) {
        inFile >> nhits[0] >> nhits[1] >> nhits[2] >> nhits[3];
      } else {
        inFile >> nhits[0] >> nhits[1] >> nhits[2] >> nhits[3] >> nhits[4] >> nhits[5] >> nhits[6] >> nhits[7];
      }
      double tmpx, tmpy, tmpz, tmpv, tmpe;
      int tmpp, tmpid;

      // References and pointers to containers
      vector<vector<MChit>*> MMtruth = {&e.mc->MM1truth, &e.mc->MM2truth, &e.mc->MM3truth, &e.mc->MM4truth, &e.mc->MM5truth, &e.mc->MM6truth, &e.mc->MM7truth, &e.mc->MM8truth};
      if(MCFileFormatVersion < 8) MMtruth = {&e.mc->MM1truth, &e.mc->MM2truth, &e.mc->MM3truth, &e.mc->MM4truth, &e.mc->GEM1truth, &e.mc->GEM2truth, &e.mc->GEM3truth, &e.mc->GEM4truth};

      // Loop over all trackers
      for (size_t idet=0; idet < nhits.size(); ++idet) {

        // Loop over all hits in tracker
        for (int ihit=0; ihit < nhits[idet]; ++ihit) {
          if (MCFileFormatVersion < 5) {
            inFile >> tmpx >> tmpy;
          } else if (MCFileFormatVersion == 5) {
            inFile >> tmpx >> tmpy >> tmpv >> tmpv >> tmpv;
          } else {
            // hit position X,Y,Z [mm]
            // hit energy deposition [eV]
            // hit particle PDG ID code
            // geant4 track ID
            // track kinetic energy [GeV]
            inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe;
            MMtruth[idet]->push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe));
          }
        }
      }

    } else if(mytag == "GEM") {

      int n0, n1, n2, n3;
      inFile >> n0 >> n1 >> n2 >> n3;
      double tmpx, tmpy, tmpz, tmpv, tmpe;
      int tmpp, tmpid;
      for(int ihit=0; ihit < n0; ihit++) {
        // hit position X,Y,Z [mm]
        // hit energy deposition [eV]
        // hit particle PDG ID code
        // geant4 track ID
        // track kinetic energy [GeV]
        inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe;
        e.mc->GEM1truth.push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe));
      }
      for(int ihit=0; ihit < n1; ihit++) {
        inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe;
        e.mc->GEM2truth.push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe));
      }
      for(int ihit=0; ihit < n2; ihit++) {
        inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe;
        e.mc->GEM3truth.push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe));
      }
      for(int ihit=0; ihit < n3; ihit++) {
        inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe;
        e.mc->GEM4truth.push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe));
      }


    } else if(mytag == "STRAW") {
      int n0;
      inFile >> n0;
      double tmpx, tmpy, tmpz, tmpv, tmpe, DistWire, CWire;
      int IStation, IPlane, IWire, tmpp, tmpid, IProj;
      vector<vector<MChit>*> STtruth = {&e.mc->ST1truth, &e.mc->ST2truth, &e.mc->ST3truth, &e.mc->ST4truth,
        &e.mc->ST5truth, &e.mc->ST6truth, &e.mc->ST11truth, &e.mc->ST12truth};
      for(int ihit=0; ihit < n0; ihit++) {
              // StationNb
              // Plane
              // WireNb
              // hit position X,Y,Z [mm]
              // hit particle PDG ID code
              // geant4 track ID
              // track kinetic energy [GeV]
              // hit energy deposition [eV]
              // Straw plane projection (0/1)
              // Distance from wire
              // Wire coordinate in projection above
        //inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe >> tmpt;
        inFile >> IStation >> IPlane >> IWire >> tmpx >> tmpy >> tmpz >> tmpp >> tmpid >> tmpv >> tmpe >> IProj >> DistWire >> CWire;
        const MChit hit(tmpx,tmpy,tmpz,tmpe,tmpp,tmpid,tmpv,0.,IProj,DistWire,CWire,IWire,IPlane);
        STtruth.at(IStation-1)->push_back(hit);
      }

    } else if(mytag == "STRAWTUBE") { // old format, to be obsoleted
      int n0, n1, n2;
      inFile >> n0 >> n1 >> n2;
      double tmpx, tmpy, tmpz, tmpv, tmpe, tmpt;
      int tmpp, tmpid;
      for(int ihit=0; ihit < n0; ihit++) {
              // hit position X,Y,Z [mm]
              // hit energy deposition [eV]
              // hit particle PDG ID code
              // geant4 track ID
              // track kinetic energy [GeV]
              // GlobalTime
        inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe >> tmpt;
        e.mc->ST1truth.push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe,tmpt));
      }
      for(int ihit=0; ihit < n1; ihit++) {
        inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe >> tmpt;
        e.mc->ST2truth.push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe,tmpt));
      }
      for(int ihit=0; ihit < n2; ihit++) {
        inFile >> tmpx >> tmpy >> tmpz >> tmpv >> tmpp >> tmpid >> tmpe >> tmpt;
        e.mc->ST3truth.push_back(MChit (tmpx,tmpy,tmpz,tmpv,tmpp,tmpid,tmpe,tmpt));
      }

    } else if(mytag == "WCAL") {

      if(MCFileFormatVersion < 5) {
        inFile >> e.WCAL[1].energy;
      } else {
        inFile >> e.WCAL[1].energy >> e.WCAT.energy >> e.WCAL[0].energy;
        e.WCAL[1].energy -= e.WCAL[0].energy;
      }

    } else if(mytag == "VTWC") {

      inFile >> e.VTWC.energy;

    } else if(mytag == "VTEC") {

      inFile >> e.VTEC.energy;

    } else if(mytag == "S2EE" || mytag == "S2") {

      inFile >> e.S2.energy;

    } else if (mytag == "V2COUNTER"){

      inFile >> e.V2.energy;

    }

    else if(mytag == "WCOUNTERS") {

      if(MCFileFormatVersion < 7 || mcrunoption<int>("SetupNumber", -1) == 18) {

        inFile >> e.V2.energy >> e.DM[0].energy >> e.S4.energy;
        e.DM[1].energy = e.DM[0].energy;

      } else {

        inFile >> e.WCAL[2].energy >> e.V2.energy >> e.S4.energy;

      }

    } else if(mytag == "ZDCAL") {

      for(int i = 0; i < 2; ++i) {
        inFile >> e.ZDCAL[i].energy;
      }

    } else if(mytag == "VHCAL") {

      for(int iy=0; iy < 4; iy++) {
        for(int ix=0; ix < 4; ix++) {
          inFile >> e.VHCAL[0][ix][iy].energy;
        }
      }

    } else if(mytag == "ENDEVENT") {

      break;

    } else {

      std::cout << "ERROR: unknown tag = " << mytag << std::endl;
      throw std::runtime_error("MC file unknown tag");
      //break;
    }

    // skip the rest of line
    inFile.ignore(1000, '\n');
  }

  // check file state
  //std::cout << "eof()=" << inFile.eof() << " fail()=" << inFile.fail() << endl;
  if (inFile.eof()) e.mc.reset();
  else if (inFile.fail())
    throw std::runtime_error("MC file IO or format error");
  
  return e;
}
