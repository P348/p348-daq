// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TCanvas.h"
#include "TFile.h"
#include "TStyle.h"
#include "TH1.h"
#include "TH2.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "timecut.h"
#include "MManager.h"

// print all global plots to pdf file
void print_global_plots(const TString fname)
{
  TCanvas c;
  
  // open output file
  c.Print(fname + "[");
  
  TIter next(gDirectory->GetList());
  while (TObject* o = next()) {
    //cout << "gdir obj = " << o->GetName() << endl;
    c.Clear();
    o->Draw();
    c.Print(fname);
  }
  
  // close output file
  c.Print(fname + "]");
}

int main(int argc, char* argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./tracking2017.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  // setup input data files list
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i) manager.AddDataSource(argv[i]);
  manager.Print();
  
  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");
  
  // book histograms
  TFile file_histo("tracking.root", "RECREATE");
  
  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  TH2I ehTplot("ehTplot", "ECAL vs HCAL (in-time);ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  
  TH2I mm1("mm1", "Beam Profile on MM1;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm2("mm2", "Beam Profile on MM2;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm3("mm3", "Beam Profile on MM3;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm4("mm4", "Beam Profile on MM4;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm5("mm5", "Beam Profile on MM5;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm7("mm7", "Beam Profile on MM7;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);

  //hit differences
  TH1I MM12X ("MM12X" , "difference in Xhit M12",10000,-100,100);
  TH1I MM12Y ("MM12Y" , "difference in Yhit M12",10000,-100,100);
  TH1I MM34X ("MM34X" , "difference in Xhit M34",10000,-100,100);
  TH1I MM34Y ("MM34Y" , "difference in Yhit M34",10000,-100,100);
  TH1I MM57X ("MM57X" , "difference in Xhit M57",10000,-100,100);
  TH1I MM57Y ("MM57Y" , "difference in Yhit M57",10000,-100,100);

  TH1I mom_simple_old("mom_simple_old", "Reconstructed momentum (simple) using only old MicroMegas; Momentum, GeV/c;#nevents", 300, 0, 150);
  TH1I mom_simple("mom_simple", "Reconstructed momentum (simple); Momentum, GeV/c;#nevents", 300, 0, 150);
  TH1I mom_simple_cut("mom_simple_cut", "Reconstructed momentum (simple) after beam spot and single cluster cut; Momentum, GeV/c;#nevents", 300, 0, 150);
  TH1I mom_genfit("mom_genfit", "Reconstructed momentum (GenFit); Momentum, GeV/c;#nevents", 300, 0, 150);
  TH2I mom_simple_genfit("mom_simple_genfit", "Reconstructed momentum (GenFit vs. simple);Momentum (simple), GeV/c;Momentum (GenFit), GeV/c", 150, 0, 150, 150, 0, 150);
  
  TH1I genfit_chi2ndf("genfit_chi2ndf", "GenFit Chi^2/Ndf;Chi^2/Ndf;nevents", 10000, 0, 2000);
  TH2I genfit_mom_vs_chi2ndf("genfit_mom_vs_chi2ndf", "GenFit Momentum vs Chi^2/Ndf;Chi^2/Ndf;Momentum, GeV/c", 1000, 0, 2000, 300, 0, 150);
  
  TH2I EHCAL_mom_simple("EHCAL_mom_simple", "Momentum (simple) vs ECAL+HCAL;ECAL+HCAL, GeV;Momentum, GeV/c", 150, 0, 150, 150, 0, 150);
  TH2I EHCAL_mom_genfit("EHCAL_mom_genfit", "Momentum (GenFit) vs ECAL+HCAL;ECAL+HCAL, GeV;Momentum, GeV/c", 150, 0, 150, 150, 0, 150);
  TH2I EHCALT_mom_genfit("EHCALT_mom_genfit", "Momentum (GenFit) vs ECAL+HCAL after time cuts;ECAL+HCAL, GeV;Momentum, GeV/c", 150, 0, 150, 150, 0, 150);
  
  ehplot.SetOption("COL");
  ehTplot.SetOption("COL");
  mm1.SetOption("COL");
  mm2.SetOption("COL");
  mm3.SetOption("COL");
  mm4.SetOption("COL");
  mom_simple_genfit.SetOption("COL");
  EHCAL_mom_simple.SetOption("COL");
  EHCAL_mom_genfit.SetOption("COL");

  //preparing MM manager
  MManager man;
  //man.displayOn();
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << nevt << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    RecoEvent e = RunP348Reco(manager);

    //initialize magnet from the geometry of the run
    man.GenfitInit(conddbpath() + "NA64Geom2017.root");
    
    // check trigger type
    if (!e.isPhysics) continue;
    
    const double ecal = e.ecalTotalEnergy();
    const double hcal = e.hcalTotalEnergy();
    
    ehplot.Fill(ecal,hcal);
    
    RecoEvent eT = e;
    timecut(eT, 4);
    const double ecalT = eT.ecalTotalEnergy();
    const double hcalT = eT.hcalTotalEnergy();
    
    ehTplot.Fill(ecalT, hcalT);
    
    if (e.MM1.hasHit()) mm1.Fill(e.MM1.xpos, e.MM1.ypos);
    if (e.MM2.hasHit()) mm2.Fill(e.MM2.xpos, e.MM2.ypos);
    if (e.MM3.hasHit()) mm3.Fill(e.MM3.xpos, e.MM3.ypos);
    if (e.MM4.hasHit()) mm4.Fill(e.MM4.xpos, e.MM4.ypos);
    if (e.MM5.hasHit()) mm5.Fill(e.MM5.xpos, e.MM5.ypos);
    if (e.MM7.hasHit()) mm7.Fill(e.MM7.xpos, e.MM7.ypos);

    //hit difference for alligment
    MM12X.Fill(e.MM1.abspos().X() - e.MM2.abspos().X());
    MM12Y.Fill(e.MM1.abspos().Y() - e.MM2.abspos().Y());
    MM34X.Fill(e.MM3.abspos().X() - e.MM4.abspos().X());
    MM34Y.Fill(e.MM3.abspos().Y() - e.MM4.abspos().Y());
    MM57X.Fill(e.MM5.abspos().X() - e.MM7.abspos().X());
    MM57Y.Fill(e.MM5.abspos().Y() - e.MM7.abspos().Y());


    ///// MOMENTUM /////////
    
    //only original MM and no cut
    man.Reset();
    man.AddMicromega(e.MM1);    
    man.AddMicromega(e.MM3);    
    man.AddMicromega(e.MM5);
    man.AddMicromega(e.MM7);
    if (man.CanTrack()) {
      const SimpleTrack track = man.SimpleTrackingMM();
      const double mom = track.momentum;
      //cout << "mom = " << mom << endl;
      mom_simple_old.Fill(mom);      
    }

    //using all the MM for tracking and no cut
    man.AddEvent(e,NOCUT);
    if (man.CanTrack()) {
      const SimpleTrack track = man.SimpleTrackingMM();
      const double mom = track.momentum;
      //cout << "mom = " << mom << endl;
      mom_simple.Fill(mom);         
    }
    
    //same but with cuts (beam spot and single cluster upstream and just single cluster downstream) and genfit reco
    man.AddEvent(e);
    if (man.CanTrack()) {
      const SimpleTrack track = man.SimpleTrackingMM();
      const double mom = track.momentum;
      //cout << "mom = " << mom << endl;
      mom_simple_cut.Fill(mom);
      //// GENFIT TRACKING
      man.GenfitTrack();
      const double momgen = man.GetGenfitmom();
      const double genchi2 = man.GetGenfitChi2();
      mom_genfit.Fill(momgen);
      genfit_chi2ndf.Fill(genchi2);
      genfit_mom_vs_chi2ndf.Fill(genchi2,momgen);

      //correlation with ecal
      mom_simple_genfit.Fill(mom,momgen);
      EHCAL_mom_simple.Fill(ecal+hcal,mom);
      EHCAL_mom_genfit.Fill(ecal+hcal,momgen);
      EHCALT_mom_genfit.Fill(ecalT+hcalT,momgen);
    }
    
    //if (nevt > 10000) break;
  }
  
  file_histo.Write();
  
  // show event display
  man.displayShow();
  
  print_global_plots("tracking.pdf");
  
  return 0;
}
