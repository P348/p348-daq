// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TCanvas.h"
#include "TStyle.h"
#include "TH2.h"

// c++
#include <iostream>
#include <fstream>
using namespace std;

// P348 reco
#include "p348reco.h"

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./example.exe file.dat" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  manager.AddDataSource(argv[1]);
  manager.Print();
  
  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");
  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  
  // output file
  ofstream fout("output.dat");
  
  if (!fout) {
    cerr << "ERROR: failed to open events output file" << endl;
    return 1;
  }
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics) continue;
    
    // process event reco data
    const double ecal = e.ecalTotalEnergy();
    const double hcal = e.hcalTotalEnergy();
    
    // output event reco summary
    cout << "Event #" << nevt
         << " ECAL = " << ecal << " GeV"
         << " HCAL = " << hcal << " GeV"
         << endl;
    
    if ((ecal < 20) && (hcal < 20)) {
      cout << "INFO: write event, event id ="
           << " run " << e.run
           << " spill " << e.spill
           << " event " << e.spillevent
           << endl;
      
      fout.write((const char*) manager.GetEvent().GetBuffer(), manager.GetEvent().GetLength());
    }
    
    ehplot.Fill(ecal, hcal);
    
    if (nevt > 10000) break;
  }
  
  TCanvas c("output", "output");
  ehplot.Draw("COL");
  c.Print("output.pdf");
  
  return 0;
}
