/* Copyright 2008-2010, Technische Universitaet Muenchen,
   Authors: Christian Hoeppner & Sebastian Neubert & Johannes Rauch

   This file is part of GENFIT.

   GENFIT is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GENFIT is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GENFIT.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ConstField2Box.h"
#include <iostream>

namespace genfit {

TVector3 ConstField2Box::get(const TVector3& pos) const {

  if((pos.X() >= _xmin1 && pos.X() <= _xmax1
     && pos.Y() >= _ymin1 && pos.Y() <= _ymax1
      && pos.Z() >= _zmin1 && pos.Z() <= _zmax1  ) ||
     (pos.X() >= _xmin2 && pos.X() <= _xmax2
      && pos.Y() >= _ymin2 && pos.Y() <= _ymax2
      && pos.Z() >= _zmin2 && pos.Z() <= _zmax2  ) ){
    return field_;
  }else{
    return TVector3(0,0,0);
  }
  return field_;
  
}

void ConstField2Box::get(const double& posX, const double& posY, const double& posZ, double& Bx, double& By, double& Bz) const {
  if((posX >= _xmin1 && posX <= _xmax1
     && posY >= _ymin1 && posY <= _ymax1
      && posZ >= _zmin1 && posZ <= _zmax1) ||
     (posX >= _xmin2 && posX <= _xmax2
      && posY >= _ymin2 && posY <= _ymax2
      && posZ >= _zmin2 && posZ <= _zmax2) ){

    Bx = field_.X();
    By = field_.Y();
    Bz = field_.Z();
    // std::cout << posX << ", " << posY << ", " << posZ << std::endl;
    // std::cout << _zmin << ", " << _zmax << ": " << Bx << ", " << By << ", " << Bz << std::endl;

  }else{
    Bx = 0.;
    By = 0.;
    Bz = 0.;
  }   
}

} /* End of namespace genfit */
