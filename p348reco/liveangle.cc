// this is an example of program with a "live" display

// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TApplication.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TText.h"
#include "TH1.h"
#include "TF1.h"
#include "TMath.h"

// c++
#include <iostream>
#include <ctime>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./liveangle.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  TApplication app("liveangle", 0, 0);
  
  // stats bar format
  gStyle->SetOptStat("emruo");
  gStyle->SetOptFit(11);
  gStyle->SetLineScalePS(1);
  
  TCanvas c1("inangle", "inangle", 20, 20, 800, 600);
  c1.SetGrid();
  c1.SetLogy();
  //c.Connect("TCanvas", "Closed()", "TApplication", &app, "Terminate()");
  
  TCanvas c2("outangle", "outangle", 40, 40, 800, 600);
  c2.SetGrid();
  c2.SetLogy();
  
  /*
  THStack vetoplots("vetoplots", "VETO;Energy, MeV;#nevents");
  veto0plot.SetLineColor(kRed);
  veto0plot.SetMarkerColor(kRed);
  veto0plot.SetMarkerStyle(kFullSquare);
  vetoplots.Add(&veto0plot);
  vetoplots.Draw("nostack");
  c.BuildLegend(0.5, 0.65, 0.7, 0.85);
  */
  
  TH1I inangle("inangle", "Angle between (upstream track) and (axis Z);angle, rad;events", 200, 0, 0.020);
  c1.cd();
  inangle.Draw();
  
  TH1I outangle("outangle", "Angle between (downstream track) and (axis Z);angle, rad;events", 500, 0, 0.050);
  c2.cd();
  outangle.Draw();
  
  TText* t = new TText(0.01, 0.01, "");
  t->SetNDC();
  //t->SetTextAlign();
  t->SetTextFont(82);
  t->SetTextSize(0.04);
  c1.cd();
  t->Draw();
  c2.cd();
  t->Draw();
  
  time_t nextUpdate = 0;
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    const SimpleTrack track = simple_tracking_4MM(e);
    
    if (track.in) inangle.Fill(track.in.theta());
    
    if (track.out) outangle.Fill(track.out.theta());
    
    const time_t now = time(0);
    if (now >= nextUpdate) {
      time_t evtt = manager.GetEvent().GetTime().first;
      char timestr[100];
      strftime(timestr, 100, "%d %b %Y %H:%M:%S %Z", localtime(&evtt));
      
      TString eid;
      eid.Form("Last event ID: run %d spill %d event %d, %s", e.run, e.spill, e.spillevent, timestr);
      t->SetTitle(eid);
      
      {
      const int maxbin = inangle.GetMaximumBin();
      const double maxpos = inangle.GetXaxis()->GetBinCenter(maxbin);
      inangle.Fit("gaus", "QW", "", maxpos-0.003, maxpos+0.003);
      TF1* ft = inangle.GetFunction("gaus");
      if (ft) ft->SetNpx(300);
      //pr->SetAxisRange(maxpos-2*50, maxpos+2*50, "X");
      //pr->Draw();
      }
      //inangle.Fit("gaus");
      //inangle.GetFunction("gaus")->SetNpx(300);
      
      {
      const int maxbin = outangle.GetMaximumBin();
      const double maxpos = outangle.GetXaxis()->GetBinCenter(maxbin);
      outangle.Fit("gaus", "QW", "", maxpos-0.003, maxpos+0.003);
      TF1* ft = outangle.GetFunction("gaus");
      if (ft) ft->SetNpx(300);
      }
      //outangle.Fit("gaus");
      //outangle.GetFunction("gaus")->SetNpx(300);

      c1.Modified();
      c1.Update();
      c2.Modified();
      c2.Update();
      gSystem->ProcessEvents();
      nextUpdate = now + 1; // next update in 1 second
    }
  }
  
  app.Run(kTRUE);
  
  return 0;
}
