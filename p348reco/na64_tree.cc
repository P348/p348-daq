// GenFit
#include <Exception.h>
#include <FieldManager.h>
#include <KalmanFitterRefTrack.h>
#include <DAF.h>
#include <StateOnPlane.h>
#include <Track.h>
#include <TrackPoint.h>

#include <MaterialEffects.h>
#include <RKTrackRep.h>
#include <AbsTrackRep.h>
#include <TGeoMaterialInterface.h>

#include <EventDisplay.h>
#include <PlanarMeasurement.h>
#include <HelixTrackModel.h>
#include <MeasurementCreator.h>

#include "mySpacepointDetectorHit.h"
#include "mySpacepointMeasurement.h"

#include "ConstFieldBox.h"
#include "ConstFieldBox.cc"

// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TStyle.h"
#include <TH1.h>
#include "TH2.h"
#include <TVector3.h>
#include <TGeoManager.h>

// c++
#include <vector>
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"
#include "timecut.h"

int main(int argc, char * argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./na64_tree.exe file1 [file2 ...]" << endl;
    return 1;
  }

  bool UseTimeCut = true;
  int TimeCut = 4; // in sigma

  TFile* file_histo = new TFile("output-histo.root", "RECREATE");

  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH1I e1plot("e1plot", "ECAL1;ECAL, GeV;#nevents", 150, 0, 150);
  TH1I e0plot("e0plot", "ECAL0;ECAL, GeV;#nevents", 150, 0, 150);
  TH1I eplot("eplot", "ECAL;ECAL, GeV;#nevents", 150, 0, 150);
  TH1I hplot("hplot", "HCAL;HCAL, GeV;#nevents", 150, 0, 150);
  TH1I vetoplot("vetoplot", "VETO;VETO,;#nevents", 500, 0, 500);
  TH1I bgoplot("bgoplot", "BGO;Energy, MeV;#nevents", 150, 0, 150);
  TH1I srdplot("srdplot", "SRD;Energy, MeV;#nevents", 150, 0, 150);
  TH1I muonplot("muonplot", "MUON;Energy;#nevents", 100, 0, 100);
  TH1I atargplot("atargplot", "ATARG;Energy;#nevents", 100, 0, 100);
  TH1I hod0X("hod0X", "hod0X;X, mm;#nhits", 35, 0, 35);
  TH1I hod0Y("hod0Y", "hod0Y;Y, mm;#nhits", 35, 0, 35);
  TH2I hod0("hod0", "hod0;X, mm;Y, mm;#nhits", 35, 0, 35, 35, 0, 35);
  TH1I hod1X("hod1X", "hod1X;X, mm;#nhits", 35, 0, 35);
  TH1I hod1Y("hod1Y", "hod1Y;Y, mm;#nhits", 35, 0, 35);
  TH1I hodnhits("hodnhits", "hodnhits;#nhits;#nevents", 10, 0, 10);
  TH1I s0_t("s0_t", "S0 time;#S0 time;#nevents", 1000, 0, 500);
  TH1I ecal133_t("ecal133_t", "ECAL 133-S0  time;#ECAL time;#nevents", 1000, -50, 50);

  TH1I* ST04X = new TH1I("ST04X","X position of hits in ST04;wire",64,0,64);
  TH1I* ST05X = new TH1I("ST05X","X position of hits in ST05;wire",64,0,64);
  TH1I* ST06X = new TH1I("ST06X","X position of hits in ST06;wire",64,0,64);
  TH1I* ST04Y = new TH1I("ST04Y","Y position of hits in ST04;wire",64,0,64);
  TH1I* ST05Y = new TH1I("ST05Y","Y position of hits in ST05;wire",64,0,64);
  TH1I* ST06Y = new TH1I("ST06Y","Y position of hits in ST06;wire",64,0,64);
  TH1I* ST04X_t = new TH1I("ST04X_t","time of x hits in ST04;wire",140,0,1400);
  TH1I* ST05X_t = new TH1I("ST05X_t","time of x hits in ST05;wire",140,0,1400);
  TH1I* ST06X_t = new TH1I("ST06X_t","time of x hits in ST06;wire",140,0,1400);
  TH1I* ST04Y_t = new TH1I("ST04Y_t","time of y hits in ST04;wire",140,0,1400);
  TH1I* ST05Y_t = new TH1I("ST05Y_t","time of y hits in ST05;wire",140,0,1400);
  TH1I* ST06Y_t = new TH1I("ST06Y_t","time of y hits in ST06;wire",140,0,1400);
  
  TH1I* mm1X = new TH1I("mm1X", "X position of beam on MM1;x(mm)", 1000, -100, 100);
  TH1I* mm1Y = new TH1I("mm1Y", "Y position of beam on MM1;y(mm)", 1000, -100, 100);
  TH1I* mm2X = new TH1I("mm2X", "X position of beam on MM2;x(mm)", 1000, -100, 100);
  TH1I* mm2Y = new TH1I("mm2Y", "Y position of beam on MM2;y(mm)", 1000, -100, 100);
  TH1I* mm3X = new TH1I("mm3X", "X position of beam on MM3;x(mm)", 1000, -100, 100);
  TH1I* mm3Y = new TH1I("mm3Y", "Y position of beam on MM3;y(mm)", 1000, -100, 100);
  TH1I* mm4X = new TH1I("mm4X", "X position of beam on MM4;x(mm)", 1000, -100, 100);
  TH1I* mm4Y = new TH1I("mm4Y", "Y position of beam on MM4;y(mm)", 1000, -100, 100);
  TH2I* mm1 = new TH2I("mm1", "Beam Profile on MM1;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);
  TH2I* mm2 = new TH2I("mm2", "Beam Profile on MM2;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);
  TH2I* mm3 = new TH2I("mm3", "Beam Profile on MM3;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);
  TH2I* mm4 = new TH2I("mm4", "Beam Profile on MM4;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);

  TH1I* mm1X_time = new TH1I("mm1X_time", "X time of beam on MM1;x(mm)", 1000, 0, 500);
  TH1I* mm1Y_time = new TH1I("mm1Y_time", "Y time of beam on MM1;y(mm)", 1000, 0, 500);
  TH1I* mm2X_time = new TH1I("mm2X_time", "X time of beam on MM2;x(mm)", 1000, 0, 500);
  TH1I* mm2Y_time = new TH1I("mm2Y_time", "Y time of beam on MM2;y(mm)", 1000, 0, 500);
  TH1I* mm3X_time = new TH1I("mm3X_time", "X time of beam on MM3;x(mm)", 1000, 0, 500);
  TH1I* mm3Y_time = new TH1I("mm3Y_time", "Y time of beam on MM3;y(mm)", 1000, 0, 500);
  TH1I* mm4X_time = new TH1I("mm4X_time", "X time of beam on MM4;x(mm)", 1000, 0, 500);
  TH1I* mm4Y_time = new TH1I("mm4Y_time", "Y time of beam on MM4;y(mm)", 1000, 0, 500);

  TH1I* mm1X_time0 = new TH1I("mm1X_time0", "X time of beam on MM1-master;t(ns)", 1000, -400,0);
  TH1I* mm1Y_time0 = new TH1I("mm1Y_time0", "Y time of beam on MM1-master;t(ns)", 1000, -400, 0);
  TH1I* mm2X_time0 = new TH1I("mm2X_time0", "X time of beam on MM2-master;t(ns)", 1000, -400, 0);
  TH1I* mm2Y_time0 = new TH1I("mm2Y_time0", "Y time of beam on MM2-master;t(ns)", 1000, -400, 0);
  TH1I* mm3X_time0 = new TH1I("mm3X_time0", "X time of beam on MM3-master;t(ns)", 1000, -400, 0);
  TH1I* mm3Y_time0 = new TH1I("mm3Y_time0", "Y time of beam on MM3-master;t(ns)", 1000, -400, 0);
  TH1I* mm4X_time0 = new TH1I("mm4X_time0", "X time of beam on MM4-master;t(ns)", 1000, -400, 0);
  TH1I* mm4Y_time0 = new TH1I("mm4Y_time0", "Y time of beam on MM4-master;t(ns)", 1000, -400, 0);

  
  TH1I* mm1X_mult = new TH1I("mm1X_mult", "X mult of beam on MM1;x(mm)", 100, 0, 100);
  TH1I* mm1Y_mult = new TH1I("mm1Y_mult", "Y mult of beam on MM1;y(mm)", 100, 0, 100);
  TH1I* mm2X_mult = new TH1I("mm2X_mult", "X mult of beam on MM2;x(mm)", 100, 0, 100);
  TH1I* mm2Y_mult = new TH1I("mm2Y_mult", "Y mult of beam on MM2;y(mm)", 100, 0, 100);
  TH1I* mm3X_mult = new TH1I("mm3X_mult", "X mult of beam on MM3;x(mm)", 100, 0, 100);
  TH1I* mm3Y_mult = new TH1I("mm3Y_mult", "Y mult of beam on MM3;y(mm)", 100, 0, 100);
  TH1I* mm4X_mult = new TH1I("mm4X_mult", "X mult of beam on MM4;x(mm)", 100, 0, 100);
  TH1I* mm4Y_mult = new TH1I("mm4Y_mult", "Y mult of beam on MM4;y(mm)", 100, 0, 100);


  TH1I* momentum = new TH1I("momentum", "Reconstructed momentum for electron;Energy(GeV);Entries", 1000, 0, 200);
  TH2I* ECAL_pos_mom = new TH2I("ECAL_pos_mom", "Beam Profile on ECAL for all momentums;x(mm);y(mm)", 3000, -500, -100, 3000, -150, 150);
  TH2I* ECAL_mom = new TH2I("ECAL_mom", "Momentum vs ECAL;ECAL(GeV);Momentum(GeV)", 1000, 0, 200, 1000, 0, 200);
  TH2I* ECAL_mom_cut = new TH2I("ECAL_mom_cut", "Momentum vs ECAL;ECAL(GeV);Momentum(GeV)", 1000, 0, 200, 1000, 0, 200);

  bool displayon = false;

  // Micromegas positions
  double z1,z2, zmU,zmD, z3, z4; //distances in cm 
  double scalecorr = 0.1;
 
  z1 = MM1_pos.pos.Z()-MM1_pos.pos.Z();
  z2 = MM1_pos.pos.Z()-MM2_pos.pos.Z();
  z3 = MM1_pos.pos.Z()-MM3_pos.pos.Z();//   z3 = 17641;mag - z3 10.78 m , mag-z4 11.95 m
  z4 = MM1_pos.pos.Z()-MM4_pos.pos.Z();//   z4 = 18861;
  zmU= z2+598;//magnet
  double mag_l = 4000; // magnet
  zmD = zmU+mag_l;//magnet

  // z3 = zmD + 10780;
  // z4 = zmD + 11950;
  
 
  // init geometry and mag. field
  new TGeoManager("Geometry", "NA64 geometry");
  TGeoManager::Import((conddbpath() + "NA64Geom2016B.root").c_str());
  genfit::ConstFieldBox field(0.,13.61, 0.0, -100, 100, -100, 100, zmU, zmD); // kGauss
  genfit::FieldManager * fman = genfit::FieldManager::getInstance();
  fman->init(&field);
  genfit::TGeoMaterialInterface matinterf;
  genfit::MaterialEffects * mateff = genfit::MaterialEffects::getInstance();
  mateff->init(&matinterf);
  
  const int pdg = 11;               // particle pdg code,

  // init fitter blah
  genfit::AbsKalmanFitter* fitter = new genfit::KalmanFitterRefTrack();
  //  genfit::AbsKalmanFitter* fitter = new genfit::DAF();
  fitter->setMinIterations(5);
  fitter->setMaxIterations(8);
  cout << "Iterations: " << fitter->getMinIterations() << ", " << fitter->getMaxIterations() << endl;
  TVector3 posM1;
  TVector3 momM;
  TVector3 posM2;
  TVector3 posM3;
  TVector3 posM4;
  
  
  
  TClonesArray myDetectorHitArray("genfit::mySpacepointDetectorHit");
  // init the measurement factoryx
  int myDetId(1);
  genfit::MeasurementFactory<genfit::AbsMeasurement> factory;
  typedef genfit::MeasurementProducer<genfit::mySpacepointDetectorHit, genfit::mySpacepointMeasurement> MyMeasurementProducer;
  factory.addProducer(myDetId, new MyMeasurementProducer(&myDetectorHitArray));
  genfit::AbsTrackRep* rep = NULL;
  
  TFile* file_tree = new TFile("output-tree.root", "RECREATE");
  TTree* tree = new TTree("tree","Tree");
    

  int run_num;
  int event;
  int m1x_ci;
  int m1y_ci;
  int m2x_ci;
  int m2y_ci;
  int m3x_ci;
  int m3y_ci;
  int m4x_ci;
  int m4y_ci;
  double MM1x_bestpos;
  double MM1y_bestpos;
  double MM2x_bestpos;
  double MM2y_bestpos;
  double MM3x_bestpos;
  double MM3y_bestpos;
  double MM4x_bestpos;
  double MM4y_bestpos;
  vector<double>*  M1x_pos = new vector<double>;
  vector<double>*  M1x_charge = new vector<double>;
  vector<double>*  M1x_mult = new vector<double>;
  vector<double>* M1x_time = new vector<double>;
  vector<double>*  M1y_pos = new vector<double>;
  vector<double>*  M1y_charge = new vector<double>;
  vector<double>*  M1y_mult = new vector<double>;
  vector<double>* M1y_time = new vector<double>;
  vector<double>*  M2x_pos = new vector<double>;
  vector<double>*  M2x_charge = new vector<double>;
  vector<double>*  M2x_mult = new vector<double>;
  vector<double>* M2x_time = new vector<double>;
  vector<double>*  M2y_pos = new vector<double>;
  vector<double>*  M2y_charge = new vector<double>;
  vector<double>*  M2y_mult = new vector<double>;
  vector<double>* M2y_time = new vector<double>;
  vector<double>*  M3x_pos = new vector<double>;
  vector<double>*  M3x_charge = new vector<double>;
  vector<double>*  M3x_mult = new vector<double>;
  vector<double>* M3x_time = new vector<double>;
  vector<double>*  M3y_pos = new vector<double>;
  vector<double>*  M3y_charge = new vector<double>;
  vector<double>*  M3y_mult = new vector<double>;
  vector<double>* M3y_time = new vector<double>;
  vector<double>*  M4x_pos = new vector<double>;
  vector<double>*  M4x_charge = new vector<double>;
  vector<double>*  M4x_mult = new vector<double>;
  vector<double>* M4x_time = new vector<double>;
  vector<double>*  M4y_pos = new vector<double>;
  vector<double>*  M4y_charge = new vector<double>;
  vector<double>*  M4y_mult = new vector<double>;
  vector<double>* M4y_time = new vector<double>;
  vector<vector<double>>*  ecal0_e = new vector<vector<double>>;
  vector<vector<double>>*  ecal0_t = new vector<vector<double>>;
  vector<vector<double>>*  ecal0_p0 = new vector<vector<double>>;
  vector<vector<double>>*  ecal0_p1 = new vector<vector<double>>;
  vector<vector<double>>* ecal0_of = new vector<vector<double>>;
  vector<vector<double>>*   ecal1_e = new vector<vector<double>>;
  vector<vector<double>>*   ecal1_t = new vector<vector<double>>;
  vector<vector<double>>*   ecal1_p0 = new vector<vector<double>>;
  vector<vector<double>>*   ecal1_p1 = new vector<vector<double>>;
  vector<vector<double>>*   ecal1_of = new vector<vector<double>>;
  vector<vector<double>>*  hcal0_e = new vector<vector<double>>;
  vector<vector<double>>*  hcal0_t = new vector<vector<double>>;
  vector<vector<double>>*  hcal0_p0 = new vector<vector<double>>;
  vector<vector<double>>*  hcal0_p1 = new vector<vector<double>>;
  vector<vector<double>>*  hcal0_of = new vector<vector<double>>;
  vector<vector<double>>*  hcal1_e = new vector<vector<double>>;
  vector<vector<double>>*  hcal1_t = new vector<vector<double>>;
  vector<vector<double>>*  hcal1_p0 = new vector<vector<double>>;
  vector<vector<double>>*  hcal1_p1 = new vector<vector<double>>;
  vector<vector<double>>*  hcal1_of = new vector<vector<double>>;
  vector<vector<double>>*  hcal2_e = new vector<vector<double>>;
  vector<vector<double>>*  hcal2_t = new vector<vector<double>>;
  vector<vector<double>>*  hcal2_p0 = new vector<vector<double>>;
  vector<vector<double>>*  hcal2_p1 = new vector<vector<double>>;
  vector<vector<double>>*  hcal2_of = new vector<vector<double>>;
  vector<vector<double>>*  hcal3_e = new vector<vector<double>>;
  vector<vector<double>>*  hcal3_t = new vector<vector<double>>;
  vector<vector<double>>*  hcal3_p0 = new vector<vector<double>>;
  vector<vector<double>>*  hcal3_p1 = new vector<vector<double>>;
  vector<vector<double>>*  hcal3_of = new vector<vector<double>>;
  /*vector<int>* ST4allX_t = new vector<int>;
  vector<int>* ST4allX_w = new vector<int>;
  vector<int>* ST5allX_t = new vector<int>;
  vector<int>* ST5allX_w = new vector<int>;
  vector<int>* ST6allX_t = new vector<int>;
  vector<int>* ST6allX_w = new vector<int>;
  vector<int>* ST4allY_t = new vector<int>;
  vector<int>* ST4allY_w = new vector<int>;
  vector<int>* ST5allY_t = new vector<int>;
  vector<int>* ST5allY_w = new vector<int>;
  vector<int>* ST6allY_t = new vector<int>;
  vector<int>* ST6allY_w = new vector<int>;*/
  vector<double>* ST4X = new vector<double>;
  vector<double>* ST5X = new vector<double>;
  vector<double>* ST6X = new vector<double>;
  vector<double>* ST4Y = new vector<double>;
  vector<double>* ST5Y = new vector<double>;
  vector<double>* ST6Y = new vector<double>;
  vector<double>* ST4X_t = new vector<double>;
  vector<double>* ST5X_t = new vector<double>;
  vector<double>* ST6X_t = new vector<double>;
  vector<double>* ST4Y_t = new vector<double>;
  vector<double>* ST5Y_t = new vector<double>;
  vector<double>* ST6Y_t = new vector<double>;
  vector<int>* ST4X_c = new vector<int>;
  vector<int>* ST4Y_c = new vector<int>;
  vector<int>* ST5X_c = new vector<int>;
  vector<int>* ST5Y_c = new vector<int>;
  vector<int>* ST6X_c = new vector<int>;
  vector<int>* ST6Y_c = new vector<int>;
  vector<double>*  bgo_e = new vector<double>;
  vector<double>*  bgo_t = new vector<double>;
  vector<double>*  bgo_p0 = new vector<double>;
  vector<double>*  bgo_p1 = new vector<double>;
  vector<int>*  bgo_of = new vector<int>;
  vector<double>*  veto_e = new vector<double>;
  vector<double>*  veto_t = new vector<double>;
  vector<double>*  veto_p0 = new vector<double>;
  vector<double>*  veto_p1 = new vector<double>;
  vector<int>*  veto_of = new vector<int>;
  vector<double>*  srd_e = new vector<double>;
  vector<double>*  srd_t = new vector<double>;
  vector<double>*  srd_p0 = new vector<double>;
  vector<double>*  srd_p1 = new vector<double>;
  vector<int>*  srd_of = new vector<int>;
  double s1_e;
  double s1_t;
  double s1_p0;
  double s1_p1;
  double s1_of;
  double s2_t;
  double ecal;
  double hcal;
  double ecal_hitx;
  double ecal_hity;
  double part_mom;
  double genfit_mom;
  double chi;
  double masterTime;
    
    tree->Branch("run_num",&run_num);
    tree->Branch("event",&event);
    tree->Branch("MM1x_bestpos",&MM1x_bestpos);
    tree->Branch("MM1y_bestpos",&MM1y_bestpos);
    tree->Branch("MM2x_bestpos",&MM2x_bestpos);
    tree->Branch("MM2y_bestpos",&MM2y_bestpos);
    tree->Branch("MM3x_bestpos",&MM3x_bestpos);
    tree->Branch("MM3y_bestpos",&MM3y_bestpos);
    tree->Branch("MM4x_bestpos",&MM4x_bestpos);
    tree->Branch("MM4y_bestpos",&MM4y_bestpos);
    tree->Branch("m1x_ci",&m1x_ci);
    tree->Branch("m1y_ci",&m1y_ci);
    tree->Branch("m2x_ci",&m2x_ci);
    tree->Branch("m2y_ci",&m2y_ci);
    tree->Branch("m3x_ci",&m3x_ci);
    tree->Branch("m3y_ci",&m3y_ci);
    tree->Branch("m4x_ci",&m4x_ci);
    tree->Branch("m4y_ci",&m4y_ci);
    tree->Branch("M1x_pos",&M1x_pos);
    tree->Branch("M1x_charge",&M1x_charge);
    tree->Branch("M1x_mult",&M1x_mult);
    tree->Branch("M1x_time",&M1x_time);
    tree->Branch("M2x_pos",&M2x_pos);
    tree->Branch("M2x_charge",&M2x_charge);
    tree->Branch("M2x_mult",&M2x_mult);
    tree->Branch("M2x_time",&M2x_time);
    tree->Branch("M3x_pos",&M3x_pos);
    tree->Branch("M3x_charge",&M3x_charge);
    tree->Branch("M3x_mult",&M3x_mult);
    tree->Branch("M3x_time",&M3x_time);
    tree->Branch("M4x_pos",&M4x_pos);
    tree->Branch("M4x_charge",&M4x_charge);
    tree->Branch("M4x_mult",&M4x_mult);
    tree->Branch("M4x_time",&M4x_time);
    tree->Branch("M1y_pos",&M1y_pos);
    tree->Branch("M1y_charge",&M1y_charge);
    tree->Branch("M1y_mult",&M1y_mult);
    tree->Branch("M1y_time",&M1y_time);
    tree->Branch("M2y_pos",&M2y_pos);
    tree->Branch("M2y_charge",&M2y_charge);
    tree->Branch("M2y_mult",&M2y_mult);
    tree->Branch("M2y_time",&M2y_time);
    tree->Branch("M3y_pos",&M3y_pos);
    tree->Branch("M3y_charge",&M3y_charge);
    tree->Branch("M3y_mult",&M3y_mult);
    tree->Branch("M3y_time",&M3y_time);
    tree->Branch("M4y_pos",&M4y_pos);
    tree->Branch("M4y_charge",&M4y_charge);
    tree->Branch("M4y_mult",&M4y_mult);
    tree->Branch("M4y_time",&M4y_time);
    tree->Branch("ecal0_e",&ecal0_e);
    tree->Branch("ecal0_t",&ecal0_t);
    tree->Branch("ecal0_p0",&ecal0_p0);
    tree->Branch("ecal0_p1",&ecal0_p1);
    tree->Branch("ecal0_of",&ecal0_of);
    tree->Branch("ecal1_e",&ecal1_e);
    tree->Branch("ecal1_t",&ecal1_t);
    tree->Branch("ecal1_p0",&ecal1_p0);
    tree->Branch("ecal1_p1",&ecal1_p1);
    tree->Branch("ecal1_of",&ecal1_of);
    tree->Branch("hcal0_e",&hcal0_e);
    tree->Branch("hcal0_t",&hcal0_t);
    tree->Branch("hcal0_p0",&hcal0_p0);
    tree->Branch("hcal0_p1",&hcal0_p1);
    tree->Branch("hcal0_of",&hcal0_of);
    tree->Branch("hcal1_e",&hcal1_e);
    tree->Branch("hcal1_t",&hcal1_t);
    tree->Branch("hcal1_p0",&hcal1_p0);
    tree->Branch("hcal1_p1",&hcal1_p1);
    tree->Branch("hcal1_of",&hcal1_of);
    tree->Branch("hcal2_e",&hcal2_e);
    tree->Branch("hcal2_t",&hcal2_t);
    tree->Branch("hcal2_p0",&hcal2_p0);
    tree->Branch("hcal2_p1",&hcal2_p1);
    tree->Branch("hcal2_of",&hcal2_of);
    tree->Branch("hcal3_e",&hcal3_e);
    tree->Branch("hcal3_t",&hcal3_t);
    tree->Branch("hcal3_p0",&hcal3_p0);
    tree->Branch("hcal3_p1",&hcal3_p1);
    tree->Branch("hcal3_of",&hcal3_of);
    tree->Branch("bgo_e",&bgo_e);
    tree->Branch("bgo_t",&bgo_t);
    tree->Branch("bgo_p0",&bgo_p0);
    tree->Branch("bgo_p1",&bgo_p1);
    tree->Branch("bgo_of",&bgo_of);
    tree->Branch("veto_e",&veto_e);
    tree->Branch("veto_t",&veto_t);
    tree->Branch("veto_p0",&veto_p0);
    tree->Branch("veto_p1",&veto_p1);
    tree->Branch("veto_of",&veto_of);
    tree->Branch("srd_e",&srd_e);
    tree->Branch("srd_t",&srd_t);
    tree->Branch("srd_p0",&srd_p0);
    tree->Branch("srd_p1",&srd_p1);
    tree->Branch("srd_of",&srd_of);
    tree->Branch("s1_e",&s1_e);
    tree->Branch("s1_t",&s1_t);
    tree->Branch("s1_p0",&s1_p0);
    tree->Branch("s1_p1",&s1_p1);
    tree->Branch("s1_of",&s1_of);
    tree->Branch("s2_t",&s2_t);
    tree->Branch("ecal_hitx",&ecal_hitx);
    tree->Branch("ecal_hity",&ecal_hity);
    tree->Branch("ecal",&ecal);
    tree->Branch("hcal",&hcal);
    tree->Branch("part_mom",&part_mom);
    tree->Branch("genfit_mom",&genfit_mom);
    tree->Branch("masterTime",&masterTime);

    tree->Branch("ST4X",&ST4X);
    tree->Branch("ST5X",&ST5X);
    tree->Branch("ST6X",&ST6X);
    tree->Branch("ST4Y",&ST4Y);
    tree->Branch("ST5Y",&ST5Y);
    tree->Branch("ST6Y",&ST6Y);
    tree->Branch("ST4X_t",&ST4X_t);
    tree->Branch("ST5X_t",&ST5X_t);
    tree->Branch("ST6X_t",&ST6X_t);
    tree->Branch("ST4Y_t",&ST4Y_t);
    tree->Branch("ST5Y_t",&ST5Y_t);
    tree->Branch("ST6Y_t",&ST6Y_t);
    tree->Branch("ST4X_c",&ST4X_c);
    tree->Branch("ST5X_c",&ST5X_c);
    tree->Branch("ST6X_c",&ST6X_c);
    tree->Branch("ST4Y_c",&ST4Y_c);
    tree->Branch("ST5Y_c",&ST5Y_c);
    tree->Branch("ST6Y_c",&ST6Y_c);
    tree->Branch("chi",&chi);

  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i) manager.AddDataSource(argv[i]);
  manager.Print();

  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");
  
  
    
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
 
    MM1x_bestpos=-99999;
    MM1y_bestpos=-99999;
    MM2x_bestpos=-99999;
    MM2y_bestpos=-99999;
    MM3x_bestpos=-99999;
    MM3y_bestpos=-99999;
    MM4x_bestpos=-99999;
    MM4y_bestpos=-99999;
    
    part_mom=-99999;
    genfit_mom=-99999;
    ecal_hitx=-99999;
    ecal_hity=-99999;
    

    M1x_pos->clear();
    M1x_charge->clear();
    M1x_mult->clear();
    M1x_time->clear();
        
    M1y_pos->clear();
    M1y_charge->clear();
    M1y_mult->clear();
    M1y_time->clear();

    M2x_pos->clear();
    M2x_charge->clear();
    M2x_mult->clear();
    M2x_time->clear();
    
    M2y_pos->clear();
    M2y_charge->clear();
    M2y_mult->clear();
    M2y_time->clear();
    
    M3x_pos->clear();
    M3x_charge->clear();
    M3x_mult->clear();
    M3x_time->clear();
         
    M3y_pos->clear();
    M3y_charge->clear();
    M3y_mult->clear();
    M3y_time->clear();
       
    M4x_pos->clear();
    M4x_charge->clear();
    M4x_mult->clear();
    M4x_time->clear();
    
    M4y_pos->clear();
    M4y_charge->clear();
           M4y_mult->clear();
    M4y_time->clear();

    ecal0_e->clear();
    ecal0_t->clear();
    ecal0_p0->clear();
    ecal0_p1->clear();
    ecal0_of->clear();
           
    ecal1_e->clear();
    ecal1_t->clear();
    ecal1_p0->clear();
    ecal1_p1->clear();
    ecal1_of->clear();
    hcal0_e->clear();
    hcal0_t->clear();
    hcal0_p0->clear();
    hcal0_p1->clear();
    hcal0_of->clear();
    hcal1_e->clear();
    hcal1_t->clear();
    hcal1_p0->clear();
    hcal1_p1->clear();
    hcal1_of->clear();
    hcal2_e->clear();
    hcal2_t->clear();
    hcal2_p0->clear();
    hcal2_p1->clear();
    hcal2_of->clear();
    hcal3_e->clear();
    hcal3_t->clear();
    hcal3_p0->clear();
    hcal3_p1->clear();
    hcal3_of->clear();
    bgo_e->clear();
    bgo_t->clear();
    bgo_p0->clear();
    bgo_p1->clear();
    bgo_of->clear();
    veto_e->clear();
    veto_t->clear();
    veto_p0->clear();
    veto_p1->clear();
    veto_of->clear();
    srd_e->clear();
    srd_t->clear();
    srd_p0->clear();
    srd_p1->clear();
    srd_of->clear();
    ST4X->clear();
    ST5X->clear();
    ST6X->clear();
    ST4Y->clear();
    ST5Y->clear();
    ST6Y->clear();
    ST4X_t->clear();
    ST5X_t->clear();
    ST6X_t->clear();
    ST4Y_t->clear();
    ST5Y_t->clear();
    ST6Y_t->clear();
    ST4X_c->clear();
    ST5X_c->clear();
    ST6X_c->clear();
    ST4Y_c->clear();
    ST5Y_c->clear();
    ST6Y_c->clear();   

    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << nevt << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    // run reconstruction
    RecoEvent e = RunP348Reco(manager);

    run_num = e.run;
    event = e.runevent;
    //    cout << e.masterTime << endl;  
    const DaqEvent::EventType type = manager.GetEvent().GetType();
     //if (type != DaqEvent::PHYSICS_EVENT) continue;
    
    if(UseTimeCut)
      timecut(e, TimeCut);

    //cout << "S1 print:\n" << e.S1 << endl;
    //cout << "ECAL[0][3][3] print:\n" << e.ECAL[0][3][3] << endl;
    
    // process only "physics" events (no random or calibration trigger)
    //    if (!e.isPhysics) continue;
    if(type==DaqEvent::PHYSICS_EVENT)
      {

    // process event reco data
    ecal = 0;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal += e.ECAL[d][x][y].energy;

    double ecal1 = 0;
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal1 += e.ECAL[1][x][y].energy;

    double ecal0 = 0;
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal0 += e.ECAL[0][x][y].energy;


    vector<double> e_energy(6);
    vector<double> e_time(6);
    vector<double> e_p0(6);
    vector<double> e_p1(6);
    vector<double> e_of(6);
    for(int x=0;x<6;x++)
      {
        for(int y=0;y<6;y++)
          {
            e_energy[y] = e.ECAL[0][x][y].energy;
            e_time[y] = e.ECAL[0][x][y].t0;
            e_p0[y] = e.ECAL[0][x][y].pedestal0;
            e_p1[y] = e.ECAL[0][x][y].pedestal1;
            e_of[y] = e.ECAL[0][x][y].hasOverflow() ? 1 : 0;
           
          }
        ecal0_e->push_back(e_energy);
        ecal0_t->push_back(e_time);
        ecal0_p0->push_back(e_p0);
        ecal0_p1->push_back(e_p1);
        ecal0_of->push_back(e_of);
      }

    for(int x=0;x<6;x++)
      {
        for(int y=0;y<6;y++)
          {
            e_energy[y] = e.ECAL[1][x][y].energy;
            e_time[y] = e.ECAL[1][x][y].t0;
            e_p0[y] = e.ECAL[1][x][y].pedestal0;
            e_p1[y] = e.ECAL[1][x][y].pedestal1;
            e_of[y] = e.ECAL[1][x][y].hasOverflow() ? 1 : 0;
           
          }
        ecal1_e->push_back(e_energy);
        ecal1_t->push_back(e_time);
        ecal1_p0->push_back(e_p0);
        ecal1_p1->push_back(e_p1);
        ecal1_of->push_back(e_of);
      }

     
    hcal = 0;
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          hcal += e.HCAL[d][x][y].energy;


    
    vector<double> h_energy(3);
    vector<double> h_time(3);
    vector<double> h_p0(3);
    vector<double> h_p1(3);
    vector<double> h_of(3);
    
        for(int x=0;x<3;x++)
          {
            for(int y=0;y<3;y++)
              {
                h_energy[y] = e.HCAL[0][x][y].energy;
                h_time[y] = e.HCAL[0][x][y].t0;
                h_p0[y] = e.HCAL[0][x][y].pedestal0;
                h_p1[y] = e.HCAL[0][x][y].pedestal1;
                h_of[y] = e.HCAL[0][x][y].hasOverflow() ? 1 : 0;
              }
            hcal0_e->push_back(h_energy);
            hcal0_t->push_back(h_time);
            hcal0_p0->push_back(h_p0);
            hcal0_p1->push_back(h_p1);
            hcal0_of->push_back(h_of);
          }

        for(int x=0;x<3;x++)
          {
            for(int y=0;y<3;y++)
              {
                h_energy[y] = e.HCAL[1][x][y].energy;
                h_time[y] = e.HCAL[1][x][y].t0;
                h_p0[y] = e.HCAL[1][x][y].pedestal0;
                h_p1[y] = e.HCAL[1][x][y].pedestal1;
                h_of[y] = e.HCAL[1][x][y].hasOverflow() ? 1 : 0;
              }
            hcal1_e->push_back(h_energy);
            hcal1_t->push_back(h_time);
            hcal1_p0->push_back(h_p0);
            hcal1_p1->push_back(h_p1);
            hcal1_of->push_back(h_of);
          }
  
        for(int x=0;x<3;x++)
          {
            for(int y=0;y<3;y++)
              {
                h_energy[y] = e.HCAL[2][x][y].energy;
                h_time[y] = e.HCAL[2][x][y].t0;
                h_p0[y] = e.HCAL[2][x][y].pedestal0;
                h_p1[y] = e.HCAL[2][x][y].pedestal1;
                h_of[y] = e.HCAL[2][x][y].hasOverflow() ? 1 : 0;
              }
            hcal2_e->push_back(h_energy);
            hcal2_t->push_back(h_time);
            hcal2_p0->push_back(h_p0);
            hcal2_p1->push_back(h_p1);
            hcal2_of->push_back(h_of);
          }

        for(int x=0;x<3;x++)
          {
            for(int y=0;y<3;y++)
              {
                h_energy[y] = e.HCAL[3][x][y].energy;
                h_time[y] = e.HCAL[3][x][y].t0;
                h_p0[y] = e.HCAL[3][x][y].pedestal0;
                h_p1[y] = e.HCAL[3][x][y].pedestal1;
                h_of[y] = e.HCAL[3][x][y].hasOverflow() ? 1 : 0;
              }
            hcal3_e->push_back(h_energy);
            hcal3_t->push_back(h_time);
            hcal3_p0->push_back(h_p0);
            hcal3_p1->push_back(h_p1);
            hcal3_of->push_back(h_of);
          }
    
    double bgo = 0;
    for (int x = 0; x < 8; ++x)
      {
      bgo += e.BGO[x].energy;
      bgo_e->push_back(e.BGO[x].energy/MeV);
      bgo_t->push_back(e.BGO[x].t0);
      bgo_p0->push_back(e.BGO[x].pedestal0);
      bgo_p1->push_back(e.BGO[x].pedestal1);
      bgo_of->push_back(e.BGO[x].hasOverflow() ? 1 : 0);
      }
    

     double muon = 0;
    for (int d = 0; d < 4; ++d)
      muon += e.MUON[d].energy;
    
    double atarg = 0;
    for (int x = 0; x < 5; ++x)
      for (int y = 0; y < 5; ++y)
        atarg += e.ATARG[x][y].energy;



     double veto=0;

    //veto after ecal
    for(uint vi=0; vi < 3; ++vi)
      {
        veto_e->push_back(e.vetoEnergy(vi));
      }

     for(int x=0;x<6;x++)
      {
        veto+=e.VETO[x].energy;
        //veto_e->push_back(e.VETO[x].energy);
        veto_t->push_back(e.VETO[x].t0);
        veto_p0->push_back(e.VETO[x].pedestal0);
        veto_p1->push_back(e.VETO[x].pedestal1);
        veto_of->push_back(e.VETO[x].hasOverflow() ? 1 : 0);
      }
    bgo /= MeV; // convert to MeV
    
   
    
    const double srd = (e.SRD[0].energy + e.SRD[1].energy + e.SRD[2].energy) / MeV;
    for(int x=0;x<3;x++)
      {
        srd_e->push_back(e.SRD[x].energy);
        srd_t->push_back(e.SRD[x].t0);
        srd_p0->push_back(e.SRD[x].pedestal0);
        srd_p1->push_back(e.SRD[x].pedestal1);
        srd_of->push_back(e.SRD[x].hasOverflow() ? 1 : 0);
      }

    const size_t hod_nhits = e.HOD0.xhits.size() + e.HOD0.yhits.size() + e.HOD1.xhits.size() + e.HOD1.yhits.size();
    ehplot.Fill(ecal,hcal);
    srdplot.Fill(srd);
    e1plot.Fill(ecal1);
    e0plot.Fill(ecal0);
    eplot.Fill(ecal);
    hplot.Fill(hcal);
    vetoplot.Fill(veto);
    if(e.ECAL[1][3][3].hasDigit)ecal133_t.Fill(e.ECAL[1][3][3].t0ns()-e.masterTime);
    muonplot.Fill(muon);
    atargplot.Fill(atarg);
  
    s1_e=e.S1.energy;
    s1_t=e.S1.t0;
    s1_p0=e.S1.pedestal0;
    s1_p1=e.S1.pedestal1;
    s1_of=e.S1.hasOverflow() ? 1 : 0;
    s2_t=e.S2.t0;
    masterTime = e.masterTime;

    chi = calcShowerChi2(e);
  
    // Straw
    for (const auto& i : e.ST04.Hits) {
      ST04X->Fill(i.x.wire());
      ST04Y->Fill(i.y.wire());
      ST04X_t->Fill(i.x.time());
      ST04Y_t->Fill(i.y.time());
      
      ST4X->push_back(i.x.wire());
      ST4Y->push_back(i.y.wire());
      ST4X_t->push_back(i.x.time());
      ST4Y_t->push_back(i.y.time());
    }

    for (const auto& i : e.ST05.Hits) {
      ST05X->Fill(i.x.wire());
      ST05Y->Fill(i.y.wire());
      ST05X_t->Fill(i.x.time());
      ST05Y_t->Fill(i.y.time());
      
      ST5X->push_back(i.x.wire());
      ST5Y->push_back(i.y.wire());
      ST5X_t->push_back(i.x.time());
      ST5Y_t->push_back(i.y.time());
    }

    for (const auto& i : e.ST06.Hits) {
      ST06X->Fill(i.x.wire());
      ST06Y->Fill(i.y.wire());
      ST06X_t->Fill(i.x.time());
      ST06Y_t->Fill(i.y.time());
      
      ST6X->push_back(i.x.wire());
      ST6Y->push_back(i.y.wire());
      ST6X_t->push_back(i.x.time());
      ST6Y_t->push_back(i.y.time());
    }

    for (const auto& i : e.ST04.XClusters) ST4X_c->push_back(i.size());
    for (const auto& i : e.ST05.XClusters) ST5X_c->push_back(i.size());
    for (const auto& i : e.ST06.XClusters) ST6X_c->push_back(i.size());
    for (const auto& i : e.ST04.YClusters) ST4Y_c->push_back(i.size());
    for (const auto& i : e.ST05.YClusters) ST5Y_c->push_back(i.size());
    for (const auto& i : e.ST06.YClusters) ST6Y_c->push_back(i.size());
    

    
double  mmx1_hit=0, mmy1_hit=0, mmx2_hit=0, mmy2_hit=0, mmx3_hit=0, mmy3_hit=0, mmx4_hit=0, mmy4_hit=0;

    if (e.MM1.hasHit()) {
      mm1X->Fill(e.MM1.xpos);
      mm1Y->Fill(e.MM1.ypos);
      mm1X_time->Fill(e.MM1.xplane.bestHitTime());
      mm1Y_time->Fill(e.MM1.yplane.bestHitTime());
      mm1->Fill(e.MM1.xpos, e.MM1.ypos);

      mm1X_time0->Fill(e.MM1.xplane.bestHitTime()-e.masterTime);
      mm1Y_time0->Fill(e.MM1.yplane.bestHitTime()-e.masterTime);
     
      mm1X_mult->Fill(e.MM1.xplane.clusters.size());
      mm1Y_mult->Fill(e.MM1.yplane.clusters.size());
      

      MM1x_bestpos = e.MM1.xpos;
      MM1y_bestpos = e.MM1.ypos;
      
      m1x_ci = e.MM1.xplane.bestClusterIndex();
      for(size_t x=0;x<e.MM1.xplane.clusters.size();x++)
        {
          M1x_pos->push_back(e.MM1.xplane.clusters[x].position());
          M1x_charge->push_back(e.MM1.xplane.clusters[x].charge_total);
          M1x_mult->push_back(e.MM1.xplane.clusters[x].size());
          M1x_time->push_back(e.MM1.xplane.clusters[x].cluster_time());
        }
      
      m1y_ci = e.MM1.yplane.bestClusterIndex();
      for(size_t x=0;x<e.MM1.yplane.clusters.size();x++)
        {
          M1y_pos->push_back(e.MM1.yplane.clusters[x].position());
          M1y_charge->push_back(e.MM1.yplane.clusters[x].charge_total);
          M1y_mult->push_back(e.MM1.yplane.clusters[x].size());
          M1y_time->push_back(e.MM1.yplane.clusters[x].cluster_time());
        }
      
    }
    
    if (e.MM2.hasHit()) {
      mm2X->Fill(e.MM2.xpos);
      mm2Y->Fill(e.MM2.ypos);
      mm2X_time->Fill(e.MM2.xplane.bestHitTime());
      mm2Y_time->Fill(e.MM2.yplane.bestHitTime());
      mm2->Fill(e.MM2.xpos, e.MM2.ypos);

      mm2X_time0->Fill(e.MM2.xplane.bestHitTime()-e.masterTime);
      mm2Y_time0->Fill(e.MM2.yplane.bestHitTime()-e.masterTime);
      
      mm2X_mult->Fill(e.MM2.xplane.clusters.size());
      mm2Y_mult->Fill(e.MM2.yplane.clusters.size());
      

      MM2x_bestpos = e.MM2.xpos;
      MM2y_bestpos = e.MM2.ypos;
     
      m2x_ci = e.MM2.xplane.bestClusterIndex();
      for(size_t x=0;x<e.MM2.xplane.clusters.size();x++)
        {
          M2x_pos->push_back(e.MM2.xplane.clusters[x].position());
          M2x_charge->push_back(e.MM2.xplane.clusters[x].charge_total);
          M2x_mult->push_back(e.MM2.xplane.clusters[x].size());
          M2x_time->push_back(e.MM2.xplane.clusters[x].cluster_time());
        }
      
      m2y_ci = e.MM2.yplane.bestClusterIndex();
      for(size_t x=0;x<e.MM2.yplane.clusters.size();x++)
        {
          M2y_pos->push_back(e.MM2.yplane.clusters[x].position());
          M2y_charge->push_back(e.MM2.yplane.clusters[x].charge_total);
          M2y_mult->push_back(e.MM2.yplane.clusters[x].size());
          M2y_time->push_back(e.MM2.yplane.clusters[x].cluster_time());
        }
     
    }
    
    if (e.MM3.hasHit()) {
      mm3X->Fill(e.MM3.xpos);
      mm3Y->Fill(e.MM3.ypos);
      mm3X_time->Fill(e.MM3.xplane.bestHitTime());
      mm3Y_time->Fill(e.MM3.yplane.bestHitTime());
      mm3->Fill(e.MM3.xpos, e.MM3.ypos);

      mm3X_time0->Fill(e.MM2.xplane.bestHitTime()-e.masterTime);
      mm3Y_time0->Fill(e.MM2.yplane.bestHitTime()-e.masterTime);
     
      mm3X_mult->Fill(e.MM3.xplane.clusters.size());
      mm3Y_mult->Fill(e.MM3.yplane.clusters.size());
       
      
      MM3x_bestpos = e.MM3.xpos;
      MM3y_bestpos = e.MM3.ypos;
      
      m3x_ci = e.MM3.xplane.bestClusterIndex();
      for(size_t x=0;x<e.MM3.xplane.clusters.size();x++)
        {
          M3x_pos->push_back(e.MM3.xplane.clusters[x].position());
          M3x_charge->push_back(e.MM3.xplane.clusters[x].charge_total);
          M3x_mult->push_back(e.MM3.xplane.clusters[x].size());
          M3x_time->push_back(e.MM3.xplane.clusters[x].cluster_time());
        }
      
      m3y_ci = e.MM3.yplane.bestClusterIndex();
      for(size_t x=0;x<e.MM3.yplane.clusters.size();x++)
        {
          M3y_pos->push_back(e.MM3.yplane.clusters[x].position());
          M3y_charge->push_back(e.MM3.yplane.clusters[x].charge_total);
          M3y_mult->push_back(e.MM3.yplane.clusters[x].size());
          M3y_time->push_back(e.MM3.yplane.clusters[x].cluster_time());
        }
      
    }
    
    if (e.MM4.hasHit()) {
      mm4X->Fill(e.MM4.xpos);
      mm4Y->Fill(e.MM4.ypos);
      mm4X_time->Fill(e.MM4.xplane.bestHitTime());
      mm4Y_time->Fill(e.MM4.yplane.bestHitTime());
      mm4->Fill(e.MM4.xpos, e.MM4.ypos);

       mm4X_time0->Fill(e.MM4.xplane.bestHitTime()-e.masterTime);
      mm4Y_time0->Fill(e.MM4.yplane.bestHitTime()-e.masterTime);
      
      mm4X_mult->Fill(e.MM4.xplane.clusters.size());
      mm4Y_mult->Fill(e.MM4.yplane.clusters.size());
       
      MM4x_bestpos = e.MM4.xpos;
      MM4y_bestpos = e.MM4.ypos;
     
      m4x_ci = e.MM4.xplane.bestClusterIndex();
      for(size_t x=0;x<e.MM4.xplane.clusters.size();x++)
        {
          M4x_pos->push_back(e.MM4.xplane.clusters[x].position());
          M4x_charge->push_back(e.MM4.xplane.clusters[x].charge_total);
          M4x_mult->push_back(e.MM4.xplane.clusters[x].size());
          M4x_time->push_back(e.MM4.xplane.clusters[x].cluster_time());
        }
      
      m4y_ci = e.MM4.yplane.bestClusterIndex();
      for(size_t x=0;x<e.MM4.yplane.clusters.size();x++)
        {
          M4y_pos->push_back(e.MM4.yplane.clusters[x].position());
          M4y_charge->push_back(e.MM4.yplane.clusters[x].charge_total);
          M4y_mult->push_back(e.MM4.yplane.clusters[x].size());
          M4y_time->push_back(e.MM4.yplane.clusters[x].cluster_time());
        }
     
    }
        
    
    const size_t mm_nclusters =
      e.MM1.xplane.clusters.size() + e.MM1.yplane.clusters.size() +
      e.MM2.xplane.clusters.size() + e.MM2.yplane.clusters.size() +
      e.MM3.xplane.clusters.size() + e.MM3.yplane.clusters.size() +
      e.MM4.xplane.clusters.size() + e.MM4.yplane.clusters.size();

    // simple track reconstruction
        if (e.MM1.hasHit() && e.MM2.hasHit() && e.MM3.hasHit() && e.MM4.hasHit()) {
       const TVector3 mm1_hit = e.MM1.abspos();
       const TVector3 mm2_hit = e.MM2.abspos();
       const TVector3 mm3_hit = e.MM3.abspos();
       const TVector3 mm4_hit = e.MM4.abspos();
       const double Bfield = MAG_field;
       
       const double mom = simple_tracking(mm1_hit, mm2_hit, mm3_hit, mm4_hit, mag_l, Bfield);
       //cout << "mom = " << mom << endl;
       //momentum->Fill(mom);
       
       // extrapolate MM3->4 line on ECAL surface
       // TODO: the definition is valid only for 2016B
       const TVector3 P_ecal = extrapolate_line(mm3_hit, mm4_hit, ECAL_pos.pos.Z());
       //cout << "ecal track x = " << P_ecal.X() << " y = " << P_ecal.Y() << endl;
       ECAL_pos_mom->Fill(P_ecal.X(), P_ecal.Y());
    
    

        part_mom=mom;
        ecal_hitx=P_ecal.X();
        ecal_hity=P_ecal.Y();
   
    genfit::TrackCand myCand;

   
      posM1.SetX(scalecorr*(mm1_hit.X()));    posM1.SetY(scalecorr*(mm1_hit.Y()));  posM1.SetZ(scalecorr*z4);
      posM2.SetX(scalecorr*(mm2_hit.X()));    posM2.SetY(scalecorr*(mm2_hit.Y()));  posM2.SetZ(scalecorr*z3);
      posM3.SetX(scalecorr*(mm3_hit.X()));    posM3.SetY(scalecorr*(mm3_hit.Y()));  posM3.SetZ(scalecorr*z2);
      posM4.SetX(scalecorr*(mm4_hit.X()));    posM4.SetY(scalecorr*(mm4_hit.Y()));  posM4.SetZ(scalecorr*z1);
  
      
      // trackrep
      rep = new genfit::RKTrackRep(pdg);
     
      
      momM.SetX(0.0);
      momM.SetY(0.0);
      momM.SetZ(1.0);
      momM.SetMag(10.0);//GeV/c
      
     
      
      double detectorResolution(0.02); // resolution of planar detectors

      // approximate covariance for spacepoint
      TMatrixDSym covM(6);
      int nMeasurements = 4;
      for (int i = 0; i < 3; ++i)
              covM(i,i) = detectorResolution*detectorResolution;
      for (int i = 3; i < 6; ++i)
              covM(i,i) = pow(detectorResolution / nMeasurements / sqrt(3), 2);
      
      
      
      // covariance for spacepoint
      TMatrixDSym cov(3);
      for (int i = 0; i < 3; ++i)
        cov(i,i) = detectorResolution*detectorResolution;


      
      TVectorD hitCoords(2);
      TVector3 norm = momM;
      norm.SetMag(1.0);
      
     
      // The patternRecognition would create the TrackCand.
      myDetectorHitArray.Clear("C");
      new(myDetectorHitArray[0]) genfit::mySpacepointDetectorHit(posM1, cov);
      myCand.addHit(myDetId, 0);
      new(myDetectorHitArray[1]) genfit::mySpacepointDetectorHit(posM2, cov);
      myCand.addHit(myDetId, 1);
      new(myDetectorHitArray[2]) genfit::mySpacepointDetectorHit(posM3, cov);
      myCand.addHit(myDetId, 2);
      new(myDetectorHitArray[3]) genfit::mySpacepointDetectorHit(posM4, cov);
      myCand.addHit(myDetId, 3);
      // set start values and pdg to cand
      myCand.setPosMomSeedAndPdgCode(posM1, momM, pdg);
      myCand.setCovSeed(covM);


      genfit::Track * fitTrack = NULL;
      try{      
      // create track
        fitTrack = new genfit::Track(myCand, factory, rep);
        fitter->processTrack(fitTrack);
        fitTrack->checkConsistency();
        
        
        genfit::StateOnPlane mystate = fitTrack->getFittedState();
        TVector3 themom = fitTrack->getCardinalRep()->getMom(mystate);
        
        genfit_mom=themom.Mag();

        momentum->Fill(genfit_mom);
        ECAL_mom->Fill(ecal,genfit_mom);

        //        if(displayon)display->addEvent(fitTrack);
        delete fitTrack;
      }
      catch(genfit::Exception& e){
              std::cerr << e.what();
              std::cerr << "Exception, next track" << std::endl;
        continue;
      }
      
        }
    
    tree->Fill(); 
      }
  
    if (nevt > 10000) break;
  }
  
  file_tree->Write();
  file_tree->Close();
  
  file_histo->Write();
  file_histo->Close();
  
  return 0;
}
