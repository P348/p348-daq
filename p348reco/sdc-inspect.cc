#include "sdc.h"
#include <cstdint>

int
main(int argc, char * argv[]) {
    std::string docsPath;
    size_t runNo = SIZE_MAX;
    if(argc == 3) {
        docsPath = argv[1];
        runNo = atoi(argv[2]);
    } else {
        std::cerr << "Error: bad arguments number." << std::endl;
        return 1;
    }
    if(runNo == SIZE_MAX) {
        std::cerr << "Error: bad run number: " << runNo << std::endl;
        return 1;
    }
    if(docsPath.empty()) {
        std::cerr << "Error: empty source documents path." << std::endl;
        return 1;
    }

    sdc::Documents<p348::RunNo_t> docs = p348::instantiate_default_docs(docsPath);

    return sdc::json_loading_log<p348::CaloCellCalib, p348::RunNo_t>(
            p348::RunNo_t{runNo}, docs, std::cout);
}

