// ROOT
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

// c++
#include <iostream>
#include <fstream>

// P348 reco
#include "p348reco.h"
#include "simu.h"
#include "shower.h"
#ifdef TRACKINGTOOLS
#include "tracking_new.h"
#endif

int main(int argc, char *argv[])
{
  if (argc != 3) {
    std::cerr << "Usage: ./exampletrackingmc.exe <MC file name> Nevents" << std::endl;
    return 1;
  }

  std::ifstream inFile(argv[1]);

  if (! inFile) {
    std::cerr << "ERROR: can't open file " << argv[1] << std::endl;
    return 1;
  }

  const int Nevents = atoi(argv[2]);

  const unsigned int NMM = 4;
  const unsigned int NST = 4;
  const unsigned int NGEM = 2;
  const bool debug = false;

  TFile* hOutputFile = new TFile("hist.root", "RECREATE");

  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH1I bgoplot("bgoplot", "BGO;Energy, MeV;#nevents", 150, 0, 150);

  TH1I mcecalentries("MC_ecalentries", "MC ECAL entry X coordinates in mm;X, mm;#nevents", 200, -400., 400.);

  TH1D Chi ("Chi","chi square distribution of shower profile",1000,0,100);

  //MM histograms
  TH2I* mmtrue[NMM];
  TH2I* mmreco[NMM];
  TH2I* mmres[NMM];
  TH1D *mmdist[NMM];
  TH2D *mmdisten[NMM];
  TH2D *mmdistdep[NMM];
  TH2I* mmclus[NMM];
  TH2I* mmcharge[NMM];
  TH1I* mmfake[NMM];
  hOutputFile->mkdir("MM");
  for(unsigned int i(0); i < NMM; i++) {
    const unsigned int imm = i+1;
    hOutputFile->mkdir(Form("MM/MM%u",imm));
    hOutputFile->cd(Form("MM/MM%u",imm));
    mmtrue[i]   = new TH2I(Form("mm%utrue",imm),Form("true hit position recorded for best cluster in MM%u; X [mm]; Y [mm]",imm),400,-100,100,400,-100,100);
    mmreco[i]   = new TH2I(Form("mm%ureco",imm),Form("reco hit position recorded for best cluster in MM%u; X [mm]; Y [mm]",imm),400,-100,100,400,-100,100);
    mmres[i]    = new TH2I(Form("mm%ures",imm),Form("difference between hit position and reconstructed hit in MM%u; X [mm]; Y [mm]",imm),200,-10,10,200,-10,10);
    mmdist[i]   = new TH1D(Form("mm%udist",imm),Form("minimal dist. between hit position and reconstructed hit in MM%u; Distance [mm]; Nevents [-]",imm),200,0,100);
    mmdisten[i] = new TH2D(Form("mm%udisten",imm),Form("dist. between true and reco vs energy in MM%u; Distance [mm]; Energy [GeV]",imm),200,0,100,1000, 0, 100);
    mmdistdep[i] = new TH2D(Form("mm%udistdep",imm),Form("dist. between true and reco vs deposition in MM%u; Distance [mm]; Energy [MeV]",imm),200,0,100,1000, 0, 100000);
    mmclus[i]   = new TH2I(Form("mm%uclus",imm),Form("Number of clusters per plane in MM%u;Xcluster;Ycluster",imm),10,-0.5,9.5,10,-0.5,9.5);
    mmcharge[i] = new TH2I(Form("mm%ucharge",imm),Form("ADC unit per plane in MM%u;Xplane;Yplane",imm),1000,-0.5,9999.5,1000,-0.5,9999.5);
    mmfake[i]   = new TH1I(Form("mm%ufake",imm),Form("Number of fake hits created by simulation of multiplexing in MM%u; (Mult_{true} - Mult_{reco}) [-]; Nevents [-]",imm),15,-4.5,10.5);
  }
  hOutputFile->cd("MM");
  TH1I mom_simple("mom_simple", "Reconstructed momentum (simple); Momentum, GeV/c;#nevents", 300, 0, 150);
  TH1I mom_genfit("mom_genfit", "Reconstructed momentum (genfit); Momentum, GeV/c;#nevents", 300, 0, 150);
  TH2I mom_corr("mom_corr", "Reconstructed momentum ; Genfit [GeV/c]; Normal [GeV/c];#entries",300,0,150, 300, 0, 150);
  TH2I mom_simple_ecal("mom_simple_ecal", "Reconstructed momentum (simple) vs ECAL energy; ECAL energy [GeV]; p_{Reco, SimpleTrack} [GeV/c];#entries",300,0,150,300,0,150);
  TH2I mom_genfit_ecal("mom_genfit_ecal", "Reconstructed momentum (genfit) vs ECAL energy; ECAL energy [GeV]; p_{Reco, GenFit} [GeV/c];#entries",300,0,150,300,0,150);

  //ST histograms
  TH2I* sttrue[NST];
  TH2I* streco[NST];
  TH2I* stres[NST];
  TH1D *stdist[NST];
  TH2D *stdisten[NST];
  TH2D *stdistdep[NST];
  TH2I* stclus[NST];
  hOutputFile->mkdir("ST");
  for(unsigned int i(0); i < NST; i++) {
    const unsigned int ist = i+1;
    hOutputFile->mkdir(Form("ST/ST%u",ist));
    hOutputFile->cd(Form("ST/ST%u",ist));
    sttrue[i]   = new TH2I(Form("st%utrue",ist),Form("true hit position recorded for best cluster in ST%u; X [mm]; Y [mm]",ist),400,-100,100,400,-100,100);
    streco[i]   = new TH2I(Form("st%ureco",ist),Form("reco hit position recorded for best cluster in ST%u; X [mm]; Y [mm]",ist),400,-100,100,400,-100,100);
    stres[i]    = new TH2I(Form("st%ures",ist),Form("difference between hit position and reconstructed hit in ST%u; X [mm]; Y [mm]",ist),200,-10,10,200,-10,10);
    stdist[i]   = new TH1D(Form("st%udist",ist),Form("minimal dist. between hit position and reconstructed hit in ST%u; Distance [mm]; Nevents [-]",ist),200,0,100);
    stdisten[i] = new TH2D(Form("st%udisten",ist),Form("dist. between true and reco vs energy in ST%u; Distance [mm]; Energy [GeV]",ist),200,0,100,1000, 0, 100);
    stdistdep[i] = new TH2D(Form("st%udistdep",ist),Form("dist. between true and reco vs deposition in ST%u; Distance [mm]; Energy [MeV]",ist),200,0,100,1000, 0, 100000);
    stclus[i]   = new TH2I(Form("st%uclus",ist),Form("Number of clusters per plane in ST%u;Xcluster;Ycluster",ist),10,-0.5,9.5,10,-0.5,9.5);
  }

  //GEM histograms
  TH2I* gemtrue[NGEM];
  TH2I* gemclus[NGEM];
  hOutputFile->mkdir("GEM");
  for(unsigned int i(0); i < NGEM; i++) {
    const unsigned int igem = i+1;
    hOutputFile->mkdir(Form("GEM/GEM%u",igem));
    hOutputFile->cd(Form("GEM/GEM%u",igem));
    gemtrue[i]   = new TH2I(Form("gem%utrue",igem),Form("true hit position recorded for best cluster in GEM%u; X [mm]; Y [mm]",igem),400,-100,100,400,-100,100);
    gemclus[i]   = new TH2I(Form("gem%uclus",igem),Form("Number of clusters per plane in GEM%u;Xcluster;Ycluster",igem),10,-0.5,9.5,10,-0.5,9.5);
  }

  hOutputFile->cd("");

  // event loop, read event by event
  for (int iev = 0; iev < Nevents; iev++) {

    RecoEvent e = RunP348RecoMC(inFile,1,0); //doDigi=1, doSmearing=0
    if (!e.mc) break;



    const vector<const Micromega*> mm_map
      = {&e.MM1, &e.MM2, &e.MM3, &e.MM4, &e.MM5, &e.MM6, &e.MM7, &e.MM8};
    const vector<const vector<MChit>*> mmtruth_map
      = {&e.mc->MM1truth, &e.mc->MM2truth, &e.mc->MM3truth, &e.mc->MM4truth,
         &e.mc->MM5truth, &e.mc->MM6truth, &e.mc->MM7truth, &e.mc->MM8truth};

    // TODO: Add digitized GEM hits
    //const vector<const gem::GEM*> gem_map
    //  = {e.GM01, e.GM02, e.GM03, e.GM04};
    const vector<const vector<MChit>*> gemtruth_map
      = {&e.mc->GEM1truth, &e.mc->GEM2truth, &e.mc->GEM3truth, &e.mc->GEM4truth};
    const GEMGeometry *gem_geo_map[4] = { &GM01Geometry, &GM02Geometry, &GM03Geometry, &GM04Geometry};

    const vector<const Straw*> st_map
      = {&e.ST01, &e.ST02, &e.ST03, &e.ST04};
    const vector<const vector<MChit>*> sttruth_map
      = {&e.mc->ST1truth, &e.mc->ST2truth, &e.mc->ST3truth, &e.mc->ST4truth};

    // print progress
    if (iev % 100 == 1)
      std::cout << "===> Event #" << iev << std::endl;

    double ecal = 0;
    for (int d = 0; d < 2; ++d)
      for(int x=0; x < ECAL_pos.Nx; x++)
        for(int y=0; y < ECAL_pos.Ny; y++)
          ecal += e.ECAL[d][x][y].energy;

    double hcal = 0;
    const int nmod = mcrunoption<int>("HCAL:NModules", 4);
    for(int d=0; d < nmod; d++)
      for(int x=0; x < HCAL_pos.Nx; x++)
        for(int y=0; y < HCAL_pos.Ny; y++)
          hcal += e.HCAL[d][x][y].energy;

    double bgo = 0;
    for (int x = 0; x < 8; ++x)
      bgo += e.BGO[x].energy;

    bgo /= MeV; // convert to MeV

    double srd = 0;
    for (int x = 0; x < 3; ++x)
      srd += e.SRD[x].energy;

    srd /= MeV; // convert to MeV

    const double chi = calcShowerChi2(e, SP_MC);

    double veto = 0.;
    for (int iv = 0; iv < 3; ++iv)
      veto += e.vetoEnergy(iv);

    //output true hits
    if(debug) {
      for(unsigned int i(0);i<NMM;++i)
        for(unsigned int j(0);j<mmtruth_map[i]->size();++j)
          cout << mmtruth_map[i]->at(j);
      for(unsigned int i(0);i<NST;++i)
        for(unsigned int j(0);j<sttruth_map[i]->size();++j)
          cout << sttruth_map[i]->at(j);
      /*
      for(unsigned int i(0);i<NGEM;++i)
        for(unsigned int j(0);j<gemtruth_map[i]->size();++j)
          cout << gemtruth_map[i]->at(j);
      */
    }

    //fill MM
    for(unsigned int i(0);i<NMM;++i)
      // If there are any true hits on MM
      if(mmtruth_map[i]->size() > 0)
      {
        for(unsigned int j(0);j<mmtruth_map[i]->size();++j) {
          const TVector3 v = mmtruth_map[i]->at(j).pos - mm_map[i]->geom->pos;
          mmtrue[i]->Fill(v.X(), v.Y());
          
          // Check if the smallest distance to one reconstructed points is within resolution of MM
          double dist = nan("");
          for (const auto& hit : mm_map[i]->hits) {
            // Calculate distance between true hits and reco hits
            const double _dist = (mmtruth_map[i]->at(j).pos - hit.abspos()).Perp();
            // Obtain minimum
            if(std::isnan(dist))
              dist = _dist;
            else if(_dist < dist)
              dist = _dist;
          }
          mmdist[i]->Fill(dist);
          mmdistdep[i]->Fill(dist, mmtruth_map[i]->at(j).deposit);
          mmdisten[i]->Fill(dist, mmtruth_map[i]->at(j).energy);
        }
        for (const auto& j : mm_map[i]->hits) {
          mmreco[i]->Fill(j.pos().X(), j.pos().Y());
          mmcharge[i]->Fill(j.xcluster->charge_total, j.ycluster->charge_total);
        }
        const int fakehits = int(mmtruth_map[i]->size()) - int(mm_map[i]->hits.size());
        mmfake[i]->Fill(fakehits);
        const TVector3 vres = mm_map[i]->abspos() - mmtruth_map[i]->at(0).pos;
        mmres[i]->Fill(vres.X(),vres.Y());
        mmclus[i]->Fill(mm_map[i]->xplane.clusters.size(),mm_map[i]->yplane.clusters.size());
      }

    //fill GEM
    for(unsigned int i(0);i<NGEM;++i) {
      // If there are any true hits on GEM
      for(const auto &gemhit : *gemtruth_map[i]) {
        const TVector3 v = gemhit.pos - gem_geo_map[i]->pos;
        gemtrue[i]->Fill(v.X(), v.Y());
      }
      gemclus[i]->Fill(gemtruth_map[i]->size(),gemtruth_map[i]->size());
    }

    //fill ST
    for(unsigned int i(0);i<NST;++i)
      // If there are any true hits on ST
      if(sttruth_map[i]->size() > 0)
      {
        for(unsigned int j(0);j<sttruth_map[i]->size();++j) {
          const TVector3 v = sttruth_map[i]->at(j).pos - st_map[i]->geom->pos;
          sttrue[i]->Fill(v.X(), v.Y());

          if(!st_map[i]->hasHit()) continue;
          // Check if the smallest distance to one reconstructed points is within resolution of ST
          double dist = nan("");
          for (const auto& hit : st_map[i]->Hits) {
            // Calculate distance between true hits and reco hits
            const double _dist = (sttruth_map[i]->at(j).pos - hit.abspos()).Perp();
            // Obtain minimum
            if(std::isnan(dist))
              dist = _dist;
            else if(_dist < dist)
              dist = _dist;
          }
          stdist[i]->Fill(dist);
          stdistdep[i]->Fill(dist, sttruth_map[i]->at(j).deposit);
          stdisten[i]->Fill(dist, sttruth_map[i]->at(j).energy);
        }
        for (const auto& j : st_map[i]->Hits) {
          streco[i]->Fill(j.pos().X(), j.pos().Y());
        }

        if(st_map[i]->hasHit()) {
          const TVector3 vres = st_map[i]->Hits.at(0).abspos() - sttruth_map[i]->at(0).pos;
          stres[i]->Fill(vres.X(),vres.Y());
          stclus[i]->Fill(st_map[i]->XClusters.size(),st_map[i]->YClusters.size());
        }
      }

    //MOMENTUM
    const SimpleTrack track = simple_tracking_4MM_2016(e); // same tracking as in 2016
    double mom(9999),momgen(9999),momchisq(9999);
    if(track) {
      //inangle =  track.in.theta()/mrad;
      //outangle = track.out.theta()/mrad;
      mom =  track.momentum;
      mom_simple.Fill(mom);
      mom_simple_ecal.Fill(ecal, mom);
    }
#ifdef TRACKINGTOOLS
    // track reconstruction with Tracking Tools
    // RECO HITS
    std::vector<std::pair<TVector3, double> > hits;
    FillAbsMMHits(e.MM1, hits);
    FillAbsMMHits(e.MM2, hits);
    FillAbsMMHits(e.MM3, hits);
    FillAbsMMHits(e.MM4, hits);
    FillAbsGEMHitsMC(e.mc->GEM1truth, hits);
    FillAbsGEMHitsMC(e.mc->GEM2truth, hits);
    FillAbsGEMHitsMC(e.mc->GEM3truth, hits);
    FillAbsGEMHitsMC(e.mc->GEM4truth, hits);
    FillAbsSTHits(e.ST01, hits);
    FillAbsSTHits(e.ST02, hits);
    FillAbsSTHits(e.ST03, hits);
    FillAbsSTHits(e.ST04, hits);

    if(debug)
      for(auto hit : hits)
        cout << "Reco point in MM: (" << hit.first.x() << ", " << hit.first.y() << ", " << hit.first.z() << ")" << endl;
    const std::vector<Track_t> tracks = GetReconstructedTracksFromHits(hits);

    // fill
    if(!tracks.empty()) {
      double _best_chi2(9999);
      double _best_mom(9999);
      // Look for the best track with the lowest chi2 value
      for (const auto &elem : tracks) {
        if (elem.chisquare < _best_chi2 && elem.chisquare > -1.) {
          _best_chi2 = elem.chisquare;
          _best_mom = elem.momentum;
        }
        if (debug) {
          std::cout << elem << std::endl;
        }
      }
      momchisq = _best_chi2;
      momgen = _best_mom;
      mom_genfit.Fill(momgen);
      mom_genfit_ecal.Fill(ecal, momgen);
      mom_corr.Fill(momgen,mom);
    }
#endif

    std::cout << "Event #" << iev
      << " ECAL = " << ecal << " GeV"
      << " HCAL = " << hcal << " GeV"
      << " BGO = " << bgo << " GeV"
      << " Veto = " << veto << " GeV"
      << " MOMENTUM = " << mom << " GeV"
      << " GENFIT MOMENTUM = " << momgen << " GeV"
      << std::endl;

    ehplot.Fill(ecal, hcal);
    bgoplot.Fill(bgo);
    if(e.mc) mcecalentries.Fill(e.mc->ECALEntryX);
    Chi.Fill(chi);
  }

  //take projection of some histograms
  for(unsigned int imm(0); imm < NMM; imm++) {
    hOutputFile->cd(Form("MM/MM%u",imm+1));
    mmres[imm]->ProjectionX();
    mmres[imm]->ProjectionY();
    mmclus[imm]->ProjectionX();
    mmclus[imm]->ProjectionY();
    mmcharge[imm]->ProjectionX();
    mmcharge[imm]->ProjectionY();
  }
  hOutputFile->cd("");
  hOutputFile->Write();
  delete hOutputFile;

  return 0;
}
