// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "TProfile2D.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "timecut.h"
#include "tracking.h"

// cache of recontruction information
struct RecoState {
  double ecal;
  double hcal;
  double muon;
  double srd;
  double s1t0;
  double master;
  double ecalT;
  double hcalT;
  SimpleTrack track;
  
  double ecal1T4;  // ECAL1 sum top 4 cells from central 3x3
  double ecal1T9;  // ECAL1 sum central 3x3
  double ecal1T25; // ECAL1 sum central 5x5
  
  double ecal1T_a4_9() const { return (ecal1T9 - ecal1T4) / ecal1T9; }
  double ecal1T_a9_25() const { return (ecal1T25 - ecal1T9) / ecal1T25; }
  
  const RecoEvent* e_;
  
  RecoState(const RecoEvent& e, const RecoEvent& eT);
};

RecoState::RecoState(const RecoEvent& e, const RecoEvent& eT)
{
  // process event reco data
  ecal = e.ecalTotalEnergy();
  hcal = e.hcalTotalEnergy();
  
  muon = 0;
  for (int d = 0; d < 4; ++d)
    muon += e.MUON[d].energy;
  
  srd = e.SRD[0].energy + e.SRD[1].energy + e.SRD[2].energy;
  
  s1t0 = e.S1.t0ns();
  master = e.masterTime;
  
  ecalT = eT.ecalTotalEnergy();
  hcalT = eT.hcalTotalEnergy();
  
  track = simple_tracking_4MM(e);
  
  // calc E25
  ecal1T25 = 0;
  for (int x = 1; x <= 5; ++x)
    for (int y = 1; y <= 5; ++y)
      ecal1T25 += eT.ECAL[1][x][y].energy;
  
  // calc E9
  ecal1T9 = 0;
  for (int x = 2; x <= 4; ++x)
    for (int y = 2; y <= 4; ++y)
      ecal1T9 += eT.ECAL[1][x][y].energy;
  
  // calc E4
  vector<double> E9vals;
  
  for (int x = 2; x <= 4; ++x)
    for (int y = 2; y <= 4; ++y)
      E9vals.push_back(eT.ECAL[1][x][y].energy);
  
  // sum top 4 elements: [5, 6, 7, 8]
  sort(E9vals.begin(), E9vals.end());
  ecal1T4 = E9vals[5] + E9vals[6] + E9vals[7] + E9vals[8];
  
  e_ = &e;
}

struct PlotsGroup {
  PlotsGroup(const TString s);
  void Fill(const RecoState& rs);
  void End();
  
  TString name;
  int nentries;
  
  TH2I* ehplot;
  TH2I* ehTplot;
  TH2I* eT_e9_vs_e25;
  TH2I* eT_e4_vs_e9;
  TH1I* eT_e9_25;
  TH1I* eT_e4_9;
  
  TH1I* srdplot;
  TH1I* muonplot;
  
  TH1I* trk_mom;
  TH2I* trk_mom_vs_sumehT;
  
  TH1I* trk_in_theta;
  TH2I* trk_in_theta2;
  TH2I* trk_mom_vs_in_theta;
  TH2I* trk_mom_vs_in_thetax;
  TH2I* trk_mom_vs_in_thetay;
  TH1I* trk_out_theta;
  TH2I* trk_out_theta2;
  TH1I* trk_in_out_angle;
  TH1I* trk_in_out_distance;
  
  TProfile2D* timeprofile;
  TH2I* timing;
  TH2I* timingMt;
  TH2I* timingMtMean;
  TH2I* timingMtMean_std;
  TH1I* s1t;
  TH1I* master;
  TH2I* s1_rise_vs_master;
  TH1I* s1_rise_rel_master;
  
  TH2I* energy;
};

PlotsGroup::PlotsGroup(const TString s)
: name(s), nentries(0)
{
  // create dedicated folder for the group
  const char* pwd = gDirectory->GetPath();
  gDirectory->mkdir(name);
  gDirectory->cd(name);
  
  // book histograms
  ehplot = new TH2I(name + "-ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  ehplot->SetOption("COL");
  
  ehTplot = new TH2I(name + "-ehTplot", "ECAL vs HCAL (in-time);ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  ehTplot->SetOption("COL");
  
  eT_e9_vs_e25 = new TH2I(name + "-eT_e9_vs_e25", "ECAL1 (in-time) E9 vs E25;E25, GeV;E9, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  eT_e9_vs_e25->SetOption("COL");
  
  eT_e4_vs_e9 = new TH2I(name + "-eT_e4_vs_e9", "ECAL1 (in-time) E4 vs E9;E9, GeV;E4, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  eT_e4_vs_e9->SetOption("COL");
  
  eT_e4_9 = new TH1I(name + "-eT_e4_9", "ECAL1 (in-time) (E9-E4)/E9;ratio;#nevents", 200, 0, 1);
  eT_e9_25 = new TH1I(name + "-eT_e9_25", "ECAL1 (in-time) (E25-E9)/E25;ratio;#nevents", 200, 0, 1);
  
  srdplot = new TH1I(name + "-srdplot", "SRD;Energy, MeV;#nevents", 150, 0, 150);
  muonplot = new TH1I(name + "-muonplot", "MUON;Energy;#nevents", 100, 0, 100);
  
  trk_mom = new TH1I(name + "-trk_mom", "Reconstructed momentum;Momentum, GeV/c;#nevents", 400, 0, 200);
  trk_mom_vs_sumehT = new TH2I(name + "-trk_mom_vs_sumehT", "ECAL+HCAL (in-time) vs Momentum;ECAL+HCAL, GeV;Momentum, GeV/c;#nevents", 150, 0, 150, 150, 0, 150);
  trk_mom_vs_sumehT->SetOption("COL");
  
  trk_in_theta = new TH1I(name + "-trk_in_theta", "In-track theta;Angle, rad;#nevents", 200, 0, 0.1);
  trk_in_theta2 = new TH2I(name + "-trk_in_theta2", "In-track theta;XAngle, rad;YAngle, rad;#nevents", 200, -0.1, 0.1, 200, -0.1, 0.1);
  trk_in_theta2->SetOption("COL");
  trk_mom_vs_in_theta = new TH2I(name + "-trk_mom_vs_in_theta", "Track Momentum vs In-track theta;Angle, rad;Momentum, GeV/c;#nevents", 200, 0, 0.1, 400, 0, 200);
  trk_mom_vs_in_theta->SetOption("COL");
  
  trk_mom_vs_in_thetax = new TH2I(name + "-trk_mom_vs_in_thetax", "Track Momentum vs In-track thetaX;Angle, rad;Momentum, GeV/c;#nevents", 400, -0.1, 0.1, 400, 0, 200);
  trk_mom_vs_in_thetax->SetOption("COL");
  
  trk_mom_vs_in_thetay = new TH2I(name + "-trk_mom_vs_in_thetay", "Track Momentum vs In-track thetaY;Angle, rad;Momentum, GeV/c;#nevents", 400, -0.1, 0.1, 400, 0, 200);
  trk_mom_vs_in_thetay->SetOption("COL");
  
  trk_out_theta = new TH1I(name + "-trk_out_theta", "Out-track theta;Angle, rad;#nevents", 200, 0, 0.1);
  trk_out_theta2 = new TH2I(name + "-trk_out_theta2", "Out-track theta;XAngle, rad;YAngle, rad;#nevents", 200, -0.1, 0.1, 200, -0.1, 0.1);
  trk_out_theta2->SetOption("COL");
  trk_in_out_angle = new TH1I(name + "-trk_in_out_angle", "In-Out tracks angle;Angle, rad;#nevents", 200, 0, 0.1);
  trk_in_out_distance = new TH1I(name + "-trk_in_out_distance", "In-Out tracks skew distance;Distance, mm;#nevents", 200, 0, 100);
  
  timeprofile = new TProfile2D(name + "-timeprofile", "Time profile;Channel;Time sample;ADC", 300, 0, 300, 32, 0, 32);
  timeprofile->SetOption("COL");
  
  timing = new TH2I(name + "-timing", "Timing;Channel;Time, ns;events", 300, 0, 300, 401, 0, 401);
  //timing->SetCanExtend(TH1::kXaxis);
  //timing->ResetBit(TH1::kCanRebin);
  timing->SetOption("COL");
  
  timingMt = new TH2I(name + "-timingMt", "Timing (-master);Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingMt->SetOption("COL");
  
  timingMtMean = new TH2I(name + "-timingMtMean", "Timing (-master -mean);Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingMtMean->SetOption("COL");
  
  timingMtMean_std = new TH2I(name + "-timingMtMean_std", "Timing STD (-master -mean);Channel;STD;events", 300, 0, 300, 4*51, 0, 51);
  timingMtMean_std->SetOption("COL");
  
  
  s1t = new TH1I(name + "-S1time", "S1.t0;Time, ns;events", 400, 0, 400);
  master = new TH1I(name + "-master", "Master time;Time, ns;events", 400, 0, 400);
  s1_rise_vs_master = new TH2I(name + "-s1_rise_vs_master", "S1.rise vs S1.com;S1.rise, ns;S1.com, ns;events", 200, 0, 400, 200, 0, 400);
  s1_rise_vs_master->SetOption("COL");
  s1_rise_rel_master = new TH1I(name + "-s1_rise_rel_master", "S1.com - S1.rise;Time, ns;events", 400, -200, 200);
  
  energy = new TH2I(name + "-energy", "Energy;Channel;Energy, GeV;events", 300, 0, 300, 302, 0, 151);
  energy->SetOption("COL");
  
  gDirectory->cd(pwd);
}

void PlotsGroup::End()
{
  timeprofile->LabelsDeflate("X");
  timeprofile->LabelsOption("a v", "X");
  
  timing->LabelsDeflate("X");
  timing->LabelsOption("a v", "X");
  
  timingMt->LabelsDeflate("X");
  timingMt->LabelsOption("a v", "X");
  
  timingMtMean->LabelsDeflate("X");
  timingMtMean->LabelsOption("a v", "X");
  
  timingMtMean_std->LabelsDeflate("X");
  timingMtMean_std->LabelsOption("a v", "X");
  
  energy->LabelsDeflate("X");
  energy->LabelsOption("a v", "X");
  
  cout << "PlotGroup=" << name << " nentries=" << nentries << endl;
}

void PlotsGroup::Fill(const RecoState& rs)
{
  nentries++;
  
  ehplot->Fill(rs.ecal, rs.hcal);
  ehTplot->Fill(rs.ecalT, rs.hcalT);
  
  eT_e9_vs_e25->Fill(rs.ecal1T25, rs.ecal1T9);
  eT_e4_vs_e9->Fill(rs.ecal1T9, rs.ecal1T4);
  
  eT_e4_9->Fill(rs.ecal1T_a4_9());
  eT_e9_25->Fill(rs.ecal1T_a9_25());
  
  srdplot->Fill(rs.srd / MeV);
  muonplot->Fill(rs.muon);
  
  if (rs.track) {
    trk_mom->Fill(rs.track.momentum);
    trk_mom_vs_sumehT->Fill(rs.ecalT+rs.hcalT, rs.track.momentum);
    
    trk_in_theta->Fill(rs.track.in.theta());
    trk_in_theta2->Fill(rs.track.in.theta_x(), rs.track.in.theta_y());
    trk_mom_vs_in_theta->Fill(rs.track.in.theta(), rs.track.momentum);
    trk_mom_vs_in_thetax->Fill(rs.track.in.theta_x(), rs.track.momentum);
    trk_mom_vs_in_thetay->Fill(rs.track.in.theta_y(), rs.track.momentum);
    trk_out_theta->Fill(rs.track.out.theta());
    trk_out_theta2->Fill(rs.track.out.theta_x(), rs.track.out.theta_y());
    trk_in_out_angle->Fill(rs.track.in_out_angle());;
    trk_in_out_distance->Fill(rs.track.in_out_distance());
  }
  
  s1t->Fill(rs.s1t0);
  master->Fill(rs.master);
  s1_rise_vs_master->Fill(rs.s1t0, rs.master);
  s1_rise_rel_master->Fill(rs.master - rs.s1t0);
  
  const double master = rs.master;
  
  const vector<const Cell*> list = listCells(*rs.e_);
  
  for (size_t i = 0; i < list.size(); ++i) {
    const Cell& c = *list[i];
    
    if (!c.hasDigit) continue;
    
    double ct0 = c.t0ns();
    if (!isfinite(ct0)) ct0 = 0;
    
    const char* name = c.calib->name.c_str();
    const double texpected = c.calib->texpected(master);
    
    const double t0 = coerce(ct0, 0, 400);
    const double t0Mt = coerce(ct0 - master, -200, 200);
    const double t0Mtmean = coerce(ct0 - texpected, -200, 200);
    const double t0Mtmean_std = coerce(fabs(ct0 - texpected)/c.calib->tsigma, 0, 50);
    
    timing->Fill(name, t0, 1.);
    timingMt->Fill(name, t0Mt, 1.);
    timingMtMean->Fill(name, t0Mtmean, 1.);
    timingMtMean_std->Fill(name, t0Mtmean_std, 1.);
    
    energy->Fill(name, coerce(c.energy, 0, 150), 1.);
    
    for (size_t j = 0; j < c.raw.size(); ++j)
      timeprofile->Fill(name, j, c.wave(j), 1.);
  }
}

// check the value x is in range (min, max)
inline bool inrange(const double x, const double min, const double max)
{
  return (min < x) && (x < max);
}

/*
// re-do time and energy reconstruction in timing calibration window
void timeWindowReco(RecoEvent& e)
{
  vector<Cell*> list = listCells(e);
  
  const double master = e.masterTime;
  
  for (size_t i = 0; i < list.size(); ++i) {
    //cout << 0 << endl;
    Cell* c = list[i];
    
    if (!c->hasDigit) continue;
    
    const double t0expected = c->calib->texpected(master);
    const double tsigma = c->calib->tsigma;
    
    const int sample_min = (t0expected - 3*tsigma) / SADC_sampling_interval;
    const int sample_max = sample_min + 5;
    
    //cout << 1 << endl;
    c->amplitude = c->A_MaxSample(sample_min, sample_max);
    c->energy = c->calib->K * c->amplitude;
    
    //cout << 2 << " " << e.run << " " << e.spill << " " << e.spillevent << "" << c->calib->name << endl;
    //cout << sample_min << " " << sample_max << endl;
    c->t0 = c->t0_RisingEdge(sample_min, sample_max);
    //cout << 3 << endl;
  }
}
*/

int countCellsHcal0(const RecoEvent& e, const double threshould)
{
  int n = 0;
  
  for (int x = 0; x < 3; ++x)
    for (int y = 0; y < 3; ++y)
      if (e.HCAL[0][x][y].energy > threshould) n++;
  
  return n;
}

// simple check for shower shape
// 'cross' cells should have at least 1% of central cell energy
bool ecalCheckShower(const RecoEvent& e, const CellXY& pos)
{
  const int x = pos.ix;
  const int y = pos.iy;
  
  // skip showers with center in edge cell
  const bool isInnerCell = (0 < x) && (x < 5) && (0 < y) && (y < 5);
  if (!isInnerCell) return false;
  
  const double Ethr = 0.01 * e.ECAL[1][x][y].energy;
  
  return (e.ECAL[1][x-1][y].energy > Ethr) && (e.ECAL[1][x+1][y].energy > Ethr) &&
         (e.ECAL[1][x][y-1].energy > Ethr) && (e.ECAL[1][x][y+1].energy  > Ethr) &&
         (e.ECAL[0][x][y].energy > Ethr);
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./2016B-invis.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  TFile* file = new TFile("2016B-invis.root", "RECREATE");
  
  PlotsGroup group0("nocut");
  PlotsGroup group1("trigger");
  PlotsGroup group2("e133max");
  PlotsGroup group3("shower");
  PlotsGroup group4("srd");
  PlotsGroup group4all("srdall");
  PlotsGroup group5("veto");
  PlotsGroup group5phys("vetophys");
  PlotsGroup group5phys10("vetophys10");
  PlotsGroup group6("ecal");
  PlotsGroup group7("hcal");
  PlotsGroup group7hcal0veto("hcal0veto");
  PlotsGroup group8("trkmom");
  PlotsGroup group9("trkintheta");
  PlotsGroup groupAll("all");
  PlotsGroup groupAllHcal0veto("all+hcal0veto");
  PlotsGroup groupAllTrkmom("all+trkmom");
  PlotsGroup groupAllTrkintheta("all+trkintheta");
  
  // output file
  ofstream fout("output.dat");
  
  // event loop
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    //RecoEvent e = e0;
    //timeWindowReco(e);
    
    // process only "physics" events
    if (!e.isPhysics) continue;
    
    // zero out-of-time cells
    RecoEvent eT = e;
    timecut(eT, 4);
    
    const RecoState rs(e, eT);
    
    /*
    // output event reco summary
    cout << "Event #" << nevt
         << " ECAL = " << rs.ecal << " GeV"
         << " HCAL = " << rs.hcal << " GeV"
         << " SRD = " << rs.srd << " MeV"
         << " MUON = " << rs.muon
         << " VETO[0] = " << e.VETO[0].energy
         << " ECALSUM[0] = " << e.ECALSUM[0].energy
         << " S1 = " << e.S1.energy
         << endl;
    */
    
    // calc cuts
    const double master_tmean = 230.;
    const double master_tsigma = 13.;
    const bool cutTrigger = fabs(e.masterTime - master_tmean) < master_tsigma;
    
    const CellXY emaxpos = eT.ecalMaxCell();
    const bool cutE33max = (emaxpos.ix == 3) && (emaxpos.iy == 3);
    
    const bool cutShower = ecalCheckShower(eT, emaxpos);
    
    const bool cutSrd0 = inrange(eT.SRD[0].energy, 1*MeV, 80*MeV);
    const bool cutSrd1 = inrange(eT.SRD[1].energy, 1*MeV, 80*MeV);
    const bool cutSrd2 = inrange(eT.SRD[2].energy, 1*MeV, 80*MeV);
    const bool cutSrd = (cutSrd0 && cutSrd1) || (cutSrd0 && cutSrd2) || (cutSrd1 && cutSrd2);
    const bool cutSrdAll = (cutSrd0 && cutSrd1 && cutSrd2);
    
    const bool cutVeto = (eT.VETO[0].energy + eT.VETO[1].energy < 15*MeV) &&
                         (eT.VETO[2].energy + eT.VETO[3].energy < 15*MeV) &&
                         (eT.VETO[4].energy + eT.VETO[5].energy < 15*MeV);
    
    const bool cutVetoPhys = (eT.vetoEnergy(0) < 15*MeV) &&
                             (eT.vetoEnergy(1) < 15*MeV) &&
                             (eT.vetoEnergy(2) < 15*MeV);
    
    const bool cutVetoPhys10 = (eT.vetoEnergy(0) < 10*MeV) &&
                             (eT.vetoEnergy(1) < 10*MeV) &&
                             (eT.vetoEnergy(2) < 10*MeV);
    
    const bool cutEcal = (rs.ecal < 150) && (rs.ecalT/rs.ecal > 0.7);
    
    const bool cutHcal = (rs.hcalT > 0.2 * rs.hcal);
    
    // check most of HCAL0 energy deposition is in central cell
    const int hcal0_n = countCellsHcal0(eT, 0.2);
    const double hcal0_tot = eT.hcalTotalEnergy(0);
    const double hcal0_11 = eT.HCAL[0][1][1].energy;
    const bool cutHcal0veto = (hcal0_n < 3) || (hcal0_11/hcal0_tot > 0.5);
    
    const bool cutTrkmom = (90 < rs.track.momentum) && (rs.track.momentum < 110);
    const bool cutTrkintheta = (rs.track.in.theta() < 0.01);
    
    const bool cutAll = cutTrigger && cutE33max && cutShower && cutSrd && cutVeto && cutEcal;
    
    const bool cutDump1 = cutAll && (rs.ecalT < 75) && (rs.hcalT < 10);
    const bool cutDump2 = cutAll && (rs.ecalT < 55) && (10 < rs.hcalT) && (rs.hcalT < 25);
    
    // fill histograms
    if (true)       group0.Fill(rs);
    if (cutTrigger) group1.Fill(rs);
    if (cutE33max)  group2.Fill(rs);
    if (cutShower)  group3.Fill(rs);
    if (cutSrd)     group4.Fill(rs);
    if (cutSrdAll)  group4all.Fill(rs);
    if (cutVeto)    group5.Fill(rs);
    if (cutVetoPhys) group5phys.Fill(rs);
    if (cutVetoPhys10) group5phys10.Fill(rs);
    if (cutEcal)    group6.Fill(rs);
    if (cutHcal)    group7.Fill(rs);
    if (cutHcal0veto) group7hcal0veto.Fill(rs);
    if (cutTrkmom) group8.Fill(rs);
    if (cutTrkintheta) group9.Fill(rs);
    
    if (cutAll)     groupAll.Fill(rs);
    
    if (cutAll && cutHcal0veto) groupAllHcal0veto.Fill(rs);
    if (cutAll && cutTrkmom) groupAllTrkmom.Fill(rs);
    if (cutAll && cutTrkintheta) groupAllTrkintheta.Fill(rs);
    
    if (cutDump1 || cutDump2) {
      cout << "INFO: dump event,"
           << " id = " << e.id()
           << " ecalT = " << rs.ecalT
           << " hcalT = " << rs.hcalT
           << endl;
      
      fout.write((const char*) manager.GetEvent().GetBuffer(), manager.GetEvent().GetLength());
    }
  }
  
  group0.End();
  group1.End();
  group2.End();
  group3.End();
  group4.End();
  group4all.End();
  group5.End();
  group5phys.End();
  group5phys10.End();
  group6.End();
  group7.End();
  group7hcal0veto.End();
  group8.End();
  group9.End();
  groupAll.End();
  groupAllHcal0veto.End();
  groupAllTrkmom.End();
  groupAllTrkintheta.End();
  
  // save all histograms to .root file
  file->Write();
  file->Close();
  
  return 0;
}
