// xcorrect, ycorrect - functions to calculate electron entry coordinates from the shower center of gravity
//                      tabulated for 3x3 cells and 100 GeV electron (MC simulation).
// ecalHitCoord - function to calculate electron entry coordinates from the event. Uses the above functions.

#pragma once

#include "TGraph.h"

#include "conddb.h"

double CenterGravityCorrect(double CenterGravityIn)
{
  double ycal[20]={0., 0.3,  0.6,  0.9,  1.2,  1.55, 1.9,  2.3,  2.7,  3.2,  3.7,  4.3,   5.,    5.8,   6.7,   7.9,   9.4,  11.2, 14.,  19.1};
  double ycorr[20]={0, 1.25, 2.37, 3.43, 4.62, 5.71, 6.73, 7.74, 8.76, 9.9, 10.77, 11.72, 12.77, 13.65, 14.47, 15.47, 16.4, 17.3, 18.26, 19.1};

  double CenterGravityAbs = fabs(CenterGravityIn);
  TGraph g(20, ycal, ycorr);
  double CenterCorrected = g.Eval(CenterGravityAbs, 0, "S");
  if(CenterGravityIn < 0.) CenterCorrected *= -1.;
  return CenterCorrected;
}

double xcorrect(double xcalin, double eecal, double EBeam)
{
  // The X correction below is the extrapolation to the front face of ECAL 
  // for the beam deflected by magnets by ~20 mrad
  // (100 GeV beam deflected by 2 magnets with maximal field)
  // The energy dependence is theoretical, uses the critical energy in lead 0.007 GeV
  return CenterGravityCorrect(xcalin) + 2.59*(100./EBeam)*log(eecal/0.007)/log(100./0.007);
}

TVector3 ecalHitCoord(const RecoEvent& e,
  const int centerx, const int centery, const int centersize0 = 1)
{
  double ecal = 0.;
  double ecalx = 0.;
  double ecaly = 0.;
  int centersize = centersize0;
  if(centersize > 2) centersize = 2;
  
  // extract ECAL dimensions from geometry
  const int NCellX = ECAL_pos.Nx;
  const int NCellY = ECAL_pos.Ny;
  
  for (int d = 0; d < 2; ++d) {
    for (int x = 0; x < NCellX; ++x) {
      for (int y = 0; y < NCellY; ++y) {
        // skip non-central cells
        const bool isOutside = abs(centerx-x) > centersize || abs(centery-y) > centersize;
        if (isOutside) continue;
        
        const double E = e.ECAL[d][x][y].energy;
        ecal  += E;
        ecalx += E * x;
        ecaly += E * y;
      }
    }
  }
  
  // center of mass
  ecalx /= ecal;
  ecaly /= ecal;
  
  // translate to {centerx, centery} origin
  ecalx -= centerx;
  ecaly -= centery;
  
  // scale to ECAL cell size 38.2 mm
  // TODO: move cell size to conddb/geometry
  ecalx *= 38.2;
  ecaly *= 38.2;
  
  return TVector3(xcorrect(ecalx, ecal, NominalBeamEnergy), CenterGravityCorrect(ecaly), 0.);
}

TVector3 ecalHitCoord(const RecoEvent& e)
{
  return ecalHitCoord(e, ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy);
}
