#pragma once

#include <string>
#include <map>
#include <set>
#include <vector>

#include <TVector3.h>

#include "led.h"
#include "beamOnlyCorrection.h"

//header added to dynamically determine the topology of the chip reading a cell
#include "DaqEventsManager.h"
#include "ChipSADC.h"
#include "ChipNA64WaveBoard.h"
using namespace std;

//MM flags and constants
extern const double mm2cm;              //conversion mm to cm
extern const double cm2mm;              //conversion cm to mm
extern double emass;                    //GeV
extern const int MM_apv_time_samples;   // APV samples collected during data taking
extern const bool MM_clear_clusters;    // Clear clusters that shares too many channel with max clusters (multiplexing)
extern const bool MM_time_ratio_sigma;  // Calculated time using the COMPASS algorithm

// MM cluster/hit sorting method:
//  kSort_ByCharge (fallback for ByTiming)
//  kSort_BySize
//  kSort_ByTiming (default, requires timing calibrations)
//  kSort_No
enum {kSort_ByCharge, kSort_BySize, kSort_ByTiming, kSort_No};

extern int MM_cluster_sort;
extern int MM_hit_sort;

string conddbpath();

// convert run timestamp to NA64 session string, like 2021, 2021mu, 2022mu, ...
string na64session(const time_t time);

string FindApvPedestalFile(const time_t time, const string detname, const int datesize = 25);

// Micromega
struct time_parameters
{
  //parameters for the timing
  /*parameters of the function described in: https://arxiv.org/pdf/1708.04087.pdf
    in the order:
    0:  r0
    1:  t0
    2:  a0
  */  
  double r0;
  double t0;
  double a0;
};

struct time_errors
{
    //covariance matrix follow
  /*first three entries are the standard deviation squared of the parameters,
    after that the covariance are given, in the following order assuming symmetric matrix:
    0: r02
    1: t02
    2: a02
    3: r0t0
    4: r0a0
    5: t0a0
   */
  double r02;
  double t02;
  double a02;
  double r0t0;
  double r0a0;
  double t0a0;
};

struct MM_time_point {
  time_parameters parameters;
  time_errors errors;

  //calculate time and error
  double time(const double& q_num,const double& q_den) const;

  double error(const double& q_num,const double& q_den,const double& sigma_n) const;
};

struct MM_plane_timing {
  //two ratio for each plane: A0/A2 and A1/A2
  MM_time_point a02;
  MM_time_point a12;

  // Calculation of t0 using information of both ratios
  // and based on method cluster_time() in mm.h
  // Calculated time using the COMPASS algorithm
  double t0() const;

  // Calculation of t0_err using information of both ratios
  // and based on method cluster_time_err() in mm.h
  // Calculated time error using the COMPASS algorithm
  double t0_err() const;
};

struct MM_plane_calib {
  //Micromegas parameters
  unsigned int MM_Nchannels;         // Number of electrinics channels of the plane
  unsigned int MM_Nstrips;           // Number of strips of the plane
  set<unsigned int> badStrips;       // Tag strips that are known to be not working or have bad gain
  double MM_strip_step;              // MM strip-to-strip distance in mm
  unsigned int MM_min_cluster_size;  // Minimum cluster size accepted in clusterization
  //sigmas and timing
  vector<double> sigma;              // Vector containing sigma of the pedestals
  MM_plane_timing timing;            // Struct containing information for timing reconstruction
  double time;
  double time_err;
  //mapping
  map<int, vector<int> > MM_map;     // electronics channel (0-63) to detector channel (strip 0-319) map
  map<int, int> MM_reverse_map;      // detector channel to electronic channel map (reverse map)
  
  // === MM mapping ===
  void print_MM_map();
  void Create_MM_reverse_map();
  
  //Init mapping using standard Micromegas mapping file, csv, first row channel, the rest are strips
  void Read_Multiplex_mapping(const string filename);
  
  //default constructor
  MM_plane_calib();
  
  void SetDesign2016();
  
  void SetDesign2021Small();

  void SetDesign2021Large();

  void ReadTimingCalib(const string filename);

  bool hasTime() const;

  void insertBadWire(unsigned int badWire);
};

ostream& operator<<(ostream& os,const MM_plane_calib& e);

struct MM_calib {
  MM_plane_calib xcalib;
  MM_plane_calib ycalib;
  string name;
  
  void SetDesign2016();
  
  void SetDesign2021Small();
  
  void SetDesign2021Large();
  
  void ReadSigmaCalib(const string filename);

  void ReadSigmaCalib(const time_t time);

  bool hasTime() const;
};

ostream& operator<<(ostream& os,const MM_calib& e);

//declare calibration
extern MM_calib MM1_calib;
extern MM_calib MM2_calib;
extern MM_calib MM3_calib;
extern MM_calib MM4_calib;
extern MM_calib MM5_calib;
extern MM_calib MM6_calib;
extern MM_calib MM7_calib;
extern MM_calib MM8_calib;
extern MM_calib MM9_calib;
extern MM_calib MM10_calib;
extern MM_calib MM11_calib;
extern MM_calib MM12_calib;
extern MM_calib MM13_calib;
//dummy calibration
extern MM_calib MM_nocalibration;

// GEM calibration data
struct APV_channel_calib {
  double channelflag;
  double pedestalamp;
  double pedestalsigma;
  double calibrationamp;
  double calibrationsigma;
};

struct APV_channel_id {
  int srcID;
  int adcID;
  int chipID;
  int chipchannel;
};

ostream& operator<<(ostream& os, const APV_channel_id& c);

bool operator<(const APV_channel_id& left, const APV_channel_id& right);

struct GEM_calib {
  string name;
  map<APV_channel_id, APV_channel_calib> data;
  
  operator bool() const { return !data.empty(); }
  
  // lookup calibration data for given electronics channel
  APV_channel_calib FindGEMPedestal(const APV_channel_id& id) const;

  void load(const time_t time);
  
  void load(const char* fx, const char* fy);
  
  // load APV pedestals data file
  // origin - gemMonitor / gemAnalyze.cc / function readPedestalfile()
  //   https://gitlab.cern.ch/compass-gem/gemMonitor/blob/master/src/gemAnalyze.cc#L71
  //
  // example file format:
  //
  //#ChipAPV 0 621 3 14
  //1 757.851 5.67425 0 0
  //...
  //#ChipAPV 0 621 3 15
  //1 745.341 5.99112 0 0
  //..
  void Read_APV_Pedestals(const char* fname);
};

extern GEM_calib GM01_calib;
extern GEM_calib GM02_calib;
extern GEM_calib GM03_calib;
extern GEM_calib GM04_calib;

/*
 * Straw Tubes calibration struct
 *
 * In these objects the parameters needed for reconstruction are initialized and used by the Straw
 * struct. Straw calibration should be initialized with the appropiate design (large UV, small XY).
 */

// r(t) function for 6mm straw tubes
double rtStraw6(int time);

// Inverse r(t) function for 6mm straw tubes
int invRtStraw6(double pos);

struct ST_calib {
  uint t0;
  uint timeWindow;
  double stereoAngle;
  string name;
  bool invertAxis;

  // default constructor
  ST_calib() :
    t0(0), timeWindow(0),
    stereoAngle(0.),
    invertAxis(false)
  {;}

  // Small XY
  void InitXY6Design(uint _t0);

  // Large UV
  void InitUV6Design(uint _t0);

  bool hasTcalib() const { return (t0 != 0) && (timeWindow != 0); }
  bool isUV() const { return (stereoAngle > 0.); }
};

//declare calibration
extern ST_calib ST01_calib;
extern ST_calib ST02_calib;
extern ST_calib ST03_calib;
extern ST_calib ST04_calib;
extern ST_calib ST05_calib;
extern ST_calib ST06_calib;
extern ST_calib ST11_calib;
extern ST_calib ST12_calib;
extern ST_calib ST13_calib;
//dummy calibration
extern ST_calib ST_nocalibration;

//geometry of the setup summarized
struct DetGeo {
  TVector3 pos;
  double Angle;
  int Nx;
  int Ny;
  
  // this flag is in use to control invertion of MM detectors X axis
  // MM.invertAxis=true for runs before session 2021B
  // MM.invertAxis=false starting from session 2021B to have same coordinate system as the rest of detectors
  bool invertAxis;
  
  // TODO: re-think the coordinate system of MM before 2021B
  //       correct it by adjusting the MM mapping?
  //       and remove this flag
  
  DetGeo(): Angle(0), Nx(0), Ny(0), invertAxis(false) {}
};

// Struct for GEM geometry description
struct GEMGeometry {
    double size;
    double center;
    double pitch;
    double angle;
    // absolute position of the detector
    TVector3 pos;

    bool isSet() const { return (size != 0.); }
};

struct CellXY {
  int ix;
  int iy;
};

// Struct for description of magnet parameters
struct MagInfo {
  string name;

  // simplified info, for pre-fit/analytical momentum
  double B; // magnetic field, Tesla
  double L; // depth of magnet
  double gap_width; 
  double gap_height;
  TVector3 entrance; // position of centre of magnet upstream face
  TVector3 exit; // position of centre of magnet downstream face

  // genfit-specific magnet parameters
  double field_factor;

  //metadata
  string pathToMap;

  /// TODO: consider adding magnet depth based on entrance and exit
  //        Check if usage of measured depth would be compatible with using 
  //        depth for the new field map parsing method (where dimensions of 
  //        magnets are used to discretise the field)
  // double L() const { return (exit-entrance).Mag(); }

};

// Struct for full description of tracking setup
struct TrackingInfo {
  map<string,MagInfo> magnets;
  string placements_path;
  string geometry_path;

  void add_magnet(MagInfo magnet){
    if(magnets.find(magnet.name) != magnets.end()){
      throw std::runtime_error("(TrackingInfo::add_magnet) Error: Magnet already exists!\n");
    }
    magnets.insert({magnet.name, magnet});
  }
};

bool operator==(const CellXY& a, const CellXY& b);

extern DetGeo ECAL_pos;
extern DetGeo HCAL_pos;
extern DetGeo S2_pos;
extern DetGeo ST2_pos;
extern DetGeo SRD_pos;
extern DetGeo S1_pos;
extern DetGeo pipe_pos;
extern DetGeo MAGD_pos;
extern DetGeo MAGU_pos;
extern DetGeo Veto_pos;
extern DetGeo S0_pos;
extern DetGeo ST1_pos;
//MicroMegas
extern DetGeo MM1_pos;
extern DetGeo MM2_pos;
extern DetGeo MM3_pos;
extern DetGeo MM4_pos;
extern DetGeo MM5_pos;
extern DetGeo MM6_pos;
extern DetGeo MM7_pos;
extern DetGeo MM8_pos;
extern DetGeo MM9_pos;
extern DetGeo MM10_pos;
extern DetGeo MM11_pos;
extern DetGeo MM12_pos;
extern DetGeo MM13_pos;

//GEM
extern DetGeo GM1_pos;
extern DetGeo GM2_pos;
extern DetGeo GM3_pos;
extern DetGeo GM4_pos;
extern GEMGeometry GM01Geometry;
extern GEMGeometry GM02Geometry;
extern GEMGeometry GM03Geometry;
extern GEMGeometry GM04Geometry;

extern GEMGeometry ST01Geometry;
extern GEMGeometry ST02Geometry;
extern GEMGeometry ST03Geometry;
extern GEMGeometry ST04Geometry;
extern GEMGeometry ST05Geometry;
extern GEMGeometry ST06Geometry;
extern GEMGeometry ST11Geometry;
extern GEMGeometry ST12Geometry;
extern GEMGeometry ST13Geometry;

// magnetic field
extern double MAG_field; // Tesla
extern double NominalBeamEnergy;
extern int NominalBeamPDG;
extern TVector3 ECAL0BEAMSPOT_pos;
extern CellXY ECAL0BEAMCELL;

// tracking info
extern TrackingInfo geo;

void Reset_Geometry();

TVector3 CCellECAL(int cellx, int celly, double cellsize);



// peak time (t0) search method:
//  kT0_MaxSample   : index of sample with maximum amplitude
//  kT0_CenterOfMass: waveform center of mass index
//  kT0_RisingEdge  : index of waveform rising edge - starting point position
//  kT0_RisingEdgeHH  : index of waveform rising edge - half height point position
enum {kT0_MaxSample, kT0_CenterOfMass, kT0_RisingEdge, kT0_RisingEdgeHH};
extern int SADC_t0_method;

// best peak search method:
//  kPeak_MaxAmplitude: 
//  kPeak_BestTiming: 
enum {kPeak_MaxAmplitude, kPeak_BestTiming};
extern const int SADC_peak_method;

// SADC corrections method:
//  - no corrections
//  - manual corrections
//  - LED corrections
enum {kAmplitude_No, kAmplitude_Manual, kAmplitude_LED};
extern int SADC_correction_method;

// trigger amplitude threshold (ADC counts)
extern const int TRIG_raw_threshold;

// hodoscope amplitude threshold
extern const double HODO_threshold;
// Hodoscope (chile) threshold 
extern const double HODO2_threshold;

// MeV to GeV convertion
extern const double MeV;

void Reset_Reco_Options();

// ADC calibration data (SADC/WB)
struct CaloCalibData {
  string name;   // channel name
  float K;       // energy
  float tmean;   // mean time
  float tsigma;  // time spread
  bool isTmeanRelative; // tmean meaning: true - difference to master time, false - absolute time
  
  bool doWaveformFit;   //enable Waveform fitting for better time and energy estimate

  // activate additional filtering in waveform reco to reject peaks
  // with very sharp rising edge:
  //   wave(peak-1) < minPeakRaisingSlopeSpeed*wave(peak)  -->  reject
  float minPeakRaisingSlopeSpeed;

  //additional fields that are different among MSADC and WB
  struct ADCdata_t{
    bool isWB;            //SADC=0, WB=1
    unsigned int pedestal_length;  //n. samples to consider for pedestal calculation
    // limit the range of SADC waveform for signal amplitude search
    // (usefull to avoid pileup effect in the head and tail of waveform)
    unsigned int signal_range_begin; //first sample for signal range
    unsigned int signal_range_end;   //last sample for signal range
    unsigned int maxADC;             //max ADC value
    double sampling_interval; //sampling period in ns
    
    //pulse shape exponential tail decay time constantin, units=adc samples
    // TODO: rewise units to ns
    double pulse_shape_decay;

  };
  ADCdata_t adc;
  
  // timing calibration is defined
  bool hasTcalib() const { return (tsigma < 9999.); }
  
  float texpected(const float master) const;
  
  string detname() const;
  
  explicit CaloCalibData(const std::string &n = "");
};

ostream& operator<<(ostream& os, const CaloCalibData& c);

// Actual ECAL, HCAL size:
//   before 2021mu: ECAL=2* 6x6, HCAL=4* 3x3
//   from 2021mu:   ECAL=2* 5x6, HCAL=2* 6x3

extern CaloCalibData BGO_calibration[8];
extern CaloCalibData SRD_calibration[4];
extern CaloCalibData LYSO_calibration[100];
extern CaloCalibData ECAL_calibration[2][12][6];
extern CaloCalibData HCAL_calibration[4][6][3];
extern CaloCalibData VHCAL_calibration[2][4][4];
extern CaloCalibData PKRCAL0_calibration[6][12];
extern CaloCalibData PKRCAL1_calibration[12][12];
extern CaloCalibData VETO_calibration[6];
extern CaloCalibData WCAL_calibration[3];
extern CaloCalibData WCAT_calibration;
extern CaloCalibData VTWC_calibration;
extern CaloCalibData VTEC_calibration;
extern CaloCalibData S0_calibration;
extern CaloCalibData S1_calibration;
extern CaloCalibData S2_calibration;
extern CaloCalibData S3_calibration;
extern CaloCalibData S4_calibration;
extern CaloCalibData T2_calibration;
extern CaloCalibData V1_calibration;
extern CaloCalibData V2_calibration;
extern CaloCalibData WBTRIG_calibration;
extern CaloCalibData Smu_calibration;
extern CaloCalibData BeamK_calibration;
extern CaloCalibData LTG_calibration[6];
extern CaloCalibData CHCNT_calibration[2];
extern CaloCalibData MUON_calibration[4];
extern CaloCalibData ECALSUM_calibration[6];
extern CaloCalibData HOD_calibration[3][2][16]; // indeces: [z index][0=x/1=y plane][cell id]
extern CaloCalibData CALO_nocalibration;

extern CaloCalibData::ADCdata_t wb_ADCdata;
extern CaloCalibData::ADCdata_t sadc_ADCdata;

// reset calibration coefficients
void Reset_Calib();

// === calibration data for 2015 runs ===
// init calibration coefficients
// calibration data received from alexander.toropin@cern.ch
void Init_Calib_2015_BGO_ECAL_HCAL();

// === calibration data of 2016A runs ===
void Init_Calib_2016A_ECAL_cal1();

void Init_Calib_2016A_BGO();

void Init_Calib_2016A_SRD();

void Init_Calib_2016A_HCAL_cal1();

void Init_Calib_2016A_ECAL_cal2();

void Init_Calib_2016A_HCAL_cal2();

// === calibration data 2016B ===
// data is taken from pcdmrc01:/online/detector/calibrs/2016.xml/ECALT.xml on 2016-11-17:
// -rw-rw-r-- 1 daq daq 2673 Oct 24 22:41 /online/detector/calibrs/2016.xml/ECALT.xml
void Init_Calib_2016B_ECAL();

// data is taken from pcdmrc01:/online/detector/calibrs/2016.xml/HCAL.xml on 2016-11-17:
// -rw-rw-r-- 1 daq daq 3318 Oct 27 16:21 /online/detector/calibrs/2016.xml/HCAL.xml
void Init_Calib_2016B_HCAL();

// calibrations prepared by Vladimir Poliakov
void Init_Calib_2016B_SRD();

void Init_Calib_2016B_VETO();

// === Additional corrections to the calibration data 2016B ===
// === Can be revised or removed after the implementation of the LED corrections
void Corr_Calib_2016B(const int run);

void Init_Calib_2016B_VISMODE();

// === calibration data 2017 ===
// data is taken from pcdmrc01:/online/detector/calibrs/2017.xml/ECALT.xml on 2017-09-14:
// -rw-r--r-- 1 daq daq 2673 Sep 14 10:38 /online/detector/calibrs/2017.xml/ECALT.xml
// Energy calibration calculated by Alexander Toropin, based on runs 3119-3159 (second stage of calibration runs)
void Init_Calib_2017_ECAL();

// Calibration prepared by Vladimir Samoylenko
// Details:
//   ELOG / Message ID: 122 / Entry time: Sun Sep 10 21:09:40 2017
//   http://pcdmfs01.cern.ch:8080/Main/122
void Init_Calib_2017_VETO();

// data is taken from pcdmrc01 on 2017-09-19:
// -rw-rw-r-- 1 daq daq 1941 Sep 15 11:05 /online/detector/calibrs/2017.xml/HCAL.xml
// which was extracted from paper logbook by Alexander Toropin
// calibration runs:
//   HCAL0   2902 - 2910
//   HCAL1   ???
//   HCAL2   2850 - 2859
//   HCAL3   2843 - 2849
void Init_Calib_2017_HCAL();

// data is extracted from pcdmfs01 on 2017-10-03:
// -rw-r--r-- 1 daq daq 397 Sep 24 22:22 /online/detector/calibrs/2017.xml/WCAL.xml
// constants calculated by S.Donskov and V.Poliakov based on
//   - some electrons calibration run for WCAL[0] and WCAL[1]
//   - some muons calibration run for Wcatcher
//   - energy deposition simulations by M.Kirsanov
void Init_Calib_2017_WCAL(const int run);

// calibrations prepared by Vladimir Poliakov and extracted from
//   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2017/list_runs_Sep_2017_v3.pdf
//   (7 September record / SRD MIP amplitude in ADC counts)
void Init_Calib_2017_SRD();

void Init_Calib_2017_MasterTime(const int run);

void Init_Calib_2017_Counters();

// === calibration data 2018 ===
// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 2713 May 20 12:38 /online/detector/calo/H4/calibrs/2018.xml/ECALT.xml
void Init_Calib_2018_ECAL(const int run);

// manual run-dependent corrections
void Init_Calib_2018_ECAL_Manual(const int run);

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 1997 May 19 18:24 /online/detector/calo/H4/calibrs/2018.xml/HCAL.xml
void Init_Calib_2018_HCAL(const int run);

// manual run-dependent corrections
void Init_Calib_2018_HCAL_Manual(const int run);

// data source:
// * energy (page1 / 18May record):
//   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2018/list_runs_May_2018_v1.pdf
//
// * timing (at pcdmfs01):
//   -rw-rw-r-- 1 daq daq 514 May 20 17:39 /online/detector/calo/H4/calibrs/2018.xml/SRDT.xml
void Init_Calib_2018_SRD(const int run);

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 982 May 28 20:17 /online/detector/calo/H4/calibrs/2018.xml/VETO.xml
void Init_Calib_2018_VETO(const int run);

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq  582 Jun 14 22:36 /online/detector/calo/H4/calibrs/2018.xml/WCAL.xml
// -rw-r--r-- 1 daq daq 1422 Jun 17 13:09 /online/detector/calo/H4/calibrs/2018.xml/BEAMCOUNTERS.xml
void Init_Calib_2018_WCAL(const int run);

// manual run-dependent corrections
void Init_Calib_2018_WCAL_Manual(const int run);

// additional after LED corrections
void Init_Calib_2018_WCAL_AfterLed(const int run);

void Init_Calib_2018_MasterTime(const int run);

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 1422 Jun 17 13:09 /online/detector/calo/H4/calibrs/2018.xml/BEAMCOUNTERS.xml
void Init_Calib_2018_Counters(const int run);

// === calibration data 2021 ===
void Init_Calib_2021_MasterTime(const int run);

// calibrations by Dipanwita Banerjee
void Init_Calib_2016B_MM(const time_t time);

void Init_Calib_2016_GEM(const time_t time);

void Init_Calib_2017_MM(const time_t time);

void Init_Calib_2017_GEM(const time_t time);

void Init_Calib_2018_MM(const time_t time);

// PRELIMINARY calibration for 2021 run
void Init_Calib_2021_MM(const time_t time);

// MM calibrations, 2021B
void Init_Calib_2021B_MM(const time_t time);

void Init_Calib_2022A_MM(const time_t time);

void Init_Calib_2022B_MM(const time_t time, const int run);

void Init_Calib_2023A_MM(const time_t time);

void Init_Calib_2023B_MM(const time_t time, const int run);

void Init_Calib_2024A_MM(const time_t time, const int run);

void Init_Calib_2024B_MM(const time_t time, const int run);

void Init_Calib_2018_GEM(const time_t time);

void Init_Calib_2021_GEM(const time_t time);

void Init_Calib_2021B_GEM(const time_t time);

void Init_Calib_2022A_GEM(const time_t time);

void Init_Calib_2022B_GEM(const time_t time);

void Init_Calib_2022A_ST();

void Init_Calib_2022B_ST(const int run);

void Init_Calib_2023A_ST(const int run);

void Init_Calib_2023B_ST();

void Init_Calib_2024A_ST();

void Init_Calib_2024B_ST();

void Init_Geometry_2016A_part1();

void Init_Geometry_2016A_part2();

// measured by Dipanwita Banerjee
void Init_Geometry_2016B();

// measured by Dipanwita Banerjee
void Init_Geometry_2017();

// INVISIBLE mode, beam energy 100 GeV
// setup geometry of runs 3574 - 4207
void Init_Geometry_2018_invis100();

// VISIBLE mode, beam energy 150 GeV
// setup geometry of runs 3574 - 4207
void Init_Geometry_2018_vis150();

//Preliminary Geometry for beamtime 2021
void Init_Geometry_2021();

// Geometry 2021B (muon mode)
void Init_Geometry_2021B();

// Geometry 2022A (muon) 
void Init_Geometry_2022A();

void Init_Geometry_2022B(const int run);

void Init_Geometry_2023A(const int run);

// DRAFT BASED ON Geometry 2022A (muon) 
void Init_Geometry_2023B(const int run);

// DRAFT BASED ON Geometry 2023A
void Init_Geometry_2024A(const int run);

// DRAFT BASED ON Geometry 2023B (muon)
void Init_Geometry_2024B(const int run);

namespace p348 { struct CaloCellCalib; }

void copy_calo_cell_calib( CaloCalibData & dest, const p348::CaloCellCalib & src );

void Init_SADC_Calibs(int runNo);

void Init_ADC(const CS::DaqEventsManager& manager);

// print calirimeters calibration data
// TODO: include the rest of calo. cells
void print_calib();

void Init_Reconstruction(const CS::DaqEventsManager& manager);
//This function is used to load, for 2023A, the multiplicative extra-factor that multiplies all cells factors
map<int, double> LoadRunFactor2023A(string fname);


// NOTE: this function is implemented in `led.h' and used in p348reco.h
void Set_LED_spill(int nrun, int nspill);
// NOTE: this function is implemented in `beamOnlyCorrection.h` and used in p348reco.h
void Set_BeamOnlyData_spill(int nrun, int nspill);

template<typename T>
T cast(const std::string& s)
{
  std::istringstream iss(s);
  T x;
  iss >> x;
  return x;
}


