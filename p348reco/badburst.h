#pragma once

#include "led.h"
#include <set>
#include <sstream>
#include <string>
// bad spills list

set<LED_spill_id_t> bad_burst;

string RunNumberToBurstName(int run)
{
  const bool is2024A = (10316 <= run && run <= 11467);
  if (is2024A) return "badburst-2024A.txt";

  const bool is2023A = (8459 <= run && run <= 9717);
  if (is2023A) return "badburst-2023A.txt";

  const bool is2023B = (9718 <= run && run <= 10315);
  if (is2023B) return "badburst-2023B.txt";

  const bool is2022B = (6035 <= run && run <= 8458);
  if (is2022B) return "badburst-2022B.txt";
  
  const bool is2018 = (3574 <= run && run <= 4310);
  if (is2018) return "badburst-2018-invis-3898_4201-v2.txt";
  
  return "";
}

void string_char_replace(std::string& s, char a, char b)
{
  while (true) {
    const std::string::size_type pos_a = s.find(a);
    const bool has_a = (pos_a != std::string::npos);
    if (!has_a) break;
    s[pos_a] = b;
  }
}

void LoadBadBurst(int run)
{
  const string bname = RunNumberToBurstName(run);
  
  if (bname.empty()) {
    cout << "INFO: LoadBadBurst() no list of bad spills for the session of run " << run << endl;
    
    // insert a single dummy entry to avoid re-loading on each new event
    bad_burst.clear();
    LED_spill_id_t id = {0, 0};
    bad_burst.insert(id);
    return;
  }
  
  const string fname = conddbpath() + bname;
  cout << "INFO: LoadBadBurst() loading file " << fname << endl;
  bad_burst.clear();
  
  ifstream f(fname);
  
  if (!f) {
    throw std::runtime_error("Error to open file");
  }
  
  std::string line;
  while (std::getline(f,line)) {
    //Format
    // Run Spill Spill Spill ... Spill  [items could be separated by ',']
    // where "Spill" can be either a single integer or a range spillMin-spillMax
    
    string_char_replace(line, ',', ' ');
    
    //split the line into the words
    std::string word;
    std::vector<std::string> words;
    std::istringstream iss(line);
    while (iss >> word) {
      words.push_back(word);
    }
    
    //There should be at least two words, the Run# and the list of spills
    if (words.size() < 2) {
      cout<<"LoadBadBurst() line: "<<line<<" not recognized"<<endl;
      throw std::runtime_error("ERROR: fail to parse input line");
    }
    
      const int runN=atoi(words[0].c_str());
      
      for (size_t j = 1; j < words.size(); ++j) {
        word = words[j];
        std::string::size_type pos = word.find('-');
        const bool isRange = (pos != std::string::npos);
        int s1=0,s2=0;
        
        if (isRange){
          s1=atoi(word.substr(0, pos).c_str());
          s2=atoi(word.substr(pos+1).c_str());
        }
        else{
          s1=s2=atoi(word.c_str());
        }
        
        // check spill ID
        const bool isValid = (s1>0) && (s2>0) && (s2>=s1);
        if (!isValid) {
          cout<<"LoadBadBurst() line: "<<line<<" : word " << word << " not recognized as a spill ID"<<endl;
          throw std::runtime_error("ERROR: fail to parse input line");
        }
        
          for (int ii=s1;ii<=s2;ii++){
            LED_spill_id_t id = {0, 0};
            id.run=runN;
            id.spill=ii;
            bad_burst.insert(id);
          }
      }
    
    // end of file or IO error
    if (!f) break;
    
    // TODO: record format check
    
    // TODO: (sanity) duplicates check?
  }
  
  // check file state
  if (!f.eof()) {
    // file read stop due to format or IO error
    //std::cout << "eof()=" << f.eof() << " fail()=" << f.fail() << endl;
    throw std::runtime_error("Error to read bad burst list - file format or IO error");
  }
  
  cout << "INFO: LoadBadBurst()"
       << " total bad spills = " << bad_burst.size()
       << endl;
}


bool IsBadBurst(int run, int spill)
{
  if (bad_burst.empty()) LoadBadBurst(run);
  
  const LED_spill_id_t id = {run, spill};
  const bool contains = (bad_burst.find(id) != bad_burst.end());
  return contains;
}
