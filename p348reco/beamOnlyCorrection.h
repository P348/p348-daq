#pragma once

#include <map>
#include <string>

#include "led.h"

using namespace std;

extern map<LED_spill_id_t, LED_event_t> BeamOnlycalib;

extern LED_event_t BeamOnlyref;    // Reference run data
extern LED_event_t BeamOnlycurr;   // Current run data
extern LED_spill_id_t BeamOnlycurrid; // Current run data ID (run, spill)
extern LED_spill_id_t BeamOnlyrefid; // Reference run data ID (run, spill)

void Set_BeamOnlyData_spill(int nrun, int nspill);
string BeamOnlyPath(const int run);
string BeamOnlyPath(const LED_spill_id_t id);

void Load_BeamOnly_corrections_Format2023A(const string fname);

void Init_BeamOnly_corrections_2023A(const int run);

