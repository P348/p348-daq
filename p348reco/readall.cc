// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include "TStyle.h"
#include "TProfile.h"

// c++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

// P348 reco
#include "p348reco.h"
#include "shower.h"

// timing
#include "timecut.h"

int procev(RecoEvent &e);
void endrun();

int ievproc=-1;
int ifmc=0;

TH2I* ehplot;
TH1D* chi2profile;
TH1D* eecal;

int main(int argc, char *argv[])
{
  if (argc < 2) {
    std::cerr << "Usage: ./mkcode.exe <file name>" << std::endl;
    return 1;
  }
  
  std::string inpfile(argv[1]);
  int inputtype = 0;
  if( inpfile.find(std::string(".d")) == inpfile.size()-2 ) inputtype = 1;

  // ROOT calls
  TFile* hOutputFile = new TFile("hist.root", "RECREATE");

  if(inputtype == 0) { // Reading data file(s) ---------------------------

    std::cout << "Reading data files, first is " << argv[1] << std::endl;

    DaqEventsManager manager;
    manager.SetMapsDir("../maps");
    for (int i = 1; i < argc; ++i)
      manager.AddDataSource(argv[i]);
    manager.Print();

    while (manager.ReadEvent()) { // event loop, read event by event
 
      const int nevt = manager.GetEventsCounter();

      // print progress
      if (nevt % 1000 == 1)
        cout << "===> Event #" << manager.GetEventsCounter() << endl;

      // decode event (prepare digis)
      const bool decoded = manager.DecodeEvent();

      // skip events with decoding problems
      if (!decoded) {
        cout << "WARNING: fail to decode event #" << nevt << endl;
        continue;
      }

      // run reconstruction
      RecoEvent e = RunP348Reco(manager);

      // process only "physics" events (no random or calibration trigger)
      if (!e.isPhysics) continue;

      int ifproc = procev(e);
    }
  }  // End of data file(s) reading -------------------------------------

  if(inputtype == 1) { // Reading MC file -------------------------------

    std::cout << "Reading MC file " << argv[1] << std::endl;

    std::ifstream inFile(argv[1]);

    if (!inFile) {
      std::cerr << "ERROR: can't open file " << argv[2] << std::endl;
      return 1;
    }
  
    // event loop, read event by event
    for (int iev = 0; true; iev++) {

      RecoEvent e = RunP348ReadMC(inFile);

      if (!e.mc) break;

      // print progress
      if (iev % 100 == 1)
        std::cout << "===> Event #" << iev << std::endl;

      int ifproc = procev(e);

    }

  } // End of MC file reading -------------------------------------------

  endrun();

  hOutputFile->Write();

  return 0;
}


int procev(RecoEvent &e0)
{
  ievproc++;
  if(ievproc == 0) { // initialization

    ehplot = new TH2I("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 120, 0, 120, 120, 0, 120);
    eecal = new TH1D("eecal", "ECAL energy", 120, 0., 120.);

  }  // end of initialization

  //std::cout << "Event #" << ievproc;

  if(e0.mc) ifmc = 1;

  int ipass = 1;

  int itimingok = 1;

  // apply time cuts
  RecoEvent e = e0;
  if (!e.mc) { // timecut is relevant only for real data events
    timecut(e, 4);
    const double ETot = e0.ecalTotalEnergy();
    const double EGood = e.ecalTotalEnergy();
    if(e.run < 2467) { // Use bad energy cut only for invisible mode
      //if (EGood/ETot < 0.7) itimingok = 0; // old BadEnergy cut
      if (ETot - EGood > 30.) itimingok = 0; // new BadEnergy cut
    }
  }

  const double srd = (e.SRD[0].energy + e.SRD[1].energy + e.SRD[2].energy) / MeV;
  if(srd < 6. || srd > 1000.) ipass = 0;
  //if(e.SRD[0].energy/MeV < 1. || e.SRD[1].energy/MeV < 1. || e.SRD[2].energy/MeV < 1.) ipass = 0;
  bool srd0ok = e.SRD[0].energy/MeV > 1. && e.SRD[0].energy/MeV < 90.;
  bool srd1ok = e.SRD[1].energy/MeV > 1. && e.SRD[1].energy/MeV < 90.;
  bool srd2ok = e.SRD[2].energy/MeV > 1. && e.SRD[2].energy/MeV < 90.;
  int ipasssrd = 0;
  if(srd0ok && srd1ok) ipasssrd = 1;
  if(srd0ok && srd2ok) ipasssrd = 1;
  if(srd1ok && srd2ok) ipasssrd = 1;
  //if(srd0ok && srd1ok && e0.SRD[2].energy/MeV > 1.) ipasssrd = 1; // improved method
  //if(srd0ok && srd2ok && e0.SRD[1].energy/MeV > 1.) ipasssrd = 1;
  //if(srd1ok && srd2ok && e0.SRD[0].energy/MeV > 1.) ipasssrd = 1;
  if(!ipasssrd) ipass = 0;

  double ecal = 0.;
  double prs = 0.;
  for (int d = 0; d < 2; ++d) {
    for (int x = 0; x < 6; ++x) {
      for (int y = 0; y < 6; ++y) {
        ecal += e.ECAL[d][x][y].energy;
        if(d == 0) prs += e.ECAL[d][x][y].energy;
      }
    }
  }
  double imaxcellx=-1, imaxcelly=-1, emaxcell=0.;
  for (int x = 0; x < 6; ++x) {
    for (int y = 0; y < 6; ++y) {
      if((e.ECAL[0][x][y].energy+e.ECAL[1][x][y].energy) > emaxcell) {
        emaxcell = e.ECAL[0][x][y].energy+e.ECAL[1][x][y].energy;
        imaxcellx = x;
        imaxcelly = y;
      }
    }
  }
  int narrowsh=0;
  if(e.ECAL[1][2][2].energy < 0.01*emaxcell &&
     e.ECAL[1][3][2].energy < 0.01*emaxcell &&
     e.ECAL[1][4][2].energy < 0.01*emaxcell &&
     e.ECAL[1][2][3].energy < 0.01*emaxcell &&
     e.ECAL[1][4][3].energy < 0.01*emaxcell &&
     e.ECAL[1][2][4].energy < 0.01*emaxcell &&
     e.ECAL[1][3][4].energy < 0.01*emaxcell &&
     e.ECAL[1][4][4].energy < 0.01*emaxcell) narrowsh=1;

  if(!itimingok) ipass = 0;

  if(imaxcellx != 3 || imaxcelly != 3) ipass = 0;

  if(narrowsh) ipass = 0;

//  if(prs < 0.5) ipass = 0; // This was sometimes in trigger

  double hcal=0, hcal0=0;
  for (int d = 0; d < 4; ++d)
    for (int x = 0; x < 3; ++x)
      for (int y = 0; y < 3; ++y) {
        hcal += e.HCAL[d][x][y].energy;
        if(d == 0) hcal0 += e.HCAL[d][x][y].energy;
      }

  if(!ipass) {
    //std::cout << std::endl;
    return 0;
  }

  // Event passed, analysing and histogramming ------------------------

  if(ecal > 90. && ecal < 115.) std::cout << "shower chi2 = " << calcShowerChi2(e, 3, 3, 3, SP_ECAL) << std::endl;

  ehplot->Fill(ecal, hcal);

  eecal->Fill(ecal);

  return 1;
}


void endrun()
{
  std::cout << "Integral in ECAL = " << eecal->Integral() << std::endl;
}
