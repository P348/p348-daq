#pragma once

// GenFit
#include <Exception.h>
#include <FieldManager.h>
#include <KalmanFitterRefTrack.h>
#include <DAF.h>
#include <StateOnPlane.h>
#include <Track.h>
#include <MaterialEffects.h>
#include <RKTrackRep.h>
#include <AbsTrackRep.h>
#include <TGeoMaterialInterface.h>
#include <EventDisplay.h>
#include "mySpacepointDetectorHit.h"
#include "mySpacepointMeasurement.h"
//GENFIT FIELDS
#include "ConstFieldBox.h"
#include "ConstField2Box.h"
#include "ConstFieldBox.cc"
#include "ConstField2Box.cc"

//root
#include <TVector3.h>
#include <TGeoManager.h>
#include <TClass.h>

//std
#include<numeric>

// P348 reco
#include "mm.h"
#include "tracking.h"

//possible condition for MM
enum MMcond {NOCUT, BEAMSPOTCUT, SINGLECLUS, ALL};

const double MM_minimum_distance = 100;  //in mm
const int myDetId = 1;
//minimum iteration of the Kalman filter
const int MinIter = 5;
//maximum iteration of the Kalman filter
const int MaxIter = 100;

//tollerance of hit in case of beam cut
const double MMXsigma = 3;
const double MMYsigma = 2.58;
const double Beamtol = 3.;
const double EnergyRes = 3.;

struct MManager
{

 //genfit variables
 genfit::AbsKalmanFitter* fitter;
 genfit::Track* track;
 int pdg; //electron
 double detectorResolution; //detector resolution of MM in cm

  // activate event display
  void displayOn() { display = genfit::EventDisplay::getInstance(); }

  // show event display
  void displayShow() { if (display) display->open(); }

 //FUNCTION
 MManager();
 void AddMicromega(const Micromega&,const MMcond);
 void AddEvent(const RecoEvent&);
 void AddEvent(const RecoEvent&,const MMcond);
 bool UpStreamOK() const;
 bool DownStreamOK() const;
 bool CanTrack() const;
 SimpleTrack SimpleTrackingMM();
 SimpleTrack PointDeflectionTracking();
 double GetAngleSimple();
 void PrintMM(unsigned int);
 void Reset();
 //genfit
 void GeometryInit(const TString,const unsigned int,const int,const double);
 void GenfitInit(const TString);
 void MagnetInit();
 void GenfitTrack();
 double GetGenfitmom() const;
 double GetGenfitChi2() const;

private:
 //collection of MM upstream
 vector<const Micromega*> upstream;
 TVector3 mm1;
 TVector3 mm2;
 //collection of MM downstream
 vector<const Micromega*> downstream;
 TVector3 mm3;
 TVector3 mm4;

  // EventDisplay crash on machines with Intel graphics, workaround:
  //   $ export LIBGL_ALWAYS_INDIRECT=1
  //   https://root.cern.ch/phpBB3/viewtopic.php?t=21274
  genfit::EventDisplay* display;

public:
  void CollectPointsSimple(const vector<const Micromega*>&,TVector3&,TVector3&);
  vector<const Micromega*> GetUpstreamMM() const{return upstream;}
  vector<const Micromega*> GetDownstreamMM() const{return downstream;}
};

//////IMPLEMENTATION OF THE FUNCTION //////////////

MManager::MManager():
fitter(0),track(0),pdg(11),detectorResolution(0.1), display(0)
{}

//ADDITION WITH STANDARD CONDITION FOR MM////
void MManager::AddEvent(const RecoEvent& e)
{
 Reset();
 AddMicromega(e.MM1,ALL);
 AddMicromega(e.MM2,ALL);
 AddMicromega(e.MM3,ALL);
 AddMicromega(e.MM4,ALL);
 //NO BEAMSPOT REQUIRED DOWNSTREAM
 AddMicromega(e.MM5,SINGLECLUS);
 AddMicromega(e.MM6,SINGLECLUS);
 AddMicromega(e.MM7,SINGLECLUS);
 AddMicromega(e.MM8,SINGLECLUS);
}

//ADDITION WITH CUSTOMIZED CONDITION///
void MManager::AddEvent(const RecoEvent& e,const MMcond MMcondition)
{
 Reset();
 AddMicromega(e.MM1,MMcondition);
 AddMicromega(e.MM2,MMcondition);
 AddMicromega(e.MM3,MMcondition);
 AddMicromega(e.MM4,MMcondition);
 //NO BEAMSPOT REQUIRED DOWNSTREAM
 AddMicromega(e.MM5,MMcondition);
 AddMicromega(e.MM6,MMcondition);
 AddMicromega(e.MM7,MMcondition);
 AddMicromega(e.MM8,MMcondition);
}

void MManager::AddMicromega(const Micromega& mm,const MMcond MMcondition = NOCUT)
{
 //if Micromega has not hit no use to add it to the manager
 if(!mm.hasHit())
  return;
 //check if hit is inside the beam spot
 const bool InsideSpot = fabs(mm.abspos().X()) < Beamtol*MMXsigma && fabs(mm.abspos().Y()) < Beamtol*MMYsigma;
 const bool singleClus = mm.xplane.clusters.size() == 1 && mm.yplane.clusters.size() == 1;

 if(MMcondition == SINGLECLUS){if(!singleClus) return;}
 else if(MMcondition == BEAMSPOTCUT){if(!singleClus) return;}
 else if(MMcondition == ALL){if(!singleClus || !InsideSpot) return;}
 else if(MMcondition == NOCUT){}
 else {cout << "not valid condition. MM not added to the manager" << endl;return;}
 //declare mm pointer
 const Micromega* mmp = &mm;
 //decide if Micromega is upstream or downstream
 const double ECAL_d = fabs(mm.geom->pos.Z() -ECAL_pos.pos.Z());
 const double MAGU_d = fabs(mm.geom->pos.Z() -MAGU_pos.pos.Z());
 if(ECAL_d <= MAGU_d)
  {
   //MM is near ECAL, it must be downstream
   downstream.push_back(mmp);
  }
 else
  {
   //MM is near downstream magnet, it must be upstream
   upstream.push_back(mmp);
  }
}

bool MManager::UpStreamOK() const
{
 const unsigned int Nup = upstream.size();
 if(Nup < 2)     return false;
 else if (Nup > 2 ) return true;
 else
  {
   //check if MM have enough distance
   const Micromega* m1 = upstream.front();
   const Micromega* m2 = upstream.back();
   //can reconstruct only if enough distance is given
   return fabs(m1->geom->pos.Z() - m2->geom->pos.Z()) > MM_minimum_distance;
  }

}

bool MManager::DownStreamOK() const
{

 const unsigned int Nup = downstream.size();
 if(Nup < 1)     return false; //TODO: in theory is possible with just one
 else return true;
}

bool MManager::CanTrack() const
{
 //if(!UpStreamOK())cout << "not enough hit upstream" << endl;
 //if(!DownStreamOK())cout << "not enough hit downstream" << endl;
 return UpStreamOK() && DownStreamOK();
}

void MManager::CollectPointsSimple(const vector<const Micromega*>& vec,TVector3& mmup,TVector3& mmdown)
{
 //collect points
 double max(0),min(99999);
 for(auto it = vec.begin();it!=vec.end();++it)
  {
   const Micromega* mm = *it;
   const double dist = fabs(mm->geom->pos.Z() - ECAL_pos.pos.Z());
   if(dist > max )
    {
     max = dist;
     mmup = mm->abspos();
    }
   if(dist < min )
    {
     min = dist;
     mmdown = mm->abspos();
    }
  }
  
}

SimpleTrack MManager::SimpleTrackingMM()
{

 SimpleTrack st;

 if(!CanTrack())
  return st;

 ///ELSE:simple tracking algorithm for the MM

 //collect best MM for tracking
 CollectPointsSimple(upstream,mm1,mm2);
 CollectPointsSimple(downstream,mm3,mm4);

 st.in.set(mm1, mm2);
 st.out.set(mm3, mm4);

 const double mag_len = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
 st.momentum = simple_tracking(mm1, mm2, mm3, mm4, mag_len, MAG_field);

 return st;
}

SimpleTrack MManager::PointDeflectionTracking()
{
 //define track
 SimpleTrack st;

 if(!CanTrack())
  return st;

 //use all point downstream and upstream to have an estimate on momentum reconstruction
 vector<double> momentums;
 for(auto pu1 = upstream.begin();pu1 != upstream.end(); ++pu1)
  for(auto pu2 = pu1+1;pu2 != upstream.end(); ++pu2)
   for(auto pd = downstream.begin();pd != downstream.end(); ++pd)
    {
     momentums.push_back(simple_tracking3((*pu1)->abspos(),(*pu2)->abspos(),(*pd)->abspos(),MAGU_pos.pos.Z(),MAGD_pos.pos.Z(),MAG_field));
    }
 //calculate the mean of momentum measured
 double meanmom = std::accumulate(momentums.begin(),momentums.end(),0.0) / momentums.size();
 //remove outlier under assumption on momentum spread
 auto it = std::remove_if(momentums.begin(),momentums.end(),[meanmom](double mom){return (mom-meanmom) > 3*EnergyRes;});
 momentums.erase(it,momentums.end());
 //if all momentum are spread, select the mean
 if(momentums.size() == 0) st.momentum = meanmom;
 else  st.momentum = std::accumulate(momentums.begin(),momentums.end(),0.0) / momentums.size();

 //complete the rest of the track as usual
 //TODO: exclude micromea with bad momentum reco from angle computation
 CollectPointsSimple(upstream,mm1,mm2);
 CollectPointsSimple(downstream,mm3,mm4);

 st.in.set(mm1, mm2);
 st.out.set(mm3, mm4);

 return st;
}

double MManager::GetAngleSimple()
{
 const SimpleTrack t = SimpleTrackingMM();
 const double mrad = 1e-3; // radians to milli-radians conversion
 return t.in.theta()/mrad;
}


void MManager::PrintMM(unsigned int verbose = 3)
{
 if(CanTrack() && verbose > 1)
  cout << "THIS EVENT TRACKING IS POSSIBLE" << endl;
 else if(verbose > 1)
  cout << "THIS EVENT TRACKING IS NOT POSSIBLE" << endl;

 cout << "MM with hits upstream " << endl;
 for(auto it = upstream.begin();it!=upstream.end();++it)
  {
   const Micromega* mm = *it;
   cout << mm->calib->name << endl;
  }

 cout << "MM with hits downstream " << endl;
 for(auto it = downstream.begin();it!=downstream.end();++it)
  {
   const Micromega* mm = *it;
   cout << mm->calib->name << endl;
  }
}


void MManager::Reset()
{
 upstream.clear();
 downstream.clear();
 delete track;
 track = 0;
}


////genfit
void MManager::GeometryInit(const TString GeoFile, const unsigned int verbose = 1, const int newpdg = 11 /* for electrons */, const double MMres = 0.1)
{
 //set basic variable
 pdg = newpdg;
 detectorResolution = MMres; //first guess resolution for MM

 TGeoManager::Import(GeoFile);
 TGeoManager::SetVerboseLevel(0);

 genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface);
 //init fitter
 fitter = new genfit::KalmanFitterRefTrack();
 fitter->setMinIterations(MinIter);

 if(verbose > 1)
  {
   fitter->setMaxIterations(MaxIter);
   cout << "Fitter:"
        << " name = " << fitter->IsA()->GetName()
        << " min iterations = " << fitter->getMinIterations()
        << " max iterations = " << fitter->getMaxIterations()
        << endl;
  }
}

void MManager::MagnetInit()
{
 //initialize field, units are kGauss
 genfit::ConstFieldBox* field = new genfit::ConstFieldBox(0, 10*MAG_field, 0, -100, 100, -100, 100, mm2cm*min(MAGU_pos.pos.Z(), MAGD_pos.pos.Z()), mm2cm*max(MAGU_pos.pos.Z(), MAGD_pos.pos.Z()));
 genfit::FieldManager::getInstance()->init(field);
}

void MManager::GenfitInit(const TString geofile)
{
 MagnetInit();

 // TODO: load proper geometry depending on the run number
 GeometryInit(geofile);
}

void MManager::GenfitTrack()
{

 if(!CanTrack())
  return;

 typedef genfit::MeasurementProducer<genfit::mySpacepointDetectorHit, genfit::mySpacepointMeasurement> MyMeasurementProducer;
 TClonesArray detHits ("genfit::mySpacepointDetectorHit");
 genfit::MeasurementFactory<genfit::AbsMeasurement> factory;
 factory.addProducer(myDetId, new MyMeasurementProducer(&detHits));

 // setup detector hits
 TMatrixDSym cov(3);
 for (int i = 0; i < 3; ++i) cov(i,i) = detectorResolution*detectorResolution;

 mm1 = upstream.front()->abspos();
 mm2 = upstream.back()->abspos();

 detHits.Clear("C");
 size_t nMeasurements = 0;
 for(auto it = upstream.begin();it!=upstream.end();++it)
  {
   const Micromega* mm = *it;
   const TVector3 pos = mm->abspos();
   new(detHits[nMeasurements]) genfit::mySpacepointDetectorHit(mm2cm * pos, cov);
   ++nMeasurements;
  }
 for(auto it = downstream.begin();it!=downstream.end();++it)
  {
   const Micromega* mm = *it;
   const TVector3 pos = mm->abspos();
   new(detHits[nMeasurements]) genfit::mySpacepointDetectorHit(mm2cm * pos, cov);
   ++nMeasurements;
  }


 // setup track seed state
 TVector3 momM = 10*(mm2-mm1).Unit(); // vector parallel to line with 10 GeV/c momentum
 TMatrixDSym covM(6);
 for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution*detectorResolution;
 for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / nMeasurements / sqrt(3), 2);

 // setup candidate (hits + seed)
 genfit::TrackCand cand;
 for(size_t i = 0; i < nMeasurements; ++i)
  cand.addHit(myDetId, i);
 cand.setPosMomSeedAndPdgCode(mm2cm * mm1, momM, pdg);
 cand.setCovSeed(covM);


 try {
  // create track
  genfit::AbsTrackRep* rep = new genfit::RKTrackRep(pdg);

  track = new genfit::Track (cand, factory, rep);

  fitter->processTrack(track);

  track->checkConsistency();

  if (display) display->addEvent(track);
 }
 catch (genfit::Exception& e) {
  std::cerr << e.what();
  std::cerr << "Exception, next track" << std::endl;
  return;
 }
}


double MManager::GetGenfitmom() const
{
 track->checkConsistency();
 const genfit::StateOnPlane state = track->getFittedState();
 return track->getCardinalRep()->getMomMag(state);
}

double MManager::GetGenfitChi2() const
{
 track->checkConsistency();
 const genfit::FitStatus* fit = track->getFitStatus();
 return fit->getChi2() /  fit->getNdf();

}
