#pragma once

//std function
#include<cmath>
#include<map>

//root function
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TList.h"
#include "TParameter.h"
#include "TVector3.h"
#include "TFile.h"
#include "TString.h"
#include "TSystemDirectory.h"

//include tracking.h to extrapolate position
#include "tracking.h"
#include "conddb.h"
#include "calocoord.h"
#include "timecut.h"
#ifdef TRACKINGTOOLS
#include "tracking_new.h"
#endif

//small utility function to take parameters from a root file

template<class type>
type ReadParameterFromList(TList* list, const char* parname)
{
  if(!list) {
    throw std::runtime_error("shower.h ReadParameterFromList() ERROR: can't read list");
  }
  //read parameter
  TParameter<type>* par = (TParameter<type>*)list->FindObject(parname);
  if(!par) {
    throw std::runtime_error(Form("shower.h ReadParameterFromList() ERROR: can't read parameter %s from list", parname));
  }
  
  return par->GetVal();
}

enum Chi2Method {
  kOrig=0,
  kCorrSigma=1,
  kSqrtE=2,
  kEneDependency=3
};

//root object where to store the profiles
TProfile2D* shower_profiles[6][6];
TProfile2D *shower_profiles_full[6][6];
TProfile3D *shower_profiles_vsEnergy[6][6];

bool data_loaded = false;
bool data_available_vsEnergy = true;
bool data_available_Tor = true;

//shower method
// note: values 0-3 explicilty defined for backward compatibility
enum SPmethod {SP_MM34=0, SP_MC=1, SP_MM57=2, SP_ECAL=3, SP_MM56=4, SP_MM36=5, SP_TrackExtrap=6,SP_TrackerAuto=7, SP_GENFIT=8};
//PARAMETERS
//central cells in the database
int pcenterX;
int pcenterY;
//maximum cell covered in the database
int pMaxCellX;
int pMaxCellY;
//norm used as default in the database
double pnorm;
double pNominalBeamEnergy;
double pECAL0BEAMSPOT_posX;
double pECAL0BEAMSPOT_posY;

// load shower profile from a .root file
void Load_shower_data(const char* fname)
{
  const string fpath = conddbpath() + fname;
  std::cout << "INFO: Load_shower_data(): loading shower profile from file " << fpath << std::endl;
  
  TFile input(fpath.c_str());
  
  if(!input.IsOpen()) {
    throw std::runtime_error("Error to open file");
  }

  data_loaded = false;

  // shower metadata
  TList* parlist = (TList*)input.Get("parameters");
  pcenterX           = ReadParameterFromList<int>(parlist, "pcenterX");
  pcenterY           = ReadParameterFromList<int>(parlist, "pcenterY");
  pMaxCellX          = ReadParameterFromList<int>(parlist, "dimX");
  pMaxCellY          = ReadParameterFromList<int>(parlist, "dimY");
  pnorm              = ReadParameterFromList<double>(parlist, "norm");
  pNominalBeamEnergy = ReadParameterFromList<double>(parlist, "NominalBeamEnergy");
  if (parlist->FindObject("ECAL0BEAMSPOT_posX")){
  pECAL0BEAMSPOT_posX =ReadParameterFromList<double>(parlist,"ECAL0BEAMSPOT_posX");
  pECAL0BEAMSPOT_posY =ReadParameterFromList<double>(parlist,"ECAL0BEAMSPOT_posY");
  }else{
    pECAL0BEAMSPOT_posX =  ECAL0BEAMSPOT_pos.X(); 
    pECAL0BEAMSPOT_posY = ECAL0BEAMSPOT_pos.Y(); 
  }
  for(int x=0;x < 6;++x)
    {
      for(int y=0;y < 6;++y)
        {
          TString name;
          name.Form("ECAL1-%i-%i",x,y);
          TProfile2D* p = (TProfile2D*) input.GetObjectChecked(name, "TProfile2D");
          
          if (!p) {
            std::cout << "ERROR: failed to load shower database, can't read profile " << name << endl;
            return;
          }
          
          p->SetDirectory(0);
          p->SetErrorOption("s"); // force to take standard deviation as error
          shower_profiles[x][y] = p;


          //L.M. try to load the 2D shower as defined in A.Toropin's analysis
          if (data_available_Tor) {
            name.Form("ECAL1-%i-%i-full", x, y);
            TProfile2D *pp = (TProfile2D*) input.GetObjectChecked(name, "TProfile2D");
            if (!pp) {
              //std::cout << "INFO: the shower profile as from A.T. analysis is not available, disable this option " << name << endl;
              data_available_Tor = false;
            } else {
              pp->SetDirectory(0);
              pp->SetErrorOption("s"); // force to take standard deviation as error
              shower_profiles_full[x][y] = pp;
            }
          }

          //A.C. also try to load the shower "vs energy".
          if (data_available_vsEnergy) {
            name.Form("ECAL1-%i-%i-vsEnergy", x, y);
            TProfile3D *p3D = (TProfile3D*) input.GetObjectChecked(name, "TProfile3D");
            if (!p3D) {
              //std::cout << "INFO: the shower vs energy is not available, disable this option" << name << endl;
              data_available_vsEnergy = false;
            } else {
              p3D->SetDirectory(0);
              p3D->SetErrorOption("s"); // force to take standard deviation as error
              shower_profiles_vsEnergy[x][y] = p3D;
            }
          }
        }
    }
  data_loaded = true;
  
  // print summary
  cout << "INFO: Load_shower_data(): metadata\n"
       << "      pcenterX=" << pcenterX << " pcenterY=" << pcenterY << "\n"
       << "      dimX=" << pMaxCellX << " dimY=" << pMaxCellY << "\n"
       << "      norm=" << pnorm << " NominalBeamEnergy=" << pNominalBeamEnergy << "\n"
       << "      ECAL0BEAMSPOT_posX=" <<pECAL0BEAMSPOT_posX << " ECAL0BEAMSPOT_posY=" <<pECAL0BEAMSPOT_posY << "\n" 
       << "      data_loaded=" << data_loaded
       << " data_available_Tor=" << data_available_Tor
       << " data_available_vsEnergy=" << data_available_vsEnergy
       << endl;
  
  // sanity check
  if ((pMaxCellX > 6) || (pMaxCellY > 6)) {
    cout << "ERROR: the profile matrix size is too big: (dimX x dimY) > 6x6" << endl;
    throw std::runtime_error("ERROR: can't load the profile");
  }
}


//dirIn is relative to conddbpath()
void scan_dir(std::map<int, std::string> &list, const string dirIn, const string format, const int firstRun=-1) {

  const TString dirname = conddbpath() + "/" + dirIn;

  //get list of file from directory
  TSystemDirectory dir(dirname, dirname);
  TList *filelist = dir.GetListOfFiles();
  if (!filelist) {
    std::cout << "ERROR: scan_dir() can't list directory " << dirname << std::endl;
    throw std::runtime_error("ERROR: Failed to open directory");
  }

  //loop over file in the directory
  int runN = 0;
  int minRunN = 999999;
  TSystemFile *thisfile;
  TIter itr((TCollection*) filelist);
  while ((thisfile = (TSystemFile*) itr())) {
    const TString filename = thisfile->GetName();
    if (sscanf(filename.Data(), format.c_str(), &runN) < 1)
      continue; // skip stray file, but better CRASH
    list[runN] = Form("%s/%s", dirIn.c_str(), filename.Data());
    if (runN < minRunN)
      minRunN = runN;
  }

  // sanity check: new entries were added to the list
  if (minRunN == 999999) {
    throw std::runtime_error("ERROR: scan_dir() no files in the input directory");
  }

  // sanity check: the firstRun <= minRunN
  if (firstRun > minRunN) {
    std::cout << "ERROR: scan_dir() firstRun=" << firstRun << " minRunN=" << minRunN << std::endl;
    throw std::runtime_error("ERROR: scan_dir() detected inconsistency");
  }

  //Fix for the first runs of the period dirIn refers to.
  //Associate to them the profile with the lowest run number among those in the folder.
  //Do so only if there is at least one file in the map and if this feature was requested
  if ((list.size() > 0)&&(firstRun!=-1)) {
    list[firstRun] = list[minRunN];
  }
}

//A.C. This code is called only once per run
std::string run_to_shower_name(const RecoEvent &e) {

  if (e.mc) {
    const int setupNumber = mcrunoption<int>("SetupNumber", 0);
    const int geometryVersion = mcrunoption<int>("General:GeometryVersion", 0);
    const bool is2022B = (setupNumber == 1 || setupNumber == 2) && geometryVersion == 2;
    
    std::cout << "INFO: run_to_shower_name() setup-dependent MC shower: setupNumber=" << setupNumber << " geoVersion=" << geometryVersion << " is2022B=" << is2022B << std::endl;
    
    // TODO: always use only one shower for any MC?

    if (is2022B) {
      // https://gitlab.cern.ch/P348/p348-daq/-/issues/52
      //return "shower-2022B-MC-v1.root"; // This profile was made with wrong beamspot_X = -355.
      return "shower-2022B-MC-v2.root";   // This profile was made with wide beam MC calib file /afs/cern.ch/work/m/mkirsano/public/simulation_data/ecalcalib/CaloHits_2022e_widebeam.d
      //return "shower55-MC-v1.root";     // This profile is for the function calcShowerChi2_55
    }
    else {
      return "shower-2016B-MC-v3.root";
    }
  }

  //list of shower profile files:
  //each key is a runNumber, each value is the name of a showerProfile file relative to the conddb() path
  //each profile should be associated from the runNumber corresponding to the key to the runNumber corresponding to the next key (the last item is associated to all those following)
  std::map<int, std::string> list;

  // the default value for all runs from run 0 and up to next loaded with scan_dir
  list[0] = "shower-2016B-v4.root";

  //load 2022B data
  const bool is2022B = (6035 <= e.run && e.run <= 8458);
  if (is2022B) scan_dir(list, "shower-2022B-v1", "profile_%06d.root", 6035);

  //load 2023A data
  const bool is2023A = (8459 <= e.run && e.run <= 9717);
  if (is2023A) scan_dir(list, "shower-2023A-v1", "profile_%06d.root", 8727);
  
  // sanity check - the list should not be empty
  if (list.empty()) {
    throw std::runtime_error("ERROR: run_to_shower_name() the list is empty");
  }

  /*
  cout << "INFO: run_to_shower_name() shower db list" << endl;
  for (auto i : list)
    cout << "  run=" << i.first << " file=" << i.second << endl;
  */

  //c++ map are ordered. Be "n" and "n+1" one element and the subsequent one,
  //with run_n and run_(n+1) the corresponding keys (run).
  //I need the element that has run_n <= run < run_(n+1). If the element is before the first one, return the first element in map
  //if the e.run is >= than the last key, use the last value

  auto it = list.upper_bound(e.run); //return an iterator pointing to the first element that is greater (strictly >) than the key.
  if (it != list.begin()) it--; //get the previous one, if available
  return it->second;
}


void DumpProfile(TProfile2D* Shower_Profiles[6][6])
{
  int binid_c = Shower_Profiles[0][0]->FindBin(0., 0.);
  std::cout << std::endl;
  std::cout << "ECAL matrix 5x5, view from behind (from HCAL):" << std::endl;
  std::cout << std::endl;
  for (int y=4; y >= 0; y--) {
    for (int x=0; x < 5; x++) {
      std::cout << Shower_Profiles[x][y]->GetBinContent(binid_c);
      if (x < 4) std::cout << " || ";
    }
    std::cout << std::endl;;
  }
  std::cout << std::endl;
}


int CheckProfileStatistics(TProfile2D* Shower_Profiles[6][6], double RCentralPart = 18.)
{
  int NPoorStat = 0;
  int NPoorStat1 = 0;
  std::cout << std::endl;
  for (int ix=1; ix <= Shower_Profiles[0][0]->GetXaxis()->GetNbins(); ix++) {
    for (int iy=1; iy <= Shower_Profiles[0][0]->GetYaxis()->GetNbins(); iy++) {
      double xbin = Shower_Profiles[0][0]->GetXaxis()->GetBinCenter(ix);
      double ybin = Shower_Profiles[0][0]->GetYaxis()->GetBinCenter(iy);
      int binid = Shower_Profiles[0][0]->GetBin(ix, iy);
      if(Shower_Profiles[0][0]->GetBinEntries(binid) < 3) {
        NPoorStat++;
        if(sqrt(xbin*xbin+ybin*ybin) < RCentralPart) NPoorStat1++;
      }
    }
  }
  std::cout << "Nbins with poor statistics = " << NPoorStat << ", in central part R < " << RCentralPart << " :  " << NPoorStat1 << std::endl;
  return NPoorStat1;
}


TVector3 GetProfileCenter(TProfile2D* Shower_Profiles[6][6], int MatrixSizeIn = 5)
{
  if(MatrixSizeIn%2 == 0) {
    std::cout << "Matrix size must be odd, MatrixSize = " << MatrixSizeIn << std::endl;
    exit(1);
  }
  int binid_c = Shower_Profiles[0][0]->FindBin(0., 0.);
  double ecal = 0.;
  double ecalx = 0.;
  double ecaly = 0.;
  for (int x = 0; x < MatrixSizeIn; ++x) {
    for (int y = 0; y < MatrixSizeIn; ++y) {
      const double E = Shower_Profiles[x][y]->GetBinContent(binid_c);
      ecal += E;
      ecalx += E * x;
      ecaly += E * y;
    }
  }
  ecalx /= ecal;
  ecaly /= ecal;
  // translate to center origin
  int MatrixCenter = (MatrixSizeIn - 1)/2;
  ecalx -= MatrixCenter;
  ecaly -= MatrixCenter;
  // scale to ECAL cell size 38.2 mm
  ecalx *= 38.2;
  ecaly *= 38.2;
  return TVector3(CenterGravityCorrect(ecalx), CenterGravityCorrect(ecaly), 0.);
}


// compute shower chi2 vs. reference shower shape
// input parameters:
//   centerx, centery, centersize: specify ECAL1 zone (range of cells) for chi2 calculation
//   spmethod: method to compute shower entry point on ECAL surface (0: by MM3->4 line, 1: by MC truth, 2: by MM5->7 line,
//                                                                   3: by ECAL center of mass of the calculation zone, 4: by MM5->6 line, 5: by MM3->6 line)
//   normalize: return chi2 divided by number of cells in zone
//   chi2_method: which method to use to calculate shower chi2:
//   0 -> original
//   1 -> sigma correction vs sqrt(E)
//   2 -> "a-la-Toropin"
//   3 -> 3D profile
// TODO: make use of track.out segment for spmethod=0 and spmethod=2
double calcShowerChi2(const RecoEvent& e,
                      const int centerx, const int centery, const int centersize = 3,
                      const SPmethod spmethod = SP_MM34,
                      const bool normalize = true,
                      const Chi2Method chi2_method = kOrig,
                      const TVector3 hitpoint = TVector3(-9999.,-9999.,0.))
{
  // auto-load shower profile
  if (!data_loaded) {
      Load_shower_data(run_to_shower_name(e).c_str());  
  }
  
  //check if data are loaded
  if (!data_loaded) {
    cout << "ERROR: shower database is not loaded, impossible to compute the chi-square" << endl;
    return 99990;
  }
  
  if ((chi2_method == 2) && !data_available_Tor) {
    cout << "ERROR: the option to compute the chi2 with the A.Toropin definition has been asked but profiles are not available" << endl;
    return 99991;
  }
  
  if ((chi2_method == 3) && !data_available_vsEnergy) {
    cout << "ERROR: the option to compute vs energy was asked but no profiles 3D are available" << endl;
    return 99992;
  }
  
  if (pNominalBeamEnergy != NominalBeamEnergy) {
    cout << "ERROR: Nominal beam energy from shower profile file doesn't match nominal value from data" << endl;
  }
  
  if(pECAL0BEAMSPOT_posX != ECAL0BEAMSPOT_pos.X() || pECAL0BEAMSPOT_posY != ECAL0BEAMSPOT_pos.Y()){
    cout << "ERROR: Nominal ECAL0BEAMSPOT_pos loaded from shower profile doesn't mach value in conddb.h" << endl;
  }

  //use total energy deposited in the ecal as normalization factor as it was done for the shower profile
  const double ecal1 = e.ecalTotalEnergy(1);
  
  //if Shower is less than 1 GeV avoid computation
  if (ecal1 < 1.) {
    return 99993;
  }
 
  TVector3 showerpos;
  
  // Use information from MM3 and MM4 to extrapolate initial shower position
  if (spmethod == SP_MM34) {
    // extrapolate MM3->4 line on ECAL surface
    const TVector3 p1 = e.MM3.abspos();
    const TVector3 p2 = e.MM4.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  }
  // Use information from MC truth to extrapolate initial shower position
  else if (spmethod == SP_MC) {
    if (!e.mc) {
      cout << "ERROR: MC truth information not avaiable, impossible to compute the chi-square" << endl;
      return 99994;
    }
    const TVector3 P_ecal(e.mc->ECALEntryX, e.mc->ECALEntryY, 0.);
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_MM57) {
    // extrapolate MM5->7 line on ECAL surface
    const TVector3 p1 = e.MM5.abspos();
    const TVector3 p2 = e.MM7.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_MM56) {
    // extrapolate MM5->6 line on ECAL surface
    const TVector3 p1 = e.MM5.abspos();
    const TVector3 p2 = e.MM6.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_MM36) {
    // extrapolate MM3->6 line on ECAL surface
    const TVector3 p1 = e.MM3.abspos();
    const TVector3 p2 = e.MM6.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_ECAL) {
    showerpos = ecalHitCoord(e, centerx, centery, centersize);
  }
 // Use track fit extrapolation on ECAL surface
  else if (spmethod == SP_TrackExtrap) {
    if(hitpoint.X() <= -9000.) { // Extrapolation is not provided
      cout << "Profile ERROR: track fit extrapolation method chosen, but extrapolation is not provided, is track reco on?" << endl;
      return 99996;
    }
    showerpos = hitpoint - ECAL0BEAMSPOT_pos;
  }
  // Use tracker information auto mode: track fit extrapolation if available, otherwise MM34 method
  else if (spmethod == SP_TrackerAuto) {
    if(hitpoint.X() > -9000.) { // Track extrapolation is provided, use it
      showerpos = hitpoint - ECAL0BEAMSPOT_pos;
    } else { // Use MM34 method
      // extrapolate MM3->4 line on ECAL surface
      const TVector3 p1 = e.MM3.abspos();
      const TVector3 p2 = e.MM4.abspos();
      const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
      showerpos = P_ecal - ECAL0BEAMSPOT_pos;
    }
  }else if(spmethod == SP_GENFIT){
#ifdef TRACKINGTOOLS
    TVector3 P_ecal(99999.,99999.,0.);
    // track reconstruction with Tracking Tools
    const std::vector<Track_t> upstream = GetReconstructedTracks(e,kRegion::upstream);
    if(upstream.size()>0){
      //take the first track (LARGER P_VALUE)
      P_ecal = upstream[0].getECALHit();
    }
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
#else
    throw std::runtime_error("ERROR: SP_GENFIT method required but no TRACKINGTOOLS defined!");  
#endif
  }  
  // Incorrect method value
  else {
    throw std::runtime_error("ERROR: incorrect method for initial shower position extrapolation");
 }

  // extract the bin ID corresponding to the track entry point
  int binid = shower_profiles[0][0]->FindBin(showerpos.X(), showerpos.Y());
  if (chi2_method == 3) {
    binid = shower_profiles_vsEnergy[0][0]->FindBin(showerpos.X(), showerpos.Y(), ecal1);
  }
  
  //Check number of cells from the calorimeter geometry
  const int NCellX = ECAL_pos.Nx;
  const int NCellY = ECAL_pos.Ny;
  
  //transformation of coordinates to match data with profile
  const int cx = pcenterX - centerx;
  const int cy = pcenterY - centery;
  
  // chi2 calculation
  double chi2 = 0;
  int counter = 0;
  
  for (int x = 0; x < NCellX; ++x){
    for (int y = 0; y < NCellY; ++y) {

      //compute actual cell to consider (shift relative to profile)
      const int rx = x + cx;
      const int ry = y + cy;

      // skip non-central cells
      const bool isOutsideField = abs(centerx - x) > centersize || abs(centery - y) > centersize;
      const bool isOutsideCal = x >= NCellX || y >= NCellY;
      const bool isOutsideProfile = rx >= pMaxCellX || ry >= pMaxCellY || rx < 0 || ry < 0;
      if (isOutsideField || isOutsideProfile || isOutsideCal)
        continue;

      double expected, measured, error;

      switch(chi2_method){
      case kOrig:
        expected = shower_profiles[rx][ry]->GetBinContent(binid);
        measured = pnorm * e.ECAL[1][x][y].energy / ecal1;
        error = shower_profiles[rx][ry]->GetBinError(binid);
        break;

      //A.C. correction for the energy dependence, see: https://gitlab.cern.ch/P348/p348-daq/-/issues/37
      //This assumes that the profile was computed with calib. data at "NominalBeamEnergy"
      case kCorrSigma:
        expected = shower_profiles[rx][ry]->GetBinContent(binid);
        measured = pnorm * e.ECAL[1][x][y].energy / ecal1;
        error = shower_profiles[rx][ry]->GetBinError(binid);
        error *= sqrt((0.95 * NominalBeamEnergy) / ecal1);
        break;

      //Version similar to A. Toropin analysis.
      case kSqrtE:
        //A. Toropin has a check, energy should be >.5 GeV, otherwise ignore the cell. 
        //I use a "continue" statement for this, since it is ignored by "switch" and taken by the for-loop
        expected = shower_profiles_full[rx][ry]->GetBinContent(binid) * ecal1 / 95.;
        measured = e.ECAL[1][x][y].energy;
        if (measured<0.5) continue;
        error = sqrt(measured);
        break;

      case kEneDependency:
        expected = shower_profiles_vsEnergy[rx][ry]->GetBinContent(binid);
        measured = pnorm * e.ECAL[1][x][y].energy / ecal1;
        error = shower_profiles_vsEnergy[rx][ry]->GetBinError(binid);
        break;

      default:
        throw std::runtime_error("shower.h calcChiSquare method not supported");
      }

      if (error < 1e-6) {
        //std::cerr << "WARNING: calcShowerChi2(): error is too low to resolve compatibility in cell:(" << x << "," << y << "), skipping \n";
        //cout << "Expected: " << expected << "   Measured:" << measured << " Error: " << error  << endl;
        //cout << "X: " << showerpos.X() << " Y: " << showerpos.Y() << endl;
        continue;
      }

      //add to the chi square.
      const double addchi2 = pow((measured - expected) / error, 2);
      chi2 += addchi2;
//      if(addchi2 > 10000.) std::cout << "big deviation: " << addchi2 << " " << x << " " << y << " " << expected << " " << measured << " " << error << " "
//                                     << " " << ecal1 << std::endl;

      ++counter;
    }
  }

  if (counter == 0) {
    //no valid cells found
    //std::cout << " counter = 0! " << ecal1 << " " << centerx << " " << centery << " " << binid << std::endl;
    return 99995;
  }
  
  return normalize ? chi2/counter : chi2;
}



// Alternative code from MK: fixed max profile size to 5x5, added calculation from track extrapolation etc.
//
// compute shower chi2 vs. reference shower shape
// input parameters:
//   centerx, centery, centersize: specify ECAL1 zone (range of cells) for chi2 calculation
//   spmethod: method to compute shower entry point on ECAL surface (0: by MM3->4 line or track extrap if available, 1: by MC truth, 2: by MM5->7 line,
//                                                                   3: by ECAL center of mass of the calculation zone)
//   normalize: return chi2 divided by number of cells in zone
//   chi2_method: which method to use to calculate shower chi2:
//   0 -> original
//   1 -> sigma correction vs sqrt(E)
//   2 -> "a-la-Toropin"
//   3 -> 3D profile
// TODO: make use of track.out segment for spmethod=0 and spmethod=2
double calcShowerChi2_55(const RecoEvent& e,
  const int centerx, const int centery, const int centersize = 3,
  const SPmethod spmethod = SP_MM34,
  const bool normalize = true,
  const Chi2Method chi2_method = kOrig,
  const TVector3 hitpoint = TVector3(-9999.,-9999.,0.))
{
  // auto-load shower profile
  if (!data_loaded) {
    if(e.mc) {
      Load_shower_data("shower55-MC-v1.root");
    } else {
      //Load_shower_data(run_to_shower_name(e).c_str());
      Load_shower_data("shower55-2022B.root");
    }
  }

  //check if data are loaded
  if (!data_loaded) {
    cout << "ERROR: shower database is not loaded, impossible to compute the chi-square" << endl;
    return 99990;
  }

  if ((chi2_method == 2) && !data_available_Tor) {
    cout << "ERROR: the option to compute the chi2 with the A.Toropin definition has been asked but profiles are not available" << endl;
    return 99991;
  }

  if ((chi2_method == 3) && !data_available_vsEnergy) {
    cout << "ERROR: the option to compute vs energy was asked but no profiles 3D are available" << endl;
    return 99992;
  }

  if (pNominalBeamEnergy != NominalBeamEnergy) {
    cout << "ERROR: Nominal beam energy from shower profile file doesn't match nominal value from data" << endl;
  }

  //use energy deposited in the 5x5 ecal1 as the normalization factor as it was done for the shower profile
  double ecal1 = 0.;
  for (int x = 0; x < 5; x++)
    for (int y = 0; y < 5; y++)
      ecal1 += e.ECAL[1][x+centerx-2][y+centery-2].energy;

  //if Shower is less than 1 GeV avoid computation
  if (ecal1 < 1.) {
    return 99993;
  }

  TVector3 showerpos;

  // Use information from MM3 and MM4 to extrapolate initial shower position
  if (spmethod == SP_MM34) {
    // extrapolate MM3->4 line on ECAL surface
    const TVector3 p1 = e.MM3.abspos();
    const TVector3 p2 = e.MM4.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  }
  // Use information from MC truth to extrapolate initial shower position
  else if (spmethod == SP_MC) {
    if (!e.mc) {
      cout << "ERROR: MC truth information not avaiable, impossible to compute the chi-square" << endl;
      return 99994;
    }
    const TVector3 P_ecal(e.mc->ECALEntryX, e.mc->ECALEntryY, 0.);
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_MM57) {
    // extrapolate MM5->7 line on ECAL surface
    const TVector3 p1 = e.MM5.abspos();
    const TVector3 p2 = e.MM7.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_MM56) {
    // extrapolate MM5->6 line on ECAL surface
    const TVector3 p1 = e.MM5.abspos();
    const TVector3 p2 = e.MM6.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_MM36) {
    // extrapolate MM3->6 line on ECAL surface
    const TVector3 p1 = e.MM3.abspos();
    const TVector3 p2 = e.MM6.abspos();
    const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
    showerpos = P_ecal - ECAL0BEAMSPOT_pos;
  } else if (spmethod == SP_ECAL) {
    showerpos = ecalHitCoord(e, centerx, centery, centersize);
  }
  // Use track fit extrapolation on ECAL surface
  else if (spmethod == SP_TrackExtrap) {
    if(hitpoint.X() <= -9000.) { // Extrapolation is not provided
      cout << "Profile ERROR: track fit extrapolation method chosen, but extrapolation is not provided, is track reco on?" << endl;
      return 99996;
    }
    showerpos = hitpoint - ECAL0BEAMSPOT_pos;
  }
  // Use tracker information auto mode: track fit extrapolation if available, otherwise MM34 method
  else if (spmethod == SP_TrackerAuto) {
    if(hitpoint.X() > -9000.) { // Track extrapolation is provided, use it
      showerpos = hitpoint - ECAL0BEAMSPOT_pos;
    } else { // Use MM34 method
      // extrapolate MM3->4 line on ECAL surface
      const TVector3 p1 = e.MM3.abspos();
      const TVector3 p2 = e.MM4.abspos();
      const TVector3 P_ecal = extrapolate_line(p1, p2, ECAL_pos.pos.Z());
      showerpos = P_ecal - ECAL0BEAMSPOT_pos;
    }
  }
  // Incorrect method value
  else {
    throw std::runtime_error("ERROR: incorrect method for initial shower position extrapolation");
  }

  // extract the bin ID corresponding to the track entry point
  int binid = shower_profiles[0][0]->FindBin(showerpos.X(), showerpos.Y());
  if (chi2_method == 3) {
    binid = shower_profiles_vsEnergy[0][0]->FindBin(showerpos.X(), showerpos.Y(), ecal1);
  }

  // chi2 calculation
  double chi2 = 0;
  int counter = 0;

  for (int x = 0; x < 5; ++x) {
    for (int y = 0; y < 5; ++y) {

      //compute actual cell to consider (shift relative to profile)
      int xoffset = centerx - 2;
      int yoffset = centery - 2;
      const int rx = x + xoffset;
      const int ry = y + yoffset;

      // skip non-central cells
      const bool isOutsideField = abs(x-2) > centersize || abs(y-2) > centersize;
      const bool isOutsideCal = rx >= ECAL_pos.Nx || ry >= ECAL_pos.Ny || rx < 0 || ry < 0;
      if (isOutsideField || isOutsideCal)
        continue;

      double expected, measured, error;

      switch(chi2_method){
      case kOrig:
        expected = shower_profiles[x][y]->GetBinContent(binid);
        measured = pnorm * e.ECAL[1][rx][ry].energy / ecal1;
        error = shower_profiles[x][y]->GetBinError(binid);
        break;

      //A.C. correction for the energy dependence, see: https://gitlab.cern.ch/P348/p348-daq/-/issues/37
      //This assumes that the profile was computed with calib. data at "NominalBeamEnergy"
      case kCorrSigma:
        expected = shower_profiles[x][y]->GetBinContent(binid);
        measured = pnorm * e.ECAL[1][rx][ry].energy / ecal1;
        error = shower_profiles[x][y]->GetBinError(binid);
        error *= sqrt((0.95 * NominalBeamEnergy) / ecal1);
        break;

      //Version similar to A. Toropin analysis.
      case kSqrtE:
        //A. Toropin has a check, energy should be >.5 GeV, otherwise ignore the cell.
        //I use a "continue" statement for this, since it is ignored by "switch" and taken by the for-loop
        expected = shower_profiles_full[x][y]->GetBinContent(binid) * ecal1 / 95.;
        measured = e.ECAL[1][rx][ry].energy;
        if (measured < 0.5) continue;
        error = sqrt(measured);
        break;

      case kEneDependency:
        expected = shower_profiles_vsEnergy[x][y]->GetBinContent(binid);
        measured = pnorm * e.ECAL[1][rx][ry].energy / ecal1;
        error = shower_profiles_vsEnergy[x][y]->GetBinError(binid);
        break;

      default:
        throw std::runtime_error("shower.h calcChiSquare method not supported");
      }

      if (error < 1e-6) {
        //std::cerr << "WARNING: calcShowerChi2(): error is too low to resolve compatibility in cell:(" << x << "," << y << "), skipping \n";
        //cout << "Expected: " << expected << "   Measured:" << measured << " Error: " << error  << endl;
        //cout << "X: " << showerpos.X() << " Y: " << showerpos.Y() << endl;
        continue;
      }

      //add to the chi square.
      chi2 += pow((measured - expected) / error, 2);

      double addchi2 = pow((measured - expected) / error, 2);
//      if(addchi2 > 10000.) std::cout << "big deviation: " << addchi2 << " " << x << " " << y << " " << expected << " " << measured << " " << error << " "
//                                     << " " << ecal1 << std::endl;

      ++counter;
    }
  }

  if (counter == 0) {
    //no valid cells found
    //std::cout << " counter = 0! " << ecal1 << " " << centerx << " " << centery << " " << binid << std::endl;
    return 99995;
  }
  
  return normalize ? chi2/counter : chi2;
}


double calcShowerChi2(const RecoEvent &e, const SPmethod spmethod = SP_MM34) {
  return calcShowerChi2(e, ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, 3, spmethod, true, kOrig);
}

double calcShowerChi2_sqrt(const RecoEvent &e, const SPmethod spmethod = SP_MM34) {
  return calcShowerChi2(e, ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, 3, spmethod, true, kCorrSigma);
}

double calcShowerChi2_Tor(const RecoEvent &e, const SPmethod spmethod = SP_MM34) {
  //A. Toropin is not normalizing the chi2 by NDF
  return calcShowerChi2(e, ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, 1, spmethod,false, kSqrtE);
}

double calcShowerChi2_vsEnergy(const RecoEvent &e, const SPmethod spmethod = SP_MM34) {
  return calcShowerChi2(e, ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, 3, spmethod, true, kEneDependency);
}

double calcShowerChi2_MC(const RecoEvent &e) {
  return calcShowerChi2(e, ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, 3, SP_MC, true, kCorrSigma);
}
