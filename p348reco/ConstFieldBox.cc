/* Copyright 2008-2010, Technische Universitaet Muenchen,
   Authors: Christian Hoeppner & Sebastian Neubert & Johannes Rauch

   This file is part of GENFIT.

   GENFIT is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GENFIT is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GENFIT.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ConstFieldBox.h"
#include <iostream>

namespace genfit {

TVector3 ConstFieldBox::get(const TVector3& pos) const {

  if(pos.X() >= _xmin && pos.X() <= _xmax
     && pos.Y() >= _ymin && pos.Y() <= _ymax
     && pos.Z() >= _zmin && pos.Z() <= _zmax){
    return field_;
  }else{
    return TVector3(0,0,0);
  }
  return field_;
  
}

void ConstFieldBox::get(const double& posX, const double& posY, const double& posZ, double& Bx, double& By, double& Bz) const {
  if(posX >= _xmin && posX <= _xmax
     && posY >= _ymin && posY <= _ymax
     && posZ >= _zmin && posZ <= _zmax){
    Bx = field_.X();
    By = field_.Y();
    Bz = field_.Z();
    // std::cout << posX << ", " << posY << ", " << posZ << std::endl;
    // std::cout << _zmin << ", " << _zmax << ": " << Bx << ", " << By << ", " << Bz << std::endl;

  }else{
    Bx = 0.;
    By = 0.;
    Bz = 0.;
  }
  
    
}

} /* End of namespace genfit */
