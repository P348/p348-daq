# ifndef H_GEMRECO_H
# define H_GEMRECO_H

# include <CsGEMPlane.h>
# include <CsGEMHit.h>
# include <CsGEMCluster.h>

/*
 * This is wrapper for CsGEM library
 */

namespace gem {

// Wrapper's member. It is not actually plane but cluster, we found it convenient
// to use e.GM01->xplane[0].position() in reconstruction. Btw, we
// keep pointer to original cluster.
// https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/geom/gem/CsGEMCluster.h
struct CsGEMClusterWrapper {
    const CsGEMCluster * orig;
    
    CsGEMClusterWrapper(const CsGEMCluster* gem = 0): orig(gem) {}

    operator bool() const { return (orig != nullptr); }
    
    double time() const {
        double time = 0, err = 0;
        orig->GetTime( time, err);
        return time;
    }
    double time_err() const {
        double time = 0, err = 0;
        orig->GetTime( time, err);
        return err;
    }
    float position() const {
        return orig->GetPosition();
    }
    float position_err() const {
        return orig->GetPositionErr();
    }

};

struct GemHitPoint {
    // X and Y clusters which build the 2D hit point
    CsGEMClusterWrapper x;
    CsGEMClusterWrapper y;
    const GEMGeometry* geom;
    
    // check both clusters are defined
    operator bool() const { return (x && y); }
    
    GemHitPoint(const CsGEMCluster * xclu = nullptr,
                const CsGEMCluster * yclu = nullptr,
                const GEMGeometry* g = nullptr)
    : x(xclu), y(yclu), geom(g) {}
    
    // Get method for relative (to detector) hit position
    TVector3 rel_pos() const;
    // Get absolute (geometrical) position
    TVector3 abs_pos() const;
};

// hit position in local coordinate system
TVector3 GemHitPoint::rel_pos() const {
    double xpos = x.position();
    double ypos = y.position();
    
    // translate into center of GEM surface
    const double center = geom->size/2.0 - 0.5;
    xpos -= center;
    ypos -= center;
    
    // scale
    xpos *= geom->pitch;
    ypos *= geom->pitch;
    
    // rotate
    TVector3 pos(xpos, ypos, 0);
    pos.RotateZ(geom->angle);
    
    return pos;
}

TVector3 GemHitPoint::abs_pos() const {
    return (rel_pos() + geom->pos);
}

// Simple GEM class. It's ordinary wrapper around CsGem library.
// Please, reffer to it for more information about available methods.
// Since, calibrations are not well defined part of the library there
// are not guarantees in proper perfoms of the methods assosiated with
// DB.
class GEM {
public :
    GEM(const CS::Chip::Maps&, const GEMGeometry&, const GEM_calib&);
    // To create full (almost) operating GEM user should create planes
    // and add necessary amount of channels. GEMMap contains information
    // about channels.
    // GEMs in NA64 have 2 planes // 24 Sep. 2017
    CsGEMPlane xPlane;
    CsGEMPlane yPlane;
    // To get best cluster info:
    // 1) hits should be clusterized
    // 2) GM1.bestHit.x;
    //    GM1.bestHit.y;
    GemHitPoint bestHit;
    // This vector provides access to the all clusters in each plane
    std::vector<CsGEMClusterWrapper> xplane, yplane;
    // wrapper method for GEM class. This method calls native clusterize
    // methods for each available plane and fills xplane and yplane vectors
    void clusterize();
    // method for debugging. We need to dump hits from time to time.
    void debug_print() const;
    // Clear planes and CsGEMClusterWrapper
    void clear();
    std::string getname() const { return _name; }
    const GEMGeometry* GetGemGeometry() const{return &_geometry;} 
protected :
    // create gem detector via provided map and calibration data
    void create_default_gem(const CS::Chip::Maps&, const GEM_calib&);
    // inner method to avoid dublication of the code. Actually the best
    // decision is overload CsGEMPlane and add print_plane_hits() to
    // the daughter class
    void print_plane_hits(const CsGEMPlane * plane) const;
    void find_best_hit();
    GEMGeometry _geometry;
    string _name;
};
// end class GEM

const int NGEM = 4;
GEM * rawgem[NGEM] = {nullptr, nullptr, nullptr, nullptr};

// Setup GEM geometry and channels mapping.
// To be called on every new run.
void Init_gem_reconstruction(const CS::DaqEventsManager& manager) {
    const CS::Chip::Maps & maps = manager.GetMaps();
    
    for (int i = 0; i < NGEM; ++i) delete rawgem[i];
    
    rawgem[0] = new GEM(maps, GM01Geometry, GM01_calib);
    rawgem[1] = new GEM(maps, GM02Geometry, GM02_calib);
    rawgem[2] = new GEM(maps, GM03Geometry, GM03_calib);
    rawgem[3] = new GEM(maps, GM04Geometry, GM04_calib);
}

// Cleanup GEM reco state before every new event
void Init_gem_event() {
    for (int i = 0; i < NGEM; ++i) rawgem[i]->clear();
}

void GEM::clear() {
    xPlane.Clear();
    yPlane.Clear();
    xplane.clear();
    yplane.clear();
    bestHit = GemHitPoint();
}

// assume the best hit candidate is defined by best clusters in X and Y planes
// this should give reasonable results for a single track events
void GEM::find_best_hit() {
    const std::list<CsGEMCluster*> & xClusters = xPlane.GetClusters();
    const CsGEMCluster* xbest = nullptr;
    float maxAmp = 0;
    for ( auto itClusterPtr = xClusters.begin(); itClusterPtr != xClusters.end(); ++itClusterPtr ) {
        const std::vector<float>& amps = (*itClusterPtr)->GetAmp();
        for ( auto itAmp = amps.begin(); itAmp != amps.end(); ++itAmp ) {
            if ( *itAmp > maxAmp ) {
                maxAmp = *itAmp;
                xbest = *itClusterPtr;
            }
        }
    }
    
    // Dublication of the code. TODO It's needed to be reworked.
    const std::list<CsGEMCluster*> & yClusters = yPlane.GetClusters();
    const CsGEMCluster* ybest = nullptr;
    maxAmp = 0;
    for ( auto itClusterPtr = yClusters.begin(); itClusterPtr != yClusters.end(); ++itClusterPtr ) {
        const std::vector<float>& amps = (*itClusterPtr)->GetAmp();
        for ( auto itAmp = amps.begin(); itAmp != amps.end(); ++itAmp ) {
            if ( *itAmp > maxAmp ) {
                maxAmp = *itAmp;
                ybest = *itClusterPtr;
            }
        }
    }
    
    bestHit = GemHitPoint(xbest, ybest, &_geometry);
}

// copy items from one container to another
template <typename SrcType, typename DstType>
void copy_items(const SrcType& src, DstType& dst) {
    dst.clear();
    for (auto i = src.begin(); i != src.end(); ++i)
      dst.push_back(*i);
}

void GEM::clusterize() {
    // Here native clusterize method from Cs library is called
    xPlane.Clusterize();
    // copy clusters pointers to vector of wrappers for usage convenience
    copy_items(xPlane.GetClusters(), xplane);
    
    // Same for Y plane
    yPlane.Clusterize();
    copy_items(yPlane.GetClusters(), yplane);
    
    // Maybe finding best cluster should be moved out of here.
    find_best_hit();
}

void GEM::print_plane_hits(const CsGEMPlane * plane) const {
    const std::list<CsGEMHit*> & hits = plane->GetHits();
    std::cout << "-------------------------------------------------------------" << std::endl;
    std::cout << "Hits in plane: " << plane->GetName() << std::endl;
    std::cout << "amp1 amp2 amp3 \t"
              << "XTalk \t" << "NClu \t" << std::endl;
    for ( auto hit = hits.begin(); hit != hits.end(); ++hit ) {
        const std::vector<float> & amp = (*hit)->GetAmp();
        std::cout << amp[0] << " " << amp[1] << " " << amp[2] << " "
                  << (*hit)->GetXTalk() << " \t"
                  << (*hit)->GetNrClusters() << " \t" << std::endl;
    }
}

void GEM::debug_print() const {
    print_plane_hits(&xPlane);
    print_plane_hits(&yPlane);
    std::cout << "===gem's best cluster info===" << std::endl
        << _name << " " << std::endl
        << "Wire: " << bestHit.x.position() << " " << bestHit.y.position() << std::endl
        << "Rel_pos: " << bestHit.rel_pos().X() << " " << bestHit.rel_pos().Y() << std::endl
        << "Time: " << bestHit.x.time() << " " << bestHit.y.time()
        << std::endl;
}

void GEM::create_default_gem(const CS::Chip::Maps & map_, const GEM_calib& calib) {
    // This code from Michael. It should be configurable.
    // Set plane parameters
    int do_clustering = 1;
    float fThresholdHit_ = 3.;        //! Threshold for hits in units of sigma
    float fThresholdClu_ = 5.;        //! Threshold for clusters in units of sigma
    xPlane.GetPar()->SetClusMeth(do_clustering);
    xPlane.GetPar()->SetThrClus(fThresholdClu_);
    xPlane.GetPar()->SetThrHit(fThresholdHit_);
    xPlane.GetPar()->SetSample(2); // sample used for clustering

    yPlane.GetPar()->SetClusMeth(do_clustering);
    yPlane.GetPar()->SetThrClus(fThresholdClu_);
    yPlane.GetPar()->SetThrHit(fThresholdHit_);
    yPlane.GetPar()->SetSample(2); // sample used for clustering

    // Create time calibration object
    CsGEMTimeCalOld* _cal0X = new CsGEMTimeCalOld(0.2,1.1,-40.,22.2,1.65);
    CsGEMTimeCalOld* _cal1X = new CsGEMTimeCalOld(0.3,1.0,0.,27.,1.23);
    // CsGEMPlane takes care to 'delete' allocated CsGEMTimeCalOld
    xPlane.SetTimeCals(CsGEMTimeCals(_cal0X, _cal1X));

    CsGEMTimeCalOld* _cal0Y = new CsGEMTimeCalOld(0.2,1.1,-40.,22.2,1.65);
    CsGEMTimeCalOld* _cal1Y = new CsGEMTimeCalOld(0.3,1.0,0.,27.,1.23);
    yPlane.SetTimeCals(CsGEMTimeCals(_cal0Y, _cal1Y));
    
    for( CS::Chip::Maps::const_iterator it=map_.begin(); it!=map_.end(); ++it ) {
        // skip mapping of other detectors
        const std::string& detname = it->second->GetDetID().GetName();
        if (detname.find(_name) != 0) continue;
        
        // check the mapping item corresponds to APV chip
        const CS::ChipAPV::Digit *d_apv=dynamic_cast<const CS::ChipAPV::Digit *>(it->second);
        if (!d_apv) {
            std::cerr << "ERROR: create_default_gem(): got non-APV mapping for detname=" << detname << std::endl;
            break;
        }
        
        // default calibration data
        // TODO: move to conddb.h
        double pedbase = 750.;
        double pedsigma = 5.;
        
        if (calib) {
          const APV_channel_id id = {d_apv->GetSourceID(),d_apv->GetAdcID(),d_apv->GetChip(),d_apv->GetChipChannel()};
          APV_channel_calib c = calib.FindGEMPedestal(id);
          pedbase = c.pedestalamp;
          pedsigma = c.pedestalsigma;
        }
        
        if (detname.find(_name+"X1__") == 0) {
            xPlane.AddChan(/*int detChan*/ d_apv->GetChannel(),
                           /*int hem*/     d_apv->GetChanPos(),
                           /*int flag*/    1,
                           /*float ped*/   pedbase,
                           /*float sigma*/ pedsigma,
                           /*int apvchip*/ d_apv->GetChip(),
                           /*int apvchan*/ d_apv->GetChipChannel() );
        }
        if (detname.find(_name+"Y1__") == 0) {
            yPlane.AddChan(/*int detChan*/ d_apv->GetChannel(),
                           /*int hem*/     d_apv->GetChanPos(),
                           /*int flag*/    1,
                           /*float ped*/   pedbase,
                           /*float sigma*/ pedsigma,
                           /*int apvchip*/ d_apv->GetChip(),
                           /*int apvchan*/ d_apv->GetChipChannel() );
        }
        
        /*
        std::cout << "INFO: create_default_gem(): new channel:"
                  << " detname=" << detname
                  << " channel=" << d_apv->GetChannel()
                  << " chip=" << d_apv->GetChip()
                  << " chipchannel=" << d_apv->GetChipChannel()
                  << " pedbase=" << pedbase
                  << " pedsigma=" << pedsigma
                  << endl;
        */
        
        // TODO: unknown name warning if no match?
    }
}

GEM::GEM(const CS::Chip::Maps & map_, const GEMGeometry& geom_, const GEM_calib& calib_) :
    xPlane("xPlane"), yPlane("yPlane"), _geometry(geom_), _name(calib_.name) {
    create_default_gem(map_, calib_);
    if ( !_geometry.isSet() ) {
        std::cout << "ERROR: GEM::GEM() name = " << _name << ", geometry is not set" << std::endl;
    }
}

 //FUNCTION TO COLLECT GEMS HIT FROM CORAL
 //USER FUNCTION TO ADAPT GEMS HITS

 std::vector<TVector3> GetHits(gem::GEM * GM)
 {
  //results
  std::vector<TVector3> GEMhits;
  const std::list<CsGEMCluster*> & xClusters = GM->xPlane.GetClusters();
  const std::list<CsGEMCluster*> & yClusters = GM->yPlane.GetClusters();

  //very rudimental, select for every hit
  for(auto itx = xClusters.begin(); itx != xClusters.end(); ++itx)
   for(auto ity = yClusters.begin(); ity != yClusters.end(); ++ity)
    {
     //get time of clusters
     double tx,txerr,ty,tyerr;
     (*itx)->GetTime(tx,txerr);(*ity)->GetTime(ty,tyerr);    
     //if clustersize less than 2 reject hit
     const unsigned int ClusterSizex = (*itx)->GetHits().size();
     const unsigned int ClusterSizey = (*ity)->GetHits().size();
     if((ClusterSizex < 2 || ClusterSizey < 2)) continue;
     //if((ClusterSizex < 3 || ClusterSizey < 3)) continue;
     //if distance between time stamp is bigger than 3sigma of the error reject hit
     if( fabs(tx-ty) > 3 * sqrt(txerr*txerr + tyerr*tyerr) ) continue;         
     //construct GEM hits
     gem::GemHitPoint thishit ((*itx), (*ity), GM->GetGemGeometry());
     //save all relevant quantity
     TVector3 thispos = thishit.abs_pos();
     GEMhits.push_back(thispos);
     //save hits
    }
  return GEMhits;
 } // end AddGemHits

 unsigned int CountGEMPlaneHits(const std::list<CsGEMCluster*> & Clusters)
 {
   unsigned int Nhits(0);
  //very rudimental, select for every hit
   for(auto it = Clusters.begin(); it != Clusters.end(); ++it)
     {
       //get time of clusters
       double tx,txerr;
       (*it)->GetTime(tx,txerr);
       //if clustersize less than 2 reject hit
       const unsigned int ClusterSize = (*it)->GetHits().size();
       if(ClusterSize < 2) continue;
       //Increment
       ++Nhits;
     }
   return Nhits;
 } //end CountGEMHits

 unsigned int CountGEMHitsX(gem::GEM * GM)
 {
   return CountGEMPlaneHits(GM->xPlane.GetClusters());
 }

 unsigned int CountGEMHitsY(gem::GEM * GM)
 {
   return CountGEMPlaneHits(GM->yPlane.GetClusters());
 }

 unsigned int CountGEMHits(gem::GEM * GM)
 {
   if(CountGEMHitsX(GM) != CountGEMHitsY(GM))
     {       
       //cout << "Xplane: " << CountGEMHitsX(GM) << endl;
       //cout << "Yplane: " << CountGEMHitsY(GM) << endl;
     }
   return floor(0.5*(CountGEMHitsX(GM) + CountGEMHitsY(GM)));
 }
  
}  // namespace gem
# endif  // H_GEMRECO_H
