// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "THStack.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./example_led.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  // stats bar format
  gStyle->SetOptStat("emruo");
  gStyle->SetOptFit(111);
  
  TFile* file = new TFile("example_led.root", "RECREATE");
  
  TH2D all("all", "Energy distribution;channel;energy", 1, 0, 1, 1500, 0, 300);
  all.SetOption("COL");
  all.SetStats(kFALSE);
  
  //enum {kAmplitude_No, kAmplitude_Manual, kAmplitude_LED};
  SADC_correction_method = kAmplitude_No;
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << nevt << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics) continue;
    
    // histogramming
    all.Fill("ECAL0", e.ecalTotalEnergy(0), 1.);
    all.Fill("ECAL1", e.ecalTotalEnergy(1), 1.);
    all.Fill("ECAL01", e.ecalTotalEnergy(), 1.);
    
    all.Fill("HCAL0", e.hcalTotalEnergy(0), 1.);
    all.Fill("HCAL1", e.hcalTotalEnergy(1), 1.);
    all.Fill("HCAL2", e.hcalTotalEnergy(2), 1.);
    all.Fill("HCAL3", e.hcalTotalEnergy(3), 1.);
    all.Fill("HCAL01", e.hcalTotalEnergy(0)+e.hcalTotalEnergy(1), 1.);
    all.Fill("HCAL012", e.hcalTotalEnergy(0)+e.hcalTotalEnergy(1)+e.hcalTotalEnergy(2), 1.);
    
    all.Fill("WCAL0", e.WCAL[0].energy, 1.);
    all.Fill("WCAL1", e.WCAL[1].energy, 1.);
    all.Fill("WCAL2", e.WCAL[2].energy/MeV, 1.);
    all.Fill("WCAL01", e.WCAL[0].energy+e.WCAL[1].energy, 1.);
  }
  
  all.LabelsDeflate("X");
  all.LabelsOption("v", "X");
  
  // generate projections
  for (int i = 1; i <= all.GetXaxis()->GetNbins(); ++i) {
    const TString name = all.GetXaxis()->GetBinLabel(i);
    //cout << name << endl;
    
    TH1D* pr = all.ProjectionY(name, i, i, "");
    pr->SetTitle(name);
  }
  
  // save all histograms to .root file
  file->Write();
  file->Close();
  
  return 0;
}
