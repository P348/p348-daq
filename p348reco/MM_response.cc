// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TString.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "TF1.h"
#include "TTree.h"
#include <vector>
#include <vector>
#include <TH1F.h>
#include <TH2F.h>
#include <TCanvas.h>


// c++
#include <iostream>
#include <string>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking_new.h"

struct TrackHisto
{
  std::string name;
  TH1D* mom;
  TH1D* chi2;
  TH1D* anglein;
  TH1D* angleout;
  TH1D* angleinout;

  void Init(std::string _name)
  {
    TString title(_name);

    mom = new TH1D(title+"_mom",
        "Momentum reconstructed for " + title,
        600.,-0.5,199.5
        );

    chi2 = new TH1D(title+"_chi2",
        "#chi^{2} of reconstructed momentum for " + title,
        600.,-0.5,199.5
        );

    anglein = new TH1D(title+"_angleZX",
        "Angle In for " + title,
        400,-0.5,99.5
        );

    angleout = new TH1D(title+"_angleout",
        "Angle Out for " + title,
        400,-0.5,99.5
        );

    angleinout = new TH1D(title+"_angleinout",
        "Angle In-Out for " + title,
        400,-0.5,99.5
        );

    std::cout << "Initialized TrackHisto with name: " << name << "\n";
  }

  void Fill(const std::vector<Track_t>& tracks)
  {
    if(tracks.empty())
      return;

    mom->Fill(tracks.at(0).momentum);
    mom->Fill(tracks.at(0).chisquare);
    // in mrad
    const double rad2mrad = 1000;
    anglein   ->Fill(rad2mrad * tracks.at(0).in.theta());
    angleout  ->Fill(rad2mrad * tracks.at(0).out.theta());
    angleinout->Fill(rad2mrad * tracks.at(0).in_out_angle());
  }

  void FitMom(const double r1 = 80., const double r2 = 120.)
  {
    //define double gaussian with shared mean
    TF1* dgaus = new TF1("dgaus", "[0]*TMath::Gaus(x, [1],[2]) + [3]*TMath::Gaus(x,[1],[4])",r1,r2);
    //define paramters
    dgaus->SetParameter(0,       mom->GetBinContent(mom->GetMaximumBin()));
    dgaus->SetParameter(1,       mom->GetBinCenter(mom->GetMaximumBin()));
    dgaus->SetParameter(2,       mom->GetStdDev());
    dgaus->SetParameter(3,       mom->GetBinContent(mom->GetMaximumBin()) / 4);
    dgaus->SetParameter(4,   4 * mom->GetStdDev());
    dgaus->SetParNames("Norm1","Momentum","MomRes","Norm2","MomResTail");
    mom->Fit("dgaus","QM+","",r1,r2);
    gStyle->SetOptFit();
  }

  TrackHisto()
    :
      name(""),
      mom(0),chi2(0),anglein(0),angleout(0),angleinout(0)
  {
    Init(name);
  }

  TrackHisto(std::string _name)
    :
      name(_name),
      mom(0),chi2(0),anglein(0),angleout(0),angleinout(0)
  {
    //initialize histogram
    Init(name);
  }

};

struct MM_histo_plane
{
  TH1D* clus_time;
  TH2D* strip_time;
  TH2D* time_charge;
  TH2D* strip_charge;
  TH2D* chan_charge;
  TH2D* clus_pos_charge;
  TH2D* clus_pos_size;
  TH2D* clus_pos_time;
  TH2D* clus_time_charge;
  TH2D* clus_time_index;
  TH2D* clus_charge_index;
  TH2D* clus_pos_index;
  TH1D* mult;
  TH1D* clussize;
  TH1D* tot_charge;

  TH2D* bestclus_amp_ratio;


  MM_histo_plane()
    :
      clus_time(0),
      strip_time(0),
      time_charge(0),
      strip_charge(0),
      chan_charge(0),
      clus_pos_charge(0),
      clus_pos_size(0),
      clus_pos_time(0),
      clus_time_charge(0),
      clus_time_index(0),
      clus_charge_index(0),
      clus_pos_index(0),
      mult(0),
      clussize(0),
      tot_charge(0),
      bestclus_amp_ratio(0)
  {}

  //includes histograms from eth-mm-dev branch
  void Init(const char* _name, const MM_plane_calib& calib)
  {
    std::cout << "MESSAGE: initializing plane: " << _name << "\n";
    const unsigned int Nchan     = calib.MM_Nchannels;
    const unsigned int Nstrip    = calib.MM_Nstrips;
    clus_time     = new TH1D(TString::Format("%s_clus_time", _name),
        TString::Format("cluster time in %s; # Time (ns)", _name),
        250, -500, 500);
    strip_time    = new TH2D(TString::Format("%s_strip_time",_name),
        TString::Format("strip vs time in %s; # Strip; Time (ns)", _name),
        Nstrip,-0.5,Nstrip-0.5,
        250,-500,500);
    time_charge    = new TH2D(TString::Format("%s_time_charge",_name),
        TString::Format("time vs max charge in %s; Time (ns); Charge", _name),
        250,-500,500,
        250,0,1000);
    strip_charge  = new TH2D(TString::Format("%s_strip_charge",_name),
        TString::Format(" strips vs charge in %s; # Strip; Charge", _name),
        Nstrip,-0.5,Nstrip-0.5,
        250,0,1000);
    chan_charge  = new TH2D(TString::Format("%s_chan_charge",_name),
        TString::Format(" chan vs charge in %s; # Channel; Charge", _name),
        Nchan,-0.5,Nchan-0.5,
        250,0,1000);
    clus_pos_charge  = new TH2D(TString::Format("%s_clus_pos_charge",_name),
        TString::Format(" clus position vs charge in %s; C.O.M. Strip; Charge", _name),
        Nstrip,-0.5,Nstrip-0.5,
        250,0,15000);
    clus_pos_size    = new TH2D(TString::Format("%s_clus_pos_size",_name),
        TString::Format("Position in strip plane vs size of cluster in %s; C.O.M. Strip; NStrips", _name),
        Nstrip,-0.5,Nstrip-0.5,
        30,-0.5,29.5);
    clus_pos_time    = new TH2D(TString::Format("%s_clus_pos_time",_name),
        TString::Format("Position in strip plane vs time of cluster in %s; C.O.M. Strip; Time (ns)", _name),
        Nstrip,-0.5,Nstrip-0.5,
        500,-500,500);
    clus_time_charge    = new TH2D(TString::Format("%s_clus_time_charge",_name),
        TString::Format("Time vs total charge of cluster in %s; Time (ns); Charge", _name),
        500,-500,500,
        500,0,30000);
    clus_time_index    = new TH2D(TString::Format("%s_clus_time_index",_name),
        TString::Format("Time vs index of cluster in %s; Time (ns); index", _name),
        500,-500,500,
        10,-0.5,9.5);
    clus_charge_index    = new TH2D(TString::Format("%s_clus_charge_index",_name),
        TString::Format("Charge vs index of cluster in %s; Charge; index", _name),
        500,0,30000,
        10,-0.5,9.5);
    clus_pos_index    = new TH2D(TString::Format("%s_clus_pos_index",_name),
        TString::Format("Position in strip plane vs index of cluster in %s; C.O.M. Strip; index", _name),
        Nstrip,-0.5,Nstrip-0.5,
        10,-0.5,9.5);
    mult          = new TH1D(TString::Format("%s_mult",_name),
        TString::Format("Number of clusters in %s ; NClusters", _name),
        30,-0.5,29.5);
    clussize      = new TH1D(TString::Format("%s_clussize",_name),
        TString::Format("Size of clusters in %s ; NStrips", _name),
        30,-0.5,29.5);
    tot_charge    = new TH1D(TString::Format("%s_tot_charge",_name),
        TString::Format("Total Charge in one event in %s; Charge [ADC]; Nevents [-]", _name),
        1000,0,20000);
    bestclus_amp_ratio = new TH2D(TString::Format("%s_bestclus_amp_ratio",_name),
        TString::Format("Best Cluster Amp Ratio in %s; a1/a2; a0/a2", _name),
        100,0.,2,
        100,0.,2.);
  }

  void Fill(TString name, const MicromegaPlane& plane)
  {
    double totalcharge = 0.;
    //CHANNEL
    for(size_t i = 0; i < plane.calib->MM_Nchannels; i++)
      chan_charge->Fill(i, plane.strips[(plane.calib->MM_map.at(i))[0]].charge); //map channel to strip
    //STRIPS
    for(size_t i = 0; i < plane.clusters.size(); i++)
    {
      totalcharge += plane.clusters[i].charge_total;
      for(auto strip : plane.clusters[i].strips)
      {
        //amp ratio
        double a0 = strip.raw[0]; // map channel to strip
        double a1 = strip.raw[1];
        double a2 = strip.raw[2];
        double a12 = a2>0 ? a1/a2 : 0;
        double a02 = a2>0 ? a0/a2 : 0;
        if(a12!=0 && a02!=0) bestclus_amp_ratio->Fill(a12,a02);
        
        strip_time->Fill(strip.bin,strip.time());
        strip_charge->Fill(strip.bin,strip.charge);
        time_charge->Fill(strip.time(),strip.charge);
      }
      clus_time->Fill(plane.clusters[i].cluster_time());
      clus_pos_charge->Fill(plane.clusters[i].position(), plane.clusters[i].charge_total);
      clus_pos_size->Fill(plane.clusters[i].position(), plane.clusters[i].size());
      clus_pos_time->Fill(plane.clusters[i].position(), plane.clusters[i].cluster_time());
      clus_time_charge->Fill(plane.clusters[i].cluster_time(), plane.clusters[i].charge_total);
      clus_time_index->Fill(plane.clusters[i].cluster_time(), i);
      clus_charge_index->Fill(plane.clusters[i].charge_total, i);
      clus_pos_index->Fill(plane.clusters[i].position(), i);
    }
    mult->Fill(plane.clusters.size());
    clussize->Fill(plane.clusters[plane.bestClusterIndex()].size());
    tot_charge->Fill(totalcharge);
  }

};

struct MM_histo
{
  //name
  const char* name;
  bool IsInitialized;
  TFile* out;
  //general MM profile
  TH2D* profile;
  TH2D* profile_global;
  TH2D* profile_all;
  TH2D* time_corr;
  TH2D* charge_corr;
  TH1D* charge_r_corr;
  TH1D* time_diff;

  MM_histo_plane xplane;
  MM_histo_plane yplane;

  MM_histo()
    :
      name("dummy"),
      IsInitialized(false),
      out(0),
      profile(0),
      profile_global(0),
      profile_all(0),
      time_corr(0),
      charge_corr(0),
      charge_r_corr(0),
      time_diff(0)
  {}

  void SetOutFile(TFile* file)
  {
    if(file)
      out = file;
    else
    {
      std::cerr << "invalid file, not initialized \n";
    }
  }


  //includes histograms from eth-mm-dev branch
  void Init(const MM_calib* calib)
  {
    const unsigned int XNstrip    = calib->xcalib.MM_Nstrips;
    const unsigned int Xstripstep = calib->xcalib.MM_strip_step;
    const unsigned int YNstrip    = calib->ycalib.MM_Nstrips;
    const unsigned int Ystripstep = calib->ycalib.MM_strip_step;
    name = calib->name.c_str();
    if(out){out->mkdir(name);out->cd(name);}
    profile = new TH2D(TString::Format("%s_profile",name),
        TString::Format(" XY profile in %s; X [mm]; Y [mm]", name),
        XNstrip, -XNstrip * Xstripstep / 2, XNstrip * Xstripstep / 2,
        YNstrip, -YNstrip * Ystripstep / 2, YNstrip * Ystripstep / 2);
    profile_global = new TH2D(TString::Format("%s_profile_global",name),
        TString::Format(" XY profile in %s in global coordinate; X [mm]; Y [mm]", name),
        1200, -600, 600,
        400,  -200, 200);
    profile_all = new TH2D(TString::Format("%s_profile_all",name),
        TString::Format(" XY profile in %s in global coordinate all hits; X [mm]; Y [mm]", name),
        1200, -600, 600,
        400,  -200, 200);
    time_corr = new TH2D(TString::Format("%s_time_corr",name),
        TString::Format(" Time of clusters in X and Y planes for hits in  %s; X-plane time [ns]; Y-plane time [ns]", name),
        500, -500, 500,
        500, -500, 500);
    charge_corr = new TH2D(TString::Format("%s_charge_corr",name),
        TString::Format(" Charge of clusters in X and Y planes for hits in %s; X-plane charge [ADC counts]; Y-plane [ADC counts]", name),
        600, 0, 6000,
        600, 0, 6000);
    time_diff = new TH1D(TString::Format("%s_time_diff",name),
        TString::Format(" RMS of expected calib time and cluster time for hits in %s; #sqrt{(t_{x}-t_{x,calib})^{2}+(t_{y}-t_{y,calib})^{2}} [ns]", name),
        1000, 0, 1000);
    TString planenameX(name),planenameY(name),dirname(name);
    planenameX+="X";
    planenameY+="Y";
    dirname+="/";
    if(out){
      out->mkdir(dirname+planenameX);out->cd(dirname+planenameX);
    }
    xplane.Init(planenameX.Data(), calib->xcalib);

    if(out){
      out->mkdir(dirname+planenameY);out->cd(dirname+planenameY);
    }
    yplane.Init(planenameY.Data(), calib->ycalib);
    //finalize
    if(out)out->cd();
    IsInitialized = true;
  }

  void Fill(const Micromega* mm)
  {
    if(!IsInitialized)
    {
      std::cout << "initialize histogram collection with: " << mm->calib->name << "\n";
      Init(mm->calib);
    }

    if(!mm->hasHit())
      return;

    //fill
    profile->Fill(mm->xpos, mm->ypos);
    profile_global->Fill(mm->abspos().X(), mm->abspos().Y());
    for (const auto& hit : mm->hits) {
      profile_all->Fill(hit.abspos().X(), hit.abspos().Y());
      time_diff->Fill(hit.timediff());
      time_corr->Fill(hit.xcluster->cluster_time(), hit.ycluster->cluster_time());
      charge_corr->Fill(hit.xcluster->charge_total, hit.ycluster->charge_total);
    }
    xplane.Fill(TString(name)+"X", mm->xplane);
    yplane.Fill(TString(name)+"Y", mm->yplane);
  }

  //write projection
  void WriteProj(const bool fit = false)
  {
    TString planenameX(name),planenameY(name),dirname(name);
    planenameX+="X";
    planenameY+="Y";
    dirname+="/";

    if(out)out->cd(dirname+planenameX);
    TH1D* px =  profile->ProjectionX();
    TH1D* pxg = profile_global->ProjectionX();
    if(fit){px->Fit("gaus","Q");pxg->Fit("gaus","Q");gStyle->SetOptFit(1);}

    if(out)out->cd(dirname+planenameY);
    TH1D* py =  profile->ProjectionY();
    TH1D* pyg = profile_global->ProjectionY();
    if(fit){py->Fit("gaus","Q");pyg->Fit("gaus","Q");gStyle->SetOptFit(1);}

    if(out)out->cd();
  }
};

//source: eth-mm-dev branch
struct MM_comparison {
  //name
  const char* name;
  bool IsInitialized;
  TFile* out;
  //general MM comparisons
  TH2D* time[2];
  TH2D* pos[2];
  TH2D* charge[2];

  MM_comparison()
    :
      name("dummy"),
      IsInitialized(false),
      out(0)
  {
    time[0] = nullptr;
    time[0] = nullptr;
    pos[0] = nullptr;
    pos[1] = nullptr;
    charge[0] = nullptr;
    charge[1] = nullptr;
  }

  void SetOutFile(TFile* file)
  {
    if(file)
      out = file;
    else
    {
      std::cerr << "invalid file, not initialized \n";
    }
  }



  void Init(const MM_calib* calib1, const MM_calib* calib2)
  {
    const unsigned int XNstrip1    = calib1->xcalib.MM_Nstrips;
    const unsigned int Xstripstep1 = calib1->xcalib.MM_strip_step;
    const unsigned int YNstrip1    = calib1->ycalib.MM_Nstrips;
    const unsigned int Ystripstep1 = calib1->ycalib.MM_strip_step;
    TString name1 = calib1->name.c_str();
    const unsigned int XNstrip2    = calib2->xcalib.MM_Nstrips;
    const unsigned int Xstripstep2 = calib2->xcalib.MM_strip_step;
    const unsigned int YNstrip2    = calib2->ycalib.MM_Nstrips;
    const unsigned int Ystripstep2 = calib2->ycalib.MM_strip_step;
    TString name2 = calib2->name.c_str();
    name = (name1+"_vs_"+name2).Data();
    if(out){out->mkdir(name);out->cd(name);}
    pos[0] = new TH2D(TString::Format("%s_vs_%s_pos_x", name1.Data(), name2.Data()),
        TString::Format(" X pos in %s vs %s; X in %s [mm]; X in %s [mm]", name1.Data(), name2.Data(), name1.Data(), name2.Data()),
        XNstrip1, -XNstrip1 * Xstripstep1 / 2, XNstrip1 * Xstripstep1 / 2,
        XNstrip2, -XNstrip2 * Xstripstep2 / 2, XNstrip2 * Xstripstep2 / 2);
    pos[1] = new TH2D(TString::Format("%s_vs_%s_pos_y", name1.Data(), name2.Data()),
        TString::Format(" Y pos in %s vs %s; Y in %s [mm]; Y in %s [mm]", name1.Data(), name2.Data(), name1.Data(), name2.Data()),
        YNstrip1, -YNstrip1 * Ystripstep1 / 2, YNstrip1 * Ystripstep1 / 2,
        YNstrip2, -YNstrip2 * Ystripstep2 / 2, YNstrip2 * Ystripstep2 / 2);
    time[0] = new TH2D(TString::Format("%s_vs_%s_time_x", name1.Data(), name2.Data()),
        TString::Format("Time of clusters in X for hits in %s vs %s; X-plane time in %s [ns]; X-plane time in %s [ns]", name1.Data(), name2.Data(), name1.Data(), name2.Data()),
        500, -500, 500,
        500, -500, 500);
    time[1] = new TH2D(TString::Format("%s_vs_%s_time_y", name1.Data(), name2.Data()),
        TString::Format("Time of clusters in Y for hits in %s vs %s; Y-plane time in %s [ns]; Y-plane time in %s [ns]", name1.Data(), name2.Data(), name1.Data(), name2.Data()),
        500, -500, 500,
        500, -500, 500);
    charge[0] = new TH2D(TString::Format("%s_vs_%s_charge_x", name1.Data(), name2.Data()),
        TString::Format(" Charge of clusters in X for hits in %s vs %s; X-plane charge in %s [ADC counts]; X-plane in %s [ADC counts]", name1.Data(), name2.Data(), name1.Data(), name2.Data()),
        600, 0, 6000,
        600, 0, 6000);
    charge[1] = new TH2D(TString::Format("%s_vs_%s_charge_y", name1.Data(), name2.Data()),
        TString::Format(" Charge of clusters in Y for hits in %s vs %s; Y-plane charge in %s [ADC counts]; Y-plane in %s [ADC counts]", name1.Data(), name2.Data(), name1.Data(), name2.Data()),
        1000, 0, 10000,
        1000, 0, 10000);

    //finalize
    if(out)out->cd();
    IsInitialized = true;
  }

  void Fill(const Micromega* mm1, const Micromega* mm2)
  {
    if(!IsInitialized)
    {
      std::cout << "initialize histogram collection with: " << mm1->calib->name << " and " << mm2->calib->name << "\n";
      Init(mm1->calib, mm2->calib);
    }

    if(!mm1->hasHit() || !mm2->hasHit())
      return;

    //fill
    for (const auto& hit1 : mm1->hits) {
      for (const auto& hit2 : mm2->hits) {
        pos[0]->Fill(hit1.pos().X(), hit2.pos().X());
        pos[1]->Fill(hit1.pos().Y(), hit2.pos().Y());
        charge[0]->Fill(hit1.xcluster->charge_total, hit2.xcluster->charge_total);
        charge[1]->Fill(hit1.ycluster->charge_total, hit2.ycluster->charge_total);
        time[0]->Fill(hit1.xcluster->cluster_time(), hit2.xcluster->cluster_time());
        time[1]->Fill(hit1.ycluster->cluster_time(), hit2.ycluster->cluster_time());
      }
    }
  }
};


int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./MM_response.exe file1 [file2 ...]" << endl;
    return 1;
  }

  int event;
  const unsigned int NMM = 11;
  std::string file(argv[1]);
  std::size_t found_run = file.find("1001-0");
  std::size_t found_dotroot = file.find(".dat");
  std::string run_name = file.substr(found_run,found_dotroot-found_run);
  std::string path = file.substr(0,found_run);

  std::string new_file = "run"+run_name+"_MM_response.root";

  TFile* file_new = new TFile(new_file.c_str(),"RECREATE");


  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();

  std::vector<MM_histo*> histvec;
  std::vector<MM_comparison*> compvec;

#if 0
  unsigned int kRUN = 0;
  ConfigFileParser parser;
  parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config.json");
  std::vector<Track_t> tracks;
  std::vector<std::pair<TVector3, double> > hits;
#endif

  //histogram for momentum
  file_new->mkdir("tracking");file_new->cd("tracking");
  TrackHisto* MMall = new TrackHisto("all");
  TrackHisto* MM3   = new TrackHisto("MM3");
  TrackHisto* MM4   = new TrackHisto("MM4");
  TrackHisto* MM5   = new TrackHisto("MM5");
  TrackHisto* MM6   = new TrackHisto("MM6");
  file_new->cd();

  unsigned int nr = 0;
  for(unsigned int i(0); i < NMM; ++i){
    histvec.push_back(new MM_histo);
    histvec[i]->SetOutFile(file_new);
    for(unsigned int j(i+1); j < NMM; ++j){
      compvec.push_back(new MM_comparison);
      compvec[nr]->SetOutFile(file_new);
      ++nr;
    }
  }


  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");


  while (manager.ReadEvent()) {


    const int nevt= manager.GetEventsCounter();

    event=manager.GetEventsCounter();

    if(nevt%1000==1) cout << "Run "<<manager.GetEvent().GetRunNumber()<<"===> Event #" << manager.GetEventsCounter() << endl;
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();

    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }

#if 0
    if (kRUN != manager.GetEvent1Run().GetRun()) {
      kRUN = manager.GetEvent1Run().GetRun();
      InitTracking(kRUN);
    }
#endif

    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);

    // process only "physics" events (no random or calibration trigger)
    //if (!e.isPhysics) continue;
    if (!e.isTriggerBeamOnly()) continue;

    nr = 0;
    for(unsigned int i(1); i <= NMM; ++i)
    {
      const Micromega* mm = e.getMM(i);

      //lowering index by 1, just to match index of MM
      histvec[i-1]->Fill(mm);

      for(unsigned int j(i+1); j <= NMM; ++j)
      {
        const Micromega* mm2 = e.getMM(j);
        
        compvec[nr]->Fill(mm, mm2);
        ++nr;
      }
    }

    //FILL MOMENTUM
    //complete momentum
#if 0
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM3, hits, parser);FillAbsMMHits(e.MM4, hits, parser);
    FillAbsMMHits(e.MM5, hits, parser);//FillAbsMMHits(e.MM6, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MMall->Fill(tracks);

    //MM3
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM3, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM3->Fill(tracks);

    //MM4
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM4, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM4->Fill(tracks);

    //MM5
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM5, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM5->Fill(tracks);

    //MM6
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM6, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM6->Fill(tracks);
#endif

    //if (nevt > 10000) break;
  }
  //}
for(auto p : histvec)p->WriteProj(1);
MMall->FitMom();MM3->FitMom();MM4->FitMom();MM5->FitMom();MM6->FitMom();
file_new->Write();
file_new->Close();

return 0;
}
