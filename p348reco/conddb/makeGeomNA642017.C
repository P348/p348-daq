#include "TVector3.h"

// utility function
TGeoMatrix* makeGeoMatrix(const TVector3& pos, const TGeoRotation &rot)
{
  const TGeoTranslation tr(pos.X(), pos.Y(), pos.Z());
  return new TGeoCombiTrans(tr, rot);
}

void makeGeomNA642017()
{
  //--- Definition of a simple geometry
  //   gSystem->Load("libGeom");
   TGeoManager::SetVerboseLevel(0);
   new TGeoManager("genfitGeom", "NA64MM geometry");

   unsigned int medInd(0);

   TGeoMaterial *_siliconMat = new TGeoMaterial("siliconMat",28.0855,14.,2.329);
   _siliconMat->SetRadLen(1.);//calc automatically, need this for elemental mats.
   TGeoMedium *sil = new TGeoMedium("silicon",medInd++,_siliconMat,0);

   TGeoMixture *_airMat = new TGeoMixture("airMat",3);
   _airMat->AddElement(14.01,7.,.78);
   _airMat->AddElement(16.00,8.,.21);
   _airMat->AddElement(39.95,18.,.01);
   _airMat->SetDensity(1.2e-3);
   TGeoMedium *air = new TGeoMedium("air",medInd++,_airMat,0);

   TGeoMixture *_vacuumMat = new TGeoMixture("vacuumMat",3);
   _vacuumMat->AddElement(14.01,7.,.78);
   _vacuumMat->AddElement(16.00,8.,.21);
   _vacuumMat->AddElement(39.95,18.,.01);
   _vacuumMat->SetDensity(1.2e-15);
   TGeoMedium *vacuum = new TGeoMedium("vacuum",medInd++,_vacuumMat,0);


   TGeoVolume *top = gGeoManager->MakeBox("TOPPER", air, 100., 100., 3000.);
   gGeoManager->SetTopVolume(top); // mandatory !

   // conversion mm to cm
   const double mm2cm = 0.1;

   // micromega size in cm
   const double mmdx = 8;
   const double mmdy = 8;
   const double mmdz = 0.05;
   
   const TGeoRotation r_old("MM_rotation", 45, 0, 0);
   const TGeoRotation r_new("MM_rotation", 135, 0, 0);
   
   // copy-paste from conddb.h: first version, only main position
   const TVector3 MM1_pos(-17,-6,-19670);
   const TVector3 MM2_pos(-6,8,-19580);
   const TVector3 MM3_pos(-17,-1.5,-18220);
   const TVector3 MM4_pos(-8,2.7,-18130);
   const TVector3 MM5_pos(-317,0,-3170);
   const TVector3 MM7_pos(-366,5,-1030);

   TGeoVolume *MM7 = gGeoManager->MakeBox("MM7", sil,mmdx,mmdy,mmdz);
   MM7->SetLineColor(kBlue);
   top->AddNode(MM7, 1, makeGeoMatrix(mm2cm * MM7_pos, r_old));
   
   TGeoVolume *MM5 = gGeoManager->MakeBox("MM5", sil,mmdx,mmdy,mmdz);
   MM5->SetLineColor(kBlue);
   top->AddNode(MM5, 1, makeGeoMatrix(mm2cm * MM5_pos, r_old));

   TGeoVolume *MM4 = gGeoManager->MakeBox("MM4", sil,mmdx,mmdy,mmdz);
   MM4->SetLineColor(kBlue);
   top->AddNode(MM4, 1, makeGeoMatrix(mm2cm * MM4_pos, r_new));
  
   TGeoVolume *MM3 = gGeoManager->MakeBox("MM3", sil,mmdx,mmdy,mmdz);
   MM3->SetLineColor(kBlue);
   top->AddNode(MM3, 1, makeGeoMatrix(mm2cm * MM3_pos, r_old));

   TGeoVolume *MM2 = gGeoManager->MakeBox("MM2", sil,mmdx,mmdy,mmdz);
   MM2->SetLineColor(kBlue);
   top->AddNode(MM2, 1, makeGeoMatrix(mm2cm * MM2_pos, r_new));

   TGeoVolume *MM1 = gGeoManager->MakeBox("MM1", sil,mmdx,mmdy,mmdz);
   MM1->SetLineColor(kBlue);
   top->AddNode(MM1, 1, makeGeoMatrix(mm2cm * MM1_pos, r_old));

   
   //--- close the geometry
   gGeoManager->CloseGeometry();

   //--- draw the ROOT box
   gGeoManager->SetVisLevel(10);
   if (!gROOT->IsBatch()) top->Draw("ogl");
   
   TFile outfile("NA64Geom2017.root", "RECREATE");
   gGeoManager->Write();
}
