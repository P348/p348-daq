# VHCAL calibrations for 2023-e run

runs=8459-9717

# The 1.5 GeV is peak position of MIP energy deposition distribution for 160 GeV mu
# (Calculated by Mikhail Kirsanov)
factor.VHCAL0=1.5

# Energy calibrations data by Vladimir Samoylenko,
# released 2023 May 28:
#   1110 May 29 16:05 /online/detector/calibrs/2023.xml/VHCAL.xml
#   Based on Runs 8644-8645


columns=name, x, y, peakpos,ledref
VHCAL0        0  0    102.    1667.
VHCAL0        0  1    147.    2426.
VHCAL0        0  2    169.    1046.
VHCAL0        0  3    179.    2494.
VHCAL0        1  0     63.    992.
VHCAL0        1  1    109.    2308.
VHCAL0        1  2    136.    3158.
VHCAL0        1  3    132.    2177.
VHCAL0        2  0    135.    1914.
VHCAL0        2  1    164.    918.
VHCAL0        2  2    139.    1212.
VHCAL0        2  3    158.    427.
VHCAL0        3  0     93.    997.
VHCAL0        3  1     95.    82.
VHCAL0        3  2    180.    320.
VHCAL0        3  3     63.    219.

#Time calibrations by A. Celentano from runs 9061-9066 (h calib. runs)
#Documentation: https://gitlab.cern.ch/P348/p348-daq/-/issues/79#note_7096987
columns=name, x, y, time,timesigma
VHCAL0  0    0  58.24  2.4
VHCAL0  0    1  56.35  2.2
VHCAL0  0    2  59.72  2.1
VHCAL0  0    3  54.01  2.3
VHCAL0  1    0  56.39  2.4
VHCAL0  1    1  52.06  1.8
VHCAL0  1    2  57.46  1.7
VHCAL0  1    3  54.93  2.4
VHCAL0  2    0  51.90  2.0
VHCAL0  2    1  58.39  1.8
VHCAL0  2    2  58.39  1.9
VHCAL0  2    3  56.43  2.3
VHCAL0  3    0  59.60  2.3
VHCAL0  3    1  57.36  2.0
VHCAL0  3    2  62.99  2.2
VHCAL0  3    3  63.01  2.1

