# VETO calibrations for 2021-e run
runs=4642-5187

# mean energy deposition in one cell (MC by M.Kirsanov)
factor.VETO=0.0098

# VETO calibration
# by Vladimir Samoilenko
# plots:
#   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2021A/Veto.pdf

# Calibration runs:
# Run 5016 for Veto11, Veto12 (X0Y0, X1Y0)
# Run 5007 for Veto21, Veto22 (X2Y0, X3Y0)
# Run 5013 for Veto31, Veto32 (X4Y0, X5Y0)

# Fit was performed by the sum:
#   Landau (for muon Signal) + Exponent (for a Background)

# time: preliminary calibration, extracted from run 5175 (by AK)

columns=name,x,y,peakpos,peakpos_err,time,timesigma
#name  X Y  MPV    MPV_error   time timesigma
VETO   0 0  479.5  4.0         -15.  5.
VETO   1 0  478.1  3.9         -15.  5.
VETO   2 0  508.0  1.0         -15.  5.
VETO   3 0  427.6  1.1         -15.  5.
VETO   4 0  454.7  1.1         -15.  5.
VETO   5 0  448.3  1.3         -15.  5.
