# VETO calibrations for 2022-mu run

# Energy calibration data by Vladimir Samoylenko,
# Released 2022 May 09:
#   https://twiki.cern.ch/twiki/bin/view/P348/RunsInfo2022A
#   Calibration coefficients: file veto_clb.dat
#   Fit plots: file veto_clb.pdf

# Calibration by muon, Runs 5620, 5621, 5622.
# Fit performed by Landau distribution.
# Format data File:
# name | X | Y | Norm | Er(Norm) | MPV | Er(MPV) | Sigma | Er(Sigma) | Chi2/NDF 

runs=5575-6034

# mean energy deposition in one cell is 9.8 MeV (MC by M.Kirsanov)
factor.VETO=0.0098

# Only bottom PMs are connected to MSADC, top PMs are connected to discriminator for calibration Beam trigger.

# bottom PMs:
columns=name,x,y,norm,norm_err,peakpos,peakpos_err,sigma,sigma_err,chi2ndf
#name   X  Y     ...       ...     peakpos   ...
VETO    0  0     304.2     10.9     149.2    1.1    20.9    0.7   1.33
VETO    2  0    46970.    238.0      69.2   0.05     6.1   0.03   30.2
VETO    4  0     255.1      9.1     139.2    1.4    24.9   0.83   1.09

# top PMs (not connected to ADC): enforce "zero" .energy with peakpos=1e10
columns=name,x,y,peakpos
#name   X  Y   peakpos
VETO    1  0     1e10
VETO    3  0     1e10
VETO    5  0     1e10


# Timing calibrations by Anton Karneyeu
# released 2022 May 30
# preliminary, made by visual inspection of `timing.cc` histograms for run 6030

# name   X    Y    tmean   tsigma
columns=name,x,y,time,timesigma
VETO     0    0    65.     10.
VETO     1    0    65.     10.
VETO     2    0    65.     10.
VETO     3    0    65.     10.
VETO     4    0    65.     10.
VETO     5    0    65.     10.
