# LYSO calibrations for 2024-e run

runs=10316-11467



# Energy deposited in LYSO for MIP-like particle (70 GeV/c pi+) passing through the 3.2 cm-long side:
# As predicted from MC: https://gitlab.cern.ch/P348/p348-daq/-/issues/126#note_8280021
# See also: https://indico.cern.ch/event/1423568/
factor.LYSO=0.031

# LYSO preliminary calibration data by Andrea Celentano
# released June 2024, using run 11415. 70 GeV hadrons.
#  
#  

columns=name,x,y,peakpos
LYSO   0 0  2368
LYSO   1 0  2309
LYSO   2 0  2466
LYSO   3 0  2316
LYSO   4 0  2299
LYSO   5 0  1982

