# VETO, 2024-electron run

# Timing by A. Celentano, from hadron calb. runs 9061-9066.
# Documentation: https://gitlab.cern.ch/P348/p348-daq/-/issues/146
# These values are the average time (relative to master time) for Amplitude ~ 500 (MIP region)
# See code in `p348reco.h` regarding time-walk correction

runs=10316-11467
columns=name,x,y,time,timesigma
VETO  0  0  101.21  2.8
VETO  1  0  100.73  2.7
VETO  2  0   98.37  2.0
VETO  3  0   99.29  2.0
VETO  4  0  101.19  3.3
VETO  5  0  101.13  2.8