// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile2D.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"

// per-channel projections of TH3I time-channel-amplitude histogram
void th3_slice_by_channels(TH3I* h3,double aMin=10.)
{
  const TString pwd = gDirectory->GetPath();
  const TString subdir = h3->GetName() + TString("slices");
  gDirectory->mkdir(subdir);
  gDirectory->cd(subdir);
  
  TAxis* aC = h3->GetYaxis(); // channel axis
  TAxis* aA = h3->GetZaxis(); // amplitude axis
  
  for (int i = 1; i <= aC->GetNbins(); ++i) {
    const TString name = aC->GetBinLabel(i);
    //cout << name << endl;
    
    // => select channel 'name'
    aC->SetRange(i, i);
    
    // select all amplitudes
    aA->SetRange();
    
    const TString iname = TString::Format("h%d", i);
    TH2D* pr = (TH2D*) h3->Project3D(iname+"_" + "zx"); // zx = amplitude-vs-time
    pr->SetNameTitle(name, "Channel " + name);
    pr->SetOption("COL");
    
    const TString tname = TString::Format("ht%d", i);
    TH1D* pt = (TH1D*) h3->Project3D(tname+"_" + "x"); // x = time
    pt->SetNameTitle(name+"time", "Channel " + name + " time");
    //pt->SetOption("COL");
    
    // select amplitudes above threshold to reject noise peaks
    // the default A>=10. corresponds to ~3sigma of typical noise level of ~a few ADC counts
    // the '+0.0001' is to avoid the uncertanty on the bin edge
    aA->SetRangeUser(aMin+0.0001, 1e9);
    
    {
    const TString iname = TString::Format("h10%d", i);
    TH2D* pr = (TH2D*) h3->Project3D(iname+"_" + "zx"); // zx = amplitude-vs-time
    pr->SetNameTitle(name+"_a10", "Channel " + name + " {A>10}");
    pr->SetOption("COL");
    
    const TString tname = TString::Format("h10t%d", i);
    TH1D* pt = (TH1D*) h3->Project3D(tname+"_" + "x"); // x = time
    pt->SetNameTitle(name+"_a10_time", "Channel " + name + " time" + " {A>10}");
    //pt->SetOption("COL");
    }
  }
  
  gDirectory->cd(pwd);
}

template<typename HISTO>
void sort_bins(HISTO* h, TString axis, TString option = "a v")
{
  h->LabelsDeflate(axis);
  h->LabelsOption(option, axis);
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./timing.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  TFile* file = new TFile("timing.root", "RECREATE");
  
  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");
  
  // master time
  TH1I* masterT = new TH1I("masterT", "Master time;Time, ns;events", 400, 0, 400);
  
  // per-channel mean waveform
  TProfile2D* waveformprofile = new TProfile2D("waveformprofile", "Waveform profile;Channel;Time sample;ADC", 300, 0, 300, 100, 0, 100);
  waveformprofile->SetOption("COL");
  
  TProfile2D* waveformprofile_Master = new TProfile2D("waveformprofile_Master", "Waveform profile (-master);Channel;Time sample;ADC", 300, 0, 300, 100, -50, 50);
  waveformprofile_Master->SetOption("COL");
  
  TProfile2D* waveformprofile_Expected = new TProfile2D("waveformprofile_Expected", "Waveform profile (-expected);Channel;Time sample;ADC", 300, 0, 300, 100, -50, 50);
  waveformprofile_Expected->SetOption("COL");
  
  // histograms channel-time for best peak
  TH2I* timing = new TH2I("timing", "Best peak timing;Channel;Time, ns;events", 300, 0, 300, 401, 0, 401);
  timing->SetOption("COL");
  
  TH2I* timingMaster = new TH2I("timingMaster", "Best peak timing (-master);Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingMaster->SetOption("COL");
  
  TH2I* timingExpected = new TH2I("timingExpected", "Best peak timing (-expected);Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingExpected->SetOption("COL");
  
  TH2I* timingStd = new TH2I("timingStd", "Best peak timing (-expected) spread;Channel;spread, 1#sigma;events", 300, 0, 300, 4*51, 0, 51);
  timingStd->SetOption("COL");
  
  // histograms channel-time for best peak
  // with cut A>10 to suppress noise peaks
  TH2I* timingMaster10 = new TH2I("timingMaster10", "Best peak timing (-master) {A>10};Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingMaster10->SetOption("COL");
  
  TH2I* timingExpected10 = new TH2I("timingExpected10", "Best peak timing (-expected) {A>10};Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingExpected10->SetOption("COL");
  
  TH2I* timingStd10 = new TH2I("timingStd10", "Best peak timing (-expected) spread {A>10};Channel;spread, 1#sigma;events", 300, 0, 300, 4*51, 0, 51);
  timingStd10->SetOption("COL");
  
  // histograms channel-time for all peaks
  TH2I* timingA = new TH2I("timingA", "All peaks timing;Channel;Time, ns;events", 300, 0, 300, 401, 0, 401);
  timingA->SetOption("COL");
  
  TH2I* timingAMaster = new TH2I("timingAMaster", "All peaks timing (-master);Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingAMaster->SetOption("COL");
  
  TH2I* timingAExpected = new TH2I("timingAExpected", "All peaks timing (-expected);Channel;Time, ns;events", 300, 0, 300, 402, -201, 201);
  timingAExpected->SetOption("COL");
  
  // histograms channel-time-amplitude
  // in use to prepare per-channel time-amplitude histograms
  TH3I* peaksall = new TH3I("peaksall", "All peaks;Time, ns;Channel;Amplitude, ADC", 400, 0, 400, 400, 0, 400, 410, 0, 4100);
  peaksall->SetOption("BOX1");
  peaksall->SetDirectory(0);
  
  TH3I* peaksbest = new TH3I("peaksbest", "Best peaks;Time, ns;Channel;Amplitude, ADC", 400, 0, 400, 400, 0, 400, 410, 0, 4100);
  peaksbest->SetOption("BOX1");
  peaksbest->SetDirectory(0);
  
  TH3I* peaksbestM = new TH3I("peaksbest_master", "Best peaks (-master);Time, ns;Channel;Amplitude, ADC", 400, -200, 200, 400, 0, 400, 410, 0, 4100);
  peaksbestM->SetOption("BOX1");
  peaksbestM->SetDirectory(0);
  
  TH3I* peaksbestE = new TH3I("peaksbest_expected", "Best peaks (-expected);Time, ns;Channel;Amplitude, ADC", 400, -200, 200, 400, 0, 400, 410, 0, 4100);
  peaksbestE->SetOption("BOX1");
  peaksbestE->SetDirectory(0);
  
  //A.C. adding largest peak
  TH3I* peakslargest = new TH3I("peakslargest", "Largest peak (A>10);Time, ns;Channel;Amplitude, ADC", 400, 0, 400, 400, 0, 400, 410, 0, 4100);
  peakslargest->SetOption("BOX1");
  peakslargest->SetDirectory(0);

  TH3I* peakslargestM = new TH3I("peakslargestM", "Largest peaks (A>10)(-master);Time, ns;Channel;Amplitude, ADC", 400, -200, 200, 400, 0, 400, 410, 0, 4100);
  peakslargestM->SetOption("BOX1");
  peakslargestM->SetDirectory(0);

  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << nevt << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics) continue;
    
    const bool hasMaster = e.hasMasterTime();
    const double master = e.masterTime;

    
    masterT->Fill(master);
    
    // skip events without master time
    if (!hasMaster) continue;

   
   
    const vector<const Cell*> list = listCells(e);
    
    for (size_t i = 0; i < list.size(); ++i) {
      const Cell& c = *list[i];
      
      if (!c.hasDigit) continue;
      
      double ct0 = c.t0ns();
      if (!isfinite(ct0)) ct0 = 0;

      const double amplScaleFactor = (c.calib->adc.isWB ? 0.25 : 1);
      
      const char* name = c.calib->name.c_str();
      const double masterSample = master / c.calib->adc.sampling_interval;
      const double texpected = c.calib->texpected(master);
      const double texpectedSample = texpected / c.calib->adc.sampling_interval;
      
      const double t0 = coerce(ct0, 0, 400);
      const double t0Mt = coerce(ct0 - master, -200, 200);
      const double t0Ex = coerce(ct0 - texpected, -200, 200);
      const double t0Ex_std = coerce(fabs(ct0 - texpected)/c.calib->tsigma, 0, 50);
      
      // best peaks
      timing->Fill(name, t0, 1.);
      timingMaster->Fill(name, t0Mt, 1.);
      timingExpected->Fill(name, t0Ex, 1.);
      timingStd->Fill(name, t0Ex_std, 1.);
      peaksbest->Fill(ct0, name, c.amplitude*amplScaleFactor, 1.);
      peaksbestM->Fill(t0Mt, name, c.amplitude*amplScaleFactor, 1.);
      peaksbestE->Fill(t0Ex, name, c.amplitude*amplScaleFactor, 1.);
      
      // best peaks, A>10
      if ((c.amplitude*amplScaleFactor) > 10.) {
        timingMaster10->Fill(name, t0Mt, 1.);
        timingExpected10->Fill(name, t0Ex, 1.);
        timingStd10->Fill(name, t0Ex_std, 1.);
      }
      
      // waveform
      for (size_t j = 0; j < c.raw.size(); ++j) {
        waveformprofile->Fill(name, (double)j, c.wave(j)*amplScaleFactor, 1.);
        waveformprofile_Master->Fill(name, (double)(j) - masterSample, c.wave(j)*amplScaleFactor, 1.);
        waveformprofile_Expected->Fill(name, (double)(j) - texpectedSample, c.wave(j)*amplScaleFactor, 1.);
      }
      
      // all peaks
      double ApeakMax=-999;
      double t0peakMax=-999;

      if (c.calib->doWaveformFit==false){
        for (size_t j = 0; j < c.peaks.size(); ++j) {
          const double t0peak = c.calib->adc.sampling_interval*c.peaktime(j);
          const double Apeak = c.peakamplitude(j)*amplScaleFactor;
        
          if (Apeak>ApeakMax){
              ApeakMax=Apeak;
              t0peakMax=t0peak;
          }

          timingA->Fill(name, t0peak, 1.);
          timingAMaster->Fill(name, t0peak - master, 1.);
          timingAExpected->Fill(name, t0peak - texpected, 1.);
          peaksall->Fill(t0peak, name, Apeak, 1.);
        }
      }
      else{
        for(const auto & fittedPeak : c.fittedPeaksInfo) {
          if(!fittedPeak.are_amp_and_time_valid()) continue;
          const double t0peak = fittedPeak.time;
          const double Apeak = fittedPeak.rawAmp;

          if (Apeak>ApeakMax){
              ApeakMax=Apeak;
              t0peakMax=t0peak;
          }

          timingA->Fill(name, t0peak, 1.);
          timingAMaster->Fill(name, t0peak - master, 1.);
          timingAExpected->Fill(name, t0peak - texpected, 1.);
          peaksall->Fill(t0peak, name, Apeak, 1.);
        }
      }
   
      if (ApeakMax>0.){
          peakslargest->Fill(t0peakMax,name,ApeakMax,1.);
          peakslargestM->Fill(t0peakMax-master,name,ApeakMax,1.);
      }
    }
  }
  
  cout << "INFO: generating plots..." << endl;
  
  // sort bins by channel name
  sort_bins(waveformprofile, "X");
  sort_bins(waveformprofile_Master, "X");
  sort_bins(waveformprofile_Expected, "X");
  sort_bins(timing, "X");
  sort_bins(timingMaster, "X");
  sort_bins(timingExpected, "X");
  sort_bins(timingStd, "X");
  sort_bins(timingMaster10, "X");
  sort_bins(timingExpected10, "X");
  sort_bins(timingStd10, "X");
  sort_bins(timingA, "X");
  sort_bins(timingAMaster, "X");
  sort_bins(timingAExpected, "X");
  sort_bins(peaksall, "Y");
  sort_bins(peaksbest, "Y");
  sort_bins(peaksbestM, "Y");
  sort_bins(peaksbestE, "Y");
  sort_bins(peakslargest,"Y");
  sort_bins(peakslargestM,"Y");
  
  // per-channel projections for time-amplitude plots
  th3_slice_by_channels(peaksall);
  th3_slice_by_channels(peaksbest);
  th3_slice_by_channels(peaksbestM);
  th3_slice_by_channels(peaksbestE);
  th3_slice_by_channels(peakslargest);
  th3_slice_by_channels(peakslargestM);

  
  // save all histograms to .root file
  file->Write();
  file->Close();
  
  cout << "INFO: processing finished, output file " << file->GetName() << endl;
  
  return 0;
}
