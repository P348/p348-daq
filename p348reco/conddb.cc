#include "conddb.h"

#include <cmath>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include "TVector3.h"
#include "TSystemDirectory.h"
#include "TCollection.h"
#include "sdc.h"


const double mm2cm = 0.1; //conversion mm to cm
const double cm2mm = 10.; //conversion cm to mm
double emass  = 0.510998e-03;
const int MM_apv_time_samples = 3;
const bool MM_clear_clusters = true;
const bool MM_time_ratio_sigma = true;


int MM_cluster_sort = kSort_ByTiming;
int MM_hit_sort = kSort_ByTiming;

string conddbpath()
{
  // path to conddb/ directory
  static string root = "";
  
  // locate and init the path
  if (root == "") {
    const char* v = getenv("CONDDB");
    root = (v) ? v : "conddb";
    
    // empty string is `current directory`
    if (root == "") root = ".";
    
    // append '/'
    if (root[root.size()-1] != '/')
      root += '/';
    
    cout << "INFO: conddbpath() CONDDB=" << root << endl;
  }
  
  return root;
}

string na64session(const time_t time)
{
  char year[10];
  char month[10];
  char day[10];
  strftime(year, 10, "%Y", localtime(&time));
  strftime(month, 10, "%m", localtime(&time));
  strftime(day, 10, "%d", localtime(&time));
  
  const bool isMu = 
    (atoi(year)==2021 && (atoi(month)>=10 && atoi(month)<=11)) ||
    (atoi(year)==2022 && (atoi(month)>=4 && atoi(month)<=5))   ||
    (atoi(year)==2023 && ((atoi(month)==7 && atoi(day)>=12) || atoi(month)==8)) || 
    (atoi(year)==2024 && ((atoi(month)==7 && atoi(day)>=16) || atoi(month)==8 || (atoi(month)==9 && atoi(day)<=4)) );
  
  string session = year;
  session += (isMu ? "mu" : "");
  return session;
}

string FindApvPedestalFile(const time_t time, const string detname, const int datesize)
{
  // directory structure format:
  // pedestal/YEAR/NAME~~start-DATE~~finish-DATE
  
  // NOTE: pedestal data was recorded with gemMonitor program at CERN-Prevessin EHN1 location,
  //       and 'DATE' field is the local time of timezone "TZ=Europe/Paris"
  //       (see Init_Reconstruction() function)
  
  // construct the path to pedestals directory
  const TString dirname = conddbpath() + "pedestal/" + na64session(time) + "/";
  
  //get list of file from directory
  TSystemDirectory dir(dirname, dirname);
  TList* filelist = dir.GetListOfFiles();

  if (!filelist) {
    std::cout << "ERROR: FindApvPedestalFile() can't list directory " << dirname << std::endl;
    throw std::runtime_error("ERROR: Failed to open directory");
  }

  //loop over file in the directory
  TSystemFile *thisfile;
  TIter itr((TCollection*)filelist);
  while((thisfile=(TSystemFile*)itr()))
  {

    if( thisfile->IsDirectory() ) continue; //skip every subdirectory

    const TString filename = thisfile->GetName();

    if( !filename.BeginsWith(detname+"~~") ) continue; //skip file with different header

    //extract timing from the string
    //TODO: proper search for finish, fix bug
    struct tm tm;
    unsigned int start = filename.Index("start");
    strptime(filename(start, datesize).Data(),              "start-%Y-%m-%d-%T",  &tm);
    tm.tm_isdst = -1; // mktime() will detect daylight saving time
    time_t start_time = mktime(&tm);    
    strptime(filename(start+datesize+2, datesize).Data(),   "finish-%Y-%m-%d-%T", &tm);
    tm.tm_isdst = -1; // mktime() will detect daylight saving time
    time_t end_time = mktime(&tm);
    //cout << "filename=" << filename << " start_time=" << start_time << " end_time=" << end_time << endl;

    //compare timing of the run with pedestal, accept only if the time is inside in the range (boundaries are included)
    if((time >= start_time) && (time <= end_time))
    {
      return (dirname+filename).Data();
    }   
  }

  //no files matching time
  cout << "WARNING: FindApvPedestalFile() no pedestal file matching time" << endl;
  return "";
}

  //calculate time and error
  double MM_time_point::time(const double& q_num,const double& q_den) const
  {
    double ret_t = nan("");
    double q_ratio = q_num/q_den;

    if(parameters.r0 > q_ratio && q_ratio != 0 && q_den > 0) {
      ret_t = parameters.t0+parameters.a0*log((parameters.r0/q_ratio)-1);
    }
    return ret_t;
  }

  double MM_time_point::error(const double& q_num,const double& q_den,const double& sigma_n) const
  {
    double sigma_t = nan("");
    double q_ratio = q_num/q_den;

    if(parameters.r0 > q_ratio && q_ratio != 0 && q_den > 0) {
      //calculate partial derivates for each parameters
      double sigma_r=(sigma_n+(1/sqrt(12)))*((sqrt(pow(q_num,2)+pow(q_den,2)))/pow(q_den,2));
      double dtdr=(parameters.a0*parameters.r0)/(pow(q_ratio,2)*(parameters.r0/q_ratio-1));
      double dtda0=log((parameters.r0/q_ratio)-1);
      double dtdt0=1;
      double dtdr0=parameters.a0/(q_ratio*(parameters.r0/q_ratio-1));
      //calculating final error with gaussian error propagation
      sigma_t=sqrt((pow(dtdr,2)*pow(sigma_r,2))+(pow(dtdr0,2)*errors.r02)+(pow(dtdt0,2)*errors.t02)+(pow(dtda0,2)*errors.a02)+2*(dtdt0*dtdr0*errors.r0t0)+2*(dtda0*dtdr0*errors.r0a0)+2*(dtda0*dtdt0*errors.t0a0));
    }

    return sigma_t;
  }

  // Calculation of t0 using information of both ratios
  // and based on method cluster_time() in mm.h
  // Calculated time using the COMPASS algorithm
  double MM_plane_timing::t0() const
  {
    // Estimate expected time from t0 parameters obtained for the two ratios
    double time_t02 = a02.parameters.t0;
    double time_t12 = a12.parameters.t0;
    double sigma_t02 = a02.error(a02.parameters.r0, 2., 0.);
    double sigma_t12 = a12.error(a12.parameters.r0, 2., 0.);
    double t0_num = (time_t02/pow(sigma_t02,2)) + (time_t12/pow(sigma_t12,2));
    double t0_den = (1/pow(sigma_t02,2)) + (1/pow(sigma_t12,2));
    return (t0_den > 0) ? t0_num/t0_den : nan("");
  }

  // Calculation of t0_err using information of both ratios
  // and based on method cluster_time_err() in mm.h
  // Calculated time error using the COMPASS algorithm
  double MM_plane_timing::t0_err() const
  {
    double sigma_t02 = a02.error(a02.parameters.r0, 2., 0.);
    double sigma_t12 = a12.error(a12.parameters.r0, 2., 0.);
    double t0_err_den = (1/pow(sigma_t02,2)) + (1/pow(sigma_t12,2));
    return (t0_err_den > 0) ? 1/t0_err_den : nan("");
  }

namespace {
// split string into array of int numbers
vector<int> explode(const string& line, const char delimiter)
{
  vector<int> v;
  istringstream iss(line);
  string entry;
  
  while (getline(iss, entry, delimiter)) {
    istringstream convert(entry);
    int x;
    convert >> x;
    v.push_back(x);
  }
  return v;
}
}

  
  //Init mapping using standard Micromegas mapping file, csv, first row channel, the rest are strips
  void MM_plane_calib::Read_Multiplex_mapping(const string filename)
  {
    //open file and check if it is available
    ifstream inFile(filename);
    std::cout << "INFO: Read_Multiplex_mapping() Loading mapping file: " << filename << endl;
    
    //check if file is open
    if(!inFile) {
        throw std::runtime_error("ERROR: Failed to open file");
    }
    
    MM_map.clear();
    
    string line;
    while(getline(inFile,line))
      {
        // skip comments
        if((line.find("#") == 0)) continue;
        
        //read the line
        const vector<int> v = explode(line, ',');
        const int channel = v[0];                        // first entry is the channel
        const vector<int> strips(v.begin()+1, v.end());  // the rest is list of strips
        
        //save in the mapping
        MM_map[channel] = strips;
      }

    //create reverse map
    Create_MM_reverse_map();
    
    // check mapping integrity
    const bool isOK = (MM_map.size() == MM_Nchannels) && (MM_reverse_map.size() == MM_Nstrips);
    if (!isOK) {
        throw std::runtime_error("ERROR: Incorrect mapping data (mismatch with expected number of channels and strips)");
    }
    
    //cout << "MM electronics channels to strips map:" << endl;
    //print_MM_map();
  }
  
  //default constructor
  MM_plane_calib::MM_plane_calib()
  {
    MM_Nchannels           = 0;
    MM_Nstrips             = 0;
    MM_strip_step          = 0;
    MM_min_cluster_size    = 0;
    
    timing.a02 = {{0,0,0}, {0,0,0,0,0,0}};
    timing.a12 = {{0,0,0}, {0,0,0,0,0,0}};

    time = nan("");
    time_err = nan("");
  }
  
  void MM_plane_calib::SetDesign2016()
  {
    // structure
    MM_Nchannels           = 64;
    MM_Nstrips             = 320;
    MM_strip_step          = 0.25;
    
    // reco options
    MM_min_cluster_size    = 2;
    
    // pedestals sigma
    sigma.assign(MM_Nchannels, 0.);
    
    // mapping
    Read_Multiplex_mapping(conddbpath() + "multiplex_map/multiplexing_p64_m5.csv");
  }
  
  void MM_plane_calib::SetDesign2021Small()
  {
    // structure
    MM_Nchannels           = 64;
    MM_Nstrips             = 320;
    MM_strip_step          = 0.25;
    
    // reco options
    MM_min_cluster_size    = 2;
    
    // pedestals sigma
    sigma.assign(MM_Nchannels, 0.);
    
    // mapping
    Read_Multiplex_mapping(conddbpath() + "multiplex_map/multiplexing_p64_m5_new.csv");
  }

  void MM_plane_calib::SetDesign2021Large()
  {
    // structure
    MM_Nchannels           = 192;
    MM_Nstrips             = 960;
    MM_strip_step          = 0.25;
    
    // reco options
    MM_min_cluster_size    = 2;
    
    // pedestals sigma
    sigma.assign(MM_Nchannels, 0.);
    
    // mapping
    Read_Multiplex_mapping(conddbpath() + "multiplex_map/multiplexing_p192_m5.csv");
  }

  void MM_plane_calib::ReadTimingCalib(const string filename)
  {
    ifstream inFile(filename.c_str());
    std::cout << "INFO: ReadTimingCalib() filename=" << filename << endl;
    
    //check if file is open
    if (!inFile)
      throw std::runtime_error("Timing file - cannot open file");
    
    string tag;
    getline(inFile,tag);
    istringstream instring (tag);
    //taking all timing parameter
    instring >> timing.a02.parameters.r0 >> timing.a02.parameters.t0 >> timing.a02.parameters.a0
             >> timing.a12.parameters.r0 >> timing.a12.parameters.t0 >> timing.a12.parameters.a0
             >> timing.a02.errors.r02 >> timing.a02.errors.t02 >> timing.a02.errors.a02
             >> timing.a02.errors.r0t0 >> timing.a02.errors.r0a0 >> timing.a02.errors.t0a0
             >> timing.a12.errors.r02 >> timing.a12.errors.t02 >> timing.a12.errors.a02
             >> timing.a12.errors.r0t0 >> timing.a12.errors.r0a0 >> timing.a12.errors.t0a0;

    //error catching
    if (inFile.fail())
      throw std::runtime_error("Timing file IO or format error");
    time = timing.t0();
    time_err = timing.t0_err();
    /*
    std::cout << "INFO: Obtained from filename: " << filename << std::endl;
    std::cout << "INFO: T0 = " << timing.t0() << " -- SIGMA_T0 = " << timing.t0_err() << std::endl;
    */
  } //end ReadTimingCalib

  bool MM_plane_calib::hasTime() const
  {
    return !std::isnan(time);
  }

  void MM_plane_calib::insertBadWire(unsigned int badWire)
  {
    if (badWire >= MM_Nchannels)
      throw std::runtime_error("ERROR: MM_plane_calib::insertBadWire() incorrect wire number.");

    /*std::cout << "INFO: Bad wire tagged: " << badWire << std::endl;*/

    for(const auto& badBin : MM_map.at(badWire)) {
      badStrips.insert(badBin);
      /*std::cout << "INFO: Inserting bad strip: " << badBin << std::endl;*/
    }
  }

  ostream& operator<<(ostream& os,const MM_plane_calib& e)
  {
    os << "PEDESTALS: " << endl;
     for(unsigned int i(0);i<e.MM_Nchannels;++i)
      {
        os << "channel Nr." << i << " has pedestal: " << e.sigma[i] << endl;
      }
     
     os << "TIMING CONSTANT: " << endl;
     os << "A02: " << endl;
     os << "PARAMETERS: " << "r0: " << e.timing.a02.parameters.r0 << " t0: " << e.timing.a02.parameters.t0 << " a0: " << e.timing.a02.parameters.a0 <<  endl;
     os << "ERRORS: " << "r02: " << e.timing.a02.errors.r02 << " t02: " << e.timing.a02.errors.t02 << " a02 " << e.timing.a02.errors.a02 << " r0t0: " << e.timing.a02.errors.r0t0 << " r0a0: " << e.timing.a02.errors.r0a0 << " t0a0: " << e.timing.a02.errors.t0a0 << endl;
     
     os << "A12: " << endl;
     os << "PARAMETERS: " << "r0: " << e.timing.a12.parameters.r0 << " t0: " << e.timing.a12.parameters.t0 << " a0: " << e.timing.a12.parameters.a0 <<  endl;
     os << "ERRORS: " << "r02: " << e.timing.a12.errors.r02 << " t02: " << e.timing.a12.errors.t02 << " a02: " << e.timing.a12.errors.a02 << " r0t0: " << e.timing.a12.errors.r0t0 << " r0a0: " << e.timing.a12.errors.r0a0 << " t0a0: " << e.timing.a12.errors.t0a0 << endl;
     
     return os;
  }
  
  void MM_calib::SetDesign2016()
  {
    xcalib.SetDesign2016();
    ycalib.SetDesign2016();
  }
  
  void MM_calib::SetDesign2021Small()
  {
    xcalib.SetDesign2021Small();
    ycalib.SetDesign2021Small();
  }
  
  void MM_calib::SetDesign2021Large()
  {
    xcalib.SetDesign2021Large();
    ycalib.SetDesign2021Small();
  }
  
  void MM_calib::ReadSigmaCalib(const string filename)
  {
    std::cout << "INFO: ReadSigmaCalib() detname=" << name << " filename=" << filename << std::endl;
    
    ifstream inFile(filename.c_str());
    
    // check if file is open
    if(!inFile) {
      throw std::runtime_error("ERROR: failed to open file");
    }

    unsigned int counter = 0;
    string tag;
    while(getline(inFile,tag))
      {
        // skip comments
        if((tag.find("#") == 0)) continue;
        
        // read calibration data
        //TODO: make it flexible to a larger number of multiplexing
        double dummy;
        double sigma;
        istringstream instring (tag);
        instring >> dummy >> dummy >> sigma >> dummy >> dummy;
        
        // Small MM -> one chip  -> 128 entries in pedestal file
        // Large MM -> two chips -> 256 entries in pedestal file 
        if(counter<128) // first chip contains both x and y
        {
          //calibration of x plane
          if(counter%2 == 0)
          {
            xcalib.sigma[counter/2] = sigma;
          }
          //calibration of y plane
          else
          {
            ycalib.sigma[counter/2] = sigma;
          }
        } else { // second chip contains only x
          // very important to check that this is always the case!
          //calibration of x plane (64-127)
          if(counter%2 == 0)
          {
            xcalib.sigma[(191-(counter/2))] = sigma;
          }
          //calibration of x plane (128-191)
          else
          {
            xcalib.sigma[(255-(counter/2))] = sigma;
          }
        }         

        // check parsing errors
        if (!instring) {
          throw std::runtime_error("ERROR: ReadSigmaCalib(): fail to parse the input line: " + tag);
        }
        
        ++counter;
      }
    
    if (counter != 128 && counter != 256) {
      throw std::runtime_error("ERROR: ReadSigmaCalib(): insufficient data in the pedestal file, expected 128 channels.");
    }
  }

 void MM_calib::ReadSigmaCalib(const time_t time)
 {
   // construct the path to pedestals directory
   const string session = na64session(time);
   const string dirname = conddbpath() + "pedestal/" + session + "/";

   // detname substring in filename
   string detname = name;
   const bool isNameMX = !(session == "2016" || session == "2022mu");
   if (isNameMX) detname[1] = 'X'; //second letter is an X

   // search file for the timestamp
   string filename = FindApvPedestalFile(time, detname);

   //Apply calibration
   ReadSigmaCalib(filename);
  
 }//end ReadSigmaCalib

 bool MM_calib::hasTime() const
 {
   return xcalib.hasTime() && ycalib.hasTime();
 }

ostream& operator<<(ostream& os,const MM_calib& e)
{
  os << "MM has name: " << e.name << endl;
  os << "CALIBRATION OF X PLANE: "<< endl;
  os << e.xcalib << endl;
  os << "CALIBRATION OF Y PLANE: "<< endl;
  os << e.ycalib << endl;
  return os;
}


//declare calibration
MM_calib MM1_calib;
MM_calib MM2_calib;
MM_calib MM3_calib;
MM_calib MM4_calib;
MM_calib MM5_calib;
MM_calib MM6_calib;
MM_calib MM7_calib;
MM_calib MM8_calib;
MM_calib MM9_calib;
MM_calib MM10_calib;
MM_calib MM11_calib;
MM_calib MM12_calib;
MM_calib MM13_calib;
//dummy calibration
MM_calib MM_nocalibration;

ostream& operator<<(ostream& os, const APV_channel_id& c)
{
  os << "srcID=" << c.srcID
     << " adcID=" << c.adcID
     << " chipID=" << c.chipID
     << " chipchannel=" << c.chipchannel;
  return os;
}

bool operator<(const APV_channel_id& left, const APV_channel_id& right)
{
  if (left.srcID != right.srcID) return (left.srcID < right.srcID);
  if (left.adcID != right.adcID) return (left.adcID < right.adcID);
  if (left.chipID != right.chipID) return (left.chipID < right.chipID);
  return (left.chipchannel < right.chipchannel);
}

// lookup calibration data for given electronics channel
APV_channel_calib GEM_calib::FindGEMPedestal(const APV_channel_id& id) const
{
  //cout << "FindGEMPedestal() detname=" << name << ", datasize: " << data.size() << " ID:" << id << endl;
  return data.at(id);

  // TODO: issue readable message in case of missing channel
  //cout << "no valid combination was found, returning" << endl;
  //cout << "correct chip combination was found for: " << Chipchannel << endl;
  //cout << "pedestal was: " << c.pedestalamp << " " << c.pedestalsigma << endl;
}

void GEM_calib::load(const time_t time)
{
  //find correct pedestal file
  const string fx = FindApvPedestalFile(time, name+"X1__");
  const string fy = FindApvPedestalFile(time, name+"Y1__");
  //load it
  load(fx.c_str(), fy.c_str());
} 

void GEM_calib::load(const char* fx, const char* fy)
{
  Read_APV_Pedestals(fx);
  Read_APV_Pedestals(fy);
}

// load APV pedestals data file
// origin - gemMonitor / gemAnalyze.cc / function readPedestalfile()
//   https://gitlab.cern.ch/compass-gem/gemMonitor/blob/master/src/gemAnalyze.cc#L71
//
// example file format:
//
//#ChipAPV 0 621 3 14
//1 757.851 5.67425 0 0
//...
//#ChipAPV 0 621 3 15
//1 745.341 5.99112 0 0
//..
void GEM_calib::Read_APV_Pedestals(const char* fname)
{
  //cout << "INFO: Read_APV_Pedestals() detname=" << name << " fname=" << fname << endl;

  std::ifstream infile(fname);

  if (!infile) {
    std::cerr << "ERROR: Read_APV_Pedestals(): can't open calibration data file " << fname << "\n";
    throw std::runtime_error("ERROR: Read_APV_Pedestals(): can't open calibration data file");
  }

  string line;
  APV_channel_id id;

  while (getline(infile, line)) {
    istringstream iss(line);

    // start of new chip, read ID data
    if (line.find("#ChipAPV ") == 0) {
      string header;
      int dummy;

      iss >> header >> dummy >> id.srcID >> id.adcID >> id.chipID;
      id.chipchannel = 0;
      continue;
    }
  
    // read electronics channel calibration data
    else {
      APV_channel_calib c;

      iss >> c.channelflag
          >> c.pedestalamp
          >> c.pedestalsigma
          >> c.calibrationamp
          >> c.calibrationsigma;

      data[id] = c;
      id.chipchannel++;
    }
    //cout << "ID: " << id << endl;
  
    // check parsing errors
    if (!iss) {
      throw std::runtime_error("ERROR: Read_APV_Pedestals(): fail to parse the input line: " + line);
    }
  }
  //cout << "INFO: nentries=" << data.size() << endl;
}

GEM_calib GM01_calib;
GEM_calib GM02_calib;
GEM_calib GM03_calib;
GEM_calib GM04_calib;

/*
 * Straw Tubes calibration struct
 *
 * In these objects the parameters needed for reconstruction are initialized and used by the Straw
 * struct. Straw calibration should be initialized with the appropiate design (large UV, small XY).
 */

// r(t) function for 6mm straw tubes
double rtStraw6(int time)
{
  if (time > 0)      return  1.0/11.23*(pow((double)time, 1.0/1.203));
  else  if (time < 0)    return  -1.0/11.23*(pow((double)(-time), 1.0/1.203));
  else return 0.0;
}

// Inverse r(t) function for 6mm straw tubes
int invRtStraw6(double pos)
{
  double time = pow(11.23*pos, 1.203);
  return (int)(round(time));
}

  // Small XY
  void ST_calib::InitXY6Design(uint _t0)
  {
    t0 = _t0;
    // maximum drift time for 3mm is ~70 ns
    timeWindow = 70.;
    stereoAngle = 0.;
  }

  // Large UV
  void ST_calib::InitUV6Design(uint _t0)
  {
    t0 = _t0;
    // maximum drift time for 3mm is ~70 ns
    timeWindow = 70.;
    stereoAngle = M_PI*7.0/180.0;
  }

//declare calibration
ST_calib ST01_calib;
ST_calib ST02_calib;
ST_calib ST03_calib;
ST_calib ST04_calib;
ST_calib ST05_calib;
ST_calib ST06_calib;
ST_calib ST11_calib;
ST_calib ST12_calib;
ST_calib ST13_calib;
//dummy calibration
ST_calib ST_nocalibration;


//geometry of the setup summarized

bool operator==(const CellXY& a, const CellXY& b)
{
  return (a.ix == b.ix) && (a.iy == b.iy);
}

DetGeo ECAL_pos;
DetGeo HCAL_pos;
DetGeo S2_pos;
DetGeo ST2_pos;
DetGeo SRD_pos;
DetGeo S1_pos;
DetGeo pipe_pos;
DetGeo MAGD_pos;
DetGeo MAGU_pos;
DetGeo Veto_pos;
DetGeo S0_pos;
DetGeo ST1_pos;
//MicroMegas
DetGeo MM1_pos;
DetGeo MM2_pos;
DetGeo MM3_pos;
DetGeo MM4_pos;
DetGeo MM5_pos;
DetGeo MM6_pos;
DetGeo MM7_pos;
DetGeo MM8_pos;
DetGeo MM9_pos;
DetGeo MM10_pos;
DetGeo MM11_pos;
DetGeo MM12_pos;
DetGeo MM13_pos;

//GEM
DetGeo GM1_pos;
DetGeo GM2_pos;
DetGeo GM3_pos;
DetGeo GM4_pos;
GEMGeometry GM01Geometry;
GEMGeometry GM02Geometry;
GEMGeometry GM03Geometry;
GEMGeometry GM04Geometry;

GEMGeometry ST01Geometry;
GEMGeometry ST02Geometry;
GEMGeometry ST03Geometry;
GEMGeometry ST04Geometry;
GEMGeometry ST05Geometry;
GEMGeometry ST06Geometry;
GEMGeometry ST11Geometry;
GEMGeometry ST12Geometry;
GEMGeometry ST13Geometry;

// magnetic field
double MAG_field; // Tesla
double NominalBeamEnergy;
int NominalBeamPDG;
TVector3 ECAL0BEAMSPOT_pos;
CellXY ECAL0BEAMCELL;

// tracking info
TrackingInfo geo;

void Reset_Geometry()
{
  const DetGeo none;
  ECAL_pos = none;
  HCAL_pos = none;
  S2_pos   = none;
  GM2_pos  = none;
  ST2_pos  = none;
  GM1_pos  = none;
  SRD_pos  = none;
  S1_pos   = none;
  pipe_pos = none;
  MAGD_pos = none;
  MAGU_pos = none;
  Veto_pos = none;
  S0_pos   = none;
  ST1_pos  = none;
  MM1_pos = none;
  MM2_pos = none;
  MM3_pos = none;
  MM4_pos = none;
  MM5_pos = none;
  MM6_pos = none;
  MM7_pos = none;
  MM8_pos = none;
  MM9_pos = none;
  MM10_pos = none;
  MM11_pos = none;
  MM12_pos = none;
  MM13_pos = none;
  MAG_field = 0;
  
  // TODO: set default to 0. and explicitly define actual beam energy
  //       in corresponding Init_Geometry_YYYY() functions
  NominalBeamEnergy = 100.;
  NominalBeamPDG = 11;

  // TODO: set default to 0 / 0
  ECAL_pos.Nx = 6;
  ECAL_pos.Ny = 6;
  HCAL_pos.Nx = 3;
  HCAL_pos.Ny = 3;
  
  const GEMGeometry gmnone = {0, 0, 0, 0, TVector3()};
  GM01Geometry = gmnone;
  GM02Geometry = gmnone;
  GM03Geometry = gmnone;
  GM04Geometry = gmnone;
  
  ST01Geometry = gmnone;
  ST02Geometry = gmnone;
  ST03Geometry = gmnone;
  ST04Geometry = gmnone;
  ST05Geometry = gmnone;
  ST06Geometry = gmnone;
  ST11Geometry = gmnone;
  ST12Geometry = gmnone;
  ST13Geometry = gmnone;
  
  // coordinate of the beam central cell center
  // https://gitlab.cern.ch/P348/p348-daq/-/issues/52#note_6580473
  ECAL0BEAMSPOT_pos = TVector3();
  
  // TODO: set default to {-1,-1} "undefined"
  // TODO: verify actual cental cell in Init_Geometry_YYYY() functions
  // index of the beam central cell:
  //   2015-2016A  ?
  //   2016B-2018  {3,3}
  //   2021A     {2,2}
  //   2021B     {2,3}
  //   2022A     {2,2}
  //   2022B     {2,3}
  ECAL0BEAMCELL = {3, 3};
}

TVector3 CCellECAL(int cellx, int celly, double cellsize)
{
  double ECALX = ECAL_pos.pos.X();
  double ECALY = ECAL_pos.pos.Y(); // For this, add setting Y in simu.h
  double ECALWidth = cellsize * (double)(ECAL_pos.Nx);
  double ECALHeight = cellsize * (double)(ECAL_pos.Ny);
  double ECALBegX = ECALX - 0.5*ECALWidth;
  double ECALBegY = ECALY - 0.5*ECALHeight;
  double Xcell = ECALBegX + ((double)cellx + 0.5) * cellsize;
  double Ycell = ECALBegY + ((double)celly + 0.5) * cellsize;
  return TVector3(Xcell, Ycell, 0);
}

//detname is currently not used
void Init_ADC_channel(CaloCalibData &calib, bool isWB) {
  if (isWB) {
    calib.adc=wb_ADCdata;
  } else {
    calib.adc=sadc_ADCdata;
  }
}


void Init_ADC(const CS::DaqEventsManager& manager){
  const CS::Chip::Maps & maps = manager.GetMaps();
  for( CS::Chip::Maps::const_iterator it=maps.begin(); it!=maps.end(); ++it ) {

    const CS::ChipSADC::Digit *d_sadc=dynamic_cast<const CS::ChipSADC::Digit *>(it->second);
    const CS::ChipNA64WaveBoard::Digit *d_wb=dynamic_cast<const CS::ChipNA64WaveBoard::Digit *>(it->second);

    //skip if it not a SADC or a WB
    if ((d_sadc==0)&&(d_wb==0))
      continue;

    const bool isWB=(d_sadc==0);
    const std::string &detname = (isWB) ? d_wb->GetDetID().GetName() : d_sadc->GetDetID().GetName();
    const int x = (isWB) ? d_wb->GetX() : d_sadc->GetX();
    const int y = (isWB) ? d_wb->GetY() : d_sadc->GetY();
    //here set the ADC parameters
    if (detname=="TRIG"){
      continue;
    }
    if (detname=="ECAL0") {
      Init_ADC_channel(ECAL_calibration[0][x][y],isWB);
    } else if (detname=="ECAL1") {
      Init_ADC_channel(ECAL_calibration[1][x][y],isWB);
    }
    else if (detname=="PKRCAL0") {
      Init_ADC_channel(PKRCAL0_calibration[x][y],isWB);
    } else if (detname=="PKRCAL1") {
      Init_ADC_channel(PKRCAL1_calibration[x][y],isWB);
    }
    else if (detname=="ECALSUM"){
      Init_ADC_channel(ECALSUM_calibration[x], isWB);
    }


    else if (detname=="HCAL0") {
      Init_ADC_channel(HCAL_calibration[0][x][y], isWB);
    } else if (detname=="HCAL1") {
      Init_ADC_channel(HCAL_calibration[1][x][y], isWB);
    } else if (detname=="HCAL2") {
      Init_ADC_channel(HCAL_calibration[2][x][y], isWB);
    } else if (detname=="HCAL3") {
      Init_ADC_channel(HCAL_calibration[3][x][y], isWB);
    }

    else if (detname=="SRD") {
      Init_ADC_channel(SRD_calibration[x], isWB);
    } else if (detname=="LYSO") {
      Init_ADC_channel(LYSO_calibration[x], isWB);
    } else if (detname=="BGO") {
      Init_ADC_channel(BGO_calibration[x], isWB);
    }

    else if (detname=="VHCAL0") {
      Init_ADC_channel(VHCAL_calibration[0][x][y], isWB);
    }
    else if (detname=="VHCAL1") {
      Init_ADC_channel(VHCAL_calibration[1][x][y], isWB);
    }

    // muon
    else if (detname == "MUON0" || detname == "MUON1" || detname == "MUON2" || detname == "MUON3") {
      const int z = cast<int>(detname.substr(4, 1));
      Init_ADC_channel(MUON_calibration[z],isWB);
    }

    // Live Target
    else if (detname == "LTG") {
      Init_ADC_channel(LTG_calibration[x], isWB);
    }

    // Cherenkov counters
    else if (detname == "CHCNT") {
      Init_ADC_channel(CHCNT_calibration[x], isWB);
    }

    // hodoscope
    else if (detname == "HOD0X" || detname == "HOD1X" || detname == "HOD0Y" || detname == "HOD1Y" || detname == "HOD2X" || detname == "HOD2Y") {
      const int z = cast<int>(detname.substr(3, 1));
      const bool isXplane = (detname.substr(4, 1) == "X");
      const int p = isXplane ? 0 : 1;
      Init_ADC_channel(HOD_calibration[z][p][y],isWB);
    }

    else if (detname=="VETO") {
      Init_ADC_channel(VETO_calibration[x], isWB);
    }

    else if (detname == "ATARG") { //no ATARG calibrations??
      void();
    }

    // beam counters
    else if (detname == "S0") {
      Init_ADC_channel(S0_calibration, isWB);
    }
    else if (detname == "S1") {
      Init_ADC_channel(S1_calibration, isWB);
    } else if (detname == "S2") {
      Init_ADC_channel(S2_calibration, isWB);
    }
    else if (detname == "T2") {
      Init_ADC_channel(T2_calibration, isWB);
    } else if (detname == "S3") {
      Init_ADC_channel(S3_calibration, isWB);
    } else if (detname == "S4") {
      Init_ADC_channel(S4_calibration, isWB);
    } else if (detname == "V1") {
      Init_ADC_channel(V1_calibration, isWB);
    } else if (detname == "V2") {
      Init_ADC_channel(V2_calibration, isWB);
    } else if (detname == "ZC") {
      void();
      //              Init_ADC_channel(ZC_calibration,"ZC",isWB);
    } else if (detname == "Smu") {
      Init_ADC_channel(Smu_calibration, isWB);
    } else if (detname == "BeamK") {
      Init_ADC_channel(BeamK_calibration, isWB);
    }

    // visible mode detectors
    else if (detname=="WCAT"){
      Init_ADC_channel(WCAT_calibration, isWB);
    }
    else if (detname=="WCAL"){
      Init_ADC_channel(WCAL_calibration[x], isWB);
    }
    else if (detname=="VTEC"){
      Init_ADC_channel(VTEC_calibration, isWB);
    }
    else if (detname=="VTWC"){
      Init_ADC_channel(VTWC_calibration, isWB);
    }
    else if (detname=="WBTRIG"){
      Init_ADC_channel(WBTRIG_calibration, isWB);
    }
    else{
      cout<<"conddb.cc, Init_ADC, det name: "<<detname<<" NOT RECOGNIZED. Exit "<<endl;
      exit(1);
    }
  }
}





// peak time (t0) search method:
//  kT0_MaxSample   : index of sample with maximum amplitude
//  kT0_CenterOfMass: waveform center of mass index
//  kT0_RisingEdge  : index of waveform rising edge - starting point position
//  kT0_RisingEdgeHH  : index of waveform rising edge - half height point position
int SADC_t0_method = kT0_RisingEdgeHH;

// best peak search method:
//  kPeak_MaxAmplitude: 
//  kPeak_BestTiming: 
const int SADC_peak_method = kPeak_BestTiming;

// SADC corrections method:
//  - no corrections
//  - manual corrections
//  - LED corrections
int SADC_correction_method = kAmplitude_LED;

// trigger amplitude threshold (ADC counts)
const int TRIG_raw_threshold = 500;

// hodoscope amplitude threshold
const double HODO_threshold = 10.;
// Hodoscope (chile) threshold 
const double HODO2_threshold = 50.;

// MeV to GeV convertion
const double MeV = 0.001;

void Reset_Reco_Options()
{
  // the default t0 calculation method for reco of 2015-2017 sessions
  SADC_t0_method = kT0_RisingEdge;
  
  //default values for WB and SADC parameters
  wb_ADCdata.isWB=1;
  wb_ADCdata.pedestal_length=20;
  wb_ADCdata.signal_range_begin = 40;
  wb_ADCdata.signal_range_end = 100;
  wb_ADCdata.sampling_interval=4.;
  wb_ADCdata.maxADC=16383; // WB is 14 bits, max value is 16383, hex 0x3FFF
  wb_ADCdata.pulse_shape_decay=999; //TODO

  sadc_ADCdata.isWB=0;
  sadc_ADCdata.pedestal_length=8;
  sadc_ADCdata.signal_range_begin=10;   // the default SADC signal zone of 2015-2024 sessions
  sadc_ADCdata.signal_range_end=24;
  sadc_ADCdata.sampling_interval=12.5;//ns
  sadc_ADCdata.maxADC=4095; // MSADC is 12 bit, max value is 4095 (hex 0xFFF)
  sadc_ADCdata.pulse_shape_decay=2.2; // by V. Samoylenko, units=12.5ns
}
  
  float CaloCalibData::texpected(const float master) const
  {
    if (isTmeanRelative) return (master + tmean);
    
    return tmean;
  }
  
  string CaloCalibData::detname() const
  {
    const size_t pos = name.find("-");
    return (pos != string::npos) ? name.substr(0, pos) : name;
  }
  
  CaloCalibData::CaloCalibData(const std::string &n): name(n), K(1.), tmean(0.), tsigma(10000.), isTmeanRelative(true),
    doWaveformFit(false),
    minPeakRaisingSlopeSpeed(-1),adc(sadc_ADCdata)
  {}

ostream& operator<<(ostream& os, const CaloCalibData& c)
{
  os << "name=" << c.name
     << " detname()=" << c.detname()
     << " K=" << c.K
     << " hasTcalib()=" << c.hasTcalib()
     << " tmean=" << c.tmean << "ns (" << (c.isTmeanRelative ? "relative" : "absolute") << ")"
     << " tsigma=" << c.tsigma << "ns";
  
  return os;
}

// Actual ECAL, HCAL size:
//   before 2021mu: ECAL=2* 6x6, HCAL=4* 3x3
//   from 2021mu:   ECAL=2* 5x6, HCAL=2* 6x3

CaloCalibData BGO_calibration[8];
CaloCalibData SRD_calibration[4];
CaloCalibData LYSO_calibration[100];
CaloCalibData ECAL_calibration[2][12][6];
CaloCalibData HCAL_calibration[4][6][3];
CaloCalibData VHCAL_calibration[2][4][4];
CaloCalibData PKRCAL0_calibration[6][12];
CaloCalibData PKRCAL1_calibration[12][12];
CaloCalibData VETO_calibration[6];
CaloCalibData WCAL_calibration[3];
CaloCalibData WCAT_calibration;
CaloCalibData VTWC_calibration;
CaloCalibData VTEC_calibration;
CaloCalibData S0_calibration;
CaloCalibData S1_calibration;
CaloCalibData S2_calibration;
CaloCalibData S3_calibration;
CaloCalibData S4_calibration;
CaloCalibData T2_calibration;
CaloCalibData V1_calibration;
CaloCalibData V2_calibration;
CaloCalibData Smu_calibration;
CaloCalibData WBTRIG_calibration;
CaloCalibData BeamK_calibration;
CaloCalibData LTG_calibration[6];
CaloCalibData CHCNT_calibration[2];
CaloCalibData MUON_calibration[4];
CaloCalibData ECALSUM_calibration[6];
CaloCalibData HOD_calibration[3][2][16]; // indeces: [z index][0=x/1=y plane][cell id]
CaloCalibData CALO_nocalibration;
CaloCalibData::ADCdata_t wb_ADCdata;
CaloCalibData::ADCdata_t sadc_ADCdata;

// reset calibration coefficients
void Reset_Calib()
{
  typedef CaloCalibData none;
  
  CALO_nocalibration = none();
  
  for (int x = 0; x < 8; ++x) {
    std::ostringstream oss;
    oss << "BGO-" << x;
    BGO_calibration[x] = none(oss.str());
  }
  
  for (int x = 0; x < 4; ++x) {
    std::ostringstream oss;
    oss << "SRD-" << x;
    SRD_calibration[x] = none(oss.str());
  }
  
  for (int x = 0; x < 6; ++x) {
    std::ostringstream oss;
    oss << "LYSO-" << x;
    LYSO_calibration[x] = none(oss.str());
  }

  for (int i = 0; i < 2; ++i) {
    for (int x = 0; x < 12; ++x) {
      for (int y = 0; y < 6; ++y) {
        std::ostringstream oss;
        oss << "ECAL" << i << "-" << x << "-" << y;
        ECAL_calibration[i][x][y] = none(oss.str());
      }
    }
  }
 
  for (int x = 0; x < 6; ++x) {
    for (int y = 0; y < 12; ++y) {
      std::ostringstream oss;
      oss << "PKRCAL0-" << x << "-" << y;
      PKRCAL0_calibration[x][y] = none(oss.str());
    }
  }

  for (int x = 0; x < 12; ++x) {
    for (int y = 0; y < 12; ++y) {
      std::ostringstream oss;
      oss << "PKRCAL1-" << x << "-" << y;
      PKRCAL1_calibration[x][y] = none(oss.str());
    }
  }
  
  for (int i = 0; i < 4; ++i) {
    for (int x = 0; x < 6; ++x) {
      for (int y = 0; y < 3; ++y) {
        std::ostringstream oss;
        oss << "HCAL" << i << "-" << x << "-" << y;
        HCAL_calibration[i][x][y] = none(oss.str());
      }
    }
  }

  for (int i = 0; i < 2; ++i) {
    for (int x = 0; x < 4; ++x) {
      for (int y = 0; y < 4; ++y) {
        std::ostringstream oss;
        oss << "VHCAL" << i << "-" << x << "-" << y;
        VHCAL_calibration[i][x][y] = none(oss.str());
      }
    }
  }

  for (int i = 0; i < 6; ++i) {
    std::ostringstream oss;
    oss << "VETO-" << i;
    VETO_calibration[i] = none(oss.str());
  }

  for (int i = 0; i < 3; ++i) {
    std::ostringstream oss;
    oss << "WCAL-" << i;
    WCAL_calibration[i] = none(oss.str());
  }
  
  WCAT_calibration = none("WCAT");
  
  VTWC_calibration = none("VTWC");

  VTEC_calibration = none("VTEC");

  S0_calibration = none("S0");
  S1_calibration = none("S1");
  S2_calibration = none("S2");
  S3_calibration = none("S3");
  S4_calibration = none("S4");
  T2_calibration = none("T2");
  V1_calibration = none("V1");
  V2_calibration = none("V2");
  Smu_calibration = none("Smu");
  BeamK_calibration = none("BeamK");
  
  WBTRIG_calibration = none("WBTRIG");

  for (int i = 0; i < 6; ++i) {
    std::ostringstream oss;
    oss << "LTG-" << i;
    LTG_calibration[i] = none(oss.str());
  }

  for (int i = 0; i < 2; ++i) {
    std::ostringstream oss;
    oss << "CHCNT-" << i;
    CHCNT_calibration[i] = none(oss.str());
  }
  
  for (int i = 0; i < 4; ++i) {
    std::ostringstream oss;
    oss << "MUON" << i;
    MUON_calibration[i] = none(oss.str());
  }
  
  for (int i = 0; i < 6; ++i) {
    std::ostringstream oss;
    oss << "ECALSUM-" << i;
    ECALSUM_calibration[i] = none(oss.str());
  }
  
  for (int z = 0; z < 3; ++z)
    for (int p = 0; p < 2; ++p)
      for (int i = 0; i < 16; ++i) {
        // only HOD2 has 16 cells
        if ((z != 2) && (i == 15)) continue;
        
        const bool isX = (p == 0);
        std::ostringstream oss;
        oss << "HOD" << z << (isX ? "X" : "Y") << "-" << i;
        HOD_calibration[z][p][i] = none(oss.str());
      }
  
  //reset MM calib
  const MM_calib mmnone;
  MM_nocalibration = mmnone;
  
  MM1_calib=mmnone;
  MM2_calib=mmnone;
  MM3_calib=mmnone;
  MM4_calib=mmnone;
  MM5_calib=mmnone;
  MM6_calib=mmnone;
  MM7_calib=mmnone;
  MM8_calib=mmnone;
  MM9_calib=mmnone;
  MM10_calib=mmnone;
  MM11_calib=mmnone;
  MM12_calib=mmnone;
  MM13_calib=mmnone;

  //set MM names
  MM1_calib.name = "MM01";
  MM2_calib.name = "MM02";
  MM3_calib.name = "MM03";
  MM4_calib.name = "MM04";
  MM5_calib.name = "MM05";
  MM6_calib.name = "MM06";
  MM7_calib.name = "MM07";
  MM8_calib.name = "MM08";  
  MM9_calib.name = "MM09";  
  MM10_calib.name = "MM10";  
  MM11_calib.name = "MM11";  
  MM12_calib.name = "MM12";   
  MM13_calib.name = "MM13";
  
  // reset GEM calibrations
  const GEM_calib gemnone;
  GM01_calib = gemnone;
  GM02_calib = gemnone;
  GM03_calib = gemnone;
  GM04_calib = gemnone;
  
  //assign name
  GM01_calib.name = "GM01";
  GM02_calib.name = "GM02";
  GM03_calib.name = "GM03";
  GM04_calib.name = "GM04";

  //reset ST calib
  const ST_calib stnone;
  ST_nocalibration = stnone;

  ST01_calib=stnone;
  ST02_calib=stnone;
  ST03_calib=stnone;
  ST04_calib=stnone;
  ST05_calib=stnone;
  ST06_calib=stnone;
  ST11_calib=stnone;
  ST12_calib=stnone;
  ST13_calib=stnone;

  //set ST names
  ST01_calib.name = "ST01";
  ST02_calib.name = "ST02";
  ST03_calib.name = "ST03";
  ST04_calib.name = "ST04";
  ST05_calib.name = "ST05";
  ST06_calib.name = "ST06";
  ST11_calib.name = "ST11";
  ST12_calib.name = "ST12";
  ST13_calib.name = "ST13";

  // reset geometry
  Reset_Geometry();
  
  // reset reco options
  Reset_Reco_Options();
}

// === calibration data for 2015 runs ===
// init calibration coefficients
// calibration data received from alexander.toropin@cern.ch
void Init_Calib_2015_BGO_ECAL_HCAL()
{
  // BGO
  const float calbgo[8] = {820, 1142, 1088, 1066, 483, 1317, 1316, 1161};
  
  for (int i = 0; i < 8; i++)
    BGO_calibration[i].K = 0.064 / calbgo[i]; // in GeV
  
  
  // ECAL
      // arrays for calibration (Ecal)
      const int Ndim = 36;

      //float c0maxEC[2] = {3.9, 82.9};
      float c0maxEC[2] = {3.04, 61.2};

      // calibration from run 411
      //float c0EC[2][Ndim] = {
      // Ecal plane 0:
      //        {1028,  378,  806, 1834,  722, 1498,
      //         624,  646,  896,  725,  459,  767,
      //         1126,  482,  780, 1013,  977, 1855,
      //         1741, 1788, 1105,  763, 1575,  469,
      //         501,  346, 1418,  883,  944, 1547,
      //         731,  998, 1279, 1037,  726,  946},
      // Ecal plane 1:        
      //        {1874, 1528, 586, 1397, 2304, 3502,
      //         2219, 2280, 1263, 894, 1008, 3106,
      //         770, 2028, 2859, 795, 2344, 1311,
      //         0,  2627, 2866, 2638, 2621, 1423,
      //         2560, 2821, 1391, 1383, 972, 864,
      //         2483, 1237, 794, 1513, 1356, 976}
      //      };
        
      // calibration from run 439
      //{0,0 0,1 0,2 0,3 0,4 0,5
      // 1,0 1,1 1,2 1,3 1,4 1,5
      // .......................
      //float c0EC[2][Ndim] = {
      //        // Ecal plane 0:
      //        {2053, 1838, 2074, 1749, 1850, 1908,
      //         1889, 1981, 2191, 1779, 2500, 1980,
      //         1906, 1597, 2373, 2047, 1574, 1949,
      //         2053, 1587, 1822, 1730, 1826, 1079,
      //         1641, 2202, 2256, 1410, 1622, 1983,
      //         1696, 1130, 2096, 1315, 1622, 1757},
      //        // Ecal plane 1:
      //        {2698, 2874, 3377, 2430, 2330, 3190,
      //         2564, 2510, 2743, 2730, 2314, 1337,
      //         2110, 2774, 2344, 2639, 2178, 2863,
      //         0,    2891, 2685, 2678, 3127, 2790,
      //         2646, 2918, 2584, 2813, 2607, 3073,
      //         2407, 2209, 2450, 2441, 3900, 3100}
      //};

     // calibration from runs 531-549
      //{0,0 0,1 0,2 0,3 0,4 0,5
      // 1,0 1,1 1,2 1,3 1,4 1,5
      // .......................
      float c0EC[2][Ndim] = { // for 80 GeV e-
        // Ecal plane 0:
        {2053, 1838, 2074, 1749, 1850, 1908,
         1889, 1776, 1820, 1480, 2500, 1980,
         1906, 1262, 1707, 1742, 1582, 1949,
         2053, 1336, 1476, 1533, 1646, 1079,
         1641, 2202, 1760, 1856, 1497, 1983,
         1696, 1130, 2096, 1315, 1622, 1757},
        // Ecal plane 1:
        {2698, 2874, 3377, 2430, 2330, 3190,
         2564, 2202, 2300, 2196, 2314, 1337,
         2110, 2266, 2276, 2290, 3632, 2863,
         0,    2398, 2613, 2418, 2479, 2790,
         2646, 2238, 2224, 2360, 2333, 3073,
         2407, 2209, 2560, 2441, 3900, 3100}
      };

      for (int ipl = 0; ipl < 2; ipl++) {
        int l = 0;
        for (int i = 0; i < 6; i++) {
          for (int j = 0; j < 6; j++) {
            float ratio = c0EC[ipl][l] > 0 ? c0maxEC[ipl]/c0EC[ipl][l] : 1;
            ECAL_calibration[ipl][i][j].K = ratio;
            l++;
          }
        }
      }
  
  // HCAL
  for (int d = 0; d < 4; ++d)
    for (int x = 0; x < 3; ++x)
      for (int y = 0; y < 3; ++y)
        HCAL_calibration[d][x][y].K = 80./2732.;
}

// === calibration data of 2016A runs ===
void Init_Calib_2016A_ECAL_cal1()
{
  // calibration data collected during runs 1070-1071 (3-Jul-2016)
  //{0,0 0,1 0,2 0,3 0,4 0,5
  // 1,0 1,1 1,2 1,3 1,4 1,5
  //........................
  float c0maxEC[2] = {5., 95.};
  float c0EC[2][6][6] = { // for 100 GeV e-
    // Ecal plane 0:
    {{1715, 1077, 1314, 1265, 1394, 1473},
     {1288, 1128, 1433, 1401, 1300, 1595},
     {1372, 1291, 1498, 1383, 1514, 1507},
     {1548, 1597, 1635, 1536, 1529, 1329},
     {1454, 1428, 1587, 1376, 1656, 1596},
     {1269, 1635, 1609, 1459, 1501, 1635}},
    // Ecal plane 1:
    {{2602, 2278, 2540, 2517, 2590, 2644},
     {2378, 2177, 2617, 2590, 2106, 3629},
     {2219, 2627, 2399, 2439, 3050, 2723},
     {2569, 2680, 2592, 2838, 2653, 2657},
     {1993, 2573, 2659, 2607, 1856, 2559},
     {2572, 2281, 2565, 2273, 2374, 2638}}
  };  
  // normalized to 100 GeV
  for (int ipl = 0; ipl < 2; ipl ++) 
    for (int j = 0; j < 6; j++) 
      for (int i = 0; i < 6; i++) {
        if (ipl == 1) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.7456/c0EC[ipl][i][j];
        if (ipl == 0) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.8006/c0EC[ipl][i][j];
      }
}

void Init_Calib_2016A_BGO()
{  
  // calibration during runs 1130 - 1154
  //   (0, 1, 2, 3, 4, 5, 6, 7)
  float calBGO[8] = {1701, 1261, 1772, 1390, 1197, 1471, 2054, 1396};
  float c0BGO(0.064); // 64 MeV

  for (int i = 0; i < 8; i++)
    BGO_calibration[i].K = c0BGO/calBGO[i]; // in GeV
}

void Init_Calib_2016A_SRD()
{
  // calibration data in runs 1581, 1582
  // SRD had only 2 channels during 2016A
  // 42 MeV is mean energy deposition in active media,
  // total mean energy deposition is 80 MeV (active media + absorber)
  // TODO: change calibration to 42MeV to be in line with 2016B and 2017
  SRD_calibration[0].K = 0.080 / 3365.;
  SRD_calibration[1].K = 0.080 / 1831.;
  SRD_calibration[2].K = 0.;
}

void Init_Calib_2016A_HCAL_cal1()
{
  // preliminary calibration of HCAL
  
  //                 (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  //                  ...........................................................
  //                  100 GeV, pi-
  float calHC[4][3][3] = {{{2569, 2378, 2410}, {2485, 2358, 2475}, {2402, 2454, 2552}},
                          {{   1,    1,    1}, {   1,    1,    1}, {   1,    1,    1}},
                          {{2358, 3062, 2601}, {2683, 2766, 2816}, {2693, 2710, 2872}},
                          {{2284, 2468, 2115}, {2515, 2510, 2141}, {2299, 2204, 2230}}};

  // normalized to 100 GeV
  for (int ipl = 0; ipl < 4; ipl++) 
    for (int j = 0; j < 3; j++) 
      for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K = 100.*100./125./calHC[ipl][i][j];
}

void Init_Calib_2016A_ECAL_cal2()
{
      // ............. 10 -Jul-2016 ... run 1493 ...............
      //   {0,0 0,1 0,2 0,3 0,4 0,5
      //    1,0 1,1 1,2 1,3 1,4 1,5
      //......................................................................
      float c0maxEC[2] = {5., 95.};
      float c0EC[2][6][6] = { // for 100 GeV e-
        // Ecal plane 0: (PS)
        {{1565, 902.3, 1125, 1234, 869.1, 1413},
         {886.4, 969.1, 1357, 1003, 1284, 1126},
         {1180, 1040, 1135, 1843, 1183, 1276},
         {1252, 1307, 1218, 1342, 1409, 972.6},
         {1129, 1024, 1123, 986.8, 1273, 1470},
         {993.7, 1264, 1308, 1001, 1551, 1331}},
        // Ecal plane 1: (EC)
        {{2149, 2044, 2131, 2744, 2373, 2290},
         {2387, 2078, 2070, 2174, 2132, 3244},
         {2154, 2412, 2405, 1957, 2362, 2087},
         {2548, 2540, 2687, 2369, 2507, 2552},
         {2305, 2360, 1729, 2357, 2287, 2465},
         {2389, 1935, 2011, 2512, 2412, 2416}}
      };
  
      for (int ipl = 0; ipl < 2; ipl ++) 
        for (int j = 0; j < 6; j++) 
          for (int i = 0; i < 6; i++) {
            if (ipl == 1) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.7456/c0EC[ipl][i][j];
            if (ipl == 0) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.8006/c0EC[ipl][i][j];
          }
}

void Init_Calib_2016A_HCAL_cal2()
{  
      // Hcal calibration  HC0, HC1, HC2, HC3
      //              [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
      //              [1]  ...........................................................
      //                  100 GeV, pi-
      float cal0HC[4][3][3] = {
    {{2284, 2468, 2115}, {2515, 2510, 2141}, {2299, 2204, 2230}},
    {{2569, 2378, 2410}, {2485, 2358, 2475}, {2402, 2454, 2552}},
    {{2358, 3062, 2601}, {2683, 2766, 2816}, {2693, 2710, 2872}},
    {{1320, 1480, 1520}, {3600, 1400, 1520}, {1680, 1320,  960}}
      };
     
      // normalized to 100 GeV
      for (int ipl = 0; ipl < 4; ipl++) {
        for (int j = 0; j < 3; j++)
          for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K = 100.*0.655/cal0HC[ipl][i][j];
     }
}

// === calibration data 2016B ===
// data is taken from pcdmrc01:/online/detector/calibrs/2016.xml/ECALT.xml on 2016-11-17:
// -rw-rw-r-- 1 daq daq 2673 Oct 24 22:41 /online/detector/calibrs/2016.xml/ECALT.xml
void Init_Calib_2016B_ECAL()
{
  // for 100 GeV e-
  float c0maxEC[2] = {5., 95.};
  //   {0,0 0,1 0,2 0,3 0,4 0,5
  //    1,0 1,1 1,2 1,3 1,4 1,5
  float c0EC[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {1747, 1166, 1886, 1709, 1382, 1896},
      {1177, 1753, 1530, 1508, 2021, 1847},
      {1504, 1344, 1512, 1654, 1700, 1711},
      {1604, 1321, 1621, 1465, 1711, 1340},
      {1798,  883, 1584, 1526, 1561, 1864},
      {1195, 1530, 1846, 1476, 1668, 1398}
    },
    {
      // Ecal plane 1: (EC)
      {2652, 2019, 2305, 2538, 2358, 2506},
      {2638, 2516, 2752, 2231, 2370, 2504},
      {1622, 2431, 2518, 2357, 2587, 2845},
      {2672, 2513, 2273, 2189, 2404, 2482},
      {2702, 2582, 2523, 2299, 2307, 2054},
      {2661, 2072, 3101, 2369, 2249, 2399}
    }
  };
  
  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        if (ipl == 0) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.8006/c0EC[ipl][i][j];
        if (ipl == 1) ECAL_calibration[ipl][i][j].K = c0maxEC[ipl]*0.7456/c0EC[ipl][i][j];
      }
  
  // tmean define expected channel timing vs. master time
  //   texpected = master + tmean
  // 
  // the value computed as
  //   tmean = <tchannel - master>
  //     tchannel - channel "rising edge" time: Cell::t0_RisingEdge()
  //     master - S1 counter "center of mass" time: S1.t0_CenterOfMass(15, 32)
  //     <> - average over statistics
  float tmean[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {-42.9, -50.7, -62.7, -59.3, -52.9, -41.7},
      {-57.0, -55.0, -58.0, -55.9, -56.5, -61.5},
      {-55.0, -61.7, -61.6, -53.8, -51.0, -56.1},
      {-56.4, -52.6, -64.0, -56.1, -52.0, -68.1},
      {-55.4, -66.3, -52.0, -61.3, -54.8, -59.7},
      {-40.6, -54.9, -61.7, -60.6, -62.1, -38.2}
    },
    {
      // Ecal plane 1: (EC)
      {-38.8, -42.7, -44.8, -52.8, -39.9, -38.0},
      {-47.6, -42.7, -45.2, -46.9, -46.9, -37.8},
      {-44.3, -43.4, -44.0, -43.3, -42.3, -54.5},
      {-55.8, -32.8, -32.2, -32.2, -33.6, -55.2},
      {-57.6, -33.7, -31.6, -37.6, -43.1, -51.3},
      {-36.0, -59.4, -61.4, -60.9, -60.5, -32.8}
    }
  };

  float tsigma[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {4.0, 4.6, 4.0, 4.7, 4.9, 4.6},
      {3.0, 4.8, 4.2, 3.1, 4.3, 4.7},
      {3.2, 4.6, 4.8, 3.3, 4.5, 4.2},
      {4.7, 4.9, 3.1, 2.3, 3.4, 4.3},
      {6.5, 7.2, 7.2, 3.4, 4.3, 5.5},
      {6.3, 5.4, 5.0, 4.0, 4.6, 6.1}
    },
    {
      // Ecal plane 1: (EC)
      {4.0, 4.0, 4.1, 3.7, 4.9, 4.8},
      {3.3, 3.4, 3.0, 3.5, 4.0, 5.3},
      {3.0, 4.6, 3.0, 2.3, 3.9, 4.2},
      {4.1, 3.8, 2.3, 1.8, 3.7, 4.5},
      {3.4, 3.4, 4.3, 3.2, 4.4, 5.0},
      {4.8, 4.4, 4.3, 5.0, 5.1, 5.4}
    }
  };

  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        ECAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        ECAL_calibration[ipl][i][j].tsigma = tsigma[ipl][i][j];
      }

}

// data is taken from pcdmrc01:/online/detector/calibrs/2016.xml/HCAL.xml on 2016-11-17:
// -rw-rw-r-- 1 daq daq 3318 Oct 27 16:21 /online/detector/calibrs/2016.xml/HCAL.xml
void Init_Calib_2016B_HCAL()
{
  // Hcal calibration  HC0, HC1, HC2, HC3
  //              [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  //              [1]  ...........................................................
  //                  100 GeV, pi-
  float cal0HC[4][3][3] = {
    {{2489, 2503, 2493}, {2493, 2502, 2491}, {2502, 2476, 2490}},
    {{2295, 2280, 2362}, {2390, 2320, 2125}, {2382, 2211, 2572}},
    {{2306, 2472, 2284}, {2473, 2522, 2482}, {2467, 2500, 2397}},
    {{2526, 2368, 2948}, {3150, 2573, 2478}, {2438, 2493, 2772}}
  };
  
  // normalized to 100 GeV
  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++)
      for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K = 100.*0.655/cal0HC[ipl][i][j];
  }
  
  // see Init_Calib_2016B_ECAL() for tmean definition
  float tmean[4][3][3] = {
    {{-40., -45., -39.}, {-38., -38., -38.}, {-40., -35., -36.}},
    {{-35., -42., -40.}, {-39., -31., -32.}, {-37., -43., -39.}},
    {{-43., -33., -40.}, {-41., -37., -33.}, {-35., -39., -35.}},
    {{-44., -35., -40.}, {-33., -30., -40.}, {-36., -34., -32.}}
  };

  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) {
        HCAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        HCAL_calibration[ipl][i][j].tsigma = 4.;
      }
    }
  }

}

// calibrations prepared by Vladimir Poliakov
void Init_Calib_2016B_SRD()
{
  // 42 MeV is mean energy deposition in active media of SRD (based on simulations by M.Kirsanov)
  SRD_calibration[0].K = 0.042 / 1080.;
  SRD_calibration[1].K = 0.042 / 1900.;
  SRD_calibration[2].K = 0.042 / 1740.;
  
  // see Init_Calib_2016B_ECAL() for tmean definition
  SRD_calibration[0].tmean = -59.6;
  SRD_calibration[1].tmean = -54.1;
  SRD_calibration[2].tmean = -54.1;
  
  SRD_calibration[0].tsigma = 2.2;
  SRD_calibration[1].tsigma = 1.8;
  SRD_calibration[2].tsigma = 1.9;
}

void Init_Calib_2016B_VETO()
{
  VETO_calibration[0].K = 0.01 / 800.; // preliminarily checked ising dimuons in visible mode setup
  VETO_calibration[1].K = 0.01 / 650.;
  VETO_calibration[2].K = 0.01 / 600.;
  VETO_calibration[3].K = 0.01 / 650.;
  VETO_calibration[4].K = 0.01 / 820.;
  VETO_calibration[5].K = 0.01 / 900.;

  //VETO_calibration[0].K = 0.0096 / (1753./2.); // calibration from 30.12.2016 from Samoilenko
  //VETO_calibration[1].K = 0.0096 / (1320./2.);
  //VETO_calibration[2].K = 0.0096 / (1841./2.);
  //VETO_calibration[3].K = 0.0096 / (1550./2.);
  //VETO_calibration[4].K = 0.0096 / 820.;
  //VETO_calibration[5].K = 0.0096 / 900.;

  //Donskov: pmtime is used
  // see Init_Calib_2016B_ECAL() for tmean definition
  VETO_calibration[0].tmean = -49.6;
  VETO_calibration[1].tmean = -50.0;
  VETO_calibration[2].tmean = -49.4;
  VETO_calibration[3].tmean = -50.9;
  VETO_calibration[4].tmean = -49.9;
  VETO_calibration[5].tmean = -51.2;

  for(int iveto=0; iveto < 6; iveto++)
    VETO_calibration[iveto].tsigma = 6.;
}

// === Additional corrections to the calibration data 2016B ===
// === Can be revised or removed after the implementation of the LED corrections
void Corr_Calib_2016B(const int run)
{
  //ECAL corrections to put the peak in the run 2363 to 100 GeV
  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        if (ipl == 0) ECAL_calibration[ipl][i][j].K *= 1.09*1.0989;
        if (ipl == 1) ECAL_calibration[ipl][i][j].K *= 1.0989;
      }

  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly run 2351
  float mkfact[4];
  mkfact[0] = 1.4;
  mkfact[1] = 1.38;
  mkfact[2] = 1.237;
  mkfact[3] = 1.21;
  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++)
      for (int i = 0; i < 3; i++)
        HCAL_calibration[ipl][i][j].K *= mkfact[ipl];
  }

  //Corect the HCAL channel in module 2 recommutated after the HCAL calibration
  if(run >= 2114) HCAL_calibration[2][2][1].K *= 0.25;
}

void Init_Calib_2016B_VISMODE()
{
  WCAL_calibration[0].K = 100. / 1990.;
  //Donskov: pmtime is used
  // see Init_Calib_2016B_ECAL() for tmean definition
  WCAL_calibration[0].tmean = -36.5;
  WCAL_calibration[0].tsigma = 1.8;

  VTWC_calibration.K = 0.75 * 0.002 / 340.; // preliminarily checked using dimuons in visible mode setup
  VTWC_calibration.tmean = -52.5;
  VTWC_calibration.tsigma = 2.6;

  VTEC_calibration.K = 0.0038 / 300.; // preliminarily checked using dimuons in visible mode setup (0.002 changed to 0.0038)
  VTEC_calibration.tmean = -71.1;
  VTEC_calibration.tsigma = 24.9;

  S2_calibration.K = 0.00048 / 140.; // preliminarily checked using dimuons in visible mode setup
  S2_calibration.tmean = -27.5;
  S2_calibration.tsigma = 2.9;
}


// === calibration data 2017 ===
// data is taken from pcdmrc01:/online/detector/calibrs/2017.xml/ECALT.xml on 2017-09-14:
// -rw-r--r-- 1 daq daq 2673 Sep 14 10:38 /online/detector/calibrs/2017.xml/ECALT.xml
// Energy calibration calculated by Alexander Toropin, based on runs 3119-3159 (second stage of calibration runs)
void Init_Calib_2017_ECAL()
{
  // for 100 GeV e-
  const float factor[2] = {
     5. * 0.84191 * 100./111.,
    95. * 0.84191 * 100./111.
  };
  
  //   {0,0 0,1 0,2 0,3 0,4 0,5
  //    1,0 1,1 1,2 1,3 1,4 1,5
  const float peakmean[2][6][6] = {
    {
      // Ecal plane 0: (Preshower)
      {1081, 1439, 1367, 1460, 1564, 1418},
      {1328, 1383, 1471, 1308, 1490, 1642},
      {1323, 1107, 1278, 1321, 1396, 1426},
      {1387, 1398, 1401, 1445, 1428, 1069},
      {1393, 1051, 1265, 1508, 1358, 1521},
      {1323, 1310, 1619, 1410,  724, 1253}
    },
    {
      // Ecal plane 1: (EC)
      {2345, 2331, 2335, 2156, 2260, 2432},
      {2401, 2347, 2151, 2035, 2343, 2235},
      {2350, 2356, 2356, 2324, 2325, 2271},
      {2040, 2185, 2287, 2267, 2407, 2190},
      {2417, 2616, 2177, 2292, 2341, 2183},
      {2680, 2028, 2297, 2279, 2144, 2219}
    }
  };
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; x++)
      for (int y = 0; y < 6; y++)
        ECAL_calibration[d][x][y].K = factor[d] / peakmean[d][x][y];


  // tmean define expected channel timing vs. master time
  //   texpected = master + tmean
  //
  // the value computed as
  //   tmean = <tchannel - master>
  //     tchannel - channel "rising edge" time: Cell::t0_RisingEdge()
  //     master - T2 counter "rising edge" time: T2.t0_RisingEdge()
  //     <> - average over statistics
  //
  //  Time constants from the Donskov code pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  //     
  float tmean[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {-75.3, -45.6, -51.7, -48.3, -47.9, -72.2},
      {-48.8, -50.9, -44.4, -45.8, -45.8, -47.5},
      {-44.8, -49.6, -49.6, -42.8, -44.0, -44.1},
      {-44.5, -44.6, -54.5, -44.7, -45.1, -57.6},
      {-45.2, -57.2, -43.0, -49.3, -44.8, -48.8},
      {-67.3, -44.3, -56.6, -51.0, -52.8, -70.0}
    },
    {
      // Ecal plane 1: (EC)
      {-65.6, -32.2, -31.9, -38.4, -34.0, -70.6},
      {-34.7, -65.0, -66.0, -69.1, -68.5, -24.8},
      {-33.6, -63.2, -67.2, -67.3, -61.5, -44.3},
      {-41.5, -59.9, -59.4, -58.5, -64.2, -42.7},
      {-43.0, -62.0, -57.6, -60.4, -69.7, -36.8},
      {-68.5, -47.2, -47.1, -50.0, -48.1, -71.0}
    }
  };

  float tsigma[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {6.6, 9.0, 8.4, 6.9, 6.6, 7.8},
      {9.6, 7.5, 8.4, 11.1, 9.0, 10.5},
      {9.3, 10.8, 9.0, 6.9, 7.5, 7.2},
      {7.5, 9.3, 9.3, 6.6, 6.3, 12.6},
      {10.5, 9.3, 8.4, 8.4, 6.3, 6.6},
      {15.0, 7.2, 13.5, 15.6, 9.0, 10.5}
    },
    {
      // Ecal plane 1: (EC)
      {10.8, 5.1, 7.2, 9.3, 9.3, 10.8},
      {5.1, 7.8, 5.7, 6.3, 7.2, 4.8},
      {6.6, 5.1, 6.0, 5.4, 4.8, 6.0},
      {5.4, 6.6, 5.1, 5.4, 7.5, 5.4},
      {5.4, 6.0, 4.2, 6.0, 6.6, 5.1},
      {12.0, 5.7, 6.0, 5.7, 4.5, 12.0}
    }
  };

  for (int ipl = 0; ipl < 2; ipl ++)
    for (int j = 0; j < 6; j++)
      for (int i = 0; i < 6; i++) {
        ECAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        ECAL_calibration[ipl][i][j].tsigma = tsigma[ipl][i][j];
      }

}

// Calibration prepared by Vladimir Samoylenko
// Details:
//   ELOG / Message ID: 122 / Entry time: Sun Sep 10 21:09:40 2017
//   http://pcdmfs01.cern.ch:8080/Main/122
void Init_Calib_2017_VETO()
{
  // mean energy deposition in one cell (MC by M.Kirsanov)
  const float factor = 9.8*MeV;
  // Landau fit MPV parameter of amplitude distribution
  const float MPV[6] = {590, 501, 590, 637, 610, 566};
  
  for (int i = 0; i < 6; i++)
    VETO_calibration[i].K = factor / MPV[i];
}

// data is taken from pcdmrc01 on 2017-09-19:
// -rw-rw-r-- 1 daq daq 1941 Sep 15 11:05 /online/detector/calibrs/2017.xml/HCAL.xml
// which was extracted from paper logbook by Alexander Toropin
// calibration runs:
//   HCAL0   2902 - 2910
//   HCAL1   ???
//   HCAL2   2850 - 2859
//   HCAL3   2843 - 2849
void Init_Calib_2017_HCAL()
{
  // beam pi- 100 GeV
  const float factor = 100. * 0.655;
  
  // HCAL0 [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  // HCAL1 [1]  ...........................................................
  const float amplitude[4][3][3] = {
    {{2529, 2519, 2531}, {2484, 2524, 2497}, {2506, 2468, 2497}},
    {{2453, 2467, 2591}, {2520, 2536, 2450}, {2499, 2558, 2566}},
    {{2423, 2433, 2480}, {2568, 2521, 2559}, {2459, 2570, 2546}},
    {{2528, 2425, 2445}, {2501, 2423, 2470}, {2400, 2550, 2380}}
  };
  
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; x++)
      for (int y = 0; y < 3; y++)
        HCAL_calibration[d][x][y].K = factor / amplitude[d][x][y];

  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly run 3387
  const float mkfact[4] = {1.27, 1.48, 1.21, 1.};
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; x++)
      for (int y = 0; y < 3; y++)
        HCAL_calibration[d][x][y].K *= mkfact[d];
  
  // see Init_Calib_2017_ECAL() for tmean definition
  // Time constants from the Donskov code pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  // tsigma values are not copied in details from the code above, values assigned according to the
  // general tendency (they are big anyway)
  // 
  float tmean[4][3][3] = {
    {{-60.6, -68.7, -58.4}, {-58.1, -61.5, -59.8}, {-65.1, -56.5, -59.5}},
    {{-62.9, -66.0, -70.7}, {-67.1, -56.6, -58.7}, {-62.3, -68.0, -72.9}},
    {{-68.5, -54.3, -57.8}, {-72.5, -61.2, -58.4}, {-61.4, -57.4, -59.2}},
    {{-65.7, -64.0, -66.8}, {-63.7, -64.3, -68.6}, {-67.2, -64.5, -57.7}}
  };

  for (int ipl = 0; ipl < 4; ipl++) {
    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) {
        HCAL_calibration[ipl][i][j].tmean = tmean[ipl][i][j];
        HCAL_calibration[ipl][i][j].tsigma = 8.;
        if(ipl == 1) HCAL_calibration[ipl][i][j].tsigma = 14.;
      }
    }
  }

}

// data is extracted from pcdmfs01 on 2017-10-03:
// -rw-r--r-- 1 daq daq 397 Sep 24 22:22 /online/detector/calibrs/2017.xml/WCAL.xml
// constants calculated by S.Donskov and V.Poliakov based on
//   - some electrons calibration run for WCAL[0] and WCAL[1]
//   - some muons calibration run for Wcatcher
//   - energy deposition simulations by M.Kirsanov
void Init_Calib_2017_WCAL(const int run)
{
  // values are in "MeV / ADC count"
  WCAL_calibration[0].K =  3.1*MeV;
  WCAL_calibration[1].K = 58.0*MeV;
  WCAT_calibration.K =  5.0*MeV;
  
  // Wcatcher analog signal was split to be included in trigger
  // which decreased amplitude by 2 times
  if (run >= 3504) WCAT_calibration.K *= 2.0;

  if (run <= 3462) WCAL_calibration[1].K *= (100./96.1); // This and below: correct WCAL calibration using runs 3400, 3466, 3521
  if (run >= 3463) WCAL_calibration[1].K *= (100./106.); // correction (approximate) for 30X geometry. Partly explaned by HV increase.

  // see Init_Calib_2017_ECAL() for tmean definition
  // taken from Donskov code pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  //
  WCAL_calibration[0].tmean = -44.8;
  WCAL_calibration[1].tmean = -36.6;
  WCAT_calibration.tmean = -38.8;

  WCAL_calibration[0].tsigma = 4.2;
  WCAL_calibration[1].tsigma = 3.6;
  WCAT_calibration.tsigma = 5.4;
}

// calibrations prepared by Vladimir Poliakov and extracted from
//   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2017/list_runs_Sep_2017_v3.pdf
//   (7 September record / SRD MIP amplitude in ADC counts)
void Init_Calib_2017_SRD()
{
  // 42 MeV is mean energy deposition in active media of SRD (based on simulations by M.Kirsanov)
  SRD_calibration[0].K = 0.042 / 1050.;
  SRD_calibration[1].K = 0.042 / 1070.;
  SRD_calibration[2].K = 0.042 / 1040.;

  // see Init_Calib_2017_ECAL() for tmean definition
  // taken from pcdmfs01 /online/detector/calo/H4/INVIS/time_ph_part.cc
  //
  SRD_calibration[0].tmean = -78.3;
  SRD_calibration[1].tmean = -78.9;
  SRD_calibration[2].tmean = -77.2;

  SRD_calibration[0].tsigma = 0.6 * 8.4;
  SRD_calibration[1].tsigma = 0.6 * 7.8;
  SRD_calibration[2].tsigma = 0.6 * 7.8;
}

void Init_Calib_2017_MasterTime(const int run)
{
  // T2 timing calibration data extracted from T2 rising edge histograms of runs 3191-3573
  // NOTE: T2 is available only starting from run 3191
  T2_calibration.isTmeanRelative = false;
  T2_calibration.tmean = 210.;
  T2_calibration.tsigma = 6.6;
  
  // S2 timing extracted from runs 3183-3190
  if (3183 <= run && run <= 3190) {
    S2_calibration.isTmeanRelative = false;
    S2_calibration.tmean = 239.;
    S2_calibration.tsigma = 6.0;
  }
  
  if (3163 <= run && run <= 3182) {
    S2_calibration.isTmeanRelative = false;
    S2_calibration.tmean = 200.;
    S2_calibration.tsigma = 6.0;
  }
  
  if (3119 <= run && run <= 3162) {
    S2_calibration.isTmeanRelative = false;
    S2_calibration.tmean = 214.;
    S2_calibration.tsigma = 6.5;
  }
}

void Init_Calib_2017_Counters()
{
  // Calibrated using dimuons from 3387 - 3389
  // Time calibration from dimuons and hadron calib run 3378
  //
  V2_calibration.K = 0.0075 / 1250.;
  S4_calibration.K = 0.00023 / 130.;
  //
  V2_calibration.tmean = -51.8;
  S4_calibration.tmean = -37.;
  //
  V2_calibration.tsigma = 4.;
  S4_calibration.tsigma = 4.;
}


// === calibration data 2018 ===
// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 2713 May 20 12:38 /online/detector/calo/H4/calibrs/2018.xml/ECALT.xml
void Init_Calib_2018_ECAL(const int run)
{
  // factors extracted from Donskov's constants file
  // for 100 GeV e-
  float factor[2] = {
    0.05 * 100.,
    0.78 * 100.
  };
  
  //   {0,0 0,1 0,2 0,3 0,4 0,5
  //    1,0 1,1 1,2 1,3 1,4 1,5
  const float peakmean[2][6][6] = {
    {
      // Ecal plane 0: (Preshower)
      {1545, 1326, 1504, 1350, 1360, 1333},
      {1276, 1487, 1468,  955, 1347, 1572},
      {1451,  968, 1400, 1392, 1260, 1538},
      {1476, 1294, 1247, 1358, 1330, 1703},
      {1398, 1198, 1349, 1716, 1429, 1295},
      {1364, 1419, 1347, 1246, 1429, 1472}
    },
    {
      // Ecal plane 1: (EC)
      {2373, 2169, 2410, 2405, 2163, 2388},
      {2363, 2431, 2066, 2307, 2488, 2700},
      {2739, 2199, 2454, 2193, 2832, 2165},
      {2941, 2879, 2436, 2109, 2303, 1807},
      {2374, 2498, 2307, 2195, 2482, 2195},
      {1981, 2081, 2317, 2330, 2834, 2325}
    }
  };
  
  // tmean define expected channel timing vs. master time
  //   texpected = master + tmean
  //
  // the value computed as
  //   tmean = <tchannel - master>
  //     tchannel - channel "rising edge" time: Cell::t0_RisingEdge()
  //     master - T2 counter "rising edge" time: T2.t0_RisingEdge()
  //     <> - average over statistics
  
  // tmean is the same as in the 2017 except of
  // channel ECAL1-3-3:  -58.5  -->  -63.5
  const float tmean_1[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {-75.3, -45.6, -51.7, -48.3, -47.9, -72.2},
      {-48.8, -50.9, -44.4, -45.8, -45.8, -47.5},
      {-44.8, -49.6, -49.6, -42.8, -44.0, -44.1},
      {-44.5, -44.6, -54.5, -44.7, -45.1, -57.6},
      {-45.2, -57.2, -43.0, -49.3, -44.8, -48.8},
      {-67.3, -44.3, -56.6, -51.0, -52.8, -70.0}
    },
    {
      // Ecal plane 1: (EC)
      {-65.6, -32.0, -31.9, -38.4, -34.0, -70.6},
      {-34.7, -65.0, -66.0, -69.1, -68.5, -24.8},
      {-33.6, -63.2, -67.2, -67.3, -61.5, -44.3},
      {-41.5, -59.9, -59.4, -63.5, -64.2, -42.7},
      {-43.0, -62.0, -57.6, -60.4, -69.7, -36.8},
      {-68.5, -47.2, -47.1, -50.0, -48.1, -71.0}
    }
  };
  
  // visible mode, Donskov 05Apr2019
  const float tmean_2[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {-70.0, -42.6, -50.0, -44.3, -41.7, -67.5},
      {-47.1, -46.1, -36.4, -40.1, -41.8, -43.5},
      {-44.0, -41.6, -47.2, -36.4, -43.5, -37.7},
      {-41.0, -37.6, -50.5, -37.8, -48.2, -47.0},
      {-40.0, -54.2, -41.5, -44.6, -40.3, -41.5},
      {-71.4, -38.6, -43.4, -44.0, -45.7, -68.2}
    },

    {
      // Ecal plane 1: (EC)
      {-64.6, -27.8, -29.5, -33.8, -30.7, -58.0},
      {-29.5, -58.9, -61.5, -61.9, -61.6, -21.6},
      {-41.4, -57.6, -61.7, -62.8, -55.0, -41.3},
      {-37.5, -56.8, -55.2, -54.5, -59.4, -37.5},
      {-37.9, -57.2, -54.2, -56.2, -65.4, -31.8},
      {-67.5, -44.6, -39.3, -38.3, -43.1, -67.8}
    }
  };
  
  const float(*tmean)[6][6]  = (run >= 4224) ? tmean_2 : tmean_1;
  
  // tsigma is set 3 times higher than in original data source
  // (the same as in 2017 calibration data)
  const float tsigma[2][6][6] = {
    {
      // Ecal plane 0: (PS)
      {6.6, 9.0, 8.4, 6.9, 6.6, 7.8},
      {9.6, 7.5, 8.4, 11.1, 9.0, 10.5},
      {9.3, 10.8, 9.0, 6.9, 7.5, 7.2},
      {7.5, 9.3, 9.3, 6.6, 6.3, 12.6},
      {10.5, 9.3, 8.4, 8.4, 6.3, 6.6},
      {15.0, 7.2, 13.5, 15.6, 9.0, 10.5}
    },
    {
      // Ecal plane 1: (EC)
      {10.8, 5.1, 7.2, 9.3, 9.3, 10.8},
      {5.1, 7.8, 5.7, 6.3, 7.2, 4.8},
      {6.6, 5.1, 6.0, 5.4, 4.8, 6.0},
      {5.4, 6.6, 5.1, 5.4, 7.5, 5.4},
      {5.4, 6.0, 4.2, 6.0, 6.6, 5.1},
      {12.0, 5.7, 6.0, 5.7, 4.5, 12.0}
    }
  };
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; x++)
      for (int y = 0; y < 6; y++) {
        CaloCalibData& c = ECAL_calibration[d][x][y];
        c.K = factor[d] / peakmean[d][x][y];
        c.tmean = tmean[d][x][y];
        c.tsigma = tsigma[d][x][y];
      }
}

// manual run-dependent corrections
void Init_Calib_2018_ECAL_Manual(const int run)
{
  float factor[2] = {1., 1.};
  
  // corrected using all calibration runs // LIV
  const float corr_prs = 1.06;
//  const float corr_ecal = 0.932;
  
  if (run >= 3897 && run < 4098) {
    factor[0] *= corr_prs;
    factor[1] *= 100./(-0.067 * run + 369.1);
  }
  
  // only Ecal[0] correction is necessary, Ecal[1] corrected by HV adjustment
  if (run >= 4098 && run <= 4223) {
    factor[0] *= corr_prs;
    factor[1] *= 100./(-0.0622*run+360.1);
  }

  // corrections for the visible mode, no run dependence, uses dimuons
  if (run >= 4224) {
    factor[0] *= corr_prs;
    factor[1] *= 0.85 * 100./(-0.0622*4186.+360.1);
  }
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; x++)
      for (int y = 0; y < 6; y++) {
        CaloCalibData& c = ECAL_calibration[d][x][y];
        c.K *= factor[d];
      }
}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 1997 May 19 18:24 /online/detector/calo/H4/calibrs/2018.xml/HCAL.xml
void Init_Calib_2018_HCAL(const int run)
{
  // factor as defined in Donskov's codes
  // beam hadrons 100 GeV
  const float factor = 1. * 100.;
  
  // HCAL0 [0] (0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), 2,1), (2,2)
  // HCAL1 [1]  ...........................................................
  const float peakmean[4][3][3] = {
    {{2102, 2281, 2302}, {2318, 2344, 2303}, {2407, 2114, 2069}},
    {{2363, 2408, 2283}, {2352, 2374, 2411}, {2486, 2342, 2262}},
    {{1880, 1994, 1551}, {2252, 2000, 1863}, {2273, 2203, 2184}},
    {{2245, 1917, 1829}, {2208, 2850, 2454}, {1920, 1850, 2679}}
  };
  
  // tmean definition: see Init_Calib_2018_ECAL()
  // the data is the same as in 2017
  // INVISIBLE mode
  const float tmeanINV[4][3][3] = {
    {{-60.6, -68.7, -58.4}, {-58.1, -61.5, -59.8}, {-65.1, -56.5, -59.5}},
    {{-62.9, -66.0, -70.7}, {-67.1, -56.6, -58.7}, {-62.3, -68.0, -72.9}},
    {{-68.5, -54.3, -57.8}, {-72.5, -61.2, -58.4}, {-61.4, -57.4, -59.2}},
    {{-65.7, -64.0, -66.8}, {-63.7, -64.3, -68.6}, {-67.2, -64.5, -57.7}}
  };
  
  // timing for VISIBLE mode (by Donskov 05Apr2019)
  const float tmeanVIS[4][3][3] = {
    {{-55.9, -64.9, -53.8}, {-53.7, -56.8, -55.0}, {-59.7, -56.6, -54.2}},
    {{-49.6, -55.2, -60.7}, {-50.9, -50.3, -47.4}, {-47.5, -54.4, -59.1}},
    {{-60.7, -51.0, -52.3}, {-61.9, -54.4, -52.4}, {-54.0, -53.7, -48.9}},
    {{-59.5, -57.4, -60.5}, {-57.1, -60.4, -65.5}, {-62.1, -56.2, -52.1}}
  };
  
  const float (*tmean)[3][3] = (run >= 4224) ? tmeanVIS : tmeanINV;
  
  // tsigma the same as 2017
  // (see Init_Calib_2017_HCAL() for details on values)
  const float tsigma[4] = {8., 14., 8., 8.};
  
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; x++)
      for (int y = 0; y < 3; y++) {
        CaloCalibData& c = HCAL_calibration[d][x][y];
        c.K = factor / peakmean[d][x][y];
        c.tmean = tmean[d][x][y];
        c.tsigma = tsigma[d];
      }
}

// manual run-dependent corrections
void Init_Calib_2018_HCAL_Manual(const int run)
{
  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly run 3898 3899 3900
  float mkfact[4] = {0.84, 0.81, 0.76, 1.};

  //HCAL corrections to put the dimuon peak to the place predicted by MC, mainly runs 4224, 4244, 4245, 4298 visible mode
  const float corr_hcal0_vism = 1.12;
  const float corr_hcal1_vism = 1.456;

  if (run >= 4224) {
        mkfact[0] *= corr_hcal0_vism;
        mkfact[1] *= corr_hcal1_vism;
        }  
  
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; x++)
      for (int y = 0; y < 3; y++) {
        CaloCalibData& c = HCAL_calibration[d][x][y];
        c.K *= mkfact[d];
      }
}

// data source:
// * energy (page1 / 18May record):
//   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2018/list_runs_May_2018_v1.pdf
//
// * timing (at pcdmfs01):
//   -rw-rw-r-- 1 daq daq 514 May 20 17:39 /online/detector/calo/H4/calibrs/2018.xml/SRDT.xml
void Init_Calib_2018_SRD(const int run)
{
  // 42 MeV is mean energy deposition in active media of SRD
  // (based on simulations by M.Kirsanov)
  // peak position is extracted from MIP signal distribution (MPV of landau fit) in runs 3852-3854
  SRD_calibration[0].K = 0.042 / 1515.;
  SRD_calibration[1].K = 0.042 / 1260.;
  SRD_calibration[2].K = 0.042 / 1050.;
  
  if (run >= 4224) { // only for visible mode
    // corrected using run 4238
    // correction calculated based on comparison of MC and DATA SRD energy depositions distributions
    // factor makes DATA distribution matching to MC distribution
    SRD_calibration[0].K *= 37./28.;
    SRD_calibration[1].K *= 30./27.;
    // NOTE: SRD[2] is absent in visible mode
  }
  
  SRD_calibration[0].tmean = -68.0;
  SRD_calibration[1].tmean = -68.0;
  SRD_calibration[2].tmean = -68.0;

  if (run < 4208) { // runs with 100 GeV
    SRD_calibration[0].tmean += 6.;
    SRD_calibration[1].tmean += 6.;
    SRD_calibration[2].tmean += 6.;
  }
  else if (run < 4224) { // runs with 150 GeV
    SRD_calibration[0].tmean += 4.;
    SRD_calibration[1].tmean += 4.;
    SRD_calibration[2].tmean += 4.;
  }
  else { // VISIBLE mode
    SRD_calibration[0].tmean -= 1.;
    SRD_calibration[1].tmean -= 1.;
    SRD_calibration[2].tmean -= 1.;
  }
 
  SRD_calibration[0].tsigma = 2.0;
  SRD_calibration[1].tsigma = 2.0;
  SRD_calibration[2].tsigma = 2.0;

  if (run >= 4208) { // runs with 150 GeV
  SRD_calibration[0].tsigma = 3.0;
  SRD_calibration[1].tsigma = 3.0;
  SRD_calibration[2].tsigma = 3.0;
  }

}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 982 May 28 20:17 /online/detector/calo/H4/calibrs/2018.xml/VETO.xml
void Init_Calib_2018_VETO(const int run)
{
  // mean energy deposition in one cell (MC by M.Kirsanov)
  const float factor = 9.8*MeV;
  
  // Landau fit MPV parameter of amplitude distribution
  // initial calibration
  const float mpvinit[6] = {790, 772, 820, 820, 790, 800};
  // new calibration done in runs 4013, 4014
  const float mpv4013[6] = {790, 772, 820, 400, 600, 800};
  // veto3 & veto5 counters swapped and HV on veto5 encreased  before run 4061
  const float mpv4061[6] = {790, 772, 820, 800, 600, 750};
  
  const float tmeanINV[6] = {-77.4, -78.8, -75.5, -76.7, -78.4, -78.9}; // invisible mode
  const float tmeanVIS[6] = {-73.6, -74.0, -71.0, -72.0, -73.5, -72.5}; // visible mode, Donskov 29Mar2019
  const float* tmean = (run >= 4224) ? tmeanVIS : tmeanINV;
  
  const float tsigmainit[6] = {3.4, 3.3, 3.2, 3.1, 3.7, 3.7};
  const float tsigma4013[6] = {5.0, 5.0, 5.0, 5.0, 5.0, 5.0};
  
  const float* mpv = 0;
  if (run < 4013) mpv = mpvinit;
  else if (run < 4061) mpv = mpv4013;
  else mpv = mpv4061;
  
  const float* tsigma = 0;
  if (run < 4013) tsigma = tsigmainit;
  else tsigma = tsigma4013;
  
  for (int i = 0; i < 6; i++) {
    CaloCalibData& c = VETO_calibration[i];
    c.K = factor / mpv[i];
    c.tmean = tmean[i];
    c.tsigma = tsigma[i];
  }
}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq  582 Jun 14 22:36 /online/detector/calo/H4/calibrs/2018.xml/WCAL.xml
// -rw-r--r-- 1 daq daq 1422 Jun 17 13:09 /online/detector/calo/H4/calibrs/2018.xml/BEAMCOUNTERS.xml
void Init_Calib_2018_WCAL(const int run)
{
  // values are in "MeV / ADC count"

  WCAL_calibration[0].K = 4.0*MeV;
  WCAL_calibration[1].K = 68.3*MeV;
  WCAL_calibration[2].K = .017*MeV;

  // TODO: check calibration
  WCAT_calibration.K =  5.0*MeV;
  
  WCAL_calibration[0].tmean = -51.;
  WCAL_calibration[1].tmean = -49.;
  WCAL_calibration[2].tmean = -35.;
  WCAT_calibration.tmean = -65.0;
  
  if (run >= 4224) { // visible mode
    WCAL_calibration[0].tmean = -49.8;
    WCAL_calibration[1].tmean = -49.4;
    WCAL_calibration[2].tmean = -33.5+8.8;
    
    // activate additional filtering of peaks to compensate EM interference
    WCAL_calibration[2].minPeakRaisingSlopeSpeed = 0.05;
  }
  
  // visible mode
  if (4215 <= run)
    WCAT_calibration.tmean = -59.0;

  WCAL_calibration[0].tsigma = 3.;
  WCAL_calibration[1].tsigma = 3.;
  WCAL_calibration[2].tsigma = 4.;
  WCAT_calibration.tsigma = 5.0;
}

// manual run-dependent corrections
void Init_Calib_2018_WCAL_Manual(const int run)
{
  // corrected using runs visible mode 4238, 4263, 4288  // LIV
  const float corr_wcal0 = 1.42;
  const float corr_wcal1 = 1.08;
  // corrected using runs 4239  // LIV
  const float corr_wcal2 = 3.6;

  if (run >= 4224) {
  WCAL_calibration[0].K *= corr_wcal0;
  WCAL_calibration[1].K *= corr_wcal1;
  WCAL_calibration[2].K *= corr_wcal2;
  }
  
  //run - dependent correction for WCAL0 and WCAL1  //
  float corr_wcal1_rundep = 1;
  if (run < 4263) {
    corr_wcal1_rundep = 142.03 / (-0.324*run + 1517.5); 
  }
  if (run >= 4263) {
    corr_wcal1_rundep = 142.03 / (-0.01*run + 178.94);
  }
  WCAL_calibration[1].K *= corr_wcal1_rundep;
  
  float corr_wcal0_rundep = 1;     
  if (run < 4249) {
    corr_wcal0_rundep = 8.2 / 6.988; 
  }
    if (run >= 4249 && run < 4277) {
    corr_wcal0_rundep = 8.2 / 9.813; 
  }
  if (run >= 4277) {
    corr_wcal0_rundep = 8.2 / 7.87;
  }
  WCAL_calibration[0].K *= corr_wcal0_rundep;
}


// additional after LED corrections
void Init_Calib_2018_WCAL_AfterLed(const int run)
{
  // WCAL additionally corrected using the electron calibration runs and extrapolation ECAL+HCAL vs WCAL to ECAL+HCAL=0 in the physical runs
  //
  if (run < 4239) {
    WCAL_calibration[0].K *= 150./142.;
    WCAL_calibration[1].K *= 150./141.;
  }
  if (run == 4227) {
    WCAL_calibration[0].K *= 1.005;
    WCAL_calibration[1].K *= 1.005;
  }
  if (run == 4225) {
    WCAL_calibration[0].K *= 1.01;
    WCAL_calibration[1].K *= 1.01;
  }
  if (run == 4224) {
    WCAL_calibration[0].K *= 1.015;
    WCAL_calibration[1].K *= 1.015;
  }
  if (run >= 4288 && run < 4295) {
    WCAL_calibration[0].K *= 8.3/8.7;
    WCAL_calibration[1].K *= 150./153.;
  }
  if (run >= 4295) {
    WCAL_calibration[0].K *= 0.98 * 8.3/7.2;
    WCAL_calibration[1].K *= 0.98 * 148.2/160.;
  }

  // ECAL in the visible mode was additionally corrected using the signal from muons in the hadron calibration runs
  //
  if (run >= 4224 && run <= 4309) {
    for (int i = 0; i < 2; ++i) {
      for (int x = 0; x < 6; ++x) {
        for (int y = 0; y < 6; ++y) {
          ECAL_calibration[i][x][y].K *= 1./1.05;
        }
      }
    }
  }

  // ECAL in the invisible mode
  //
  if (run >= 3918 && run <= 3925) {
    for (int i = 0; i < 2; ++i) {
      for (int x = 0; x < 6; ++x) {
        for (int y = 0; y < 6; ++y) {
          if(i != 0) ECAL_calibration[i][x][y].K *= 100./102.8;
          if(i == 0) ECAL_calibration[i][x][y].K *= 1.11;
        }
      }
    }
  }
  if (run == 3922 || run == 3923) { // Correction from muons in the hadron calibration runs
    for (int i = 0; i < 2; ++i) {
      for (int x = 0; x < 6; ++x) {
        for (int y = 0; y < 6; ++y) {
          if(x == 3 && y == 3) ECAL_calibration[i][x][y].K /= 1.1;
          if(x != 3 && y != 3 && abs(x-3) <= 1 && abs(y-3) <= 1) ECAL_calibration[i][x][y].K /= 1.09;
        }
      }
    }
  }

  //HCal corrected using runs 3900 3951 3974 4000 4040 4098 4117 4156 4189  // LIV 

  float corr_hcal_invis[4] = {1.19, 1.08, 0.93, 1.};

  if (run < 4224 && run >= 3855) {
         for (int d = 0; d < 4; d++)
          for (int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++) {
              CaloCalibData& c = HCAL_calibration[d][x][y];
              c.K *= corr_hcal_invis[d];
            }
  }  

}


void Init_Calib_2018_MasterTime(const int run)
{
  // T2 is master time, tmean is absolute value
  T2_calibration.isTmeanRelative = false;
  
  // calibration data extracted with `SADC_t0_method = kT0_RisingEdgeHH` algorithm (default for 2018)
  
  if (run < 4224) {
    // visible mode
    // from timing.cc:
    //   run 3918: mean=219.4 stddev=7.251
    //   run 4038: mean=219.1 stddev=7.258
    T2_calibration.tmean = 219.25;
    T2_calibration.tsigma = 7.3;
    
    // old calibration, `SADC_t0_method = kT0_RisingEdge` algorithm, run 4038
    //T2_calibration.tmean = 207.;
    //T2_calibration.tsigma = 5.6;
  }
  else {
    // invisible mode
    // from timing.cc:
    //   run 4238: mean=226.2 stddev=7.316
    //   run 4239: mean=224.5 stddev=7.527
    T2_calibration.tsigma = 7.4;
    
    // Donskov data, 21Apr2019:
    T2_calibration.tmean = 225.57;
  }
}

// data source (at pcdmfs01):
// -rw-r--r-- 1 daq daq 1422 Jun 17 13:09 /online/detector/calo/H4/calibrs/2018.xml/BEAMCOUNTERS.xml
void Init_Calib_2018_Counters(const int run)
{
  if (run <= 4214) {
    V2_calibration.K = 0.0075 / 610.;
    V2_calibration.tmean = -37.0;
    V2_calibration.tsigma = 7.;
  }
  
  if (4215 <= run) {
    V2_calibration.K = 0.0075 / 700.;
    V2_calibration.tmean = -36.5;
    V2_calibration.tsigma = 7.;
  }

  // corrected using runs visible mode 4239, 4264-4267  // LIV
  
  if (4224 <= run) {
    V2_calibration.K *= 3.55 / 5.47;
  }

  const float corr_s4 = 1.177;

  if (4224 <= run) {
    S4_calibration.K = corr_s4 * 0.00023 / 130.;
    S4_calibration.tmean = -37.;
    S4_calibration.tsigma = 4.;
  }
}


// === calibration data 2021 ===
void Init_Calib_2021_MasterTime(const int run)
{
  // T2 is master time, tmean is absolute value
  T2_calibration.isTmeanRelative = false;
  
  // time point calculation algorithm is `SADC_t0_method = kT0_RisingEdgeHH`
  //   - run 5175 with timing.cc:
  T2_calibration.tmean = 148.;
  T2_calibration.tsigma = 9.;
}

// === MM mapping ===
void MM_plane_calib::print_MM_map()
{
    for (auto it = MM_map.begin(); it != MM_map.end(); ++it) {
      cout << "ch#" << it->first << " strips = ";
      for (size_t j = 0; j < it->second.size(); ++j) {
        cout << it->second[j] << " ";
      }
      cout << endl;
    }
}

void MM_plane_calib::Create_MM_reverse_map()
{
  MM_reverse_map.clear();
  
  for (auto i = MM_map.begin(); i != MM_map.end(); ++i) {
    const int channel = i->first;
    const vector<int>& strips = i->second;
    
    for (size_t j = 0; j < strips.size(); ++j) {
      const int strip = strips[j];
      MM_reverse_map[strip] = channel;
    }
  }
  
  //cout << "MM strips to electronics channels map:" << endl;
  //for (auto it = MM_reverse_map.begin(); it != MM_reverse_map.end(); ++it) {
  //  cout << "strip = " << it->first << " channel = " << it->second << endl;
  //}
}

// calibrations by Dipanwita Banerjee
void Init_Calib_2016B_MM(const time_t time)
{
  // design 2016
  MM1_calib.SetDesign2016();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2016();
  MM4_calib.SetDesign2016();
  
  //READ pedestals from file   
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  
 //timing calibration
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2016/MM04Y__timing");

 //put names
} //end Init_Calib_2016B_MM

void Init_Calib_2016_GEM(const time_t time)
{
  GM01_calib.load(time);
  GM02_calib.load(time);
}

void Init_Calib_2017_MM(const time_t time)
{
  // design 2016
  MM1_calib.SetDesign2016();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2016();
  MM4_calib.SetDesign2016();
  MM5_calib.SetDesign2016();
  MM7_calib.SetDesign2016();
  
  //READ pedestals from file  
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM7_calib.ReadSigmaCalib(time);
  
  //timing constant
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM05Y__timing");
  MM7_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM07X__timing");
  MM7_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM07Y__timing");


} //end Init_Calib_2017_MM

void Init_Calib_2017_GEM(const time_t time)
{
  GM01_calib.load(time);
  GM02_calib.load(time);
  GM03_calib.load(time);
  GM04_calib.load(time);
}

void Init_Calib_2018_MM(const time_t time)
{
  // design 2016
  MM1_calib.SetDesign2016();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2016();
  MM4_calib.SetDesign2016();
  MM5_calib.SetDesign2016();
  MM6_calib.SetDesign2016();
  
  //READ pedestals from file
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  
  //timing constant --- copy pasted from 2017 for now
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM07X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2017/MM07Y__timing");

} //end Init_Calib_2018_MM

// PRELIMINARY calibration for 2021 run
void Init_Calib_2021_MM(const time_t time)
{
  // design 2016 + 2021
  MM1_calib.SetDesign2021Small();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2016();
  MM4_calib.SetDesign2016();
  MM5_calib.SetDesign2021Small();
  MM6_calib.SetDesign2016();  
  //MM7_calib.SetDesign2021Large();

  //READ pedestals from file
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  //MM7_calib.ReadSigmaCalib(time);
  
  //timing constant
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM06X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021/MM06Y__timing");

  //testing
  //MM1_calib.xcalib.print_MM_map();
  //MM1_calib.ycalib.print_MM_map();
  //MM2_calib.xcalib.print_MM_map();
  //MM2_calib.ycalib.print_MM_map();

} //end Init_Calib_2021_MM

// MM calibrations, 2021B
void Init_Calib_2021B_MM(const time_t time)
{
  // design
  MM1_calib.SetDesign2016();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2021Small();
  MM4_calib.SetDesign2016();
  
  MM5_calib.SetDesign2021Large();
  MM6_calib.SetDesign2021Large();  
  MM7_calib.SetDesign2021Large();
  
  // pedestals
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  MM7_calib.ReadSigmaCalib(time);
  
  // PRELIMINARY timing constant: -- adapted from 2021A
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM06X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM06Y__timing");
  MM7_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM07X__timing");
  MM7_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2021mu/MM07Y__timing");
}

void Init_Calib_2022A_MM(const time_t time)
{
  // design
  MM1_calib.SetDesign2016();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2021Small();
  MM4_calib.SetDesign2016();
  
  MM5_calib.SetDesign2021Large();
  MM6_calib.SetDesign2021Large();  
  MM7_calib.SetDesign2021Large();
  
  // pedestals
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  MM7_calib.ReadSigmaCalib(time);
  
  // timing constant
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM06X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM06Y__timing");
  MM7_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM07X__timing");
  MM7_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022mu/MM07Y__timing");
}

void Init_Calib_2022B_MM(const time_t time, const int run)
{
  // design
  MM1_calib.SetDesign2021Small();
  if (run <= 6137) MM2_calib.SetDesign2021Small();
  else             MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2016();
  MM4_calib.SetDesign2016();
  MM5_calib.SetDesign2021Large();
  MM6_calib.SetDesign2021Large();  
  MM7_calib.SetDesign2021Large();
  
  // pedestals
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  MM7_calib.ReadSigmaCalib(time);
  
  // timing constant
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM06X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM06Y__timing");
  MM7_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM07X__timing");
  MM7_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2022/MM07Y__timing");
}

void Init_Calib_2023A_MM(const time_t time)
{
  // design
  MM1_calib.SetDesign2021Small();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2016();
  MM4_calib.SetDesign2016();
  MM5_calib.SetDesign2021Large();
  MM6_calib.SetDesign2021Large();  
  MM7_calib.SetDesign2021Large();
  
  // pedestals
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  MM7_calib.ReadSigmaCalib(time);
  
  // timing constant
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM06X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM06Y__timing");
  MM7_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM07X__timing");
  MM7_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023/MM07Y__timing");
}

void Init_Calib_2023B_MM(const time_t time, const int run)
{
  // design
  MM1_calib.SetDesign2021Small();
  MM2_calib.SetDesign2016();
  MM4_calib.SetDesign2016();
  MM5_calib.SetDesign2021Large();
  MM6_calib.SetDesign2021Large();
  MM7_calib.SetDesign2021Large();
  MM8_calib.SetDesign2016();
  MM10_calib.SetDesign2016();
  MM11_calib.SetDesign2016();
  
  if (run < 9885) {
    MM3_calib.SetDesign2021Small();
    MM9_calib.SetDesign2016();
  }
  else {
    // from run 9885:
    // Exchanged MM03 with MM09 due to small efficient area in MM03
    // http://pcdmfs01.cern.ch:8080/Main/781
    MM3_calib.SetDesign2016();
    MM9_calib.SetDesign2021Small();
  }

  // pedestals
  // TODO: prepare calibration data
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  MM7_calib.ReadSigmaCalib(time);
  MM8_calib.ReadSigmaCalib(time);
  MM9_calib.ReadSigmaCalib(time);
  MM10_calib.ReadSigmaCalib(time);
  MM11_calib.ReadSigmaCalib(time);

  // timing constant
  // TODO: verify
  MM1_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM06X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM06Y__timing");
  MM7_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM07X__timing");
  MM7_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM07Y__timing");
  MM8_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM08X__timing");
  MM8_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM08Y__timing");
  MM9_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM09X__timing");
  MM9_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2023mu/MM09Y__timing");
  MM10_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023mu/MM10X__timing");
  MM10_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023mu/MM10Y__timing");
  MM11_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2023mu/MM11X__timing");
  MM11_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2023mu/MM11Y__timing");
}

void Init_Calib_2024A_MM(const time_t time, const int run)
{
  // design
  MM1_calib.SetDesign2021Small();
  MM2_calib.SetDesign2016();
  MM3_calib.SetDesign2016();
  MM4_calib.SetDesign2016();

  // pedestals
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);

  // timing constant
  MM1_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM04Y__timing");

  // timestamp corresponds to Thursday May 02 2024 17:11:54 GMT+2
  // This corresponds to the time the large MM were first installed and working
  if (time >= 1714669914) {
    // design
    MM5_calib.SetDesign2021Large();
    MM6_calib.SetDesign2021Large();
    MM7_calib.SetDesign2021Large();

    // pedestals
    MM5_calib.ReadSigmaCalib(time);
    MM6_calib.ReadSigmaCalib(time);
    MM7_calib.ReadSigmaCalib(time);

    // timing constant
    MM5_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM05X__timing");
    MM5_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM05Y__timing");
    MM6_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM06X__timing");
    MM6_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM06Y__timing");
    MM7_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM07X__timing");
    MM7_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024/MM07Y__timing");
  }
}

void Init_Calib_2024B_MM(const time_t time, const int run)
{
  // design
  if (run <= 12163) {
    MM1_calib.SetDesign2016();
    MM2_calib.SetDesign2016();
  } else {
    MM1_calib.SetDesign2021Small();
    MM2_calib.SetDesign2021Small();
  }
  MM3_calib.SetDesign2021Small();
  MM4_calib.SetDesign2021Small();
  // Old Micromegas replaced a new one starting from run 11562
  if (run <= 11561) MM5_calib.SetDesign2021Small();
  else MM5_calib.SetDesign2016();
  MM6_calib.SetDesign2021Small();
  MM7_calib.SetDesign2016();
  MM8_calib.SetDesign2021Small();
  MM9_calib.SetDesign2016();
  MM10_calib.SetDesign2021Small();
  MM11_calib.SetDesign2021Large();
  MM12_calib.SetDesign2021Large();
  MM13_calib.SetDesign2021Large();

  // pedestals
  MM1_calib.ReadSigmaCalib(time);
  MM2_calib.ReadSigmaCalib(time);
  MM3_calib.ReadSigmaCalib(time);
  MM4_calib.ReadSigmaCalib(time);
  MM5_calib.ReadSigmaCalib(time);
  MM6_calib.ReadSigmaCalib(time);
  MM7_calib.ReadSigmaCalib(time);
  MM8_calib.ReadSigmaCalib(time);
  MM9_calib.ReadSigmaCalib(time);
  MM10_calib.ReadSigmaCalib(time);
  MM11_calib.ReadSigmaCalib(time);
  MM12_calib.ReadSigmaCalib(time);
  MM13_calib.ReadSigmaCalib(time);

  // timing constant, prepared by B. Banto using latency runs 11827-11834
  MM1_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM01X__timing");
  MM1_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM01Y__timing");
  MM2_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM02X__timing");
  MM2_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM02Y__timing");
  MM3_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM03X__timing");
  MM3_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM03Y__timing");
  MM4_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM04X__timing");
  MM4_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM04Y__timing");
  MM5_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM05X__timing");
  MM5_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM05Y__timing");
  MM6_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM06X__timing");
  MM6_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM06Y__timing");
  MM7_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM07X__timing");
  MM7_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM07Y__timing");
  MM8_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM08X__timing");
  MM8_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM08Y__timing");
  MM9_calib.xcalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM09X__timing");
  MM9_calib.ycalib.ReadTimingCalib(conddbpath()  + "pedestal/2024mu/MM09Y__timing");
  MM10_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM10X__timing");
  MM10_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM10Y__timing");
  MM11_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM11X__timing");
  MM11_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM11Y__timing");
  MM12_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM12X__timing");
  MM12_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM12Y__timing");
  MM13_calib.xcalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM13X__timing");
  MM13_calib.ycalib.ReadTimingCalib(conddbpath() + "pedestal/2024mu/MM13Y__timing");

  // Correction for bad wires in MM
  // SMALL MM
  if (run <= 12163) {
    MM1_calib.xcalib.insertBadWire(0);
    MM1_calib.ycalib.insertBadWire(51);
    MM2_calib.xcalib.insertBadWire(4);
    MM2_calib.xcalib.insertBadWire(9);
    MM2_calib.xcalib.insertBadWire(36);
    MM2_calib.xcalib.insertBadWire(45);
    MM2_calib.xcalib.insertBadWire(53);
    MM2_calib.ycalib.insertBadWire(5);
    MM2_calib.ycalib.insertBadWire(6);
    MM2_calib.ycalib.insertBadWire(43);
    MM2_calib.ycalib.insertBadWire(45);
    MM2_calib.ycalib.insertBadWire(63);
  } else {
    MM1_calib.ycalib.insertBadWire(62);
    MM2_calib.xcalib.insertBadWire(63);
    MM2_calib.ycalib.insertBadWire(62);
    MM2_calib.ycalib.insertBadWire(63);
  }
  MM3_calib.ycalib.insertBadWire(62);
  MM3_calib.ycalib.insertBadWire(63);
  MM4_calib.xcalib.insertBadWire(0);
  MM4_calib.ycalib.insertBadWire(62);
  MM6_calib.xcalib.insertBadWire(1);
  MM7_calib.xcalib.insertBadWire(24);
  MM7_calib.xcalib.insertBadWire(25);
  MM7_calib.ycalib.insertBadWire(24);
  MM10_calib.ycalib.insertBadWire(20);
  MM10_calib.ycalib.insertBadWire(55);
  // LARGE MM
  MM11_calib.xcalib.insertBadWire(165);
  MM12_calib.xcalib.insertBadWire(49);
  MM12_calib.xcalib.insertBadWire(63);
  MM12_calib.xcalib.insertBadWire(127);
  MM12_calib.xcalib.insertBadWire(157);
  MM12_calib.xcalib.insertBadWire(189);
  MM13_calib.xcalib.insertBadWire(0);
  MM13_calib.xcalib.insertBadWire(1);
  MM13_calib.xcalib.insertBadWire(64);
  MM13_calib.xcalib.insertBadWire(65);
  MM13_calib.xcalib.insertBadWire(66);
  MM13_calib.xcalib.insertBadWire(67);
  MM13_calib.xcalib.insertBadWire(127);
  MM13_calib.xcalib.insertBadWire(128);
  MM13_calib.xcalib.insertBadWire(129);
  MM13_calib.xcalib.insertBadWire(130);
  MM13_calib.xcalib.insertBadWire(131);
  MM13_calib.xcalib.insertBadWire(132);
  MM13_calib.ycalib.insertBadWire(0);
  MM13_calib.ycalib.insertBadWire(1);
}

void Init_Calib_2018_GEM(const time_t time)
{
  GM01_calib.load(time);
  GM02_calib.load(time);
  GM03_calib.load(time);
  GM04_calib.load(time);
}

void Init_Calib_2021_GEM(const time_t time)
{
  GM01_calib.load(time);
  GM02_calib.load(time);
}

void Init_Calib_2021B_GEM(const time_t time)
{
  GM01_calib.load(time);
  GM02_calib.load(time);
  GM03_calib.load(time);
  GM04_calib.load(time);
}

void Init_Calib_2022A_GEM(const time_t time)
{
  GM01_calib.load(time);
  GM02_calib.load(time);
  GM03_calib.load(time);
  GM04_calib.load(time);
}

void Init_Calib_2022B_GEM(const time_t time)
{
  GM01_calib.load(time);
  GM02_calib.load(time);
}

void Init_Calib_2022A_ST()
{
  // Set design and parameters
  ST01_calib.InitXY6Design( 490);
  ST02_calib.InitXY6Design( 490);
  ST04_calib.InitXY6Design(1120);
  ST05_calib.InitXY6Design(1120);
}

void Init_Calib_2022B_ST(const int run)
{
  // T0 these values are based on maximizing the number of two-wire hits per plane
  // These were estimated using calib run 6501 by B. Banto
  // shift_t0 was estimated using runs 6762 and 7956 by B. Banto
  // Set design and parameters
  double shift_t0 = 0.0;
  if (run >= 6034 && run < 6726) shift_t0 = 0.0;
  else if (run >= 6726 && run < 6779) shift_t0 = 15.0;
  else if (run >= 6779 && run < 8459) shift_t0 = 35.0;
  ST01_calib.InitXY6Design(1176 + shift_t0);
  ST02_calib.InitXY6Design(1162 + shift_t0);
  ST03_calib.InitXY6Design( 911 + shift_t0);
  ST04_calib.InitXY6Design( 908 + shift_t0);
  ST05_calib.InitXY6Design( 979 + shift_t0);
  ST06_calib.InitXY6Design( 952 + shift_t0);
  ST11_calib.InitUV6Design(   0);
  ST12_calib.InitUV6Design(   0);
}

void Init_Calib_2023A_ST(const int run)
{
  // The T0 values were estimated by A. Toropin using run 9711
  // See https://indico.cern.ch/event/1382393/contributions/5811899/attachments/2802254/4889126/hadrons_16_Feb_2024.pdf
  // See also: https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/471#note_8078630
  // Set design and parameters
  // timeWindow = 70 ns for 3 mm drift
  if (run >= 9592 && run <= 9717) {
    ST01_calib.InitXY6Design(1580);
    ST02_calib.InitXY6Design(1574);
    ST03_calib.InitXY6Design(1315);
    ST04_calib.InitXY6Design(1313);
    ST05_calib.InitXY6Design(1384);
    ST06_calib.InitXY6Design(1379);
  } else {
  // T0: these values are based on centering the time peak for all wires to be at +30ns
  // See: https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/471#note_8078630
  // These were estimated using run 9022 by B. Banto
  // Set design and parameters
  // timeWindow = 70 ns for 3 mm drift
    ST01_calib.InitXY6Design(1242);
    ST02_calib.InitXY6Design(1238);
    ST03_calib.InitXY6Design( 980);
    ST04_calib.InitXY6Design( 977);
    ST05_calib.InitXY6Design(1050);
    ST06_calib.InitXY6Design(1048);
  }
  // UV t0 estimated preliminary by B.Banto (23.01.2024) using run 8585, latest estimation by M. Tuzi (02.02.2024)
  // See also git issue: https://gitlab.cern.ch/P348/p348-daq/-/work_items/103#note_7557360
  ST11_calib.InitUV6Design(991); //compromise, ideal values for each plane separately: 991 for ST11U, 992 for ST11V
  ST12_calib.InitUV6Design(987); //compromise, ideal values for each plane separately: 986 for ST12U, 989 for ST12V

}

void Init_Calib_2023B_ST()
{
  // T0 these values are based on centering the time peak of events around +30ns
  // These were estimated using run 10151 by B. Banto
  // Set design and parameters
  // timeWindow = 70 ns for 3 mm drift
  ST01_calib.InitXY6Design( 659);
  ST02_calib.InitXY6Design( 660);
  ST03_calib.InitXY6Design( 675);
  ST04_calib.InitXY6Design(1095);
  ST05_calib.InitXY6Design(1377);
  ST06_calib.InitXY6Design(1378);
  // UV t0 estimations done by M. Tuzi 16.01.2024, using run 10151
  // See also git issue: https://gitlab.cern.ch/P348/p348-daq/-/work_items/103#note_7486335
  ST11_calib.InitUV6Design( 626); //compromise, ideal values for each plane separately: 627 for ST11U, 624 for ST11V
  ST12_calib.InitUV6Design( 593); //compromise, ideal values for each plane separately: 592 for ST12U, 595 for ST12V

  ST01_calib.invertAxis=true;
  ST02_calib.invertAxis=true;
  ST03_calib.invertAxis=true;
  ST04_calib.invertAxis=true;
  ST05_calib.invertAxis=true;
  ST06_calib.invertAxis=true;

}

// TODO: FOR THE MOMENT PRELIMINARY VALUES ESTIMATED WITH RUN 10602
void Init_Calib_2024A_ST()
{
  // T0 these values are based on centering the time peak of events around +30ns
  // These were estimated using run 9022 by B. Banto
  // Set design and parameters
  // timeWindow = 70 ns for 3 mm drift
  ST01_calib.InitXY6Design(742);
  ST02_calib.InitXY6Design(738);
  ST03_calib.InitXY6Design(480);
  ST04_calib.InitXY6Design(477);
  ST05_calib.InitXY6Design(550);
  ST06_calib.InitXY6Design(548);
  ST11_calib.InitUV6Design(491); //compromise, ideal values for each plane separately: 991 for ST11U, 992 for ST11V
  ST12_calib.InitUV6Design(487); //compromise, ideal values for each plane separately: 986 for ST12U, 989 for ST12V

}

void Init_Calib_2024B_ST()
{
  // T0 these values are based on centering the time peak of events around +30ns
  // These were estimated using run 11827 by B. Banto (16.08.2024)
  // Set design and parameters
  // timeWindow = 70 ns for 3 mm drift
  ST01_calib.InitXY6Design( 781);
  ST02_calib.InitXY6Design( 782);
  ST03_calib.InitXY6Design(1234);
  ST04_calib.InitXY6Design(1747);
  ST05_calib.InitXY6Design(1754);
  ST06_calib.InitXY6Design(1754);
  ST11_calib.InitUV6Design( 853);
  ST12_calib.InitUV6Design( 882);
  ST13_calib.InitUV6Design( 885);
}

void Init_Geometry_2016A_part1()
{
  // measured by Dipanwita Banerjee
  // Micromegas offsets actual for runs range: "start of 2016A" - #1426
  // offsets was measured just before run #1389
  ECAL_pos.pos = TVector3(-340,      0,       0);
  MM4_pos.pos  = TVector3(-300,    -10,    -640);
  MM3_pos.pos  = TVector3(-260,     -8,   -1840);
  MAGD_pos.pos = TVector3(   0,      0,  -19215+500+4000); // position of magnet downstream side (magnet length = 4 meters)
  MAGU_pos.pos = TVector3(   0,      0,  -19215+500);      // position of magnet upstream side
  MM2_pos.pos  = TVector3(-4.6,   13.1,  -19215);
  MM1_pos.pos  = TVector3(-8.32,  10.6,  -20015);
  
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = M_PI/4;
  MM1_pos.invertAxis = true;
  MM2_pos.invertAxis = true;
  MM3_pos.invertAxis = true;
  MM4_pos.invertAxis = true;
}

void Init_Geometry_2016A_part2()
{
  // measured by Dipanwita Banerjee
  // Micromegas offsets actual for runs range: #1427 - "end of 2016A"
  // offsets was measured just before run #1427
  MM1_pos.pos  = TVector3(  -5,  14,   -20015);
  MM2_pos.pos  = TVector3(   0,  16,   -19215);
  MAGU_pos.pos = TVector3(   0,   0,   -19215+500);      // position of magnet upstream side
  MAGD_pos.pos = TVector3(   0,   0,   -19215+500+4000); // position of magnet downstream side (magnet length = 4 meters)
  MM3_pos.pos  = TVector3(-300,  -8,    -1840);
  MM4_pos.pos  = TVector3(-320, -10,     -640);
  ECAL_pos.pos = TVector3(-350,   0,        0);
  
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = M_PI/4;
  MM1_pos.invertAxis = true;
  MM2_pos.invertAxis = true;
  MM3_pos.invertAxis = true;
  MM4_pos.invertAxis = true;
}

// measured by Dipanwita Banerjee
void Init_Geometry_2016B()
{
  // MM offsets actual for runs range: #2000 - ...
  ECAL_pos.pos = TVector3(  -380,   0,      0);
  // 2016B data, estimated by Emilio 
  ECAL0BEAMSPOT_pos = TVector3(-372.9, +9.7, 0);
  ST2_pos.pos  = TVector3(     0,   0,  -2210);
  S2_pos.pos   = TVector3(     0,   0,   -713);
  SRD_pos.pos  = TVector3(     0,   0,  -2871);
  S1_pos.pos   = TVector3(     0,   0,  -2940);
  pipe_pos.pos = TVector3(     0,   0,  -3216);
  MAGD_pos.pos = TVector3(     0,   0, -20100+598+4000);
  MAGU_pos.pos = TVector3(     0,   0, -20100+598);      
  Veto_pos.pos = TVector3(     0,   0, -20235);
  S0_pos.pos   = TVector3(     0,   0, -20379);
  ST1_pos.pos  = TVector3(     0,   0, -20668);
  //GEM position
  GM2_pos.pos  = TVector3(     0,   0,  -1174);
  GM1_pos.pos  = TVector3(     0,   0,  -2233);
  //MM position
  MM1_pos.pos  = TVector3(    -2,   1, -20900);
  MM2_pos.pos  = TVector3(  -1.5, -11, -20100);
  MM3_pos.pos  = TVector3(  -315,   0,  -2490);
  MM4_pos.pos  = TVector3(  -350,   0,   -924);
  //MMangles
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = M_PI/4;
  MM1_pos.invertAxis = true;
  MM2_pos.invertAxis = true;
  MM3_pos.invertAxis = true;
  MM4_pos.invertAxis = true;
  //magnetic field
  MAG_field = 2.1;
}

// measured by Dipanwita Banerjee
void Init_Geometry_2017()
{
  // MM offsets actual for runs range: #2000 - ...
  ECAL_pos.pos = TVector3(     0,   0,     0);
  ECAL0BEAMSPOT_pos = TVector3(-372.9, +9.7, 0);// copy from 2016B
  pipe_pos.pos = TVector3(     0,   0,  -4260);
  MAGD_pos.pos = TVector3(     0,   0, -18130+170+4300);//magnet downstream side
  MAGU_pos.pos = TVector3(     0,   0, -18130+170);//magnet upstream side
  //GEM position
  GM1_pos.pos  = TVector3(     -294,   0,  -1126-234-1022-621);
  GM2_pos.pos  = TVector3(     -349,   0,  -1126);
  GM3_pos.pos  = TVector3(     -342,   0,  -1126-234-1022);
  GM4_pos.pos  = TVector3(     -343,   0,  -1126-234);  
  //MM position
  //////////////////////////////////////////////////////////////////
  ////first values is coordinate respect the beam axis in the beam area
  ////second cordinate is the correction for the beam axis measured
  //////////////////////////////////////////////////////////////////
  MM1_pos.pos  = TVector3(-17,-6,-19670) + TVector3(-1.3+0.2,-0.1-0.06,0);
  MM2_pos.pos  = TVector3(-6,8,-19580) + TVector3(-3.09,-13.82,0);
  MM3_pos.pos  = TVector3(-17,-1.5,-18220) + TVector3(-1.3,0.2,0);
  MM4_pos.pos  = TVector3(-8,2.7,-18130) + TVector3(4.2,-10.7,0);
  MM5_pos.pos  = TVector3(-317,0,-3170) + TVector3(-8.1,-8.5,0);
  MM7_pos.pos  = TVector3(-366,5,-1030) + TVector3(2.4,-5.25,0);
  //MM angles
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = 3*M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = 3*M_PI/4;
  MM5_pos.Angle = M_PI/4;
  MM7_pos.Angle = M_PI/4;
  MM1_pos.invertAxis = true;
  MM2_pos.invertAxis = true;
  MM3_pos.invertAxis = true;
  MM4_pos.invertAxis = true;
  MM5_pos.invertAxis = true;
  MM7_pos.invertAxis = true;
  //magnetic field Tesla
  MAG_field = 1.8;
  
  // GEMs
  // TODO: the geometry was extracted from some detectors.dat and
  //       coordinate system does not match with conddb.h
  GM01Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM1_pos.pos  // TODO: from detectors.dat {-295.10777,3.09302,16620.}
  };
  GM02Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM2_pos.pos  // TODO: from detectors.dat {-34.381352,2.368,18605.0}
  };
  GM03Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM3_pos.pos  // TODO: from detectors.dat {-349.46117,0.02498,17252.05}
  };
  GM04Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ GM4_pos.pos  // TODO: from detectors.dat {-349.42229,7.41795,18371.05}
  };
}

// INVISIBLE mode, beam energy 100 GeV
// setup geometry of runs 3574 - 4207
void Init_Geometry_2018_invis100()
{
 //////////////////////////////////////////////////////////////////////////
 ///// Downstream detectors, Measured from ECAL position in Z and the
 //////from the deflection of the original beam line in X, second
 //////vector on the right if present represents a corrections
 //////for the tracking
 /////////////////////////////////////////////////////////////////////////
  ECAL_pos.pos = TVector3(-391.3,   0,     0);  // X as calculated by Alexander Toropin
  ECAL0BEAMSPOT_pos = TVector3(-372.9, +9.7, 0);// copy from 2016B
  pipe_pos.pos = TVector3(     0,   0,  -4197);
  //Magnet positions  
  MAGU_pos.pos = TVector3(     0,   0, -17466);//magnet upstream side
  #if 0
  /////////////////////////////////////
  /// this is the true magnet position,
  /// for simplicity we just use 4m magnet neglecting
  /// the gap to simplify calculations
  ///////////////////////////////////////////
  MAGD_pos.pos = TVector3(     0,   0, -12402);//magnet downstream side
  #endif
  // fake magnet position
  MAGD_pos.pos = TVector3(     0,   0, -13466);//magnet downstream side
  //MM position
  //////////////////////////////////////////////////////////////////
  ////first values is coordinate respect the beam axis in the beam area
  ////second cordinate is the correction for the beam axis measured
  //////////////////////////////////////////////////////////////////
  MM1_pos.pos  = TVector3(-10,   0,  -19671) + TVector3(0,                  0,   0 );
  MM2_pos.pos  = TVector3(17,    0,  -18201) + TVector3(4.378,         -3.564,   0 );
  MM3_pos.pos  = TVector3(-319,  0,  -3646 ) + TVector3(1.1,           -20.48,   0 );
  MM4_pos.pos  = TVector3(-329,  0,  -3452 ) + TVector3(5.06-3, -20.48+0.7781,   0 );
  MM5_pos.pos  = TVector3(-381,  0,  -1191 ) + TVector3(4.4,           -5.381,   0 );
  MM6_pos.pos  = TVector3(-397,  0,  -979  ) + TVector3(18.4,    -5.381-10.15,   0 );
  //MM angles
  MM1_pos.Angle = -M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = -M_PI/4;
  MM4_pos.Angle = -M_PI/4;
  MM5_pos.Angle = M_PI/4;
  MM6_pos.Angle = -M_PI/4;
  MM1_pos.invertAxis = true;
  MM2_pos.invertAxis = true;
  MM3_pos.invertAxis = true;
  MM4_pos.invertAxis = true;
  MM5_pos.invertAxis = true;
  MM6_pos.invertAxis = true;
  
  // GEMs, not yet corrected for tracking
  GM01Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-259,0,-863}
  };
  GM02Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-233,0,-1606}
  };
  GM03Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-251,0,-1052}
  };
  GM04Geometry = {
                          /*size*/ 256,
                          /*center*/ 0,
                          /*pitch mm*/ 0.4,
                          /*angle*/ 0.,
                          /*abs pos mm*/ {-242,0,-1437}
  };
  
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // TODO: unify GEM geometry definition
  
  // straw
  // data source:
  //   X - private communications with STRAW team
  //   Z - Emilio's measurements
  // TODO: STRAW team Z data does not match Emilio's meas.
  // TODO: check actual station numbering 04, 05, 06 - ???
  ST04Geometry = {64, 0, 6., 0, {-100,0,-3344}};
  ST05Geometry = {64, 0, 6., 0, {-150,0,-1866}};
  ST06Geometry = {64, 0, 6., 0, {-200,0,-736}};
  
  
  //magnetic field Tesla
  MAG_field = 1.72;

  //////////////////////////////////////
  //Additional detector measured but not used for Tracking:
  ////////////////////////////////
  ///Straw measured are based on position of X plane, distance between X and Y plane
  /// was reported to be 15 mm
  /////////////////////////////////////////////////////
  //// StrawX6.pos = TVector3(0,0,-1866);
  //// StrawX7.pos = TVector3(0,0,-736);
  //// StrawX8.pos = TVector3(0,0,-3344);
  /////////////////////////////////////////
  //// Veto , S1 and tube begin were measured upstream
  ///////////////////////////////////////////////
  ///// pipe_begin.pos = TVector3(0,0,-17922);
  ///// Veto.pos = TVector3(0,0,-18731);
  ///// S1.pos = TVector3(0,0,-18833.5);
  /////////////////////////////////////////////////
  ///// left side of ECAL was perfectly aligned with
  //// with undeflected beam axis. Veto and HCAL
  //// are placed immediately after that
  ////////////////////////////////////////////////
  //// Measured beam outlet until after ring of the tube
  ///////////////////////////////////////////////////
  ///// BeamOutlet.pos = TVector3(0,0,−20073);
  ///////////////////////////////////////////////////
  
} //end Geometry 2018

// VISIBLE mode, beam energy 150 GeV
// setup geometry of runs 3574 - 4207
void Init_Geometry_2018_vis150()
{
 //////////////////////////////////////////////////////////////////////////
 ///// Downstream detectors, Measured from ECAL position in Z and the
 //////from the deflection of the original beam line in X, second
 //////vector on the right if present represents a corrections
 //////for the tracking
 /////////////////////////////////////////////////////////////////////////
  //WARNING: rough geometry using list_runs_may_2018_v2.pdf
  ECAL_pos.pos = TVector3(-391.3,   0,     0);  // X as calculated by Alexander Toropin
  ECAL0BEAMSPOT_pos = TVector3(-372.9, +9.7, 0);// copy from 2016B
  pipe_pos.pos = TVector3(     0,   0,  -4197);
  //Magnet positions  
  MAGU_pos.pos = TVector3(     0,   0, -24111);//magnet upstream side
  #if 0
  /////////////////////////////////////
  /// this is the true magnet position,
  /// for simplicity we just use 4m magnet neglecting
  /// the gap to simplify calculations
  ///////////////////////////////////////////
  MAGD_pos.pos = TVector3(     0,   0, -19137);//magnet downstream side
  #endif
  // fake magnet position
  MAGD_pos.pos = TVector3(     0,   0, -20111);//magnet downstream side
  //MM position
  //////////////////////////////////////////////////////////////////
  ////first values is coordinate respect the beam axis in the beam area
  ////second cordinate is the correction for the beam axis measured for tracking
  //////////////////////////////////////////////////////////////////
  MM1_pos.pos  = TVector3(-10,   0,  -25948) + TVector3(0,                  0,   0 );  
  MM2_pos.pos  = TVector3(-10,  0,  -25581) + TVector3(4.4,           -5.381,   0 );
  MM3_pos.pos  = TVector3(17,    0,  -24846) + TVector3(4.378,         -3.564,   0 );
  MM4_pos.pos  = TVector3(17,    0,  -24479) + TVector3(4.378,         -3.564,   0 );
  //deflection calculated, not measured, corrected for point deflection algorithm
  MM5_pos.pos  = TVector3(-217,  0,  -6950 ) + TVector3(44.6,           -20.48,   0 );
  MM6_pos.pos  = TVector3(-220,  0,  -6750 ) + TVector3(38.06,        -10.7019,   0 );
  //MM angles
  MM1_pos.Angle = -M_PI/4;
  MM2_pos.Angle = -M_PI/4;
  MM3_pos.Angle =  M_PI/4;
  MM4_pos.Angle =  M_PI/4;
  MM5_pos.Angle = -M_PI/4;
  MM6_pos.Angle = -M_PI/4;
  MM1_pos.invertAxis = true;
  MM2_pos.invertAxis = true;
  MM3_pos.invertAxis = true;
  MM4_pos.invertAxis = true;
  MM5_pos.invertAxis = true;
  MM6_pos.invertAxis = true;

  // GEMs, position received from Michael Hoesgen after CORAL alignment
  GM01Geometry = {
    /*size*/ 256,
    /*center*/ 0,
    /*pitch mm*/ 0.4,
    /*angle*/ 0.,
    /*abs pos mm*/ {-300.53,-10.21,-581}
  };
  GM02Geometry = {
    /*size*/ 256,
    /*center*/ 0,
    /*pitch mm*/ 0.4,
    /*angle*/ 0.,
    /*abs pos mm*/ {-305.47,-18.55,-1591}
  };
  GM03Geometry = {
    /*size*/ 256,
    /*center*/ 0,
    /*pitch mm*/ 0.4,
    /*angle*/ 0.,
    /*abs pos mm*/ {-296.78,-2.68,-1040}
  };
  GM04Geometry = {
    /*size*/ 256,
    /*center*/ 0,
    /*pitch mm*/ 0.4,
    /*angle*/ 0.,
    /*abs pos mm*/ {-305.48,-6.31,-1424}
  };
  
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // TODO: unify GEM geometry definition
  
  //magnetic field Tesla
  MAG_field = 1.72;
  
  // incoming beam
  NominalBeamEnergy = 150.;
  NominalBeamPDG = 11;

  //////////////////////////////////////
  //Additional detector measured but not used for Tracking:
  ////////////////////////////////
  ///Straw measured are based on position of X plane, distance between X and Y plane
  /// was reported to be 15 mm
  /////////////////////////////////////////////////////
  //// StrawX6.pos = TVector3(0,0,-1866);
  //// StrawX7.pos = TVector3(0,0,-736);
  //// StrawX8.pos = TVector3(0,0,-3344);
  /////////////////////////////////////////
  //// Veto , S1 and tube begin were measured upstream
  ///////////////////////////////////////////////
  ///// pipe_begin.pos = TVector3(0,0,-17922);
  ///// Veto.pos = TVector3(0,0,-18731);
  ///// S1.pos = TVector3(0,0,-18833.5);
  /////////////////////////////////////////////////
  ///// left side of ECAL was perfectly aligned with
  //// with undeflected beam axis. Veto and HCAL
  //// are placed immediately after that
  ////////////////////////////////////////////////
  //// Measured beam outlet until after ring of the tube
  ///////////////////////////////////////////////////
  ///// BeamOutlet.pos = TVector3(0,0,−20073);
  ///////////////////////////////////////////////////
  
} //end Geometry 2018 VIS

//Preliminary Geometry for beamtime 2021
void Init_Geometry_2021()
{
  //Based on the preliminary report of 25.08.2021, see https://twiki.cern.ch/twiki/bin/view/P348/Manual_Alignment
  
  //WARNING: rough geometry measured with tape, accuracy ~cm
  ECAL_pos.pos = TVector3(360.,  0,     21528); // x=360. - very approximate estimate by AK
  ECAL0BEAMSPOT_pos = TVector3( 357., -8.2, 0);
  pipe_pos.pos = TVector3(0.,    0,     18633);
  //Magnet positions, based on tecnical drawing and rough understanding of magnet position, measured tube +60 mm
  MAGU_pos.pos = TVector3(     0,   0, 2230+60);//magnet upstream side
  #if 0
  /////////////////////////////////////
  /// this is the true magnet position,
  /// for simplicity we just use 4m magnet neglecting
  /// the gap to simplify calculations
  ///////////////////////////////////////////
  MAGD_pos.pos = TVector3(     0,   0,      8299.);//magnet downstream side
  #endif
  // fake magnet position
  MAGD_pos.pos = TVector3(     0,   0,      6290.);//magnet downstream side
  
  //MM position
  // X position preliminary based on aluminum profile, see report
  //upstream corrected using beamspot, downstream corrected using rough momentum assumption

  // after run 5034 MM were removed to perform a material budget test, alignment seems to have changed after then.
  //MM1_pos.pos  = TVector3(-4.3     ,         4.173,                 592);
  //MM2_pos.pos  = TVector3(1.2      ,         9.869,                1636);
  //MM3_pos.pos  = TVector3(311      ,         -3.235,               19657);
  //MM4_pos.pos  = TVector3(315      ,         -5.236,               19798);
  //MM5_pos.pos  = TVector3(344      ,          3.902,               20893);
  //MM6_pos.pos  = TVector3(343      ,          4.945,               21123);
  //MM7_pos.pos  = TVector3(0.,                     0,                   0);
  
  MM1_pos.pos  = TVector3(-4.4     ,          4.067,                 592);
  MM2_pos.pos  = TVector3(3.2      ,          9.899,                1636);
  MM3_pos.pos  = TVector3(314      ,         -3.235,               19657);
  MM4_pos.pos  = TVector3(318      ,         -5.236,               19798);
  MM5_pos.pos  = TVector3(347      ,          3.902,               20893);
  MM6_pos.pos  = TVector3(346      ,          4.945,               21123);
  MM7_pos.pos  = TVector3(0.,                     0,                   0);
  
  //MM angle
  MM1_pos.Angle =  -M_PI/4;
  MM2_pos.Angle =  -M_PI/4;
  MM3_pos.Angle =  -M_PI/4;
  MM4_pos.Angle = -3*M_PI/4;
  MM5_pos.Angle =  -M_PI/4;
  MM6_pos.Angle = -3*M_PI/4;
  MM7_pos.Angle = 0;
  MM1_pos.invertAxis = true;
  MM2_pos.invertAxis = true;
  MM3_pos.invertAxis = true;
  MM4_pos.invertAxis = true;
  MM5_pos.invertAxis = true;
  MM6_pos.invertAxis = true;
  MM7_pos.invertAxis = true;

  // GEMs, position extracted from preliminary software alignment from Micheal Hoesgen
  GM01Geometry = {
    /*size*/ 256,
    /*center*/ 0,
    /*pitch mm*/ 0.4,
    /*angle*/ 0.,
    /*abs pos mm*/ {-2.440777, -1.396702, 293.}
  };
  GM02Geometry = {
    /*size*/ 256,
    /*center*/ 0,
    /*pitch mm*/ 0.4,
    /*angle*/ 0.,
    /*abs pos mm*/ {-2.399352, -1.792811, 1373.}
  };
  
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  
  // STRAW
  // just a draft to define number of straw tubes per plane = 64 and tube diameter = 6.0 mm
  ST01Geometry = {64, 0, 6., 0, {0,0,0}};
  ST02Geometry = {64, 0, 6., 0, {0,0,0}};
  ST03Geometry = {64, 0, 6., 0, {0,0,0}};
  ST04Geometry = {64, 0, 6., 0, {0,0,0}};
  ST05Geometry = {64, 0, 6., 0, {0,0,0}};
  ST06Geometry = {64, 0, 6., 0, {0,0,0}};
  
  //magnetic field Tesla, preliminary based on centroid
  MAG_field = 1.74;
  
  // incoming beam
  NominalBeamEnergy = 100.;
  NominalBeamPDG = 11;

  //////////////////////////////////////
  //TODO: Additional detector measured but not currently used for Tracking:
  ////////////////////////////////
  
  // beam central cell
  ECAL0BEAMCELL = {2, 2};

} //end Geometry 2021 INVIS

// Geometry 2021B (muon mode)
void Init_Geometry_2021B()
{
  // ECAL is 5x6
  ECAL_pos.Nx = 5;
  ECAL_pos.Ny = 6;
  
  // HCAL is 6x3
  HCAL_pos.Nx = 6;
  HCAL_pos.Ny = 3;
  
  // TODO: complete absolute positions data
  
  // => BMS zone geometry (premilinary!), as extracted from:
  //   https://gitlab.cern.ch/hsieber/tracking-tools/-/blob/main/examples/example1/config/bms-config.json
  // The bms-config.json file is based on 'tape measurements':
  //   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2021B/Tape_measurements_trackers.pdf
  // zone drawing is slide 2 of
  //   https://indico.cern.ch/event/1088924/contributions/4577815/attachments/2332671/3975583/molina_NA64meetting_22102021.pdf
  MAG_field = 1.06;
  MAGU_pos.pos = TVector3(0.0, -12.5, -11996.550);
  MAGD_pos.pos = TVector3(0.0, -182.29, 3506.4500);
  MM1_pos.pos  = TVector3(-2.0, -9.2, -38112.0);
  MM2_pos.pos  = TVector3(4.5, -3.6, -37262.0);
  MM3_pos.pos  = TVector3(-17.2, -675.0, 21667.10);
  MM4_pos.pos  = TVector3(-18.0, -755.898, 22467.10);
  // <= BMS zone
  
  
  // MMs
  // rotations as extracted from coool/src/GroupMM.cc
  MM1_pos.Angle = -3*M_PI/4;
  MM2_pos.Angle =   -M_PI/4;
  MM3_pos.Angle = -3*M_PI/4;
  MM4_pos.Angle =   -M_PI/4;
  MM5_pos.Angle = 0.;
  MM6_pos.Angle = 0.;
  MM7_pos.Angle = 0.;
  
  // GEMs
  //              size  center  pitch[mm]  angle    abspos[mm]
  GM01Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  GM02Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  GM03Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  GM04Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // STRAW
  // no geometry as the only installed station was ST11 ("large"),
  // which is not supported by reco for the moment
  
  // beam central cell
  ECAL0BEAMCELL = {2, 3};
}

// Geometry 2022A (muon) 
void Init_Geometry_2022A()
{
  // ECAL is 5x6
  ECAL_pos.Nx = 5;
  ECAL_pos.Ny = 6;
  
  // HCAL is 6x3
  HCAL_pos.Nx = 6;
  HCAL_pos.Ny = 3;
  
  // NOTE: coordinate system is the one from upstream (survey was done for both upstream
  // and downstream coordinates system)
  
  MAG_field = 1.06;
  MAGU_pos.pos = TVector3(-0.35,-12.6,-5667.35-0.5*5001.0);
  MAGD_pos.pos = TVector3(0.85,-181.95,5911.5+0.5*5001.0);
  MM1_pos.pos  = TVector3(-10.8,-2.1,-27674.5);
  MM2_pos.pos  = TVector3(-9.3,-8.0,-26795.9);
  MM3_pos.pos  = TVector3(4.2,-768.9,25436.3);
  MM4_pos.pos  = TVector3(0.0,-805.8,26658.1);
  MM5_pos.pos  = TVector3(-50.625,-1629.175,54315.575);
  MM6_pos.pos  = TVector3(-59.925, -1640.5750,54659.325);
  MM7_pos.pos  = TVector3(-72.15,-1655.0250,55050.425);
  // <= BMS zone
  
  
  // MMs
  // rotations as extracted from coool/src/GroupMM.cc
  MM1_pos.Angle = -3*M_PI/4;
  MM2_pos.Angle =   -M_PI/4;
  MM3_pos.Angle = -3*M_PI/4;
  MM4_pos.Angle =   -M_PI/4;
  MM5_pos.Angle = 0.;
  MM6_pos.Angle = 0.;
  MM7_pos.Angle = 0.;
  
  // GEMs
  //              size  center  pitch[mm]  angle    abspos[mm]
  GM01Geometry = { 256,      0,       0.4,    0.,  {-18.025,-1432.25,47516.4} };
  GM02Geometry = { 256,      0,       0.4,    0.,  {-16.55,-1508.75,49030.75} };
  GM03Geometry = { 256,      0,       0.4,    0.,  {-7.325,-1540.075,50732.05} };
  GM04Geometry = { 256,      0,       0.4,    0.,  {22.625,-1545.3,51330.725} };
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // STRAW
  // just a draft to define number of straw tubes per plane = 64 and tube diameter = 6.0 mm
  ST01Geometry = {64, 0, 6., 0, {-2.9625,50.50,-28820.137}}; // ST05
  ST02Geometry = {64, 0, 6., 0, {-11.525,-8.90,-27950.40}}; // ST04
  ST04Geometry = {64, 0, 6., 0, {-1.075,-1543.60,50945.0}}; // ST03
  ST05Geometry = {64, 0, 6., 0, {-54.6375,-1646.7375,54859.012}}; // ST02
  // ST11 - not supported for the moment
  // ST11Geometry = {64/*to be changed*/, 0, 6., 0, {-249.7625,-1819.025,60066.500}}; // ST11

  // TODO: implement second magnet
  // Coordinates already accurate from survey
  // MAG_field = 1.4;
  // MAGU_pos.pos = TVector3(-4.450,-1584.30,52816.05-0.5*2000.);
  // MAGD_pos.pos = TVector3(-4.450,-1584.30,52816.05+0.5*2000.);
  
  // beam central cell
  ECAL0BEAMCELL = {2, 2};
}

void Init_Geometry_2022B(const int run)
{
  // ECAL is 5x6
  ECAL_pos.Nx = 5;
  ECAL_pos.Ny = 6;
  
  //latest(as of 27 Sept. 2022) geometry data prepared by Irina Tlisova is available at
  // https://twiki.cern.ch/twiki/pub/P348/Alignment2022B/2022-09-12,_report_update.pdf
  
  // front face of ECAL
  ECAL_pos.pos      = TVector3(-379.9,  8.5,   25109.1);
  // AK: visual inspection of beamspot XY plot at ECAL surface for run 7185
  ECAL0BEAMSPOT_pos = TVector3(-362.6, -8.0, 0.);
  
  pipe_pos.pos = TVector3(0.,    0,     22165.4);
  
  //true magnet position: in [mm] 
  //              X     Y      Z
  //BEND21 start  0.    0.    6350. 
  //BEND21   end  0.    0.    8350. 
  //BEND22 start  0.    0.    9363. 
  //BEND22   end  0.    0.   11363. 
  
  //equivalent magnet position for simple tracking
  //magnet upstream side: shifted half a meter dowstream to remove one meter gap between 2 MBPL magnets
  MAGU_pos.pos = TVector3(     0,   0,   6350.+506.5);
  //magnet downstream side, NOTE: fake position (just +4000. to upstream side)
  MAGD_pos.pos = TVector3(     0,   0,   6350.+506.5+4000.);
  
  //MM position: for 2022B latest data available
  MM1_pos.pos  = TVector3(       1.2,     -1.3,     4377.4);
  MM2_pos.pos  = TVector3(     -13.0,     -2.5,     5279.9);
  MM3_pos.pos  = TVector3(    -313.0,     -3.4,    23181.7);
  MM4_pos.pos  = TVector3(    -364.9,     -5.4,    24395.6);
  MM5_pos.pos  = TVector3(    -706.9,    -12.4,    42669.4);
  MM6_pos.pos  = TVector3(    -786.3,     -5.8,    46245.8);
  MM7_pos.pos  = TVector3(    -839.2,    -10.7,    47288.5);
  
  //MM angle
  MM1_pos.Angle = -3*M_PI/4;
  MM2_pos.Angle = -3*M_PI/4;
  MM3_pos.Angle = -3*M_PI/4;
  MM4_pos.Angle = -3*M_PI/4;
  MM5_pos.Angle =  0.;
  MM6_pos.Angle =  0.;
  MM7_pos.Angle =  0.;

  // GEMs
  //              size  center  pitch[mm]  angle    abspos[mm]
  GM01Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  GM02Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  GM03Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  GM04Geometry = { 256,      0,       0.4,    0.,  {0., 0., 0.} };
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // STRAW
  // just a draft to define number of straw tubes per plane = 64 and tube diameter = 6.0 mm
  ST01Geometry = {64, 0, 6.145, 0, {  -3.720919209,   -9.976705000,   4059.3}};
  ST02Geometry = {64, 0, 6.145, 0, {  -1.170995300,   -8.110670000,   5568.9}};
  ST03Geometry = {64, 0, 6.145, 0, {-322.094416700,  -10.486375500,  23760.2}};
  ST04Geometry = {64, 0, 6.145, 0, {-343.797578800,  -15.818265000,  24215.0}};
  ST05Geometry = {64, 0, 6.145, 0, {-536.200000000,  -11.900000000,  32879.0}};
  ST06Geometry = {64, 0, 6.145, 0, {-761.600000000,  -25.500000000,  42389.8}};

  // magnetic field
  MAG_field = 1.99; // Tesla
  
  // incoming beam
  NominalBeamEnergy = 100.;
  NominalBeamPDG = 11;
  
  // Positrons
  const bool isPositronStudyRuns = (8297 <= run && run <= 8323);
  if (isPositronStudyRuns){
      NominalBeamPDG = -11; //positron
  }

  // Hadron runs at 50 GeV
  const bool isHadronStudyRuns = (8396 <= run && run <= 8458);
  if (isHadronStudyRuns) {
    NominalBeamEnergy = 50.;
    NominalBeamPDG = -211;
  }
  
  // beam central cell
  ECAL0BEAMCELL = {2, 3};
}

void Init_Geometry_2023A(const int run)
{
  // ECAL is 5x6
  ECAL_pos.Nx = 5;
  ECAL_pos.Ny = 6;
  
  // front face of ECAL
  // Preliminary values based on tomography picture obtained with period2 beam trigger events
  // See: https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/426#note_7441134
  ECAL_pos.pos      = TVector3(-369,  4.0,   25151.16);
  ECAL0BEAMSPOT_pos = TVector3(-360.1, -15.82, 0.);
  
  pipe_pos.pos = TVector3(0.,    0,     22170.2);
  
  //true magnet position: in [mm] 
  //              X     Y      Z
  //BEND21 start  0.    0.    6350. 
  //BEND21   end  0.    0.    8350. 
  //BEND22 start  0.    0.    9363. 
  //BEND22   end  0.    0.   11363. 
  
  //equivalent magnet position for simple tracking
  //magnet upstream side: shifted half a meter dowstream to remove one meter gap between 2 MBPL magnets
  MAGU_pos.pos = TVector3(     0,   0,   6350.0+506.5);
  //magnet downstream side, NOTE: fake position (just +4000. to upstream side)
  MAGD_pos.pos = TVector3(     0,   0,   6350.0+506.5+4000.);
  
  //MM position: for 2023A latest data available
  MM1_pos.pos  = TVector3(   5.301042,    -1.959462,  4378.150); //2023A survey
  MM2_pos.pos  = TVector3( -11.062637,   -15.581022,  5283.350); //2023A survey
  MM3_pos.pos  = TVector3(-304.437426,    5.4751148, 22566.850); //2023A survey
  MM4_pos.pos  = TVector3(-360.87334 ,   -20.556789, 24456.225); //2023A survey
  MM5_pos.pos  = TVector3(-464.775   ,   -25.875  , 31106.817); //2023A survey
  MM6_pos.pos  = TVector3(-626.350   ,   -16.350  , 36552.725); //2023A survey
  MM7_pos.pos  = TVector3(-651.150   ,     0.750  , 37595.150); //2023A survey
  
  //MM angle
  MM1_pos.Angle = -3*M_PI/4;
  MM2_pos.Angle = -5*M_PI/4;
  MM3_pos.Angle = -3*M_PI/4;
  MM4_pos.Angle = -5*M_PI/4;
  MM5_pos.Angle =  M_PI;
  MM6_pos.Angle =  0.;
  MM7_pos.Angle =  0.;

  // GEMs
  //              size  center  pitch[mm]  angle    abspos[mm]
  GM01Geometry = { 256,      0,       0.4,    0.,  {-272.88853,  -7.7651318,  22909.000} };
  GM02Geometry = { 256,      0,       0.4,    0.,  {-358.87469,  10.6653010,  24327.275} };
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  
  // STRAW
  // just a draft to define number of straw tubes per plane = 64 and tube diameter = 6.0 mm
  ST01Geometry = {64, 0, 6.145, 0, { -0.273291, -6.65347464,  4057.125}};
  ST02Geometry = {64, 0, 6.145, 0, {  1.81544,  -5.735686,  5562.700}};
  ST03Geometry = {64, 0, 6.145, 0, { -323.10171, 7.90492, 22786.500}};
  ST04Geometry = {64, 0, 6.145, 0, { -339.559048, -19.63489, 24856.800}};
  ST05Geometry = {64, 0, 6.145, 0, { -483.175, -12.675, 32879.475}};
  ST06Geometry = {64, 0, 6.145, 0, { -489.900,  11.725, 33131.750}};
  ST11Geometry = {384, 0, 6.145, 0, {-579.500, 11.000, 41574.40}};
  ST12Geometry = {384, 0, 6.145, 0, {-766.20, 4.000, 44961.50}};

  
  // magnetic field
  MAG_field = 1.80; // Tesla
  
  // incoming beam
  NominalBeamEnergy = 100.;
  NominalBeamPDG = 11;
  
  // Alignment runs with hadrons at 100 GeV
  const bool isAlignmentRuns = ((8536 <= run && run <= 8547) || (9061 <= run && run <= 9066));

  // Positron runs at 70 GeV
  const bool isPositronRuns = (9557 <= run && run <= 9588);

  // Hadron runs with at 40 GeV
  const bool isHadronRuns = (9592 <= run && run <= 9714);

  if (isAlignmentRuns) {
    NominalBeamPDG = -211;
  } else if (isPositronRuns) {
    NominalBeamPDG = -11;
    NominalBeamEnergy = 70.;
  } else if (isHadronRuns) {
    MAG_field = 0.7434;
    NominalBeamPDG = -211;
    NominalBeamEnergy = 40.;
  }

  // beam central cell
  ECAL0BEAMCELL = {2, 2};
}

void Init_Geometry_2023B(const int run)
{
  // ECAL is 5x6
  ECAL_pos.Nx = 5;
  ECAL_pos.Ny = 6;
  
  // HCAL is 6x3
  HCAL_pos.Nx = 6;
  HCAL_pos.Ny = 3;
  
  // NOTE: coordinate system is the one from upstream (survey was done for both upstream
  // and downstream coordinates system)
  
  MAG_field = 1.06;
  MAGU_pos.pos  = TVector3(-0.35,-12.6,-5667.35-0.5*5001.0);
  MAGD_pos.pos  = TVector3(0.85,-181.95,5911.5+0.5*5001.0);
  MM1_pos.pos   = TVector3( -1.969217,         -826.348317,          -27475.397495);
  MM2_pos.pos   = TVector3( -4.933703,         -795.918793,          -26500.79323);
  MM3_pos.pos   = TVector3(  5.305094,           -0.901719,           25460.314838);
  MM4_pos.pos   = TVector3(  5.928936,           -4.413587,           26614.600277);
  MM5_pos.pos   = TVector3(163.18325638,         -1.5939173899999999, 59221.92525509455);
  MM6_pos.pos   = TVector3(179.11083849000002,    7.526187990000001,  59625.32511867867);
  MM7_pos.pos   = TVector3(173.47511599000003,   -0.94573085,         59931.325244298285);
  MM8_pos.pos   = TVector3( -5.4165390548526915,  7.852472914757856,  46728.50099198625);
  MM9_pos.pos   = TVector3(  8.780458478013959,  -3.1712747153014944, 47539.205004013806);
  MM10_pos.pos  = TVector3( 16.593825131918305,  -7.725084194201287,  50896.8028308164);
  MM11_pos.pos  = TVector3( 19.476328429785543,  -2.0676578567521022, 52150.65089364781);
  // <= BMS zone
  
  
  // MMs
  // rotations as extracted from coool/src/GroupMM.cc
  MM1_pos.Angle = -3*M_PI/4;
  MM2_pos.Angle = -5*M_PI/4;
  MM3_pos.Angle = -3*M_PI/4;
  MM4_pos.Angle = -5*M_PI/4;
  MM5_pos.Angle = M_PI;
  MM6_pos.Angle = M_PI;
  MM7_pos.Angle = M_PI;
  MM8_pos.Angle = -3*M_PI/4;
  MM9_pos.Angle = -5*M_PI/4;
  MM10_pos.Angle = -3*M_PI/4;
  MM11_pos.Angle = -5*M_PI/4;
  // Due to the space limitation the MM11 is not facing the beam, unlike the other ones.
  // So we need to manually flip the x axis. This was also confirmed with simulation.
  MM11_pos.invertAxis = true;

  
  // GEMs
  //              size  center  pitch[mm]  angle    abspos[mm]
  GM01Geometry = { 256,      0,       0.4,    0.,  {9.683917046862181,  23.967826929999998, 53653.17471651102} };
  GM02Geometry = { 256,      0,       0.4,    0.,  {27.851321349119683, 29.55876738,        54004.29642653527} };
  GM03Geometry = { 256,      0,       0.4,    0.,  {13.088300908488847, 14.323233559999998, 55462.30362852066} };
  GM04Geometry = { 256,      0,       0.4,    0.,  {30.067926570225133, 15.73687351,        56068.1013614346} };
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // STRAW
  // just a draft to define number of straw tubes per plane = 64 and tube diameter = 6.0 mm
  //                                              actual position along the beamline
  ST01Geometry = { 64, 0, 6.145, 0, { 24.500616,     7.840502,  52534.189997}}; // sixth ST
  ST02Geometry = { 64, 0, 6.145, 0, { 40.607247,     3.109637,  50501.90669}}; // fifth ST
  ST03Geometry = { 64, 0, 6.145, 0, {-10.224161,    -3.202470,  47215.711933}}; // fourth ST
  ST04Geometry = { 64, 0, 6.145, 0, { -1.05901,     12.390963,  25820.8125}}; // third ST
  ST05Geometry = { 64, 0, 6.145, 0, { -4.032402,  -864.738126, -28532.876106}}; // first ST
  ST06Geometry = { 64, 0, 6.145, 0, {  3.728638,  -855.297876, -28072.137947}}; // second ST
  ST11Geometry = {384, 0, 6.145, 0, {336.53849098, -20.494041,  60214.562565}}; // ST11
  ST12Geometry = {384, 0, 6.145, 0, {213.752983,   -43.345348,  65364.625013}}; // ST12


  // TODO: implement second magnet
  // Coordinates already accurate from survey
  // MAG_field = 1.4;
  // MAGU_pos.pos = TVector3(-4.450,-1584.30,52816.05-0.5*2000.);
  // MAGD_pos.pos = TVector3(-4.450,-1584.30,52816.05+0.5*2000.);
  
  // beam central cell
  ECAL0BEAMCELL = {2, 2};

  // RUN FLAGS
  // run periods with empty target
  const bool isHadronContamRuns        = (run >=  9774 && run <=  9855);   // hadron contamination runs
  const bool isTestRuns                = (run >=  9867 && run <=  9868);   // test runs 
  const bool isTriggerStudyRuns        = (run >=  9878 && run <=  9885);   // checks with S4/Smu in/out of trigger
  const bool isBeamTrajectoryStudyRuns = (run >= 10287 && run <= 10294);   // runs to study beam for different combinations of MS2 on/off and ECAl in/out
  const bool is80gevRuns               = (run >=  9861 && run <=  9862) || 
                                         (run >=  9952 && run <=  9953);
  const bool isHadronRuns              = (run >=  9858 && run <=  9865) || // Note: run 9863 is a bad run
                                         (run >=  9950 && run <=  9953);   // hadron runs, no ECAL
  
  // EMPTY TARGET RUNS IN PRODUCTION MODE
  // for more information about trigger IDs, see also https://twiki.cern.ch/twiki/pub/P348/RunsInfo2023B/list_runs_July_2023.pdf pg. 3, 5, 8
  const bool isEmptyTargetTrig2022Runs = (run >=  9869 && run <=  9875);   // empty target runs with trigger 2022 configuration (S0xS1xV0xS2xS4xSmu)
  const bool isEmptyTargetTrig2Runs    = (run >= 10150 && run <= 10169);   // empty target runs with trigger 2 configuration    (S0xS1xV0xS2xS4xSmuxBK)

  // all runs that require empty target placement/geometry file
  const bool isEmptyTarget = isHadronContamRuns || isEmptyTargetTrig2022Runs || isHadronRuns || isEmptyTargetTrig2Runs || isBeamTrajectoryStudyRuns || isTriggerStudyRuns || isTestRuns;

  // based on residual, it appears ST11 was shifted by ~4mm between run 9925 and run 9926
  const bool isST11moved = (run!=0 && run<=9925); 

  // runs with currents on magnet deviating from nominal value
  const bool isMS1At320A = run==9952 || run==9953; // hadrons at 80 GeV, MS1 current set at half to obtain similar deflection as for 160 GeV for incoming beam at nominal current
  const bool isMS2At400A = run==9953;              // hadrons at 80 GeV, MS2 current set at half to obtain similar deflection as for 160 GeV for outgoing beam at nominal current
  
  // PARAMETERS
  NominalBeamEnergy = is80gevRuns ? 80. : 160.;
  NominalBeamPDG = isHadronRuns ? 211 : -13;
  
  string placements_path =   isEmptyTarget ? conddbpath() + "trackingtools/2023mu/placements_ecalout.json" 
                           : isST11moved   ? conddbpath() + "trackingtools/2023mu/placements_trig1.json" 
                           :                 conddbpath() + "trackingtools/2023mu/placements.json";
  
  string pathToMS1at300Amap = conddbpath()+"magfields-2023B/FM_MS1_MBP_300A_50x50.genfit"; // field map simulated for 300A, but set/measured current was 320A
  string pathToMS1at850Amap = conddbpath()+"magfields-2023B/FM_MS1_MBP_850A_50x50.genfit";

  string geometry_path = isEmptyTarget ? conddbpath() + "trackingtools/2023mu/geometry_ecalout.gdml" : 
                                         conddbpath() + "trackingtools/2023mu/geometry.gdml";

  // nominal magnetic field strength from field maps [T]
  double B_NomFieldMap_MS1 = 1.75;
  double B_NomFieldMap_MS2 = 1.4;

  // measured magnetic field strength [T]
  double B_meas_MS1 =  1.65;
  double B_meas_MS2 = -1.50; 

  // field strength scaling factors for half-current configurations
  // estimation based on genfit extrapolation, preliminary values
  // TODO: verify
  double B_MS1At320A =  1.1424; //1.2 before // field map value is 0.8T -> scaled to 0.914T
  double B_MS2At400A = -0.6; // field map value is 1.4T -> scaled to -0.84T
  
  MagInfo BMS, MS1, MS2;

  //TODO: retrieve BMS dimensions (this is a simplified model, as consists of three magnets)
  BMS.name         = "BEND6";
  BMS.B            = 6; //account for different currents of magnet
  BMS.L            = 6000.;
  BMS.gap_width    = 300.;
  BMS.gap_height   = 140.;
  BMS.entrance     = TVector3(-1.2, 0.0, 48088.9);
  BMS.exit         = TVector3( 5.9, 0.2, 50088.9);

  MS1.name         = "MBP1";
  MS1.B            = isMS1At320A ? 0.914 : 1.65; //account for different currents of magnet
  MS1.L            = 2000.;
  MS1.gap_width    = 300.;
  MS1.gap_height   = 140.;
  MS1.entrance     = TVector3(-1.2, 0.0, 48088.9);
  MS1.exit         = TVector3( 5.9, 0.2, 50088.9);
  MS1.field_factor = isMS1At320A ? B_MS1At320A : B_meas_MS1/B_NomFieldMap_MS1;
  MS1.pathToMap    = isMS1At320A ? pathToMS1at300Amap : pathToMS1at850Amap;

  MS2.name         = "MBP2";
  MS2.B            = isMS2At400A ? 0.84 : 1.5; //account for different currents of magnet
  MS2.L            = 2000.;
  MS2.gap_width    = 300.;
  MS2.gap_height   = 200.;
  MS2.entrance     = TVector3(50.7, 0.3, 56588.5);
  MS2.exit         = TVector3(70.8, 0.4, 58588.4);
  MS2.field_factor = isMS2At400A ? B_MS2At400A : B_meas_MS2/B_NomFieldMap_MS2;
  MS2.pathToMap    = conddbpath()+"magfields-2023B/FM_MS2_MBP_50x50.genfit";

  geo.add_magnet( BMS );
  geo.add_magnet( MS1 );
  geo.add_magnet( MS2 );
  geo.placements_path = placements_path;
  geo.geometry_path   = geometry_path;

}

// TODO: Replace with actual values. For the moment this is a copy of 2023A
void Init_Geometry_2024A(const int run)
{
  // ECAL is 5x6
  ECAL_pos.Nx = 5;
  ECAL_pos.Ny = 6;
  
  // front face of ECAL
  // Preliminary values based on tomography picture obtained with period2 beam trigger events
  // See: https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/426#note_7441134
  ECAL_pos.pos      = TVector3(-369,  4.0,   25151.16);
  ECAL0BEAMSPOT_pos = TVector3(-360.1, -15.82, 0.);

  pipe_pos.pos = TVector3(0.,    0,     22170.2);

  //true magnet position: in [mm] 
  //              X     Y      Z
  //BEND21 start  0.    0.    6350.
  //BEND21   end  0.    0.    8350.
  //BEND22 start  0.    0.    9363.
  //BEND22   end  0.    0.   11363.

  //equivalent magnet position for simple tracking
  //magnet upstream side: shifted half a meter dowstream to remove one meter gap between 2 MBPL magnets
  MAGU_pos.pos = TVector3(     0,   0,   6350.0+506.5);
  //magnet downstream side, NOTE: fake position (just +4000. to upstream side)
  MAGD_pos.pos = TVector3(     0,   0,   6350.0+506.5+4000.);

  //MM position: for 2023A latest data available
  MM1_pos.pos  = TVector3(   5.301042,    -1.959462,  4378.150); //2023A survey
  MM2_pos.pos  = TVector3( -11.062637,   -15.581022,  5283.350); //2023A survey
  MM3_pos.pos  = TVector3(-304.437426,    5.4751148, 22566.850); //2023A survey
  MM4_pos.pos  = TVector3(-360.87334 ,   -20.556789, 24456.225); //2023A survey
  MM5_pos.pos  = TVector3(-464.775   ,   -25.875  , 31106.817); //2023A survey
  MM6_pos.pos  = TVector3(-626.350   ,   -16.350  , 36552.725); //2023A survey
  MM7_pos.pos  = TVector3(-651.150   ,     0.750  , 37595.150); //2023A survey

  //MM angle
  MM1_pos.Angle = -3*M_PI/4;
  MM2_pos.Angle = -5*M_PI/4;
  MM3_pos.Angle = -3*M_PI/4;
  MM4_pos.Angle = -5*M_PI/4;
  MM5_pos.Angle =  M_PI;
  MM6_pos.Angle =  0.;
  MM7_pos.Angle =  0.;

  // GEMs
  //              size  center  pitch[mm]  angle    abspos[mm]
  GM01Geometry = { 256,      0,       0.4,    0.,  {-272.88853,  -7.7651318,  22909.000} };
  GM02Geometry = { 256,      0,       0.4,    0.,  {-358.87469,  10.6653010,  24327.275} };
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;

  // STRAW
  // just a draft to define number of straw tubes per plane = 64 and tube diameter = 6.0 mm
  ST01Geometry = {64, 0, 6.145, 0, { -0.273291, -6.65347464,  4057.125}};
  ST02Geometry = {64, 0, 6.145, 0, {  1.81544,  -5.735686,  5562.700}};
  ST03Geometry = {64, 0, 6.145, 0, { -323.10171, 7.90492, 22786.500}};
  ST04Geometry = {64, 0, 6.145, 0, { -339.559048, -19.63489, 24856.800}};
  ST05Geometry = {64, 0, 6.145, 0, { -483.175, -12.675, 32879.475}};
  ST06Geometry = {64, 0, 6.145, 0, { -489.900,  11.725, 33131.750}};
  ST11Geometry = {384, 0, 6.145, 0, {-579.500, 11.000, 41574.40}};
  ST12Geometry = {384, 0, 6.145, 0, {-766.20, 4.000, 44961.50}};

  // magnetic field
  MAG_field = 1.80; // Tesla

  // incoming beam
  NominalBeamEnergy = 100.;
  NominalBeamPDG = 11;
 
  // beam central cell
  ECAL0BEAMCELL = {2, 2};
}

// DRAFT BASED ON Geometry 2023B (muon) 
void Init_Geometry_2024B(const int run)
{
  // ECAL is 5x6
  ECAL_pos.Nx = 5;
  ECAL_pos.Ny = 6;
  
  // HCAL is 6x3
  HCAL_pos.Nx = 6;
  HCAL_pos.Ny = 3;
  
  // NOTE: coordinate system is the one from upstream (survey was done for both upstream
  // and downstream coordinates system)
  
  MAG_field = 1.06;
  MAGU_pos.pos  = TVector3(-0.35,-12.6,-5667.35-0.5*5001.0);
  MAGD_pos.pos  = TVector3(0.85,-181.95,5911.5+0.5*5001.0);
  MM1_pos.pos   = TVector3( -1.969217,         -826.348317,          -27475.397495);
  MM2_pos.pos   = TVector3( -4.933703,         -795.918793,          -26500.79323);
  MM3_pos.pos   = TVector3(  5.305094,           -0.901719,           25460.314838);
  MM4_pos.pos   = TVector3(  5.928936,           -4.413587,           26614.600277);
  MM5_pos.pos   = TVector3(163.18325638,         -1.5939173899999999, 59221.92525509455);
  MM6_pos.pos   = TVector3(179.11083849000002,    7.526187990000001,  59625.32511867867);
  MM7_pos.pos   = TVector3(173.47511599000003,   -0.94573085,         59931.325244298285);
  MM8_pos.pos   = TVector3( -5.4165390548526915,  7.852472914757856,  46728.50099198625);
  MM9_pos.pos   = TVector3(  8.780458478013959,  -3.1712747153014944, 47539.205004013806);
  MM10_pos.pos  = TVector3( 16.593825131918305,  -7.725084194201287,  50896.8028308164);
  MM11_pos.pos  = TVector3( 19.476328429785543,  -2.0676578567521022, 52150.65089364781);
  // <= BMS zone
  
  
  // MMs
  // rotations as extracted from coool/src/GroupMM.cc
  MM1_pos.Angle = -3*M_PI/4;
  MM2_pos.Angle = -5*M_PI/4;
  MM3_pos.Angle = -3*M_PI/4;
  MM4_pos.Angle = -3*M_PI/4;
  MM5_pos.Angle = -5*M_PI/4;
  MM6_pos.Angle = -5*M_PI/4;
  MM7_pos.Angle = -3*M_PI/4;
  MM8_pos.Angle = -5*M_PI/4;
  MM9_pos.Angle = -3*M_PI/4;
  MM10_pos.Angle = -5*M_PI/4;
  MM11_pos.Angle = M_PI;
  MM12_pos.Angle = M_PI;
  MM13_pos.Angle = M_PI;

  
  // GEMs
  //              size  center  pitch[mm]  angle    abspos[mm]
  GM01Geometry = { 256,      0,       0.4,    0.,  {9.683917046862181,  23.967826929999998, 53653.17471651102} };
  GM02Geometry = { 256,      0,       0.4,    0.,  {27.851321349119683, 29.55876738,        54004.29642653527} };
  GM03Geometry = { 256,      0,       0.4,    0.,  {13.088300908488847, 14.323233559999998, 55462.30362852066} };
  GM04Geometry = { 256,      0,       0.4,    0.,  {30.067926570225133, 15.73687351,        56068.1013614346} };
  // propagate to alternative GEM geometry
  GM1_pos.pos  = GM01Geometry.pos;
  GM2_pos.pos  = GM02Geometry.pos;
  GM3_pos.pos  = GM03Geometry.pos;
  GM4_pos.pos  = GM04Geometry.pos;
  
  // STRAW
  // just a draft to define number of straw tubes per plane = 64 and tube diameter = 6.0 mm
  //                                              actual position along the beamline
  ST01Geometry = { 64, 0, 6.145, 0, { 24.500616,     7.840502,  52534.189997}}; // sixth ST
  ST02Geometry = { 64, 0, 6.145, 0, { 40.607247,     3.109637,  50501.90669}}; // fifth ST
  ST03Geometry = { 64, 0, 6.145, 0, {-10.224161,    -3.202470,  47215.711933}}; // fourth ST
  ST04Geometry = { 64, 0, 6.145, 0, { -1.05901,     12.390963,  25820.8125}}; // third ST
  ST05Geometry = { 64, 0, 6.145, 0, { -4.032402,  -864.738126, -28532.876106}}; // first ST
  ST06Geometry = { 64, 0, 6.145, 0, {  3.728638,  -855.297876, -28072.137947}}; // second ST
  ST11Geometry = {384, 0, 6.145, 0, {336.53849098, -20.494041,  60214.562565}}; // ST11
  ST12Geometry = {384, 0, 6.145, 0, {213.752983,   -43.345348,  65364.625013}}; // ST12
  ST13Geometry = {384, 0, 6.145, 0, {213.752983,   -43.345348,  65364.625013}}; // ST13


  // TODO: implement second magnet
  // Coordinates already accurate from survey
  // MAG_field = 1.4;
  // MAGU_pos.pos = TVector3(-4.450,-1584.30,52816.05-0.5*2000.);
  // MAGD_pos.pos = TVector3(-4.450,-1584.30,52816.05+0.5*2000.);

  // TODO: define run groups
  const bool is80gevRuns = false;
  const bool isHadronRuns = false;

  NominalBeamEnergy = is80gevRuns ? 80. : 160.;
  NominalBeamPDG = isHadronRuns ? 211 : -13;
  
  // beam central cell
  ECAL0BEAMCELL = {2, 2};
}

//#include "led.h"
//#include "beamOnlyCorrection.h"

void copy_calo_cell_calib( CaloCalibData & dest, const p348::CaloCellCalib & src )
{
  /*
  cout << "copy_calo_cell_calib():"
       << " src.name=" << src.name
       << " src.factor=" << src.factor
       << " src.peakpos=" << src.peakpos
       << endl;
  */
  
  // CaloCalibData::name is cell name and set in Reset_Calib()
  // CaloCellCalib::name is detector name
  //dest.name = src.name;

  // propagate energy and time calibration constants
  // Note that here per-part specification takes precedence for factor
  dest.K = src.factor/src.peakpos;
  if (isnan(dest.K)) dest.K = 1.;  // missing calibration
  
  // TODO: propagate factor and peakpos into CaloCalibData
  // TODO: add CaloCellCalib::has_amplitude
  
  if(src.have_time) {
    dest.tmean = src.time;
    dest.tsigma = src.timesigma;
  } else {
    dest.tsigma = 9999.;
  }
}

void Init_SADC_Calibs(int runNo) {
  // calorimeter calibrations
  auto caloCalibs = load_calib(conddbpath() + "calib/", runNo);

  for( auto cc : caloCalibs ) {
    size_t n;
    for(n = 0; n < cc.name.size() && ! std::isdigit(cc.name[n]); ++n) {;}
    const string detKin = cc.name.substr(0, n);
    
    /*
    cout << "Init_SADC_Calibs() name=" << cc.name << " x=" << cc.x << " y=" << cc.y
         << " have_ledref=" << cc.have_ledref << " ledref=" << cc.ledref
         << endl;
    */
    
    if("ECAL" == detKin) {
      assert(cc.z < 2 );
      assert(cc.x < 12);
      assert(cc.y < 6 );
      copy_calo_cell_calib( ECAL_calibration[cc.z][cc.x][cc.y], cc );
      if (cc.have_ledref) ledref.ecal[cc.z][cc.x][cc.y] = cc.ledref;
      //{
      //  const auto & c = ECAL_calibration[cc.z][cc.x][cc.y];
      //  cout << c << std::endl;
      //}
    } else if("HCAL" == detKin) {
      assert(cc.x < 6);
      assert(cc.y < 3);
      assert(cc.z < 4);
      copy_calo_cell_calib( HCAL_calibration[cc.z][cc.x][cc.y], cc );
      if (cc.have_ledref) ledref.hcal[cc.z][cc.x][cc.y] = cc.ledref;
    }
    else if("VHCAL" == detKin) {
      assert(cc.z < 2);
      copy_calo_cell_calib( VHCAL_calibration[cc.z][cc.x][cc.y], cc );
      if (cc.have_ledref) ledref.vhcal[cc.z][cc.x][cc.y] = cc.ledref;
    }
    else if("PKRCAL" == detKin){
      assert(cc.z < 2);
      if (cc.z == 0){
        copy_calo_cell_calib(PKRCAL0_calibration[cc.x][cc.y], cc );
      }
      else{
        copy_calo_cell_calib(PKRCAL1_calibration[cc.x][cc.y], cc );
      }
    }
    else if ("VETO" == detKin) {
      copy_calo_cell_calib(VETO_calibration[cc.x], cc);
    }
    else if ("SRD" == detKin) {
      copy_calo_cell_calib(SRD_calibration[cc.x], cc);
    }
    else if ("LYSO" == detKin) {
      assert(cc.x < 6 );
      copy_calo_cell_calib(LYSO_calibration[cc.x], cc);
    }
    else if ("WCAT" == detKin) {
      copy_calo_cell_calib(WCAT_calibration, cc);
    }
    else if ("S4" == cc.name) { // do not use detKin to avoid grouping trigger scintillators into one kin "S"
      copy_calo_cell_calib(S4_calibration, cc);
    }
    else if ("Smu" == detKin) {
      copy_calo_cell_calib(Smu_calibration, cc);
    }
    else if ("BeamK" == detKin) {
      copy_calo_cell_calib(BeamK_calibration, cc);
    }
    else {
      throw std::runtime_error("ERROR: Init_SADC_Calibs() Can not handle calibration for \"" + cc.name + "\"");
    }
  }
}

// print calorimeters calibration data
// TODO: include the rest of calo. cells
void print_calib()
{
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 5; x++)
      for (int y = 0; y < 6; y++) {
        CaloCalibData& c = ECAL_calibration[d][x][y];
        cout << c << endl;
      }
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; x++)
      for (int y = 0; y < 3; y++) {
        CaloCalibData& c = HCAL_calibration[d][x][y];
        cout << c << endl;
      }
  
  for (int i = 0; i < 6; i++) {
    CaloCalibData& c = VETO_calibration[i];
    cout << c << endl;
  }

  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 4; x++)
      for (int y = 0; y < 4; y++) {
        CaloCalibData& c = VHCAL_calibration[d][x][y];
        cout << c << endl;
      }
}

void Init_Reconstruction(const CS::DaqEventsManager& manager)
{

  const int run=manager.GetEvent().GetRunNumber();
  const time_t time=manager.GetEvent().GetTime().first;
  const bool isDryRun = (run > 900000);

  // explicitly set time zone of NA64 experiment location (CERN-Prevessin / France)
  // as part of calibration data have timestamps in local time
  // (example - function FindApvPedestalFile())
  setenv("TZ", "Europe/Paris", 1);
  
  char timestamp[50];
  strftime(timestamp, 50, "%Y-%m-%d %H:%M:%S %Z", localtime(&time));
  cout << "INFO: Init_Reconstruction(): loading conditions for run " << run << ", time " << timestamp << endl;
  
  Reset_Calib();
  Init_ADC(manager);
  // test beam 2015: runs range 330 - 629
  const bool is2015 = (330 <= run && run <= 629);
  if (is2015) Init_Calib_2015_BGO_ECAL_HCAL();
  
  // test beam 2016A: runs range 938 - 1600
  const bool is2016A = (938 <= run && run <= 1600);
  if (is2016A) {
    if (run <= 1371) Init_Calib_2016A_ECAL_cal1();
    if (1372 <= run) Init_Calib_2016A_ECAL_cal2();
    if (1130 <= run) Init_Calib_2016A_BGO();
    if (1456 <= run) Init_Calib_2016A_SRD();
    if (1115 <= run) Init_Calib_2016A_HCAL_cal2();
    Init_Calib_2016_GEM(time);
    if (run <= 1426) Init_Geometry_2016A_part1();
    if (1427 <= run) Init_Geometry_2016A_part2();
  }
  
  // test beam 2016B: runs range 1776 - 2551
  const bool is2016B = (1776 <= run && run <= 2551);
  if (is2016B) {
    Init_Calib_2016B_ECAL();
    Init_Calib_2016B_HCAL();
    Init_Calib_2016B_SRD();
    Init_Calib_2016B_VETO();
    Corr_Calib_2016B(run);
    if (2458 <= run) Init_Calib_2016B_VISMODE();
    Init_Calib_2016B_MM(time);
    Init_Calib_2016_GEM(time);
    Init_Geometry_2016B();
  }
  
  // test beam 2017: runs range 2817 - 3573
  const bool is2017 = (2817 <= run && run <= 3573);
  if (is2017) {
    Init_Calib_2017_ECAL();
    Init_Calib_2017_VETO();
    Init_Calib_2017_HCAL();
    Init_Calib_2017_WCAL(run);
    Init_Calib_2017_SRD();
    Init_Calib_2017_MM(time);
    Init_Calib_2017_GEM(time);
    Init_Calib_2017_MasterTime(run);
    Init_Geometry_2017();
    Init_Calib_2017_Counters();
  }

  // test beam 2018: runs range from 3574 - 4310
  const bool is2018 = (3574 <= run && run <= 4310);
  if (is2018) {
    if (3855 <= run && run <= 4207) Init_Geometry_2018_invis100();
    //geometry for visible mode 2018, exact position to be checked with tracking
    if (4224 <= run) Init_Geometry_2018_vis150();
    
    if (3855 <= run) Init_Calib_2018_MM(time);
    if (3855 <= run) Init_Calib_2018_GEM(time);
    
    // TODO: check range
    if (3870 <= run) Init_Calib_2018_ECAL(run);
    if (3870 <= run) Init_Calib_2018_HCAL(run);
    if (3870 <= run) Init_Calib_2018_SRD(run);
    if (3870 <= run) Init_Calib_2018_VETO(run);
    if (3870 <= run) Init_Calib_2018_WCAL(run);
    Init_Calib_2018_MasterTime(run);
    Init_Calib_2018_Counters(run);
    
    // activate new t0 reco method for 2018 session
    SADC_t0_method = kT0_RisingEdgeHH;
    
    if (3870 <= run && SADC_correction_method == kAmplitude_Manual) {
      Init_Calib_2018_ECAL_Manual(run);
      Init_Calib_2018_HCAL_Manual(run);
      Init_Calib_2018_WCAL_Manual(run);
    }
    
    if (SADC_correction_method == kAmplitude_LED) {
      Init_LED_calibrations_2018();
      Init_Calib_2018_WCAL_AfterLed(run);
    }
  }
  
  // => session 2021A, runs range 4642 - 5187
  const bool is2021A = (4642 <= run && run <= 5187);
  if (is2021A) {
    // calorimetry
    Init_SADC_Calibs(run);
    Init_Calib_2021_MasterTime(run);
    SADC_t0_method = kT0_RisingEdgeHH;
    // tracking detectors
    Init_Geometry_2021();
    Init_Calib_2021_MM(time);
    Init_Calib_2021_GEM(time);
  }
  
  // => session 2021B (muon mode), runs 5188 - 5572
  const bool is2021B = (5188 <= run && run <= 5572);
  if (is2021B) {
    // calorimetry
    Init_SADC_Calibs(run);
    SADC_t0_method = kT0_RisingEdgeHH;
    // tracking detectors
    Init_Geometry_2021B();
    Init_Calib_2021B_MM(time);
    Init_Calib_2021B_GEM(time);
    Init_Calib_2022A_ST();
  }
  
  // => session 2022A (muon), runs 5575-6034
  const bool is2022A = (5575 <= run && run <= 6034);
  if (is2022A) {
    // calorimetry
    Init_SADC_Calibs(run);
    SADC_t0_method = kT0_RisingEdgeHH;
    // tracking detectors
    Init_Geometry_2022A();
    Init_Calib_2022A_MM(time);
    Init_Calib_2022A_GEM(time);
    Init_Calib_2022A_ST();
  }
  
  // => session 2022B (electron), runs 6035-8458
  const bool is2022B = (6035 <= run && run <= 8458);
  if (is2022B) {
    // calorimetry
    Init_SADC_Calibs(run);
    SADC_t0_method = kT0_RisingEdgeHH;
    if (SADC_correction_method == kAmplitude_LED) {
      Init_LED_calibrations_2022B(run);
    }
    
    // tracking detectors
    Init_Geometry_2022B(run);
    Init_Calib_2022B_MM(time, run);
    Init_Calib_2022B_GEM(time);
    Init_Calib_2022B_ST(run);
  }
  
  // => session 2023A (electron), runs 8459-9717
  const bool is2023A = (8459 <= run && run <= 9717);
  if (is2023A) {
    // calorimetry
    Init_SADC_Calibs(run);
    SADC_t0_method = kT0_RisingEdgeHH;
    
    // Change signal zone starting sample (10 -> 8)
    // to allow processing of early ECAL waveforms (at least runs ~8600).
    // The latency was adjusted and this fix is no longer needed for the latter runs (from ~8700).
    // NOTE: this affects processing of all SADC subdetectors and full runs range of is2023A,
    //       currently (2024-september) there is the possibility to narrow the override to only particular subdetector/runs range,
    //       but keeping the original fix to have the physics results reproducible
    sadc_ADCdata.signal_range_begin = 8;
    Init_ADC(manager);  //need to call Init_ADC again to make the previous change effective
    
    if (SADC_correction_method == kAmplitude_LED) {
      Init_LED_calibrations_2023A(run);
    }
    
    const bool isPeriod1 = (8663 <= run) && (run <= 8746);
    const bool isPeriod2 = (8747 <= run) && (run <= 8939);
    const bool isPeriod3 = (8940 <= run) && (run <= 9161);
    const bool isPeriod4 = (9252 <= run) && (run <= 9427);
    const bool isPeriod5 = (9431 <= run) && (run <= 9556);
    const bool isPeriodPosi = (9557 <= run) && (run <= 9588) && (run != 9572);
    if (isPeriod1 || isPeriod2 || isPeriod3 || isPeriod4 || isPeriod5 || isPeriodPosi) {
      Init_BeamOnly_corrections_2023A(run);
    }

    /* A.C. temporary comment, to show how to enable the waveform fitting.
    if (isPeriodPosi){
      for (int ix=0;ix<12;ix++){
        for (int iy=0;iy<6;iy++){
          ECAL_calibration[0][ix][iy].doWaveformFit=true;
          ECAL_calibration[1][ix][iy].doWaveformFit=true;
        }
      }
    }*/

    // tracking detectors
    Init_Geometry_2023A(run);
    Init_Calib_2023A_MM(time);
    Init_Calib_2022B_GEM(time);
    Init_Calib_2023A_ST(run);
  }

  // => session 2023B (muon), runs 9718-10315
  const bool is2023B = (9718 <= run && run <= 10315);
  if (is2023B) {
    // calorimetry
    Init_SADC_Calibs(run);
    SADC_t0_method = kT0_RisingEdgeHH;

    // tracking detectors
    // TODO: dummy definitions from prev. sessions, to be corrected
    Init_Geometry_2023B(run);
    Init_Calib_2023B_MM(time, run);
    Init_Calib_2022A_GEM(time);
    Init_Calib_2023B_ST();
  }

  // => session 2024A (electron), runs 10316-11467
  // => first part of this session (10136-10409) have ECAL connected to WB for readout
  const bool is2024A = (10316 <= run && run <= 11467);
  const bool is2024A_WB = (10316 <= run && run <= 10409);
  if (is2024A) {
    // calorimetry
    Init_SADC_Calibs(run);
    SADC_t0_method = kT0_RisingEdgeHH;

    // tracking detectors
    Init_Geometry_2024A(run);
    Init_Calib_2024A_MM(time, run);
    Init_Calib_2022B_GEM(time);
    Init_Calib_2024A_ST();

    //WB-specific checks (early part of 2024 run)
    if (is2024A_WB){
      wb_ADCdata.pedestal_length=16;
      wb_ADCdata.signal_range_begin = 20;
      wb_ADCdata.signal_range_end = 50;
      Init_ADC(manager); //need to call Init_ADC() again to make above changes effective
    } else {
      //A.C. 2024 non WB fit waveforms
      for (int ix = 0; ix < 12; ix++) {
        for (int iy = 0; iy < 6; iy++) {
          ECAL_calibration[0][ix][iy].doWaveformFit = true;
          ECAL_calibration[1][ix][iy].doWaveformFit = true;
        }
      }
      for (int ix = 0; ix < 3; ix++) {
        for (int iy = 0; iy < 3; iy++) {
          HCAL_calibration[0][ix][iy].doWaveformFit = true;
          HCAL_calibration[1][ix][iy].doWaveformFit = true;
          HCAL_calibration[2][ix][iy].doWaveformFit = true;
          HCAL_calibration[3][ix][iy].doWaveformFit = true;
        }
      }
      for (int iv = 0; iv < 6; iv++)
        VETO_calibration[iv].doWaveformFit = true;
      for (int is = 0; is < 3; is++)
        SRD_calibration[is].doWaveformFit = true;
      for (int ix = 0; ix < 4; ix++) {
        for (int iy = 0; iy < 4; iy++) {
          VHCAL_calibration[0][ix][iy].doWaveformFit = true;
        }
      }
      S2_calibration.doWaveformFit = true;
      S3_calibration.doWaveformFit = true;
      V1_calibration.doWaveformFit = true;
      
      //A.C. 2024 non WB LED calibrations
      if (SADC_correction_method == kAmplitude_LED) {
        Init_LED_calibrations_2024A(run);
      }
    }
  }

  // => session 2024B (muon), runs 11468-12263
  const bool is2024B = (11468 <= run && run <= 12263);
  if (is2024B || isDryRun) {
    // calorimetry
    Init_SADC_Calibs(run);
    SADC_t0_method = kT0_RisingEdgeHH;
    // BB: During the 2024B muon session, in several MSADC channels the waveforms start at time sample 8
    // These changes were proposed by V. Poliakov and are used by S. Donskov for waveform
    // reconstruction.
    sadc_ADCdata.pedestal_length = 6;
    sadc_ADCdata.signal_range_begin = 8;
    Init_ADC(manager);

    // tracking detectors
    Init_Geometry_2024B(run);
    
    Init_Calib_2024B_MM(time, run);
    Init_Calib_2022A_GEM(time);
    Init_Calib_2024B_ST();
  }

  //print_calib();
}

//This function is used to load, for 2023A, the multiplicative extra-factor that multiplies all cells factors
map<int, double> LoadRunFactor2023A(string fname)
{
  map<int, double> data;
  ifstream f(fname.c_str());
  int run;
  double x;
  while (f >> run >> x) data[run] = x;
  return data;
}



