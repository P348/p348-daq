#!/bin/bash -e

# helper script to do separate runs of dqm.exe on each command-line input file

# example usage:
# make dqm.exe
# wget https://twiki.cern.ch/twiki/pub/P348/DataAccess/eos-2017.txt
# ./batch.sh submit "dqmbatchhelper.sh dqm.exe pedestal2017" eos-2017.txt 8nh byRun

for datname in "$@" ; do
  basename=$(basename "$datname")
  txtname=${basename/.dat/.txt}
  rootname=${basename/.dat/.root}
  echo "===>"
  echo "datname=$datname"
  echo "txtname=$txtname"
  echo "rootname=$rootname"
  rm -f dqm.txt dqm.root
  ./dqm.exe "$datname"
  mv dqm.txt $txtname
  mv dqm.root $rootname
done
