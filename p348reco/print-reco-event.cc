// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "TMarker.h"
#include "TLine.h"
#include "TText.h"
#include "TBox.h"
#include "TPaveText.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "pileup.h"
#ifdef TRACKINGTOOLS
#include "tracking_new.h"
#endif


#include "na64/reco/sadc.hh"
#include "na64/numerical/moyal.h"
static Double_t
moyal_fit_f(Double_t * x_, Double_t * par_) {
    return na64sw_common_moyal_f(*x_*par_[3], par_);
}


int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./print-reco-event.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  gStyle->SetLineScalePS(1);
  gStyle->SetGridColor(kGray);
  //gStyle->SetGridStyle(7);
  gStyle->SetGridWidth(1);
  
  TFile* file = new TFile("print-reco-event.root", "RECREATE");
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    const SimpleTrack track = simple_tracking_4MM(e);
    
    // print reco information
    cout << e << endl;
    cout << track << endl;

#ifdef TRACKINGTOOLS
    const std::vector<Track_t> upstream = GetReconstructedTracks(e, kRegion::upstream);
    cout << "(TRACKINGTOOLS) N of tracks: " << upstream.size() << endl;
    if (!upstream.empty()) {
      cout << "Best track information: " << endl;
      cout << upstream.at(0) << endl;
    }
#endif
    
    // prepare reco event plots
    const TString id = "event-" + e.id();
    file->mkdir(id);
    file->cd(id);
    
    const vector<const Cell*> list = listCells(e);
    
    // TODO: check for e.hasMasterTime()

    const double master = e.masterTime;
    const double WB_ref = e.WB_phase;
    // event type
    const TString etype =
      TString::Format("%s %s %s",
        e.isCalibration ? "CAL" : "",
        e.isPhysics ? "PHY" : "",
        e.isOnSpill ? "ON" : "OFF"
      );
    
    // trigger type
    const TString ttype =
      TString::Format("%s %s %s",
        e.isTriggerPhysics ? "PHY" : "",
        e.isTriggerRandom ? "RND" : "",
        e.isTriggerBeam ? "BEAM" : ""
      );
    
    for (size_t j = 0; j < list.size(); ++j) {
      const Cell& c = *list[j];
      
      if (!c.hasDigit) continue;

      const double refTime = (c.calib->adc.isWB ? WB_ref : master);

      const TString name = c.calib->name;
      const TString subdir = id + "/" + c.calib->detname();

         
      if (!file->GetDirectory(subdir)) file->mkdir(subdir);
      file->cd(subdir);
      
      const TString nameraw = name + "-raw";
      TCanvas c1(nameraw, nameraw);
      c1.SetGrid();
      
      const size_t nsamples = c.raw.size();
      float vx[nsamples];
      float vy[nsamples];
      
      // raw waveform
      for (size_t i = 0; i < nsamples; ++i) {
        vx[i] = i;
        vy[i] = c.raw[i];
      }
      TGraph g(nsamples, vx, vy);
      g.SetNameTitle(nameraw, id + "/" + name + "/RAW;time sample;ADC");
      g.SetMarkerStyle(kFullDotMedium);
      g.Draw("APL");
      
      TMarker marker;
      TLine line;
      TText text;
      TBox box;
      
      text.SetTextColor(kGray);
      
      // pedestal region
      const double ped_line = (c.calib->adc.pedestal_length-0.5);
      line.SetLineColor(kBlue-7);
      line.DrawLine(ped_line, 0, ped_line, 2*c.pedestal());
      text.SetTextAlign(33);
      text.DrawText(ped_line, 1.9*c.pedestal(), "pedestal");
      
      // head region
      const double head_line = (c.calib->adc.signal_range_begin - 0.5);
      line.SetLineColor(kBlue-7);
      line.DrawLine(head_line, 0, head_line, 2*c.pedestal());
      text.SetTextAlign(13);
      text.DrawText(head_line, 1.9*c.pedestal(), "head");
      
      // tail region
      const double tail_line = (c.calib->adc.signal_range_end - 0.5);
      line.SetLineColor(kBlue-7);
      line.DrawLine(tail_line, 0, tail_line, 2*c.pedestal());
      text.SetTextAlign(33);
      text.DrawText(tail_line, 1.9*c.pedestal(), "tail");
      
      // pedestal0/1 level
      line.SetLineColor(kGray);
      line.DrawLine(0, c.pedestal0, 31, c.pedestal0);
      line.SetLineColor(kGray+1);
      line.DrawLine(0, c.pedestal1, 31, c.pedestal1);
      
      // pedestal reco info
      TPaveText info1(1-0.01 - 0.22, 1-0.01 - 0.25, 1-0.01, 1-0.01, "NB NDC");
      info1.SetTextAlign(12);
      info1.AddText(TString::Format("event type = %s", etype.Data()));
      info1.AddText(TString::Format("trigger type = %s", ttype.Data()));
      info1.AddText(TString::Format("total samples = %d", (int)nsamples));
      info1.AddText(TString::Format("pedestal zone = [%d, %d)", 0, c.calib->adc.pedestal_length));
      info1.AddText(TString::Format("signal zone = [%d, %d)", (int)c.calib->adc.signal_range_begin, (int)c.calib->adc.signal_range_begin));
      info1.AddText(TString::Format("pedestal0 = %.1f", c.pedestal0));
      info1.AddText(TString::Format("pedestal1 = %.1f", c.pedestal1));



      info1.Draw();
      
      c1.Write();
      
      
      const TString namewave = name + "-wave";
      TCanvas c2(namewave, namewave);
      c2.SetGrid();
      
      // waveform without pedestal
      for (size_t i = 0; i < nsamples; ++i) {
        vx[i] = i;
        vy[i] = c.wave(i);
      }
      TGraph g2(nsamples, vx, vy);
      g2.SetNameTitle(namewave, id + "/" + name + "/Waveform;time sample;ADC");
      g2.SetMarkerStyle(kFullDotMedium);
      g2.Draw("APL");
      
      //Pile-up correction functions for ECAL0 and ECAL1
      int iPlane, iX, iY;
      if (name.Contains("ECAL1") || name.Contains("ECAL0")) {
        sscanf(name.Data(), "ECAL%i-%i-%i", &iPlane, &iX, &iY);
        if (pileup.ECAL[iPlane][iX][iY]) {
          for (unsigned int iPeak = 0; iPeak < c.peaks.size(); iPeak++) {
            auto f = pileup.ECAL[iPlane][iX][iY]->getTF1(c, iPeak);
            if (f) {
              f->SetLineColor(kRed);
              f->DrawClone("SAME");
            }
          }
        }
      }

      // pedestal region
      const double top2 = max(1., c.amplitude);
      line.SetLineColor(kBlue-7);
      line.DrawLine(ped_line, 0, ped_line, 0.4*top2);
      text.SetTextAlign(33);
      text.DrawText(ped_line, 0.35*top2, "pedestal");
      
      // head region
      line.SetLineColor(kBlue-7);
      line.DrawLine(head_line, 0, head_line, 0.4*top2);
      text.SetTextAlign(13);
      text.DrawText(head_line, 0.35*top2, "head");
      
      // tail region
      line.SetLineColor(kBlue-7);
      line.DrawLine(tail_line, 0, tail_line, 0.4*top2);
      text.SetTextAlign(33);
      text.DrawText(tail_line, 0.35*top2, "tail");
      
      text.SetTextColor(kBlack);
      
      // t0 marker
      line.SetLineColor(kRed);
      line.DrawLine(c.t0, 0, c.t0, 0.5*top2);
      text.SetTextAlign(33);
      text.DrawText(c.t0, 0.45*top2, "t0");
      
      // master time marker
      double master_i = refTime / c.calib->adc.sampling_interval;
      line.SetLineColor(kBlue);
      line.DrawLine(master_i, 0, master_i, 0.4*top2);
      text.SetTextAlign(33);
      text.DrawText(master_i, 0.25*top2, "tM");
      
      // expected signal timing
      const bool hasTcalib = c.calib->hasTcalib();
      const double texpected = c.texpected(refTime);
      
      if (hasTcalib) {
        const double t0expected_i = texpected / c.calib->adc.sampling_interval;
        const double tsigma_i = c.calib->tsigma / c.calib->adc.sampling_interval;
        
        // central value
        line.SetLineColor(kRed);
        line.DrawLine(t0expected_i, 0.50*top2, t0expected_i, 0.7*top2);
        
        // +- 3 sigma
        box.SetFillStyle(0);
        box.SetLineColor(kRed-7);
        box.DrawBox(t0expected_i - 3*tsigma_i, 0.65*top2, t0expected_i + 3*tsigma_i, 0.70*top2);
        
        // +- 2 sigma
        box.SetLineColor(kRed-4);
        box.DrawBox(t0expected_i - 2*tsigma_i, 0.60*top2, t0expected_i + 2*tsigma_i, 0.65*top2);
        
        // +- 1 sigma
        box.SetLineColor(kRed);
        box.DrawBox(t0expected_i - tsigma_i, 0.55*top2, t0expected_i + tsigma_i, 0.60*top2);
        
        text.SetTextAlign(21);
        text.DrawText(t0expected_i, 0.7*top2, "tE");
      }
      
      // peaks position
      marker.SetMarkerColor(kGray);
      marker.SetMarkerStyle(kOpenCircle);
      marker.SetMarkerSize(2);
      for (size_t i = 0; i < c.peaks.size(); ++i) {
        const int x = c.peaks[i];
        const double y = c.wave(x);
        marker.DrawMarker(x, y);
      }
      
      // max amplitude level and time
      line.SetLineColor(kGreen);
      line.DrawLine(0, c.amplitude, 31, c.amplitude);
      line.DrawLine(c.amplitude_sample, 0, c.amplitude_sample, c.amplitude);
      text.SetTextAlign(21);
      text.DrawText(c.amplitude_sample, c.amplitude, "A");
      
      // wave reco info
      TPaveText info2(1-0.01 - 0.22, 1-0.01 - 0.45, 1-0.01, 1-0.01, "NB NDC");
      info2.SetTextAlign(12);
      info2.AddText(TString::Format("event type = %s", etype.Data()));
      info2.AddText(TString::Format("trigger type = %s", ttype.Data()));
      info2.AddText(TString::Format("total samples = %d", (int)nsamples));
      info2.AddText(TString::Format("pedestal zone = [%d, %d)", 0, c.calib->adc.pedestal_length));
      info2.AddText(TString::Format("signal zone = [%d, %d)", (int)c.calib->adc.signal_range_begin, (int)c.calib->adc.signal_range_end));
      if (c.calib->adc.isWB){
        info2.AddText(TString::Format("WB ref time tM = %.1f ns", WB_ref));
      }
      else{
        info2.AddText(TString::Format("master time tM = %.1f ns", master));
      }
      info2.AddText(TString::Format("Calibration data:"));
      info2.AddText(TString::Format("  name = %s", c.calib->name.c_str()));
      info2.AddText(TString::Format("  K = %f", c.calib->K));
      info2.AddText(TString::Format("  tmean = %.1f%s ns", c.calib->tmean, (c.calib->isTmeanRelative ? " + tM" : "")));
      info2.AddText(TString::Format("  texpected tE = %.1f ns", texpected));
      info2.AddText(TString::Format("  tsigma = %.1f ns", c.calib->tsigma));
      info2.AddText(TString::Format("Wave reco data:"));
      info2.AddText(TString::Format("  reconstruction method: %s", c.calib->doWaveformFit ? "fit" : "standard"));
      info2.AddText(TString::Format("  time t0 = %.1f ns", c.t0ns()));
      info2.AddText(TString::Format("  amplitude A = %.1f", c.amplitude));
      info2.AddText(TString::Format("  amplitude sample = %d", c.amplitude_sample));
      // select units for energy display
      const bool isMeV = (c.energy < 100*MeV);
      const double evalue = c.energy / (isMeV ? MeV : 1.);
      const char* eunits = isMeV ? "MeV" : "GeV";
      info2.AddText(TString::Format("  energy = %.2f %s", evalue, eunits));
      info2.Draw();

     
      // if fitted peaks available, overimpose function
      size_t nPeak = 0;
      const na64util::iMSADCReco::PeakInfo * piBest = c.peak_BestTiming_fitted(master);
      for(const auto & peakItem : c.fittedPeaksInfo) {
          ++nPeak;
          if(!peakItem.fitFunctionUserdata) continue;
          TString bf;
          bf.Form("%s-moyal-%zu", c.calib->name.c_str(), nPeak);
          TF1 * f = new TF1(bf, moyal_fit_f
                  , peakItem.validityRange[0]/c.calib->adc.sampling_interval
                  , peakItem.validityRange[1]/c.calib->adc.sampling_interval
                  , 4 );
          auto peakColor = (&peakItem == piBest) ? kBlue :kRed;

          f->SetLineColor(peakColor);
          const double* fitdata = (const double*) peakItem.fitFunctionUserdata;
          f->SetParameters(fitdata[0]
                  , fitdata[1]
                  , fitdata[2]
                  , c.calib->adc.sampling_interval
                  );
          f->Draw("SAME");

          //Print the cluster ID, if negative is meaningless
          const int clusterID = peakItem.timeClusterLabel;
          if (clusterID>=0){
            const double labelX=f->GetMaximumX(peakItem.validityRange[0]/c.calib->adc.sampling_interval,peakItem.validityRange[1]/c.calib->adc.sampling_interval);
            text.SetTextColor(peakColor);
            text.DrawText(labelX, 1.05*peakItem.rawAmp,Form("%i",clusterID));
          }
      }
     
      
      c2.Write();
    }
  }
  
  // save all histograms to .root file
  file->Write();
  file->Close();
  
  return 0;
}
