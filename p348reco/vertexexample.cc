// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "THStack.h"
#include "TF1.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"
#include"vertexing.h"

bool CheckHits(const RecoEvent& e,const unsigned int Maxhits)
{
  //count them
  const unsigned int GEM1hits = gem::CountGEMHits(e.GM01);
  const unsigned int GEM2hits = gem::CountGEMHits(e.GM02);
  const unsigned int GEM3hits = gem::CountGEMHits(e.GM03);
  const unsigned int GEM4hits = gem::CountGEMHits(e.GM04);
  
  const bool Splash1 = GEM2hits < Maxhits;
  const bool Splash2 = GEM4hits < Maxhits;
  const bool Splash3 = GEM3hits < Maxhits;
  const bool Splash4 = GEM1hits < Maxhits;

  const bool Min1 = GEM2hits > 1;
  const bool Min2 = GEM4hits > 1;
  const bool Min3 = GEM3hits > 1;
  const bool Min4 = GEM1hits > 1;
  
  return Splash1 && Splash2 && Splash3 && Splash4 && Min1 && Min2 && Min3 && Min4;
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./vertexexample.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  // stats bar format
  gStyle->SetOptStat("emruo");
  gStyle->SetOptFit(111);
  gStyle->SetLineScalePS(1);


  const double multcut = 5;
  const double chicut = 20;
  const string geofile = conddbpath() + "NA64_vis150_2018.gdml";
  
  TFile* file = new TFile("vertexexample.root", "RECREATE");

  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH1I eplot("eplot", "ECAL;Energy, GeV;#nevents", 200, 0, 200);
  TH1I hplot("hplot", "HCAL;Energy, GeV;#nevents", 200, 0, 200);
  TH1I veto0plot("veto0plot", "VETO0;Energy, MeV;#nevents", 100, 0, 50);
  TH1I veto1plot("veto1plot", "VETO1;Energy, MeV;#nevents", 100, 0, 50);
  TH1I veto2plot("veto2plot", "VETO2;Energy, MeV;#nevents", 100, 0, 50);
  TH1I bgoplot("bgoplot", "BGO;Energy, MeV;#nevents", 150, 0, 150);
  TH1I srdplot("srdplot", "SRD;Energy, MeV;#nevents", 150, 0, 150);
  TH1I muonplot("muonplot", "MUON;Energy;#nevents", 100, 0, 100);
  TH1I atargplot("atargplot", "ATARG;Energy;#nevents", 100, 0, 100);
  TH1I wcal0plot("wcal0plot", "WCAL0;Energy, GeV;#nevents", 200, 0, 200);
  TH1I wcal1plot("wcal1plot", "WCAL1;Energy, GeV;#nevents", 200, 0, 200);
  TH1I wcal2plot("wcal2plot", "WCAL2;Energy, GeV;#nevents", 200, 0, 200);
  TH1I wcat("wcat", "WCAT;Energy, GeV;#nevents", 200, 0, 200);
  TH1I hod0X("hod0X", "hod0X;X, mm;#nhits", 35, 0, 35);
  TH1I hod0Y("hod0Y", "hod0Y;Y, mm;#nhits", 35, 0, 35);
  TH2I hod0("hod0", "hod0;X, mm;Y, mm;#nhits", 35, 0, 35, 35, 0, 35);
  TH1I hod1X("hod1X", "hod1X;X, mm;#nhits", 35, 0, 35);
  TH1I hod1Y("hod1Y", "hod1Y;Y, mm;#nhits", 35, 0, 35);
  TH1I hodnhits("hodnhits", "hodnhits;#nhits;#nevents", 10, 0, 10);
  TH1I hod2X("hod2X", "hod2X;X, mm;#nhits", 32, 0, 32);
  TH1I hod2Y("hod2Y", "hod2Y;Y, mm;#nhits", 32, 0, 32);
  TH2I hod2("hod2", "hod2;X, mm;Y, mm;#nhits", 32, 0, 32, 32, 0, 32);
  TH1I hod2nhits("hod2nhits", "hod2nhits;#nhits;#nevents", 30, 0, 30);

  TH1I masterplot("masterplot", "master time;t0, ns;#nevents", 400, 0, 400);
  TH1I e133t0plot("e133t0plot", "ECAL1[3][3].t0;t0, ns;#nevents", 400, 0, 400);
  TH1I e133mtt0plot("e133mtt0plot", "ECAL1[3][3].t0 - master;#Deltat0, ns;#nevents", 400, -200, 200);
  TH2I e133_vs_mtt0plot("e133_vs_mtt0plot", "ECAL1[3][3].t0 vs master;master, ns;ECAL1[3][3].t0, ns;#nevents", 400, 0, 400, 400, 0, 400);
  e133_vs_mtt0plot.SetOption("COL");
  
  TH1I e133_it_ratio("e133_it_ratio", "ECAL1[3][3] Sum(t0#pm3)/Sum;Ratio;#nevents", 100, 0, 1);
  
  TH1I mm1X("mm1X", "X position of beam on MM1;x(mm)", 1000, -100, 100);
  TH1I mm1Y("mm1Y", "Y position of beam on MM1;y(mm)", 1000, -100, 100);
  TH1I mm2X("mm2X", "X position of beam on MM2;x(mm)", 1000, -100, 100);
  TH1I mm2Y("mm2Y", "Y position of beam on MM2;y(mm)", 1000, -100, 100);
  TH1I mm3X("mm3X", "X position of beam on MM3;x(mm)", 1000, -100, 100);
  TH1I mm3Y("mm3Y", "Y position of beam on MM3;y(mm)", 1000, -100, 100);
  TH1I mm4X("mm4X", "X position of beam on MM4;x(mm)", 1000, -100, 100);
  TH1I mm4Y("mm4Y", "Y position of beam on MM4;y(mm)", 1000, -100, 100);
  TH2I mm1("mm1", "Beam Profile on MM1;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);
  TH2I mm2("mm2", "Beam Profile on MM2;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);
  TH2I mm3("mm3", "Beam Profile on MM3;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);
  TH2I mm4("mm4", "Beam Profile on MM4;x(mm);y(mm)", 1000, -100, 100, 1000, -100, 100);
  
  TH1I mm1X_time("mm1X_time", "MM1X best hit time;time, ns", 100, 0, 200);
  TH1I mm1Y_time("mm1Y_time", "MM1Y best hit time;time, ns", 100, 0, 200);
  TH1I mm2X_time("mm2X_time", "MM2X best hit time;time, ns", 100, 0, 200);
  TH1I mm2Y_time("mm2Y_time", "MM2Y best hit time;time, ns", 100, 0, 200);
  TH1I mm3X_time("mm3X_time", "MM3X best hit time;time, ns", 100, 0, 200);
  TH1I mm3Y_time("mm3Y_time", "MM3Y best hit time;time, ns", 100, 0, 200);
  TH1I mm4X_time("mm4X_time", "MM4X best hit time;time, ns", 100, 0, 200);
  TH1I mm4Y_time("mm4Y_time", "MM4Y best hit time;time, ns", 100, 0, 200);
  
  TH1I inangle("inangle", "Angle between (upstream track) and (axis Z);angle, mrad;events", 200, 0, 20);
  TH1I momentum("momentum", "Reconstructed momentum for electron;Energy(GeV);Entries", 400, 0, 200);

  TH2I ECAL_pos_mom("ECAL_pos_mom", "Beam Profile on ECAL for all momentums;x(mm);y(mm)", 1000, -500, -100, 1000, -150, 150);

  TH2F gem1_rel("gem1_rel","Beam Profile on GM1;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);
  TH2F gem2_rel("gem2_rel","Beam Profile on GM2;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);
  TH2F gem3_rel("gem3_rel","Beam Profile on GM3;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);
  TH2F gem4_rel("gem4_rel","Beam Profile on GM4;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);

  TH1D gem1X_time("gem1X_time","GM1X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem1Y_time("gem1Y_time","GM1Y best cluster time;time ,ns", 200, 0, 200);
  TH1D gem2X_time("gem2X_time","GM2X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem2Y_time("gem2Y_time","GM2Y best cluster time;time ,ns", 200, 0, 200);
  TH1D gem3X_time("gem3X_time","GM3X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem3Y_time("gem3Y_time","GM3Y best cluster time;time ,ns", 200, 0, 200);
  TH1D gem4X_time("gem4X_time","GM4X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem4Y_time("gem4Y_time","GM4Y best cluster time;time ,ns", 200, 0, 200);
  
  //histogram for chi
  TH1D Chi ("Chi","chi square distribution of shower profile",1000,0,100);
  TH1D Chi_srd ("Chi_srd","chi square distribution of shower profile after srd",1000,0,100);
  TH2I ehplot_chi("ehplot_chi", "ECAL vs HCAL after chi cut;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH2I ehplot_chi_srd("ehplot_chi_srd", "ECAL vs HCAL after chi cut;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);

  //for vertexing
  TH1D Chir_track ("Chir_track","chi square distribution of reconstructed tracks for real tracks",10000,0,1000);

  TH2D Vr_pos ("Vr_pos","Reconstructed vertex position in X:Z plane for real tracks; X [ mm ]; Z [ mm ]",200,-350,-150,5000,0,-5000);

  TH1D Vr_angle ("Vr_angle","Reconstructed vertex angle in X:Z plane for real tracks",1000, 0, 0.1);

  TH1D Vr_distance ("Vr_distance","Distance between line at vertex position for real tracks; mm",1000,0,10);
  
  TH2D V_particle ("V_particle","Particle of real vertex;particle ;particle 2",41,-20.5,20.5,40,-19.5,19.5);

  TH2D Nvtracks("nvtracks", "Reconstructed tracks vs reconstructed vertices;Number of tracks; Number of vertices",30,-0.5,29.5,30,-0.5,29.5);
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    RecoEvent e = RunP348Reco(manager);    
    
    //cout << "S1 print:\n" << e.S1 << endl;
    //cout << "ECAL[0][3][3] print:\n" << e.ECAL[0][3][3] << endl;
    
    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics) continue;
    
    // process event reco data
    double ecal = 0;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal += e.ECAL[d][x][y].energy;
    
    double hcal = 0;
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          hcal += e.HCAL[d][x][y].energy;
    
    double bgo = 0;
    for (int x = 0; x < 8; ++x)
      bgo += e.BGO[x].energy;
    
    bgo /= MeV; // convert to MeV
    
    double muon = 0;
    for (int d = 0; d < 4; ++d)
      muon += e.MUON[d].energy;
    
    double atarg = 0;
    for (int x = 0; x < 5; ++x)
      for (int y = 0; y < 5; ++y)
        atarg += e.ATARG[x][y].energy;
    
    const double srd = (e.SRD[0].energy + e.SRD[1].energy + e.SRD[2].energy) / MeV;
    
    const size_t hod_nhits = e.HOD0.xhits.size() + e.HOD0.yhits.size() + e.HOD1.xhits.size() + e.HOD1.yhits.size();
    // Combination of HOD2(in front of HOD0) & HOD1
    const size_t hod2_nhits = e.HOD2.xhits.size() + e.HOD2.yhits.size() + e.HOD1.xhits.size() + e.HOD1.yhits.size();
    
    // timing
    const Cell& e133 = e.ECAL[1][3][3];
    if (e133.hasDigit)
      e133t0plot.Fill(e133.t0ns());
    
    const bool hasMaster = e.hasMasterTime();
    const double master = e.masterTime;
    
    if (e133.hasDigit && hasMaster) {
      masterplot.Fill(master);
      e133mtt0plot.Fill(e133.t0ns() - master);
      e133_vs_mtt0plot.Fill(master, e133.t0ns());
    }
    
    // in-time ratio
    const double e133r = e133.inTimeWindowRatio(e133.t0, 3);
    e133_it_ratio.Fill(e133r);
    //cout << "ECAL133: t0 = " << e133.t0ns()
    //     << ", ratio(t0 +- 3 time samples) = "  << e133r
    //     << endl;
    
    if (e.MM1.hasHit()) {
      mm1X.Fill(e.MM1.xpos);
      mm1Y.Fill(e.MM1.ypos);
      mm1X_time.Fill(e.MM1.xplane.bestHitTime());
      mm1Y_time.Fill(e.MM1.yplane.bestHitTime());
      mm1.Fill(e.MM1.xpos, e.MM1.ypos);
    }
    
    if (e.MM2.hasHit()) {
      mm2X.Fill(e.MM2.xpos);
      mm2Y.Fill(e.MM2.ypos);
      mm2X_time.Fill(e.MM2.xplane.bestHitTime());
      mm2Y_time.Fill(e.MM2.yplane.bestHitTime());
      mm2.Fill(e.MM2.xpos, e.MM2.ypos);
    }
    
    if (e.MM3.hasHit()) {
      mm3X.Fill(e.MM3.xpos);
      mm3Y.Fill(e.MM3.ypos);
      mm3X_time.Fill(e.MM3.xplane.bestHitTime());
      mm3Y_time.Fill(e.MM3.yplane.bestHitTime());
      mm3.Fill(e.MM3.xpos, e.MM3.ypos);
    }
    
    if (e.MM4.hasHit()) {
      mm4X.Fill(e.MM4.xpos);
      mm4Y.Fill(e.MM4.ypos);
      mm4X_time.Fill(e.MM4.xplane.bestHitTime());
      mm4Y_time.Fill(e.MM4.yplane.bestHitTime());
      mm4.Fill(e.MM4.xpos, e.MM4.ypos);
    }
    
    // simple track reconstruction
    const SimpleTrack track = simple_tracking_4MM(e);
    if (track) {
      //cout << "mom = " << track.momentum << endl;
      inangle.Fill(track.in.theta()/1e-3);
      momentum.Fill(track.momentum);
      
      // extrapolate downstream track segment on ECAL surface
      const TVector3 P_ecal = extrapolate_line(track.out, ECAL_pos.pos.Z());
      //cout << "ecal track x = " << P_ecal.X() << " y = " << P_ecal.Y() << endl;
      ECAL_pos_mom.Fill(P_ecal.X(), P_ecal.Y());
    }
    
    const size_t mm_nclusters =
      e.MM1.xplane.clusters.size() + e.MM1.yplane.clusters.size() +
      e.MM2.xplane.clusters.size() + e.MM2.yplane.clusters.size() +
      e.MM3.xplane.clusters.size() + e.MM3.yplane.clusters.size() +
      e.MM4.xplane.clusters.size() + e.MM4.yplane.clusters.size();

    const size_t gem_nclusters =
      e.GM01->xplane.size() + e.GM01->yplane.size() +
      e.GM02->xplane.size() + e.GM02->yplane.size() +
      e.GM03->xplane.size() + e.GM03->yplane.size() +
      e.GM04->xplane.size() + e.GM04->yplane.size();   
    
    // fill histograms
    for (size_t i = 0; i < e.HOD0.xhits.size(); ++i) hod0X.Fill(e.HOD0.xhits[i]);
    for (size_t i = 0; i < e.HOD0.yhits.size(); ++i) hod0Y.Fill(e.HOD0.yhits[i]);
    
    for (size_t i = 0; i < e.HOD0.xhits.size(); ++i)
      for (size_t i = 0; i < e.HOD0.yhits.size(); ++i)
        hod0.Fill(e.HOD0.xhits[i], e.HOD0.yhits[i]);
    
    for (size_t i = 0; i < e.HOD1.xhits.size(); ++i) hod1X.Fill(e.HOD1.xhits[i]);
    for (size_t i = 0; i < e.HOD1.yhits.size(); ++i) hod1Y.Fill(e.HOD1.yhits[i]);
    
    hodnhits.Fill(hod_nhits);
   
    for (size_t i = 0; i < e.HOD2.xhits.size(); ++i) hod2X.Fill(e.HOD2.xhits[i]);
    for (size_t i = 0; i < e.HOD2.yhits.size(); ++i) hod2Y.Fill(e.HOD2.yhits[i]);
    
    for (size_t i = 0; i < e.HOD2.xhits.size(); ++i)
      for (size_t i = 0; i < e.HOD2.yhits.size(); ++i)
        hod2.Fill(e.HOD2.xhits[i], e.HOD2.yhits[i]);
    //Hits on HOD2 and HOD1
    hod2nhits.Fill(hod2_nhits); 
    
    
    ehplot.Fill(ecal, hcal);
    eplot.Fill(ecal);
    hplot.Fill(hcal);
    veto0plot.Fill(e.vetoEnergy(0)/MeV);
    veto1plot.Fill(e.vetoEnergy(1)/MeV);
    veto2plot.Fill(e.vetoEnergy(2)/MeV);
    bgoplot.Fill(bgo);
    srdplot.Fill(srd);
    muonplot.Fill(muon);
    atargplot.Fill(atarg);
    wcal0plot.Fill(e.WCAL[0].energy);
    wcal1plot.Fill(e.WCAL[1].energy);
    wcal2plot.Fill(e.WCAL[2].energy);
    wcat.Fill(e.WCAT.energy);
    
    // shower chi2 example
    const double chi = calcShowerChi2(e);
    const bool cutShower = chi < 10;
    
    const bool cutSrd = e.SRD[0].energy > 1*MeV && e.SRD[0].energy < 70*MeV &&
                        e.SRD[1].energy > 1*MeV && e.SRD[1].energy < 70*MeV &&
                        e.SRD[2].energy > 1*MeV && e.SRD[2].energy < 70*MeV;
    
    Chi.Fill(chi);
    if (cutSrd) Chi_srd.Fill(chi);
    
    if (cutShower) ehplot_chi.Fill(ecal,hcal);
    if (cutShower && cutSrd) ehplot_chi_srd.Fill(ecal,hcal);

    // filling gem's histos
    if ( e.GM01->bestHit ) {
        gem1_rel.Fill( e.GM01->bestHit.rel_pos().X(), e.GM01->bestHit.rel_pos().Y() );
        gem1X_time.Fill( e.GM01->bestHit.x.time() );
        gem1Y_time.Fill( e.GM01->bestHit.y.time() );
    }
    if ( e.GM02->bestHit ) {
        gem2_rel.Fill( e.GM02->bestHit.rel_pos().X(), e.GM02->bestHit.rel_pos().Y() );
        gem2X_time.Fill( e.GM02->bestHit.x.time() );
        gem2Y_time.Fill( e.GM02->bestHit.y.time() );
    }
    if ( e.GM03->bestHit ) {
        gem3_rel.Fill( e.GM03->bestHit.rel_pos().X(), e.GM03->bestHit.rel_pos().Y() );
        gem3X_time.Fill( e.GM03->bestHit.x.time() );
        gem3Y_time.Fill( e.GM03->bestHit.y.time() );
    }
    if ( e.GM04->bestHit ) {
        gem4_rel.Fill( e.GM04->bestHit.rel_pos().X(), e.GM04->bestHit.rel_pos().Y() );
        gem4X_time.Fill( e.GM04->bestHit.x.time() );
        gem4Y_time.Fill( e.GM04->bestHit.y.time() );
    }

    //VERTEX RECONSTRUCTION

    const bool GoodHits = CheckHits(e, multcut);
    

    vector<genfit::Track*> tracks;
    vector<NA64::Vertex> vertices;
    
    if(GoodHits)
     {
      cout << "Reconstruction in event: " << nevt << endl;
      vertices = DoVertexing(e,tracks,geofile,chicut);
     }
    
    //print true information
    if(vertices.size() == 0 && tracks.size() == 0)continue;

    for(unsigned int k(0); k < vertices.size(); ++k)
     {
      NA64::Vertex v = vertices[k];
      cout << v;
      //save information into histograms
      Vr_pos.Fill(v.pos.X(),v.pos.Z());
      Vr_distance.Fill(v.distance);
      Vr_angle.Fill(v.angle);
      Chir_track.Fill(v.GetChi1());Chir_track.Fill(v.GetChi2());
     }
    Nvtracks.Fill(tracks.size(),vertices.size());


    //garbage collection
    deleteAll(tracks);
    
    //if (nevt > 10000) break;
  }
  
  TCanvas c("vertexexample", "vertexexample");
  c.SetGrid();
  c.Print(".pdf[");
  
  // calorimeters
  ehplot.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  eplot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  hplot.Draw();
  hplot.Fit("gaus");
  TF1* fitf = hplot.GetFunction("gaus");
  if (fitf) fitf->SetNpx(300);
  c.Print(".pdf");
  
  c.Clear();
  THStack vetoplots("vetoplots", "VETO;Energy, MeV;#nevents");
  veto0plot.SetLineColor(kRed);
  veto0plot.SetMarkerColor(kRed);
  veto0plot.SetMarkerStyle(kFullSquare);
  veto1plot.SetLineColor(kGreen);
  veto1plot.SetMarkerColor(kGreen);
  veto1plot.SetMarkerStyle(kFullSquare);
  veto2plot.SetLineColor(kBlue);
  veto2plot.SetMarkerColor(kBlue);
  veto2plot.SetMarkerStyle(kFullSquare);
  vetoplots.Add(&veto0plot);
  vetoplots.Add(&veto1plot);
  vetoplots.Add(&veto2plot);
  vetoplots.Draw("nostack");
  c.BuildLegend(0.5, 0.65, 0.7, 0.85);
  c.Print(".pdf");
  
  c.Clear();
  bgoplot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  srdplot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  muonplot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  atargplot.Draw();
  c.Print(".pdf");
  
  // timing
  c.Clear();
  e133t0plot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  masterplot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  e133mtt0plot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  e133_vs_mtt0plot.Draw();
  c.Print(".pdf");
  
  // in-time ratio
  c.Clear();
  e133_it_ratio.Draw();
  c.Print(".pdf");
  
  // hodoscopes
  c.Clear();
  c.Divide(2, 2);
  c.cd(1); hod0X.Draw();
  c.cd(2); hod0Y.Draw();
  c.cd(3); hod1X.Draw();
  c.cd(4); hod1Y.Draw();
  c.Print(".pdf");
  c.Clear();
  c.Divide(1, 2);
  c.cd(1); hod2X.Draw();
  c.cd(2); hod2Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  c.Divide();
  c.SetLogy();
  hodnhits.Draw();
  c.Print(".pdf");
  c.SetLogy(kFALSE);
  
  c.Clear();
  hod0.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  c.Divide();
  c.SetLogy();
  hod2nhits.Draw();
  c.Print(".pdf");
  c.SetLogy(kFALSE);
  
  c.Clear();
  hod2.Draw("COL");
  c.Print(".pdf");

  // micromegas
  c.Clear();
  mm1X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm1Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm2X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm2Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm3X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm3Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm4X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm4Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm1.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  mm2.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  mm3.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  mm4.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  gem1_rel.Draw("COL");
  c.Print(".pdf");

  c.Clear();
  gem2_rel.Draw("COL");
  c.Print(".pdf");

  c.Clear();
  gem3_rel.Draw("COL");
  c.Print(".pdf");

  c.Clear();
  gem4_rel.Draw("COL");
  c.Print(".pdf");

  c.Clear();
  gem1X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem1Y_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem2X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem2Y_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem3X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem3Y_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem4X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem4Y_time.Draw();
  c.Print(".pdf");

  c.Clear();
  momentum.Draw();
  c.Print(".pdf");
  
  c.Clear();
  ECAL_pos_mom.Draw();
  c.Print(".pdf");
  
  c.Print(".pdf]");
  
  // save all histograms to .root file
  file->Write();
  file->Close();
  
  return 0;
}
