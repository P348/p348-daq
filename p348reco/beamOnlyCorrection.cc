#include "beamOnlyCorrection.h"

#include <fstream>

#include "conddb.h"

map<LED_spill_id_t, LED_event_t> BeamOnlycalib;

LED_event_t BeamOnlyref;    // Reference run data
LED_event_t BeamOnlycurr;   // Current run data
LED_spill_id_t BeamOnlycurrid; // Current run data ID (run, spill)
LED_spill_id_t BeamOnlyrefid; // Reference run data ID (run, spill)

void Set_BeamOnlyData_spill(int nrun, int nspill)
{
  //cout << "Set_BeamOnly_spill(): run " << nrun << " spill " << nspill << endl;
  const LED_spill_id_t eid = {nrun, nspill};
  
  // skip if data is set already
  if (!(eid != BeamOnlycurrid)) return;
  
  // load BeamOnly corrections data for a new spill
  
    // check corrections are available
    LED_spill_id_t loadid = eid;
    bool hasData = false;
    
    // for some of spills BeamOnly amplitudes may be missing,
    // try to load BeamOnly data from the a few of previous spills or from run average
    const int ntry = 3;
    for (int i = 0; i <= ntry; ++i) {
      hasData = (BeamOnlycalib.find(loadid) != BeamOnlycalib.end());
      if (hasData) break;
      
      cout << "WARNING: missing BeamOnly corrections data for spill " << loadid << endl;
      
      // navigate to previous spill
      loadid.spill -= 1;
      
      // fallback to run average
      if (i == ntry-1) loadid = eid.getRunId();
    }
    
    if (!hasData) {
      throw std::runtime_error("Missing BeamOnly corrections data");
    }
    
    if (eid != loadid) {
      cout << "WARNING: using BeamOnly data from the previous spill " << loadid << endl;
    }
    
    BeamOnlycurr = BeamOnlycalib[loadid];
    BeamOnlycurrid = eid;
    
    // notify
    const int iX=ECAL0BEAMCELL.ix;
    const int iY=ECAL0BEAMCELL.iy;

    const double ecurr = BeamOnlycurr.ecal[1][iX][iY];
    const double eref = BeamOnlyref.ecal[1][iX][iY];
    cout << "INFO: Set_BeamOnly_spill(): loaded corrections for"
         << " spill " << BeamOnlycurrid << " stat=" << BeamOnlycurr.stat
         << " | ECAL1-"<<iX<<"-"<<iX<<" value=" << ecurr
         << " | Ref: "<<eref
         << endl;
}

// resolve run number into the BeamOnly data file name
string BeamOnlyPath(const int run)
{
  char path[100];
  
  const bool is2023A = (8459 <= run && run <= 9717);
  if (is2023A) {
    sprintf(path, "BeamOnly-2023A-2023nov24/run_%d", run);
    return conddbpath() + path;
  }
  
  
  // no check for actual file existence,
  // it will be done in the following call Load_BeamOnly_calibrations_xxx()
  return "";
}

string BeamOnlyPath(const LED_spill_id_t id) { return BeamOnlyPath(id.run); }



void Load_BeamOnly_corrections_Format2023A(const string fname)
{
  /*
   * BeamOnly corrections for 2023A
   * ECAL0-2-2 and ECAL1-2-2
   *
   */
  cout << "INFO: Load_BeamOnly_corrections_Format2023A() fname=" << fname << endl;
  BeamOnlycalib.clear();
  
  ifstream f(fname);
  
  if (!f) {
    throw std::runtime_error("Error to open BeamOnly corrections data file");
  }
  
  int nread = 0;
  int nruns = 0;
  
  while (true) {
    // Format - <Run#> <Spill#> <Stat#> ECAL0-2-2 correction ECAL1-2-2 correction
    
    LED_spill_id_t id = {0, 0};
    LED_event_t beamonly = { { 0 }, { 0 }, { 0 }, { 0 }, 0 };
    
    f >> id.run;
    f >> id.spill;
    f >> beamonly.stat;
    f >> beamonly.ecal[0][2][2];
    f >> beamonly.ecal[1][2][2];

    // ignore the rest of line
    f.ignore(1000, '\n');
    
    // end of file or IO error
    if (!f) break;
    

    // integrity check for duplicate spill id
    const bool hasData = (BeamOnlycalib.find(id) != BeamOnlycalib.end());
    if (hasData) {
      throw std::runtime_error("ERROR: duplicate spill id");
    }
    
    // accumulate run average
    const LED_spill_id_t runid = id.getRunId();
    const bool hasRunData = (BeamOnlycalib.find(runid) != BeamOnlycalib.end());
    LED_event_t& runbeamonly = BeamOnlycalib[runid];
    if (!hasRunData) {
      runbeamonly.stat = 0;
      nruns++;
    }
    runbeamonly.merge(beamonly);
    
    BeamOnlycalib[id] = beamonly;
    nread++;
  }
  
  // check file state
  if (!f.eof()) {
    // file read stop due to format or IO error
    //std::cout << "eof()=" << f.eof() << " fail()=" << f.fail() << endl;
    throw std::runtime_error("Error to read BeamOnly corrections data file - file format or IO error");
  }
  
  const int nload = BeamOnlycalib.size();
  const int nspills = nload - nruns;
  const LED_spill_id_t& id1run = BeamOnlycalib.cbegin()->first;
  const LED_spill_id_t& id1 = BeamOnlycalib.lower_bound(id1run.getNextId())->first;
  const LED_spill_id_t& idN = (--BeamOnlycalib.cend())->first;
  
  cout << "INFO: Load_BeamOnly_corrections_Format2023A()"
       << " total records = " << nload
       << " total runs = " << nruns
       << " total spills = " << nspills
       << " range = " << id1 << " " << idN
       << endl;
  
  if (nread != nspills) {
    cout << "WARNING: Load_BeamOnly_corrections_Format2023A() duplicate records detected,"
         << " nread=" << nread
         << " nspills=" << nspills
         << " ndup=" << (nread - nspills)
         << endl;
  }
}


void Init_BeamOnly_corrections_2023A(const int run)
{
  //Init BeamOnlyRef
  const bool isPeriod123 = (run>=8663)&&(run<=9161);
  const bool isPeriod45 = (run>=9252)&&(run<=9556);
  const bool isPeriodPosi = (run>=9557)&&(run<=9588);

  BeamOnlyrefid.spill=0;
  //Set reference
  if (isPeriod123){ //reference run: 8605
    BeamOnlyrefid.run=8605;
  }
  else if (isPeriod45){ //reference run: 9211
    BeamOnlyrefid.run=9211;
  }
  else if (isPeriodPosi){//reference run: 9557
    BeamOnlyrefid.run=9557;
  }

  //Init the reference run
  Load_BeamOnly_corrections_Format2023A(BeamOnlyPath(BeamOnlyrefid.run));
  BeamOnlyref = BeamOnlycalib[BeamOnlyrefid];

  //Init this run
  Load_BeamOnly_corrections_Format2023A(BeamOnlyPath(run));
  BeamOnlycurr = LED_event_t();
  BeamOnlycurrid = {-1, -1};
}
