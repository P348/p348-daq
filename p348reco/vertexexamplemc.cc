// ROOT
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>

// c++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

// P348 reco
#include "p348reco.h"
#include"shower.h"
#include "simu.h"
#include"vertexing.h"

bool CheckNumber(const vector<MChit>& hits,const unsigned int Maxhits)
{
 unsigned int counter (0);
 for(auto p = hits.begin();p != hits.end(); ++p)
  counter += (int)( (*p).id != -1);

 //cout << "counter: " << counter << endl;
 return counter <= Maxhits && counter >= 2;
}

bool CheckHits(const RecoEvent& e,const unsigned int Maxhits)
{
 const bool Splash1 = CheckNumber(e.mc->GEM1truth,Maxhits);
 const bool Splash2 = CheckNumber(e.mc->GEM1truth,Maxhits);
 const bool Splash3 = CheckNumber(e.mc->GEM1truth,Maxhits);
 const bool Splash4 = CheckNumber(e.mc->GEM1truth,Maxhits);

 return Splash1 && Splash2 && Splash3 && Splash4;
}

void FillAngles(TH2D& hist,const std::shared_ptr<NA64::VertexTruth> vt)
{
 TVector3 d11 (vt->track1truth[0].pos);
 TVector3 d12 (vt->track1truth[1].pos);
 TVector3 d13 (vt->track1truth[2].pos);
 TVector3 d14 (vt->track1truth[3].pos); 
 const double dangletrack1 = (d12-d11).Angle(d14-d13); 
 TVector3 d21 (vt->track2truth[0].pos);
 TVector3 d22 (vt->track2truth[1].pos);
 TVector3 d23 (vt->track2truth[2].pos);
 TVector3 d24 (vt->track2truth[3].pos); 
 const double dangletrack2 = (d22-d21).Angle(d24-d23);
 hist.Fill(dangletrack1,dangletrack2);
}

int main(int argc, char *argv[])
{
 if (argc < 2 || argc > 4) {
    std::cerr << "Usage: ./vertexexamplemc.exe <MC file name> <OPTIONAL: Number of events" << std::endl;
    return 1;
  }
 bool LimitedEvent(false);int Limit(0);
 if(argc == 3)
  {
   LimitedEvent = true;
   Limit = atoi(argv[2]);
  }
  
  std::ifstream inFile(argv[1]);
  
  if (! inFile) {
    std::cerr << "ERROR: can't open file " << argv[1] << std::endl;
    return 1;
  }


  const double multcut = 5;
  const double chicut = 100;
  const string geofile = "NA64_vis150_2018_sim.gdml";
  
  TFile* hOutputFile = new TFile("vertexexamplemc.root", "RECREATE");

  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH1I bgoplot("bgoplot", "BGO;Energy, MeV;#nevents", 150, 0, 150);

  TH1I mcecalentries("MC_ecalentries", "MC ECAL entry X coordinates in mm;X, mm;#nevents", 100, -200., 200.);
  
  TH1D Chi ("Chi","chi square distribution of shower profile",1000,0,100);

  TH1D Chir_track ("Chir_track","chi square distribution of reconstructed tracks for real tracks",10000,0,0.1);
  TH1D Chif_track ("Chif_track","chi square distribution of reconstructed tracks for fake",10000,0,0.1);
  TH2D Vd_pos ("Vd_pos","Reconstructed vertex position in X:Z plane for real tracks for dark matter; X [ mm ]; Z [ mm ]",200,-350,-150,2000,0,2000);  
  TH2D Vr_pos ("Vr_pos","Reconstructed vertex position in X:Z plane for real tracks; X [ mm ]; Z [ mm ]",200,-350,-150,2000,0,2000);
  TH2D Vf_pos ("Vf_pos","Reconstructed vertex position in X:Z plane for fake tracks; X [ mm ]; Z [ mm ]",200,-350,-150,2000,0,2000);
  TH2D Vd_res ("Vd_res","Resolution of vertex position in X:Z plane for real tracks for DM; X [ mm ]; Z [ mm ]",200,-10,10,2000,-1000,1000);
  TH1D Vd_angleres ("Vd_angleres","Resolution of angle of vertex in X:Z plane for real tracks for DM; X [ mm ]; Z [ mm ]",200,-10,10);
  TH2D dpangler ("dpangler","Angle between fake tracks point 12 vs 34 for real; track1 [ rad ]; track2 [ rad ]",1000,0,1,1000,0,1);
  TH2D dpangled ("dpangled","Angle between fake tracks point 12 vs 34 for dark matter; track1 [ rad ]; track2 [ rad ]",1000,0,1,1000,0,1);  
  TH2D dpanglef ("dpanglef","Angle between fake tracks point 12 vs 34 for fake; track1 [ rad ]; track2 [ rad ]",1000,0,1,1000,0,1);
  TH1D Vr_angle ("Vr_angle","Reconstructed vertex angle in X:Z plane for real tracks; [ mrad ]",1000, 0, 0.1);
  TH1D Vf_angle ("Vf_angle","Reconstructed vertex angle in X:Z plane for fake tracks; [ mrad ]",1000, 0, 0.1);
  TH1D V_real ("V_real","Number of real vertices",3,-0.5,2.5);
  TH1D Vr_distance ("Vr_distance","Distance between line at vertex position for real tracks; mm",1000,0,10);
  TH1D Vf_distance ("Vf_distance","Distance between line at vertex position for fake tracks; mm",1000,0,10);
  TH2D V_particle ("V_particle","Particle of real vertex;particle ;particle 2",41,-20.5,20.5,40,-19.5,19.5);
  TH2D Nvtracks("nvtracks", "Reconstructed tracks vs reconstructed vertices;Number of tracks; Number of vertices",30,-0.5,29.5,30,-0.5,29.5);
  
  // event loop, read event by event
  Init_Geometry_MC2018();
  for (int iev = 0; true; iev++) {


    if(LimitedEvent && Limit < iev)break;
    RecoEvent e = RunP348ReadMC(inFile);   
    if (!e.mc) break;
    DoDigitization(e);

    // print progress
    if (iev % 100 == 1)
      std::cout << "===> Event #" << iev << std::endl;

    double ecal = 0;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal += e.ECAL[d][x][y].energy;
    
    double hcal = 0;
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          hcal += e.HCAL[d][x][y].energy;
    
    double bgo = 0;
    for (int x = 0; x < 8; ++x)
      bgo += e.BGO[x].energy;
    
    bgo /= MeV; // convert to MeV

    double srd = 0;
    for (int x = 0; x < 3; ++x)
      srd += e.SRD[x].energy;

    srd /= MeV; // convert to MeV

    //chi example, put 1 at the end to use MCtruth for the initial position of the shower
    double chi = calcShowerChi2(e,2,2,3,SP_MC);


    double veto = 0.;
    for (int iv = 0; iv < 3; ++iv)
      veto += e.vetoEnergy(iv);
    
    //VERTEX RECONSTRUCTION
    const bool GoodHits = CheckHits(e,5);
    //    continue;
    vector<genfit::Track*> tracks;
    vector<NA64::Vertex> vertices;
    
    if(GoodHits)
     {      
       vertices = DoVertexing(e,tracks,geofile,chicut);
     }
    
    //print true information
    if(vertices.size() == 0 && tracks.size() == 0)continue;

    for(unsigned int k(0); k < vertices.size(); ++k)
     {
      NA64::Vertex v = vertices[k];
      //save information into histograms
      if(v.vtruth->IsReal)
       {
        if(v.vtruth->IsDM)
         {
          cout << v;
          Vd_res.Fill(v.Vres().X(),v.Vres().Z());
          Vd_angleres.Fill(v.Angleres());
          Vd_pos.Fill(v.pos.X(),v.pos.Z());
          FillAngles(dpangled,v.vtruth);
          V_real.Fill(2);
          Vr_distance.Fill(v.distance);
          Vr_angle.Fill(v.angle*1000);
          Chir_track.Fill(v.GetChi1());Chir_track.Fill(v.GetChi2());
          //v.ShowVertex();
         }
        else
         {
          V_particle.Fill((v.vtruth->track1truth)[0].particle,(v.vtruth->track2truth)[0].particle);
          V_real.Fill(1);
          FillAngles(dpangler,v.vtruth);
         }
       }
      else
       {
        Vf_pos.Fill(v.pos.X(),v.pos.Z());
        Vf_angle.Fill(v.angle);
        Vf_distance.Fill(v.distance);
        Chif_track.Fill(v.GetChi1());Chif_track.Fill(v.GetChi2());
        V_real.Fill(0);
        FillAngles(dpanglef,v.vtruth);
       }
     }    
    Nvtracks.Fill(tracks.size(),vertices.size());
    //to correct for rudimental read of mctruth
    e.mc->DMTRID1 = 0;
    e.mc->DMTRID2 = 0;


    //garbage collection
    deleteAll(tracks);

    ehplot.Fill(ecal, hcal);
    bgoplot.Fill(bgo);
    if(e.mc) mcecalentries.Fill(e.mc->ECALEntryX);
    Chi.Fill(chi);

  }
  
  hOutputFile->Write();

  return 0;
}
