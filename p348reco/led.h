#pragma once

#include <iostream>
#include <map>

using namespace std;

// LED spill ID
struct LED_spill_id_t {
  int run;
  int spill;
  
  bool operator < (const LED_spill_id_t& r) const;
  
  bool operator != (const LED_spill_id_t& r) const;
  
  LED_spill_id_t getRunId() const { return {run, 0}; }
  bool isRunId() const { return (spill == 0); }
  
  LED_spill_id_t getNextId() const { return {run, spill+1}; }
};

ostream& operator<<(ostream& os, const LED_spill_id_t& id);

// LED amplitudes
struct LED_event_t {
  float ecal[2][6][6];  // [d][x][y]
  float hcal[4][3][3];  // [d][x][y]
  float vhcal[2][4][4];  // [d][x][y]
  float wcal[3];        // [d]
  int stat;
  
  //LED_event_t(): stat(0) {}
  
  // merge taking into account stats
  void merge(const LED_event_t& a);
};

ostream& operator<<(ostream& os, const LED_event_t& led);

extern map<LED_spill_id_t, LED_event_t> LEDcalib;

extern LED_event_t ledref;    // Reference run data
extern LED_event_t ledcurr;   // Current run data
extern LED_spill_id_t ledcurrid; // Current run data ID (run, spill)

void Set_LED_spill(int nrun, int nspill);

// resolve run number into the led data file name
// TODO: move the table run range -> filename into SDC .txt
string ledpath(const int run);

string ledpath(const LED_spill_id_t id);

void Load_LED_calibrations(const string fname);

void Init_LED_calibrations_2018();

void Load_LED_calibrations_Format2022B(const string fname);

void Init_LED_calibrations_2022B(const int run);

void Load_LED_calibrations_Format2023A(const string fname);

void Load_LED_calibrations_Format2023h(const string fname);

void Init_LED_calibrations_2023h(const int run);

void Init_LED_calibrations_2023A(const int run);

void Init_LED_calibrations_2024A(const int run);
