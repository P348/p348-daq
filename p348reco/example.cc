// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "THStack.h"
#include "TF1.h"
#include "TMarker.h"
#include "TBox.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"
#include "badburst.h"
#include "timecut.h"
#ifdef TRACKINGTOOLS
#include "tracking_new.h"
#endif

bool inRange(const double x, const double min, const double max)
{
  return (min <= x) && (x <= max);
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./example.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  // global style
  gStyle->SetOptStat("emruo");
  gStyle->SetOptFit(111);
  gStyle->SetLineScalePS(1);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);
  gStyle->SetGridColor(kGray);
  
  TFile* file = new TFile("example.root", "RECREATE");
  
  TH1I etypeplot("etypeplot", "Event type;type;#nevents", 1, 0, 1);
  etypeplot.SetOption("HIST TEXT0");
  
  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  ehplot.SetOption("COL");
  TH1I eplot("eplot", "ECAL;Energy, GeV;#nevents", 200, 0, 200);
  TH1I eplot_bo("eplot_bo", "ECAL {isTriggerBeamOnly()};Energy, GeV;#nevents", 200, 0, 200);
  TH2I eplot_spill("eplot_spill", "ECAL vs spill;spill number;Energy, GeV;#nevents", 200, 1, 201, 200, 0, 200);
  eplot_spill.SetOption("COL");
  TH1I hplot("hplot", "HCAL;Energy, GeV;#nevents", 200, 0, 200);
  TH1I h0plot("h0plot", "HCAL0;Energy, GeV;#nevents", 200, 0, 200);
  TH1I h1plot("h1plot", "HCAL1;Energy, GeV;#nevents", 200, 0, 200);
  TH1I vhplot("vhplot", "VHCAL;Energy, GeV;#nevents", 200, 0, 200);
  TH1I h0dimuplot("h0dimuplot", "HCAL0 {dimuon};Energy, GeV;#nevents", 50, 0, 10);
  TH1I h1dimuplot("h1dimuplot", "HCAL1 {dimuon};Energy, GeV;#nevents", 50, 0, 10);
  TH1I veto0plot("veto0plot", "VETO0;Energy, MeV;#nevents", 100, 0, 50);
  TH1I veto1plot("veto1plot", "VETO1;Energy, MeV;#nevents", 100, 0, 50);
  TH1I veto2plot("veto2plot", "VETO2;Energy, MeV;#nevents", 100, 0, 50);
  TH1I bgoplot("bgoplot", "BGO;Energy, MeV;#nevents", 150, 0, 150);
  TH1I srdplot("srdplot", "SRD;Energy, MeV;#nevents", 150, 0, 150);
  TH1I lysoplot("lysoplot", "LYSO;Amplitude, ADC counts;#nevents", 200, 0, 400);
  TH1I muonplot("muonplot", "MUON;Energy;#nevents", 100, 0, 100);
  TH1I atargplot("atargplot", "ATARG;Energy;#nevents", 100, 0, 100);
  TH1I wcal0plot("wcal0plot", "WCAL0;Energy, GeV;#nevents", 200, 0, 200);
  TH1I wcal1plot("wcal1plot", "WCAL1;Energy, GeV;#nevents", 200, 0, 200);
  TH1I wcal2plot("wcal2plot", "WCAL2;Energy, MeV;#nevents", 1000, 0, 200);
  TH1I wcal0pileup("wcal0pileup", "WCAL0 pileup;Energy, GeV;#nevents", 200, 0, 100);
  TH1I wcal1pileup("wcal1pileup", "WCAL1 pileup;Energy, GeV;#nevents", 200, 0, 100);
  TH1I wcal2pileup("wcal2pileup", "WCAL2 pileup;Energy, MeV;#nevents", 500, 0, 100);
  TH1I wcat("wcat", "WCAT;Energy, GeV;#nevents", 1000, 0, 10);
  TH1I ltg0("ltg0", "LTG0;Energy, ADC;nevents", 200, 0, 1000);
  TH1I chcnt0("chcnt0", "CHCNT0;Energy, ADC;nevents", 200, 0, 1000);
  
  TH1I hod0X("hod0X", "hod0X;X, mm;#nhits", 35, 0, 35);
  TH1I hod0Y("hod0Y", "hod0Y;Y, mm;#nhits", 35, 0, 35);
  TH2I hod0("hod0", "HOD0;X, mm;Y, mm;#nhits", 35, 0, 35, 35, 0, 35);
  TH1I hod1X("hod1X", "hod1X;X, mm;#nhits", 35, 0, 35);
  TH1I hod1Y("hod1Y", "hod1Y;Y, mm;#nhits", 35, 0, 35);
  TH1I hod2X("hod2X", "hod2X;X, mm;#nhits", 32, 0, 32);
  TH1I hod2Y("hod2Y", "hod2Y;Y, mm;#nhits", 32, 0, 32);
  TH2I hod2("hod2", "HOD2;X, mm;Y, mm;#nhits", 32, 0, 32, 32, 0, 32);

  TH1I masterplot("masterplot", "master time;t0, ns;#nevents", 400, 0, 400);
  TH1I e133t0plot("e133t0plot", "ECAL1[3][3].t0;t0, ns;#nevents", 400, 0, 400);
  TH1I e133mtt0plot("e133mtt0plot", "ECAL1[3][3].t0 - master;#Deltat0, ns;#nevents", 400, -200, 200);
  TH2I e133_vs_mtt0plot("e133_vs_mtt0plot", "ECAL1[3][3].t0 vs master;master, ns;ECAL1[3][3].t0, ns;#nevents", 400, 0, 400, 400, 0, 400);
  e133_vs_mtt0plot.SetOption("COL");
  
  TH1I e133_pileup_energy("e133_pileup_energy", "ECAL1[3][3] pileup energy;energy, GeV;#nevents", 100, 0., 100.);
  TH2I e133_pileup("e133_pileup", "ECAL1[3][3] pileup;pileup peak relative position, samples;ration (pileup peak amplitude)/(best peak amplitude);#nevents", 10, -10, 0, 100, 0., 1.);
  e133_pileup.SetOption("COLZ");
  e133_pileup.GetXaxis()->SetNdivisions(10);
  e133_pileup.GetXaxis()->CenterLabels();
  
  TH1I e133_it_ratio("e133_it_ratio", "ECAL1[3][3] Sum(t0#pm3)/Sum;Ratio;#nevents", 100, 0, 1);
  
  TH1I mm1X("mm1X", "X position of beam on MM1;x(mm)", 500, -100, 100);
  TH1I mm1Y("mm1Y", "Y position of beam on MM1;y(mm)", 500, -100, 100);
  TH1I mm2X("mm2X", "X position of beam on MM2;x(mm)", 500, -100, 100);
  TH1I mm2Y("mm2Y", "Y position of beam on MM2;y(mm)", 500, -100, 100);
  TH1I mm3X("mm3X", "X position of beam on MM3;x(mm)", 500, -100, 100);
  TH1I mm3Y("mm3Y", "Y position of beam on MM3;y(mm)", 500, -100, 100);
  TH1I mm4X("mm4X", "X position of beam on MM4;x(mm)", 500, -100, 100);
  TH1I mm4Y("mm4Y", "Y position of beam on MM4;y(mm)", 500, -100, 100);
  TH2I mm1("mm1", "Beam Profile on MM1;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm2("mm2", "Beam Profile on MM2;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm3("mm3", "Beam Profile on MM3;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm4("mm4", "Beam Profile on MM4;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm5("mm5", "Beam Profile on MM5;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm6("mm6", "Beam Profile on MM6;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm7("mm7", "Beam Profile on MM7;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm8("mm8", "Beam Profile on MM8;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm9("mm9", "Beam Profile on MM9;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm10("mm10", "Beam Profile on MM10;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm11("mm11", "Beam Profile on MM11;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  TH2I mm12("mm12", "Beam Profile on MM12;x(mm);y(mm)", 200, -100, 100, 200, -100, 100);
  
  TH1I mm1X_time("mm1X_time", "MM1X best hit time;time, ns", 2000, -400, 1600);
  TH1I mm1Y_time("mm1Y_time", "MM1Y best hit time;time, ns", 2000, -400, 1600);
  TH1I mm2X_time("mm2X_time", "MM2X best hit time;time, ns", 2000, -400, 1600);
  TH1I mm2Y_time("mm2Y_time", "MM2Y best hit time;time, ns", 2000, -400, 1600);
  TH1I mm3X_time("mm3X_time", "MM3X best hit time;time, ns", 2000, -400, 1600);
  TH1I mm3Y_time("mm3Y_time", "MM3Y best hit time;time, ns", 2000, -400, 1600);
  TH1I mm4X_time("mm4X_time", "MM4X best hit time;time, ns", 2000, -400, 1600);
  TH1I mm4Y_time("mm4Y_time", "MM4Y best hit time;time, ns", 2000, -400, 1600);
  
  TH1I in_angle("in_angle", "Angle between (income track segment) and (axis Z);angle, mrad;events", 200, 0, 20);
  TH1I out_angle("out_angle", "Angle between (outcome track segment) and (axis Z);angle, mrad;events", 400, 0, 40);
  TH1I in_out_distance("in_out_distance", "Distance between income and outcome track segments;distance,mm;entries", 400, 0, 200);
  TH1I momentum("momentum", "Simple tracking: momentum;Energy(GeV);Entries", 400, 0, 200);

  TH1D momentum_up("momentum_up", "Tracking Tools (all fitted tracks): Upstream momentum;momentum (GeV); Entries", 500, 0, 250);
  TH1D momentum_up_best("momentum_up_best", "Tracking Tools (best track only): Upstream momentum;momentum (GeV); Entries", 500, 0, 250);
  TH1D momentum_down("momentum_down", "Tracking Tools: Downstream momentum;momentum (GeV); Entries", 500, 0, 250);
  TH1D mm7x_ures("mm7x_ures", "Tracking Tools: residual MM7x; x (mm)", 200,-2,2);
  TH1D mm7x_reco("mm7x_reco", "Tracking Tools: reco hit MM7x; x (mm)", 200,-2,2);

  TH2I ECAL_hit_pos("ECAL_hit_pos", "Beam Profile on ECAL w.r.t ECAL_pos.pos;x-x_{0}(mm);y-y_{0}(mm)", 1000, -500, 500, 300, -150, 150);
  ECAL_hit_pos.SetOption("COL");
  TH2I ECAL_hit_pos_GenFit("ECAL_hit_pos_GenFit", "Beam Profile on ECAL (GenFit) w.r.t ECAL_pos.pos;x-x_{0}(mm);y-y_{0}(mm)", 1000, -500, 500, 300, -150, 150);
  ECAL_hit_pos_GenFit.SetOption("COL");
  TH1I ECAL_hit_zpos_GenFit("ECAL_hit_zpos_GenFit", "Z position of extrapolation to ECAL (GenFit) w.r.t ECAL_pos.pos.Z();z-z_{0}(mm)", 300, -150, 150);

  TH2F gem1_rel("gem1_rel","Beam Profile on GM1;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);
  TH2F gem2_rel("gem2_rel","Beam Profile on GM2;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);
  TH2F gem3_rel("gem3_rel","Beam Profile on GM3;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);
  TH2F gem4_rel("gem4_rel","Beam Profile on GM4;x(mm);y(mm)", 100, -50, 50, 100, -50, 50);

  TH1D gem1X_time("gem1X_time","GM1X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem1Y_time("gem1Y_time","GM1Y best cluster time;time ,ns", 200, 0, 200);
  TH1D gem2X_time("gem2X_time","GM2X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem2Y_time("gem2Y_time","GM2Y best cluster time;time ,ns", 200, 0, 200);
  TH1D gem3X_time("gem3X_time","GM3X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem3Y_time("gem3Y_time","GM3Y best cluster time;time ,ns", 200, 0, 200);
  TH1D gem4X_time("gem4X_time","GM4X best cluster time;time ,ns", 200, 0, 200);
  TH1D gem4Y_time("gem4Y_time","GM4Y best cluster time;time ,ns", 200, 0, 200);
  
  TH2F ST01("ST01","ST01 hits;x,mm;y,mm",64,-100,100, 64,-100,100);
  TH2F ST02("ST02","ST02 hits;x,mm;y,mm",64,-100,100, 64,-100,100);
  TH2F ST03("ST03","ST03 hits;x,mm;y,mm",64,-100,100, 64,-100,100);
  TH2F ST04("ST04","ST04 hits;x,mm;y,mm",64,-100,100, 64,-100,100);
  TH2F ST05("ST05","ST05 hits;x,mm;y,mm",64,-100,100, 64,-100,100);
  TH2F ST06("ST06","ST06 hits;x,mm;y,mm",64,-100,100, 64,-100,100);
  ST01.SetOption("COLZ");
  ST02.SetOption("COLZ");
  ST03.SetOption("COLZ");
  ST04.SetOption("COLZ");
  ST05.SetOption("COLZ");
  ST06.SetOption("COLZ");
  
  TH1I ST04X("ST04X","X position of hits in ST04;wire",64,0,64);
  TH1I ST05X("ST05X","X position of hits in ST05;wire",64,0,64);
  TH1I ST06X("ST06X","X position of hits in ST06;wire",64,0,64);
  TH1I ST04Y("ST04Y","Y position of hits in ST04;wire",64,0,64);
  TH1I ST05Y("ST05Y","Y position of hits in ST05;wire",64,0,64);
  TH1I ST06Y("ST06Y","Y position of hits in ST06;wire",64,0,64);
  TH1I ST04X_t("ST04X_t","time of x hits in ST04;time bin",140,0,1400);
  TH1I ST05X_t("ST05X_t","time of x hits in ST05;time bin",140,0,1400);
  TH1I ST06X_t("ST06X_t","time of x hits in ST06;time bin",140,0,1400);
  TH1I ST04Y_t("ST04Y_t","time of y hits in ST04;time bin",140,0,1400);
  TH1I ST05Y_t("ST05Y_t","time of y hits in ST05;time bin",140,0,1400);
  TH1I ST06Y_t("ST06Y_t","time of y hits in ST06;time bin",140,0,1400);
  
  //histogram for chi
  TH1D Chi ("Chi","shower profile chi^{2}",1000,0,100);
  TH1D Chi_srd ("Chi_srd","shower profile chi^{2} after srd",1000,0,100);
  TProfile2D profile_ECAL("profile_ECAL", "ECAL profile: genfit track hit {missing energy > 10 GeV};ECAL x, mm;ECAL y, mm;Energy, GeV", 300, -410, -320, 300, -55, 35, 0, 120);
  profile_ECAL.SetOption("COLZ");
  TH2I ehplot_chi("ehplot_chi", "ECAL vs HCAL after chi cut;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH2I ehplot_chi_srd("ehplot_chi_srd", "ECAL vs HCAL after chi cut;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e0 = RunP348Reco(manager);
    
    /*
    // print event details:
    // - EventID: run-spill-event
    cout << "EventID=" << e0.id() << endl;
    // - full event
    cout << "Event details:\n" << e0 << endl;
    // - particular SADC cell
    cout << "S1 detail:\n" << e0.S1 << endl;
    cout << "ECAL[1][3][2] details:\n" << e0.ECAL[1][3][2] << endl;
    */
   
    RecoEvent e = e0;
    timecut(e, 4.);

    const bool isGoodEnergy = (e0.ecalTotalEnergy() - e.ecalTotalEnergy()) < 30.;
    
    if (e.isCalibration) etypeplot.Fill("LED", 1.);
    if (e.isPhysics) etypeplot.Fill("PHYS", 1.);
    if (e.isTriggerPhysics) etypeplot.Fill("TRG.PHYS", 1.);
    if (e.isTriggerBeam)    etypeplot.Fill("TRG.BEAM", 1.);
    if (e.isTriggerBeamOnly()) etypeplot.Fill("TRG.BEAMO", 1.);
    if (e.isTriggerRandom)  etypeplot.Fill("TRG.RAND", 1.);
    
    // process events with the "physics" trigger flag, useful for physics analysis
    if (!e.isPhysics) continue;
    
    // process events which have only "beam" trigger flag set, useful for calibrations study
    //if (!e.isTriggerBeamOnly()) continue;
    
    // skip known "bad" spills
    if (IsBadBurst(e.run, e.spill)) continue;
    
    // process event reco data
    double ecal = 0;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal += e.ECAL[d][x][y].energy;
    
    double hcal = 0;
    // compute sum over 4 HCAL modules for total energy
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          hcal += e.HCAL[d][x][y].energy;
    
    // HCAL calorimeter modules history:
    // 2015,2016A,2016B  HCAL=0+1+2+3
    // 2017,2018         HCAL=0+1+2    (HCAL3 is a separate zero-degree calorimeter)
    // 2021A             HCAL=0+1+2+3
    // 2021B (mu run)    HCAL=0+1      (it is save to use the sum of 4 above as modules 2+3 are not present and energy is zero)
    // 2022A (mu run)    HCAL=0+1
    // 2022B             HCAL=0+1+2    (HCAL3 is a separate zero-degree calorimeter)
    
    // re-compute for 2017,2018,2022B (3 modules in sum)
    const bool is2017_2018 = (2817 <= e.run) && (e.run <= 4310);
    const bool is2022B = (6035 <= e.run && e.run <= 8458);
    if (is2017_2018 || is2022B)
      hcal = e.hcalTotalEnergy(0) + e.hcalTotalEnergy(1) + e.hcalTotalEnergy(2);
    
    // TODO: double-check the 2021A HCAL is the sum of 4
    
    double bgo = 0;
    for (int x = 0; x < 8; ++x)
      bgo += e.BGO[x].energy;
    
    bgo /= MeV; // convert to MeV
    
    double muon = 0;
    for (int d = 0; d < 4; ++d)
      muon += e.MUON[d].energy;
    
    double atarg = 0;
    for (int x = 0; x < 5; ++x)
      for (int y = 0; y < 5; ++y)
        atarg += e.ATARG[x][y].energy;
    
    ltg0.Fill(e.LTG[0].energy);
    chcnt0.Fill(e.CHCNT[0].energy);
    
    const double srd = (e.SRD[0].energy + e.SRD[1].energy + e.SRD[2].energy) / MeV;

    const double lyso = (e.LYSO[0].amplitude + e.LYSO[1].amplitude + e.LYSO[2].amplitude + e.LYSO[3].amplitude + e.LYSO[4].amplitude + e.LYSO[5].amplitude);

    const bool cutSrd = e.SRD[0].energy > 1*MeV && e.SRD[0].energy < 70*MeV &&
                        e.SRD[1].energy > 1*MeV && e.SRD[1].energy < 70*MeV &&
                        e.SRD[2].energy > 1*MeV && e.SRD[2].energy < 70*MeV;
    
    double vhcal = 0.;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 4; ++x)
        for (int y = 0; y < 4; ++y)
          vhcal += e.VHCAL[d][x][y].energy;
    
    // dimuon sample selection: beam electron -> em shower -> gamma -> mu+ mu-
    const CellXY emcid = e.ecalMaxCell();
    const double ecal0 = e.ecalTotalEnergy(0);
    const double hcal0 = e.hcalTotalEnergy(0);
    const double hcal1 = e.hcalTotalEnergy(1);
    const bool isDimuon = 
      inRange(srd, 10., 200.) &&     // beam is electron
      (emcid == ECAL0BEAMCELL) &&    // beam is in ECAL central cell
      (ecal0 > 0.5) &&
      (ecal < 60.) &&
      inRange(hcal0, 2.5, 6.25) &&   // double-MIP in HCAL0
      inRange(hcal1, 2.5, 6.25);     // double-MIP in HCAL1
      
    if (isDimuon && isGoodEnergy) {
      h0dimuplot.Fill(hcal0);
      h1dimuplot.Fill(hcal1);
    }
    
    // timing
    const Cell& e133 = e.ECAL[1][3][3];
    if (e133.hasDigit)
      e133t0plot.Fill(e133.t0ns());
    
    const bool hasMaster = e.hasMasterTime();
    const double master = e.masterTime;
    
    if (e133.hasDigit && hasMaster) {
      masterplot.Fill(master);
      e133mtt0plot.Fill(e133.t0ns() - master);
      e133_vs_mtt0plot.Fill(master, e133.t0ns());
    }
    
    // histogram preceding (pileup) peak info
    const bool e133_hasPileup = (e133.amplitude_peak_index > 0);
    if (e133_hasPileup) {
      // best peak amplitude and sample index
      const double bestA = e133.amplitude;
      const int bestS = e133.amplitude_sample;
      
      // preceding peak amplitude and sample index
      const double prevA = e133.peakamplitude(e133.amplitude_peak_index-1);
      const int prevS = e133.peaksample(e133.amplitude_peak_index-1);
      
      e133_pileup.Fill(prevS-bestS, prevA/bestA);
      e133_pileup_energy.Fill(e133.pileup());
      /*
      cout << "ECAL133 pileup:"
           << " energy=" << e133.energy
           << " pileup=" << e133.pileup()
           << " energy_nopileup=" << e133.energy_nopileup()
           << " pos=" << (prevS-bestS)
           << " rela=" << prevA/bestA
           << endl;
      */
    }
    
    // in-time ratio
    const double e133r = e133.inTimeWindowRatio(e133.t0, 3);
    e133_it_ratio.Fill(e133r);
    //cout << "ECAL133: t0 = " << e133.t0ns()
    //     << ", ratio(t0 +- 3 time samples) = "  << e133r
    //     << endl;
    
    if (e.MM1.hasHit()) {
      mm1X.Fill(e.MM1.xpos);
      mm1Y.Fill(e.MM1.ypos);
      mm1X_time.Fill(e.MM1.xplane.bestHitTime());
      mm1Y_time.Fill(e.MM1.yplane.bestHitTime());
      mm1.Fill(e.MM1.xpos, e.MM1.ypos);
      //+ e.MM1.xplane.clusters.size()
      //+ e.MM1.yplane.clusters.size()
    }
    
    if (e.MM2.hasHit()) {
      mm2X.Fill(e.MM2.xpos);
      mm2Y.Fill(e.MM2.ypos);
      mm2X_time.Fill(e.MM2.xplane.bestHitTime());
      mm2Y_time.Fill(e.MM2.yplane.bestHitTime());
      mm2.Fill(e.MM2.xpos, e.MM2.ypos);
    }
    
    if (e.MM3.hasHit()) {
      mm3X.Fill(e.MM3.xpos);
      mm3Y.Fill(e.MM3.ypos);
      mm3X_time.Fill(e.MM3.xplane.bestHitTime());
      mm3Y_time.Fill(e.MM3.yplane.bestHitTime());
      mm3.Fill(e.MM3.xpos, e.MM3.ypos);
    }
    
    if (e.MM4.hasHit()) {
      mm4X.Fill(e.MM4.xpos);
      mm4Y.Fill(e.MM4.ypos);
      mm4X_time.Fill(e.MM4.xplane.bestHitTime());
      mm4Y_time.Fill(e.MM4.yplane.bestHitTime());
      mm4.Fill(e.MM4.xpos, e.MM4.ypos);
    }
    
    if (e.MM5.hasHit()) mm5.Fill(e.MM5.xpos, e.MM5.ypos);
    if (e.MM6.hasHit()) mm6.Fill(e.MM6.xpos, e.MM6.ypos);
    if (e.MM7.hasHit()) mm7.Fill(e.MM7.xpos, e.MM7.ypos);
    if (e.MM8.hasHit()) mm8.Fill(e.MM8.xpos, e.MM8.ypos);
    if (e.MM9.hasHit()) mm9.Fill(e.MM9.xpos, e.MM9.ypos);
    if (e.MM10.hasHit()) mm10.Fill(e.MM10.xpos, e.MM10.ypos);
    if (e.MM11.hasHit()) mm11.Fill(e.MM11.xpos, e.MM11.ypos);
    if (e.MM12.hasHit()) mm12.Fill(e.MM12.xpos, e.MM12.ypos);
    
    // simple track reconstruction
    const SimpleTrack track = simple_tracking_4MM(e);
    if (track) {
      //cout << "mom = " << track.momentum << endl;
      in_angle.Fill(track.in.theta()/1e-3);
      out_angle.Fill(track.out.theta()/1e-3);
      in_out_distance.Fill(track.in_out_distance());
      momentum.Fill(track.momentum);
      
      // extrapolate downstream track segment on ECAL surface
      const TVector3 P_ecal = extrapolate_line(track.out, ECAL_pos.pos.Z());
      //cout << "ecal track x = " << P_ecal.X() << " y = " << P_ecal.Y() << endl;
      ECAL_hit_pos.Fill(P_ecal.X()-ECAL_pos.pos.X(), P_ecal.Y()-ECAL_pos.pos.Y());
    }
    
#ifdef TRACKINGTOOLS
    // track reconstruction with Tracking Tools
    const std::vector<Track_t> upstream = GetReconstructedTracks(e, kRegion::upstream);
    for (unsigned int p = 0; p < upstream.size(); p++) {
      const Track_t& track = upstream.at(p);
      if(track) {
        momentum_up.Fill(track.momentum);
        //std::cout << track << std::endl;
        // Extrapolate best track
        if (p == 0) {
          momentum_up_best.Fill(track.momentum);
          TVector3 P_ecal_genfit = track.getECALHit();
          const TVector3 v = P_ecal_genfit - ECAL_pos.pos;
          ECAL_hit_pos_GenFit.Fill(v.X(), v.Y());
          ECAL_hit_zpos_GenFit.Fill(v.Z());
          // Save ECAL profile
          if (cutSrd && ecal0 > 0.5 && abs(track.momentum-ecal-hcal0-hcal1)>10.)
            profile_ECAL.Fill(P_ecal_genfit.X(), P_ecal_genfit.Y(), ecal+hcal0+hcal1);
        }
      }
    }
    
    const std::vector<Track_t> downstream = GetReconstructedTracks(e, kRegion::downstream);
    for (unsigned int p = 0; p < downstream.size(); p++) {
      const Track_t& track = downstream.at(p);
      momentum_down.Fill(track.momentum);
      double mm7z = MM7_pos.pos.Z();
      mm7x_ures.Fill(track.GetUnbiasedResidual(mm7z).X());
      mm7x_reco.Fill(track.GetRecoHit(mm7z).X());
      //std::cout << track << std::endl;
    }
#endif
    
    // output event reco summary
    cout << "Event " << e.id() << " (#" << nevt << ")"
         << " ECAL = " << ecal << " GeV"
         << " HCAL = " << hcal << " GeV"
         << " VHCAL = " << vhcal/MeV << " MeV"
         << " VETO1 = " << e.vetoEnergy(1)/MeV << " MeV"
         << " SRD = " << srd << " MeV"
         //<< " adc " << e.SRD[0].amplitude << " " << e.SRD[1].amplitude << " " << e.SRD[2].amplitude
         //<< " S2 = " << e.S2.amplitude
         << " MT = " << master << " ns"
         << " ECAL hit point = " << ecalHitCoord(e)
         << endl;
    
    // hodoscopes
    //+ e.HOD0.xhits.size()
    //+ e.HOD0.yhits.size()
    for (size_t i = 0; i < e.HOD0.xhits.size(); ++i) hod0X.Fill(e.HOD0.xhits[i]);
    for (size_t i = 0; i < e.HOD0.yhits.size(); ++i) hod0Y.Fill(e.HOD0.yhits[i]);
    
    for (size_t i = 0; i < e.HOD0.xhits.size(); ++i)
      for (size_t j = 0; j < e.HOD0.yhits.size(); ++j)
        hod0.Fill(e.HOD0.xhits[i], e.HOD0.yhits[j]);
    
    for (size_t i = 0; i < e.HOD1.xhits.size(); ++i) hod1X.Fill(e.HOD1.xhits[i]);
    for (size_t i = 0; i < e.HOD1.yhits.size(); ++i) hod1Y.Fill(e.HOD1.yhits[i]);
    
    for (size_t i = 0; i < e.HOD2.xhits.size(); ++i) hod2X.Fill(e.HOD2.xhits[i]);
    for (size_t i = 0; i < e.HOD2.yhits.size(); ++i) hod2Y.Fill(e.HOD2.yhits[i]);
    
    for (size_t i = 0; i < e.HOD2.xhits.size(); ++i)
      for (size_t j = 0; j < e.HOD2.yhits.size(); ++j)
        hod2.Fill(e.HOD2.xhits[i], e.HOD2.yhits[j]);
    
    
    ehplot.Fill(ecal, hcal);
    eplot.Fill(ecal);
    if (e.isTriggerBeamOnly()) eplot_bo.Fill(ecal);
    eplot_spill.Fill(e.spill, ecal);
    hplot.Fill(hcal);
    h0plot.Fill(hcal0);
    h1plot.Fill(hcal1);
    vhplot.Fill(vhcal);
    veto0plot.Fill(e.vetoEnergy(0)/MeV);
    veto1plot.Fill(e.vetoEnergy(1)/MeV);
    veto2plot.Fill(e.vetoEnergy(2)/MeV);
    bgoplot.Fill(bgo);
    srdplot.Fill(srd);
    lysoplot.Fill(lyso);
    muonplot.Fill(muon);
    atargplot.Fill(atarg);
    wcal0plot.Fill(e.WCAL[0].energy);
    wcal1plot.Fill(e.WCAL[1].energy);
    wcal2plot.Fill(e.WCAL[2].energy/MeV);
    wcal0pileup.Fill(e.WCAL[0].pileup());
    wcal1pileup.Fill(e.WCAL[1].pileup());
    wcal2pileup.Fill(e.WCAL[2].pileup()/MeV);
    wcat.Fill(e.WCAT.energy);
    
    // shower chi2 example
    const double chi = calcShowerChi2(e);
    const bool cutShower = chi < 10;
    
    Chi.Fill(chi);
    if (cutSrd) Chi_srd.Fill(chi);
    
    if (cutShower) ehplot_chi.Fill(ecal,hcal);
    if (cutShower && cutSrd) ehplot_chi_srd.Fill(ecal,hcal);

    // filling gem's histos
    if ( e.GM01->bestHit ) {
        gem1_rel.Fill( e.GM01->bestHit.rel_pos().X(), e.GM01->bestHit.rel_pos().Y() );
        gem1X_time.Fill( e.GM01->bestHit.x.time() );
        gem1Y_time.Fill( e.GM01->bestHit.y.time() );
        //+ e.GM01->xplane.size()
        //+ e.GM01->yplane.size()
    }
    if ( e.GM02->bestHit ) {
        gem2_rel.Fill( e.GM02->bestHit.rel_pos().X(), e.GM02->bestHit.rel_pos().Y() );
        gem2X_time.Fill( e.GM02->bestHit.x.time() );
        gem2Y_time.Fill( e.GM02->bestHit.y.time() );
    }
    if ( e.GM03->bestHit ) {
        gem3_rel.Fill( e.GM03->bestHit.rel_pos().X(), e.GM03->bestHit.rel_pos().Y() );
        gem3X_time.Fill( e.GM03->bestHit.x.time() );
        gem3Y_time.Fill( e.GM03->bestHit.y.time() );
    }
    if ( e.GM04->bestHit ) {
        gem4_rel.Fill( e.GM04->bestHit.rel_pos().X(), e.GM04->bestHit.rel_pos().Y() );
        gem4X_time.Fill( e.GM04->bestHit.x.time() );
        gem4Y_time.Fill( e.GM04->bestHit.y.time() );
    }
    
    
    // Straw
    for (const auto& i : e.ST01.Hits) ST01.Fill(i.pos().X(), i.pos().Y());
    for (const auto& i : e.ST02.Hits) ST02.Fill(i.pos().X(), i.pos().Y());
    for (const auto& i : e.ST03.Hits) ST03.Fill(i.pos().X(), i.pos().Y());
    
    //for (const auto& i : e.ST04.Hits) {
    for (size_t n = 0; n < e.ST04.Hits.size(); ++n) {
      const StHit& i = e.ST04.Hits[n];
      ST04.Fill(i.pos().X(), i.pos().Y());
      ST04X.Fill(i.x.wire());
      ST04Y.Fill(i.y.wire());
      ST04X_t.Fill(i.x.time());
      ST04Y_t.Fill(i.y.time());
    }
    
    //for (const auto& i : e.ST05.Hits) {
    for (size_t n = 0; n < e.ST05.Hits.size(); ++n) {
      const StHit& i = e.ST05.Hits[n];
      ST05.Fill(i.pos().X(), i.pos().Y());
      ST05X.Fill(i.x.wire());
      ST05Y.Fill(i.y.wire());
      ST05X_t.Fill(i.x.time());
      ST05Y_t.Fill(i.y.time());
    }
    
    //for (const auto& i : e.ST06.Hits) {
    for (size_t n = 0; n < e.ST06.Hits.size(); ++n) {
      const StHit& i = e.ST06.Hits[n];
      ST06.Fill(i.pos().X(), i.pos().Y());
      ST06X.Fill(i.x.wire());
      ST06Y.Fill(i.y.wire());
      ST06X_t.Fill(i.x.time());
      ST06Y_t.Fill(i.y.time());
    }
    
    //if (nevt > 10000) break;
  }
  
  TCanvas c("example", "example");
  c.Print(".pdf[");
  
  c.Clear();
  etypeplot.LabelsDeflate("X");
  etypeplot.LabelsOption("a h", "X");
  etypeplot.Draw();
  c.Print(".pdf", "Title:Event type");
  
  // calorimeters
  c.Clear();
  ehplot.Draw("COL");
  c.Print(".pdf", "Title:ECAL vs HCAL");
  
  c.Clear();
  eplot.Draw();
  c.Print(".pdf", "Title:ECAL");
  
  c.Clear();
  eplot_bo.Draw();
  c.Print(".pdf", "Title:ECAL BeamOnly");
  
  c.Clear();
  eplot_spill.Draw();
  c.Print(".pdf", "Title:ECAL vs spill");
  
  c.Clear();
  hplot.Draw();
  //hplot.Fit("gaus");
  //TF1* fitf = hplot.GetFunction("gaus");
  //if (fitf) fitf->SetNpx(300);
  c.Print(".pdf", "Title:HCAL");
  
  c.Clear();
  c.SetLogy();
  vhplot.Draw();
  c.Print(".pdf", "Title:VHCAL");
  c.SetLogy(kFALSE);
  
  c.Clear();
  c.SetLogy();
  wcat.Draw();
  c.Print(".pdf", "Title:WCAT");
  c.SetLogy(kFALSE);
  
  //c.Clear();
  //bgoplot.Draw();
  //c.Print(".pdf");
  
  c.Clear();
  c.SetLogy();
  srdplot.Draw();
  c.Print(".pdf", "Title:SRD");
  c.SetLogy(kFALSE);
  
  c.Clear();
  THStack vetoplots("vetoplots", "VETO;Energy, MeV;#nevents");
  veto0plot.SetLineColor(kRed);
  veto0plot.SetMarkerColor(kRed);
  veto0plot.SetMarkerStyle(kFullSquare);
  veto1plot.SetLineColor(kGreen);
  veto1plot.SetMarkerColor(kGreen);
  veto1plot.SetMarkerStyle(kFullSquare);
  veto2plot.SetLineColor(kBlue);
  veto2plot.SetMarkerColor(kBlue);
  veto2plot.SetMarkerStyle(kFullSquare);
  vetoplots.Add(&veto0plot);
  vetoplots.Add(&veto1plot);
  vetoplots.Add(&veto2plot);
  vetoplots.Draw("nostack");
  c.BuildLegend(0.5, 0.65, 0.7, 0.85);
  c.Print(".pdf", "Title:VETO");
  
  c.Clear();
  veto0plot.Draw();
  c.Print(".pdf", "Title:VETO0");
  
  c.Clear();
  veto1plot.Draw();
  c.Print(".pdf", "Title:VETO1");
  
  c.Clear();
  veto2plot.Draw();
  c.Print(".pdf", "Title:VETO2");
  
  //c.Clear();
  //muonplot.Draw();
  //c.Print(".pdf");
  
  //c.Clear();
  //atargplot.Draw();
  //c.Print(".pdf");
  
  // shower profile
  c.Clear();
  Chi.Draw();
  c.Print(".pdf", "Title:shower profile");
  
  // timing
  c.Clear();
  masterplot.Draw();
  c.Print(".pdf", "Title:timing");
  
  c.Clear();
  e133t0plot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  e133mtt0plot.Draw();
  c.Print(".pdf");
  
  c.Clear();
  e133_vs_mtt0plot.Draw();
  c.Print(".pdf");
  
  // pileup
  c.Clear();
  c.SetLogy();
  e133_pileup_energy.Draw();
  c.Print(".pdf", "Title:pileup");
  c.SetLogy(kFALSE);
  
  //c.Clear();
  //c.SetLogz();
  //e133_pileup.Draw();
  //c.Print(".pdf");
  
  // in-time ratio
  //c.Clear();
  //e133_it_ratio.Draw();
  //c.Print(".pdf");
  
  // micromegas
  c.Clear();
  mm1.Draw("COL");
  c.Print(".pdf", "Title:MM");
  
  c.Clear();
  mm2.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  mm3.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  mm4.Draw("COL");
  c.Print(".pdf");
  
  mm5.Draw("COL");
  c.Print(".pdf");
  
  mm6.Draw("COL");
  c.Print(".pdf");
  
  mm7.Draw("COL");
  c.Print(".pdf");
  
  mm8.Draw("COL");
  c.Print(".pdf");
  
  mm9.Draw("COL");
  c.Print(".pdf");
  
  mm10.Draw("COL");
  c.Print(".pdf");
  
  mm11.Draw("COL");
  c.Print(".pdf");
  
  c.Clear();
  mm1X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm1Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm2X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm2Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm3X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm3Y.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm4X.Draw();
  c.Print(".pdf");
  
  c.Clear();
  mm4Y.Draw();
  c.Print(".pdf");
  
  // GEM
  c.Clear();
  gem1_rel.Draw("COL");
  c.Print(".pdf", "Title:GEM");

  c.Clear();
  gem2_rel.Draw("COL");
  c.Print(".pdf");

  c.Clear();
  gem3_rel.Draw("COL");
  c.Print(".pdf");

  c.Clear();
  gem4_rel.Draw("COL");
  c.Print(".pdf");

  c.Clear();
  gem1X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem1Y_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem2X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem2Y_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem3X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem3Y_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem4X_time.Draw();
  c.Print(".pdf");

  c.Clear();
  gem4Y_time.Draw();
  c.Print(".pdf");
  
  // straw
  c.Clear();
  ST01.Draw();
  c.Print(".pdf", "Title:STRAW");
  
  c.Clear();
  ST02.Draw();
  c.Print(".pdf");
  
  c.Clear();
  ST03.Draw();
  c.Print(".pdf");
  
  c.Clear();
  ST04.Draw();
  c.Print(".pdf");
  
  c.Clear();
  ST05.Draw();
  c.Print(".pdf");
  
  c.Clear();
  ST06.Draw();
  c.Print(".pdf");
  
  // hodoscopes
  c.Clear();
  hod0.Draw("COL");
  c.Print(".pdf", "Title:HOD");
  
  c.Clear();
  hod2.Draw("COL");
  c.Print(".pdf");

  // tracking: momentum
  c.Clear();
  momentum.Draw();
  c.Print(".pdf", "Title:tracking");
  
  c.Clear();
  momentum_up.Draw();
  c.Print(".pdf");

  c.Clear();
  momentum_up_best.Draw();
  c.Print(".pdf");

  c.Clear();
  momentum_down.Draw();
  c.Print(".pdf");

  c.Clear();
  ECAL_hit_pos.Draw();
  c.Print(".pdf");
  
  c.Clear();
  profile_ECAL.Draw();
  // Box defining cell size
  TVector3 cellCenter = CCellECAL(ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, 38.2);
  double cx1 = cellCenter.X() - 38.2/2.; double cx2 = cellCenter.X() + 38.2/2.;
  double cy1 = cellCenter.Y() - 38.2/2.; double cy2 = cellCenter.Y() + 38.2/2.;
  TBox *cellBox = new TBox(cx1, cy1, cx2, cy2);
  cellBox->SetFillStyle(4000); // Transparent box
  cellBox->SetLineWidth(3);
  cellBox->SetLineColor(kBlack);
  cellBox->SetLineStyle(kSolid);
  cellBox->Draw("L");
  // Marker with beam center
  //TMarker marker(cellCenter.X(), cellCenter.Y(), 47);
  TMarker marker(ECAL0BEAMSPOT_pos.X(), ECAL0BEAMSPOT_pos.Y(), 47);
  marker.Draw();
  c.Print(".pdf");

  c.Clear();
  h0dimuplot.Draw();
  h0dimuplot.Fit("gaus");
  TF1* fitf1 = h0dimuplot.GetFunction("gaus");
  if (fitf1) fitf1->SetNpx(300);
  c.Print(".pdf", "Title:dimuon");
  
  c.Clear();
  h1dimuplot.Draw();
  h1dimuplot.Fit("gaus");
  TF1* fitf2 = h1dimuplot.GetFunction("gaus");
  if (fitf2) fitf2->SetNpx(300);
  c.Print(".pdf");
  
  c.Print(".pdf]");
  
  // save all histograms to .root file
  file->Write();
  file->Close();
  
  return 0;
}
