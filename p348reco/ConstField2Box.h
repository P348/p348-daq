/* Copyright 2008-2010, Technische Universitaet Muenchen,
   Authors: Christian Hoeppner & Sebastian Neubert & Johannes Rauch

   This file is part of GENFIT.

   GENFIT is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GENFIT is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GENFIT.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @addtogroup genfit
 * @{
 */

#ifndef genfit_ConstField2Box_h
#define genfit_ConstField2Box_h

#include "AbsBField.h"

namespace genfit {

class ConstField2Box : public AbsBField {
 public:
  //! define the constant field in this ctor
 ConstField2Box(double b1, double b2, double b3, 
  double xmin1, double xmax1, double ymin1, double ymax1, double zmin1, double zmax1,
  double xmin2, double xmax2, double ymin2, double ymax2, double zmin2, double zmax2
  )
    : field_(b1, b2, b3)
    {
      _xmin1 = xmin1;
      _xmax1 = xmax1;
      _ymin1 = ymin1;
      _ymax1 = ymax1;
      _zmin1 = zmin1;
      _zmax1 = zmax1;

      _xmin2 = xmin2;
      _xmax2 = xmax2;
      _ymin2 = ymin2;
      _ymax2 = ymax2;
      _zmin2 = zmin2;
      _zmax2 = zmax2;
    }

  ConstField2Box(const TVector3& field)
    : field_(field)
  { ; }

  //! return value at position
  TVector3 get(const TVector3& pos) const;
  void get(const double& posX, const double& posY, const double& posZ, double& Bx, double& By, double& Bz) const;

 private:
  TVector3 field_;
  double _xmin1;
  double _xmax1;
  double _ymin1;
  double _ymax1;
  double _zmin1;
  double _zmax1;
  double _xmin2;
  double _xmax2;
  double _ymin2;
  double _ymax2;
  double _zmin2;
  double _zmax2;
};

} /* End of namespace genfit */
/** @} */

#endif // genfit_ConstFieldBox_h
